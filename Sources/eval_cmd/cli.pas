unit cli;

interface

Procedure Eval;
Procedure CR;
Procedure AfficheAide(commande: string = '');
Procedure Run;

implementation

uses SysUtils, evalcli, utils;

Procedure AfficheAide(commande: string = '');
begin
  if (commande = '') then
  begin
    Writeln('usage : nebula commande params');
    Writeln('commandes');
    Writeln('  eval : effectue une �val (fichier .nba) ou simulation d''�val (fichier .nbt)');
    Writeln('         param�tres :  [-genNBT] FichierNBx FichierOrdres');
    Writeln('                       -genNBT: g�n�ration d''un fichier .nbt issu de la simul d''�val');
    Writeln('');
    Writeln('  cr   : affiche le CR du fichier .nbt (d''un joueur indiqu� si fichier .nba)');
    Writeln('         param�tres : txt|nbt NomFichierNBx [numjou]');
  end
  else if commande.ToLower = 'eval' then
  begin
    Writeln('param�tres commande eval :  [-genNBT] FichierNBx FichierOrdres');
    Writeln('    [-genNBT] (option demande de g�n�rer le fichier .nbt issu de la simul d''�val. ');
    Writeln('              N�cessite un fichier .nbt en entr�e. ');
    Writeln('              Si le fichier d''entr�e est un .nba, un fichier .nba est g�n�r�');
    Writeln('    FichierNBx : chemin du fichier .nba ou .nbt');
    Writeln('              Fichier .nba : une �valuation de la partie est effectu�e.');
    Writeln('              Fichier .nbt : une simulation d''�valuation est effectu�e.');
    Writeln('    FichierOrdres : chemin du fichier contenant les ordres. ');
    Writeln('              En cas d''�valuation les ordres de chaque joueur doivent �tre pr�c�d�s de @Begin Player numjou et se terminer par @End');
    Writeln('              Pour une simulation d''�val, s''il ne s''agit pas d''une partie par �quipe le fichier d''ordres est pris en l''�tat');
    Writeln('              S''il s''agit d''une partie par �quipe, les ordres des autres co�quipiers seront ajout�s si pr�sents dans le m�me r�pertoire');
  end
  else if commande.ToLower = 'cr' then
  begin
    Writeln('param�tres commande cr :  txt|nbt FichierNBx [numjou]');
    Writeln('    txt affiche le CR texte du joueur sur la sortie standard');
    Writeln('    nbt affiche le fichier nbt du joueur sur la sortie standard');
    Writeln('    FichierNBx : chemin du fichier .nba ou .nbt');
    Writeln('    numjou : joueur dont on affiche le CR (dans le cas d''un fichier .nba)');
    Writeln('             all dans le cas d''un .nba dont on veut juste r�cup�rer tous les mondes, pas d''en-t�te et pas de r�capitulatif des ordres');
  end;
end;

Procedure Eval;
var
  i: integer;
  Nebula: TNebula;
  deltaParam: integer;
  generationNBT: boolean;
  paramTmp: string;

begin
  deltaParam := 0;
  generationNBT := False;

  for i := 2 to ParamCount do
  begin
    if ParamStr(i) = '-genNBT' then
    begin
      inc(deltaParam);
      generationNBT := True;
    end;
  end;

  if (ParamStr(1 + deltaParam) = '') or (ParamStr(2 + deltaParam) = '') or
    (ParamStr(3 + deltaParam) = '') then
  begin
    AfficheAide('eval');
    Exit;
  end;

  Nebula := TNebula.Create;
  paramTmp := ConvertFilenameSlashes(ParamStr(2 + deltaParam));
  if FileExists(paramTmp) then
    Nebula.Set_FicNBA(paramTmp)
  else
  begin
    Writeln(paramTmp + ' : fichier non trouv�');
    Exit;
  end;
  paramTmp := ConvertFilenameSlashes(ParamStr(3 + deltaParam));
  if FileExists(paramTmp) or (paramTmp = '0') then
    Nebula.Set_FicOrdres(paramTmp)
  else
  begin
    Writeln(paramTmp + ' : fichier non trouv�');
    Exit;
  end;
  Writeln(Nebula.Eval(generationNBT));
  Nebula.Free;

end;

Procedure CR;
var
  NomFic: string;
  NumJou: integer;
  Nebula: TNebula;
  TypeCR: string;
begin
  if (ParamStr(2) = '') or (ParamStr(3) = '') then
  begin
    AfficheAide('cr');
    Exit;
  end;

  TypeCR := ParamStr(2).ToLower;
  if (TypeCR <> 'txt') and (TypeCR <> 'nbt') then
  begin
    AfficheAide('cr');
    Exit;
  end;

  NomFic := ConvertFilenameSlashes(ParamStr(3));
  NumJou := 0;

  if not FileExists(NomFic) then
  begin
    Writeln('ERREUR : fichier ' + NomFic + ' non trouv�');
    Exit;
  end;

  if (String(ExtractFileExt(NomFic)).ToLower <> '.nba') and
     (String(ExtractFileExt(NomFic)).ToLower <> '.nbt') then
  begin
    Writeln('ERREUR : le fichier ' + NomFic +
      ' n''est pas un fichier .nba ou .nbt');
  end;

  if String(ExtractFileExt(NomFic)).ToLower = '.nba' then
  begin
    if (ParamStr(4) = '') then
    begin
      Writeln('ERREUR : le num�ro de joueur doit �tre sp�cifi� pour un fichier .nba');
      Writeln;
      AfficheAide('cr');
      Exit;
    end;

//    if (ParamStr(4) = '0') then
//    begin
//      Writeln('ERREUR : le num�ro de joueur ne peut �tre 0 pour un fichier .nba');
//      Writeln;
//      AfficheAide('cr');
//      Exit;
//    end;

    if not TryStrToInt(ParamStr(4), NumJou) and (ParamStr(4).ToLower <> 'all') then
      Writeln('ERREUR : le num�ro de joueur doit �tre un nombre ou ''all''');

    if ParamStr(4).ToLower = 'all' then
        NumJou := 255
  end;

  Nebula := TNebula.Create;

  Nebula.Set_FicNBA(NomFic);

  if (TypeCR = 'txt') then
    Writeln(Nebula.AfficheCRTxtFromNBx(NumJou));

  if (TypeCR = 'nbt') then
    Writeln(Nebula.AfficheNBx(NumJou));

  Nebula.Free;
end;

Procedure Run;
begin
  // On cherche la commande dans les param�tres
  if ParamStr(1).ToLower = 'eval' then
  begin
    Eval;
    Exit;
  end;

  if ParamStr(1).ToLower = 'cr' then
  begin
    CR;
    Exit;
  end;

  if ParamStr(1) <> '' then
  begin
    Writeln('*** ERREUR *** commande ' + ParamStr(1) + ' inconnue');
    Writeln;
  end;

  AfficheAide;
end;

end.
