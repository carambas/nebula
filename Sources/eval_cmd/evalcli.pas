unit evalcli;

interface

uses
  nebdata;

type
  Tnebula = class
  public
    function Eval(generationNBT: boolean): String;
    function AfficheCRTxtFromNBx(NoJou: integer): string;
    function AfficheNBx(NoJou: integer): string;

    // Setters / Getters
    function Get_FicOrdres: String;
    procedure Set_FicNBA(const Value: String);
    function Get_FicNBA: String;
    procedure Set_FicOrdres(const Value: String);
  private
    m_NomFicNBA: string;
    m_NomFicOrdres: string;
    procedure GenereCR(Data: TNebData; NoJou: integer);
    procedure GenereAllCR(Data: TNebData);
    procedure AfficheStats(Data: TNebData);
  end;

implementation

uses
  Classes, SysUtils,
  avatunit,
  erreurseval,
  imprime,
  utils,
  avateval;

function Tnebula.Get_FicNBA: String;
begin
  result := m_NomFicNBA
end;

procedure Tnebula.Set_FicNBA(const Value: String);
begin
  m_NomFicNBA := Value;
end;

function Tnebula.Get_FicOrdres: String;
begin
  result := m_NomFicOrdres;
end;

procedure Tnebula.Set_FicOrdres(const Value: String);
begin
  m_NomFicOrdres := Value;
end;

function Tnebula.Eval(generationNBT: boolean): String;
var
  Data, Data2: TNebData;
  OrdresLst, EvalLst: TStringList;
  NoJoueur, NoCoeq, i: integer;
  OrdresTmp: TStringList;
  OrdresFileName: string;
  RepNBT: string;
  Simul: boolean;
  CRList: TSTringList;
begin
  result := 'Probl�me dans l''eval';
  Data := nil;
  Data2 := nil;
  OrdresLst := nil;
  EvalLst := nil;
  if not FileExists(m_NomFicNBA) then
  begin
    result := Format('Le fichier %s n''existe pas', [m_NomFicNBA]);
    Exit
  end;
  if not(FileExists(m_NomFicOrdres) or (m_NomFicOrdres = '0')) then
  begin
    result := Format('Le fichier %s n''existe pas', [m_NomFicOrdres]);
    Exit
  end;

  try
    RepNBT := ExtractFilePath(m_NomFicNBA);
    Data := TNebData.Create(m_NomFicNBA, 0);

    Simul := Data.TypeFicDonnees = tfd_NBT;
    NoJoueur := Data.NoJou;

    if m_NomFicOrdres <> '0' then
    begin

      OrdresLst := TStringList.Create;
      if Simul and (Data.Jou[NoJoueur].Equipe.Count > 1) then
      begin
        OrdresTmp := TStringList.Create;

        // On cherche des fichiers de format %d dans le m�me r�pertoire que les ordres
        for i := 0 to Data.Jou[NoJoueur].Equipe.Count - 1 do
        begin
          NoCoeq := StrToInt(Data.Jou[NoJoueur].Equipe[i]);
          if NoCoeq <> NoJoueur then
            OrdresFileName :=
              Format('%s%d', [ExtractFilePath(m_NomFicOrdres), NoCoeq])
          else
            OrdresFileName := m_NomFicOrdres;

          if FileExists(OrdresFileName) then
          begin
            OrdresLst.Add(BeginCh + Data.Jou[NoJoueur].Equipe[i]);
            OrdresTmp.LoadFromFile(OrdresFileName);
            OrdresLst.AddStrings(OrdresTmp);
            OrdresLst.Add(EndCh);
          end;
        end;
        OrdresTmp.Free;
        Data2 := EvalTourData(Data, OrdresLst, 0, Simul);
      end
      else
      begin
        OrdresLst.LoadFromFile(m_NomFicOrdres);
        Data2 := EvalTourData(Data, OrdresLst, 0, Simul);
      end;
    end;

    if Data.TypeFicDonnees = tfd_NBT then
    begin
      EvalLst := TStringList.Create;
      ImprimeData(Data2, Data, Data2.NoJou, EvalLst);

      if generationNBT then
      begin
        EvalLst.SaveToFile(Format('%s%.3sR%.02d%.02d.txt',
          [RepNBT, Data.NomPartie, NoJoueur, Data2.Tour]));
      end;
      result := EvalLst.Text;
      EvalLst.Free;

      if generationNBT then
      begin
        Data2.SauveEnNBT(NoJoueur, RepNBT);
      end;
    end
    else
    begin
      // Eval compl�te => on g�n�re les CR
      GenereAllCR(Data2);
      // Affichage de stats
      AfficheStats(Data2);
      result := 'Eval termin�e';
    end;

  finally
    Data.Free;
    Data2.Free;
    OrdresLst.Free;
  end;
end;

procedure Tnebula.GenereCR(Data: TNebData; NoJou: integer);
begin
  //ImprimeData(Data, nil, nil, NoJou, Data.GetNomListingJouTr(NoJou, Data.Tour));
  Data.SauveEnNBT(NoJou, ExtractFilePath(Data.GetNomListing));
end;

procedure Tnebula.GenereAllCR(Data: TNebData);
var
  i: integer;
begin
  for i := 1 to Data.NbJou do
    GenereCR(Data, i);
end;

function Tnebula.AfficheCRTxtFromNBx(NoJou: integer): string;
var
  Data: TNebData;
begin
  Data := TNebData.Create(m_NomFicNBA, 0);

  if (Data.TypeFicDonnees = tfd_NBT) then
    NoJou := Data.NoJou;

  Result := ImprimeData(Data, nil, NoJou);
end;

function Tnebula.AfficheNBx(NoJou: integer): string;
var
  FileContent, CRNBT: TStringList;
  NomFichier: string;
  Data: TNebData;
begin
  if ExtractFileExt(m_NomFicNBA) = '.nbt' then
  begin
    FileContent := TStringList.Create;
    FileContent.LoadFromFile(m_NomFicNBA);
    result := FileContent.Text;
    Exit;
  end;

  NomFichier := GetNomFicTempo;
  Data := TNebData.Create(m_NomFicNBA, 0);
  Data.SauveEnNBT(NoJou, ExtractFilePath(NomFichier),
    ExtractFileName(NomFichier));

  CRNBT := TStringList.Create;
  CRNBT.LoadFromFile(NomFichier);
  DeleteFile(NomFichier);

  result := CRNBT.Text;

  CRNBT.Free;

end;

procedure Tnebula.AfficheStats(Data: TNebData);
var
  NbFlottes, NbMondes: integer;
  NbFlottesStr, NbMondesStr, ScoresStr: string;
  Vainqueur, ScoreVainqueur: integer;
  i, m, f: integer;
begin
  Vainqueur := 0;
  ScoreVainqueur := 0;
  NbFlottesStr := '';
  NbMondesStr := '';
  ScoresStr := '';

  for i := 1 to Data.NbJou do
  begin
    // Calcul des stats du joueur i
    NbFlottes := 0;
    NbMondes := 0;
    for m := 1 to Data.NbMonde do
      if Data.m[m].Proprio = i then
        Inc(NbMondes);
    for f := 1 to Data.NbFlotte do
      if Data.f[f].Proprio = i then
        Inc(NbFlottes);
    if i > 1 then
    begin
      NbFlottesStr := NbFlottesStr + ',';
      NbMondesStr := NbMondesStr + ',';
      ScoresStr := ScoresStr + ',';
    end;
    NbFlottesStr := NbFlottesStr + IntToStr(NbFlottes);
    NbMondesStr := NbMondesStr + IntToStr(NbMondes);
    ScoresStr := ScoresStr + IntToStr(Data.Jou[i].Score);
  end;

  if Data.Tour >= Data.TourFinal then
    for i := Data.NbJou downto 1 do
      if Data.Jou[i].Score > ScoreVainqueur then
      begin
        ScoreVainqueur := Data.Jou[i].Score;
        Vainqueur := i;
      end;

  // Affichage des Stats
  Writeln('Scores=' + ScoresStr);
  Writeln('NbMondes=' + NbMondesStr);
  Writeln('NbFlottes=' + NbFlottesStr);
  if Vainqueur > 0 then
    Writeln('Vainqueur=' + IntToStr(Vainqueur));
end;

end.
