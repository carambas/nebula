program Debpar;

uses
  Forms,
  MAIN in 'MAIN.PAS' {MainForm},
  PLAN in 'PLAN.PAS' {PlanForm},
  ABOUT in 'ABOUT.PAS' {AboutDialog},
  ATTENTE in 'ATTENTE.PAS' {Patience},
  INFO in 'INFO.PAS' {InfoDialog},
  TAILLEP in 'TAILLEP.PAS' {TaillePlanDialog},
  AVATUNIT in '..\Shared\AVATUNIT.PAS',
  sortie in '..\Shared\sortie.PAS' {SortieForm},
  TireMonde in 'TireMonde.pas',
  NBTStringList in '..\Shared\NBTStringList.pas',
  NebData in '..\Shared\NebData.pas',
  ListInt in '..\Shared\ListInt.pas',
  utils in '..\shared\utils.pas',
  Harmonisation in 'Harmonisation.pas';

begin
  Application.Title := 'Cr�ateur de partie de N�bula';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TPlanForm, PlanForm);
  Application.CreateForm(TAboutDialog, AboutDialog);
  Application.CreateForm(TPatience, Patience);
  Application.CreateForm(TInfoDialog, InfoDialog);
  Application.CreateForm(TTaillePlanDialog, TaillePlanDialog);
  Application.CreateForm(TSortieForm, SortieForm);
  Application.Run;
end.

