�
 TINFODIALOG 0#  TPF0TInfoDialog
InfoDialogLeft2Top� BorderStylebsDialogCaptionInfos sur la partieClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreatePixelsPerInchx
TextHeight TBitBtnOKBtnLeft[Top�Width_Height!Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder KindbkOKMarginSpacing�	IsControl	  TBitBtn	CancelBtnLeft� Top�Width_Height!Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickCancelBtnClickKindbkCancelMarginSpacing�	IsControl	  TTabbedNotebookTabbedNotebook1Left Top Width�Height�	PageIndex
TabsPerRowTabFont.CharsetDEFAULT_CHARSETTabFont.Color	clBtnTextTabFont.Height�TabFont.NameMS Sans SerifTabFont.Style TabOrder TTabPage LeftTopCaption	   Général TLabelLabel51Left?Top!WidthcHeightCaptionNom de la partie  TLabelLabel52LeftoTop\Width1HeightCaptionMondes  TLabelLabel53Left4TopzWidthnHeightCaption   Mondes de départ  TLabelLabel54Left^Top� WidthCHeightCaptionTrous noirs  TLabelLabel55Left7Top?WidthjHeightCaptionNombre de flottes  TLabelLabel1Left
TopWidth� Height� AutoSizeCaption�   Saisissez ici le nom de la partie et le nombre de flottes que vous désirez placer dans l'univers. Le nombre de mondes, de mondes de départ et de trous noirs est déjà déterminé par votre dessin du plan.WordWrap	  TLabelLabel9LeftTop� Width� HeightCaption   Nb de noms de trésors  TLabelLabel10LeftTop� Width� HeightCaption   Nb d'ajectifs de trésors  TLabelLabel11LeftTop� Width� HeightCaption   Nombre de trésor total  TEditNomEditLeft� TopWidthFHeightCharCaseecUpperCase	MaxLengthTabOrder TextGALION  TEdit	MondeEditLeft� TopWWidthFHeightEnabledTabOrderText	MondeEdit  TEditMdEditLeft� TopuWidthFHeightEnabledTabOrderTextMdEdit  TEditTNEditLeft� Top� WidthFHeightEnabledTabOrderTextTNEdit  TEdit
FlotteEditLeft� Top:WidthFHeightTabOrderText
FlotteEdit  TEditNbNomTresEditLeft� Top� WidthFHeightTabOrderTextNbNomTresEditOnChangeNbNomTresEditChange  TEditNbAdjTresEditLeft� Top� WidthFHeightTabOrderTextNbAdjTresEditOnChangeNbNomTresEditChange  TEdit
NbTresEditLeft� Top� WidthFHeightEnabledTabOrderText
NbTresEdit   TTabPage LeftTopCaption	Variantes TLabelLabel13LeftTop� WidthvHeightCaptionTir sur les industries  TLabelLabel14LeftTop"WidthYHeightCaption>   Indiquez ici les options des règles que vous voulez utiliser.  TLabelLabel15LeftTop WidthHeight  	TCheckBoxPiratageMultiClasseLeftTop(Width� HeightCaption   Piratage à 20 contre 1TabOrder   	TCheckBoxAffichageScoreLeftTop@Width� HeightCaptionAffichage ScoresChecked	State	cbCheckedTabOrder  	TCheckBoxBombesInterditesLeftTopXWidth� HeightCaptionBombes interditesTabOrder  	TCheckBoxMEVAPourTousLeftToppWidth� HeightCaptionMEVA pour tousTabOrder  	TCheckBoxDiploInterditeLeftTop� Width� HeightCaptionDiplomatie interditeTabOrder  	TCheckBoxPillagesInterditsLeftTop� Width� HeightCaptionPillages interditsTabOrder  	TComboBoxTirIndustriesLeft� Top� Width� Height
ItemHeight	ItemIndexTabOrderTextOuiItems.StringsNonOui Uniquement sur celles des autres   	TCheckBoxPartieCRUELLeftTop� WidthYHeightCaption)Partie CRUEL (harmo et classement sur CR)TabOrder  	TCheckBoxEmigrationSansHasardLeftTop� WidthHeightCaptionEmigration sans hasardTabOrder   TTabPage LeftTopCaption   Config. Réseau TLabel
EmailLabelLeft
TopWidth� HeightCaptionEmail serveur de mail  TLabelLabel5Left
TopOWidthHeightCaption0Mot de passe de la partie sur le serveur de mail  TLabelLabel6Left
Top� WidthHeightCaption1Nom de l'arbitre tel qu'il apparait sur les mails  TLabelLabel7Left
Top� Width� HeightCaptionAdresse Email de l'arbitre  TLabelLabel8Left
Top;Width�Height3AutoSizeCaptiong   Les informations que vous saisirez ici serviront à Nébula pour l'envoi des compte-rendus aux joueurs.WordWrap	  TLabelLabel12Left
Top WidthOHeightCaption8   Code cryptage utilisé pour le stockage des CR sur le SM  TEditServeurEmailEditLeft
Top'Width�HeightTabOrder TextServeurEmailEdit  TEditPasswordEditLeft
TopbWidth�HeightTabOrderTextPasswordEdit  TEditArbitreEmailEditLeft
Top� Width�HeightTabOrderTextArbitreEmailEdit  TEditNomArbitreEditLeft
Top� Width�HeightTabOrderTextNomArbitreEdit  TEditCodeCryptageEditLeft
TopWidth�HeightTabOrderTextCodeCryptageEdit   TTabPage LeftTopCaption   Mdes de dép. TLabelLabel46LeftTop3Width9HeightCaption
Industries  TLabelLabel47LeftTop[WidthGHeightCaptionPopulations  TLabelLabel48LeftTop� Width,HeightCaptionRobots  TLabelLabel49LeftTop� WidthDHeightCaptionMP initiales  TLabelLabel2Left� Top,Width� Height� AutoSizeCaption�   Saisissez ici les caractéristiques des mondes de départ des joueurs. Si vous spécifiez un nombre de MP initiales inférieur au nombre d'industries, toutes ne pourront produire au tour 0. Le nombre de robots ne s'applique qu'aux robotrons.WordWrap	  TLabelLabel16LeftTop� Width$HeightCaptionVI/VP  TLabelLabel17LeftTop� WidthbHeightCaptionVC sur les flottesWordWrap	  TLabelLabel18LeftTop$WidtheHeightCaption   Capacité minière  TEditIndLeft� Top/WidthBHeightTabOrder Text30  TEditPopLeft� TopWWidthBHeightTabOrderText50  TEditRobLeft� TopWidthBHeightTabOrderText40  TEditMPLeft� Top� WidthBHeightTabOrderText30  TEditVIVPLeft� Top� WidthAHeightTabOrderText1  TEdit	VCFlottesLeft� Top� WidthAHeightTabOrderText0  TEditCMLeft� Top WidthAHeightTabOrderText2   TTabPage LeftTopCaptionEquipes TLabelLabelEquipesLeft1Top1Width� Height0Caption]   Indiquez ici pour chaque joueur le numéro de son équipe. 0 ou blanc signifie qu'il est seulWordWrap	  TStringGridGridEquipesLeft1Top'Width)Height� ColCountDefaultColWidthnDefaultRowHeightRowCount!OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoTabsgoAlwaysShowEditor TabOrder 
RowHeights   	TCheckBoxCheckParEquipeLeft1Top
Width� HeightCaption   Partie par équipesTabOrderOnClickCheckParEquipeClick   TTabPage LeftTopCaptionMds TLabelLabelMdLeft1Top'WidthHeight0Captionk   Les Md sont classésde haut en bas et de gauche à droite. Indiquez le numéro du joueur affecté à ce Md.WordWrap	  	TCheckBoxCheckMdLeft1Top
Width� HeightCaptionAffectation manuelle des MdsTabOrder OnClickCheckMdClick  TStringGridGridMdLeft1Top'Width)Height� ColCountDefaultColWidthxDefaultRowHeightRowCount!OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoTabsgoAlwaysShowEditor TabOrder   TTabPage LeftTopCaption   Coût tech. TLabelLabel3LeftTop WidthPHeight)AutoSizeCaptionq   Saisissez ici le coût de chaque niveau technologique en fonction de la technologie et de la classe de personnageWordWrap	  TStringGridGridTechClassLeftTop1WidthJHeight� Hint&   Coût de chaque technologie par classeColCountDefaultColWidthDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoTabsgoAlwaysShowEditor ParentShowHint
ScrollBarsssNoneShowHint	TabOrder 	ColWidthsP 
RowHeights    TTabPage LeftTopCaption	Tech max. TLabelLabel4LeftTop
WidthFHeight!AutoSizeCaptioneSaisissez le niveau maximal de chaque technologie. Un niveau de 0 signifie qu'il n'y a pas de limite.WordWrap	  TStringGridGridTechMaxLeftTop1WidthJHeight� Hint$Niveau maximum de chaque technologieColCountDefaultColWidthDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoTabsgoAlwaysShowEditor ParentShowHint
ScrollBarsssNoneShowHint	TabOrder 	ColWidthsP      