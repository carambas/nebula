’
 TMAINFORM 0  TPF0	TMainFormMainFormLeft© Topž BorderStylebsSingleCaption*   CrĆ©ation d'une nouvelle partie de NĆ©bulaClientHeightClientWidthāColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style MenuMainMenuOldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthāHeight!AlignalTopParentShowHintShowHint	TabOrder  TSpeedButtonSpeedButton1Left<TopWidth\HeightHintSaisie du plan
Glyph.Data
ź  ę  BMę      v   (   W            p                                   ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ ffff fffffffffffffffff ffffffffffffffffffff``        ffffffff         fffffff        ``        ffffffff         fffffff        ``’’’’’’šffffffff »»»»»»» fffffff        ``’’’’’’šffffffff »»»»»»» fffffff        ``’’’’’’šffffffff »»»»»»» fffffff        ``’’’’’’šffffffff »»»»»»» fffffff     ``’’’’’’šffffffff »» » fffffff    ’ ``’’’’’’šffffffff »»°» fffffff    ’ ``’’’’’’šffffffff »»°» fffffff    ``’’’’’’š          »»°»            ``’’’’’’š          »°»           š ``’’’’’’šffffffff » °°» fffffff   š ``’’’’’’šffffffff »» » fffffff ’’  ``’’’’’’šffffffff »»»»»»» fffffff        ``’’’’’’šffffffff »»»»»»» fffffff        ``’’’’’’šffffffff »»»»»»» fffffff        ``        ffffffff         fffffff        ``        ffffffff         fffffff        `fffffffffffffffffffffffffffffffffffffffffff`Layout
blGlyphTopParentShowHintShowHint	OnClickSaisieduplan1Click  TSpeedButtonSpeedButton2LeftTopWidthHeightHintOuvrir un fichier
Glyph.Data
j  f  BMf      v   (               š                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’                  {{{{{{  ·····  {{{{{p  °·····°  š{{{{{{  æ  ···  ūūūš     æææææ°  ūūūūūš  ææ                 ParentShowHintShowHint	OnClickFichier1Click  TSpeedButtonSpeedButton3LeftTopWidthHeightHint   Sauver le dĆ©but de partie
Glyph.Data
j  f  BMf      v   (               š                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’               ĢĢ÷pĢ  ĢĢ÷pĢ  ĢwpĢ  Ģ`  Ģ  ĢĢĢĢĢĢ  Ę    l  Ą’’’’  Ą’’’’  Ą’’’’  Ą’’’’   ’’’’   Ą’’’’                ParentShowHintShowHint	OnClickSauver1Click  TSpeedButtonSpeedButton4Left TopWidthHeightHint   GĆ©nĆ©ration de la partie
Glyph.Data
j  f  BMf      v   (               š                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ wwwwwwwwww  wwwwwwwwww  wwwpwwwwww  wwwpwwwww  wwwpšwwwww  wwwpæwwww  wwwpūšwwww  wwwpææwww  ww  ūš www  wwææ°wwww  wwpūūūwww  wwpæææwww  wwwūūšwww  wp æ° www  wpūūūšwwww  wwæææwww  wwūūūwww  wwpæææ°www  wwp    www  wwwwwwwwww  OnClickGnrerlapartie1Click   TStringGridGrilleLeft TopxWidthįHeightxColCountDefaultColWidth DefaultRowHeightRowCount!OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine	goEditinggoTabsgoAlwaysShowEditor 
ScrollBars
ssVerticalTabOrderOnSetEditTextGrilleSetEditText	ColWidths ” \Y; }   TPanel	StatusBarLeft TopWidthāHeightAlignalBottom	AlignmenttaLeftJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightō	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TMainMenuMainMenuLeftTop( 	TMenuItemMenuCaption&Fichier 	TMenuItemFichier1Caption
&Ouvrir...OnClickFichier1Click  	TMenuItemSauver1Caption&SauverOnClickSauver1Click  	TMenuItemSauvercomme1CaptionSauver &comme...OnClickSauvercomme1Click  	TMenuItemN2Caption-  	TMenuItem
ProprietesCaption   &PropiĆ©tĆ©s de la partie...OnClickProprietesClick  	TMenuItemVrifierlasaisie1Caption   &VĆ©rifier la saisieOnClickVrifierlasaisie1Click  	TMenuItemGnrerlapartie1Caption   &GĆ©nĆ©rer la partie...OnClickGnrerlapartie1Click  	TMenuItemN1Caption-  	TMenuItemQuitter1Caption&QuitterShortCutX  OnClickQuitter1Click   	TMenuItemPlanMenuCaption&Plan 	TMenuItemTailleduplan1Caption&Taille du plan...OnClickTailleduplan1Click  	TMenuItemSaisieduplan1Caption&Saisie du planOnClickSaisieduplan1Click   	TMenuItemAide1Caption&Aide 	TMenuItemApropos1Caption&A propos...OnClickApropos1Click    TOpenDialog
OpenDialogFilter(   DĆ©but de partie|*.deb|Fichier NBA|*.nbaOptionsofFileMustExist Title   Ouverture d'un dĆ©but de partieLeft0Top(  TSaveDialog
SaveDialog
DefaultExtdebFilter   DĆ©but de partie|*.debOptionsofOverwritePrompt Title   Sauvegarde du dĆ©but dce partieLeftXTop(   