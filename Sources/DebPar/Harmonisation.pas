unit Harmonisation;

interface

uses NebData, Classes, Avatunit, Dialogs, SysUtils, TireMonde,
  System.Generics.Collections;

Type
  TTypeRes = (Industrie, CM, Flotte, Tresor);

  THarmonisation = class abstract (TObject)
    protected
      Data : PNebData;

      procedure GetMdesCour(NumJou, CourMax: Integer; Md: boolean;
        ListMdes: TList<Integer>);

    public
      constructor Create(Data : PNebData);

      procedure Harmonise; virtual; abstract;
  end;

  THarmonisationClassique = class(THarmonisation)
    public
      procedure Harmonise; override;

    private
      logFile: TextFile;
      JouList: array [0 .. NB_JOU_MAX] of TList<Integer>;
      JouListExcl: array [0 .. NB_JOU_MAX] of TList<Integer>;
      total: Integer;
      moy, Min, max: real;
      mintrouve: boolean;

      function GetMondeValeur(NoMonde: Integer;
        CompteFlottes: boolean): Integer;
      function RecenceRessourcesCouronnes(Res: TTypeRes;
        NumJou: Integer; ListMdes: TList<Integer>): Integer;
      procedure HarmoniseRessource(Res: TTypeRes;
        IntervalleMin, IntervalleMax: Integer);
      procedure HarmoniseFlottes(IntFlottesMin, IntFlottesMax: Integer);
      procedure HarmoniseTresors(IntMin, IntMax: Integer);
      procedure HarmoniseParLeBasStep(NumJou: Integer);
      procedure HarmoniseParLeHautStep(NumJou: Integer);
      procedure CalculeMoyMinMax;
      procedure HarmoniseValeursIntervalle(Intervalle: Integer);
      procedure HarmoniseValeurs;
  end;

  THarmonisationCruel = class(THarmonisation)
    public
      procedure Harmonise; override;

    private
      logFile: TextFile;
      JouFlotteList: array [-1 .. NB_JOU_MAX] of TList<Integer>;
      JouList: array [0 .. NB_JOU_MAX] of TList<Integer>;

      procedure HarmoInd(NumJou, Difference: Integer; ListMdes: TList<Integer>);
      procedure HarmoCM(NumJou, Difference: Integer; ListMdes: TList<Integer>);
      procedure HarmoPop(NumJou, Difference: Integer; ListMdes: TList<Integer>);
      procedure HarmoFlottes(NumJou, Difference: Integer;
        ListMdes, FlotteList: TList<Integer>);
  end;


implementation

constructor THarmonisation.Create(Data : PNebData);
begin
  inherited Create;
  self.Data := Data;
end;

procedure THarmonisationClassique.HarmoniseRessource(Res: TTypeRes;
  IntervalleMin, IntervalleMax: Integer);
var
  tot: Integer;
  moy: real;
  JouList: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouFlotteList: array [-1 .. NB_JOU_MAX] of TList<Integer>;
  JouTresorList: array [-1 .. NB_JOU_MAX] of TList<Integer>;
  NbRes: array [1 .. NB_JOU_MAX] of Integer;
  Min, max: Integer;

  procedure HarmoInd(NumJou: Integer);
  var
    DiffInd: Integer;
    MondeModif: Integer;
  begin
    with Data^ do
    begin
      DiffInd := 0;
      if NbRes[NumJou] < Min then
        DiffInd := Min - NbRes[NumJou]
      else if NbRes[NumJou] > max then
        DiffInd := max - NbRes[NumJou];

      // On ram�ne DiffInd � 0;
      while DiffInd <> 0 do
      begin
        // On prend un monde au hasard dans la liste des mondes du joeuur
        MondeModif := JouList[NumJou][Random(JouList[NumJou].Count)];
        if DiffInd < 0 then // faut virer des industries
        begin
          if M[MondeModif].Ind > 0 then
          begin
            if M[MondeModif].Ind >= -DiffInd then
            begin // On enl�ve jsute ce qu'il faut d'I et fini
              M[MondeModif].Ind := M[MondeModif].Ind + DiffInd;
              DiffInd := 0;
            end
            else
            begin // On vire toutes les I et ce n'est pas fini
              DiffInd := DiffInd + M[MondeModif].Ind;
              M[MondeModif].Ind := 0;
              M[MondeModif].VI := 0;
              M[MondeModif].VP := 0;
            end;
          end;
        end
        else // faut ajouter des industries
        begin
          if M[MondeModif].Ind < 4 then
          begin
            if M[MondeModif].Ind + DiffInd <= 4 then
            // On ajoute ce qu'il faut d'I et c'est fini
            begin
              M[MondeModif].Ind := M[MondeModif].Ind + DiffInd;
              DiffInd := 0;
            end
            else
            begin // On pase le monde � 4 I et ce n'est pas fini
              DiffInd := DiffInd - (4 - M[MondeModif].Ind);
              M[MondeModif].Ind := 4;
            end;
          end;
        end;
      end;

    end;
  end;

  procedure HarmoCM(NumJou: Integer);
  var
    DiffCM: Integer;
    MondeModif: Integer;
  begin
    with Data^ do
    begin
      DiffCM := 0;
      if NbRes[NumJou] < Min then
        DiffCM := Min - NbRes[NumJou]
      else if NbRes[NumJou] > max then
        DiffCM := max - NbRes[NumJou];

      // ShowMessage(Format('HarmoCM(%d)'#13'Moy = %f'#13'NbRes[NumJou]=%d'#13'DiffInd=%d', [NumJou, moy, NbRes[NumJou], DiffCM]));

      // On ram�ne DiffCM � 0;
      while DiffCM <> 0 do
      begin
        // On prend un monde au hasard dans la liste des mondes du joeuur
        MondeModif := JouList[NumJou][Random(JouList[NumJou].Count)];
        if DiffCM < 0 then // faut virer des CM
        begin
          if M[MondeModif].PlusMP > 0 then
          begin
            if M[MondeModif].PlusMP >= -DiffCM then
            begin // On enl�ve jsute ce qu'il faut de CM et fini
              M[MondeModif].PlusMP := M[MondeModif].PlusMP + DiffCM;
              DiffCM := 0;
            end
            else
            begin // On vire toutes les CM et ce n'est pas fini
              DiffCM := DiffCM + M[MondeModif].PlusMP;
              M[MondeModif].PlusMP := 0;
            end;
          end;
        end
        else // faut ajouter des CM
        begin
          if M[MondeModif].PlusMP < 9 then
          begin
            if M[MondeModif].PlusMP + DiffCM <= 9 then
            // On ajoute ce qu'il faut de CM et c'est fini
            begin
              M[MondeModif].PlusMP := M[MondeModif].PlusMP + DiffCM;
              DiffCM := 0;
            end
            else
            begin // On pase le monde � 4 I et ce n'est pas fini
              DiffCM := DiffCM - (9 - M[MondeModif].PlusMP);
              M[MondeModif].PlusMP := 9;
            end;
          end;
        end;
      end;
    end;
  end;

  procedure HarmoFlottes(NumJou: Integer);
  var
    DiffFlotte: Integer;
    i: Integer;
    NoFlotte, NoMonde: Integer;
  begin
    with Data^ do
    begin
      DiffFlotte := 0;
      if NbRes[NumJou] < Min then
        DiffFlotte := Min - NbRes[NumJou]
      else if NbRes[NumJou] > max then
        DiffFlotte := max - NbRes[NumJou];

      if DiffFlotte < 0 then // Il va faloir virer des flottes
      begin
        for i := 1 to -DiffFlotte do
        begin // On vire la flotte
          NoFlotte := JouFlotteList[NumJou]
            [Random(JouFlotteList[NumJou].Count)];
          JouFlotteList[0].Add(NoFlotte);
          JouFlotteList[NumJou].Remove(NoFlotte);
          NoMonde := JouList[0][Random(JouList[0].Count)];
          F[NoFlotte].Localisation := NoMonde;
        end;
      end
      else if DiffFlotte > 0 then // L� il faut aller ajouter des flottes
      begin
        for i := 1 to DiffFlotte do
        begin
          NoFlotte := JouFlotteList[0][Random(JouFlotteList[0].Count)];
          JouFlotteList[0].Remove(NoFlotte);
          JouFlotteList[NumJou].Add(NoFlotte);
          NoMonde := JouList[NumJou][Random(JouList[NumJou].Count)];
          F[NoFlotte].Localisation := NoMonde;
        end;
      end;
    end;
  end;

  procedure HarmoTresors(NumJou: Integer);
  var
    DiffTresor: Integer;
    i: Integer;
    NoTresor, NoMonde: Integer;
  begin
    with Data^ do
    begin
      DiffTresor := 0;
      if NbRes[NumJou] < Min then
        DiffTresor := Min - NbRes[NumJou]
      else if NbRes[NumJou] > max then
        DiffTresor := max - NbRes[NumJou];

      if DiffTresor < 0 then // Il va faloir virer des tresors
      begin
        for i := 1 to -DiffTresor do
        begin // On vire le tr�sor
          NoTresor := JouTresorList[NumJou]
            [Random(JouTresorList[NumJou].Count)];
          JouTresorList[0].Add(NoTresor);
          JouTresorList[NumJou].Remove(NoTresor);
          NoMonde := JouList[0][Random(JouList[0].Count)];
          T[NoTresor].Localisation := NoMonde;
          T[NoTresor].Statut := 0; // Sur le monde
        end;
      end
      else if DiffTresor > 0 then // L� il faut aller ajouter des tr�sors
      begin
        for i := 1 to DiffTresor do
        begin // On ajoute le tr�sor
          NoTresor := JouTresorList[0][Random(JouTresorList[0].Count)];
          JouTresorList[0].Remove(NoTresor);
          JouTresorList[NumJou].Add(NoTresor);
          NoMonde := JouList[NumJou][Random(JouList[NumJou].Count)];
          T[NoTresor].Localisation := NoMonde;
          T[NoTresor].Statut := 0; // Sur le monde
        end;
      end;
    end;
  end;

var
  ListMdes: TList<Integer>;
  fl, Md, i, tr, j: Integer;
begin
  with Data^ do
  begin
    Randomize;
    FillChar(JouList, sizeof(JouList), 0);
    FillChar(JouFlotteList, sizeof(JouFlotteList), 0);
    FillChar(JouTresorList, sizeof(JouTresorList), 0);

    JouList[0] := TList<Integer>.Create;
    for j := -1 to NbJou do
      JouFlotteList[j] := TList<Integer>.Create;

    for j := -1 to NbJou do
      JouTresorList[j] := TList<Integer>.Create;

    for Md := 1 to NbMonde do
      if (M[Md].Proprio = 0) and (not TN[Md]) then
        JouList[0].Add(Md);

    for fl := 1 to NbFlotte do
      if F[fl].Proprio = 0 then
        JouFlotteList[0].Add(fl)
      else
        JouFlotteList[-1].Add(fl);
    // On ne regarde que les flottes neutres (donc pas sur un Md)

    for tr := 1 to NbTres do
      if T[tr].Localisation > 0 Then
        JouTresorList[0].Add(tr);

    tot := 0;
    for j := 1 to NbJou do
    begin
      ListMdes := TList<Integer>.Create;
      NbRes[j] := RecenceRessourcesCouronnes(Res, j, ListMdes);
      tot := tot + NbRes[j];
      JouList[j] := ListMdes;

      // On enl�ve les mondes recens�s de la liste des mondes � C >= 3
      for i := 0 to JouList[j].Count - 1 do
        JouList[0].Remove(JouList[j][i]);

      // On met de c�t� les flottes situ�es sur ces mondes
      for fl := 1 to NbFlotte do
        if ListMdes.IndexOf(F[fl].Localisation) >= 0 then
        begin
          JouFlotteList[0].Remove(fl);
          JouFlotteList[j].Add(fl);
        end;

      // On met de c�t� les tr�sors situ�s sur ces mondes
      for tr := 1 to NbTres do
        if ListMdes.IndexOf(GetTresorMonde(tr)) >= 0 then
        begin
          JouTresorList[0].Remove(tr);
          JouTresorList[j].Add(tr);
        end;
    end;

    moy := tot / NbJou;
    Min := Round(moy * (IntervalleMin / 100));
    max := Round(moy * (IntervalleMax / 100));

    // Bidouille QOURSE
    { If Res = Flotte then
      begin
      min := 18;
      max := 18;
      end;
      If Res = Tresor then
      begin
      min := 3;
      max := 3;
      end; }

    case Res of
      Industrie:
        for j := 1 to NbJou do
          HarmoInd(j);
      CM:
        for j := 1 to NbJou do
          HarmoCM(j);
      Flotte:
        for j := 1 to NbJou do
          HarmoFlottes(j);
      Tresor:
        for j := 1 to NbJou do
          HarmoTresors(j);
    end;

    case Res of
      Industrie:
        ShowMessage(Format('Ind : min=%d, moy=%.2f, max=%d', [Min, moy, max]));
      CM:
        ShowMessage(Format('CM : min=%d, moy=%.2f, max=%d', [Min, moy, max]));
      Flotte:
        ShowMessage(Format('Fl : min=%d, moy=%.2f, max=%d', [Min, moy, max]));
      Tresor:
        ShowMessage(Format('tr : min=%d, moy=%.2f, max=%d', [Min, moy, max]));
    end;

    for j := 0 to NbJou do
      JouList[j].Free;

    for j := -1 to NbJou do
      JouFlotteList[j].Free;

    for j := -1 to NbJou do
      JouTresorList[j].Free;
  end;
end;

function THarmonisationClassique.RecenceRessourcesCouronnes(Res: TTypeRes;
  NumJou: Integer; ListMdes: TList<Integer>): Integer;
var
  i: Integer;
  fl, tr: Integer;
begin
  with Data^ do
  begin
    Result := 0;
    GetMdesCour(NumJou, 2, Res = Tresor, ListMdes);
    for i := 0 to ListMdes.Count - 1 do
      case Res of
        Industrie:
          Result := Result + M[ListMdes[i]].Ind;
        CM:
          Result := Result + M[ListMdes[i]].PlusMP;
        Flotte:
          for fl := 1 to NbFlotte do
            if F[fl].Localisation = ListMdes[i] then
              inc(Result);
        Tresor:
          for tr := 1 to NbTres do
            if GetTresorMonde(tr) = ListMdes[i] then
              inc(Result);
      end;
  end;
end;

procedure THarmonisationClassique.Harmonise;
begin
    HarmoniseTresors(100, 100);
    HarmoniseFlottes(100, 100);
    HarmoniseValeurs;
end;

procedure THarmonisationClassique.HarmoniseFlottes(IntFlottesMin, IntFlottesMax: Integer);
begin
  HarmoniseRessource(Flotte, IntFlottesMin, IntFlottesMax);
end;

procedure THarmonisationClassique.HarmoniseTresors(IntMin, IntMax: Integer);
begin
  HarmoniseRessource(Tresor, IntMin, IntMax);
end;

procedure THarmonisationClassique.HarmoniseParLeBasStep(NumJou: Integer);
var
  Min, mondeMin: Integer;
  i: Integer;
begin
  with Data^ do
  begin
    Writeln(logFile, Format('Harmonise par le bas, joueur %d', [NumJou]));
    Min := 0;
    mondeMin := 0;

    // On cherche le monde le plus mauvais
    mintrouve := False;
    for i := 0 to JouList[NumJou].Count - 1 do
    begin
      if ((not mintrouve) or (Min > GetMondeValeur(JouList[NumJou][i],
        False))) and (JouListExcl[NumJou].IndexOf(JouList[NumJou][i]) = -1)
      then
      begin
        Min := GetMondeValeur(JouList[NumJou][i], False);
        mondeMin := JouList[NumJou][i];
        mintrouve := True;
      end;
    end;

    Writeln(logFile, Format('M_%d. Valeur avant : %d', [mondeMin, Min]));
    // On retire le monde (jusqu'� 5 fois)
    for i := 1 to 5 do
    begin
      TireMde(M[mondeMin]);
      Writeln(logFile, Format('M_%d. Valeur apr�s : %d',
        [mondeMin, GetMondeValeur(mondeMin, False)]));
      if GetMondeValeur(mondeMin, False) > Min then
        break;
    end;
  end;
end;

procedure THarmonisationClassique.HarmoniseParLeHautStep(NumJou: Integer);
var
  max, mondeMax: Integer;
  i: Integer;
begin
  Writeln(logFile, Format('Harmonise par le haut, joueur %d', [NumJou]));
  max := 0;
  mondeMax := 0;

  // On cherche le meilleur monde
  for i := 0 to JouList[NumJou].Count - 1 do
  begin
    if (JouListExcl[NumJou].IndexOf(JouList[NumJou][i]) = -1) and
      (max < GetMondeValeur(JouList[NumJou][i], False)) then
    begin
      max := GetMondeValeur(JouList[NumJou][i], False);
      mondeMax := JouList[NumJou][i];
    end;
  end;

  Writeln(logFile,
    Format('M_%d. Valeur avant : %d -> On essaye de la ramener entre %.0f et %d',
    [mondeMax, max, max * 0.7, max - 1]));
  // On retire le monde (jusqu'� 5 fois)
  for i := 1 to 100 do
  begin
    TireMde(Data^.M[mondeMax]);
    Writeln(logFile, Format('M_%d. Valeur apr�s : %d',
      [mondeMax, GetMondeValeur(mondeMax, False)]));
    if (GetMondeValeur(mondeMax, False) < max) and
      (GetMondeValeur(mondeMax, False) >= max * 0.7) then
      break;
  end;
end;

procedure THarmonisationClassique.CalculeMoyMinMax;
var
  i, j: Integer;
begin
  Min := 0;
  max := 0;
  moy := 0;
  for j := 1 to Data.NbJou do
  begin
    total := 0;
    for i := 0 to JouList[j].Count - 1 do
      inc(total, GetMondeValeur(JouList[j][i], True));
    moy := moy + total;
    if j = 1 then
      Min := total
    else if Min > total then
      Min := total;
    if total > max then
      max := total;
    Writeln(logFile, Format('Jou %d : %d', [j, total]));
  end;
  moy := moy / Data^.NbJou;
  Writeln(logFile, '-------------');
  Writeln(logFile, Format('Min : %.0f, Moy : %.0f, Max : %.0f',
    [Min, moy, max]));
end;

procedure THarmonisationClassique.HarmoniseValeursIntervalle(Intervalle: Integer);
var
  i, j, bid: Integer;
begin
  moy := 0;
  Min := 0;
  max := 0;

  // On calcule la valeur des mondes moyenne de chaque joueur
  CalculeMoyMinMax;
  Min := (100 - Intervalle) * moy / 100;
  max := (100 + Intervalle) * moy / 100;
  Writeln(logFile, Format('Intervalles autoris�s : min = %.0f, max = %.0f',
    [Min, max]));

  for j := 1 to Data.NbJou do
  begin
    for bid := 1 to 100 do
    begin
      // Calcul du total
      total := 0;
      for i := 0 to JouList[j].Count - 1 do
        inc(total, GetMondeValeur(JouList[j][i], True));

      // Am�lioration par le bas pour les valeurs totales trop faibles
      if total < Min then
      begin
        Writeln(logFile, Format('Joueur %d : valeur %d trop basse',
          [j, total]));
        HarmoniseParLeBasStep(j);
      end
      else if total > max then
      begin
        Writeln(logFile, Format('Joueur %d : valeur %d trop haute',
          [j, total]));
        HarmoniseParLeHautStep(j);
      end
      else
        break;
    end;
  end;

  // On calcule la valeur des mondes moyenne de chaque joueur
  // Histoire de r�capituler
  Writeln(logFile, 'Apr�s Harmonisation');
  CalculeMoyMinMax;
end;

procedure THarmonisationClassique.HarmoniseValeurs;
var
  NumJou: Integer;
  ListMdes: TList<Integer>;

begin
  AssignFile(logFile, ExtractFilePath(ParamStr(0)) + 'debpar.log');
  Rewrite(logFile);
  Writeln(logFile, 'D�but des logs');
  Writeln(logFile, '--------------');
  Writeln(logFile, '');

  FillChar(JouList, sizeof(JouList), 0);
  JouList[0] := TList<Integer>.Create;
  JouListExcl[0] := TList<Integer>.Create;

  // Harmonisation des C1
  Writeln(logFile, 'C1');
  Writeln(logFile, '--');
  Writeln(logFile, '');

  for NumJou := 1 to Data.NbJou do
  begin
    ListMdes := TList<Integer>.Create;
    GetMdesCour(NumJou, 1, False, ListMdes);
    JouList[NumJou] := ListMdes;
    ListMdes := TList<Integer>.Create;
    JouListExcl[NumJou] := ListMdes;
  end;

  HarmoniseValeursIntervalle(10);

  // Harmonisation des C1+C2
  Writeln(logFile, '');
  Writeln(logFile, '');
  Writeln(logFile, 'C1+C2');
  Writeln(logFile, '-----');
  Writeln(logFile, '');

  for NumJou := 1 to Data.NbJou do
  begin
    ListMdes := TList<Integer>.Create;
    GetMdesCour(NumJou, 2, False, ListMdes);
    JouList[NumJou] := ListMdes;

    // Mondes � ne pas toucher : C1
    ListMdes := TList<Integer>.Create;
    GetMdesCour(NumJou, 1, False, ListMdes);
    JouListExcl[NumJou] := ListMdes;
  end;

  HarmoniseValeursIntervalle(5);

  CloseFile(logFile);
end;

procedure THarmonisationCruel.HarmoInd(NumJou, Difference: Integer; ListMdes: TList<Integer>);
// Parcours la liste des mondes et harmonise le nb d'I
var
  MondeModif: Integer;
  DiffInd: Integer;

begin
  with Data^ do
  begin
    DiffInd := Difference;
    // On ram�ne DiffInd � 0;
    while DiffInd <> 0 do
    begin
      // On prend un monde au hasard dans la liste des mondes du joueur
      MondeModif := ListMdes[Random(ListMdes.Count)];
      if DiffInd < 0 then // faut virer des industries
      begin
        if M[MondeModif].Ind > 0 then
        begin
          if M[MondeModif].Ind > -DiffInd then
          begin // On enl�ve juste ce qu'il faut d'I et fini
            M[MondeModif].Ind := M[MondeModif].Ind + DiffInd;
            DiffInd := 0;
          end
          else
          begin // On vire toutes les I et ce n'est pas fini
            DiffInd := DiffInd + M[MondeModif].Ind;
            M[MondeModif].Ind := 0;
            M[MondeModif].VI := 0;
            M[MondeModif].VP := 0;
          end;
        end;
      end
      else // faut ajouter des industries
      begin
        if M[MondeModif].Ind < 4 then
        begin
          if M[MondeModif].Ind + DiffInd <= 4 then
          // On ajoute ce qu'il faut d'I et c'est fini
          begin
            if M[MondeModif].Ind = 0 then
              // monde pas prot�g� on ajoute des vi/vp
              if Odd(DiffInd) then
                M[MondeModif].VI := Random(2) + 1
              else
                M[MondeModif].VP := Random(2) + 1;
            if M[MondeModif].PlusMP = 0 then
            // monde sans cm on ajoute de la cm
            begin
              M[MondeModif].PlusMP := Random(3) + 1;
              M[MondeModif].MP := Random(M[MondeModif].PlusMP);
            end;

            M[MondeModif].Ind := M[MondeModif].Ind + DiffInd;
            DiffInd := 0;
          end
          else
          begin // On pase le monde � 4 I et ce n'est pas fini
            if M[MondeModif].Ind = 0 then
              M[MondeModif].VP := 1;
            DiffInd := DiffInd - (4 - M[MondeModif].Ind);
            M[MondeModif].Ind := 4;
          end;
        end;
      end;
    end;
  end;
end;

procedure THarmonisationCruel.HarmoCM(NumJou, Difference: Integer; ListMdes: TList<Integer>);
var
  DiffCM: Integer;
  MondeModif: Integer;
begin
  with Data^ do
  begin
    DiffCM := Difference;

    // On ram�ne DiffCM � 0;
    while DiffCM <> 0 do
    begin
      // On prend un monde au hasard dans la liste des mondes du joueur
      MondeModif := ListMdes[Random(ListMdes.Count)];
      if DiffCM < 0 then // faut virer des CM
      begin
        if M[MondeModif].PlusMP > 1 then
        begin
          DiffCM := DiffCM + 1;
          M[MondeModif].PlusMP := M[MondeModif].PlusMP - 1;
        end;
      end
      else // faut ajouter des CM
      begin
        if M[MondeModif].PlusMP < 9 then
        begin
          DiffCM := DiffCM - 1;
          // $$$
          M[MondeModif].PlusMP := M[MondeModif].PlusMP + 1;
        end;
      end;
    end;
  end;
end;

procedure THarmonisationCruel.HarmoPop(NumJou, Difference: Integer; ListMdes: TList<Integer>);
var
  DiffPop: Integer;
  MondeModif: Integer;
begin
  with Data^ do
  begin
    DiffPop := Difference;

    // ShowMessage(Format('HarmoCM(%d)'#13'Moy = %f'#13'NbRes[NumJou]=%d'#13'DiffInd=%d', [NumJou, moy, NbRes[NumJou], DiffCM]));

    // On ram�ne DiffPop � 20 pop pr�s;
    while Abs(DiffPop) > 20 do
    begin
      // On prend un monde au hasard dans la liste des mondes du joueur
      MondeModif := ListMdes[Random(ListMdes.Count)];
      if DiffPop < 0 then // faut virer des Pop
      begin
        if M[MondeModif].Pop > 50 then
        begin
          DiffPop := DiffPop + 25;
          M[MondeModif].Pop := M[MondeModif].Pop - 25;
        end;
      end
      else // faut ajouter des pop
      begin
        DiffPop := DiffPop - 25;
        M[MondeModif].Pop := M[MondeModif].Pop + 25;
        M[MondeModif].MaxPop := M[MondeModif].MaxPop + 25;
      end;
    end;
  end;
end;

procedure THarmonisationCruel.HarmoFlottes(NumJou, Difference: Integer;
  ListMdes, FlotteList: TList<Integer>);
var
  DiffFlotte: Integer;
  i: Integer;
  NoFlotte, NoMonde: Integer;
  oldNoMonde: Integer;

begin
  with Data^ do
  begin
    Writeln(logFile, Format('  Harmo Flottes jou %d, diff %d',
      [NumJou, Difference]));
    DiffFlotte := Difference;

    // ShowMessage(Format('HarmoFlottes(%d)'#13'Moy = %f'#13'Nbres[NumJou]=%d'#13'NbFlottes=%d'#13'DiffInd=%d', [NumJou, moy, NbRes[NumJou], JouFlotteList[NumJou].Count, DiffFlotte]));

    // Attention a verifier les flottes sur mon indus ou pas !

    if DiffFlotte < 0 then // Il va falloir virer des flottes
    begin
      for i := 1 to -DiffFlotte do
      begin // On vire la flotte
        NoFlotte := FlotteList[Random(FlotteList.Count)];
        oldNoMonde := F[NoFlotte].Localisation;
        JouFlotteList[0].Add(NoFlotte);
        JouFlotteList[NumJou].Remove(NoFlotte);
        // Remove de la liste locale aussi ?
        FlotteList.Remove(NoFlotte);
        if (JouList[0].Count > 0) then
          NoMonde := JouList[0][Random(JouList[0].Count)]
        else
          NoMonde := 0;
        F[NoFlotte].Localisation := NoMonde;
        Writeln(logFile, Format('    Flotte %d enlev�e de %d vers %d',
          [NoFlotte, oldNoMonde, NoMonde]));
      end;
    end
    else if (DiffFlotte > 0) then // L� il faut aller ajouter des flottes
    begin
      if ListMdes.Count = 0 then
      begin
        Writeln(logFile, Format('    *** Erreur : la liste des mondes o� placer les flottes en trop (C3 non connect�s) est vide. Joueur %d, DiffFlotte %d',
          [NumJou, DiffFlotte]));
        ShowMessage('Erreur dans l''harmonisation des flottes');
        Exit;
      end;

      for i := 1 to DiffFlotte do
      begin
        if (JouFlotteList[0].Count > 0) then
        begin
          NoFlotte := JouFlotteList[0][Random(JouFlotteList[0].Count)];
          oldNoMonde := F[NoFlotte].Localisation;
          JouFlotteList[0].Remove(NoFlotte);
          JouFlotteList[NumJou].Add(NoFlotte);
          FlotteList.Add(NoFlotte);
          NoMonde := ListMdes[Random(ListMdes.Count)];
          F[NoFlotte].Localisation := NoMonde;
          Writeln(logFile, Format('    Flotte %d ajout�e de %d vers %d',
            [NoFlotte, oldNoMonde, NoMonde]));
        end;
      end;
    end;
  end;
end;

procedure THarmonisationCruel.Harmonise;
// YeDo Fonction d'harmonisation sp�ciale Cruel !
// On s'inspire de l'autre mais il va falloir faire le m�nage !
var

  tot: Integer;
  JouListC1: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouListC2: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouListC3: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouListC3nc: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouListC3nc2: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouFlotteListC1: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouFlotteListC2: array [0 .. NB_JOU_MAX] of TList<Integer>;
  JouFlotteListC3nc: array [0 .. NB_JOU_MAX] of TList<Integer>;
  NbInd: array [1 .. NB_JOU_MAX] of Integer;
  NbPop: array [1 .. NB_JOU_MAX] of Integer;
  NbCm: array [1 .. NB_JOU_MAX] of Integer;
  NbC1F: array [1 .. NB_JOU_MAX] of Integer;
  NbC2F: array [1 .. NB_JOU_MAX] of Integer;
  NbC3F: array [1 .. NB_JOU_MAX] of Integer;

  CptInd, CptPop, CptCm, CptC1F, CptC2F, CptC3F: Integer;


// *************** Debut du main ************** //

var
  ListMdes: TList<Integer>;
  // Il va m'en falloir des listes !

  fl, Md, i, j, k, joueur: Integer;
  moyenne, moyennec3nc: Integer;
  MsgStr: string;
  MsgInt: array [1 .. 32] of Integer;
begin
  with Data^ do
  begin
    AssignFile(logFile, ExtractFilePath(ParamStr(0)) + 'debpar.log');
    Rewrite(logFile);
    Writeln(logFile, 'D�but des logs');
    Writeln(logFile, '--------------');
    Writeln(logFile, '');

    FillChar(JouList, sizeof(JouList), 0);
    FillChar(JouFlotteList, sizeof(JouFlotteList), 0);
    FillChar(JouFlotteListC1, sizeof(JouFlotteList), 0);
    FillChar(JouFlotteListC2, sizeof(JouFlotteList), 0);
    FillChar(JouFlotteListC3nc, sizeof(JouFlotteList), 0);
    FillChar(JouListC1, sizeof(JouList), 0);
    FillChar(JouListC2, sizeof(JouList), 0);
    FillChar(JouListC3, sizeof(JouList), 0);
    FillChar(JouListC3nc, sizeof(JouList), 0);
    FillChar(JouListC3nc2, sizeof(JouList), 0);

    // Creation des Tlist de flottes
    JouList[0] := TList<Integer>.Create;
    for j := -1 to NbJou do
      JouFlotteList[j] := TList<Integer>.Create;

    for j := 0 to NbJou do
    begin
      JouListC1[j] := TList<Integer>.Create;
      JouListC2[j] := TList<Integer>.Create;
      JouListC3[j] := TList<Integer>.Create;
      JouListC3nc[j] := TList<Integer>.Create;
      JouListC3nc2[j] := TList<Integer>.Create;
      JouFlotteListC1[j] := TList<Integer>.Create;
      JouFlotteListC2[j] := TList<Integer>.Create;
      JouFlotteListC3nc[j] := TList<Integer>.Create;
      JouList[j] := TList<Integer>.Create;
    end;

    // On rempli la liste des mondes neutres !
    for Md := 1 to NbMonde do
      if (M[Md].Proprio = 0) and (not TN[Md]) then
        JouList[0].Add(Md);

    // On rempli la liste des flottes neutres, puis de chaque joueur
    for fl := 1 to NbFlotte do
      if F[fl].Proprio = 0 then
        JouFlotteList[0].Add(fl)
      else
        JouFlotteList[-1].Add(fl);
    // On ne regarde que les flottes neutres (donc pas sur un Md)

    tot := 0;
    for j := 1 to NbJou do
    // On rempli toutes les listes !
    begin
      GetMdesCour(j, 1, False, JouListC1[j]);
      GetMdesCour(j, 2, False, JouListC2[j]);
      GetMdesCour(j, 3, False, JouListC3[j]);
      GetMdesCour(j, 3, False, JouListC3nc[j]);
      GetMdesCour(j, 3, False, JouListC3nc2[j]);
      GetMdesCour(j, 3, False, JouList[j]);
      // comment faire quand on sait pas faire de copie de liste !

      // Il faut maintenant nettoyer C2 et C3 !
      for i := 0 to JouListC2[j].Count - 1 do
        JouListC3[j].Remove(JouListC2[j][i]);

      for i := 0 to JouListC1[j].Count - 1 do
        JouListC2[j].Remove(JouListC1[j][i]);
    end;

    // Nettoyage des c3nc ! Bonjour la grosse boucle bien bourrine
    ListMdes := TList<Integer>.Create;

    // construction de la liste de toutes les c3 !
    for joueur := 1 to NbJou do
    begin
      // On construit la liste des autres c3 !
      for i := 1 to NbJou do
      begin
        if i <> joueur then
        begin
          for k := 0 to JouListC3[i].Count - 1 do
            ListMdes.Add(JouListC3[i][k]);
        end;
      end;

      // Fin de construction de liste, on check maintenant
      for i := 0 to JouListC3[joueur].Count - 1 do
        for j := 0 to ListMdes.Count - 1 do
          if JouListC3[joueur][i] = ListMdes[j] then
          begin
            JouListC3nc[joueur].Remove(ListMdes[j]);
            JouListC3nc2[joueur].Remove(ListMdes[j]);
          end;

      // Remplir les C3nc2 !
      for i := 0 to JouListC2[joueur].Count - 1 do
        JouListC3nc2[joueur].Remove(JouListC2[joueur][i]);

      for i := 0 to JouListC1[joueur].Count - 1 do
        JouListC3nc2[joueur].Remove(JouListC1[joueur][i]);

      // nettoyer ListMdes !
      for i := ListMdes.Count - 1 downto 0 do
        ListMdes.Remove(ListMdes[i]);
    end;

    for j := 1 to NbJou do
    begin
      // On enl�ve les mondes recens�s de la liste des mondes � C > 3
      for i := 0 to JouListC3nc[j].Count - 1 do // grmmmbl les C3 communes !
        // JouList[0].Remove(JouList[j][i]); // OG 17/03/02 bug de YeDo Grml
        JouList[0].Remove(JouListC3nc[j][i]);

      // On met de c�t� les flottes situ�es sur ces mondes
      for fl := 1 to NbFlotte do
      begin
        if JouListC3nc[j].IndexOf(F[fl].Localisation) >= 0 then
        begin
          JouFlotteList[0].Remove(fl);
          JouFlotteList[j].Add(fl);
        end;
        if JouListC1[j].IndexOf(F[fl].Localisation) >= 0 then
        begin
          JouFlotteList[0].Remove(fl);
          JouFlotteListC1[j].Add(fl);
        end;
        if JouListC2[j].IndexOf(F[fl].Localisation) >= 0 then
        begin
          JouFlotteList[0].Remove(fl);
          JouFlotteListC2[j].Add(fl);
        end;
        if JouListC3nc2[j].IndexOf(F[fl].Localisation) >= 0 then
        begin
          JouFlotteList[0].Remove(fl);
          JouFlotteListC3nc[j].Add(fl);
        end;

      end;
    end;

    // Et maintenant on compte !
    CptInd := 0;
    CptPop := 0;
    CptCm := 0;
    CptC1F := 0;
    CptC2F := 0;
    CptC3F := 0;

    for j := 1 to NbJou do
    begin
      NbInd[j] := 0;
      NbPop[j] := 0;
      NbCm[j] := 0;
      NbC1F[j] := 0;
      NbC2F[j] := 0;
      NbC3F[j] := 0;

      for i := 0 to JouListC3nc[j].Count - 1 do
      begin
        NbInd[j] := NbInd[j] + M[JouListC3nc[j][i]].Ind;
        NbCm[j] := NbCm[j] + M[JouListC3nc[j][i]].PlusMP;
        NbPop[j] := NbPop[j] + M[JouListC3nc[j][i]].Pop;

      end;
      CptInd := CptInd + NbInd[j];
      CptCm := CptCm + NbCm[j];
      CptPop := CptPop + NbPop[j];

      // bonjour le bourrinage la encore !
      for i := 0 to JouListC1[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC1[j][i] then
            inc(NbC1F[j]);

      for i := 0 to JouListC2[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC2[j][i] then
            inc(NbC2F[j]);

      for i := 0 to JouListC3nc2[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC3nc2[j][i] then
            inc(NbC3F[j]);

      CptC1F := CptC1F + NbC1F[j];
      CptC2F := CptC2F + NbC2F[j];
      CptC3F := CptC3F + NbC3F[j];
    end;

    Writeln(logFile, 'Liste des mondes contenus dans JouList');
    for j := 0 to NbJou do
    begin
      Write(logFile, Format('Jou %d (tot=%d): ', [j, JouList[j].Count]));
      for i := 0 to JouList[j].Count - 1 do
        Write(logFile, Format('%d, ', [JouList[j][i]]));
      Writeln(logFile);
    end;
    Writeln(logFile);

    Writeln(logFile, 'Liste des mondes de C1');
    for j := 0 to NbJou do
    begin
      Write(logFile, Format('Jou %d (tot=%d): ', [j, JouListC1[j].Count]));
      for i := 0 to JouListC1[j].Count - 1 do
        Write(logFile, Format('%d, ', [JouListC1[j][i]]));
      Writeln(logFile);
    end;
    Writeln(logFile);

    Writeln(logFile, 'Liste des mondes de C2');
    for j := 0 to NbJou do
    begin
      Write(logFile, Format('Jou %d (tot=%d): ', [j, JouListC2[j].Count]));
      for i := 0 to JouListC2[j].Count - 1 do
        Write(logFile, Format('%d, ', [JouListC2[j][i]]));
      Writeln(logFile);
    end;
    Writeln(logFile);

    Writeln(logFile, 'Liste des mondes de C3nc');
    for j := 0 to NbJou do
    begin
      Write(logFile, Format('Jou %d (tot=%d): ', [j, JouListC3nc[j].Count]));
      for i := 0 to JouListC3nc[j].Count - 1 do
        Write(logFile, Format('%d, ', [JouListC3nc[j][i]]));
      Writeln(logFile);
    end;
    Writeln(logFile);

    Writeln(logFile, 'Liste des mondes de C3nc2');
    for j := 0 to NbJou do
    begin
      Write(logFile, Format('Jou %d (tot=%d): ', [j, JouListC3nc2[j].Count]));
      for i := 0 to JouListC3nc2[j].Count - 1 do
        Write(logFile, Format('%d, ', [JouListC3nc2[j][i]]));
      Writeln(logFile);
    end;
    Writeln(logFile);

    MsgStr := '1. Nombre de flottes situ�es en C2 apr�s comptage' + #10 + #13;
    for j := 1 to NbJou do
    begin
      MsgInt[j] := 0;
      for i := 0 to JouListC2[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC2[j][i] then
          begin
            inc(MsgInt[j]);
            if JouFlotteListC2[j].IndexOf(fl) = -1 then
              MsgStr := MsgStr + '(' + IntToStr(fl) + ')';
          end;
    end;
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' + IntToStr(MsgInt[j]) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '1. Nombre de flottes situ�es en C2 avant harmo (listes)' +
      #10 + #13;
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' +
        IntToStr(JouFlotteListC2[j].Count) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '1. Nombre de flottes situ�es en C3nc avant harmo (comptage)' +
      #10 + #13;
    for j := 1 to NbJou do
    begin
      MsgInt[j] := 0;
      for i := 0 to JouListC3nc2[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC3nc2[j][i] then
          begin
            inc(MsgInt[j]);
            if JouFlotteListC3nc[j].IndexOf(fl) = -1 then
              MsgStr := MsgStr + '(' + IntToStr(fl) + ')';
          end;
    end;
    Writeln(logFile);
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' + IntToStr(MsgInt[j]) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '1. Nombre de flottes situ�es en C3nc avant harmo (listes)' +
      #10 + #13;
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' +
        IntToStr(JouFlotteListC3nc[j].Count) + #10 + #13;
    Writeln(logFile, MsgStr);



    // maintenant on harmonise !

    moyenne := Trunc(CptInd / NbJou);
    for j := 1 to NbJou do
      HarmoInd(j, moyenne - NbInd[j], JouListC3nc[j]);

    moyenne := Trunc(CptPop / NbJou) + 1;
    for j := 1 to NbJou do
      HarmoPop(j, moyenne - NbPop[j], JouListC3nc[j]);

    moyenne := Trunc(CptCm / NbJou) + 1;
    for j := 1 to NbJou do
      HarmoCM(j, moyenne - NbCm[j], JouListC3nc[j]);

    // test verif
    CptInd := 0;
    CptPop := 0;
    CptCm := 0;

    for j := 1 to NbJou do
    begin
      NbInd[j] := 0;
      NbPop[j] := 0;
      NbCm[j] := 0;

      for i := 0 to JouListC3nc[j].Count - 1 do
      begin
        NbInd[j] := NbInd[j] + M[JouListC3nc[j][i]].Ind;
        NbCm[j] := NbCm[j] + M[JouListC3nc[j][i]].PlusMP;
        NbPop[j] := NbPop[j] + M[JouListC3nc[j][i]].Pop;

      end;
      CptInd := CptInd + NbInd[j];
      CptCm := CptCm + NbCm[j];
      CptPop := CptPop + NbPop[j];
    end;

    // Fin test verif

    // Passons aux flottes

    moyenne := 2; // nettoyage des flottes en c3nc2
    moyennec3nc := Trunc(CptC3F / NbJou); // sauvegarde de la moyenne
    Writeln(logFile,
      'Elimination flotte c3nc pour avoir de la flotte en reserve');
    if moyennec3nc < 3 then
      moyennec3nc := 3; // faut pas deconner !
    for j := 1 to NbJou do
      HarmoFlottes(j, moyenne - NbC3F[j], JouListC3nc2[j],
        JouFlotteListC3nc[j]);
    // mise a jour des nouvelles valeurs nbc3F !!
    for j := 1 to NbJou do
      NbC3F[j] := 2;

    Writeln(logFile, 'Harmo Flottes C1');
    moyenne := 3; // c'est pas plus mal ainsi !
    // moyenne := Trunc(CptC1F / NbJou) +1;
    for j := 1 to NbJou do
      HarmoFlottes(j, moyenne - NbC1F[j], JouListC1[j], JouFlotteListC1[j]);

    Writeln(logFile, 'Harmo Flottes C2');
    moyenne := Trunc(CptC2F / NbJou);
    for j := 1 to NbJou do
      HarmoFlottes(j, moyenne - NbC2F[j], JouListC2[j], JouFlotteListC2[j]);

    Writeln(logFile, 'Harmo Flottes C3nc2');
    moyenne := moyennec3nc;
    for j := 1 to NbJou do
      HarmoFlottes(j, moyenne - NbC3F[j], JouListC3nc2[j],
        JouFlotteListC3nc[j]);

    MsgStr := '2. Nombre de flottes situ�es en C1 apr�s comptage' + #10 + #13;
    for j := 1 to NbJou do
    begin
      MsgInt[j] := 0;
      for i := 0 to JouListC1[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC1[j][i] then
          begin
            inc(MsgInt[j]);
            if JouFlotteListC1[j].IndexOf(fl) = -1 then
              MsgStr := MsgStr + '(' + IntToStr(fl) + ')';
          end;
    end;
    Writeln(logFile);
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' + IntToStr(MsgInt[j]) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '2. Nombre de flottes situ�es en C1 apr�s harmo (listes)' +
      #10 + #13;
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' +
        IntToStr(JouFlotteListC1[j].Count) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '2. Nombre de flottes situ�es en C2 apr�s comptage' + #10 + #13;
    for j := 1 to NbJou do
    begin
      MsgInt[j] := 0;
      for i := 0 to JouListC2[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC2[j][i] then
          begin
            inc(MsgInt[j]);
            if JouFlotteListC2[j].IndexOf(fl) = -1 then
              MsgStr := MsgStr + '(' + IntToStr(fl) + ')';
          end;
    end;
    Writeln(logFile);
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' + IntToStr(MsgInt[j]) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '2. Nombre de flottes situ�es en C2 apr�s harmo (listes)' +
      #10 + #13;
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' +
        IntToStr(JouFlotteListC2[j].Count) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '2. Nombre de flottes situ�es en C3nc apr�s comptage' + #10 + #13;
    for j := 1 to NbJou do
    begin
      MsgInt[j] := 0;
      for i := 0 to JouListC3nc2[j].Count - 1 do
        for fl := 1 to NbFlotte do
          if F[fl].Localisation = JouListC3nc2[j][i] then
          begin
            inc(MsgInt[j]);
            if JouFlotteListC3nc[j].IndexOf(fl) = -1 then
              MsgStr := MsgStr + '(' + IntToStr(fl) + ')';
          end;
    end;
    Writeln(logFile);
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' + IntToStr(MsgInt[j]) + #10 + #13;
    Writeln(logFile, MsgStr);

    MsgStr := '2. Nombre de flottes situ�es en C3nc apr�s harmo (listes)' +
      #10 + #13;
    for j := 1 to NbJou do
      MsgStr := MsgStr + IntToStr(j) + ' : ' +
        IntToStr(JouFlotteListC3nc[j].Count) + #10 + #13;
    Writeln(logFile, MsgStr);

    // ShowMessage(Format('Ind : min=%d, moy=%.2f, max=%d', [min, moy, max])  );
    // Verif debug avec listes
    for j := 0 to NbJou do
    begin
      // JouList[j].Free;
      JouListC1[j].Free;
      JouListC2[j].Free;
      JouListC3[j].Free;
    end;

    for j := -1 to NbJou do
      JouFlotteList[j].Free;

    CloseFile(logFile);
  end;
end;

// Renvoie la liste des mondes entre les couronnes 1 et CourMax incluses
{$R-}

procedure THarmonisation.GetMdesCour(NumJou, CourMax: Integer; Md: boolean;
  ListMdes: TList<Integer>);
type
  PExploMonde = ^TExploMonde;
  TExploMonde = array [1 .. 1] of Integer;
var
  CExplo: PExploMonde;

  procedure ExploCouronne(NumJou, monde, CourAExplo: Integer);
  var
    C: Integer;
  begin
    with Data^ do
    begin
      if CExplo[monde] = -1 then // on n'a pas encore compt� ce monde
        ListMdes.Add(monde);
      CExplo[monde] := CourAExplo;
      for C := 1 to 8 do
        if ((M[monde].Connect[C] > 0) and
          (CourAExplo > CExplo[M[monde].Connect[C]] + 1)) then
          ExploCouronne(NumJou, M[monde].Connect[C], CourAExplo - 1);
    end;
  end;

begin
  with Data^ do
  begin
    GetMem(CExplo, NbMonde * sizeof(CExplo[1]));
    FillChar(CExplo^, NbMonde * sizeof(CExplo^), -1);
    ExploCouronne(NumJou, jou[NumJou].Md, CourMax);
    if not Md then
      ListMdes.Remove(jou[NumJou].Md); // on ne compte pas le Md
    FreeMem(CExplo);
  end;
end;
{$R+}

function THarmonisationClassique.GetMondeValeur(NoMonde: Integer;
  CompteFlottes: boolean): Integer;
const
  valPop = 1;
  valCM = 20;
  valI = 30;
  valFlotte = 100;
  valBonusIAct = 30;
  valBonus1IAct = 40;
var
  Pop, IAct, IActPopMax, IActPopInit: Integer;
  i, flottes: Integer;
begin
  with Data^ do
  begin
    // Nombre de populations
    Pop := M[NoMonde].Pop;

    // Nombre d'industries auto suffisantes...
    IAct := M[NoMonde].Ind;
    if M[NoMonde].PlusMP < IAct then
      IAct := M[NoMonde].PlusMP;

    // ... avec la pop initiale
    IActPopInit := IAct;
    if IActPopInit * 2 > Pop then
      IActPopInit := Pop div 2;

    // ... et la pop max
    IActPopMax := IAct;
    if IActPopMax * 2 > M[NoMonde].MaxPop then
      IActPopMax := M[NoMonde].MaxPop div 2;

    // Nombre de flottes sur le monde
    flottes := 0;
    if CompteFlottes then
    begin
      for i := 1 to NbFlotte do
        if F[i].Localisation = NoMonde then
          inc(flottes);
    end;

    if M[NoMonde].Robot > Pop then
      Pop := M[NoMonde].Robot;
    Result := ((Pop + M[NoMonde].MaxPop) div 2) * valPop + M[NoMonde].PlusMP *
      valCM + M[NoMonde].Ind * valI + flottes * valFlotte +
      (IActPopInit + IActPopMax) * valBonusIAct div 2;

    if IActPopInit > 0 then
      Result := Result + valBonus1IAct;
  end;
end;

end.
