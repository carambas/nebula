unit TireMonde;

interface

uses AvatUnit;

procedure TireMde(var M : TMonde);
procedure TireMdeCRUEL(var M : TMonde);

implementation

procedure TireMde(var M : TMonde);
var r : word;
begin
   M.Statut:=0;
   {--- Population ---}
   M.Conv:=0;
   M.ProprConv:=0;
   M.Robot:=0;
   r:=Random(10);
   case r of
      0..3 : M.MaxPop:=Random(100)+1;
      4..8 : M.MaxPop:=Random(260)+1;
      9    : M.MaxPop:=Random(500)+1;
   end;
   M.Pop:=Random(M.MaxPop)+1;
   {--- Industries ---}
   r:=random(7);
   if r<2 then M.Ind:=Random(4)+1 else M.Ind:=0;
   {--- VI,VP ---}
   if M.Ind>0 then
   begin
      r:=Random(3);
      if r<2 then M.VI:=Random(4) else M.VI:=0;
      r:=Random(3);
      if r<2 then M.VP:=Random(4) else M.VP:=0;
   end
   else
   begin
      M.VI:=0;
      M.VP:=0;
   end;
   {--- PC ---}
   M.PC:=0;
   {--- Production mini�re ---}
   {
     +0 15%
     +1 22%
     +2 25%
     +3 10%
     +4 10%
     +5  8%
     +6  3%
     +7  3%
     +8  2%
     +9  2%
   }
   r:=Random(100)+1;
   case r of
      01..15  : M.PlusMP := 0;
      16..37  : M.PlusMP := 1;
      38..62  : M.PlusMP := 2;
      63..72  : M.PlusMP := 3;
      73..82  : M.PlusMP := 4;
      83..90  : M.PlusMP := 5;
      91..93  : M.PlusMP := 6;
      94..96  : M.PlusMP := 7;
      97..98  : M.PlusMP := 8;
      99..100 : M.PlusMP := 9;
   end;
   M.MP:=Random(8);
end;

procedure TireMdeCRUEL(var M : TMonde);
var r : word;
begin
   M.Statut:=0;
   {--- Population ---}
   M.Conv:=0;
   M.ProprConv:=0;
   M.Robot:=0;
   r:=Random(10);
   case r of //$$YeDo 2002/09/17 monde trop pauvres en pop.
      0..2 : M.MaxPop:=Random(80)+20; //mondes de 20 � 100
      3..6 : M.MaxPop:=Random(80)+70; //mondes de 70 � 150
      7..8 : M.MaxPop:=Random(80)+120; //mondes de 120 � 200
      9    : M.MaxPop:=Random(150)+150;//mondes de 150 � 300
   end;
   if M.MaxPop < 20 then
     M.MaxPop := 20;

   M.Pop:=Random(M.MaxPop-3)+3; //$$YeDo 2002/09/17 monde trop pauvres en pop.

   {--- Production mini�re ---}
   {
     +0 15%
     +1 22%
     +2 25%
     +3 10%
     +4 10%
     +5  8%
     +6  3%
     +7  3%
     +8  2%
     +9  2%
   }
   r:=Random(100)+1;
   case r of
      01..15  : M.PlusMP := 0;
      16..37  : M.PlusMP := 1;
      38..62  : M.PlusMP := 2;
      63..72  : M.PlusMP := 3;
      73..82  : M.PlusMP := 4;
      83..90  : M.PlusMP := 5;
      91..93  : M.PlusMP := 6;
      94..96  : M.PlusMP := 7;
      97..98  : M.PlusMP := 8;
      99..100 : M.PlusMP := 9;
   end;
   M.MP:=Random(8);

   {--- Industries ---}
   r:=random(7);
   if r<2 then M.Ind:=Random(4)+1 else M.Ind:=0;
   {--- VI,VP ---}
   if M.Ind>0 then
   begin
      r:=Random(3);
      if r<2 then M.VI:=Random(2)+1 else M.VI:=0;
      r:=Random(3);
      if r<2 then M.VP:=Random(2)+1 else M.VP:=0;

      if M.VI + M.VP = 0 then
        M.VI := 1;

      if M.PlusMP = 0 then
        M.PlusMP := random(2)+1;
   end
   else
   begin
      M.VI:=0;
      M.VP:=0;
   end;
   {--- PC ---}
   M.PC:=0;
end;

end.
