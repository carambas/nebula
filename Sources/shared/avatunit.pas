Unit avatunit;

{
  Nebutil TODO
  Probl�me test version plus r�cente : composant http n'arrive pas � r�cup�rer une URL sur host virtuel
  Envoi de mail � un serveur SMTP � revoir
  Composant Editeur de texte en couleur � revoir
}

INTERFACE

uses NBTStringList, Types;

const
{$IFDEF PASEXPLO}
  NBCLASSE = 6;
{$ELSE}
  NBCLASSE = 7;
{$ENDIF}
  // EXPLOMAX = 4; {Nb d'explo max d'un monde}
  NBNOMTRES = 13;
  NBADJTRES = 14;
  // NbRotCode = 4; // Code de cryptage pour l'envoi des CR
  MAXPOTEXPLO = 300;
  MAXMEVA = 8;
  MAX_CHEMIN = 9;
  NBTYPEORDRES = 21;
  NB_STATS = 11;
{$IFDEF NOGUI}
  mtWarning = 0;
  mbOK = 0;
{$ENDIF}
{$IFDEF LINUX}
  DirectorySeparator = '/';
{$ENDIF}
{$IFDEF MSWINDOWS}
  DirectorySeparator = '\';
{$ENDIF}
{$IFNDEF DEBUG}
  URL_API_NEBMAIL = 'https://nebmail.ludimail.net/api';
{$ELSE}
  //URL_API_NEBMAIL = 'http://localhost:8080/nebmail/public/api';
  URL_API_NEBMAIL = 'https://dev-nebmail.ludimail.net/api';
{$ENDIF}

  COEF_ESPIONNAGE: array [0 .. 7] of real = (0, 0.5, 0.3, 0.2, 0.1, 0.05, 0.02, 0);

  STAT_CM = 1;
  STAT_POPULATIONS = 2;
  STAT_CARGAISON = 3;
  STAT_MONDES = 4;
  STAT_FLOTTES = 5;
  STAT_INDUSTRIES = 6;
  STAT_VAISSEAUX = 7;
  STAT_TRESORS = 8;
  STAT_IND_ACTIVES = 9;
  STAT_CONVERTIS = 10;
  STAT_ROBOTS = 11;

type
  TTypePlan = (PlanAvecCR, PlanSansCR);
  TTechClasse = array [1 .. 6, 1 .. NBCLASSE] of UInt8;
  TMaxTech231 = array [1 .. 6] of UInt8;
  TMaxTech = array [1 .. 6, 1 .. NBCLASSE] of UInt8; { tech,classe }
  TConnexion = Record
    md1, md2: Integer;
  end;



const
  NbTres = 200;
  Version = '4.22';
  VersionNebutil = 402;
  VersionNebutilbeta = 0;
  VersionNebutilpl = 0;
  VersionNebutilMin = 233;
  adresseSMdefaut = 'nebmail@ludimail.net';
  NB_MONDE_MAX = 1024;
  NB_FLOTTE_MAX = 1024;
  NB_JOU_MAX = 32;


  Empereur = 1;
  Marchand = 2;
  Pirate = 3;
  Antiquaire = 4;
  Robotron = 5;
  Missionnaire = 6;
  Explorateur = 7;

  TECH_DEP = 1;
  TECH_ATT = 2;
  TECH_DEF = 3;
  TECH_RAD = 4;
  TECH_ALI = 5;
  TECH_CAR = 6;

  TechCh: array [1 .. 6] of String = ('DEP', 'ATT', 'DEF', 'RAD', 'ALI', 'CAR');

  ConstrInd: array [1 .. 7] of UInt16 = (4, 5, 5, 5, 5, 5, 5);
  direction: array [1 .. 8, 1 .. 2] of Int8 = ((-1, -1), (0, -1), (1, -1),
    (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0));

const
{$IFDEF PASEXPLO}
  TechClasseDef: TTechClasse =
  { Em, Ma, Pi, An, Ro, Mi }
    ((10, 10, 09, 10, 10, 10), { DEP }
    (05, 10, 10, 10, 10, 10), { ATT }
    (10, 10, 11, 05 10, 10), { DEF }
    (05, 05, 05, 05, 05, 05), { RAD }
    (05, 05, 05, 04, 05, 05), { ALI }
    (10, 05, 10, 08, 10, 10)); { CAR }
{$ELSE}
  TechClasseDef: TTechClasse =
  { Em, Ma, Pi, An, Ro, Mi, Ex }
    ((10, 08, 09, 10, 10, 10, 06), { DEP }
    (05, 12, 10, 10, 10, 10, 12), { ATT }
    (10, 12, 11, 05, 10, 10, 12), { DEF }
    (05, 05, 05, 05, 05, 05, 04), { RAD }
    (05, 05, 05, 04, 05, 05, 03), { ALI }
    (10, 05, 10, 08, 10, 10, 10)); { CAR }
{$ENDIF}
{$IFDEF PASEXPLO}
  MaxTechDef: TMaxTech =
  { Em, Ma, Pi, An, Ro, Mi, Ex }
    ((7, 7, 7, 7, 7, 7), { DEP }
    (12, 10, 10, 10, 10, 10), { ATT }
    (10, 10, 10, 12, 10, 10), { DEF }
    (3, 3, 3, 3, 3, 3), { RAD }
    (0, 0, 0, 0, 0, 0), { ALI }
    (0, 0, 0, 0, 0, 0)); { CAR }
{$ELSE}
  MaxTechDef: TMaxTech =
  { Em, Ma, Pi, An, Ro, Mi, Ex }
    ((7, 7, 7, 7, 7, 7, 7), { DEP }
    (12, 09, 10, 10, 10, 10, 09), { ATT }
    (10, 08, 10, 12, 10, 10, 09), { DEF }
    (3, 3, 3, 3, 3, 3, 3), { RAD }
    (0, 0, 0, 0, 0, 0, 0), { ALI }
    (0, 0, 0, 0, 0, 0, 0)); { CAR }
{$ENDIF}
  CAVT: real = 0.30;
  CDVT: real = 0.70;
  BeginCh = '@Begin Player ';
  EndCh = '@End';

  // Familles de tr�sors
  PtsFamilles: array [0 .. 15] of Integer = (0, // n=0
    0, // n=1
    0, // n=2
    0, // n=3
    0, // n=4
    100, // n=5
    150, // n=6
    250, // n=7
    400, // n=8
    600, // n=9
    850, // n=10
    1150, // n=11
    1500, // n=12
    1900, // n=13
    2350, // n=14
    2850); // n=15 : ne peut pas arriver pour l'instant

  PtsDecouverteMdExplo: array [1 .. 5] of UInt32 = (800, 500, 400, 300, 200);

  { Detail score }
  NBSCORE = 32;
  DS_POP = 1;
  DS_IND = 2;
  DS_CM = 3;
  DS_FLOTTES = 4;
  DS_MONDES = 5;
  DS_MONDESCONV = 6;
  DS_CONV = 7;
  DS_MONDESROB = 8;
  DS_TRESORS = 9;
  DS_TRESMAUDIT = 10;
  DS_PILLAGES = 11;
  DS_MARTYRS = 12;
  DS_POPTUEES = 13;
  DS_VAISSDETR = 14;
  DS_BOMBES = 15;
  DS_CHOSE = 16;
  DS_MPDECH = 17;
  DS_PCDECH = 18;
  DS_MALUSVAISSDETR = 19;
  DS_MALUSPOPTUEES = 20;
  DS_NOUVCONTACT = 21;
  DS_MONDELOCALISE = 22;
  DS_BONMDELOCA1ER = 23;
  DS_MONDEOBSERVE = 24;
  DS_BONMDEOBS1ER = 25;
  DS_MEVA = 26;
  DS_MDEPLOCALISE = 27;
  DS_MUSEES = 28;
  DS_FAMILLES = 29;
  DS_BONUS = 30;
  DS_DECONV = 31;
  DS_INDDETR = 32;

  ScoreNom: array [1 .. NBSCORE] of String = ('Populations', { 1 }
    'Industries', { 2 }
    'Capacit�s mini�res', { 3 }
    'Flottes', { 4 }
    'Mondes', { 5 }
    'Mondes convertis', { 6 }
    'Convertis', { 7 }
    'Mondes robotis�s', { 8 }
    'Tr�sors', { 9 }
    'Tr�sors maudits', { 10 }
    'Pillages', { 11 }
    'Martyrs', { 12 }
    'Populations tu�es', { 13 }
    'Vaisseaux d�truits', { 14 }
    'Largage de bombe', { 15 }
    'La Chose', { 16 }
    'MP D�charg�es', { 17 }
    'PC D�charg�s', { 18 }
    'Malus Vaisseaux d�truits', { 19 }
    'Malus Populations tu�es', { 20 }
    'Nouvelles rencontres', { 21 }
    'Mondes localis�s', { 22 }
    'Bonus 1er � localiser un monde', { 23 }
    'Mondes observ�s', { 24 }
    'Bonus 1er � observer un monde', { 25 }
    'Exploration', { 26 }
    'Observation de mondes de d�part', { 27 }
    'Mus�es', { 28 }
    'Familles de tr�sors', { 29 }
    '(Bonus potentiel de fin de partie)' { 30 } , 'D�conversion' { 31 } ,
    'Industries d�truites' { 32 } );

  // Type d'erreur pour la v�rification des ordres par nebutil
  LE_RIEN = -1;
  LE_FLOTTE_EXCLUSIF = -2;
  LE_MONDE_PRODUCTION = -3;

  { Statuts :
    TMonde.Statut : bit 0-2 : Nb de tour pill�
    bit 3-5 : Nb de fois pill�
    bit 6 : TN (0=non)
    bit 7 : BOMBE (0=non)
    TFlotte.Statut : bit 0 : En paix
    bit 7 : Transporte BOMBE
    TTresor.Statut : bit 0 : Sur une Flotte (1=oui)
    TMondeEvt.Statut : bit 0 : captur�
    1 : pill�
    2 : offert
    (pareil pour TFlotteEvt.Statut)
    TFlotteEvt.Statut : bit 0 : captur�e
    1 : pirat�e
    2 : offerte
    3 : a fait une embuscade
    4 : a d�charg� des PC
    5 : a explor� un monde
    ActionVx = 0 : rien
    1 : *C
    2 : *N
    3 : *F
    4 : **
    bit 6 : Capture;
    (tirs conditionnels : bit 7 arm�)
    TFlotteEvt.OrdreExcl = 1 : Tire sur une flotte
    2 : tire sur un monde
    3 : tire sur pop
    4 : tire sur I
    5 : attaque de robots
    6 : largue la BOMBE
    7 : d�placement
    8 : cadeau
  }

Type
  PrelationJou = ^TRelationJou;
  // TRelationJou = array[1..JouMax] of longint;
  TRelationJou = array [0 .. NB_JOU_MAX] of longint;
  PIntMondesJou = ^TIntMondesJou;
  TIntMondesJou = array [1 .. NB_MONDE_MAX, 0 .. NB_JOU_MAX] of integer;

  TMonde = Record
    Connect: Array [1 .. 8] of UInt16;
    Duree, ProprConv, PlusMP, Statut, PC: UInt8;
    Proprio, Ind: UInt16;
    Pop, Conv, Robot, MaxPop, MP, VI, VP: UInt16;
    X, Y: UInt8; { position sur le plan }
    NbExplo, NbExploCM: UInt8;
    NomIndex_obsolete: smallint;
    NbExploLoc: UInt8; // explo de Md
    NbExploPop: UInt8; // Explo aug pop
    bourrage: array [1 .. 16] of byte;
    Possession: array [1 .. NB_JOU_MAX] of integer;
    PremierProprio: integer;
    PotExplo: integer; // potentiel d'exploration
  end;

  { A fusionner avec TMonde ensuite }
  TMondeSup = Record
  end;

  TTousMonde = Array [1 .. NB_MONDE_MAX] of TMonde;

  TFlotte = Record
    Localisation: UInt16;
    NbVC, NbVT, MP, N, C, R: UInt16;
    Proprio, Statut: UInt8;
    bourrage: array [1 .. 20] of byte;
  end;

  PToutesFlotte = ^TToutesFlotte;
  TToutesFlotte = array [1 .. 1024] of TFlotte;

  TTresor = Record
    Proprio, Localisation, Statut: UInt16;
    bourrage: array [1 .. 20] of byte;
  end;

  PTousTresor = ^TTousTresor;
  TTousTresor = Array [1 .. NbTres] of TTresor;

  TMondeEvt = Record
    AncienProprio, Statut: UInt16;
    ActionVI, ActionVP, CibleVI, CibleVP, IndLibre: UInt16;
    PopLibre: UInt16;
    PopMoins, ConvMoins, RobMoins: UInt16;
    PC: UInt16;
  end;

  TTousMondeEvt = Array [1 .. 1024] of TMondeEvt;

  TFlotteEvt = Record
    AncienProprio, OrdrExcl, Cible, Statut, Victime: UInt16;
    Chemin: Array [1 .. MAX_CHEMIN] of Int16;
    VaisMoins, NbRob: UInt16;
    bourrage: array [1 .. 20] of byte;
  end;

  TJoueur = Record
    Nom: String;
    Pseudo: String;
    Classe, TourChgtJihad, Md: UInt16;
    Jihad: word;
    OldScore: UInt16;
    DEP, ATT, DEF, RAD, ALI, CAR: UInt64;
    DEPreste, ATTreste, DEFreste: UInt64;
    RADreste, ALIreste, CARreste: UInt64;
    Espions: array[1..NB_STATS] of UInt64;
    Stats: array [1..NB_JOU_MAX, 1..NB_STATS] of Integer;
    StatsClassementRang: array [1..NB_JOU_MAX, 1..NB_STATS] of Integer; // Place => Numjou
    StatsPremier: array [1..NB_STATS] of Integer;
    StatsDernier: array [1..NB_STATS] of Integer;
    StatsMoyenne: array [1..NB_STATS] of Integer;
    CA, CD: UInt16;
    PopTuees, NbMdesPilles, DifMdesPilles: UInt64;
    Score: longint;
    Coeq: byte;
    bourrage: array [1 .. 45] of byte;
    Email: TNBTStringList;
    Equipe: TNBTStringList;
  end;

  TLesJoueurs = Array [1 .. NB_JOU_MAX] of TJoueur;

  TOrdre = Record
    O: Array [1 .. 8] of UInt16;
    Ch: Array [1 .. 16] of String;
    Auteur: UInt16;
    Erreur, TypeO, STypeO: smallint;
    OrdreCh: string;
    DataCh: string; // Sert pour le nommage de monde
  end;

  PTousFlotteEvt = ^TTousFlotteEvt;
  TTousFlotteEvt = Array [1 .. 1024] of TFlotteEvt;

  TStat = array [1 .. NB_JOU_MAX, 1 .. 10] of UInt16;
  PStat = ^TStat;

  PAvuTab = ^TAvuTab;
  TAvuTab = Array [1 .. 1024] of longint;

  PEvt = ^TEvt;

  TEvt = Record
    ME: TTousMondeEvt;
    FE: TTousFlotteEvt;
    Connu: TAvuTab;
    Contact: TRelationJou;
    Stat: TStat;
    ScoresDecroissants : TStringDynArray; // Nebutil
  end;

  PLesJoueurs = ^TLesJoueurs;

  TDetailScore = array [1 .. NBSCORE] of Integer;
  PDetailScores = ^TDetailScores;
  TDetailScores = array [1 .. NB_JOU_MAX] of TDetailScore;

  PPartie = ^TPartie;

  TPartie = Record
    Version: String;
    TourFinal: UInt16;
    NbJou, NbMonde, NbFlotte: UInt16;
    Jou: TLesJoueurs;
    M: TTousMonde;
    F: TToutesFlotte;
    T: TTousTresor;
    ConNom, Allie, Chargeur: TRelationJou;
    TechClasse: TTechClasse;
    MaxTech: TMaxTech;
    AdresseServeurMail: String;
    PasswordPartie: String;
    NomArbitre: string;
    EmailArbitre: string;
    ChargeurPop, DechargeurPop, Pilleur: TRelationJou;
    NbRotCode: smallint; // Codage pour l'envoi des CR au serveur de mail
  end;

  PPartie2 = ^TPartie2;

  TPartie2 = Record
    AVuConnect, AVuCoord: TAvuTab;
    DetailScores: TDetailScores;
    StationMonde: TAvuTab; // Historique des stations
  end;

  TDir = Record
    X, Y: ShortInt;
  end;

  TConnectionTmp = Array [1 .. 8] of smallint;

  TMondeTmp = Record
    X, Y, NbConnect, ConnectPres: smallint;
    ConnectDir: Array [1 .. 8] of TDir;
    Connect: TConnectionTmp;
    TN, Md: smallint;
  end;

  PFichPlan = ^TFichPlan;

  TFichPlan = Record
    LongX, LongY: smallint;
    M: Array [1 .. 1024] of TMondeTmp;
    NbMonde: smallint;
  end;

  PTousMonde = ^TTousMonde;
  PTousMondeEvt = ^TTousMondeEvt;
  PToutesFlotteEvt = ^TToutesFlotteEvt;
  TToutesFlotteEvt = array [1 .. 1024] of TFlotteEvt;
  PTabFlottes = ^TTabFlottes;
  TTabFlottes = array [1 .. 1024] of TFlotte;
  PTabTresors = ^TTabTresors;
  TTabTresors = array [1 .. NbTres] of TTresor;
  PUniv = ^TUniv;
  TUniv = Array [1 .. 32, 1 .. 32] of smallint;
  PPlanTmp = ^TPlanTmp;
  TPlanTmp = Array [1 .. 1024] of TMondeTmp;

  PJoueurDeb = ^TJoueurDeb;

  TJoueurDeb = record
    Nom: String[30];
    Pseudo: String[30];
    Email: array [1 .. 2] of string[50];
    Classe: smallint;
    Score: smallint;
  end;

  PJouTab = ^TJouTab;
  TJouTab = array [1 .. NB_JOU_MAX, 1 .. 3] of TJoueurDeb;

  PChoixJoueurs = ^TChoixJoueurs;
  TChoixJoueurs = array [1 .. NB_JOU_MAX] of boolean;

  TParamRec = record
    NomPartie: string;
    TourCh: string;
    RepFic: string;
    Email: string;
  end;

  { Ajout du 28/12/95 : nouveau plan pour N�bula }
  TLesConnect = array [1 .. 32, 1 .. 32, 1 .. 8] of boolean;

  PLesMondesPlan = ^TLesMondesPlan;
  TLesMondesPlan = array [1 .. 32, 1 .. 32] of record
    No: smallint;
    Md, TN: boolean;
  end;

PInfoMap = ^TInfoMap;
TInfoMap = record
  Mondes: TLesMondesPlan;
  Connect: TLesConnect;
  LongX, LongY: smallint;
end;

InfoMonde = Array [0 .. 1024] of longint;
PInfoMonde = ^InfoMonde;

{ G�n�ration des donn�es pour les joueurs }
TDonneesMonde = record Connect: array [1 .. 8] of word;
// Num�ros des mondes connect�s

ConnectDir: byte; // Champ de bit.
                  // Bit 0 = en haut � gauche
                  // Bit 1 = en haut
                  // On continue en tournant dans le sens
                  // des aiguilles d'une montre...
                  // Bit 7 = � gauche.

X, Y: byte; // Coordonn�es du monde
  Proprio: byte;
  Industries: smallint;
  TN: boolean; // TN=True si le monde est un trou noir
  VI, VP: smallint;
end;

TDonneesFlotte = record Localisation: word;
  Proprio: smallint;
  VC, VT: smallint;
end;

TDonneesTresor = record Monde: smallint;
// Monde et Flotte : l'un des deux doit �tre nul
  Flotte: smallint;
  Proprio: byte;
end;

{ Gestion des fichiers de sauvegarde }
PEntete = ^TEntete;
TEntete = record
  Version: string;
  TourMax: smallint;
  TourLock: smallint;
  Depl: array [0 .. 50] of longint;
{ CoutTech : array[1..6, 1..6] of byte;
  CoutMax  : array[1..6, 1..6] of byte; }
end;

PLigne = ^TLigne;
TLigne = array [1 .. NB_JOU_MAX] of integer;
PTab = ^TTab;
TTab = array [1 .. 1024] of PLigne;

Const
  Tres1: Array [1 .. NBNOMTRES] of String = ('La statue', 'La couronne',
    'La monnaie', 'La croix', 'L''�p�e', 'La machine', 'L''arche', 'La potion',
    'L''armure', 'La bague', 'L''arme', 'La perle', 'La gemme');

  Tres2: Array [1 .. NBADJTRES] of String = ('ancienne', 'royale', 'd''or',
    'sacr�e', 'brillante', 'noire', 'perdue', 'maudite', 'magique', 'pr�cieuse',
    'l�gendaire', 'merveilleuse', 'ultime', 'galactique');

  TresSpc: Array [1 .. 18] of String = ('Le Mangeur de Points Gigantesque',
    'Le Tr�s Grand Mangeur de Points', 'Le Grand Mangeur de Points',
    'Le Petit Mangeur de Points', 'Le Diamant Gigantesque',
    'Le Tr�s Grand Diamant', 'Le Grand Diamant', 'Le Petit Diamant',
    '1001 Sorts Magiques, Tome I', '1001 Sorts Magiques, Tome II',
    '1001 Sorts Magiques, Tome III', '1001 Sorts Magiques, Tome IV',
    '1001 Sorts Magiques, Tome V', '1001 Sorts Magiques, Tome VI',
    '1001 Sorts Magiques, Tome VII', '1001 Sorts Magiques, Tome VIII',
    '1001 Sorts Magiques, Tome IX', 'La Chose');
  NomClasse: Array [1 .. NBCLASSE] of String = ('Empereur', 'Marchand',
    'Pirate', 'Antiquaire', 'Robotron', 'Missionnaire', 'Explorateur');

Function TresNom(N: integer): String;
Function Puis2(N: integer): longint;
Function RND(X: real): longint;
function CalcNoVersionNebutil(Version, beta, pl: integer): string;
function NoVersionNebutil: string;
procedure InitOrdre(var O: TOrdre);


IMPLEMENTATION

uses SysUtils, Math;

Function TresNom(N: integer): String;
begin
  Result := '';
  if N <= NBNOMTRES * NBADJTRES then // Tr�sor normal
  begin
    Result := Tres1[(N - 1) div NBADJTRES + 1];
    Result := Result + ' ';
    Result := Result + Tres2[(N - 1) mod NBADJTRES + 1];
  end
  else
  begin
    N := N - NBNOMTRES * NBADJTRES;
    Result := TresSpc[N]
  end;
end;

Function Puis2(N: integer): longint;
begin
  if N < 0 then
    Puis2 := 0
  else
    Puis2 := longint(1) SHL N;
end;

// TODO Supprimer : utiliser fonction Ceil
Function RND(X: real): longint;
begin
  Result := Ceil(X);
end;

function CalcNoVersionNebutil(Version, beta, pl: integer): string;
begin
  Result := Format('%d.%.2d', [Version div 100, Version mod 100]);
  // Result := IntToStr(version div 100) + '.' +
  // IntToStr(version mod 100);
  if beta > 0 then
    Result := Result + 'b' + IntToStr(beta);
  if pl > 0 then
    Result := Result + 'pl' + IntToStr(pl);
end;

function NoVersionNebutil: string;
begin
  Result := CalcNoVersionNebutil(VersionNebutil, VersionNebutilbeta,
    VersionNebutilpl)
end;

procedure InitOrdre(var O: TOrdre);
begin
  with O do
  begin
    FillChar(O, sizeof(O), 0);
    FillChar(Ch, sizeof(Ch), 0);
    Auteur := 0;
    Erreur := 0;
    TypeO := -1;
    STypeO := 0;
    OrdreCh := '';
    DataCh := '';
  end;
end;

end.
