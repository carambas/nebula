unit Eval.ActionChose;

Interface

uses Avateval, eval.etape;

type

  TEvalActionChose = class(TEvalEtape)
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = 16;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;
  end;

Implementation

uses SysUtils, Avatunit, Utils, Eval.APIPoints;

procedure TEvalActionChose.EvalueOrdre;
begin
  inherited;

end;

Procedure TEvalActionChose.Finalise;
var md,pr,alea : word;
    i,j : integer;
    St : string;
    TechCh : string;
    NoTresChose : integer;
    LogStr: String;
begin
  if Simul then
    Exit;

   LogStr := 'La Chose ';
   NoTresChose := 200;
   if Data.T[NoTresChose].Localisation <> 0 then
   with Data do
   begin
     pr:=T^[NoTresChose].Proprio;
     md:=T^[NoTresChose].Localisation;
     if T^[NoTresChose].Statut=1 then md:=F^[md].Localisation;
     alea := 0;
     if pr = 0 then
     begin
        alea := 9;
     end;
     if (Random(2) > 0) or (alea = 9) then
     begin
        if alea = 0 then alea:=Random(19)+1;
        case alea of
           1 : begin
                  i:=Random(21)-10;
                  if M^[md].Ind+i<0 then M^[md].Ind:=0
                  else M^[md].Ind := M^[md].Ind + i;
                  St := IntToStr(i);
                  LogStr := LogStr + 'a construit ' + St + ' industries';
               end;
           2 : begin
                  i:=Random(41)-20;
                  //if M^[md].MaxPop+i>255 then M^[md].MaxPop:=255
                  if M^[md].MaxPop+i<0 then M^[md].maxPop:=0
                  else M^[md].MaxPop := M^[md].MaxPop + i;
                  St := IntToStr(i);
                  LogStr := LogStr + 'a augment� de ' + St + ' la limite de population';
               end;
           3 : begin
                    i:=Random(41)-20;
                    //if M^[md].Pop+i>255 then M^[md].Pop:=255
                    if M^[md].Pop+i<0 then M^[md].Pop:=0
                    else if M^[md].Robot=0 then M^[md].Pop := M^[md].Pop + i
                    else
                    begin
                      M^[md].Robot := M^[md].Robot + i;
                      if M^[md].Robot+i<0 then M^[md].Robot:=0
                    end;
                    St := IntToStr(i);
                    LogStr := LogStr + 'a augment� de ' + St + ' la population';
                 end;
           4 : begin
                    i:=Random(11)-5;
                    if M^[md].PlusMP+i<0 then M^[md].PlusMP:=0
                    else M^[md].PlusMP := M^[md].PlusMP + i;
                    St := IntToStr(i);
                    LogStr := LogStr + 'a augment� de ' + St + ' la capacit� mini�re';
                 end;
           5 : begin
                  {
                  if Jou[pr].Classe <> Explorateur then
                  begin
                    for i:=1 to NbJou do if ConnuDeNom[pr,i] then
                       DevientConnu(jou[i].Md,pr);
                    EcritLog('a montr� les mondes de d�part de ses voisins');
                  end
                  else
                    EcritLog('a annul� son action car le proprio est explorateur');
                  }
                  LogStr := LogStr + 'a montr� les mondes de d�part de ses voisins (fonction annul�e)';
               end;

           6 : begin
                  if Jou[pr].Classe <> Explorateur then
                  begin
                    for i:=1 to Random(10)+1 do
                       DevientConnu(Random(NbMonde)+1,pr);
                    LogStr := LogStr + 'a montr� quelques mondes au hasard dans la galaxie';
                  end
                  else
                    LogStr := LogStr + 'a annul� son action car le proprio est explorateur';
               end;
           7 : begin
                  i := Random(2000) - 1000;
                  Points.PtsChose(pr, i);
                  St := IntToStr(i);
                  LogStr := LogStr + 'a augment� le score de ' + St + 'points';
               end;
           8 : begin
                  {
                  for i:=1 to NbJou do
                    if ConnuDeNom[i,pr] and (Jou[i].Classe <> Explorateur) then
                      DevientConnu(md,i);
                  EcritLog('a montr� son Md a ses voisins (sauf les explos)');
                  }
                  LogStr := LogStr + 'a montr� son Md a ses voisins (fonction annul�e)';
               end;
           9..12 : begin
                  T^[NoTresChose].Statut:=0;
                  Repeat
                     i:=Random(NbMonde)+1;
                  Until (M^[i].Statut and Puis2(6))=0;
                  T^[NoTresChose].Localisation:=i;
                  T^[NoTresChose].Proprio:=M^[i].Proprio;
                  St := IntToStr(i);
                  LogStr := LogStr + 's''est t�l�port�e au M_' + St;
               end;
           13 : begin
                   {for i:=1 to NbFlotte do if F^[i].Localisation=Jou[pr].Md then
                   begin
                      j:=0;
                      Repeat
                         inc(j);
                      Until FE^[i].Chemin[j]=0;
                      FE^[i].Chemin[j]:=Jou[pr].Md;
                      Repeat
                         F^[i].Localisation:=Random(NbMonde)+1;
                      Until (M^[F^[i].Localisation].Statut and Puis2(6))=0;
                      DevientConnu(F^[i].Localisation,F^[i].Proprio);
                   end;}
                   LogStr := LogStr + 'n''a pas vir� les flottes du monde de d�part (fonction annul�e)';
                end;
           14 : begin
                   i:=Random(21)-10;
                   if M^[md].VI+i<0 then M^[md].VI:=0
                   else M^[md].VI := M^[md].VI + i;
                   St := IntToStr(i);
                   LogStr := LogStr + 'a ajout� ' + St + ' VI';
                end;
           15 : begin
                   i:=Random(21)-10;
                   if M^[md].VP+i<0 then M^[md].VP:=0
                   else M^[md].VP := M^[md].VP + i;
                   St := IntToStr(i);
                   LogStr := LogStr + 'a ajout� ' + St + ' VP';
                end;
           16 : begin
                   {
                   M^[md].Statut:=Puis2(6);
                   M^[md].Proprio:=0;
                   M^[md].ProprConv:=0;
                   for i:=1 to NbFlotte do if F^[i].Localisation=md then
                   begin
                      j:=0;
                      Repeat
                         inc(j)
                      Until FE^[i].Chemin[j]=0;
                      FE^[i].Chemin[j]:=md;
                      Repeat
                         F^[i].Localisation:=Random(NbMonde)+1;
                      Until (M^[F^[i].Localisation].Statut and Puis2(6))=0;
                      F^[i].NbVC:=RND(F^[i].NbVC*(Random(11))/10);
                      F^[i].NbVT:=RND(F^[i].NbVT*(Random(11))/10);
                      DevientConnu(F^[i].Localisation,F^[i].Proprio);
                   end;
                   for i:=1 to NbTres do if ((T^[i].Localisation=md)
                      and(T^[i].Statut=0)) then
                   begin
                      Repeat
                         T^[i].Localisation:=Random(NbMonde)+1;
                      Until (M^[T^[i].Localisation].Statut and Puis2(6))=0;
                      T^[i].Statut:=0;
                      T^[i].Proprio:=M^[T^[i].Localisation].Proprio;
                   end;
                   EcritLog('a transform� le monde en trou noir');
                   }
                   LogStr := LogStr + 'a transform� le monde en trou noir (fonction annul�e)';
                end;
           17..18 :
                begin
                   i := Random(6) + 1;
                   j := Random(4) - 2;
                   if j >= 0 then
                     j := j + 1;
                   case i of
                     1 :
                       begin
                         TechCh := 'DEP';
                         Jou[pr].DEP := Jou[pr].DEP + j;
                         if (Jou[pr].DEP > MaxTech[1, Jou[pr].Classe]) and
                           (MaxTech[1, Jou[pr].Classe] > 0) then
                           Jou[pr].DEP := MaxTech[1, Jou[pr].Classe];
                       end;
                     2 :
                       begin
                         TechCh := 'ATT';
                         Jou[pr].ATT := Jou[pr].ATT + j;
                         if (Jou[pr].ATT > MaxTech[2, Jou[pr].Classe]) and
                           (MaxTech[2, Jou[pr].Classe] > 0) then
                           Jou[pr].ATT := MaxTech[2, Jou[pr].Classe];
                         if (Jou[pr].ATT < 1) then
                            Jou[pr].ATT := 1;
                         Jou[pr].CA := CalculeCA(Jou[pr].ATT);
                       end;
                     3 :
                       begin
                         TechCh := 'DEF';
                         Jou[pr].DEF := Jou[pr].DEF + j;
                         if (Jou[pr].DEF > MaxTech[3, Jou[pr].Classe]) and
                           (MaxTech[3, Jou[pr].Classe] > 0) then
                           Jou[pr].DEF := MaxTech[3, Jou[pr].Classe];
                         if (Jou[pr].DEF < 1) then
                            Jou[pr].DEF := 1;
                         Jou[pr].CD := CalculeCD(Jou[pr].DEF);
                       end;
                     4 :
                       begin
                         TechCh := 'RAD';
                         Jou[pr].RAD := Jou[pr].RAD + j;
                         if (Jou[pr].RAD > MaxTech[4, Jou[pr].Classe]) and
                           (MaxTech[4, Jou[pr].Classe] > 0) then
                           Jou[pr].RAD := MaxTech[4, Jou[pr].Classe];
                       end;
                     5 :
                       begin
                         TechCh := 'ALI';
                         Jou[pr].ALI := Jou[pr].ALI + j;
                         if (Jou[pr].ALI > MaxTech[5, Jou[pr].Classe]) and
                           (MaxTech[5, Jou[pr].Classe] > 0) then
                           Jou[pr].ALI := MaxTech[5, Jou[pr].Classe];
                       end;
                     6 :
                       begin
                         TechCh := 'CAR';
                         Jou[pr].CAR := Jou[pr].CAR + j;
                         if (Jou[pr].CAR > MaxTech[6, Jou[pr].Classe]) and
                           (MaxTech[6, Jou[pr].Classe] > 0) then
                           Jou[pr].CAR := MaxTech[6, Jou[pr].Classe];
                       end;
                   end;
                   St := IntToStr(j);
                   LogStr := LogStr + 'a augment� de ' + St +' le ' + TechCh;
                end;
           19 :
                begin
                   //for i := 1 to 10 do
                   //  Stat[pr, i] := 2;
                   //EcritLog('a mis toutes les stat au niveau 2');
                   LogStr := LogStr + 'n''a rien fait';
                end;
        end;
     end
     else LogStr := LogStr + 'n''a rien fait';
   end
   else // Data.T[NoTresChose].Localisation = 0
   begin
     LogStr := LogStr + 'n''est pas sur la carte';
   end;
   EcritLog(LogStr);
end;

function TEvalActionChose.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalActionChose.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalActionChose.Init;
begin
  inherited;

end;

end.
