unit Eval.ConstructionInd;

Interface

uses Avateval, eval.etape;

type
  TEvalContructionIndustrie = class(TEvalEtape)
  public const
    TYPE_ORDRE = 6;
    ETAPE_EVAL = 6;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure ConstrNouvInd(md, x: word);
    procedure RobConstInd(md, x: word);
  end;

Implementation

uses Avatunit, erreurseval;

procedure TEvalContructionIndustrie.ConstrNouvInd(md,x : word);
Var n : word;
begin
  with Data do
  begin
    n:=AvatUnit.ConstrInd[jou[O.Auteur].Classe];
    if M^[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
    else if ME^[md].IndLibre<n*x then
    begin
       Erreur(ERR_PAS_ASSEZ_I);
       x:=ME^[md].IndLibre div n;
       OK:=True;
    end;
    if OK then
    begin
       Inc(M^[md].Ind,x);
       Dec(ME^[md].IndLibre,n*x);
       Dec(ME^[md].PopLibre,n*x);
       dec(M^[md].MP,n*x);
    end;
  end;
end;

procedure TEvalContructionIndustrie.RobConstInd(md,x : word);
begin
  with data do
  begin
    if Data.Jou[O.Auteur].Classe <> Robotron then Erreur(ERR_MAUVAISE_CLASSE)
    else if M^[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
    else if M^[md].Robot < x then
    begin
      Erreur(ERR_PAS_ASSEZ_ROB);
      x := (M^[md].Robot div 12) * 12;
      OK := true;
    end
    else if ((x div 12) * 12 <> x) then
    begin
       Erreur(ERR_NB_ROB_PAS_MULT12);
       x := (x div 12) * 12;
       OK:=true;
    end;
    if OK then
    begin
       Inc(M^[md].Ind,x div 12);
       Dec(M^[md].Robot,x);
       if M^[md].Robot = 0 then
         M^[md].Proprio := 0;
    end;
  end;
end;

Procedure TEvalContructionIndustrie.EvalueOrdre;
begin
  OK:=True;
  case O.StypeO of
    0 : ConstrNouvInd(O.O[1],O.O[2]);
    1 : RobConstInd(O.O[1], O.O[2]);
  end;
end;

procedure TEvalContructionIndustrie.Finalise;
begin
  inherited;

end;

function TEvalContructionIndustrie.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalContructionIndustrie.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalContructionIndustrie.Init;
begin
  inherited;

end;

end.
