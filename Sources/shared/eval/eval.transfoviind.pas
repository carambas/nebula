unit Eval.TransfoVIInd;

Interface

uses Avateval, eval.etape;

type
  TEvalTransfoVIInd = class(TEvalEtape)
  public
    const TYPE_ORDRE = 7;
    const ETAPE_EVAL = 7;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure TVaisNouvInd(md, x: word);
  end;

Implementation

uses Avatunit;

procedure TEvalTransfoVIInd.Finalise;
begin
  inherited;

end;

function TEvalTransfoVIInd.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalTransfoVIInd.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalTransfoVIInd.Init;
begin
  inherited;

end;

procedure TEvalTransfoVIInd.TVaisNouvInd(md,x : word);
const ConsIndVI : array[1..NBCLASSE] of word = (4,6,6,6,6,5,6);
var n : word;
begin
  with data do
  begin
    n:=ConsIndVI[jou[O.Auteur].Classe];
    if O.Auteur<>M^[md].Proprio then Erreur(4)
    else if M^[md].VI<n*x then
    begin
       Erreur(7);
       x:=M^[md].VI div n;
       OK:=True;
    end;
    if OK then
    begin
       Inc(M^[md].Ind,x);
       Dec(M^[md].VI,n*x);
    end;
  end;
end;

Procedure TEvalTransfoVIInd.EvalueOrdre;
begin
  OK:=True;
  TVaisNouvInd(O.O[1],O.O[2]);
end;

end.
