unit Eval.Pillage;

Interface

uses
  Avateval, eval.etape;

type
  TEvalPillage = class(TEvalEtape)
  public const
    TYPE_ORDRE = 18;
    ETAPE_EVAL = 18;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure NommeMonde(Md, NomIndex: integer);
    procedure PilleMonde(Md: integer);
  end;

Implementation

uses Avatunit, erreurseval, Eval.APIPoints;

  procedure TEvalPillage.PilleMonde(Md : integer);
  var pr : word;
  begin
    with data do
    begin
      pr:=M^[md].Proprio;
      OK := True;
      if PillagesInterdits then Erreur(ERR_ORDRE_INTERDIT)
      else if (O.Auteur <> pr) then
      begin
        if (ME[md].AncienProprio <> O.Auteur) or
           not EstPilleur(pr, O.Auteur) then
          Erreur(ERR_PAS_PROPRIO_MONDE)
      end;
      if OK then
      begin
        if (ME^[md].Statut and Puis2(1))>0 then Erreur(23)
        else if M^[md].Statut mod 8>1 then Erreur(24)
        else
        begin
           Pille[md] := True;
           if Pi[md] = 0 then
             inc(Jou[O.Auteur].DifMdesPilles); // Ca sert encore ces machins ?
           inc(Jou[O.Auteur].NbMdesPilles); // Ca sert encore ces machins ?
           Pi[md] := Pi[md] + 1;
           RecupPi[md] := 5;
           Points.PtsPillages(O.Auteur, md, Pi[md]);
        end;
      end;
    end;
  end;


{Nommage des mondes, modif du 08/11/97}
procedure TEvalPillage.Finalise;
begin
  inherited;

end;

function TEvalPillage.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalPillage.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalPillage.Init;
begin
  inherited;

end;

procedure TEvalPillage.NommeMonde(Md, NomIndex : integer);
var
  pr : integer;
begin
  pr:=Data.M^[md].Proprio;
  if O.Auteur <> pr then
    Erreur(ERR_PAS_PROPRIO_MONDE)
  else
    Data.NomMonde[md] := Copy(O.DataCh, 1, 70);
  if Length(O.DataCh) > 70 then
    Erreur(ERR_NOM_TROP_LONG);
end;

Procedure TEvalPillage.EvalueOrdre;
begin
  case O.STypeO of
    0 : PilleMonde(O.O[1]); // Pillage
    1 : NommeMonde(O.O[1], O.O[2]); // Nommage d'un monde
  end;
end;

end.
