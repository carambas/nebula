unit Eval.DechargePC;

Interface

uses Avateval, Avatunit, eval.etape;

type
  //TPCDech = array[1..NB_MONDE_MAX, 1..NB_JOU_MAX] of integer;

  TEvalDechargePC = class(TEvalEtape)
  public const
    TYPE_ORDRE = 9;
    ETAPE_EVAL = 9;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    PCDech : array[1..NB_MONDE_MAX, 1..NB_JOU_MAX] of integer;
    procedure DechLesPC(fl, x: word);
  end;

Implementation

uses Eval.APIPoints;

procedure TEvalDechargePC.DechLesPC(fl : word;x : word);
var md : word;
begin
  with data do
  begin
    md:=F^[fl].Localisation;
    if O.Auteur<>F^[fl].Proprio then Erreur(12)
    else if F^[fl].MP<x then
    begin
       Erreur(14);
       x:=F^[fl].MP;
       if x>0 then OK:=True;
    end;
    if x=0 then OK:=False;
    if OK then
    begin
       inc(PCDech[md, F^[fl].Proprio], x);
       Inc(ME^[md].PC);
       Dec(F^[fl].MP,x);
       FE^[fl].Statut:=(FE^[fl].Statut or Puis2(4));
    end;
  end;
end;

procedure TEvalDechargePC.Finalise;
var
  md, j : word;
  totPC : integer;
  x : double;
  PC : integer;
begin
  with Data do for md:=1 to NbMonde do
  begin
    totPC := 0;
    for j := 1 to NbJou do if PCDech[md, j] > 0 then
      inc(totPC, PCDech[md, j]);
    if totPC > 0 then Inc(M^[md].PC);

    while (totPC > 0) and (M[md].Conv > 0) do
    begin
      // On d�termine � qui est le PC en question
      x := random;
      PC := 0;
      for j := 1 to NbJou do
      begin
        inc(PC, PCDech[md, j]);
        if PC >= x * totPC then
          break; // j correspond au joueur qui a largu� le PC en question
      end;
      Dec(totPC);  // Le PC disparait
      Dec(PCDech[md, j]);
      if Random(2) = 0 then // le PC a d�converti une population
      begin
        Dec(M^[md].Conv);
        Points.PtsDeconv(j, 1);
      end
      else
        Points.PtsPCDech(j, 1);
    end;

    // On donne les points pour les PC restant
    if totPC > 0 then
      for j := 1 to NbJou do
        if PCDech[md, j] > 0 then
          Points.PtsPCDech(j, PCDech[md, j]);
  end;
end;

function TEvalDechargePC.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalDechargePC.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalDechargePC.Init;
begin
  inherited;
  FillChar(PCDech, sizeof(PCDech), 0);
end;

Procedure TEvalDechargePC.EvalueOrdre;
var
  n : word;
begin
  OK:=True;
  n:=O.O[2];
  if n = 0 then
    n:=Data.F^[O.O[1]].MP;
  DechLesPC(O.O[1], n);
end;


end.
