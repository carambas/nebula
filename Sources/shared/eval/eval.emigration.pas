unit Eval.Emigration;

INTERFACE

uses
  AvatUnit,
  AvatEval,
  Eval.Etape,
  Eval.Conversion;

type
  TEvalEmigration = class(TEvalEtapeConflitsConversion)
  public const
    TYPE_ORDRE = 5;
    ETAPE_EVAL = 5;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure MiseAuPtTresors;
    procedure ResolConflitPop;
    function DetermineQuantite(md1, nbEmig: Word): Word;
    procedure EffectueMigrationViaInd(MdOrigine, MdDest, nbEmig: Word);
    procedure EffectueMigrationViaDechargement;

  private
    NouvN, NouvC, NouvR: array [0 .. NB_JOU_MAX, 1 .. NB_MONDE_MAX] of word;
    Emig: array [1 .. NB_MONDE_MAX] of boolean;
  end;

IMPLEMENTATION

uses
  SysUtils,
  Utils,
  Eval.APIPoints,
  erreurseval,
  ordreslist;


// En fonction du nombre de populations sur le monde d'origine
// On ajuste si n�cessaire le nombre de populations qui seront �migr�es
function TEvalEmigration.DetermineQuantite(md1, nbEmig: Word): Word;
begin
  with Data do
  begin
    // Populations
    if ((O.STypeO = 1) and (M[md1].Pop < nbEmig)) then
    begin
      Erreur(ERR_PAS_ASSEZ_POP);
      nbEmig := M[md1].Pop;
    end

    // Convertis
    else if ((O.STypeO = 2) and (M[md1].Conv < nbEmig)) then
    begin
      Erreur(ERR_PAS_ASSEZ_POP);
      nbEmig := M[md1].Conv;
    end

    // Non convertis
    else if ((O.STypeO = 3) and (M[md1].Pop - M[md1].Conv < nbEmig)) then
    begin
      Erreur(ERR_PAS_ASSEZ_POP);
      nbEmig := M[md1].Pop - M[md1].Conv;
    end

    // Robots
    else if ((O.STypeO = 4) and (M[md1].Robot < nbEmig)) then
    begin
      Erreur(ERR_PAS_ASSEZ_POP);
      nbEmig := M[md1].Robot;
    end;

    if ME[md1].IndLibre < nbEmig then
    begin
      Erreur(ERR_PAS_ASSEZ_I);
      nbEmig := ME[md1].IndLibre;
    end;
  end;

  Result := nbEmig;
end;

procedure TEvalEmigration.EffectueMigrationViaInd(MdOrigine, MdDest, nbEmig: Word);
var
  x1, x2: word;
  i: Integer;
begin
  with Data do
  begin
    Dec(M[MdOrigine].MP, nbEmig);
    Dec(ME[MdOrigine].IndLibre, nbEmig);
    Dec(ME[MdOrigine].PopLibre, nbEmig);
    DevientConnu(MdDest, O.Auteur);
    Emig[MdDest] := True;

    if O.STypeO = 2 then
    begin
      Dec(M[MdOrigine].Conv, nbEmig);
      Dec(M[MdOrigine].Pop, nbEmig);
      Inc(NouvC[M[MdOrigine].ProprConv, MdDest], nbEmig);
    end

    else if O.STypeO = 3 then
    begin
      Dec(M[MdOrigine].Pop, nbEmig);
      Inc(NouvN[O.Auteur, MdDest], nbEmig);
    end

    else if O.STypeO = 4 then
    begin
      Dec(M[MdOrigine].Robot, nbEmig);
      Inc(NouvR[O.Auteur, MdDest], nbEmig);
    end

    else if O.STypeO = 1 then
    begin
      x2 := (M[MdOrigine].Conv * nbEmig) div M[MdOrigine].Pop;
      x1 := nbEmig - x2;
      Dec(M[MdOrigine].Pop, nbEmig);
      Dec(M[MdOrigine].Conv, x2);
      Inc(NouvN[O.Auteur, MdDest], x1);
      Inc(NouvC[M[MdOrigine].ProprConv, MdDest], x2);
    end;

  if (M[MdDest].Statut and Puis2(6)) > 0 then
    for i := 1 to Data.NbJou do
    begin
      NouvN[i, MdDest] := 0;
      NouvC[i, MdDest] := 0;
      NouvR[i, MdDest] := 0;
    end;
  end;
end;

procedure TEvalEmigration.EffectueMigrationViaDechargement;
var
  fl, loc, qte, Proprietaire, x1, x2: Word;
  Poptot: Integer;
  j: Integer;
begin
  with Data do
  begin
    fl := O.O[1];
    loc := F[fl].Localisation;
    qte := O.O[2];
    Proprietaire := F[fl].Proprio;

    if Proprietaire <> O.Auteur then
      Erreur(ERR_PAS_PROPRIO_FLOTTE)

    else
      if (Proprietaire <> M[loc].Proprio) and
        (not EstDechargeurDePop(M[loc].Proprio, Proprietaire)) and
        ((M[loc].Pop > 0) or (M[loc].Robot > 0)) then
      Erreur(ERR_NON_DECHARGEUR_POP);

    if OK then
    begin
      Emig[loc] := True;
      if qte = 0 then
        case O.STypeO of
          5:
            qte := F[fl].N;
          6:
            qte := F[fl].C;
          7:
            qte := F[fl].R;
          8:
            qte := F[fl].N + F[fl].C;
        end;
      case O.STypeO of
        { D�chargement de non convertis }
        5:
          begin
            if qte > F[fl].N then
            begin
              Erreur(ERR_FLOTTE_PAS_ASSEZ_CONV);
              qte := F[fl].N;
            end;

            { On v�rifie que la population ne va pas d�passer la limite du monde }
            Poptot := M[loc].Pop;
            for j := 1 to Data.NbJou do
              Inc(Poptot, NouvN[j, loc] + NouvC[j, loc]);
            if qte + Poptot > M[loc].MaxPop + 10 then
            begin
              if Poptot >= M[loc].MaxPop + 10 then
                qte := 0
              else
                qte := M[loc].MaxPop + 10 - Poptot;
              Erreur(ERR_MONDE_SURPEUPLE);
            end;

            if qte > 0 then
            begin
              Inc(NouvN[Proprietaire, loc], qte);
              Dec(F[fl].N, qte);
              OK := True;
            end;
          end;
        { D�chargement de convertis }
        6:
          begin
            if qte > F[fl].C then
            begin
              Erreur(ERR_FLOTTE_PAS_ASSEZ_CONV);
              qte := F[fl].C;
            end;

            { On v�rifie que la population ne va pas d�passer la limite du monde }
            Poptot := M[loc].Pop;
            for j := 1 to Data.NbJou do
              Inc(Poptot, NouvN[j, loc] + NouvC[j, loc]);
            if qte + Poptot > M[loc].MaxPop + 10 then
            begin
              if Poptot >= M[loc].MaxPop + 10 then
                qte := 0
              else
                qte := M[loc].MaxPop + 10 - Poptot;
              Erreur(ERR_MONDE_SURPEUPLE);
            end;

            if qte > 0 then
            begin
              Inc(NouvC[Proprietaire, loc], qte);
              Dec(F[fl].C, qte);
              OK := True;
            end;
          end;
        { D�chargement de robots }
        7:
          begin
            if qte > F[fl].R then
            begin
              Erreur(ERR_FLOTTE_PAS_ASSEZ_ROB);
              qte := F[fl].R;
              OK := True;
            end;
            Inc(NouvR[Proprietaire, loc], qte);
            Dec(F[fl].R, qte);
            OK := True;
          end;
        { D�chargement de populations }
        8:
          begin
            if qte > F[fl].N + F[fl].C then
            begin
              Erreur(ERR_FLOTTE_PAS_ASSEZ_CONV);
              qte := F[fl].N + F[fl].C;
            end;

            { On v�rifie que la population ne va pas d�passer la limite du monde }
            Poptot := M[loc].Pop;
            for j := 1 to Data.NbJou do
              Inc(Poptot, NouvN[j, loc] + NouvC[j, loc]);

            if qte + Poptot > M[loc].MaxPop + 10 then
            begin
              if Poptot >= M[loc].MaxPop + 10 then
                qte := 0
              else
                qte := M[loc].MaxPop + 10 - Poptot;
              Erreur(ERR_MONDE_SURPEUPLE);
            end;

            if qte > 0 then
            begin
              x2 := (F[fl].C * qte) div (F[fl].N + F[fl].C);
              x1 := qte - x2;
              Inc(NouvC[Proprietaire, loc], x2);
              Inc(NouvN[Proprietaire, loc], x1);
              Dec(F[fl].C, x2);
              Dec(F[fl].N, x1);
              OK := True;
            end;
          end;
      end;
    end;
  end;
end;


procedure TEvalEmigration.EvalueOrdre;
var
  md1, md2, x: word;
begin
  inherited;

  with Data do
  begin
    if O.STypeO < 5 then
    begin
      md1 := O.O[1];
      md2 := O.O[3];
      x := O.O[2];

      // Test de propri�t�
      // A garder en premier pour �viter des messages d'erreur
      // donnant des infos sur un monde non vu
      if O.Auteur <> M[md1].Proprio then
      begin
        Erreur(ERR_PAS_PROPRIO_MONDE);
        x := 0; // On ne pousse pas le test plus loin
      end
      else if not Connect(md1, md2) then
      begin
        Erreur(ERR_MONDES_PAS_CONNECTES);
        x := 0; // On ne pousse pas le test plus loin
      end
      else
      begin
        x := DetermineQuantite(md1, x);
      end;
      if x > 0 then
      begin
        EffectueMigrationViaInd(md1, md2, x);
      end;

    end
    else
    begin
      // dechargement de Populations
      EffectueMigrationViaDechargement;
    end;
  end;

end;

procedure TEvalEmigration.Finalise;
begin
  inherited;

  ResolConflitPop;
  MiseAuPtTresors;
end;

function TEvalEmigration.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalEmigration.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalEmigration.MiseAuPtTresors;
var
  tresor: Integer;
begin
  with Data do
    for tresor := 1 to NbTres do
      if T[tresor].Localisation > 0 then
      begin
        if T[tresor].Statut = 0 then
          T[tresor].Proprio := M[T[tresor].Localisation].Proprio
        else
          T[tresor].Proprio := F[T[tresor].Localisation].Proprio;
      end;
end;

procedure TEvalEmigration.ResolConflitPop;
var
  TC, TP, TR, TPE: word;
  monde, NP: Integer;
  alea, x: real;
  proprC, tres, i: word;
  propr: UInt16;
  R, P, C: TRelationJou;
  // Nb de pops �migr�es par celui qui en �migre le plus
  maxP: Integer;
  // Celui qui �migre le plus de pops
  maxPj: Integer;
  // Nb de joueurs ayant �migr� le max de pops (> 1 en cas d'ex-aequo)
  nbJmax: Integer;
  maxR, gagnant, nbMax, RM: Integer;
  tues: Integer;
  pertePop: Integer;
begin
  with Data do
    for monde := 1 to NbMonde do
      if Emig[monde] then
      begin
        propr := M[monde].Proprio;
        proprC := 0;
        TP := M[monde].Pop;
        TR := M[monde].Robot;
        TC := M[monde].Conv;
        TPE := 0;
        FillChar(R, sizeof(R), 0);
        FillChar(P, sizeof(P), 0);
        FillChar(C, sizeof(C), 0);
        maxP := 0;
        maxPj := 0;
        nbJmax := 0;
        for i := 1 to Data.NbJou do
        begin
          R[i] := NouvR[i, monde];
          P[i] := NouvN[i, monde] + NouvC[i, monde];
          C[i] := NouvC[i, monde];
          if (P[i] > 0) and (P[i] > maxP) then
          begin
            maxP := P[i];
            maxPj := i;
            nbJmax := 1;
          end
          else if (P[i] > 0) and (P[i] = maxP) then
          begin
            Inc(nbJmax);
          end;
        end;
        Inc(R[M[monde].Proprio], M[monde].Robot);
        Inc(P[M[monde].Proprio], M[monde].Pop);
        Inc(C[M[monde].ProprConv], M[monde].Conv);
        for i := 1 to Data.NbJou do
        begin
          Inc(TP, NouvN[i, monde] + NouvC[i, monde]);
          Inc(TR, NouvR[i, monde]);
          Inc(TC, NouvC[i, monde]);
          Inc(TPE, NouvN[i, monde] + NouvC[i, monde]);
        end;
        if (TR > RND(TP / 4)) then
        begin
          // Seul le plus fort l'emporte
          // ex-aequo = match nul
          maxR := 0;
          gagnant := 0;
          nbMax := 0;

          for i := 0 to Data.NbJou do
          begin
            if R[i] > maxR then
            begin
              maxR := R[i];
              gagnant := i;
            end;
          end;

          // On v�rifie s'il n'y aurait pas d'ex-aequo

          for i := 0 to Data.NbJou do
            if maxR = R[i] then
              Inc(nbMax);

          if nbMax > 1 then
          begin
            propr := 0;
            M[monde].Robot := 0;
            M[monde].Pop := 0;
            M[monde].Conv := 0;
            Inc(ME[monde].PopMoins, TP);
            Inc(ME[monde].ConvMoins, TC);
            Inc(ME[monde].RobMoins, TR);
            ME[monde].PopLibre := 0;
          end
          else
          begin
            propr := gagnant;
            M[monde].Pop := 0;
            M[monde].Conv := 0;
            Inc(ME[monde].PopMoins, TP);
            Inc(ME[monde].ConvMoins, TC);
            // ProprC:=0;
            if M[monde].Robot = 0 then
              ME[monde].PopLibre := 0;

            tues := R[gagnant] - RND(TP / 4) - M[monde].Robot;
            if tues >= ME[monde].PopLibre then
              ME[monde].PopLibre := 0
            else
              ME[monde].PopLibre := ME[monde].PopLibre + tues;

            RM := RND(TP / 4); { RM=Robots Morts }
            // On enl�ve � tout le monde les pertes du�s au conflit avec les pops
            for i := 0 to Data.NbJou do
              Dec(R[i], Round2(R[i] * RM / TR));

            // Et maintenant le gagnant perd un robot par robot ennemi restant
            if R[gagnant] > (TR - RM - R[gagnant]) then
              M[monde].Robot := 2 * R[gagnant] + RM - TR
            else
              // mais en garde au minimum 1 :-)
              M[monde].Robot := 1;

            Inc(ME[monde].RobMoins, TR - M[monde].Robot);
          end;

        end
        else if 4 * TR < TP then
        begin
          if (M[monde].Proprio > 0) and (M[monde].Robot = 0) then
            propr := M[monde].Proprio
          else
          begin
            if not EmigrationSansHasard then
            begin
              // Ici on tire au pif le nouveau proprio en cas de victoire des pops
              x := 0;
              alea := Random;
              i := 0;
              if TPE > 0 then
              begin
                Repeat
                  Inc(i);
                  { modif le 10/02/94 : P[i] au lieu de P[i]+C[i] }
                  x := x + P[i] / TPE;
                Until alea < x;
                propr := i;
              end
              else
                propr := 0;
            end
            else
            begin
              // Ici plus de hasard
              if nbJmax = 1 then
                propr := maxPj
              else
                propr := 0;
            end;

          end;
          if TC > 0 then // Des convertis. Conflit �ventuel � r�soudre
          begin
            ResolConflitsConversion(C, monde);
            // Attention, TC a peut-�tre chang� de valeur apr�s cet appel
            TC := M[monde].Conv;
            proprC := M[monde].ProprConv;

            if M[monde].Pop > 0 then
              if (M[monde].Pop = M[monde].Conv) and (M[monde].Proprio = 0) then
                propr := proprC;
            // M[monde].ProprConv:=proprC;
          end;

          // Si les populations l'emportent...
          if TP - TC + C[proprC] - 4 * TR > 0 then
          begin
            pertePop := TP - TC + C[proprC] - 4 * TR - M[monde].Pop;
            Data.ME[monde].PopLibre := Data.ME[monde].PopLibre + pertePop;
            M[monde].Pop := TP - TC + C[proprC] - 4 * TR;
            // Manque : d�terminer le nombre de convertis...
          end
          else
          // Ici ce sont les robots qui ont gagn�
          begin
            ME[monde].PopLibre := 0;
            M[monde].Pop := 0;
          end;
          if M[monde].Pop = 0 then
          begin
            propr := 0;
            // ProprC:=0;
            M[monde].Conv := 0;
            M[monde].Robot := 0;
          end
          else
          begin
            M[monde].Conv := C[proprC];
            M[monde].Robot := 0;
          end;
          ME[monde].PopMoins := TP - M[monde].Pop;
          ME[monde].ConvMoins := TC - M[monde].Conv;
          ME[monde].RobMoins := TR - M[monde].Robot;
        end
        else
        begin
          M[monde].Proprio := 0;
          M[monde].ProprConv := 0;
          M[monde].Pop := 0;
          M[monde].Conv := 0;
          M[monde].Robot := 0;
          ME[monde].PopMoins := TP;
          ME[monde].ConvMoins := TC;
          ME[monde].RobMoins := TR;
        end;
        if (propr <> M[monde].Proprio) and (M[monde].Robot + M[monde].Pop > 0)
        then
        begin
          ME[monde].AncienProprio := M[monde].Proprio;
          M[monde].Proprio := propr;
          ME[monde].Statut := (ME[monde].Statut or Puis2(0));
        end;
        NP := M[monde].Pop;
        if TR > 0 then
          for i := 1 to Data.NbJou do
            if R[i] > 0 then
            begin
              Points.PtsPopTuees(i, M[monde].Proprio,
                ((TP - NP) * R[i]) div TR, monde);
              Inc(jou^[i].PopTuees, ((TP - NP) * R[i]) div TR);
            end;
        if ((TC > 0) and (TR > 0)) then
          for i := 1 to Data.NbJou do
            if C[i] > 0 then
              if (((jou^[i].Jihad > 0) and (R[jou^[i].Jihad] = 0) and
                (C[jou^[i].Jihad] = 0)) or (jou^[i].Jihad = 0)) then
                Points.PtsMartyrs(-1, i,
                  ((TC - M[monde].Conv) * C[i]) div TC);

        if M[monde].Proprio <> ME[monde].AncienProprio then
          for tres := 1 to NbTres do
            if ((T[tres].Localisation = monde) and (T[tres].Statut = 0)) then
              T[tres].Proprio := M[monde].Proprio;
        if M[monde].ProprConv = 0 then
          M[monde].Conv := 0;

      end;

end;

Procedure TEvalEmigration.Init;
var
  i: word;
begin
  with Data do
  begin
    FillChar(NouvN, sizeof(NouvN), 0);
    FillChar(NouvC, sizeof(NouvC), 0);
    FillChar(NouvR, sizeof(NouvR), 0);
    FillChar(Emig, sizeof(Emig), 0);

    // On rep�re les robots construits par les I
    for i := 1 to NbMonde do
      if (M[i].Robot > 0) and (M[i].Pop > 0) then
        if M[i].Proprio > 0 then
        begin
          Emig[i] := True;
          Inc(NouvR[M[i].Proprio, i], M[i].Robot);
          M[i].Robot := 0;
        end;
  end;
end;

end.
