unit eval.ChargeTresorExplo;

Interface

uses Avateval, Avatunit, eval.etape;

type

  TEvalChargTresEtExplo = class(TEvalEtape)
  public const
    TYPE_ORDRE = 13;
    ETAPE_EVAL = 12;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    ExploP : array[0..1024, 0..NB_JOU_MAX] of byte;
    ExploCM : array[0..1024, 0..NB_JOU_MAX] of byte;

    procedure ChargeLeTresor(tr, fl: word);
    procedure ExplorationCM(fl, x: integer);
    procedure ExplorationP(fl, x: integer);
    procedure ResolutionExploration;
  end;

Implementation

uses erreurseval, Eval.APIPoints;

procedure TEvalChargTresEtExplo.Init;
begin
  FillChar(ExploP, sizeof(ExploP), 0);
  FillChar(ExploCM, sizeof(ExploCM), 0);
end;


procedure TEvalChargTresEtExplo.ChargeLeTresor(tr,fl : word);
var fl2,md : word;
begin
  with Data do
  begin
    md:=T^[tr].Localisation;
    fl2:=md;
    if O.Auteur<>T^[tr].proprio then Erreur(ERR_PAS_PROPRIO_TRESOR)
    else if ((T^[tr].Statut=0)and(md<>F^[fl].Localisation)) then Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
    else if ((T^[tr].Statut=1)and(F^[fl2].localisation<>F^[fl].Localisation))
       then Erreur(ERR_FLOTTE_PAS_SUR_MONDE);
    if (O.Erreur = 0) and (Jou[O.Auteur].Classe <> Antiquaire) then
    begin
      if F^[fl].Proprio = 0 then
         Erreur(ERR_DEST_PAS_BONNE_CLASSE)
      else if ((O.Auteur<>F^[fl].Proprio)and(jou[F^[fl].Proprio].Classe<>Antiquaire))
         then Erreur(ERR_DEST_PAS_BONNE_CLASSE)
      else if (T^[tr].Statut=1) and (jou[F^[fl].Proprio].Classe<>Antiquaire)
         then Erreur(ERR_DEST_PAS_BONNE_CLASSE)
    end;
    if O.Erreur = 0 then
    begin
       T^[tr].Statut:=1;
       T^[tr].Localisation:=fl;
    end;
  end;
end;

procedure TEvalChargTresEtExplo.ExplorationP(fl, x : integer);
begin
  with Data do
  begin
    if x > MAXMEVA - M[F^[fl].Localisation].NbExplo then
    begin
      x := MAXMEVA - M[F^[fl].Localisation].NbExplo;
      Erreur(ERR_MAX_EXPLO);
      OK := True;
    end;
    if F^[fl].NbVC < x then
    begin
      Erreur(ERR_FLOTTE_PAS_ASSEZ_VC);
      OK := True;
      x := F^[fl].NbVC;
    end;
    if x < 0 then x := 0;
    if (Jou[O.Auteur].Classe <> Explorateur) and (not MEVAPourTous) then Erreur(ERR_MAUVAISE_CLASSE)
    else if F^[fl].Proprio <> O.Auteur then Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if (x > 0) and OK then
    begin
      ExploP[F^[fl].Localisation, O.Auteur] := ExploP[F^[fl].Localisation, O.Auteur] + x;
      Dec(F[fl].NbVC, x);
      F^[fl].Statut := F^[fl].Statut or Puis2(5);
    end;
  end;
end;

procedure TEvalChargTresEtExplo.ExplorationCM(fl, x : integer);
begin
  with Data do
  begin
    if (Jou[O.Auteur].Classe <> Explorateur) and (not MEVAPourTous) then Erreur(ERR_MAUVAISE_CLASSE)
    else if F[fl].Proprio <> O.Auteur then Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else
    begin
      if x > MAXMEVA - M[F[fl].Localisation].NbExplo then
      begin
        x := MAXMEVA - M[F[fl].Localisation].NbExplo;
        Erreur(ERR_MAX_EXPLO);
        OK := True;
      end;
      if F^[fl].NbVC < x then
      begin
        Erreur(ERR_FLOTTE_PAS_ASSEZ_VC);
        OK := True;
        x := F^[fl].NbVC;
      end;
      if x < 0 then x := 0;
      if (x > 0) and OK then
      begin
        ExploCM[F[fl].Localisation, O.Auteur] := ExploCM[F[fl].Localisation, O.Auteur] + x;
        Dec(F[fl].NbVC, x);
        F[fl].Statut := F[fl].Statut or Puis2(5);
      end;
    end;
  end;
end;

// pour r�gler les conflits o� plusieurs explorateurs explorent en m�me temps
// Mise � jour vaisseaux flottes
procedure TEvalChargTresEtExplo.ResolutionExploration;
var
  md : integer;
  i,j : integer;
  TotExpl : integer;
  x, x2 : Integer;
  NbVCExplo : integer;
  //PlusPop : integer;
begin
  with Data do for md := 1 to NbMonde do
  begin
    //PlusPop := 0;
    TotExpl := 0;
    for j := 1 to NbJou do inc(TotExpl, ExploP[md, j] + ExploCM[md, j]);
    if TotExpl > 0 then
    begin
      for j := 1 to NbJou do
        Points.PtsMEVA(j, md, (ExploP[md, j] + ExploCM[md, j]) * (ExploP[md, j] + ExploCM[md, j]) / TotExpl);
      NbVCExplo := TotExpl;
      if NbVCExplo > MAXMEVA - M[md].NbExplo then
        NbVCExplo := MAXMEVA - M[md].NbExplo;
      for i := 1 to NbVCExplo do
      begin
        x2 := 0;
        x := Random(TotExpl) + 1; {On choisit quel Vc va explorer}
        for j := 1 to NbJou do {on retrouve le VC en question dans la liste}
        begin
          x2 := x2 + ExploP[md, j];
          if x2 >= x then
          begin
            {cette fois-ci on fait l'exploration avec MAV de la pop}
            Dec(ExploP[md, j]);
            Inc(M[md].NbExplo);
            Inc(M[md].NbExploPop);
            // 5 MaxPop de plus depuis le 16/09/98
            Inc(M[md].MaxPop, 5);
            //Inc(PlusPop); {on r�gle �a plus tard}
            Dec(TotExpl);
            Break;
          end;
          x2 := x2 + ExploCM[md, j];
          if x2 >= x then
          begin
            {cette fois-ci on fait l'exploration avec MAV de la CM}
            Dec(ExploCM[md, j]);
            Inc(M[md].NbExplo);
            Inc(M[md].NbExploCM);
            {On fait l'augmentation de CM si n�cessaire}
//            if (M^[md].Duree + M[md].NbExploCM * 2) mod 7 <= 1 then
//              if ((M^[md].Duree + M[md].NbExploCM * 2) > 0) and (M^[md].PlusMP < 10)
            if (M^[md].Duree + M[md].NbExploCM * 3) mod 7 <= 2 then
              if ((M^[md].Duree + M[md].NbExploCM * 3) > 0) and (M^[md].PlusMP < 10)
                then Inc(M^[md].PlusMP);
            Dec(TotExpl);
            Break;
          end;
        end;
      end;
    end;
    {Si n�cessaire, on augmente la pop de 5% par MEVA investie dans la pop}
    {if PlusPop > 0 then
      M^[md].Pop := Round(M^[md].Pop * (1 + 0.05 * PlusPop));}
  end;
end;

Procedure TEvalChargTresEtExplo.Finalise;
var
  i : integer;
begin
  ResolutionExploration;
  for i := 1 to Data.NbFlotte do
    Data.UpdateContenuFlotte(i);

end;

function TEvalChargTresEtExplo.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalChargTresEtExplo.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

Procedure TEvalChargTresEtExplo.EvalueOrdre;
begin
  case O.StypeO of
    0 : ChargeLeTresor(O.O[1],O.O[2]);
    1 : ExplorationP(O.O[1], O.O[2]);
    2 : ExplorationCM(O.O[1], O.O[2]);
  end;
end;

end.
