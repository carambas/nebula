unit Eval.Construction;

Interface

uses Avateval,eval.etape;

type
  TEvalConstruction = class(TEvalEtape)
  public
    const TYPE_ORDRE = 3;
    const ETAPE_EVAL = 3;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure IndConstrFl(md,x,fl : word);
    procedure RobConstVx(md,x : word);
    procedure RobConstFl(md,x,fl : word);
    procedure IndConstrVI(md,x : word);
    procedure IndConstrVP(md,x : word);
    procedure IndConstrRob(md,x : word);
    procedure IndConstrEsp(md, x, categorie : word);
    procedure IndConstTech(md, x, tech : word);
  end;

Implementation

uses Avatunit, erreurseval, utils, eval.APIPoints;

procedure TEvalConstruction.IndConstrFl(md,x,fl : word);
begin
   if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.F[fl].Localisation<>md then Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
   else if Data.ME[md].IndLibre<x then
   begin
      Erreur(ERR_PAS_ASSEZ_I);
      x:=Data.ME[md].IndLibre;
      OK:=True;
   end;
   if OK then
   begin
      if O.StypeO = 1 then
        Inc(Data.F[fl].NbVC,x);
      if O.StypeO = 14 then
        Inc(Data.F[fl].NbVT,x);
      Dec(Data.M[md].MP,x);
      Dec(Data.ME[md].IndLibre,x);
      Dec(Data.ME[md].PopLibre,x);
   end;
end;

procedure TEvalConstruction.RobConstVx(md,x : word);
begin
   if Data.Jou[O.Auteur].Classe <> Robotron then Erreur(ERR_MAUVAISE_CLASSE)
   else if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.M[md].Robot < x then
   begin
     Erreur(ERR_PAS_ASSEZ_ROB);
     x := Data.M[md].Robot;
     OK := true;
   end
   else if Odd(x) then
   begin
      Erreur(ERR_NB_ROB_PAS_PAIR);
      dec(x);
      OK:=true;
   end;
   if OK then
   begin
      if O.StypeO = 6 then // VI
        Inc(Data.M[md].VI,x div 2)
      else // VP
        Inc(Data.M[md].VP,x div 2);
      Dec(Data.M[md].Robot,x);
      if (Data.M[md].Robot + Data.M[md].Pop) = 0 then
        Data.M[md].Proprio := 0;
   end;
end;

procedure TEvalConstruction.RobConstFl(md,x,fl : word);
begin
   if Data.Jou[O.Auteur].Classe <> Robotron then Erreur(ERR_MAUVAISE_CLASSE)
   else if Data.M[md].Proprio <> O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.F[fl].Localisation <> md then Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
   else if Data.M[md].Robot < x then
   begin
     Erreur(ERR_PAS_ASSEZ_ROB);
     x := Data.M[md].Robot;
     OK := true;
   end
   else if Odd(x) then
   begin
      Erreur(ERR_NB_ROB_PAS_PAIR);
      dec(x);
      OK:=true;
   end;
   if OK then
   begin
      Inc(Data.F[fl].NbVC,x div 2);
      Dec(Data.M[md].Robot,x);
      if (Data.M[md].Robot + Data.M[md].Pop) = 0 then
        Data.M[md].Proprio := 0;
   end;
end;

procedure TEvalConstruction.IndConstrVI(md,x : word);
begin
   if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.ME[md].IndLibre<x then
   begin
      Erreur(ERR_PAS_ASSEZ_I);
      x:=Data.ME[md].IndLibre;
      OK:=true;
   end;
   if OK then
   begin
      Inc(Data.M[md].VI,x);
      Dec(Data.M[md].MP,x);
      Dec(Data.ME[md].PopLibre,x);
      Dec(Data.ME[md].IndLibre,x);
   end;
end;

procedure TEvalConstruction.IndConstrVP(md,x : word);
begin
   if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.ME[md].IndLibre<x then
   begin
      Erreur(ERR_PAS_ASSEZ_I);
      x:=Data.ME[md].IndLibre;
      OK:=true;
   end;
   if OK then
   begin
      Inc(Data.M[md].VP,x);
      Dec(Data.M[md].MP,x);
      Dec(Data.ME[md].PopLibre,x);
      Dec(Data.ME[md].IndLibre,x);
   end;
end;

procedure TEvalConstruction.IndConstrRob(md,x : word);
var
  PopMoins, RobMoins, ConvMoins : integer;
begin
   if Data.Jou[O.Auteur].Classe<>5 then Erreur(ERR_MAUVAISE_CLASSE)
   else if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.ME[md].IndLibre<x then
   begin
      Erreur(ERR_PAS_ASSEZ_I);
      x:=Data.ME[md].IndLibre;
      OK:=True;
   end;
   if OK then
   begin
      Inc(Data.M[md].Robot,x);
      Dec(Data.M[md].MP,x);
      Dec(Data.ME[md].PopLibre,x);
      Dec(Data.ME[md].indLibre,x);
   end;

   if (Data.M[md].Pop > 0) and (Data.M[md].Robot > 0) then
   begin
     // On regarde les �ventuelles pops tu�es
     ConvMoins := 0;
     PopMoins := Data.M[md].Robot * 4;
     if PopMoins > Data.M[md].Pop then
       PopMoins := Data.M[md].Pop;
     RobMoins := Round2(Data.M[md].Pop / 4);
     if RobMoins > Data.M[md].Robot then
       RobMoins := Data.M[md].Robot;

     // 16/11/2003 : Gestion des convertis
     // => prorata arrondi en-dessous (flemme de faire plus subtil)
     if Data.M[md].Conv > 0 then
       ConvMoins := PopMoins * Data.M[md].Conv div Data.M[md].Pop;

     Inc(Data.ME[md].PopMoins, PopMoins);
     Inc(Data.ME[md].RobMoins, RobMoins);
     Dec(Data.M[md].Pop, PopMoins);
     Dec(Data.M[md].Conv, ConvMoins);
     Dec(Data.M[md].Robot, RobMoins);

     Points.PtsPopTuees(Data.M[md].Proprio, Data.M[md].Proprio, PopMoins, md);
     Points.PtsMartyrs(Data.M[md].Proprio, Data.M[md].ProprConv, ConvMoins);

     if (Data.M[md].Robot + Data.M[md].Pop) = 0 then
       Data.M[md].Proprio := 0;
   end;
end;

procedure TEvalConstruction.Finalise;
begin
  inherited;

end;

function TEvalConstruction.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalConstruction.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalConstruction.IndConstrEsp(md, x, categorie : word);
begin
   if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if categorie > 9 then
     Erreur(ERR_CATEGORIE_ESPION_INEXISTANTE)
   else if Data.ME[md].IndLibre<x then
   begin
      Erreur(ERR_PAS_ASSEZ_I);
      x:=Data.ME[md].IndLibre;
      OK:=true;
   end;
   if OK then
   begin
      Inc(Espions[O.Auteur, categorie], x);
      Dec(Data.M[md].MP,x);
      Dec(Data.ME[md].PopLibre,x);
      Dec(Data.ME[md].IndLibre,x);
   end;
end;

procedure TEvalConstruction.IndConstTech(md, x, tech : word);
var
  techcourante, techrestante, cout : word;
  depense : word;
begin
   techrestante := 0;
   techcourante := 0;
   cout := Data.TechClasse[tech, Data.Jou[O.Auteur].Classe];
   case tech of
     1 :
       begin
         techcourante := Data.Jou[O.Auteur].DEP;
         techrestante := Data.Jou[O.Auteur].DEPreste;
       end;
     2 :
       begin
         techcourante := Data.Jou[O.Auteur].ATT;
         techrestante := Data.Jou[O.Auteur].ATTreste;
       end;
     3 :
       begin
         techcourante := Data.Jou[O.Auteur].DEF;
         techrestante := Data.Jou[O.Auteur].DEFreste;
       end;
     4 :
       begin
         techcourante := Data.Jou[O.Auteur].RAD;
         techrestante := Data.Jou[O.Auteur].RADreste;
       end;
     5 :
       begin
         techcourante := Data.Jou[O.Auteur].ALI;
         techrestante := Data.Jou[O.Auteur].ALIreste;
       end;
     6 :
       begin
         techcourante := Data.Jou[O.Auteur].CAR;
         techrestante := Data.Jou[O.Auteur].CARreste;
       end;
   end;
   if Data.M[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
   else if Data.ME[md].IndLibre<x then
   begin
      Erreur(ERR_PAS_ASSEZ_I);
      x:=Data.ME[md].IndLibre;
      OK:=true;
   end;
   if OK then
   begin
     if Data.MaxTech[tech, Data.Jou[O.Auteur].Classe] > 0 then
       if techcourante = Data.MaxTech[tech, Data.Jou[O.Auteur].Classe] then Erreur(33);
     if OK then
     begin
       depense := x;
       Repeat
         if depense < cout - techrestante then
         begin
           techrestante := depense + techrestante;
           depense := 0;
         end
         else
         begin
           dec(depense, cout - techrestante);
           inc(techcourante);
           techrestante := 0;
         end;
       Until (depense = 0) or ((Data.MaxTech[tech, Data.Jou[O.Auteur].Classe] > 0) and
         (techcourante = Data.MaxTech[tech, Data.Jou[O.Auteur].Classe]));
      Dec(Data.M[md].MP,x - depense);
      Dec(Data.ME[md].PopLibre,x - depense);
      Dec(Data.ME[md].IndLibre,x - depense);
      if depense > 0 then Erreur(ERR_TECH_DEPASSE_MAX);
      case tech of
        1 :
          begin
            Data.Jou[O.Auteur].DEP := techcourante;
            Data.Jou[O.Auteur].DEPreste := techrestante;
          end;
        2 :
          begin
            Data.Jou[O.Auteur].ATT := techcourante;
            Data.Jou[O.Auteur].ATTreste := techrestante;
            Data.Jou[O.Auteur].CA := CalculeCA(techcourante);
          end;
        3 :
          begin
            Data.Jou[O.Auteur].DEF := techcourante;
            Data.Jou[O.Auteur].DEFreste := techrestante;
            Data.Jou[O.Auteur].CD := CalculeCD(techcourante);
          end;
        4 :
          begin
            Data.Jou[O.Auteur].RAD := techcourante;
            Data.Jou[O.Auteur].RADreste := techrestante;
          end;
        5 :
          begin
            Data.Jou[O.Auteur].ALI := techcourante;
            Data.Jou[O.Auteur].ALIreste := techrestante;
          end;
        6 :
          begin
            Data.Jou[O.Auteur].CAR := techcourante;
            Data.Jou[O.Auteur].CARreste := techrestante;
          end;
      end;
     end;
   end;
end;

procedure TEvalConstruction.Init;
begin
  inherited;

end;

Procedure TEvalConstruction.EvalueOrdre;
begin
  OK:=True;
  Case O.StypeO of
     1,14   : IndConstrFl(O.O[1],O.O[2],O.O[3]);
     2      : IndConstrVI(O.O[1],O.O[2]);
     3      : IndConstrVP(O.O[1],O.O[2]);
     4      : IndConstrRob(O.O[1],O.O[2]);
     5      : IndConstrEsp(O.O[1], O.O[2], O.O[3]);
     8      : IndConstTech(O.O[1], O.O[2], 1);
     9      : IndConstTech(O.O[1], O.O[2], 2);
     10     : IndConstTech(O.O[1], O.O[2], 3);
     11     : IndConstTech(O.O[1], O.O[2], 4);
     12     : IndConstTech(O.O[1], O.O[2], 5);
     13     : IndConstTech(O.O[1], O.O[2], 6);
     6,7    : RobConstVx(O.O[1], O.O[2]);
     15     : RobConstFl(O.O[1],O.O[2],O.O[3]);
     16..24 : IndConstrEsp(O.O[1], O.O[2], O.StypeO - 15);
  end;
end;

end.
