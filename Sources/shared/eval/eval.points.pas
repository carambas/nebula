unit Eval.Points;

Interface

uses
  Avateval, eval.etape;

type
  TEvalPoints = class(TEvalEtape)
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = 100;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  end;

Implementation

uses Avatunit, eval.APIPoints, SysUtils
{$IFDEF NEBULA}
, Vcl.Dialogs
{$ENDIF}
;

procedure TEvalPoints.EvalueOrdre;
begin
  inherited;

end;

Procedure TEvalPoints.Finalise;
var j : word;
    Victoire : boolean;
begin
   with Data do
   begin
     Points.CompteFamillesTresors;

     for j := 1 to NbJou do
     begin
       Points.PtsPop(j);
       Points.PtsInd(j);
       Points.PtsCM(j);
       Points.PtsFlottes(j);
       Points.PtsMondes(j);
       Points.PtsMondesConv(j);
       Points.PtsConv(j);
       Points.PtsMondesRob(j);
       Points.PtsTresors(j);
       if Points.Bonus(j) <> 0 then
         DetailScores[j, DS_BONUS] := Points.Bonus(j);
     end;

     Victoire := (Tour = TourFinal);

     EcritLog('');
     EcritLog('Etat des bonus de fin de partie en l''�tat des choses :');

     for j := 1 to NbJou do if Points.Bonus(j) <> 0 then
     begin
       EcritLog(Format('%30s : %4d', [Jou[j].Pseudo, Points.Bonus(j)]));
     end;
     EcritLog('');

     if Victoire then
     begin
        Points.AjouteBonus;

        EcritLog('La partie est termin�e. Voir stat.');
        {$IFDEF NEBULA}
        ShowMessage('La partie est termin�e. Voir stat.');
        {$ENDIF}

     end
   end;
end;

function TEvalPoints.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalPoints.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalPoints.Init;
begin
  inherited;

end;

end.
