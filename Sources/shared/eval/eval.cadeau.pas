unit Eval.Cadeau;

Interface

uses Avateval, eval.etape;

type

  TEvalCadeau = class(TEvalEtape)
  public const
    TYPE_ORDRE = 20;
    ETAPE_EVAL = 20;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure OffreFlotte(fl, j: word);
    procedure OffreMonde(md, j: word);
  end;

  // On regarde les ordres de cadeaux en avance de phase
  // afin de noter qu'il y a un ordre exclusif
  TEvalNoteCadeauxFlottes = class(TEvalEtape)
  public const
    TYPE_ORDRE = 20;
    ETAPE_EVAL = 13;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure NoteOffreFlotte(fl, j: word);
  end;

  Implementation

uses Avatunit, erreurseval;

{ TNebEvalCadeau }

procedure TEvalCadeau.Finalise;
begin
  inherited;

end;

function TEvalCadeau.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalCadeau.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalCadeau.Init;
begin
  inherited;

end;

procedure TEvalCadeau.OffreFlotte(fl,j : word);
begin
  with Data do
  begin
    if O.Auteur<>F^[fl].Proprio then Erreur(5)
    else if (not ConnuDeNom[O.Auteur,j]) then Erreur(ERR_CONNAIT_PAS_DE_NOM)
    else if (not EstCoequipier(O.Auteur, j)) and DiploInterdite then Erreur(ERR_ORDRE_INTERDIT)
    else if (FE^[fl].Statut and Puis2(2))<>0 then Erreur(20)
    else if ((FE^[fl].OrdrExcl<>8)and(FE^[fl].Statut=0)) then Erreur(20)
    else
    begin
       if FE^[fl].AncienProprio=0 then FE^[fl].AncienProprio:=F^[fl].Proprio;
       F^[fl].Proprio:=j;
       F^[fl].Statut:=(F^[fl].Statut or Puis2(0));
       FE^[fl].Statut:=(FE^[fl].Statut or Puis2(2));
       DevientConnu(F^[fl].Localisation,j);

       // On enl�ve les convertis lors du cadeau
       F^[fl].N := F^[fl].N + F^[fl].C;
       F^[fl].C := 0;
    end;
  end;
end;

procedure TEvalCadeau.OffreMonde(md,j : word);
var tr : word;
begin
  with Data do
  begin
    if O.Auteur<>M^[md].Proprio then Erreur(4)
    else if (ME^[md].Statut and Puis2(2))<>0 then Erreur(4)
    else if (not ConnuDeNom[O.Auteur,j]) then Erreur(ERR_CONNAIT_PAS_DE_NOM)
    else if (not EstCoequipier(O.Auteur, j)) and DiploInterdite then Erreur(ERR_ORDRE_INTERDIT)
    else
    begin
       if ME^[md].AncienProprio=0 then ME^[md].AncienProprio:=M^[md].Proprio;
       M^[md].Proprio:=j;
       ME^[md].Statut:=(ME^[md].Statut or Puis2(2));

       for tr:=1 to NbTres do if ((T^[tr].Localisation=md)and(T^[tr].Statut=0))
          then T^[tr].Proprio:=j;
       DevientConnu(md,j);
    end;
  end;
end;

Procedure TEvalCadeau.EvalueOrdre;
begin
  case O.StypeO of
    1 : OffreFlotte(O.O[1],O.O[2]);
    2 : OffreMonde(O.O[1],O.O[2]);
  end;
end;

{ TEvalNoteCadeauxFlottes }

procedure TEvalNoteCadeauxFlottes.EvalueOrdre;
begin
  inherited;

  case O.StypeO of
    1 : NoteOffreFlotte(O.O[1],O.O[2]);
  end;
end;

procedure TEvalNoteCadeauxFlottes.Finalise;
begin
  inherited;

end;

function TEvalNoteCadeauxFlottes.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalNoteCadeauxFlottes.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalNoteCadeauxFlottes.Init;
begin
  inherited;

end;

// Note les cadeaux pour savoir si l'ordre est exclusif
procedure TEvalNoteCadeauxFlottes.NoteOffreFlotte(fl, j: word);
begin
  with data do
  begin
    if O.Auteur=F^[fl].Proprio then {pas d'erreur si non, ca viendra plus tard}
    if (not ConnuDeNom[O.Auteur,j]) then Erreur(ERR_CONNAIT_PAS_DE_NOM)
    else if ((FE^[fl].OrdrExcl<>0)and(FE^[fl].Statut=0)) then {Erreur(20) -> rien car ca viendra plus tard}
    else
    begin
       FE^[fl].OrdrExcl := 8;
    end;
  end;
end;

end.
