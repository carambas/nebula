unit Eval.Combat;

INTERFACE

uses
  AvatUnit, Avateval, eval.etape;

type
  TEvalCombat = class(TEvalEtape)
  public
  public const
    TYPE_ORDRE = 14;
    ETAPE_EVAL = 14;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    VPMoins,VIMoins : array[1..1024] of word;
    BombeLarguee : array[1..1024, 1..NB_JOU_MAX] of Integer;

    function CA(fl: word; victime: integer): real;
    function CD(fl: word): real;
    function CAm(md: word; victime: integer): real;
    function CDm(md: word): real;
    function EstEnEmbuscade(md, j: integer): boolean;
    procedure EnleveVaissFl(fl, d: word);
    procedure FlotteBouge;
    procedure FlotteTire;
    procedure VITire;
    procedure VPTire;
    procedure ResolCombats;
    procedure ResolReste;
    procedure ResolFinale;
    procedure ResolDeplacements;
  end;

IMPLEMENTATION

uses
  Utils, Eval.APIPoints, erreurseval;

Procedure TEvalCombat.EnleveVaissFl(fl, d: word);
begin
  with Data do
  begin
    if d > F[fl].NbVC + F[fl].NbVT then
    begin
      F[fl].NbVC := 0;
      F[fl].NbVT := 0;
      F[fl].Proprio := 0;
    end
    else if d > F[fl].NbVC then
    begin
      F[fl].NbVT := F[fl].NbVT - (d - F[fl].NbVC);
      F[fl].NbVC := 0;
    end
    else
    begin
      F[fl].NbVC := F[fl].NbVC - d;
    end;
    UpdateContenuFlotte(fl);
  end;
end;

Function TEvalCombat.EstEnEmbuscade(md, j: integer): boolean;
begin
  Result := (Embuscade[md] and Puis2(j - 1)) = 0;
end;

Function TEvalCombat.CA(fl: word; victime: integer): real;
var
  CAtt: real;
begin
  if (Data.NoJou > 0) and (Data.F[fl].Proprio <> Data.NoJou) then
  begin
    if (Data.F[fl].Proprio > 0) and (Data.Jou[Data.F[fl].Proprio].ATT = 0) then
    begin
      Result := 0;
      Exit;
    end;
  end;

  begin
    if Data.F[fl].Proprio = 0 then
      Result := 0.5
    else
    begin
      CAtt := Data.Jou[Data.F[fl].Proprio].CA / 100;
      if (victime > 0) and (Data.Jou[Data.F[fl].Proprio].Classe = Missionnaire)
        and (Data.Jou[Data.F[fl].Proprio].Jihad = victime) then
        CAtt := CalculeCA(Data.Jou[Data.F[fl].Proprio].ATT + 4) / 100;
      Result := CAtt;
    end;
  end;
end;

Function TEvalCombat.CD(fl: word): real;
var
  c: real;
begin
  if (Data.NoJou > 0) and (Data.F[fl].Proprio <> Data.NoJou) then
  begin
    if (Data.F[fl].Proprio > 0) and (Data.Jou[Data.F[fl].Proprio].DEF = 0) then
    begin
      Result := 1;
      Exit;
    end;
  end;

  begin
    if Data.F[fl].Proprio = 0 then
      c := 1
    else if Data.F[fl].NbVC + Data.F[fl].NbVT > 0 then
      c := (Data.Jou[Data.F[fl].Proprio].CD / 100 * Data.F[fl].NbVC + CDVT *
        Data.F[fl].NbVT) / (Data.F[fl].NbVC + Data.F[fl].NbVT) *
        (1 + Data.ContenuFlotte(fl) / Data.CapaciteFlotte(fl))
    else
      c := 1;
    if Data.FE[fl].OrdrExcl = 7 then
      c := c / 2;
    CD := c;
  end;
end;

Function TEvalCombat.CAm(md: word; victime: integer): real;
begin
  if (Data.NoJou > 0) and (Data.M[md].Proprio <> Data.NoJou) then
  begin
    if (Data.M[md].Proprio > 0) and (Data.Jou[Data.M[md].Proprio].ATT = 0) then
    begin
      Result := 0;
      Exit;
    end;
  end;

  with Data do
  begin
    if M[md].Proprio = 0 then
      Result := 0.5
    else
    begin
      Result := Data.Jou[M[md].Proprio].CA / 100;
      if (victime > 0) and (Jou[M[md].Proprio].Classe = Missionnaire) and
        (Jou[M[md].Proprio].Jihad = victime) then
        Result := CalculeCA(Jou[M[md].Proprio].ATT + 4) / 100;
    end;
  end;

end;

Function TEvalCombat.CDm(md: word): real;
begin
  if (Data.NoJou > 0) and (Data.M[md].Proprio <> Data.NoJou) then
  begin
    if (Data.M[md].Proprio > 0) and (Data.Jou[Data.M[md].Proprio].DEF = 0) then
    begin
      Result := 1;
      Exit;
    end;
  end;

  with Data do
  begin
    if M[md].Proprio = 0 then
      Result := 1
    else
      Result := Jou^[M[md].Proprio].CD / 100;
  end;
end;

procedure TEvalCombat.VPTire;
var
  monde, fl: word;
begin
  with Data do
  begin
    monde := O.O[1];
    fl := O.O[2];
    if O.Auteur <> M[monde].Proprio then
      Erreur(ERR_PAS_PROPRIO_MONDE)
    else if ME[monde].ActionVP > 0 then
      Erreur(ERR_VAISS_DEJA_ORDRE_EXCL)
    else if M[monde].VP = 0 then
      Erreur(ERR_PAS_ASSEZ_VP)
    else
    begin
      if ((O.StypeO = 1)) then
        ME[monde].ActionVP := 1
      else if ((O.StypeO = 2)) then
        ME[monde].ActionVP := 2
      else
      begin
        if F[fl].Localisation <> monde then
          Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
        else
        begin
          ME[monde].ActionVP := 3;
          ME[monde].CibleVP := fl;
          if O.StypeO > 128 then
            Inc(ME[monde].ActionVP, 128);
        end;
      end;
    end;
  end;
end;

procedure TEvalCombat.VITire;
var
  monde, fl: word;
begin
  with Data do
  begin
    monde := O.O[1];
    fl := O.O[2];
    if O.Auteur <> M[monde].Proprio then
      Erreur(ERR_PAS_PROPRIO_MONDE)
    else if ME[monde].ActionVI > 0 then
      Erreur(ERR_VAISS_DEJA_ORDRE_EXCL)
    else if M[monde].VI = 0 then
      Erreur(ERR_PAS_ASSEZ_VI)
    else if F[fl].Localisation <> monde then
      Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
    else
    begin
      ME[monde].ActionVI := 3;
      if O.StypeO > 128 then
        Inc(ME[monde].ActionVI, 128);
      ME[monde].CibleVI := fl;
    end;
  end;
end;

procedure TEvalCombat.FlotteTire;
var
  fl, monde, fl2, x: word;
begin
  with Data do
  begin
    fl := O.O[1];
    monde := F[fl].Localisation;
    if F[fl].Proprio <> O.Auteur then
      Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if ((FE[fl].OrdrExcl > 0) and (FE[fl].OrdrExcl <> 8)) then
      Erreur(ERR_FLOTTE_DEJA_ORDRE_EXCL) { 18/06/97 }
    else
    begin
      // F_f * I
      if O.StypeO mod 128 = 5 then
      begin
        if (TirIndustries = 0) or
          ((TirIndustries = 2) and EstCoequipier(O.Auteur, M[monde].Proprio))
        then
          Erreur(ERR_ORDRE_INTERDIT)
        else
        begin
          FE[fl].OrdrExcl := 4;
          FE[fl].victime := M[monde].Proprio;
        end;
      end
      else if O.StypeO mod 128 = 6 then
      begin
        FE[fl].OrdrExcl := 3;
        FE[fl].victime := M[monde].Proprio;
      end
      else if O.StypeO mod 128 = 7 then
      begin
        FE[fl].OrdrExcl := 2;
        FE[fl].victime := M[monde].Proprio;
      end
      else if O.StypeO mod 128 = 8 then
      begin
        fl2 := O.O[2];
        if F[fl2].Localisation <> F[fl].Localisation then
          Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
        else
        begin
          FE[fl].OrdrExcl := 1;
          FE[fl].victime := F[fl2].Proprio;
          FE[fl].Cible := fl2;
        end;
      end
      // F_f L BOMBE
      else if O.StypeO mod 128 = 9 then
      begin
        if BombesInterdites then
          Erreur(ERR_ORDRE_INTERDIT)
        else if (F[fl].Statut and Puis2(7)) = 0 then
          Erreur(ERR_FLOTTE_PAS_BOMBE)
        else
        begin
          FE[fl].OrdrExcl := 6;
          FE[fl].victime := M[monde].Proprio;
        end;
      end
      else if O.StypeO = 10 then { Attaque de robots }
      begin
        if Jou^[O.Auteur].Classe <> 5 then
          Erreur(ERR_CONNAIT_PAS_DE_NOM)
        else
        begin
          x := O.O[2];
          if x > F[fl].NbVC * 2 then
          begin
            Erreur(ERR_PAS_ASSEZ_VAISSEAUX);
            x := F[fl].NbVC * 2;
          end;
          if x > 0 then
            FE[fl].OrdrExcl := 5;
          FE[fl].victime := M[monde].Proprio;
          FE[fl].NbRob := x;
        end;
      end;
      if O.StypeO > 128 then
        Inc(FE[fl].OrdrExcl, 128);
    end;
  end;
end;

function TEvalCombat.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalCombat.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalCombat.FlotteBouge;
var
  fl: word;
begin
  with Data do
  begin
    fl := O.O[1];
    if O.Auteur <> F[fl].Proprio then
      Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if not connect(F[fl].Localisation, O.O[2]) then
      Erreur(ERR_MONDES_PAS_CONNECTES)
    else if ((FE[fl].OrdrExcl > 0) and (FE[fl].OrdrExcl <> 8)) then
      Erreur(ERR_FLOTTE_DEJA_ORDRE_EXCL) { 18/06/97 }
    else if F[fl].NbVC + F[fl].NbVT = 0 then
      Erreur(ERR_PAS_ASSEZ_VAISSEAUX)
    else
      FE[fl].OrdrExcl := 7;
  end;
end;

procedure TEvalCombat.ResolCombats;
var
  i, j, pr, monde, fl2: word;
  tire: boolean;
  moins, d, p, ind_detuites: word;
begin
  with Data do
  begin
    for i := 1 to NbMonde do
    begin
      { VP M_m * C }
      if ME[i].ActionVP = 1 then
      begin
        pr := M[i].Proprio;
        d := RND(CAm(i, M[i].ProprConv) * M[i].VP);
        if M[i].Robot = 0 then
          if M[i].Conv < d then
            d := M[i].Conv;
        Dec(M[i].Conv, d);
        Dec(M[i].Pop, d);
        Inc(ME[i].ConvMoins, d);
        Inc(Jou^[pr].PopTuees, d);

        Points.PtsPopTuees(pr, M[i].ProprConv, d, i);
        Points.PtsMartyrs(pr, M[i].ProprConv, d);

        if M[i].Conv = 0 then
          M[i].ProprConv := 0;
      end
      { VP M_m * N }
      else if ME[i].ActionVP = 2 then
      begin
        pr := M[i].Proprio;
        d := RND(CAm(i, 0) * M[i].VP);
        if M[i].Pop > 0 then
        begin
          if M[i].Pop - M[i].Conv < d then
            d := M[i].Pop - M[i].Conv;
          Dec(M[i].Pop, d);
          Inc(ME[i].PopMoins, d);
          Inc(Jou^[pr].PopTuees, d);

          Points.PtsPopTuees(pr, pr, d, i);

        end
        else if M[i].Robot > 0 then
        begin
          if M[i].Robot < d then
            d := M[i].Robot;
          Dec(M[i].Robot, d);
          Inc(ME[i].RobMoins, d);
        end;
      end
      { VP M_m * F_f }
      else if ME[i].ActionVP mod 128 = 3 then
      begin
        tire := False;
        pr := M[i].Proprio;
        fl2 := ME[i].CibleVP;
        if ME[i].ActionVP = 3 then
          tire := True
        else
        begin
          for j := 1 to NbFlotte do
            if ((F[j].Localisation = i) and ((FE[j].victime = pr) or
              EstAllie(pr, FE[j].victime)) and (FE[j].OrdrExcl < 128) and
              (F[j].Proprio = F[fl2].Proprio)) then
              tire := True;
        end;
        if tire then
        begin
          ME[i].ActionVP := 3;
          d := RND(CAm(i, F[fl2].Proprio) * M[i].VP);
          d := RND(d * CD(fl2));
          if F[fl2].NbVC + F[fl2].NbVT - FE[fl2].VaisMoins < d then
            d := F[fl2].NbVC + F[fl2].NbVT - FE[fl2].VaisMoins;
          Inc(FE[fl2].VaisMoins, d);

          Points.PtsVaissDetr(pr, F[fl2].Proprio, d, 0);
        end;
      end;
      { VI M_m * F_f }
      if ME[i].ActionVI mod 128 = 3 then
      begin
        tire := False;
        pr := M[i].Proprio;
        fl2 := ME[i].CibleVI;
        if ME[i].ActionVI = 3 then
          tire := True
        else
        begin
          for j := 1 to NbFlotte do
            if ((F[j].Localisation = i) and ((FE[j].victime = pr) or
              EstAllie(pr, FE[j].victime)) and (FE[j].OrdrExcl < 128) and
              (F[j].Proprio = F[fl2].Proprio)) then
              tire := True;
        end;
        if tire then
        begin
          ME[i].ActionVI := 3;
          d := RND(CAm(i, F[fl2].Proprio) * M[i].VI);
          d := RND(d * CD(fl2));
          if F[fl2].NbVC + F[fl2].NbVT - FE[fl2].VaisMoins < d then
            d := F[fl2].NbVC + F[fl2].NbVT - FE[fl2].VaisMoins;
          Inc(FE[fl2].VaisMoins, d);

          Points.PtsVaissDetr(pr, F[fl2].Proprio, d, 0);
        end;
      end;
    end;
    for i := 1 to NbFlotte do
    begin
      { F_f * F_g }
      if FE[i].OrdrExcl mod 128 = 1 then
      begin
        tire := False;
        fl2 := FE[i].Cible;
        pr := F[i].Proprio;
        monde := F[i].Localisation;
        if FE[i].OrdrExcl = 1 then
          tire := True
        else
        begin
          for j := 1 to NbFlotte do
            if ((F[j].Localisation = monde) and ((FE[j].victime = pr) or
              EstAllie(pr, FE[j].victime)) and (FE[j].OrdrExcl < 128) and
              (F[j].Proprio = F[fl2].Proprio)) then
              tire := True;
          if (M[monde].Proprio = F[fl2].Proprio) and (ME[monde].ActionVI = 3)
          then
            if (F[ME[monde].CibleVI].Proprio = pr) or
              EstAllie(pr, F[ME[monde].CibleVI].Proprio) then
              tire := True;
          if (M[monde].Proprio = F[fl2].Proprio) and (ME[monde].ActionVP = 3)
          then
            if (F[ME[monde].CibleVP].Proprio = pr) or
              EstAllie(pr, F[ME[monde].CibleVP].Proprio) then
              tire := True;
        end;
        if tire then
        begin
          FE[i].OrdrExcl := 1;
          d := RND(CA(i, F[fl2].Proprio) * F[i].NbVC + CAVT * F[i].NbVT);
          d := RND(d * CD(fl2));
          if F[fl2].NbVC + F[fl2].NbVT - FE[fl2].VaisMoins < d then
            d := F[fl2].NbVC + F[fl2].NbVT - FE[fl2].VaisMoins;
          Inc(FE[fl2].VaisMoins, d);

          Points.PtsVaissDetr(pr, F[fl2].Proprio, d, 0);

        end;
      end
      { F_f * M }
      else if FE[i].OrdrExcl mod 128 = 2 then
      begin
        tire := False;
        pr := F[i].Proprio;
        monde := F[i].Localisation;
        if FE[i].OrdrExcl = 2 then
          tire := True
        else
        begin
          if ME[monde].ActionVI = 3 then
            if ((F[ME[monde].CibleVI].Proprio = pr) or EstAllie(pr,
              F[ME[monde].CibleVI].Proprio)) then
              tire := True;
          if ME[monde].ActionVP = 3 then
            if ((F[ME[monde].CibleVP].Proprio = pr) or EstAllie(pr,
              F[ME[monde].CibleVP].Proprio)) then
              tire := True;
          for j := 1 to NbFlotte do
            if ((F[j].Proprio = M[monde].Proprio) and
              ((FE[j].victime = pr) or EstAllie(pr, FE[j].victime)) and
              (FE[j].OrdrExcl < 128) and (F[j].Localisation = monde)) then
              tire := True;
        end;
        if tire then
        begin
          FE[i].OrdrExcl := 2;
          d := RND(CA(i, M[monde].Proprio) * F[i].NbVC + CAVT * F[i].NbVT);
          d := RND(CDm(monde) * d);
          if d > M[monde].VI + M[monde].VP - VIMoins[monde] - VPMoins[monde]
          then
            d := M[monde].VI + M[monde].VP - VIMoins[monde] - VPMoins[monde];
          if M[monde].VI - VIMoins[monde] < d / 2 then
          begin
            moins := M[monde].VI - VIMoins[monde];
            VIMoins[monde] := M[monde].VI
          end
          else
          begin
            moins := d div 2;
            Inc(VIMoins[monde], moins);
          end;
          if M[monde].VP - VPMoins[monde] < d - moins then
          begin
            Inc(moins, M[monde].VP - VPMoins[monde]);
            VPMoins[monde] := M[monde].VP
          end
          else
          begin
            Inc(VPMoins[monde], d - moins);
            moins := d;
          end;
          if M[monde].VI - VIMoins[monde] > 0 then
          begin
            Inc(VIMoins[monde], d - moins);
            if VIMoins[monde] > M[monde].VI then
              VIMoins[monde] := M[monde].VI;
          end;

          Points.PtsVaissDetr(pr, M[monde].Proprio, d, monde);

        end;
      end;
    end;
    for i := 1 to NbFlotte do
    begin
      { F_f * P }
      if FE[i].OrdrExcl mod 128 = 3 then
      begin
        pr := F[i].Proprio;
        monde := F[i].Localisation;
        tire := False;
        if FE[i].OrdrExcl = 3 then
          tire := True
        else
        begin
          if ((ME[monde].ActionVI = 3) and ((F[ME[monde].CibleVI].Proprio = pr)
            or (EstAllie(pr, F[ME[monde].CibleVI].Proprio)))) then
            tire := True;
          if ((ME[monde].ActionVP = 3) and ((F[ME[monde].CibleVP].Proprio = pr)
            or (EstAllie(pr, F[ME[monde].CibleVP].Proprio)))) then
            tire := True;
          for j := 1 to NbFlotte do
            if ((F[j].Proprio = M[monde].Proprio) and
              (F[j].Localisation = monde) and ((FE[j].victime = pr) or
              (EstAllie(pr, FE[j].victime)) and (FE[j].OrdrExcl < 128))) then
              tire := True;
        end;
        if tire then
        begin
          FE[i].OrdrExcl := 3;
          d := RND(F[i].NbVC * CA(i, M[monde].Proprio) + CAVT * F[i].NbVT);
          if pr <> M[monde].Proprio then // m�me proprio -> CD = 100
            d := RND(d * CDm(monde));
          if M[monde].Robot > 0 then
            p := M[monde].Robot
          else
            p := M[monde].Pop;
          if d > M[monde].VP - VPMoins[monde] + p then
            d := M[monde].VP - VPMoins[monde] + p;
          if d > M[monde].VP - VPMoins[monde] then
          begin
            moins := M[monde].VP - VPMoins[monde];
            VPMoins[monde] := M[monde].VP;
          end
          else
          begin
            Inc(VPMoins[monde], d);
            moins := d;
          end;

          Points.PtsVaissDetr(pr, M[monde].Proprio,
            moins, monde);

          { l� y'a de la pop qui y est pass�e }
          if moins < d then
          begin
            if M[monde].Robot > 0 then
            begin
              Dec(M[monde].Robot, d - moins);
              Inc(ME[monde].RobMoins, d - moins);
              { les robots sont consid�r�s comme des vaisseaux pour les points }

              Points.PtsVaissDetr(pr, M[monde].Proprio,
                d - moins, monde);

            end
            else if M[monde].Pop > 0 then
            begin
              for j := 1 to d - moins do
                if M[monde].Conv > 0 then
                begin
                  if Random <= M[monde].Conv / M[monde].Pop then
                  begin
                    Inc(ME[monde].PopMoins);
                    Dec(M[monde].Conv);
                    Dec(M[monde].Pop);

                    Points
                      .PtsMartyrs(pr, M[monde].ProprConv, 1);
                  end
                  else
                  begin
                    Inc(ME[monde].PopMoins);
                    Dec(M[monde].Pop);
                  end
                end
                else
                { $$ YeDo ne pas oublier aussi d'enlever des pops dans ce cas (code non optimal !) }
                begin
                  Inc(ME[monde].PopMoins);
                  Dec(M[monde].Pop);
                end;
              Points.PtsPopTuees(pr, M[monde].Proprio,
                d - moins, monde);
              Inc(Jou^[pr].PopTuees, d - moins);
            end;
          end;
        end;
      end
      { F_f * I }
      else if FE[i].OrdrExcl mod 128 = 4 then
      begin
        pr := F[i].Proprio;
        monde := F[i].Localisation;
        tire := False;
        if FE[i].OrdrExcl = 4 then
          tire := True
        else
        begin
          if ((ME[monde].ActionVI = 3) and ((F[ME[monde].CibleVI].Proprio = pr)
            or (EstAllie(pr, F[ME[monde].CibleVI].Proprio)))) then
            tire := True;
          if ((ME[monde].ActionVP = 3) and ((F[ME[monde].CibleVP].Proprio = pr)
            or (EstAllie(pr, F[ME[monde].CibleVP].Proprio)))) then
            tire := True;
          for j := 1 to NbFlotte do
            if ((F[j].Proprio = M[monde].Proprio) and
              (F[j].Localisation = monde) and ((FE[j].victime = pr) or
              (EstAllie(pr, FE[j].victime)) and (FE[j].OrdrExcl < 3))) then
              tire := True;
        end;
        if tire then
        begin
          FE[i].OrdrExcl := 4;
          d := RND(CA(i, M[monde].Proprio) * F[i].NbVC + CAVT * F[i].NbVT);
          if pr <> M[monde].Proprio then
            d := RND(d * CDm(monde));
          if d > (M[monde].VI - VIMoins[monde]) + M[monde].Ind then
            d := (M[monde].VI - VIMoins[monde]) + M[monde].Ind;
          if d > (M[monde].VI - VIMoins[monde]) then
          begin
            moins := M[monde].VI - VIMoins[monde];
            VIMoins[monde] := M[monde].VI;
          end
          else
          begin
            moins := d;
            Inc(VIMoins[monde], d);
          end;

          ind_detuites := 0;
          if d - moins > 0 then
          begin
            if d - moins > M[monde].Ind then
            begin
              ind_detuites := M[monde].Ind;
              M[monde].Ind := 0;
            end
            else
            begin
              ind_detuites := d - moins;
              Dec(M[monde].Ind, d - moins);
            end;
          end;

          Points.PtsIndDetr(pr, M[monde].Proprio,
            ind_detuites, monde);
          Points.PtsVaissDetr(pr, M[monde].Proprio,
            d - ind_detuites, monde);
        end;
      end;
    end;
  end;
end;

procedure TEvalCombat.ResolDeplacements;
var
  i, fl: word;
  O2: TOrdre;
  procedure Erreur(n: word);
  begin
    O2.Erreur := n;
    OK := False;
  end;
  procedure Deplace(md2, n: word; traverse: boolean);
  var
    loc, pm, i: word;
    d: word;
  begin
    with Data do
    begin
      loc := F[fl].Localisation;
      pm := M[loc].Proprio;
      if F[fl].NbVC + F[fl].NbVT = 0 then
        Erreur(ERR_PAS_ASSEZ_VAISSEAUX)
      else if not connect(loc, md2) then
        Erreur(ERR_MONDES_PAS_CONNECTES)
      else if (not Simul) and (OldData.NbAVuConnect[loc] + OldData.NbAVuConnect
        [md2] = 0) then
        Erreur(ERR_CONNEXION_INCONNUE_DE_TOUS)
      else
      begin
        if traverse then
        begin
          if pm <> F[fl].Proprio then
            if not EstAllie(pm, F[fl].Proprio) then
              if EstEnEmbuscade(loc, pm) then
              begin
                // Embuscades VP
                // ActionVP = 0 => rien
                // ActionVP = 4 => embuscade
                if ((ME[loc].ActionVP in [0, 4]) and (M[loc].VP > 0)) then
                begin
                  d := RND(CAm(loc, F[fl].Proprio) * M[loc].VP * 2);
                  d := RND(CD(fl) * d * 2);
                  if d > F[fl].NbVC + F[fl].NbVT then
                    d := F[fl].NbVC + F[fl].NbVT;
                  EnleveVaissFl(fl, d);

                  Points.PtsVaissDetr(pm, F[fl].Proprio, d, 0);

                  ME[loc].ActionVP := 4;
                end;
                // Embuscades VI
                // ActionVI = 0 => rien
                // ActionVI = 4 => embuscade
                if ((ME[loc].ActionVI in [0, 4]) and (M[loc].VI > 0)) then
                begin
                  d := RND(CAm(loc, F[fl].Proprio) * M[loc].VI * 2);
                  d := RND(CD(fl) * d * 2);
                  if d > F[fl].NbVC + F[fl].NbVT then
                    d := F[fl].NbVC + F[fl].NbVT;
                  EnleveVaissFl(fl, d);

                  Points.PtsVaissDetr(pm, F[fl].Proprio, d, 0);

                  ME[loc].ActionVI := 4;
                end;
              end;
          { Embuscades flottes }
          for i := 1 to NbFlotte do
            if ((F[i].Localisation = loc) and (F[i].Proprio <> F[fl].Proprio)
              and (FE[i].OrdrExcl = 0) and (not EstEnPaix(i)) and
              (not EstAllie(F[i].Proprio, F[fl].Proprio)) and
              EstEnEmbuscade(loc, F[i].Proprio) and (F[i].NbVC + F[i].NbVT > 0))
            then
            begin
              d := RND((CA(i, F[fl].Proprio) * F[i].NbVC + CAVT *
                F[i].NbVT) * 2);
              d := RND(CD(fl) * d * 2);
              if d > F[fl].NbVC + F[fl].NbVT then
                d := F[fl].NbVC + F[fl].NbVT;
              EnleveVaissFl(fl, d);

              Points.PtsVaissDetr(F[i].Proprio,
                F[fl].Proprio, d, 0);

              FE[i].Statut := (FE[i].Statut or Puis2(3));
            end;
        end;
        if F[fl].NbVC + F[fl].NbVT = 0 then
          Erreur(ERR_PAS_ASSEZ_VAISSEAUX)
        else
        begin
          DevientConnu(md2, F[fl].Proprio);
          FE[fl].Chemin[n] := loc;
          FE[fl].Chemin[n + 1] := md2;
          F[fl].Localisation := md2;

          { Trou noir }
          if (M[md2].Statut and Puis2(6)) > 0 then
            Repeat
              for i := n + 1 to 8 do
                O2.O[i] := 0;
              Inc(n);
              Repeat
                md2 := Random(NbMonde) + 1;
              Until (M[md2].Statut and Puis2(6)) = 0;
              FE[fl].Chemin[n] := F[fl].Localisation;
              F[fl].Localisation := md2;
              DevientConnu(md2, F[fl].Proprio);
              d := Round2((F[fl].NbVC + F[fl].NbVT) * (Random(100) + 1) / 100);
              if d > F[fl].NbVC then
              begin
                F[fl].NbVT := F[fl].NbVT - (d - F[fl].NbVC);
                F[fl].NbVC := 0;
              end
              else
                F[fl].NbVC := F[fl].NbVC - d;
            until (M[md2].Statut and Puis2(6)) = 0;
        end;
      end;
    end;
  end;

var
  no: Integer;

begin

  begin
    for no := 0 to ListeOrdres.Count(TYPE_ORDRE) - 1 do
    begin
      O2 := ListeOrdres[TYPE_ORDRE, no];
      if ((O2.StypeO = 11) and (O2.Erreur = 0)) then
      with Data do
      begin
        fl := O2.O[1];
        EnleveVaissFl(fl, FE[fl].VaisMoins);
        FE[fl].VaisMoins := 0;
        i := 1;
        while (i <= 7) and (i <= Jou^[O2.Auteur].DEP) and (O2.O[i + 1] > 0) do
        begin
          if ((O2.O[i + 1] > 0) and (O2.Erreur = 0)) then
          begin
            if i = 1 then
              Deplace(O2.O[i + 1], i, False)
            else
              Deplace(O2.O[i + 1], i, True);
          end;
          Inc(i);
        end;
        if i < 8 then
          if (i > Jou^[O2.Auteur].DEP) and (O2.O[i + 1] > 0) then
          begin
            Erreur(ERR_PAS_ASSEZ_DEP);
          end;
      end;
      ListeOrdres[TYPE_ORDRE, no] := O2;
    end;
  end;
end;

procedure TEvalCombat.ResolReste;
type
  TTab = array [1 .. NB_MONDE_MAX, 0 .. NB_JOU_MAX] of word;
var
  fl, monde, i, j, proprC, prf: word;
  pm, CM, RM, TR: word;
  Rob, RobInit: TTab;
  ji: boolean;
  maxR, gagnant, nbMax: integer;
begin
  with Data do
  begin
    FillChar(Rob, sizeof(Rob), 0);
    FillChar(RobInit, sizeof(RobInit), 0);

    for i := 1 to NbMonde do
      for j := 1 to NbJou do
        Rob[i, j] := 0;

    for fl := 1 to NbFlotte do
      if FE[fl].OrdrExcl = 5 then
      begin
        if FE[fl].NbRob < FE[fl].VaisMoins * 2 then
          FE[fl].NbRob := 0
        else
          Dec(FE[fl].NbRob, FE[fl].VaisMoins * 2);

        Inc(FE[fl].VaisMoins, Round2(FE[fl].NbRob / 2));
        Inc(Rob[F[fl].Localisation, F[fl].Proprio], FE[fl].NbRob);
      end;

    for monde := 1 to NbMonde do
      if (M[monde].Statut and Puis2(6)) = 0 then
      begin
        { fin modif }
        TR := 0;
        Inc(Rob[monde, M[monde].Proprio], M[monde].Robot);
        RobInit := Rob;

        for i := 0 to NbJou do
          Inc(TR, Rob[monde, i]);

        proprC := M[monde].ProprConv;

        if TR > RND(M[monde].Pop / 4) then
        begin
          // les robots l'emportent
          maxR := 0;
          gagnant := 0;
          nbMax := 0;
          // le joueur neutre participe
          for i := 0 to NbJou do
          begin
            if Rob[monde][i] > maxR then
            begin
              maxR := Rob[monde][i];
              gagnant := i;
            end;
          end;

          // On v�rifie s'il n'y aurait pas d'ex-aequo
          for i := 0 to NbJou do
            if maxR = Rob[monde][i] then
              Inc(nbMax);

          if nbMax > 1 then
          begin
            // Traitement des ex-aequo : match nul
            M[monde].Proprio := 0;
            M[monde].Duree := 0;
            M[monde].Robot := 0;
          end
          else
          begin
            // Un vainqueur sans bavure
            M[monde].Proprio := gagnant;
            if M[monde].Proprio <> ME[monde].AncienProprio then
            begin
              M[monde].Duree := 0;
              ME[monde].Statut := (ME[monde].Statut or Puis2(0));
            end;
            RM := RND(M[monde].Pop / 4); { RM=Robots Morts }

            // On enl�ve � tout le monde les pertes du�s au conflit avec les pops
            for i := 0 to NbJou do
              Dec(Rob[monde][i], Round2(Rob[monde][i] * RM / TR));

            // Et maintenant le gagnant perd un robot par robot ennemi restant
            if Rob[monde][gagnant] > (TR - RM - Rob[monde][gagnant]) then
              M[monde].Robot := 2 * Rob[monde][gagnant] + RM - TR
            else
              // mais en garde au minimum 1 :-)
              M[monde].Robot := 1;
          end;

          RM := TR - M[monde].Robot;
          pm := M[monde].Pop;
          CM := M[monde].Conv;
          M[monde].Pop := 0;
          M[monde].Conv := 0;
          M[monde].ProprConv := 0;
        end
        else if TR < RND(M[monde].Pop / 4) then
        begin
          pm := 4 * TR;
          CM := pm * M[monde].Conv div M[monde].Pop;
          RM := TR;
          Dec(M[monde].Pop, pm);
          Dec(M[monde].Conv, CM);
        end
        else
        begin
          M[monde].Proprio := 0;
          M[monde].ProprConv := 0;
          pm := M[monde].Pop;
          CM := M[monde].Conv;
          RM := TR;
          M[monde].Pop := 0;
          M[monde].Conv := 0;
        end;
        ji := False;
        if proprC > 0 then
          for i := 1 to NbJou do
            if ((Rob[monde, i] > 0) and (Jou^[proprC].Jihad = i)) then
              ji := True;
        if ((proprC > 0) and (not ji)) then
          Points.PtsMartyrs(-1, proprC, CM);

        if TR > 0 then
          for j := 1 to NbJou do
            Points.PtsPopTuees(j, M[monde].Proprio,
              Round2(pm * RobInit[monde, j] / TR), monde);

        Inc(ME[monde].PopMoins, pm);
        Inc(ME[monde].ConvMoins, CM);
        Inc(ME[monde].RobMoins, RM);

        { d�termine qui a tu� les populations }
        if TR > 0 then
          for i := 1 to NbJou do
            if Rob[monde, i] > 0 then
            begin
              Inc(Jou^[i].PopTuees, (pm * Rob[monde, i]) div TR);
            end;
      end;
    for fl := 1 to NbFlotte do
      if FE[fl].OrdrExcl = 6
      then { BOMBE : modif si plusieurs joueurs la larguent simultan�ment }
      begin
        monde := F[fl].Localisation;
        prf := F[fl].Proprio;
        Inc(BombeLarguee[monde, prf]);
        F[fl].Statut := (F[fl].Statut and (not Puis2(7)));
      end;
  end;
end;

procedure TEvalCombat.ResolFinale;
var
  monde, fl: word;
  tot: integer;
  j: integer;
  PopDetruites, ConvDetruits, IndDetruites, TotDetruit: integer;
  prc, prm: integer;
begin
  with Data do
  begin
    // On enl�ve les d�g�ts
    for monde := 1 to NbMonde do
    begin
      if VIMoins[monde] > M[monde].VI then
        M[monde].VI := 0
      else
        Dec(M[monde].VI, VIMoins[monde]);
      if VPMoins[monde] > M[monde].VP then
        M[monde].VP := 0
      else
        Dec(M[monde].VP, VPMoins[monde]);
      if ((M[monde].Pop = 0) and (M[monde].Robot = 0)) then
      begin
        M[monde].Proprio := 0;
        M[monde].ProprConv := 0;
      end;
    end;
    for fl := 1 to NbFlotte do
      EnleveVaissFl(fl, FE[fl].VaisMoins);

    // R�solution bombe
    for monde := 1 to NbMonde do
    begin
      tot := 0;
      for j := 1 to NbJou do
        Inc(tot, BombeLarguee[monde, j]);
      if tot > 0 then
      begin
        prc := M[monde].ProprConv;
        prm := M[monde].Proprio;
        for j := 1 to NbJou do
          if BombeLarguee[monde, j] > 0 then
          begin
            // cl := Jou^[j].Classe;
            TotDetruit :=
              Round2((BombeLarguee[monde, j] * (M[monde].Pop + M[monde].VP +
              M[monde].VI + M[monde].Ind) / tot));
            PopDetruites :=
              Round2((BombeLarguee[monde, j] * M[monde].Pop) / tot);
            ConvDetruits :=
              Round2((BombeLarguee[monde, j] * M[monde].Conv) / tot);
            IndDetruites :=
              Round2((BombeLarguee[monde, j] * M[monde].Ind) / tot);

            Points.PtsMartyrs(j, prc, ConvDetruits);

            Points.PtsPopTuees(j, prm, PopDetruites, monde);
            Points.PtsVaissDetr(j, prm,
              TotDetruit - PopDetruites - IndDetruites, monde);
            Points.PtsIndDetr(j, prm, IndDetruites, monde);
            Points.PtsBombes(j, prm, monde);

            Inc(Jou^[j].PopTuees, PopDetruites);
          end;
        M[monde].Statut := Puis2(7);

        Inc(ME[monde].PopMoins, M[monde].Pop);
        Inc(ME[monde].ConvMoins, M[monde].Conv);
        Inc(ME[monde].RobMoins, M[monde].Robot);

        { On remet les compteurs du monde � z�ro }
        M[monde].Pop := 0;
        M[monde].Conv := 0;
        M[monde].ProprConv := 0;
        M[monde].Robot := 0;
        M[monde].Ind := 0;
        M[monde].VI := 0;
        M[monde].MaxPop := 0;
        M[monde].VP := 0;
        M[monde].MP := 0;
        M[monde].PlusMP := 0;
        M[monde].PC := 0;
        M[monde].Proprio := 0;
        M[monde].Duree := 0;
      end;
    end;

    // On remet � 0 les actions dans le cas de
    // tirs conditionnels n'ayant pas entrain� de tir.
    for monde := 1 to NbMonde do
    begin
      if ME[monde].ActionVI > 128 then
        ME[monde].ActionVI := 0;
      if ME[monde].ActionVP > 128 then
        ME[monde].ActionVP := 0;
    end;

    for fl := 1 to NbFlotte do
      if FE[fl].OrdrExcl > 128 then
        FE[fl].OrdrExcl := 0;
  end;
end;

procedure TEvalCombat.Init;
begin
  FillChar(BombeLarguee, sizeof(BombeLarguee), 0);
  FillChar(VPMoins, sizeof(VPMoins), 0);
  FillChar(VIMoins, sizeof(VIMoins), 0);
end;

procedure TEvalCombat.Finalise;
begin
  ResolCombats;
  ResolDeplacements;
  ResolReste;
  ResolFinale;
end;

Procedure TEvalCombat.EvalueOrdre;
begin
  Case (O.StypeO mod 128) of
    1, 2, 4: VPTire;
    3: VITire;
    5 .. 10: FlotteTire;
    11: FlotteBouge;
  end;
end;

end.
