unit avateval;

INTERFACE

uses
  Avatunit,
  Classes,
  LectOrdres,
  NebData,
  ordreslist,
  eval.etape;

function EvalTourData(theData: TNebData; Ordres: TStringList;
  EtapeStop: integer; Simul: boolean): TNebData;

Type


  TNebEval = class(TObject)
  private
    EtapeStop: integer;
    Ordres: TStringList;
    Data, OldData: TNebData;
    Simul: Boolean;
    MPDech: PTab;
    ListeOrdres: TListeOrdres;
    Embuscade: PInfoMonde; // eval.declaration, eval.combat
    Contact: TTabContacts;
    Espions: TTabEspions;


  private
    procedure EvalueOrdres;
    procedure SauvePartie;
    procedure LectureDesOrdres(NoJouDef: integer);
    procedure InitEvalEtape(evalEtape: TEvalEtape);

  public
    constructor Create(theData: TNebData; Ordres: TStringList;
      EtapeStop: integer; Simul: boolean); overload;
    constructor Create(NomPart: String; Tour: integer; RepFic: String;
      Simul: boolean); overload;
    destructor Destroy; override;
    function EvalueTour: TNebData;

  end;

IMPLEMENTATION

uses
  Utils,
  SySUtils,
  Eval.Init,
  Eval.Emigration,
  Eval.Combat,
  Eval.APIPoints,
  Eval.Special,
  Eval.Declaration,
  Eval.Sondage,
  Eval.Construction,
  Eval.LimitePop,
  Eval.ConstructionInd,
  Eval.TransfoVIInd,
  Eval.DechargeMP,
  Eval.DechargePC,
  Eval.DechargeTresor,
  Eval.Transfert,
  Eval.ChargeTresorExplo,
  Eval.ChargeMP,
  Eval.Cadeau,
  Eval.ConstructionBombe,
  Eval.ActionChose,
  Eval.Conversion,
  Eval.Capture,
  Eval.Pillage,
  Eval.Piratage,
  Eval.MiseAuPoint,
  Eval.Points,
  ListInt;

constructor TNebEval.Create(NomPart: String; Tour: integer; RepFic: String;
  Simul: boolean);
var
  theData: TNebData;
begin
  theData := TNebData.Create(RepFic + '\' + NomPart, Tour - 1);
  Create(theData, nil, 0, Simul);
  theData.Free;
end;

constructor TNebEval.Create(theData: TNebData; Ordres: TStringList;
  EtapeStop: integer; Simul: boolean);
var
  i: integer;
begin
  inherited Create;

  OldData := TNebData.Create('', 0);
  OldData.Assign(theData); // OldData = infos du tour pr�c�dent

  if Assigned(Data) then
    Data.Free;
  Data := TNebData.Create('', 0); // Data : infos du nouveau tour
  Data.Assign(OldData);

  self.Ordres := TStringList.Create;
  if Assigned(Ordres) then
  begin
    self.Ordres.Assign(Ordres);
  end;

  ListeOrdres := TListeOrdres.Create;
  Self.Simul := Simul;

  FillChar(Espions, sizeof(Espions), 0);
  Embuscade := New(PInfoMonde);
  MPDech := New(PTab);
  for i := 1 to 1024 do
    GetMem(MPDech^[i], sizeof(TLigne));

end;

destructor TNebEval.Destroy;
var
  i: integer;
begin
  for i := 1 to 1024 do
    FreeMem(MPDech^[i], sizeof(TLigne));
  Dispose(MPDech);

  Dispose(Embuscade);
  FreeAndNil(Data);
  FreeAndNil(OldData);
  FreeAndNil(Ordres);
  FreeAndNil(ListeOrdres);

  inherited Destroy;
end;

function TNebEval.EvalueTour: TNebData;
begin
  // Truc : mettre Seed=0 dans fic nbt ou nba pour avoir toujours la m�me s�quence
  // de nombre al�atoires. Utile pour les tests de non r�gression

  Data.LogEval.Add(Format('�valuation N�bula, partie %s, tour %d', [Data.NomPartie, Data.Tour + 1]));
  Data.LogEval.Add('');

  if Data.Seed < 0 then
    Randomize
  else
    RandSeed := Data.Seed;

  LectureDesOrdres(Data.NoJou);
  EvalueOrdres;
  SauvePartie;

  Result := TNebData.Create('', 0);
  Result.Assign(Data);
end;


procedure TNebEval.InitEvalEtape(evalEtape: TEvalEtape);
begin
    evalEtape.Simul := Simul;
    evalEtape.MPDech := MPDech;
    evalEtape.Embuscade := Embuscade;
    evalEtape.Contact := @Contact;
    evalEtape.Espions := @Espions;
end;

Procedure TNebEval.EvalueOrdres;
var
  etape: TEvalEtape;
  i: Integer;
const
  Etapes : array[0..25] of TEvalEtapeClass =
  (
    TEvalInit,
    TEvalOrdresSpeciaux,
    TEvalDeclaration,
    TEvalSondage,
    TEvalConstruction,
    TEvalLimitePopulation,
    TEvalEmigration,
    TEvalContructionIndustrie,
    TEvalTransfoVIInd,
    TEvalDechargeMP,
    TEvalDechargePC,
    TEvalDechargeTresor,
    TEvalTransfert,
    TEvalChargTresEtExplo,
    TEvalChargeMP,
    TEvalNoteCadeauxFlottes,
    TEvalCombat,
    TEvalConstructionBombe,
    TEvalActionChose,
    TEvalConversion,
    TEvalCapture,
    TEvalPillage,
    TEvalPiratage,
    TEvalCadeau,
    TEvalMiseAuPoint,
    TEvalPoints
  );
begin

  If EtapeStop = 0 then
    EtapeStop := 100;

  for i := Low(Etapes) to High(Etapes) do
  begin
    etape := Etapes[i].Create(Data, OldData, ListeOrdres);

    if EtapeStop < etape.EtapeEval then
    begin
      etape.Free;
      Exit;
    end;

    InitEvalEtape(etape);
    etape.EvalueOrdres;
    etape.Free;
  end;

end;

Procedure TNebEval.SauvePartie;
begin
  Data.AssignOrdres(ListeOrdres);

  If not Simul then
    Data.SauveEnNBT(0, '');
end;

Procedure TNebEval.LectureDesOrdres(NoJouDef: integer);
Var
  jn, i: integer;
  O: TOrdre;
  St: String;
  NebOrdres: TNebOrdres;
  OrdresFileName: String;

begin
  NebOrdres := TNebOrdres.Create(Data);

  ListeOrdres.Clear;
  Data.FreeOrdres;

  // Si on n'a pas d�j� des ordres
  if Ordres.Count = 0 then
  begin
    // Lecture du fichier des ordres
    OrdresFileName := Data.GetNomFicOrdres;
    if FileExists(OrdresFileName) then
      Ordres.LoadFromFile(OrdresFileName);
  end;

  // Remplissage de la liste des ordres
  jn := NoJouDef;
  for i := 0 to Ordres.Count - 1 do
  begin
    St := Ordres[i];
    if St.Length > 0 then
    begin
      begin
        NebOrdres.Traite(St, O, jn);
        if O.TypeO >= 0 then
        begin
          ListeOrdres.Add(O);
        end;
      end;
    end;
  end;

  NebOrdres.Free;
end;

function EvalTourData(theData: TNebData; Ordres: TStringList;
  EtapeStop: integer; Simul: boolean): TNebData;
var
  NebEval: TNebEval;
begin
  NebEval := TNebEval.Create(theData, Ordres, EtapeStop, Simul);
  Result := NebEval.EvalueTour;
  NebEval.Free;
end;

end.
