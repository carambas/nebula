unit eval.etape;

interface

uses
  Avatunit,
  nebdata,
  ordreslist,
  Eval.APIPoints;


type
  TEvalEtapeClass = class of TEvalEtape;

  PTabContacts = ^TTabContacts;
  TTabContacts = array [1 .. NB_JOU_MAX] of longint;

  PTabEspions = ^TTabEspions;
  TTabEspions = array [1 .. NB_JOU_MAX, 1 .. NB_STATS] of integer;

  TEvalEtape = class(TObject)
  public
    Points: TAPIPoints;

    constructor Create(Data, OldData: TNebData; Ordres: TListeOrdres); virtual;
    destructor Destroy; override;
    procedure Init; virtual; abstract;
    procedure Finalise; virtual; abstract;
    procedure EvalueOrdres;
    procedure EcritLog(St: string);

  public
    const TYPE_ORDRE = -1;
    const ETAPE_EVAL = -1;

  protected
    Data, OldData: TNebData;
    OK: boolean;
    O: TOrdre;
    ListeOrdres: TListeOrdres;

  public
    Simul: boolean;
    MPDech: PTab;
    Embuscade: PInfoMonde;
    Contact: PTabContacts;
    Espions: PTabEspions;


  protected
    procedure EvalueOrdre; virtual; abstract;
    Procedure Erreur(Err: integer);
    function GetTypeOrdre: Integer; virtual;
    function GetEtapeEval: Integer; virtual;

  public
    property TypeOrdre: Integer read GetTypeOrdre;
    property EtapeEval: Integer read GetEtapeEval;
  end;

implementation

uses SysUtils;

{ TEvalEtape }

constructor TEvalEtape.Create(Data, OldData: TNebData; Ordres: TListeOrdres);
begin
  Points := TAPIPoints.Create(Data);;
  Self.Data := Data;
  Self.OldData := OldData;
  ListeOrdres := Ordres;
end;


destructor TEvalEtape.Destroy;
begin
  Points.Free;

  inherited;
end;

procedure TEvalEtape.EcritLog(St: string);
begin
  Data.LogEval.Add(St);
end;

procedure TEvalEtape.Erreur(Err: integer);
begin
  O.Erreur := Err;
  OK := False;
end;

procedure TEvalEtape.EvalueOrdres;
var
  i: Integer;
begin
  Init;

  if TypeOrdre >= 0 then
    for i := 0 to ListeOrdres.Count(TypeOrdre) - 1 do
    begin
      O := ListeOrdres[TypeOrdre, i];
      OK := True;
      EvalueOrdre;
      ListeOrdres[TypeOrdre, i] := O;
    end;

  Finalise;

end;

function TEvalEtape.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalEtape.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;


end.

