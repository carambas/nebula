unit Eval.Sondage;

Interface

uses Avateval, Avatunit, eval.etape;

type
  TEvalSondage = class(TEvalEtape)
  public
    const TYPE_ORDRE = 2;
    const ETAPE_EVAL = 2;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure VISonde(md1,md2 : word);
    procedure VPSonde(md1,md2 : word);
    procedure FSonde(fl,md : word);
  end;

Implementation

uses SysUtils;

procedure TEvalSondage.VISonde(md1,md2 : word);
begin
   if Data.M[md1].Proprio<>O.Auteur then Erreur(4)
   else if not Data.Connect(md1,md2) then Erreur(6)
   else if Data.M[md1].VI=0 then Erreur(7)
   else
   begin
      Data.DevientConnu(md2,O.Auteur);
      Dec(Data.M[md1].VI);
   end;
end;

procedure TEvalSondage.VPSonde(md1,md2 : word);
begin
   if Data.M[md1].Proprio<>O.Auteur then Erreur(4)
   else if not Data.Connect(md1,md2) then Erreur(6)
   else if Data.M[md1].VP=0 then Erreur(8)
   else
   begin
      Data.DevientConnu(md2,O.Auteur);
      Dec(Data.M[md1].VP);
   end;
end;

procedure TEvalSondage.Finalise;
begin
  inherited;

end;

procedure TEvalSondage.FSonde(fl,md : word);
begin
   if Data.F[fl].Proprio<>O.Auteur then Erreur(5)
   else if not Data.Connect(Data.F[fl].Localisation,md) then Erreur(6)
   else if Data.F[fl].NbVC=0 then Erreur(10)
   else
   begin
      Data.DevientConnu(md,O.Auteur);
      Dec(Data.F[fl].NbVC);
   end;
end;

function TEvalSondage.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalSondage.GetTypeOrdre: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

procedure TEvalSondage.Init;
begin
  inherited;

end;

Procedure TEvalSondage.EvalueOrdre;

begin
  OK:=True;
  Case O.STypeO of
     1 : VISonde(O.O[1],O.O[2]);
     2 : VPSonde(O.O[1],O.O[2]);
     3 : FSonde(O.O[1],O.O[2]);
  end;
end;

end.
