unit Eval.Piratage;

Interface

uses
  Avateval,eval.etape;

type
  TEvalPiratage = class(TEvalEtape)
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = 19;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;
  end;

Implementation

uses Avatunit;

procedure TEvalPiratage.EvalueOrdre;
begin
  inherited;

end;

Procedure TEvalPiratage.Finalise;
var
  md, fl, j: word;
  TV: word;
  VC: array [0 .. NB_JOU_MAX] of word;
  VT: array [0 .. NB_JOU_MAX] of word;
begin
  with Data do
    for md := 1 to NbMonde do
    begin
      TV := 0;
      FillChar(VC, sizeof(VC), 0);
      FillChar(VT, sizeof(VT), 0);

      for fl := 1 to NbFlotte do
        if ((F^[fl].Localisation = md) and (not estEnPaix(fl))) then
        begin
          Inc(TV, F^[fl].NbVC + F^[fl].NbVT);
          Inc(VC[F^[fl].Proprio], F^[fl].NbVC);
          Inc(VT[F^[fl].Proprio], F^[fl].NbVT);
        end;

      Inc(TV, M^[md].VI + M^[md].VP);
      Inc(VT[M^[md].Proprio], M^[md].VI + M^[md].VP);
      for j := 1 to NbJou do
        if (((jou[j].Classe = Pirate) and (VC[j] >= 3 * (TV - VC[j] - VT[j])) or
          ((PiratageMultiClasse) and (VC[j] >= 20 * (TV - VC[j] - VT[j]))))
          and (VC[j] > 0) and (TV > 0)) then
          for fl := 1 to NbFlotte do
            if ((F^[fl].Localisation = md) and (not EstAllie(j, F^[fl].Proprio))
              and (F^[fl].Proprio <> j)) then
            begin
              F^[fl].Proprio := j;
              FE^[fl].Statut := (FE^[fl].Statut or Puis2(1));
              F^[fl].Statut := (F^[fl].Statut and (not Puis2(0)));
              inc(F^[fl].N, F^[fl].C);
              F^[fl].C := 0;
            end;
    end;
end;

function TEvalPiratage.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalPiratage.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalPiratage.Init;
begin
  inherited;

end;

end.
