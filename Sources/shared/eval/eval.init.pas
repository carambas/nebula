unit eval.init;

interface

uses Eval.Etape;

type

  // Etape d'initialisation des donn�es en d�but d'�valuation
  TEvalInit = class(TEvalEtape)
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = -1;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  end;


implementation

uses Avatunit;

{ TEvalDechargeMP }

procedure TEvalInit.EvalueOrdre;
begin
  inherited;

end;

procedure TEvalInit.Finalise;
begin
  inherited;

end;

function TEvalInit.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalInit.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalInit.Init;
Var
  i, j, x, xc, fl: integer;
  indl: integer;
begin
  Inherited;

  with Data do
  begin
    NouveauTour; // Tour +1 et mise � Z�ro de Evt

    for i := 1 to NbMonde do
    begin
      // Trou noirs
      if TN[i] then
        with M[i] do
        begin
          Robot := 0;
          Conv := 0;
          Robot := 0;
          Proprio := 0;
          ProprConv := 0;
        end;

      // Initialisations du monde pour rattraper d'anciens bugs
      with ME[i] do
      begin
        // Correction d'un bug : Indlibres mal calcul�es au tour z�ro
        // dans ce cas on recalcule les industries actives
        If Tour = 1 then
        begin
          indl := M[i].Ind;
          if PopLibre < indl then
            indl := PopLibre;
          if M[i].MP < indl then
            indl := M[i].MP;
          IndLibre := indl;
        end;
      end;

      Embuscade^[i] := 0;
    end;

    // Augmentation du potentiel d'exploration
    for j := 1 to NbJou do
      if Jou[j].md > 0 then
      begin
        Inc(M[Jou[j].md].PotExplo, 100);
      end;

    for i := 1 to NbTres do
      if T[i].Localisation > 0 then
      begin
        if T[i].Statut = 0 then
          T[i].Proprio := M[T[i].Localisation].Proprio
        else
          T[i].Proprio := F[T[i].Localisation].Proprio;
      end;
    for fl := 1 to NbFlotte do
      DevientConnu(F[fl].Localisation, F[fl].Proprio);
    for i := 1 to NB_JOU_MAX do
    begin
      Contact[i] := 0;
      FillChar(DetailScores^, sizeof(DetailScores^), 0);
    end;
    for i := 1 to NbMonde do
      if M[i].Pop > M[i].MaxPop then
      begin
        x := 0;
        for j := M[i].MaxPop + 1 to M[i].Pop do
          if Random(2) = 0 then
            Inc(x);
        xc := M[i].Conv * x div M[i].Pop;
        Dec(M[i].Pop, x);
        Dec(M[i].Conv, xc);
        Inc(ME^[i].PopMoins, x);
        Inc(ME^[i].ConvMoins, xc);
      end;
  end;
end;

end.

