unit Eval.ChargeMP;

Interface

uses Avateval,eval.etape;

type

  TEvalChargeMP = class(TEvalEtape)
  public const
    TYPE_ORDRE = 12;
    ETAPE_EVAL = 13;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    DemandeMPFlotte : array[1..1024] of integer;

    procedure ChargeLaCargaison(fl, x: word);
    procedure ComptePointsMPDech;
    procedure ResolChargeMP;
  end;

Implementation

uses Utils, ListInt, erreurseval, Eval.APIPoints;

procedure TEvalChargeMP.ComptePointsMPDech;
var
  i: word;
begin
  for i := 1 to Data.NbMonde do
  begin
    Points.PtsMPDech(i, MPDech^[i]);
  end;
end;

procedure TEvalChargeMP.ChargeLaCargaison(fl: word; x: word);
var
  md, x1, x2: word;
  Pmd, Pfl, Pconv: word;
begin
  with Data do
  begin
    md := F^[fl].Localisation;
    Pmd := M^[md].Proprio;
    Pconv := M^[md].ProprConv;
    Pfl := F^[fl].Proprio;
    // Quand on n'est pas proprio des convertis : C P -> C N.
    if (Pfl <> Pconv) and (O.StypeO = 4) then
      O.StypeO := 1;

    if x > CapaciteFlotte(fl) - ContenuFlotte(fl) - DemandeMPFlotte[fl] then
    begin
      Erreur(ERR_PAS_ASSEZ_VAISSEAUX);
      x := CapaciteFlotte(fl) - ContenuFlotte(fl) - DemandeMPFlotte[fl];
      OK := True;
    end;

    if O.Auteur <> Pfl then
      Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if ((O.StypeO > 0) and (Pfl <> Pmd) and not EstChargeurDePop(Pmd, Pfl))
    then
      Erreur(ERR_NON_CHARGEUR_POP) // Pop et pas proprio monde ni chargeur pop
    else if ((O.StypeO = 2) and (Pfl <> Pconv)) then
      Erreur(ERR_PAS_PROPRIO_CONV)
    else if ((O.StypeO = 0) and (Pfl <> Pmd) and (not EstChargeur(Pmd, Pfl)))
    then
      Erreur(ERR_PAS_CHARGEUR) // MP mais pas proprio ni chargeur
    else if (M^[md].MP < x) and (O.StypeO = 0) then
    begin
      Erreur(ERR_PAS_ASSEZ_MP);
      x := M^[md].MP;
      OK := True;
    end
    else if (M^[md].Pop - M^[md].Conv < x) and (O.StypeO = 1) then
    begin
      Erreur(ERR_PAS_ASSEZ_POP);
      x := M^[md].Pop - M^[md].Conv;
      OK := True;
    end
    else if (M^[md].Conv < x) and (O.StypeO = 2) then
    begin
      Erreur(ERR_PAS_ASSEZ_CONV);
      x := M^[md].Conv;
      OK := True;
    end
    else if (M^[md].Robot < x) and (O.StypeO = 3) then
    begin
      Erreur(ERR_PAS_ASSEZ_ROB);
      x := M^[md].Robot;
      OK := True;
    end
    else if (M^[md].Pop < x) and (O.StypeO = 4) then
    begin
      Erreur(ERR_PAS_ASSEZ_ROB);
      x := M^[md].Pop;
      OK := True;
    end;

    if OK then
    begin
      case O.StypeO of
        0:
          begin
            if x > M^[md].MP then
            begin
              Erreur(ERR_PAS_ASSEZ_MP);
              x := M^[md].MP;
              OK := True;
            end;
            // On d�clare juste ce que la flotte veut charger
            Inc(DemandeMPFlotte[fl], x);
          end;
        1:
          begin
            if x > M^[md].Pop then
            begin
              Erreur(ERR_PAS_ASSEZ_POP);
              x := M^[md].Pop;
              OK := True;
            end;
            Inc(F^[fl].N, x);
            Dec(M^[md].Pop, x);
          end;
        2:
          begin
            if x > M^[md].Conv then
            begin
              Erreur(ERR_PAS_ASSEZ_CONV);
              x := M^[md].Conv;
              OK := True;
            end;
            Inc(F^[fl].C, x);
            Dec(M^[md].Conv, x);
            Dec(M^[md].Pop, x);
          end;
        3:
          begin
            if x > M^[md].Robot then
            begin
              Erreur(ERR_PAS_ASSEZ_ROB);
              x := M^[md].Robot;
              OK := True;
            end;
            Inc(F^[fl].R, x);
            Dec(M^[md].Robot, x);
          end;
        4:
          if M^[md].Pop > 0 then
          begin
            if x > M^[md].Pop then
            begin
              Erreur(ERR_PAS_ASSEZ_POP);
              x := M^[md].Pop;
              OK := True;
            end;
            x1 := (M^[md].Conv * x) div M^[md].Pop;
            x2 := x - x1;
            Inc(F^[fl].C, x1);
            Dec(M^[md].Conv, x1);
            Inc(F^[fl].N, x2);
            Dec(M^[md].Pop, x);
          end;
      end;
    end;
  end;
end;

procedure TEvalChargeMP.ResolChargeMP;
var
  fl, md: integer;
  DemandeMP: array [1 .. 32, 1 .. 1024] of integer;
  ListeNoFLotte, ListeDemande, ListeResultat: TIntList;
begin
  FillChar(DemandeMP, sizeof(DemandeMP), 0);

  // On recence les chargements de MP
  for fl := 1 to Data.NbFlotte do
    if (DemandeMPFlotte[fl] > 0) and (Data.F[fl].Proprio > 0) then
      Inc(DemandeMP[Data.F[fl].Proprio, DemandeMPFlotte[fl]]);

  // On laisse les propri�taires de monde charger
  for fl := 1 to Data.NbFlotte do
    if (DemandeMPFlotte[fl] > 0) and
      (Data.F[fl].Proprio = Data.M[Data.F[fl].Localisation].Proprio) then
    begin
      md := Data.F[fl].Localisation;
      if DemandeMPFlotte[fl] > Data.M[md].MP then
      begin
        DemandeMPFlotte[fl] := Data.M[md].MP;
      end;
      Inc(Data.F[fl].MP, DemandeMPFlotte[fl]);
      Dec(Data.M[md].MP, DemandeMPFlotte[fl]);
      DemandeMPFlotte[fl] := 0;
    end;

  // On s'occupe des autres joueurs

  ListeNoFLotte := TIntList.Create;
  ListeDemande := TIntList.Create;
  ListeResultat := TIntList.Create;

  for md := 1 to Data.NbMonde do
  begin
    ListeNoFLotte.Clear;
    ListeDemande.Clear;
    ListeResultat.Clear;

    // On recence les chargements de MP du monde dans les listes
    for fl := 1 to Data.NbFlotte do
      if (Data.F[fl].Localisation = md) and (DemandeMPFlotte[fl] > 0) then
      begin
        ListeNoFLotte.Add(fl);
        ListeDemande.Add(DemandeMPFlotte[fl]);
        ListeResultat.Add(0);
      end;

    // On r�soud les conflits
    RepartitionRessources(ListeDemande, ListeResultat, Data.M[md].MP);

    // On effectue les chargements
    for fl := 0 to ListeNoFLotte.Count - 1 do
    begin
      Inc(Data.F[ListeNoFLotte[fl]].MP, ListeResultat[fl]);
      Dec(Data.M[md].MP, ListeResultat[fl]);
      MPDech^[Data.F[ListeNoFLotte[fl]].Localisation]^
        [Data.F[ListeNoFLotte[fl]].Proprio] :=
        MPDech^[Data.F[ListeNoFLotte[fl]].Localisation]^
        [Data.F[ListeNoFLotte[fl]].Proprio] - ListeResultat[fl];
    end;
  end;

  ListeNoFLotte.Free;
  ListeDemande.Free;
  ListeResultat.Free;
end;

procedure TEvalChargeMP.EvalueOrdre;
var
  n: word;

begin
  with Data do
  begin
    n := 0;
    if (F[O.O[1]].Proprio <> O.Auteur) then
      Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else
    begin
      if O.O[2] = 0 then
      begin
        case O.StypeO of
          0: n := M^[F^[O.O[1]].Localisation].MP;
          1: n := M^[F^[O.O[1]].Localisation].Pop - M^[F^[O.O[1]].Localisation].Conv;
          2: n := M^[F^[O.O[1]].Localisation].Conv;
          3: n := M^[F^[O.O[1]].Localisation].Robot;
          4: n := M^[F^[O.O[1]].Localisation].Pop;
        end;
        if n > (CapaciteFlotte(O.O[1]) - ContenuFlotte(O.O[1]) - DemandeMPFlotte[O.O[1]]) then
          n := CapaciteFlotte(O.O[1]) - ContenuFlotte(O.O[1]) - DemandeMPFlotte[O.O[1]];
      end
      else
        n := O.O[2];
      ChargeLaCargaison(O.O[1], n);
    end;
  end;
end;

procedure TEvalChargeMP.Finalise;
begin
  ResolChargeMP;
  ComptePointsMPDech;
end;

function TEvalChargeMP.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalChargeMP.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalChargeMP.Init;
begin
  FillChar(DemandeMPFlotte, sizeof(DemandeMPFlotte), 0);
end;

end.
