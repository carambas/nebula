unit Eval.Special;

interface

uses
  AvatUnit,
  AvatEval,
  nebdata,
  Classes,
  ordreslist,
  eval.etape;

type
  TEvalOrdresSpeciaux = class(TEvalEtape)
  public
    const TYPE_ORDRE = 0;
    const ETAPE_EVAL = 0;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure ActionBot(Auteur, NumBot : integer; ListeOrdres: TListeOrdres);
  private
  end;

type
  TBot = class(TObject)
  private
    NumJou : integer;
    OrdresBot : TStringList;
    Data : TNebData;
    ListeOrdres: TListeOrdres;

    procedure SupprimeOrdres;
    procedure AjouteOrdres;
    procedure GenereOrdres; virtual; abstract; // M�thode � impl�menter pour chaque bot
  public
    constructor Create(theData : TNebData; NoJou : integer; ListeOrdres: TListeOrdres);
    destructor Destroy; override;
    procedure Action;
  end;

  TBotTest = class(TBot)
  private
    procedure GenereOrdres; override;
  end;

  TBotMarvin = class(TBot)
  private
    procedure GenereOrdres; override;
    function isEnnemiPresent(monde : integer) : boolean;
    function RechercheCible(monde : integer; estmonde : boolean) : string;
    function getRandomMondeVoisin(monde : integer) : integer;
  end;

implementation

uses
  SysUtils, Utils, eval.APIPoints, erreurseval, lectordres;

// --- classe TBot ----------------------

constructor TBot.Create(theData : TNebData; NoJou : integer; ListeOrdres: TListeOrdres);
begin
  OrdresBot := TStringList.Create;
  Self.ListeOrdres := ListeOrdres;
  Data := theData;
  NumJou := NoJou;
end;

destructor TBot.Destroy;
begin
  OrdresBot.Free;
end;


procedure TBot.Action;
begin
  SupprimeOrdres;
  OrdresBot.Clear;
  GenereOrdres;
  AjouteOrdres;
end;


// SupprimeOrdres : supprime tous les ordres du joueur j
// sauf ceux de l'�tape 0
procedure TBot.SupprimeOrdres;
begin
  ListeOrdres.RemoveOrdresJoueur(NumJou);
end;

procedure TBot.AjouteOrdres;
var
  i : integer;
  jn : integer;
  St : string;
  O : TOrdre;
  NebOrdres : TNebOrdres;
begin
  NebOrdres := TNebOrdres.Create(Data);

  // Remplissage de la liste des ordres
  jn := NumJou;
  for i := 0 to OrdresBot.Count - 1 do
  begin
    St := OrdresBot[i];
    if St.Length > 0 then
    begin
      begin
        NebOrdres.Traite(St, O, jn);
        if O.TypeO > 0 then // > 0 car on ne g�re pas les ordres sp�ciaux ici
        begin
          ListeOrdres.Add(O);
        end;
      end;
    end;
  end;

  NebOrdres.Free;

end;

// --- Fin classe TBot ----------------------


// --- Classe TBotMarvin --------------------

procedure TBotMarvin.GenereOrdres;
var
  m, f : integer;
begin
  // On bloucle sur les mondes poss�d�s
  for m := 1 to Data.NbMonde do
  begin
    if (Data.M[m].Proprio = NumJou) then
    begin
      // On construit des VI avec les industries actives
      if Data.ME[m].IndLibre > 0 then
        OrdresBot.Add(Format('M %d C %d VI', [m, Data.ME[m].IndLibre]));

      // Si un ennemi est pr�sent, on tire avec les VI
      if isEnnemiPresent(m) and (Data.M[m].VI > 0) then
        OrdresBot.Add(Format('VI M %d * %s', [m, RechercheCible(m, True)]));

      // Si un ennemi est pr�sent, on tire avec les VP
      if isEnnemiPresent(m) and (Data.M[m].VP > 0) then
        OrdresBot.Add(Format('VP M %d * %s', [m, RechercheCible(m, True)]));
    end;
  end;

  // On boucle sur les flottes poss�d�es
  for f := 1 to Data.NbFlotte do
  begin
    if Data.F[f].Proprio = NumJou then
    begin
      if isEnnemiPresent(Data.F[f].Localisation) then
      begin
        // On tire
        OrdresBot.Add(Format('F %d * %s', [f, RechercheCible(Data.F[f].Localisation, False)]));
      end
      else
      begin
        // On se d�place si la flotte n'est pas vide
        if Data.F[f].NbVC + Data.F[f].NbVT > 0 then 
          OrdresBot.Add(Format('F %d M %d', [f, getRandomMondeVoisin(Data.F[f].Localisation)]));
      end;
    end;
  end;
end;

function TBotMarvin.isEnnemiPresent(monde : integer) : boolean;
var
  f : integer;
begin
  Result := False;

  if (Data.M[monde].Proprio <> NumJou) 
    and (Data.M[monde].VI + Data.M[monde].VP > 0) then
  begin
    Result := True;
    Exit;
  end;

  for f := 1 to Data.NbFlotte do
  begin
    if (Data.F[f].Localisation = monde) and (Data.F[f].Proprio <> NumJou)
      and (Data.F[f].NbVC + Data.F[f].NbVT > 0) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TBotMarvin.RechercheCible(monde : integer; estmonde : boolean) : string;
var
  f : integer;
  ListeCibles : TStringList;
begin
  Result := '';
  ListeCibles := TStringList.Create;

  if (Data.M[monde].Proprio <> NumJou)
    and (Data.M[monde].VI + Data.M[monde].VP > 0) then
  begin
    ListeCibles.Add('M');
    if (Data.M[monde].Ind > 0) and (not estmonde)
      and (Data.M[monde].Proprio > 0)then
      ListeCibles.Add('I');
  end;

  for f := 1 to Data.NbFlotte do
    if (Data.F[f].Localisation = monde) and (Data.F[f].Proprio <> NumJou)
      and (Data.F[f].NbVC + Data.F[f].NbVT > 0) then
    begin
      ListeCibles.Add('F ' + IntToStr(f));
    end;

  if ListeCibles.Count > 0 then
    Result := ListeCibles[Random(ListeCibles.Count)];

  ListeCibles.Free;
end;

function TBotMarvin.getRandomMondeVoisin(monde : integer) : integer;
var
  connect : array[1..8] of integer;
  i, n : integer;
begin
  n := 0;
  for i := 1 to 8 do
    if Data.M[monde].Connect[i] > 0 then
    begin
      Inc(n);
      connect[n] := Data.M[monde].Connect[i];
    end;

  Result := connect[Random(n) + 1]; 
end;
// --- Fin classe TBotMarvib ----------------------


// --- Classe TBotTest --------------------
procedure TBotTest.GenereOrdres;
begin
  OrdresBot.Add('Num�ro de bot non d�fini');
end;

// Appelle une classe de bot diff�rente en fonction du num�ro de bot
procedure TEvalOrdresSpeciaux.ActionBot(Auteur, NumBot : integer; ListeOrdres: TListeOrdres);
var
  bot : TBot;
begin
  case NumBot of
    1 : bot := TBotMarvin.Create(Data, Auteur, ListeOrdres);
  else
    bot := TBotTest.Create(Data, Auteur, ListeOrdres);
  end;

  bot.Action;
  bot.Free;
end;

procedure TEvalOrdresSpeciaux.EvalueOrdre;
begin
  Case O.STypeO of
    1 : ActionBot(O.Auteur, O.O[1], ListeOrdres);
  end;
end;

procedure TEvalOrdresSpeciaux.Finalise;
begin
  inherited;

end;

function TEvalOrdresSpeciaux.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalOrdresSpeciaux.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalOrdresSpeciaux.Init;
begin
  inherited;

end;

end.
