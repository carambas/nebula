unit Eval.Declaration;

Interface

uses
  Avateval,
  Avatunit,
  erreurseval,
  eval.etape;

type
  TEvalDeclaration = class(TEvalEtape)
  public
    const TYPE_ORDRE = 1;
    const ETAPE_EVAL = 1;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure Dezactive(md : word);
    procedure DeclareAllie(j1,j2 : word);
    procedure DeclareEnnemi(j1,j2 : word);
    procedure DeclareChargeur(j1,j2 : word);
    procedure DeclareNonChargeur(j1,j2 : word);
    procedure DeclarePilleur(j1,j2 : word);
    procedure DeclareNonPilleur(j1,j2 : word);
    procedure DeclareJihad(j1,j2 : word);
    procedure Paix(fl : word);
    procedure Guerre(fl : word);
    procedure DeclareChargeurPop(j1,j2 : word);
    procedure DeclareNonChargeurPop(j1,j2 : word);
    procedure DeclareDechargeurPop(j1,j2 : word);
    procedure DeclareNonDechargeurPop(j1,j2 : word);
  end;



Implementation

procedure TEvalDeclaration.Dezactive(md : word);
var i : integer;
begin
   if md>0 then Embuscade^[md]:=(Embuscade^[md] or Puis2(O.Auteur-1))
   else for i:=1 to Data.NbMonde do Embuscade^[i]:=(Embuscade^[i] or
      Puis2(O.Auteur-1));
end;

procedure TEvalDeclaration.DeclareAllie(j1,j2 : word);
begin
  if not Data.ConnuDeNom[j1, j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
  else Data.DeclareAllie(j1, j2);
end;

procedure TEvalDeclaration.DeclareEnnemi(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1, j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareEnnemi(j1, j2);
end;

procedure TEvalDeclaration.DeclareChargeur(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareChargeur(j1, j2);
end;

procedure TEvalDeclaration.DeclareNonChargeur(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareNonChargeur(j1,j2);
end;

procedure TEvalDeclaration.DeclarePilleur(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclarePilleur(j1, j2);
end;

procedure TEvalDeclaration.DeclareNonPilleur(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareNonPilleur(j1,j2);
end;

procedure TEvalDeclaration.DeclareJihad(j1,j2 : word);
begin
   if Data.Jou[j1].Classe<>Missionnaire then Erreur(2)
   else if j1=j2 then Erreur(9)
   else if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else if (Data.Jou[j1].Jihad > 0) and
           (Data.Tour < Data.Jou[j1].TourChgtJihad + 5) then
             Erreur(3)
   else
   begin
      Data.DeclareJihad(j1,j2);
   end;
end;

procedure TEvalDeclaration.Paix(fl : word);
begin
   if O.Auteur<> Data.F^[fl].Proprio then Erreur(5)
   else Data.Paix[fl] := True;
end;

function TEvalDeclaration.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalDeclaration.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalDeclaration.Guerre(fl : word);
begin
   if O.Auteur <> Data.F^[fl].Proprio then Erreur(5)
   else Data.Paix[fl] := False;
end;

procedure TEvalDeclaration.DeclareChargeurPop(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareChargeurPop(j1, j2);
end;

procedure TEvalDeclaration.DeclareNonChargeurPop(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareNonChargeurPop(j1,j2);
end;

procedure TEvalDeclaration.DeclareDechargeurPop(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareDechargeurPop(j1,j2);
end;

procedure TEvalDeclaration.DeclareNonDechargeurPop(j1,j2 : word);
begin
   if not Data.ConnuDeNom[j1,j2] then Erreur(ERR_CONNAIT_PAS_DE_NOM)
   else Data.DeclareNonDechargeurPop(j1,j2);
end;

procedure TEvalDeclaration.EvalueOrdre;
begin
  OK := True;
  if (Data.Jou[O.Auteur].Equipe.Count = 1) then
  begin
    if (O.StypeO <> 5) and Data.DiploInterdite then
      Erreur(ERR_ORDRE_INTERDIT);
  end
  else
  begin
    if (not O.StypeO in [5, 7, 8]) and Data.DiploInterdite then
      Erreur(ERR_ORDRE_INTERDIT);
  end;
  if OK then
    Case O.StypeO of
       1  : DeclareAllie(O.Auteur,O.O[1]);
       2  : DeclareEnnemi(O.Auteur,O.O[1]);
       3  : DeclareChargeur(O.Auteur,O.O[1]);
       4  : DeclareNonChargeur(O.Auteur,O.O[1]);
       5  : DeclareJihad(O.Auteur,O.O[1]);
       6  : Dezactive(O.O[1]);
       7  : Paix(O.O[1]);
       8  : Guerre(O.O[1]);
       9  : DeclareChargeurPop(O.Auteur, O.O[1]);
       10 : DeclareNonChargeurPop(O.Auteur, O.O[1]);
       11 : DeclareDechargeurPop(O.Auteur, O.O[1]);
       12 : DeclareNonDechargeurPop(O.Auteur, O.O[1]);
       13 : DeclarePilleur(O.Auteur, O.O[1]);
       14 : DeclareNonPilleur(O.Auteur, O.O[1]);
    end;
end;


procedure TEvalDeclaration.Finalise;
begin
  inherited;
  //
end;

{ TEvalDeclarations }
procedure TEvalDeclaration.Init;
begin
  inherited;
end;


end.
