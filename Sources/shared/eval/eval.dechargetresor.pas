unit Eval.DechargeTresor;

Interface

uses Avateval, eval.etape;

type

  TEvalDechargeTresor = class(TEvalEtape)
  public const
    TYPE_ORDRE = 10;
    ETAPE_EVAL = 10;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure DechLeTresor(tr: word);
  end;

Implementation


procedure TEvalDechargeTresor.DechLeTresor(tr : word);
var fl,md : word;
begin
  with Data do
  begin
    fl := 0;
    md := 0;
    if tr > 0 then
      fl:=T^[tr].Localisation;
    if fl > 0 then
      md:=F^[fl].Localisation;
    if md > 0 then
    begin
      if T^[tr].Proprio<>O.Auteur then Erreur(15)
      else if T^[tr].Statut=0 then Erreur(16)
      else
      begin
         T^[tr].Statut:=0;
         T^[tr].Localisation:=md;
         T^[tr].Proprio:=M^[md].Proprio;
      end;
    end;
  end;
end;

Procedure TEvalDechargeTresor.EvalueOrdre;
begin
  DechLeTresor(O.O[1]);
end;

procedure TEvalDechargeTresor.Finalise;
begin
  inherited;

end;

function TEvalDechargeTresor.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalDechargeTresor.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalDechargeTresor.Init;
begin
  inherited;

end;

end.
