unit Eval.Transfert;

Interface

uses Avateval, Avatunit, eval.etape;

type

  TEvalTransfert = class(TEvalEtape)
  public const
    TYPE_ORDRE = 11;
    ETAPE_EVAL = 11;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    TabVI, TabVP : array[1..NB_MONDE_MAX] of Integer;
    TabFlotteVC, TabFlotteVT : array[1..NB_FLOTTE_MAX] of Integer;

    procedure TransDeMd(md, x: word);
    procedure TransVCDeFl(fl, x: word);
    procedure TransVCVTFl(fl, x, fl2: integer);
    procedure TransVTDeFl(fl, x: word);
    procedure TransVxVTmd(md, x, fl2: integer);
  end;

Implementation

uses erreurseval;

procedure TEvalTransfert.Init;
begin
  FillChar(TabVI, sizeof(TabVI), 0);
  FillChar(TabVP, sizeof(TabVP), 0);
  FillChar(TabFlotteVC, sizeof(TabFlotteVC), 0);
  FillChar(TabFlotteVT, sizeof(TabFlotteVT), 0);
end;

procedure TEvalTransfert.TransDeMd(md : word;x : word);
begin
  with Data do
  begin
    if M^[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
    else
    begin
       if ((O.StypeO in [1,3])and(M^[md].VI<x)) then  // Source = VI
       begin
          Erreur(ERR_PAS_ASSEZ_VI);
          x:=M^[md].VI;
          OK:=True;
       end;
       if ((O.StypeO in [2,4])and(M^[md].VP<x)) then // Source = VP
       begin
          Erreur(ERR_PAS_ASSEZ_VP);
          x:=M^[md].VP;
          OK:=true;
       end;
    end;
    if OK then
    begin
       if O.STypeO = 1 then  // M_m T x VI VP
         Inc(TabVP[md],x)
       else if O.STypeO = 2 then // M_m T x VP VI
         Inc(TabVI[md],x)
       else if O.StypeO in [3,4]  then // M_m T x XX F_f
          if F^[O.O[3]].Localisation<>md then Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
          else Inc(TabFlotteVC[O.O[3]],x);
       if OK then
       begin
          if O.StypeO in [1,3] then Dec(M^[md].VI,x); // Source = VI
          if O.StypeO in [2,4] then Dec(M^[md].VP,x); // Source = VP
       end;
    end;
  end;
end;

procedure TEvalTransfert.TransVCDeFl(fl : word;x : word);
begin
  with Data do
  begin
    if F^[fl].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if F^[fl].NbVC<x then
    begin
       Erreur(ERR_PAS_ASSEZ_VAISSEAUX);
       x:=F^[fl].NbVC;
       OK:=True;
    end;
    if OK then
    begin
       if O.StypeO = 5 then Inc(TabVI[F^[fl].Localisation],x) // Dest = VI
       else if O.StypeO = 6 then Inc(TabVP[F^[fl].Localisation],x) // Dest = VP
       else if O.StypeO = 7 then // Dest = F_g
       begin
         if O.O[3] = O.O[1] then
           Erreur(ERR_TRANSFERT_MEME_FLOTTE)
         else if F^[O.O[3]].Localisation<>F^[O.O[1]].Localisation then
           Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
         else Inc(TabFlotteVC[O.O[3]],x);
       end;
       if OK then Dec(F^[fl].NbVC,x);
    end;
  end;
end;

procedure TEvalTransfert.TransVTDeFl(fl : word;x : word);
begin
  with Data do
  begin
    if F^[fl].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if F^[fl].NbVT<x then
    begin
       Erreur(ERR_PAS_ASSEZ_VAISSEAUX);
       x:=F^[fl].NbVT;
       OK:=True;
    end;
    if OK then
    begin
       if F^[O.O[3]].Localisation<>F^[O.O[1]].Localisation then
          Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
       else
          Inc(TabFlotteVT[O.O[3]],x);
       if OK then Dec(F^[fl].NbVT,x);
    end;
  end;
end;

procedure TEvalTransfert.TransVCVTFl(fl, x, fl2 : integer);
begin
  with Data do
  begin
    if Jou[O.Auteur].Classe <> Marchand then Erreur(ERR_MAUVAISE_CLASSE)
    else if F^[fl].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if F^[fl].NbVC<x then
    begin
       Erreur(ERR_PAS_ASSEZ_VAISSEAUX);
       x:=F^[fl].NbVC;
       OK:=True;
    end;
    if OK then
    begin
       if fl2 = 0 then Inc(TabFlotteVT[fl],x)
       else if F^[O.O[3]].Localisation<>F^[O.O[1]].Localisation
         then Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
       else Inc(TabFlotteVT[fl2],x);
       if OK then Dec(F^[fl].NbVC,x);
    end;
  end;
end;

procedure TEvalTransfert.TransVxVTmd(md, x, fl2 : integer);
begin
  with Data do
  begin
    if Jou[O.Auteur].Classe <> Marchand then Erreur(ERR_MAUVAISE_CLASSE)
    else if M^[md].Proprio<>O.Auteur then Erreur(ERR_PAS_PROPRIO_MONDE)
    else
    begin
       if ((O.StypeO = 11) and (M^[md].VI<x)) then // Source = VI
       begin
          Erreur(ERR_PAS_ASSEZ_VI);
          x:=M^[md].VI;
          OK:=True;
       end;
       if ((O.StypeO = 12) and (M^[md].VP<x)) then // Source = VP
       begin
          Erreur(ERR_PAS_ASSEZ_VP);
          x:=M^[md].VP;
          OK:=true;
       end;
    end;
    if OK then
    begin
       if F^[O.O[3]].Localisation<>md then Erreur(ERR_FLOTTE_PAS_SUR_MONDE)
       else Inc(TabFlotteVT[O.O[3]],x);
       if OK then
       begin
          if O.StypeO = 11 then Dec(M^[md].VI,x); // Source = VI
          if O.StypeO = 12 then Dec(M^[md].VP,x); // Source = VP
       end;
    end;
  end;
end;

procedure TEvalTransfert.Finalise;
var i : integer;
begin
  with Data do
  begin
    for i := 1 to NbMonde do
    begin
      Inc(M^[i].VI, TabVI[i]);
      Inc(M^[i].VP, TabVP[i]);
    end;

    for i := 1 to NbFlotte do
    begin
      Inc(F^[i].NbVC, TabFlotteVC[i]);
      Inc(F^[i].NbVT, TabFlotteVT[i]);
    end;

    {Mise � jour des cargaisons transport�es}
    for i := 1 to NbFlotte do
      UpdateContenuFlotte(i);
  end;
end;

function TEvalTransfert.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalTransfert.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

Procedure TEvalTransfert.EvalueOrdre;
begin
  Case O.STypeO of
    1..4 : TransDeMd(O.O[1],O.O[2]);
    5..7 : TransVCDeFl(O.O[1],O.O[2]);
    8 : TransVTDeFl(O.O[1],O.O[2]);
    9..10 : TransVCVTFl(O.O[1], O.O[2], O.O[3]);
    11..12 : TransVxVTmd(O.O[1], O.O[2], O.O[3]);
  end;
end;

end.
