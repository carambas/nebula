unit Eval.ConstructionBombe;

Interface

uses Avateval, eval.etape;

type

  TEvalConstructionBombe = class(TEvalEtape)
  public const
    TYPE_ORDRE = 15;
    ETAPE_EVAL = 15;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  end;

Implementation

uses Avatunit, erreurseval;

Procedure TEvalConstructionBombe.EvalueOrdre;
var
  fl: word;
begin
  with Data do
  begin
    fl := O.O[1];
    if BombesInterdites then
      Erreur(ERR_ORDRE_INTERDIT)
    else if F^[fl].Proprio <> O.Auteur then
      Erreur(5)
    else if F^[fl].NbVC < 25 then
      Erreur(37)
    else
    begin
      Dec(F^[fl].NbVC, 25);
      F^[fl].Statut := (F^[fl].Statut or Puis2(7));
      UpdateContenuFlotte(fl);
    end;
  end;
end;

procedure TEvalConstructionBombe.Finalise;
begin
  inherited;

end;

function TEvalConstructionBombe.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalConstructionBombe.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalConstructionBombe.Init;
begin
  inherited;

end;

end.
