unit Eval.APIPoints;


interface

uses NebData, AvatUnit;


type
  PEnumFamilleNomTres = ^TEnumFamilleNomTres;
  TEnumFamilleNomTres = array [0 .. NB_MONDE_MAX, 1 .. NBNOMTRES] of byte;
  PEnumFamilleAdjTres = ^TEnumFamilleAdjTres;
  TEnumFamilleAdjTres = array [0 .. NB_MONDE_MAX, 1 .. NBADJTRES] of byte;

type
  TAPIPoints = class(TObject)
    Data: TNebData;
    TotNomTres: PEnumFamilleNomTres;
    TotAdjTres: PEnumFamilleAdjTres;
    const PtsPill : array[1..5] of word = (80,50,40,30,20);

    constructor Create(Data: TNebData);
    procedure CompteFamillesTresors;

    {fonctions g�n�rales -> regarde toute la situation d'un joueur donn�
                         -> s'effectue au moment de la mise au point}
    procedure PtsPop(j : integer);
    procedure PtsInd(j : integer);
    procedure PtsCM(j : integer);
    procedure PtsFlottes(j : integer);
    procedure PtsMondes(j : integer);
    procedure PtsMondesConv(j : integer);
    procedure PtsConv(j : integer);
    procedure PtsMondesRob(j : integer);
    procedure PtsTresors(j : integer);
    procedure AjouteBonus;
    function Bonus(j : integer) : integer;

    {fonctions plus sp�cifiques -> appel�es � chaque fois que n�cessaire}
    procedure PtsPillages(j, md, nbPi : integer);
    procedure PtsMartyrs(Agresseur, Victime, NbPop : integer); {Points de la victime}
    procedure PtsPopTuees(Agresseur, Victime, NbPop, NoMonde : integer); {Points de l'agresseur}
    procedure PtsVaissDetr(Agresseur, Victime, NbVaiss, NoMonde : integer); {Points de l'agresseur}
    procedure PtsIndDetr(Agresseur, Victime, NbInd, NoMonde : integer); {Points de l'agresseur}
    procedure PtsBombes(Agresseur, Victime, NoMonde : integer); {Points de l'agresseur}
    procedure PtsChose(j, Points : integer);
    procedure PtsMPDech(md : integer; nbMP : PLigne);
    procedure PtsPCDech(j, n : integer);
    procedure PtsDeconv(j, n : integer);
    procedure PtsNouvContact(j, j2 : integer);
    procedure PtsMondeObserve(j, md, NbSimultane : integer; var NbExploDecouvreMd : byte);
    procedure PtsMondeLocalise(j, md : integer);
    procedure PtsMdepObserve(j, md : integer; NbExploDecouvreMd : byte);
    procedure PtsMEVA(j, md : integer; NbVC : real);
    procedure PtsPE(j, md, NbExplo : integer);

    {A usage interne, � passer en impl�mentation mais on le garde l�
     pour l'instant pour des raisons de compatibilit� avec points.pas}
    function PointTres(tr,cl : word) : ShortInt;
  public
    destructor Destroy; override;
  end;


implementation

uses Utils, SysUtils;

{fonctions g�n�rales}

procedure TAPIPoints.PtsPop(j : integer);
var
  md, fl : integer;
  pop : LongInt;
  score : Integer;
begin
  with data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Empereur then
    begin
      pop := 0;
      for md := 1 to NbMonde do if M^[md].Proprio = j then
        pop := pop + M^[md].Pop;
      for fl := 1 to NbFlotte do if F^[fl].Proprio = j then
        pop := pop + F^[fl].N + F^[fl].C;
      score := pop div 10;

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_POP] := DetailScores[j, DS_POP] + score;
  end;
end;

procedure TAPIPoints.PtsInd(j : integer);
var
  md : integer;
  ind : LongInt;
  score : Integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Empereur then
    begin
      ind := 0;
      for md := 1 to NbMonde do if M^[md].Proprio = j then
        ind := ind + M^[md].Ind + ME[md].IndLibre; // 29/08/98 : +1 pt par ind active
      score := Ind;

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_IND] := DetailScores[j, DS_IND] + score;
  end;
end;

procedure TAPIPoints.PtsCM(j : integer);
var
  md : integer;
  CM : LongInt;
  score : Integer;
begin
  with data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Empereur then
    begin
      CM := 0;
      for md := 1 to NbMonde do if M^[md].Proprio = j then
        CM := CM + M^[md].PlusMP;
      score := CM;

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_CM] := DetailScores[j, DS_CM] + score;
  end;
end;

procedure TAPIPoints.PtsFlottes(j : integer);
var
  fl : integer;
  flottes : LongInt;
  score : Integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Pirate then
    begin
      flottes := 0;
      for fl := 1 to NbFlotte do if F^[fl].Proprio = j then
        flottes := flottes + 1;
      score := flottes * 5;

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_FLOTTES] := DetailScores[j, DS_FLOTTES] + score;
  end;
end;

procedure TAPIPoints.PtsMondes(j : integer);
var
  md : integer;
  monde : LongInt;
  score : Integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Missionnaire then
    begin
      monde := 0;
      for md := 1 to NbMonde do if M^[md].Proprio = j then
        monde := monde + 1;
      score := monde * 5;

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_MONDES] := DetailScores[j, DS_MONDES] + score;
  end;
end;

procedure TAPIPoints.PtsMondesConv(j : integer);
var
  md : integer;
  monde : LongInt;
  score : Integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Missionnaire then
    begin
      monde := 0;
      for md := 1 to NbMonde do if M^[md].Proprio = j then
        if (M^[md].Conv = M^[md].Pop) and (M^[md].Conv > 0) then
          monde := monde + 1;
      //score := monde * 5;
      score := monde * 20; // 29/08/98 les pts d'un monde conv passent de 5 � 20

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_MONDESCONV] := DetailScores[j, DS_MONDESCONV] + score;
  end;
end;

procedure TAPIPoints.PtsConv(j : integer);
var
  md, fl : integer;
  conv : LongInt;
  score : Integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Missionnaire then
    begin
      conv := 0;
      for md := 1 to NbMonde do if M^[md].ProprConv = j then
        conv := conv + M^[md].Conv;
      for fl := 1 to NbFlotte do if F^[fl].Proprio = j then
        conv := conv + F^[fl].C;
      score := conv div 10;

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_CONV] := DetailScores[j, DS_CONV] + score;
  end;
end;

procedure TAPIPoints.PtsMondesRob(j : integer);
var
  md : integer;
  monde : LongInt;
  score : Integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then if Jou[j].Classe = Robotron then
    begin
      monde := 0;
      for md := 1 to NbMonde do if M^[md].Proprio = j then
        if M^[md].Robot > 0 then
          monde := monde + 1;
      score := monde * 20; // 29/08/98 : 20 pts pour un monde robotis� (10 avant)

      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_MONDESROB] := DetailScores[j, DS_MONDESROB] + score;
  end;
end;

procedure TAPIPoints.PtsTresors(j : integer);
var
  tr : integer;
  score : Integer;
  md : integer;
  NbTresMd : integer;
  i : integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then
    begin
      for tr := 1 to NbTres do if T^[tr].Proprio = j then
      begin
        score := score + PointTres(tr, Jou[j].Classe);
        if PointTres(tr, Jou[j].Classe) >= 0 then
          DetailScores[j, DS_TRESORS] := DetailScores[j, DS_TRESORS] + PointTres(tr, Jou[j].Classe)
        else
          DetailScores[j, DS_TRESMAUDIT] := DetailScores[j, DS_TRESMAUDIT] + PointTres(tr, Jou[j].Classe);
      end;

      if Jou[j].CLasse = Antiquaire then
        for md:=1 to NbMonde do if M^[md].Proprio=j then
        begin
           NbTresMd:=0;
           for tr:=1 to NbTres do
             if (T^[tr].Localisation=md) and (T^[tr].Statut = 0) then
               inc(NbTresMd);
           if NbTresMd >= 12 then
           begin
             inc(score,100); {pts d'un mus�e par tour}
             DetailScores[j, DS_MUSEES] := DetailScores[j, DS_MUSEES] + 100;
           end;

           // Familles de Tr�sors
           for i := 1 to NBNOMTRES do
           begin
             inc(score, PtsFamilles[TotNomTres[md, i]] div 10);
             DetailScores[j, DS_FAMILLES] :=
               DetailScores[j, DS_FAMILLES] + PtsFamilles[TotNomTres[md, i]] div 10;
           end;
           for i := 1 to NBADJTRES do
           begin
             inc(score, PtsFamilles[TotAdjTres[md, i]] div 10);
             DetailScores[j, DS_FAMILLES] :=
               DetailScores[j, DS_FAMILLES] + PtsFamilles[TotAdjTres[md, i]] div 10;
           end;
        end;


      Jou[j].Score := Jou[j].Score + score;
    end;
  end;
end;

{fonctions sp�cifiques}

procedure TAPIPoints.PtsPillages(j, md, nbPi : integer);
var
  score : integer;
  PopMoins : integer;
  //i : integer;
  NCMoins, CMoins : integer;
begin
  with data do
  begin
    score := 0;
    NCMoins := 0;
    CMoins := 0;
    if j <> 0 then
    begin
      if nbPi > 5 then nbPi := 5;
      score := PtsPill[nbPi];

      // Ici on enl�ve 1/3 de la pop du monde
      PopMoins := Round(M[md].Pop / 3);
      while PopMoins > 0 do
      begin
        Dec(PopMoins, 1); // On vire 1 pop
        if Random(M[md].Pop)+1 <= M[md].Conv then
        begin // On tue 1 converti
          M[md].Conv := M[md].Conv - 1;
          M[md].Pop := M[md].Pop - 1;
          inc(CMoins);
        end
        else // On tue un non converti
        begin
          M[md].Pop := M[md].Pop - 1;
          inc(NCMoins);
        end;
      end;
      Inc(ME[md].PopMoins, NCMoins + CMoins);
      Inc(ME[md].ConvMoins, CMoins);

      if Jou[j].Classe <> Pirate then
        score := - score div 2;
      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
    begin
      DetailScores[j, DS_PILLAGES] := DetailScores[j, DS_PILLAGES] + score
    end;
  end;
end;

procedure TAPIPoints.PtsMartyrs(Agresseur, Victime, NbPop : integer);
var
  score : integer;
begin
  with Data do
  begin
    {Agresseur = -1 -> les points doivent �tre marqu�s}
    score := 0;
    if Agresseur <> 0 then
      if (Victime <> 0) and (Jou[Victime].Classe = Missionnaire) then
        if (Jou[Victime].Jihad <> Agresseur) and (Agresseur <> Victime) then
          score := NbPop * 2;

    if Victime <> 0 then
    begin
      Jou[Victime].Score := Jou[Victime].Score + score;
      DetailScores[Victime, DS_MARTYRS] := DetailScores[Victime, DS_MARTYRS] + score;
    end;
  end;
end;

procedure TAPIPoints.PtsPopTuees(Agresseur, Victime, NbPop, NoMonde : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := 0;
    if Agresseur <> 0 then
    begin
      score := NbPop * 2;
      if (Jou[Agresseur].classe <> Robotron) and not ((Jou[Agresseur].classe = Missionnaire) and
        (((jou[Agresseur].Jihad = Victime) and (Victime <> 0))or EstCorrompu(NoMonde, Agresseur)) {and (Victime <> 0)}) then {Pas robotron, ni missionnaire en jihad sur son ennemi}
          score := - NbPop;

      Jou[Agresseur].Score := Jou[Agresseur].Score + score;
    end;

    if Agresseur <> 0 then
    begin
      if score >= 0 then
        DetailScores[Agresseur, DS_POPTUEES] := DetailScores[Agresseur, DS_POPTUEES] + score
      else
        DetailScores[Agresseur, DS_MALUSPOPTUEES] := DetailScores[Agresseur, DS_MALUSPOPTUEES] + score
    end;
  end;
end;

procedure TAPIPoints.PtsVaissDetr(Agresseur, Victime, NbVaiss, NoMonde : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := 0;
    if Agresseur <> 0 then
    begin
      if Jou[Agresseur].classe = Robotron then
        score := NbVaiss * 2;
      if Jou[Agresseur].classe = Missionnaire then
      begin
        if ((jou[Agresseur].Jihad = Victime) and (Victime <> 0)) or (EstCorrompu(NoMonde, Agresseur)) then
          score := NbVaiss * 2
        else
          score := - NbVaiss;
      end;

      Jou[Agresseur].Score := Jou[Agresseur].Score + score;
    end;

    if Agresseur <> 0 then
    begin
      if score >= 0 then
        DetailScores[Agresseur, DS_VAISSDETR] := DetailScores[Agresseur, DS_VAISSDETR] + score
      else
        DetailScores[Agresseur, DS_MALUSVAISSDETR] := DetailScores[Agresseur, DS_MALUSVAISSDETR] + score
    end;
  end;
end;

procedure TAPIPoints.PtsIndDetr(Agresseur, Victime, NbInd, NoMonde : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := 0;
    if Agresseur <> 0 then
    begin
      if Jou[Agresseur].classe = Robotron then
        score := NbInd * 2;

      Jou[Agresseur].Score := Jou[Agresseur].Score + score;
    end;

    if Agresseur <> 0 then
    begin
      if score >= 0 then
        DetailScores[Agresseur, DS_INDDETR] := DetailScores[Agresseur, DS_INDDETR] + score;
    end;
  end;
end;

procedure TAPIPoints.PtsBombes(Agresseur, Victime, NoMonde : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := 0;
    if Agresseur <> 0 then
    begin
      score := -100;
      if Jou[Agresseur].classe = Robotron then
        score := 200;
      if (Jou[Agresseur].classe = Missionnaire) and
        (EstCorrompu(NoMonde, Agresseur)) then
        score := 0;

      Jou[Agresseur].Score := Jou[Agresseur].Score + score;
    end;

    if Agresseur <> 0 then
      DetailScores[Agresseur, DS_BOMBES] := DetailScores[Agresseur, DS_BOMBES] + score;
  end;
end;

procedure TAPIPoints.PtsChose(j, Points : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := Points;
    Jou[j].Score := Jou[j].Score + score;

    if j <> 0 then
      DetailScores[j, DS_CHOSE] := DetailScores[j, DS_CHOSE] + score;
  end;
end;

procedure TAPIPoints.PtsMPDech(md : integer; nbMP : PLigne);
const
  //PointsParMP = 6;
  //PointsParMP = 8; // 29/08/98 on revient � 8 points
  PointsParMP = 7; // 10/09/98 et hop 7 points
var
  score : integer;
  PtsMPDech : TLigne;
  j : integer;
  SommeMP : integer;
begin
  if md = 0 then exit;
  with Data do
  begin
    FillChar(PtsMPDech, sizeof(PtsMPDech), 0);
    SommeMP := 0;
    // On regarde si les marchands n'ont pas d�charg� trop de MP
    for j := 1 to NbJou do
    begin
      if (nbMP[j] > 0) and (Jou[j].Classe = Marchand) and (j <> M^[md].Proprio) then
      begin
        PtsMPDech[j] := nbMP[j] * PointsParMP;
        inc(SommeMP, nbMP[j]);
      end;
    end;

    // Les marchands ont d�pass� la limite du double des industries
    if SommeMP > M[md].Ind * 2 then
    begin
      for j := 1 to NbJou do
        PtsMPDech[j] := Round(PtsMPDech[j] * M[md].Ind * 2 / SommeMP);
    end;

    for j := 1 to NbJou do
    begin
      score := PtsMPDech[j];
      Jou[j].Score := Jou[j].Score + score;
      DetailScores[j, DS_MPDECH] := DetailScores[j, DS_MPDECH] + score;
    end;
  end;
end;

procedure TAPIPoints.PtsPCDech(j, n : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then
    begin
      if (Jou[j].Classe = Marchand) then
        score := score + n;
      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_PCDECH] := DetailScores[j, DS_PCDECH] + score;
  end;
end;

procedure TAPIPoints.PtsDeconv(j, n : integer);
var
  score : integer;
begin
  with Data do
  begin
    score := 0;
    if j <> 0 then
    begin
      if (Jou[j].Classe = Marchand) then
        score := score + n * 15;
      Jou[j].Score := Jou[j].Score + score;
    end;

    if j <> 0 then
      DetailScores[j, DS_DECONV] := DetailScores[j, DS_DECONV] + score;
  end;
end;

procedure TAPIPoints.PtsNouvContact(j, j2 : integer);
begin
// Plus de points pour une rencontre
end;

procedure TAPIPoints.PtsMondeLocalise(j, md : integer);
var
  score : integer;
begin
  if j <> 0 then if Data.Jou[j].Classe = Explorateur then
    if Data.EstConnu(md, j) and not Data.AVuConnect[md, j] then
    begin
      score := 20;
      Data.Jou[j].Score := Data.Jou[j].Score + score;
      Data.DetailScores[j, DS_MONDELOCALISE] :=
        Data.DetailScores[j, DS_MONDELOCALISE] + score;
    end;
end;

procedure TAPIPoints.PtsMdepObserve(j, md : integer; NbExploDecouvreMd : byte);
begin
  // 10/09/1999 on change les points d'observation d'un monde
  // Maintenant c'est PtsPE qui fait le boulot
  Exit
end;

procedure TAPIPoints.PtsPE(j, md, NbExplo : integer);
var
  score : integer;
begin
   score := Data.M[md].PotExplo div NbExplo;
   if score > MAXPOTEXPLO then
     score := MAXPOTEXPLO;
   Data.Jou[j].Score := Data.Jou[j].Score + score;
   Data.DetailScores[j, DS_MDEPLOCALISE] :=
     Data.DetailScores[j, DS_MDEPLOCALISE] + score;
end;

procedure TAPIPoints.PtsMondeObserve(j, md, NbSimultane : integer; var NbExploDecouvreMd : byte);
var
  score : integer;
  MDep : boolean;
  joueur : integer;
begin
  //score := 0;
  MDep := False;

  if j <> 0 then if Data.Jou[j].Classe = Explorateur then
  begin
    if not Data.StationMonde[md, j] then // Si le joueur n'a pas encore stationn�...
    begin
      {Monde de d�part}
      for joueur := 1 to Data.NbJou do
        if Data.Jou[joueur].Md = md then MDep := True;
      if MDep then
        inc(NbExploDecouvreMd);
      //else
      begin  // pas de pts d'observation direct pour les Md juste le bonus
             // Depuis le 11/09/1999 si :)
        score := 40 - Data.M[md].NbExplo * 10;
        if score < 0 then score := 0;
        Data.Jou[j].Score := Data.Jou[j].Score + score;
        Data.DetailScores[j, DS_MONDEOBSERVE] :=
          Data.DetailScores[j, DS_MONDEOBSERVE] + score;
      end;
    end;
  end;

end;

procedure TAPIPoints.PtsMEVA(j, md : integer; NbVC : real);
var
  score : integer;
begin
  with Data do
  begin
    if j <> 0 then
    begin
      // Avec MEVAPourTous des non explo peuvent passer ici
      // Seuls les explos marquent les points
      if Jou[j].Classe = Explorateur then
      begin
        score :=  Round2(NBVC * 20);
        Jou[j].Score := Jou[j].Score + score;
        DetailScores[j, DS_MEVA] :=
          DetailScores[j, DS_MEVA] + score;
      end;
    end;
  end;
end;

function TAPIPoints.PointTres(tr,cl : word) : ShortInt;
const PtsTres : array[1..NBCLASSE] of word = (2,3,5,1,6,4,7);
var
  d, u : integer;
  points : integer;
begin
  Result := 0;
   points:=0;
   if cl > 0 then
   begin
     if tr <= NBNOMTRES * NBADJTRES then
     begin
       d := (tr-1) div NBADJTRES + 1; // "dizaines" en base NBADJTRES
       u := (tr-1) mod NBADJTRES + 1; // "unit�s" en base NBADJTRES
       if d=PtsTres[cl] then inc(points,5);
       if u=PtsTres[cl] then inc(points,5);
       if ((d=PtsTres[cl])and(u=PtsTres[cl])) then inc(points,5);
       if cl=Antiquaire then
       case points of
         0 : points:=15;
         5 : points:=30;
         15 : points:=90;
       end;
       if ((u=8)and(cl<>4)) then dec(points,10);
     end
     else // tr�sors sp�ciaux
     begin
       if cl=Antiquaire then
       case tr - NBNOMTRES * NBADJTRES of
         1     : points := 40;  // Le Mangeur de Points Gigantesque
         2     : points := 40;  // Le Tr�s Grand Mangeur de Points
         3     : points := 30;  // Le Grand Mangeur de Points
         4     : points := 30;  // Le Petit Mangeur de Points
         5     : points := 40;  // Le Diamant Gigantesque
         6     : points := 40;  // Le Tr�s Grand Diamant
         7     : points := 30;  // Le Grand Diamant
         8     : points := 30;  // Le Petit Diamant
         9..17 : points := 30;  // Tomes
         18    : points := 15;  // La chose
       end
       else
       case tr - NBNOMTRES * NBADJTRES of
         1     : points := -40; // Le Mangeur de Points Gigantesque
         2     : points := -35; // Le Tr�s Grand Mangeur de Points
         3     : points := -30; // Le Grand Mangeur de Points
         4     : points := -15; // Le Petit Mangeur de Points
         5     : points := 40;  // Le Diamant Gigantesque
         6     : points := 30;  // Le Tr�s Grand Diamant
         7     : points := 20;  // Le Grand Diamant
         8     : points := 10;  // Le Petit Diamant
         9..17 : points := 0;   // Tomes
         18    : points := 0;   // La chose
       end;
     end;
     Result:=points;
   end;
end;



function TAPIPoints.Bonus(j : integer) : integer;
const PtsTres : array[1..NBCLASSE] of word = (1,2,4,0,5,3,6);
var
  tr,NbTresMd,NbTome : word;
  md : UInt16;
  score : integer;
  i : integer;
begin
  with Data do
  begin
     score := 0;
     NbTome:=0;
     for tr:=1 to NbTres do
     begin
        if ((tr in [191..199]) and (T^[tr].Proprio=j))
          then inc(NbTome);
     end;
     if NbTome >= 3 then
       inc (score, (NbTome-2)*500);

     if Jou[j].CLasse = Antiquaire then
     begin
       for md:=1 to NbMonde do if M^[md].Proprio=j then
       begin
          NbTresMd:=0;
          for tr:=1 to NbTres do if T^[tr].Localisation=md then inc(NbTresMd);
           if NbTresMd >= 12 then
              inc(score,1000); {Un mus�e}

          // Familles de Tr�sors
          for i := 1 to NBNOMTRES do
            inc(score, PtsFamilles[TotNomTres[md, i]]);
          for i := 1 to NBADJTRES do
            inc(score, PtsFamilles[TotAdjTres[md, i]]);
       end;
     end;
     Result := score;
   end;
end;

procedure TAPIPoints.CompteFamillesTresors;
var
  tr: integer;
begin
  // Familles de tr�sors
  FillChar(TotNomTres^, sizeof(TotNomTres^), 0);
  FillChar(TotAdjTres^, sizeof(TotAdjTres^), 0);
  for tr := 1 to NBNOMTRES * NBADJTRES do
    if Data.T[tr].Statut = 0 then
    begin
      Inc(TotNomTres^[Data.T[tr].Localisation, (tr - 1) div NBADJTRES + 1]);
      Inc(TotAdjTres^[Data.T[tr].Localisation, (tr - 1) mod NBADJTRES + 1]);
    end;
end;


constructor TAPIPoints.Create(Data: TNebData);
begin
  self.Data := Data;
  TotNomTres := New(PEnumFamilleNomTres);
  TotAdjTres := New(PEnumFamilleAdjTres);
  CompteFamillesTresors;
end;

destructor TAPIPoints.Destroy;
begin
  Dispose(TotNomTres);
  Dispose(TotAdjTres);
end;

procedure TAPIPoints.AjouteBonus;
var
  j : integer;
begin
   for j:=1 to Data.NbJou do
     Data.Jou[j].Score := Data.Jou[j].Score + Bonus(j);
end;

end.
