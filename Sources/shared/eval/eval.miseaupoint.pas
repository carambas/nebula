unit Eval.MiseAuPoint;

Interface

uses
  Avateval, eval.etape, Avatunit, Utils;

type
  TEvalMiseAuPoint = class(TEvalEtape)
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = 100;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    Procedure TriOrdres;
    function CalculeIndActive(md: integer): integer;

  private
    ConNomDbl: TRelationJou;
    procedure ChercheConNom;
    procedure Explo;
    procedure ExploPtsMondeLocalise;
    procedure CompteursCorruption;
    procedure ResetExploCM;
    procedure CalculePopLibre(md: Integer);
    procedure DecrementeCompteurPillage(md: Integer);
    procedure AugmenteCompteurPossessionEtCM(md: Integer);
    procedure NouvellesRencontres;
    procedure UpdateVisionMondes;
    procedure VisibiliteMondes;
  end;

Implementation

uses Eval.APIPoints, Espionnage;

// TODO : refaire un tri des ordres en s'appuyant sur TListOrdres
procedure TEvalMiseAuPoint.TriOrdres;
//Var
//    i : word;
//    O : TOrdre;
//    OrdreCourant, OrdrePrec : LOrdre;
//    Tempo : TOrdre;
//    fin : boolean;
begin
   // On d�sacive le tri � partir de la version 4.13b2.
   // Le tri ne fonctionnait et rendait impossible les tests de non r�gression
   //    par dif des CR
   Exit;

   // Tri par O.STypeO et en cas d'�galit� par O.O[1] (num monde, flotte, tr�sor)

//   for i := 0 To 21 do
//   begin
//      if data.ListeOrdresEvalues[i] <> nil then
//      begin
//         { tri }
//         FinOrdres[i] := nil;
//         while data.ListeOrdresEvalues[i] <> FinOrdres[i] do
//         begin
//            OrdreCourant := data.ListeOrdresEvalues[i];
//            OrdrePrec := nil;
//            while OrdreCourant^.Suivant <> FinOrdres[i] do
//            begin
//               if OrdreCourant^.Ordre.STypeO
//                  > OrdreCourant^.Suivant^.Ordre.STypeO then
//               begin
//                  Tempo := OrdreCourant^.Suivant^.Ordre;
//                  OrdreCourant^.Suivant^.Ordre := OrdreCourant^.Ordre;
//                  OrdreCourant^.Ordre := Tempo;
//               end
//               else if OrdreCourant^.Ordre.O[1]
//                  > OrdreCourant^.Suivant^.Ordre.O[1] then
//               begin
//                  Tempo := OrdreCourant^.Suivant^.Ordre;
//                  OrdreCourant^.Suivant^.Ordre := OrdreCourant^.Ordre;
//                  OrdreCourant^.Ordre := Tempo;
//               end;
//               OrdrePrec := OrdreCourant;
//               OrdreCourant := OrdreCourant^.Suivant;
//            end;
//            FinOrdres[i] := OrdreCourant;
//         end;
//      end;
//      { sauvegarde sur fichier }
//      // TODO : le code ci-dessous n'a l'air de rien faire.
//      FinOrdres := data.ListeOrdresEvalues;
//      fin := false;
//      Repeat
//         if data.ListeOrdresEvalues[i]<>nil then
//         begin
//            O:=FinOrdres[i]^.Ordre;
//            FinOrdres[i]:=FinOrdres[i]^.Suivant;
//         end;
//         if FinOrdres[i]=nil then
//           fin := true
//         else
//           if FinOrdres[i]^.Suivant=nil then
//             fin := true;
//      until fin;
//   end;
//   O.Auteur:=0;
//   O.TypeO:=21;
//   O.STypeO:=0;
//   //for i:=1 to 8 do O.Ch[i]:='';
//   for i:=1 to 4 do O.O[i]:=0;
//   new(CO);
//   CO^.Ordre:=O;
//   CO^.Suivant:=nil;
//   if FinOrdres[21]<>nil then FinOrdres[21]^.Suivant:=CO
//   else FinOrdres[21]:=CO;
//   FinOrdres:=data.ListeOrdresEvalues;
end;

// TODO sortir sous-fonction et variables au niveau de la classe
procedure TEvalMiseAuPoint.EvalueOrdre;
begin
  inherited;

end;

procedure TEvalMiseAuPoint.VisibiliteMondes;
var
  m: Integer;
begin
  for m := 1 to Data.NbMonde do
    begin
      if Data.M[m].Conv > 0 then
        Data.DevientConnu(m, Data.M[m].ProprConv);
    end;
end;


procedure TEvalMiseAuPoint.ChercheConNom;
var
  fl,k,j : word;
  md: Integer;

begin
  with Data do
  begin
    for md:=1 to NbMonde do for j:=1 to NbJou do if EstConnu(md,j) then
    begin
       ConnuDeNom[j,M^[md].Proprio] := True;
       ConnuDeNom[j,M^[md].ProprConv] := True;
       if M^[md].Proprio<>0 then ConnuDeNom[j,ME^[md].AncienProprio] := True;
       for fl:=1 to NbFlotte do
       begin
          if F^[fl].Localisation=md then
          begin
             ConnuDeNom[j,F^[fl].Proprio] := True;
             ConnuDeNom[j,FE^[fl].AncienProprio] := True;
          end;
          for k:=1 to MAX_CHEMIN do if FE^[fl].Chemin[k]=md then
             ConnuDeNom[j,FE^[fl].AncienProprio] := True;
       end;
    end;
  end;
end;

procedure TEvalMiseAuPoint.ExploPtsMondeLocalise;
var
  i, j: Integer;
  PremierAVu: array[1..1024] of integer;
begin
  FillChar(PremierAVu, sizeof(PremierAvu), 0);
  {Visibilit� -> Localisation}
  for i := 1 to Data.NbMonde do
  begin
    {on regarde combien de personnes d�couvrent ce monde � ce tour}
    if Data.NbAVuConnect[i] = 0 then
    for j := 1 to Data.NbJou do
    begin
      if Data.EstConnu(i, j) then
        PremierAVu[i] := PremierAVu[i] + 1;
    end;
    for j := 1 to Data.NbJou do
    begin
      if Data.EstConnu(i, j) then
      begin
        Points.PtsMondeLocalise(j, i);
      end;
    end;
  end;
end;

procedure TEvalMiseAuPoint.Explo;
var
  PremierStation, Station : array[1..1024] of integer;
  NbExploDecouvreMd : byte;
  NbExploSurMd : integer;
  i, j: Integer;
begin
   // Exploration
   FillChar(PremierStation, sizeof(PremierStation), 0);
   FillChar(Station, sizeof(Station), 0);

   with Data do
   begin
     // Stations -> Observation
     for i := 1 to NbFlotte do if F^[i].Localisation > 0 then
       if (F^[i].Proprio > 0) and (F^[i].NbVC + F^[i].NbVT > 0) then
         Station[F^[i].Localisation] := Station[F^[i].Localisation] or Puis2(F^[i].Proprio-1);
     for i := 1 to NbMonde do if (M^[i].Proprio > 0) and (M^[i].VI + M^[i].VP > 0) then
       Station[i] := Station[i] or Puis2(M^[i].Proprio-1);
     for i := 1 to NbMonde do if Station[i] <> 0 then
       for j := 1 to NbJou do if (Station[i] and Puis2(j-1)) <> 0 then
         Inc(PremierStation[i]);

     // Pts Premi�re Station
     for i := 1 to NbMonde do if Station[i] <> 0 then
     begin
       NbExploDecouvreMd := 0;

       // ici on compte les points d'observation
       for j := 1 to NbJou do
       begin
          if (not StationMonde[i, j]) and ((Station[i] and Puis2(j-1)) <> 0) then
            Points.PtsMondeObserve(j, i, PremierStation[i], NbExploDecouvreMd);
       end;

       // L� on augmente le comppteur de MEVA pour chaque observation pr�c�dente
       // On fait �a en deux temps pour que tout le monde touche le m�me
       // nombre de points
       for j := 1 to NbJou do
         if Data.Jou[j].Classe = Explorateur then
       begin
          if (not StationMonde[i, j]) and ((Station[i] and Puis2(j-1)) <> 0) then
            if Data.M[i].NbExplo < MAXMEVA then
              inc(Data.M[i].NbExplo);
       end;

       for j := 1 to NbJou do
         StationMonde[i, j] := StationMonde[i, j] or ((Station[i] and Puis2(j-1)) <> 0);
     end;

     // Affectation des points de potentiel d'exploration
     for i := 1 to Data.NbJou do
       if (Data.Jou[i].Md) > 0 then // pour chaque md
     begin
       NbExploSurMd := 0;

       // On regarde les explos pr�sents sur Data.Jou[i].Md
       for j := 1 to Data.NbJou do
         if (Data.Jou[j].Classe = Explorateur) and
            ((Station[Data.Jou[i].Md] and Puis2(j-1)) <> 0) and
            (j <> i) then
           Inc(NbExploSurMd);

       // Partage du PE
       if NbExploSurMd > 0 then
       begin
         for j := 1 to Data.NbJou do
           if (Data.Jou[j].Classe = Explorateur) and
              ((Station[Data.Jou[i].Md] and Puis2(j-1)) <> 0) and
              (j <> i) then
             Points.PtsPE(j, Data.Jou[i].Md, NbExploSurMd);
         if MAXPOTEXPLO * NbExploSurMd > Data.M[Data.Jou[i].Md].PotExplo then
           Data.M[Data.Jou[i].Md].PotExplo := 0
         else
           Dec(Data.M[Data.Jou[i].Md].PotExplo, MAXPOTEXPLO * NbExploSurMd);
       end;
     end;

     ExploPtsMondeLocalise;
   end;
end;

procedure TEvalMiseAuPoint.CompteursCorruption;
var
  i: Integer;
begin
  with Data do
  begin
     for i := 1 to NbMonde do
     begin
       if (M[i].Proprio > 0) and (M[i].PremierProprio = 0) then
         M[i].PremierProprio := M[i].Proprio;
       if M[i].Proprio > 0 then
         inc(M[i].Possession[M[i].Proprio]);
     end;
  end;
end;


procedure TEvalMiseAuPoint.ResetExploCM;
var
  md: Integer;
begin
  with Data do
    for md:=1 to NbMonde do
  begin
    // MAZ de l'explo de CM en cas de changement de propri�taire
    // ou tout simplement de capture (on peut acpturer et rendre � l'ancien proprio)
    if (M[md].Proprio <> ME[md].AncienProprio) or
       (M[md].Proprio = 0) or
       ((ME^[md].Statut and Puis2(0)) > 0) then
    begin
      if M^[md].Duree + M^[md].NbExploCM * 3 > 7 then
        M^[md].NbExploCM := 0;
      M^[md].Duree := 0;
    end;
  end;

end;

procedure TEvalMiseAuPoint.CalculePopLibre(md: Integer);
var pop : word;
begin
  pop := Data.M^[md].Pop;

  if Data.M^[md].Robot>0 then
    pop := Data.M^[md].Robot;

  if Data.ME^[md].PopLibre > pop
    then Data.ME^[md].PopLibre := pop;
end;

procedure TEvalMiseAuPoint.DecrementeCompteurPillage(md: Integer);
begin
  if Data.M^[md].Statut mod 8>0 then
    Dec(Data.M^[md].Statut);
end;

procedure TEvalMiseAuPoint.AugmenteCompteurPossessionEtCM(md: Integer);
begin
  with Data do
  begin
    // Augmentation de Duree et de CM
    if (not Capture[md] and (M^[md].Proprio > 0)) then
    begin
       if not Cadeau[md] then
       begin
          if (((M^[md].PlusMP > 0) or (M^[md].Duree + M^[md].NbExploCM * 3 > 0)))
             then inc(M^[md].Duree);

          if (((M^[md].Duree + M^[md].NbExploCM * 3) mod 7 = 0)
             and ((M^[md].Duree + M^[md].NbExploCM * 3) > 0)
             and (M^[md].PlusMP < 10))
             then
              inc(M^[md].PlusMP);
       end;

       if not Pille[md] then // gel� le tour du pillage
       begin
         // Augmentation de la pop

         // On est en-dessous du max de pop
         if (M[md].Pop < M[md].MaxPop) and (M[md].Pop > 0) then
         begin
           // monde enti�rement converti
           //       -> nouvelles populations d�j� converties
           if M[md].Conv = M[md].Pop then
           begin
             M[md].Conv := AugPop(M[md].Conv, M[md].MaxPop, M[md].NbExploPop);
             M[md].Pop := M[md].Conv;
           end

           // Monde pas enti�rement converti
           //        -> Nouvelles pops non converties
           else
             M[md].Pop := AugPop(M[md].Pop, M[md].MaxPop, M[md].NbExploPop);

           if M[md].Pop = M^[md].MaxPop then
             M[md].Pop := M[md].MaxPop;
           if M[md].Conv > M[md].Pop then
             M[md].Conv := M[md].Pop;
         end;
       end;

       if RecupPi[md] = 0 then // Si le monde n'est pas en phase de r�cup�ration de pillage
       begin
          // Production de MP
          if ME[md].PopLibre > M[md].PlusMP then
            Inc(M[md].MP, M[md].PlusMP)
          else
            Inc(M[md].MP, ME[md].PopLibre);
       end;
    end;

  end;
end;


procedure TEvalMiseAuPoint.NouvellesRencontres;
var
  i, j: Integer;
begin
   EcritLog('');
   EcritLog('Nouvelles rencontres : ');

   with Data do
   begin
     for i:=1 to NbJou do
       for j := i + 1 to nbJou do
     begin
        if (((ConNom[i] and Puis2(j-1))<>(ConNomDbl[i] and Puis2(j-1)))
           or ((ConNom[j] and Puis2(i-1))<>(ConNomDbl[j] and Puis2(i-1))))
        then if (ConnuDeNom[i,j] and ConnuDeNom[j,i])
        then
        begin
           Points.PtsNouvContact(i, j);
           Points.PtsNouvContact(j, i);
           EcritLog(Jou[i].Pseudo + ' <=> ' + Jou[j].Pseudo);
        end
        else if ConnuDeNom[i,j] then
        begin
           Points.PtsNouvContact(i, j);
           EcritLog(Jou[i].Pseudo + ' a vu ' + Jou[j].Pseudo);
        end
        else
        begin
           EcritLog(Jou[i].Pseudo + ' a vu ' + Jou[j].Pseudo);
        end;
     end;
   end;
end;

procedure TEvalMiseAuPoint.UpdateVisionMondes;
var
  i, j, k: Integer;
begin
  with Data do
  begin
     { Mondes vus depuis le d�but de la partie (02/02/96)}
     for i := 1 to NbMonde do for j := 1 to NbJou do
         AvuConnect[i, j] := AvuConnect[i, j] or Connu[i, j];

     for i := 1 to NbJou do
     begin
       if Jou[i].RAD = 1 then
       begin
          for j := 1 to NbMonde do if M^[j].Proprio = i then
            AVuCoord[j, i] := True;
       end;

       if Jou[i].RAD >= 2 then
       begin
          for j := 1 to NbMonde do if EstConnu(j, i) then
          begin
            AVuCoord[j, i] := True;

            {RAD = 3 -> on voit les coord des mondes voisins}
            if (Jou[i].RAD >= 3) and (M^[j].Statut and Puis2(6) = 0) then
            begin
              for k := 1 to 8 do
                if M^[j].Connect[k] > 0 then
                  AVuCoord[M^[j].Connect[k], i] := True;
            end;
          end;
       end;
     end;
  end;

end;

Procedure TEvalMiseAuPoint.Finalise;
var
  md, fl, fl2, tr : word;
  StatsEspionnage: TStatsEspionnage;


begin
   VisibiliteMondes;
   Explo;

   with Data do
   begin

     CompteursCorruption;

     ChercheConNom;

     ResetExploCM;

     for md:=1 to NbMonde do
     begin
        CalculePopLibre(md);
        DecrementeCompteurPillage(md);

        AugmenteCompteurPossessionEtCM(md);

        ME^[md].IndLibre := CalculeIndActive(md);

        if M^[md].Conv = 0 then
          M^[md].ProprConv := 0;

        MetEnContact(M^[md].Proprio, M^[md].ProprConv);

        for tr:=1 to NbTres do
          // Si tr�sor sur le monde
          if ((T^[tr].Localisation = md) and TresorSurMonde[tr]) then
           T^[tr].Proprio := M^[md].Proprio;
     end;

     for fl:=1 to NbFlotte do
      if F^[fl].Localisation > 0 then
     begin
        MetEnContact(F^[fl].Proprio, M^[F^[fl].Localisation].Proprio);

        MetEnContact(F^[fl].Proprio, M^[F^[fl].Localisation].ProprConv);

        for fl2 := 1 to NbFlotte do
          if F^[fl2].Localisation = F^[fl].Localisation then
           MetEnContact(F^[fl].Proprio,F^[fl2].Proprio);

        for tr := 1 to NbTres do
          if ((T^[tr].Localisation = fl) and TresorSurFlotte[tr]) then
            T^[tr].Proprio := F^[fl].Proprio;

        UpdateContenuFlotte(fl);
     end;

     NouvellesRencontres;

     TriOrdres;

     UpdateVisionMondes;

     StatsEspionnage := TStatsEspionnage.Create(Data, Espions);
     StatsEspionnage.CalculeStatsEspionnage;
     StatsEspionnage.Free;

     TriScores;
   end;

end;


{ Calcul des industries actives d'un monde }
function TEvalMiseAuPoint.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalMiseAuPoint.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

Function TEvalMiseAuPoint.CalculeIndActive(md: integer): integer;
var
  i, IA, max: integer;
  VaisAdv, VaisPossed: integer;
begin
  VaisPossed := 0;
  VaisAdv := 0;
  with Data do
  begin
    for i := 1 to NbFlotte do
      if ((F^[i].Localisation = md) and (not EstEnPaix(i)) and
        (not EstAllie(F^[i].Proprio, M^[md].Proprio)) and
        (F^[i].Proprio <> M^[md].Proprio)) then
        Inc(VaisAdv, F^[i].NbVC)
      else if ((F^[i].Localisation = md) and (F^[i].Proprio = M^[md].Proprio))
      then
        Inc(VaisPossed, F^[i].NbVC);
    Inc(VaisPossed, M^[md].VI * 2 + M^[md].VP);

    { Modif 24/02/96 : on emp�che les indutries de produire en
      partant de ce qui aurait produit sans les vaisseaux et non
      pas du nombre d'industries totales. Ca a une importance s'il
      n'y a pas assez de MP }

    IA := M^[md].Ind;
    if M^[md].MP < IA then
      IA := M^[md].MP;
    max := M^[md].Pop;
    if max < M^[md].Robot then
      max := M^[md].Robot;
    if max < IA then
      IA := max;

    if VaisPossed < VaisAdv then
    begin
      if IA + VaisPossed - VaisAdv < 0 then
        IA := 0
      else
        IA := IA + VaisPossed - VaisAdv;
    end;
  end;
  Result := IA;
end;


procedure TEvalMiseAuPoint.Init;
begin
  inherited;
  ConNomDbl := Data.ConNom;
end;

end.
