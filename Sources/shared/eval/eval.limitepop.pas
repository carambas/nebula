unit Eval.LimitePop;

Interface

uses Avateval, eval.etape;

type
  TEvalLimitePopulation = class(TEvalEtape)
  public
    const TYPE_ORDRE = 4;
    const ETAPE_EVAL = 4;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure IndConstrPop(md, x: word);
  end;

Implementation

uses Avatunit;

Const ConsPop : Array[1..NBCLASSE] of word = (1,1,1,1,1,1,1);

procedure TEvalLimitePopulation.Finalise;
begin
  inherited;

end;

function TEvalLimitePopulation.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalLimitePopulation.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalLimitePopulation.IndConstrPop(md,x : word);
var n : word;
begin
  with Data do
  begin
    n:=ConsPop[jou[O.Auteur].Classe];
    if O.Auteur<>M^[md].Proprio then Erreur(4)
    else if ME^[md].IndLibre<n*x then
    begin
       Erreur(12);
       x:=ME^[md].IndLibre div n;
       OK:=True;
    end;
    if OK then
    begin
       Inc(M^[md].MaxPop,x);
       Dec(M^[md].MP,x*n);
       Dec(ME^[md].PopLibre,x*n);
       Dec(ME^[md].IndLibre,x*n);
    end;
  end;
end;

procedure TEvalLimitePopulation.Init;
begin
  inherited;

end;

procedure TEvalLimitePopulation.EvalueOrdre;
begin
  IndConstrPop(O.O[1],O.O[2]);
end;


end.
