unit Eval.Capture;

Interface

uses
  Avateval, eval.etape;

type
  TEvalCapture = class(TEvalEtape)
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = 17;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;
  end;

Implementation

uses Avatunit;

procedure TEvalCapture.EvalueOrdre;
begin
  inherited;

end;

Procedure TEvalCapture.Finalise;
var md,fl,tr,j : word;
    TV : word;
    V : array[0..NB_JOU_MAX] of word;
    cap : boolean;
begin
   with Data do for md:=1 to NbMonde do
   begin
      cap:=False;
      for j:=1 to NbJou do V[j]:=0;
      TV := M^[md].VI + M^[md].VP;
      V[M^[md].Proprio]:=TV;
      for fl:=1 to NbFlotte do if ((F^[fl].Localisation=md)and(not
         EstEnPaix(fl))) then
      begin
         inc(TV,F^[fl].NbVC + F^[fl].NbVT);
         inc(V[F^[fl].Proprio],F^[fl].NbVC + F^[fl].NbVT);
      end;
      if TV=0 then
      begin
         for fl:=1 to NbFlotte do if F^[fl].Localisation=md then
            if (F^[fl].NbVC + F^[fl].NbVT)=0 then F^[fl].Proprio:=0
      end
      else for j:=1 to NbJou do if V[j]=TV then
      begin
         cap:=True;
         if ((M^[md].Proprio<>j)and(not EstAllie(j,M^[md].Proprio))and
            ((M^[md].Robot=0)or(jou[j].Classe=5))
            and(M^[md].Conv<M^[md].Pop)and(M^[md].Pop>0)) then
         begin
            M^[md].Proprio:=j;
            ME^[md].Statut:=(ME^[md].Statut or Puis2(0));
            // NbExplOCM repasse � 0 si Nb Tour Possession > 7
            //if M^[md].Duree + M^[md].NbExploCM * 3 > 7 then
            //  M^[md].NbExploCM := 0;

            //M^[md].Duree:=0;  lors de la Mise au point
            for tr:=1 to NbTres do if ((T^[tr].Localisation=md)and
               (T^[tr].Statut=0)) then T^[tr].Proprio:=j;
         end;
         for fl:=1 to NbFlotte do if ((F^[fl].Localisation=md)and
            (F^[fl].Proprio<>j)and(F^[fl].NbVC + F^[fl].NbVT=0)) then
         begin
            F^[fl].Proprio:=j;
            FE^[fl].Statut:=(FE^[fl].Statut or Puis2(0));
            F^[fl].Statut:=(F^[fl].Statut and (not Puis2(0)));
            for tr:=1 to NbTres do if ((T^[tr].Localisation=fl)and
               (T^[tr].Statut=1)) then T^[tr].Proprio:=j;
         end;
      end;
      if ((not cap)and(TV>0)) then
      begin
         for fl:=1 to NbFlotte do if ((F^[fl].Localisation=md)and(F^[fl]
            .NbVC + F^[fl].NbVT=0)) then
         begin
            F^[fl].Proprio:=0;
         end;
         if ((M^[md].VP+M^[md].VI=0)and(M^[md].Proprio>0)) then
            for fl:=1 to NbFlotte do if ((F^[fl].Localisation=md)and
            (not EstAllie(F^[fl].Proprio,M^[md].Proprio))and(F^[fl].Proprio<>
            M^[md].Proprio)and(not EstEnPaix(fl))and(F^[fl].NbVC + F^[fl].NbVT>0)
            and(M^[md].Robot=0)and
            (not((M^[md].Pop>0)and(M^[md].Conv=M^[md].Pop)))) then
               M^[md].Proprio:=0;
      end;
   end;
end;

function TEvalCapture.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL
end;

function TEvalCapture.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalCapture.Init;
begin
  inherited;

end;

end.
