unit Eval.DechargeMP;

Interface

uses Avateval,eval.etape;

type

  TEvalDechargeMP = class(TEvalEtape)
  public const
    TYPE_ORDRE = 8;
    ETAPE_EVAL = 8;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;

  private
    procedure DechLesMP(fl, x: word);
  end;

Implementation

uses erreurseval, Utils;

procedure TEvalDechargeMP.DechLesMP(fl : word;x : word);
begin
  with Data do
  begin
    if O.Auteur<>F^[fl].Proprio then Erreur(ERR_PAS_PROPRIO_FLOTTE)
    else if F^[fl].MP<x then
    begin
       Erreur(ERR_FLOTTE_PAS_ASSEZ_MP);
       x:=F^[fl].MP;
       OK:=True;
    end;
    if OK then
    begin
       Inc(M^[F^[fl].Localisation].MP,x);
       Inc(MPDech^[F^[fl].Localisation]^[F^[fl].Proprio],x);
       Dec(F^[fl].MP,x);
    end;
  end;
end;

Procedure TEvalDechargeMP.Init;
var
  i,j : word;
begin
   for i:=1 to Data.NbMonde
    do for j:=1 to Data.NbJou
      do MPDech^[i]^[j]:=0;
end;

Procedure TEvalDechargeMP.EvalueOrdre;
var
  n : word;
begin
  OK:=True;
  n:=O.O[2];
  if n = 0 then n:=Data.F^[O.O[1]].MP;
  DechLesMP(O.O[1],n);
end;

procedure TEvalDechargeMP.Finalise;
begin
  inherited;

end;

function TEvalDechargeMP.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalDechargeMP.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

end.
