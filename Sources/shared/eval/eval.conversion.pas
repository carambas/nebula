unit Eval.Conversion;

Interface

uses
  Avateval,
  Avatunit,
  eval.etape;

type
  // Utilisation classe interm�diaire car sert �g�lament de classe anc�tre
  // pour TEvalEmigration
  TEvalEtapeConflitsConversion  = class(TEvalEtape)
    procedure ResolConflitsConversion(var Conv: TRelationJou; md: integer);
  end;

type
  TEvalConversion = class(TEvalEtapeConflitsConversion)
  public
  public const
    TYPE_ORDRE = -1;
    ETAPE_EVAL = 16;

  public
    procedure EvalueOrdre; override;
    procedure Init; override;
    procedure Finalise; override;

  protected
    function GetTypeOrdre: Integer; override;
    function GetEtapeEval: Integer; override;
  end;

Implementation

procedure TEvalConversion.EvalueOrdre;
begin
  inherited;

end;

Procedure TEvalConversion.Finalise;
var i,md,fl : word;
    Conv : TIntMondesJou;
    QuiConverti : TIntMondesJou;
    c : TRelationJou;
begin
  FillChar(QuiConverti, sizeof(QuiConverti), 0);
  FillChar(Conv, sizeof(Conv), 0);

  with Data do
  begin
    // 1. On recence
    // Flottes
    for fl := 1 to NbFlotte do
      if F[fl].Proprio > 0 then
        if ((jou[F[fl].Proprio].Classe=6)and(not EstEnPaix(fl))) then
          inc(QuiConverti[F[fl].Localisation, F[fl].Proprio], F[fl].NbVC);

    // Mondes
    for md := 1 to NbMonde do
    begin
      if M[md].ProprConv > 0 then
      begin
        inc(QuiConverti[md, M[md].ProprConv], M[md].Conv);
        inc(Conv[md, M[md].ProprConv], M[md].Conv);
      end;

      if M[md].Proprio > 0 then
        if Jou[M[md].Proprio].Classe = 6 then
          inc(QuiConverti[md, M[md].Proprio], M[md].VP);
    end;

    // 2. R�solution
    for md := 1 to NbMonde do
    begin
      for i := 1 to NbJou do
      begin
        inc(Conv[md, i], QuiConverti[md, i] div 4);
        if random(4) < (QuiConverti[md, i] mod 4) then
          inc(Conv[md, i]);
      end;
    end;

    // 3. R�solution conflits
    for md := 1 to NbMonde do
    begin
      for i := 1 to NbJou do
        C[i] := Conv[md, i];
      ResolConflitsConversion(C, md);
      
      // On enl�ve les convertis potentiels en trop
      if M[md].Conv >= M[md].Pop then
        M[md].Conv := M[md].Pop;

      // Capture le cas �ch�ant
      if ((M[md].Pop > 0) and (M[md].Conv = M[md].Pop)) then
         if M[md].Proprio <> M[md].ProprConv then
         begin
            M[md].Proprio := M[md].ProprConv;
            ME[md].Statut := (ME[md].Statut or Puis2(0));
            M[md].Duree:=0;
         end;
    end;
  end;

end;

{ TEvalEtapeConflitsConversion }

// ResolConflits : appell�e apr�s �migration et conversion
function TEvalConversion.GetEtapeEval: Integer;
begin
  Result := Self.ETAPE_EVAL;
end;

function TEvalConversion.GetTypeOrdre: Integer;
begin
  Result := Self.TYPE_ORDRE;
end;

procedure TEvalConversion.Init;
begin
  inherited;

end;



procedure TEvalEtapeConflitsConversion.ResolConflitsConversion(var Conv : TRelationJou; md : integer);
var i : word;
    TC, max, nbelimines, nbjou_presents : word;
    alea, x, moy : real;
    Proba : array[0..NB_JOU_MAX] of real;
begin
  with Data do
  begin
    // Recensement et �limination des faibles
    repeat
      // Recencement
      FillChar(Proba, sizeof(Proba), 0);
      nbelimines := 0;     // nb de joueurs �limin�s
      nbjou_presents := 0; // Nombre de joueurs ayant des conv potentiels
      TC := 0;             // nb total de C
      max := 0;            // nb max de conv potentiels
      moy := 0;            // moyenne des convertis pr�sents
      for i:=1 to NbJou do
      begin
        inc(TC,Conv[i]);
        if Conv[i] > max then
        begin
          max := Conv[i];
        end;
        if Conv[i] > 0 then
          inc(nbjou_presents)
      end;

      // Elimination
      if TC > 0 then
      begin
        moy := TC / nbjou_presents;
        for i := 1 to NbJou do
        begin
          if (Conv[i] > 0) and
             ((Conv[i] <= max / 3) or (Conv[i] <= moy / 2)) then
          begin
            Conv[i] := 0;
            inc(nbelimines);
          end;
        end;
      end;
    until nbelimines = 0;

    // tirage au sort si n�cessaire
    if TC > 0 then
    begin
      // Avec les �liminations pr�c�dentes, on a forc�ment 0 <= proba(i) <= 1
      for i := 1 to NbJou do
        if Conv[i] > 0 then
          Proba[i] := (2*(Conv[i] - moy) + moy) / TC;

       i := 0;
       x := 0;
       alea := Random;
       Repeat
          inc(i);
          x := x + Proba[i];
       Until x > alea;
       M[md].ProprConv := i;
       M[md].Conv := Conv[i];
    end;
  end;
end;


end.
