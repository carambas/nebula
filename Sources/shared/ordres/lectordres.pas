unit lectordres;

interface

uses AvatUnit, NebData;

type
  TNebOrdres = class
    public
      constructor Create; overload;
      constructor Create(theData : TNebData); overload;
      destructor Destroy; override;
      procedure Traite(St : string; var O : TOrdre; var jn : integer);
      class function OrdreCh(O : TOrdre; Jou : PLesJoueurs) : string; static;
      function OrdreChSimple(O : TOrdre) : string;
      class Procedure ParseOrdre(St : string; var O : TOrdre); static;
      class function EnleveCommentsAndSpaces(st : string) : string; static;
      function CheckSyntax(St : string) : boolean;
    private
      Data : TNebData;
      function OrdreChaineToOrdreSyntaxe(St : string; var O : TOrdre) : string;
      class function CheckNbNombres(O: TOrdre; index: integer): boolean; static;
  end;


implementation

uses Utils, SysUtils, WideStrUtils, Avateval, erreurseval;

const NbOrdresDif = 124;

// OrdreTypes

const TabOrdresBrut : array [1..NbOrdresDif, 1..2] of string = (
// Ordres sp�ciaux � ex�cuter avant l'�val (bots)
('BOT x', '0.1'),

// Ordres de d�claration :
('A : j', '1.1'),
('E : j', '1.2'),
('C : j', '1.3'),
('N : j', '1.4'),
('J : j', '1.5'),
('F_f P', '1.7'),
('F_f G', '1.8'),
('Z m', '1.6'),
('Z', '1.6'),
('CP : j', '1.9'),
('NCP : j', '1.10'),
('DP : j', '1.11'),
('NDP : j', '1.12'),
('PI : j', '1.13'),
('NPI : j', '1.14'),

//Ordres de sondage :
('VI M_m S M_m', '2.1'),
('VP M_m S M_m', '2.2'),
('F_f S M_m', '2.3'),

//Ordres de construction :
('M_m C x VC F_f', '3.1'),
('M_m C x VT F_f', '3.14'),
('M_m C x VI', '3.2'),
('M_m C x VP', '3.3'),
('M_m C x R', '3.4'),
('M_m C x I', '6.0'),
('M_m C x P', '4.0'),
('VI M_m C x I', '7.0'),
('F_f C BOMBE', '15.0'),
('M_m T x R I', '6.1'),
('M_m T x R F_f', '3.15'),
('M_m T x R VI', '3.6'),
('M_m T x R VP', '3.7'),

//Ordres d'espionnage :
('M_m C x E x',   '3.5'),
('M_m C x E CM',  '3.16'),
('M_m C x E P',   '3.17'),
('M_m C x E CAR', '3.18'),
('M_m C x E M',   '3.19'),
('M_m C x E F',   '3.20'),
('M_m C x E I',   '3.21'),
('M_m C x E V',   '3.22'),
('M_m C x E T',   '3.23'),
('M_m C x E IA',  '3.24'),

//Ordres de recherche technologique :
('M_m C x DEP', '3.8'),
('M_m C x ATT', '3.9'),
('M_m C x DEF', '3.10'),
('M_m C x RAD', '3.11'),
('M_m C x CAR', '3.13'),
('M_m C x ALI', '3.12'),

//Ordres de d�chargement :
('F_f D x MP', '8.0'),
('F_f D MP', '8.0'),
('F_f D x PC', '9.0'),
('F_f D PC', '9.0'),
('F_f D x P', '5.8'),
('F_f D P', '5.8'),
('F_f D x C', '5.6'),
('F_f D C', '5.6'),
('F_f D x R', '5.7'),
('F_f D R', '5.7'),
('F_f D x N', '5.5'),
('F_f D N', '5.5'),
('T_t M', '10.0'),

//Ordres de transfert :
('M_m T x VI VP', '11.1'),
('M_m T x VP VI', '11.2'),
('M_m T x VI F_f', '11.3'),
('M_m T x VP F_f', '11.4'),
('M_m T x VI VT F_f', '11.11'),
('M_m T x VP VT F_f', '11.12'),

('F_f T x VC VI', '11.5'),
('F_f T x VC VP', '11.6'),
('F_f T x VC F_f', '11.7'),
('F_f T x VT F_f', '11.8'),
('F_f T x VC VT', '11.9'),
('F_f T x VC VT F_f', '11.10'),

//Ordres de chargement :
('F_f C x MP', '12.0'),
('F_f C MP', '12.0'),
('F_f C x P', '12.4'),
('F_f C P', '12.4'),
('F_f C x C', '12.2'),
('F_f C C', '12.2'),
('F_f C x R', '12.3'),
('F_f C R', '12.3'),
('F_f C x N', '12.1'),
('F_f C N', '12.1'),
('T_t F_f', '13.0'),

//Ordres de migration :
('M_m E x P M_m', '5.1'),
('M_m E x C M_m', '5.2'),
('M_m E x N M_m', '5.3'),
('M_m E x R M_m', '5.4'),

//Ordres de tir :
('VP M_m * C', '14.1'),
('VP M_m * N', '14.2'),
('VI M_m * F_f', '14.3'),
('VP M_m * F_f', '14.4'),
('F_f * I', '14.5'),
('F_f * P', '14.6'),
('F_f * M', '14.7'),
('F_f * F_f', '14.8'),
//('VP M_m ? C', '14.129'),
//('VP M_m ? N', '14.130'),
('VI M_m ? F_f', '14.131'),
('VP M_m ? F_f', '14.132'),
('F_f ? I', '14.133'),
('F_f ? P', '14.134'),
('F_f ? M', '14.135'),
('F_f ? F_f', '14.136'),

//Ordres d'attaques sp�ciales :
('F_f L BOMBE', '14.9'),
('F_f T x R', '14.10'),
('M_m P', '18.0'),
('M_m = s', '18.1'),

//Ordres de cadeaux :
('F_f C : j', '20.1'),
('M_m C : j', '20.2'),

//Ordres d'exploration (MEVA)
('F_f E x VC P', '13.1'),
('F_f E x VC CM', '13.2'),

//Ordres de mouvement :
('F_f MM m', '14.11'),
('F_f MM m m', '14.11'),
('F_f MM m m m', '14.11'),
('F_f MM m m m m', '14.11'),
('F_f MM m m m m m', '14.11'),
('F_f MM m m m m m m', '14.11'),
('F_f MM m m m m m m m', '14.11'),
('F_f M_m', '14.11'),
('F_f M_m M_m', '14.11'),
('F_f M_m M_m M_m', '14.11'),
('F_f M_m M_m M_m M_m', '14.11'),
('F_f M_m M_m M_m M_m M_m', '14.11'),
('F_f M_m M_m M_m M_m M_m M_m', '14.11'),
('F_f M_m M_m M_m M_m M_m M_m M_m', '14.11')
);

type
  ParseMode = (pm_Normal, pm_Nombre, pm_Chaine);

const
  espaces = [' ', '_', #9, #$A0];
  chiffre = ['0'..'9'];
  Commentaires : array[1..2] of Char = (';','#');
  nombres = ['j','m','t','f','x'];

var
  TabOrdres : array [1..NbOrdresDif] of string;
  TabOrdresTypes : array[1..NbOrdresDif, 1..2] of integer;

function OrdreTypeToOrdreSyntaxe(st : string) : string;
var
  c : Char;
begin
  Result := '';
  for c in st do
  begin
      if CharInSet(c, nombres) then
      Result := Result + 'n'
    else if not CharInSet(c, espaces) then
      Result := Result + c;
  end;
end;

// Analyse lexicale d'un ordre
//function OrdreChaineToOrdreSyntaxe(St : string; var O : TOrdre; RepFic, NomPartie : string) : string;
function TNebOrdres.OrdreChaineToOrdreSyntaxe(St : string; var O : TOrdre) : string;
var
  c : Char;
  pm : ParseMode;
  NoNombre : integer;
  NombreCh : string;
  chaine : string;

  // Sauver le nombre dans le TOrdre
  procedure FinNombre;
  var
    Err : integer;
  begin
    Result := Result + 'n';
    if NoNombre <= 8 then
    Val(NombreCh, O.O[NoNombre], Err);
    NombreCh := '';
    pm := pm_Normal;
  end;

  // Sauver la chaine dans le fichier des noms et l'index associ� dans le TOrdre
  procedure FinChaine;
  begin
    Result := Result + 's';
    if NoNombre <= 8 then
      O.DataCh := chaine;
    chaine := '';
    pm := pm_Normal;
  end;

begin
  Result := '';
  //FillChar(O, sizeof(O), 0);
  chaine := '';
  NoNombre := 0;
  NombreCh := '';
  pm := pm_Normal;
  for c in St do
  begin
    // Chaine de caract�re en cours
    if pm = pm_Chaine then
    begin
      if c = '"' then
      begin
        FinChaine;
        pm := pm_Normal
      end
      else
        chaine := chaine + c;
    end

    //Pas de chaine de caract�re en cours
    else
    begin
      // D�but d'un nombre
      if CharInSet(c, chiffre) then
      begin
        if pm <> pm_Nombre then
        begin
          pm := pm_Nombre;
          inc(NoNombre);
        end;
        NombreCh := NombreCh + c;
      end
      else if pm = pm_Nombre then
        FinNombre;

      //D�but d'un commentaire
      if String(Commentaires).IndexOf(c) >=0 then
      begin
        if pm = pm_Nombre then
          FinNombre;
        Exit;
      end;

      // D�but d'une chaine de caract�res
      if c = '"' then
      begin
        pm := pm_Chaine;
        inc(NoNombre);
      end;

      // Espace : on ne fait rien, fin d'un cnombre.
      if CharInSet(c, espaces) then
      begin
        pm := pm_Normal;
      end

      // Pas d'espace et mode normal: on ajoute le caract�re en maj au r�sultat
      else if pm = pm_Normal then
      begin
        Result := Result + UpCase(c);
      end;
    end;
  end;
  if pm = pm_Nombre then
    FinNombre;
end;

//Procedure Traite(St : string; var O : TOrdre; var jn : integer; RepFic, NomPartie : string);
Procedure TNebOrdres.Traite(St : string; var O : TOrdre; var jn : integer);

  procedure SyntaxError(var O : TOrdre; NoErreur : integer);
  begin
    O.TypeO:=21;
    O.STypeO:=0;
    O.Erreur:=NoErreur;
  end;

   procedure TypeOIndex(index : integer);
   begin
     O.TypeO := TabOrdresTypes[index, 1];
     O.StypeO := TabOrdresTypes[index, 2];
   end;

   //Ajouter les tests sur les intervalles

  procedure CheckIntervalleNombres(var O : TOrdre; index : integer);

    procedure TestIntervalleMonde(var O : TOrdre; NoMonde : integer);
    var
      NbMonde : integer;
    begin
      if Assigned(Data) then
        NbMonde := Data.NbMonde
      else
        NbMonde := NB_MONDE_MAX;

      if (NoMonde <= 0) or (NoMonde > NbMonde) then
        SyntaxError(O, ERR_MONDE_EXISTE_PAS);
    end;

    procedure TestIntervalleFlotte(var O : TOrdre; NoFlotte : integer);
    var
      NbFlotte : integer;
    begin
      if Assigned(Data) then
        NbFlotte := Data.NbFlotte
      else
        NbFlotte := NB_FLOTTE_MAX;

      if (NoFlotte <= 0) or (NoFlotte > NbFlotte) then
        SyntaxError(O, ERR_FLOTTE_EXISTE_PAS);
    end;

    procedure TestIntervalleJou(var O : TOrdre; NoJou : integer);
    var
      NbJou : integer;
    begin
      if Assigned(Data) then
        NbJou := Data.NbJou
      else
        NbJou := NB_JOU_MAX;

      if (NoJou <= 0) or (NoJou > NbJou) then
        SyntaxError(O, ERR_JOU_EXISTE_PAS);
    end;

    procedure TestIntervalleTresor(var O : TOrdre; NoTresor : integer);
    var
      Erreur : boolean;
    begin
      Erreur := False;
      if (NoTresor <= 0) or (NoTresor > NbTres) then
        Erreur := True;
      if not Erreur and Assigned(Data) then
        if Data.T[NoTresor].Localisation = 0 then
          Erreur := True;
      if Erreur then
        SyntaxError(O, ERR_TRESOR_EXISTE_PAS);
    end;

  var
    c : Char;
    NbNombres1 : integer;
  begin
    NbNombres1 := 0;

    // On regarde le nombre de nombres dans la description des ordres
    for c in TabOrdresBrut[index, 1] do
      if CharInSet(c, nombres) then
      begin
        inc(NbNombres1);
        // Au passage on en profite pour v�rifier l'intervalle des num�ros
        case c of
          'j' : TestIntervalleJou(O, O.O[NbNombres1]);
          'm' : TestIntervallemonde(O, O.O[NbNombres1]);
          'f' : TestIntervalleFlotte(O, O.O[NbNombres1]);
          't' : TestIntervalleTresor(O, O.O[NbNombres1]);
        end;
      end;
  end;

var
   i : integer;
   OrdreSyntaxe : string;
begin
  InitOrdre(O);
  St := St.Trim;

  // Nouveau joueur
  if St.StartsWith(BeginCh) then
    try
      jn := St.Substring(BeginCh.Length, 2).ToInteger;
    except
      on E : EConvertError do SyntaxError(O, ERR_SYNTAXE);
    end
   // Fin d'un joueur
   else if St.StartsWith(EndCh) then
     jn := 0
   else // pas une cha�ne de contr�le
   begin
     O.OrdreCh := St;
     if (jn > 0) and (jn <= NB_JOU_MAX) then
       O.Auteur := jn;

     // Ici on fait le parsing de la cha�ne transmise

     //OrdreSyntaxe := OrdreChaineToOrdreSyntaxe(St, O, RepFic, NomPartie);
     OrdreSyntaxe := OrdreChaineToOrdreSyntaxe(St, O);

     // On cherche l'ordre qui correspond
     for i := 1 to NbOrdresDif do
       if (OrdreSyntaxe = TabOrdres[i]) then
       begin
         TypeOIndex(i);
         CheckIntervalleNombres(O, i);
         Break;
       end;

     if (O.TypeO = -1) and (OrdreSyntaxe <> '') then SyntaxError(O, ERR_SYNTAXE);
   end;
end;

class function TNebOrdres.CheckNbNombres(O : TOrdre; index : integer) : boolean;
var
  c : Char;
  i : Integer;
  NbNombres1, NbNombres2 : integer;
begin
  NbNombres1 := 0;
  NbNombres2 := 8;

  // On regarde le nombre de nombres dans la description des ordres
  for c in TabOrdresBrut[index, 1] do
    if CharInSet(c, nombres) then
      inc(NbNombres1);

  // On regarde le nombre de nombres dans l'ordre
  for i := 1 to 8 do
    if O.O[i] = 0 then
  begin
    NbNombres2 := i-1;
    Break;
  end;
  Result := NbNombres1 = NbNombres2
end;

// On ne s'occupe pas des pseudos ici
function TNebOrdres.OrdreChSimple(O : TOrdre) : string;
var
  c : Char;
  i : integer;
  NoNombre : integer;
begin
  Result := '';
  NoNombre := 0;

  if O.TypeO = 21 then
  begin
    Result := O.OrdreCh;
  end;

  for i := 1 to NbOrdresDif do
    if (TabOrdresTypes[i, 1] = O.TypeO) and (TabOrdresTypes[i, 2] = O.StypeO) then
      if CheckNbNombres(O, i) then
  begin
    for c in TabordresBrut[i, 1] do
      if CharInSet(c, nombres) then
      begin
        inc(NoNombre);
        Result := Result + IntToStr(O.O[NoNombre]);
      end
      else
        Result := Result + c;
    Exit;
  end;
end;

class function TNebOrdres.OrdreCh(O : TOrdre; Jou : PLesJoueurs) : string;
var
  c : Char;
  i : integer;
  NoNombre : integer;
begin
  Result := '';
  NoNombre := 0;

  if O.TypeO = 21 then
  begin
    Result := '(Impossible de retrouver l''ordre)';
    Exit;
  end;

  for i := 1 to NbOrdresDif do
    if (TabOrdresTypes[i, 1] = O.TypeO) and (TabOrdresTypes[i, 2] = O.StypeO) then
      if CheckNbNombres(O, i) then
  begin
      for c in TabordresBrut[i, 1] do
      if CharInSet(c, nombres) then
      begin
        inc(NoNombre);
        if (c = 'j') and (Jou <> nil) then
          // Mettre le pseudo du joueur � la place
          Result := Result + Jou^[O.O[NoNombre]].Pseudo
        else
          Result := Result + IntToStr(O.O[NoNombre]);
      end
      else
      begin
        if (c = 's') then
        begin
          Result := Result + Format('"%s"', [O.DataCh]);
        end
        else
        begin
          Result := Result + c;
        end;
      end;
    Exit;
  end;
end;

// Sp�cifique Nebutil

class Procedure TNebOrdres.ParseOrdre(St : string; var O : TOrdre);
var
  idx : integer;
  NoNombre : integer;
  splitted : TArray<String>;
begin
  InitOrdre(O);

  // On supprime le commentaire s'il y a
  idx := St.IndexOfAny(Commentaires);
  if idx <> -1 then St := St.Substring(0, idx);

  St := St.Trim;

  // D�coupage de l'ordres en max 16 mots selon les s�parateurs par ' ' ou '_' ou #9
  // et remplissage du tableau d'ordre O
  splitted := St.split([' ', '_', #9], 16, TStringSplitOptions.ExcludeEmpty);

  // Cas particulier de l'ordre de nommage de monde
  if (length(splitted) > 3) and (splitted[0].ToUpper = 'M') and
    EstNombre(splitted[1]) and (splitted[2] = '=') then
  begin
    O.Ch[1] := splitted[0].ToUpper;
    O.Ch[2] := splitted[1];
    O.Ch[3] := splitted[2];
    St := St.Substring(St.IndexOf('=') + 1);
  end
  else
  begin
    // Copie en majuscule des �l�ments d�coup� dans le tableu des ordres
    // On profite aussi du parcour pour convertir les nombres
    NoNombre := 0;
    for idx := 1 to length(splitted) do
    begin
      O.Ch[idx] := splitted[idx-1].ToUpper;

      if EstNombre(O.Ch[idx]) and (NoNombre < Length(O.O)) then
      begin
        inc(NoNombre);
        O.O[NoNombre] := O.Ch[idx].ToInteger;
      end;
    end;
  end;

end;

class function TNebOrdres.EnleveCommentsAndSpaces(st : string) : string;
var
  p : integer;
begin
  p := st.IndexOfAny(Commentaires);
  if p = -1 then
    Result := St.Trim
  else
    Result := St.Substring(0, p).Trim;
end;

function TNebOrdres.CheckSyntax(St : string) : boolean;
var
  O : TOrdre;
  bid : integer;
begin

  O.Erreur := 0;
  bid := 0;
  Traite(st, O, bid);
  Result := (O.erreur <> 21);
end;

constructor TNebOrdres.Create(theData: TNebData);
begin
  inherited Create;

  Data := theData;
end;

constructor TNebOrdres.Create;
begin
  Create(nil);
end;

destructor TNebOrdres.Destroy;
begin
  inherited;
end;

var
  i, index: Integer;

Initialization

  for i := 1 to NbOrdresDif do
  begin
    Tabordres[i] := OrdreTypeToOrdreSyntaxe(TabOrdresBrut[i, 1]);
    index := TabOrdresBrut[i, 2].IndexOf('.');
    TabOrdresTypes[i, 1] := TabOrdresBrut[i, 2].Substring(0, index).ToInteger();
    TabOrdresTypes[i, 2] := TabOrdresBrut[i, 2].Substring(index + 1).ToInteger();
  end;


end.
