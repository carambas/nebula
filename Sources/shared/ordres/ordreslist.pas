unit ordreslist;

interface

uses Generics.Collections, Generics.Defaults, Avatunit, Classes;

type

  TListeTypeOrdres = TList<TOrdre>;
  TListeOrdres = class(TPersistent)
    private
      fOrdres: array[0..NBTYPEORDRES] of TListeTypeOrdres;

      function GetOrdre(TypeOrdre: Integer; Index: Integer): TOrdre;
      procedure SetOrdre(TypeOrdre, Index: Integer; const Value: TOrdre);

    protected
      procedure AssignTo(Dest: TPersistent); override;

    public
      constructor Create;
      destructor Destroy; override;
      procedure Add(O: TOrdre);
      procedure Clear;
      function Count(TypeOrdre: Integer): Integer;
      procedure RemoveOrdresJoueur(numjou: INteger);
      function DumpOrdres: String;

      property Items[TypeOrdre, Index: Integer]: TOrdre read GetOrdre write SetOrdre; default;
  end;

implementation

uses SysUtils;

procedure TListeOrdres.Add(O: TOrdre);
begin
  fOrdres[O.TypeO].Add(O);
end;

procedure TListeOrdres.AssignTo(Dest: TPersistent);
var
  ListeOrdresDest: TListeOrdres;
  i: Integer;

begin
  if Dest is TListeOrdres then
  begin
    ListeOrdresDest := TListeOrdres(Dest);
    ListeOrdresDest.Clear;
    for i := Low(fOrdres) to High(fOrdres) do
      ListeOrdresDest.fOrdres[i].AddRange(fOrdres[i]);
  end
  else
    inherited;
end;

procedure TListeOrdres.Clear;
var
  i: Integer;
begin
  for i := Low(fOrdres) to High(fOrdres) do
    fOrdres[i].Clear;
end;

function TListeOrdres.Count(TypeOrdre: Integer): Integer;
begin
  Result := fOrdres[TypeOrdre].Count;

end;

constructor TListeOrdres.Create;
var
  i: Integer;
begin
  inherited Create;

  for i := Low(fOrdres) to High(fOrdres) do
    fOrdres[i] := TListeTypeOrdres.Create;
end;

destructor TListeOrdres.Destroy;
var
  i: Integer;
begin
  for i := Low(fOrdres) to High(fOrdres) do
    FreeAndNil(fOrdres[i]);

  inherited Destroy;
end;

function TListeOrdres.GetOrdre(TypeOrdre, Index: Integer): TOrdre;
begin
  Result := fOrdres[TypeOrdre][Index];
end;

procedure TListeOrdres.RemoveOrdresJoueur(NumJou: Integer);
var
  TypeOrdre, i: Integer;
begin
  for TypeOrdre := 1 to High(fOrdres) do // On ne d�marre volontairement pas de 0 car on ne supprime pas les ordres sp�ciaux
    for i := Count(TypeOrdre) - 1 downto 0 do
      if fOrdres[TypeOrdre][i].Auteur = NumJou then
        fOrdres[TypeOrdre].Delete(i);
end;

procedure TListeOrdres.SetOrdre(TypeOrdre, Index: Integer; const Value: TOrdre);
begin
  fOrdres[TypeOrdre][Index] := Value;
end;

// Liste des TOrdres sous forme d'une cha�ne
function TListeOrdres.DumpOrdres: string;
var
  i, typeo: integer;
  O: TOrdre;
  Liste: TStringList;
begin
  Liste := TStringList.Create;
  for typeo := 1 to NBTYPEORDRES do
  begin
    for i := 0 to Count(typeo) - 1 do
    begin
      O := Items[typeo, i];
      Liste.Add(Format('%d: %s (%d.%d)', [O.Auteur, O.OrdreCh, O.TypeO, O.STypeO]));
    end;
    Liste.Add(Format('-- fin typeo %d', [typeo]));
  end;
  Liste.Add('###');
  Result := Liste.Text;
  Liste.Free;
end;



end.

