unit chemin;

interface

uses
  Classes, NebData;

type
  TCritere = array[1..7] of integer;
  TChemin = class(TPersistent)
  private
    FChemin : array[0..10] of integer;
    FCount : integer;

    function GetItem(index : integer) : integer;
    function GetLast : integer;
  public
    function Count : integer;
    procedure Add(monde : integer);
    procedure RemoveLast;
    procedure Assign(Source : TPersistent); override;
    function MondeTraverse(monde : integer) : boolean;
    function MondeSurRoute(monde : integer) : boolean; // Chemin mais ni debut ni fin
    function ConnectSurChemin(monde1, monde2 : integer) : boolean;
    procedure EffaceChemin;

    property Last : integer read GetLast;
    property Items[index : integer] : integer read GetItem; default;
  end;

  TCheminList = class(TObject)
  private
    FCheminList : TList;
    FData : TNebData;

    function GetChemin(index : integer) : TChemin;
    procedure SetChemin(index : integer; item : TChemin);
    procedure ChercheChemin(DepMax, MdePrec, MdeDep, MdeArr : integer; Chemin : TChemin);
  public
    constructor Create(Data : TNebData);
    destructor Destroy; override;
    procedure AddCopy(item : TChemin);
    procedure Sort;
    function Count : integer;
    procedure RecenceChemins(MdeDep, MdeArr : integer);

    property Chemin[index : integer] : TChemin read GetChemin write SetChemin; default;
  end;

function GetMondeDanger(NoMonde : integer) : integer;

implementation

/// TChemin

function TChemin.GetItem(index : integer) : integer;
begin
  Result := FChemin[index];
end;

function TChemin.GetLast : integer;
begin
  Result := 0;
  if FCount > 0 then
    Result := FChemin[FCount - 1];
end;

function TChemin.Count : integer;
begin
  Result := FCount;
end;

procedure TChemin.Add(monde : integer);
begin
  if Count < High(FChemin) then
  begin
    inc(FCount);
    FChemin[FCount - 1] := monde;
  end;
end;

procedure TChemin.RemoveLast;
begin
  if FCount > 0 then
  begin
    FChemin[FCount - 1] := 0;
    dec(FCount);
  end;
end;

procedure TChemin.Assign(Source : TPersistent);
begin
  FCount := (Source as TChemin).FCount;
  FChemin := (Source as TChemin).FChemin;
end;

function TChemin.MondeSurRoute(monde : integer) : boolean; // Chemin mais ni debut ni fin
var
  i : integer;
begin
  Result := False;
  for i := 1 to FCount - 2 do
    if FChemin[i] = monde then
  begin
    Result := True;
    Exit;
  end
end;

function TChemin.MondeTraverse(monde : integer) : boolean;
var
  i : integer;
begin
  Result := False;
  for i := 0 to FCount - 1 do
    if FChemin[i] = monde then
  begin
    Result := True;
    Exit;
  end
end;

function TChemin.ConnectSurChemin(monde1, monde2 : integer) : boolean;
var
  i : integer;
begin
  Result := False;
  for i := 0 to FCount - 2 do
    if ((FChemin[i] = monde1) and (FChemin[i+1] = monde2)) or
       ((FChemin[i] = monde2) and (FChemin[i+1] = monde1)) then
  begin
    Result := True;
    Exit;
  end
end;


// TCheminList

var Data : TNebData;

procedure GetMondeCriteres(NoMonde : integer; var Critere : TCritere);
var
  VC : integer;
  k : integer;
begin
  //FillChar(Critere, sizeof(Critere), 0);
  // Crit�re 1 : Monde inconnu
  if not Data.AvuConnect[NoMonde, Data.NoJou] then
    inc(Critere[1]);

  // Crit�re 2 : Le monde n'est pas sur le CR � ce tour (peut avoir �t� vu avant)
  if not Data.Connu[NoMonde, Data.NoJou] then
    inc(Critere[2]);

  // Crit�re 3 : Pr�sence de vaisseaux ennemis ou neutres
  VC := 0;
  if Data.Confiance[Data.M[NoMonde].proprio] >= 3 then
    inc(VC, Data.M[NoMonde].VI + Data.M[NoMonde].VP);
  for k := 1 to Data.NbFlotte do
    if (Data.F[k].Localisation = NoMonde) and (Data.Confiance[Data.F[k].Proprio] >= 3) then
      inc(VC, Data.F[k].NbVC + Data.F[k].NbVT);
  inc(Critere[3], VC);

  // Crit�re 4 : le monde appartient � un non alli�
  if Data.Confiance[Data.M[NoMonde].proprio] >= 3 then
    inc(Critere[4]);

  // Crit�re 5 : tenter de passer par des mondes � nous qu'on a pr�t�
  // et qu'on n'aurait pas sur le CR sans les visiter
  // Pas impl�ment�. il faudrait d�cr�memnetr le compteur pour
  // les mondes dans ce cas.

  // Crit�re 6 : Le monde ne nous appartient pas
  if Data.M[NoMonde].Proprio <> data.NoJou then
    inc(Critere[6]);

  // Crit�re 7 : la distance, priorit� aux chemins les plus courts
  inc(Critere[7]);
end;

function GetMondeDanger(NoMonde : integer) : integer;
var
  Critere : TCritere;
  i : integer;
begin
  Result := 0;
  FillChar(Critere, sizeof(Critere), 0);
  GetMondeCriteres(NoMonde, Critere);
  for i := 1 to 7 do
    if Critere[i] > 0 then
  begin
    Result := 7 - i;
    Exit;
  end;
end;

function SortFunc(Item1, Item2 : Pointer) : integer;
var
  Criteres, CriteresTmp : array[1..2] of TCritere;
  Chemin : TChemin;
  i, j, k : integer;
  monde : integer;
begin
  FillChar(Criteres, sizeof(Criteres), 0);

  for i := 1 to 2 do
  begin
    if i = 1 then
      Chemin := TChemin(Item1)
    else
      Chemin := TChemin(Item2);
    for j := 1 to Chemin.Count - 2 do
    begin
      monde := Chemin[j];
      GetMondeCriteres(monde, Criteres[i]);
      //for k := 1 to 7 do
        //Criteres[i] := CriteresTmp[i];
    end;
  end;

  Result := 0;
  for i := 1 to 7 do if Criteres[1, i] <> Criteres[2, i] then
    if Criteres[1, i] < Criteres[2, i] then
    begin
      Result := -1;
      exit;
    end
    else if Criteres[1, i] > Criteres[2, i] then
    begin
      Result := 1;
      exit;
    end;
end;


constructor TCheminList.Create(Data : TNebData);
begin
  FData := Data;
  FCheminList := TList.Create;
end;

destructor TCheminList.Destroy;
var
  i : integer;
begin
  for i := 0 to FCheminList.Count - 1 do
    TChemin(FCheminList[i]).Free;

  FCheminList.Free;
end;

function TCheminList.GetChemin(index : integer) : TChemin;
begin
  Result := FCheminList[index];
end;

procedure TCheminList.SetChemin(index : integer; item : TChemin);
begin
  FCheminList[index] := item;
end;

function TCheminList.Count : integer;
begin
  Result := FCheminList.Count;
end;

procedure TCheminList.AddCopy(item : TChemin);
var
  Chemin : TChemin;
begin
  Chemin := TChemin.Create;
  Chemin.Assign(item);
  FCheminList.Add(Chemin);
end;

procedure TCheminList.Sort;
begin
  Data := FData;
  FCheminList.Sort(SortFunc);
end;

procedure TCheminList.RecenceChemins(MdeDep, MdeArr : integer);
var
  Chemin : TChemin;
begin
  Chemin := TChemin.Create;
  Chemin.Add(MdeDep);
  ChercheChemin(FData.EvalDEP, 0, MdeDep, MdeArr, Chemin);
end;

procedure TCheminList.ChercheChemin(DepMax, MdePrec, MdeDep, MdeArr : integer; Chemin : TChemin);
var
  connex : integer;
begin
  for connex := 1 to 8 do
    if (FData.M[MdeDep].Connect[connex] > 0) and
       (FData.M[MdeDep].Connect[connex] <> MdePrec) and
       ((not FData.TN[FData.M[MdeDep].Connect[connex]]) or
       (FData.M[MdeDep].Connect[connex] = MdeArr)) then
  begin
    Chemin.Add(FData.M[MdeDep].Connect[connex]);
    if (Chemin.Last = MdeArr) or (Chemin.Count = DepMax + 1) then
    begin
      if Chemin.Last = MdeArr then
        AddCopy(Chemin);
      Chemin.RemoveLast;
    end
    else
      ChercheChemin(DepMax, MdeDep, Chemin.Last, MdeArr, Chemin);
  end;
  if Chemin.Last <> MdeArr then Chemin.RemoveLast;
end;

procedure TChemin.EffaceChemin;
begin
  FillChar(FChemin, sizeof(FChemin), 0);
  FCount := 0;
end;

end.
