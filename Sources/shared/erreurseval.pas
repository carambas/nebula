unit erreurseval;

interface

function GetErrorString(error : integer) : string;

const
      ERR_CONNAIT_PAS_DE_NOM = 1;
      ERR_MAUVAISE_CLASSE = 2;
      ERR_PAS_PROPRIO_MONDE = 4;
      ERR_PAS_PROPRIO_FLOTTE = 5;
      ERR_MONDES_PAS_CONNECTES = 6;
      ERR_PAS_ASSEZ_VI = 7;
      ERR_PAS_ASSEZ_VP = 8;
      ERR_PAS_ASSEZ_VAISSEAUX = 10;
      ERR_FLOTTE_PAS_SUR_MONDE = 11;
      ERR_PAS_ASSEZ_I = 12;
      ERR_PAS_ASSEZ_POP = 13;
      ERR_FLOTTE_PAS_ASSEZ_MP = 14;
      ERR_PAS_PROPRIO_TRESOR = 15;
      ERR_PAS_CHARGEUR = 17;
      ERR_PAS_ASSEZ_MP = 18;
      ERR_VAISS_DEJA_ORDRE_EXCL = 19;
      ERR_FLOTTE_DEJA_ORDRE_EXCL = 20;
      ERR_SYNTAXE = 21;
      ERR_FLOTTE_PAS_BOMBE = 22;
      ERR_DEST_PAS_BONNE_CLASSE = 25;
      ERR_FLOTTE_PAS_ASSEZ_CONV = 26;
      ERR_PAS_PROPRIO_CONV = 27;
      //ERR_PAS_ASSEZ_POP = 28;
      ERR_PAS_ASSEZ_CONV = 29;
      ERR_PAS_ASSEZ_ROB = 30;
      ERR_FLOTTE_PAS_ASSEZ_ROB = 32;
      ERR_TECH_DEPASSE_MAX = 33;
      ERR_NB_ROB_PAS_PAIR = 35;
      ERR_NB_ROB_PAS_MULT12 = 36;
      ERR_FLOTTE_PAS_ASSEZ_VC = 37;
      ERR_PAS_ASSEZ_DEP = 38;
      ERR_CATEGORIE_ESPION_INEXISTANTE = 40;
      ERR_MONDE_SURPEUPLE = 41;
      ERR_MONDE_EXISTE_PAS = 42;
      ERR_FLOTTE_EXISTE_PAS = 43;
      ERR_TRESOR_EXISTE_PAS = 44;
      ERR_JOU_EXISTE_PAS = 45;
      ERR_MAX_EXPLO = 46;
      ERR_NON_CHARGEUR_POP = 47;
      ERR_NON_DECHARGEUR_POP = 48;
      ERR_NOM_TROP_LONG = 49;
      ERR_TRANSFERT_MEME_FLOTTE = 50;
      ERR_CONNEXION_INCONNUE_DE_TOUS = 51;
      ERR_ORDRE_INTERDIT = 52;

implementation

uses SysUtils;

const
  ErrorsTab : array[1..52] of string =
  (
   'L''auteur ne connait pas ce joueur',                        // 1
   'L''auteur n''a pas la bonne classe',                        // 2
   'L''auteur a d�j� d�clar� un Jihad il y a moins de 5 tours', // 3
   'L''auteur ne poss�de pas le monde',                         // 4
   'L''auteur ne poss�de pas la flotte',                        // 5
   'Les mondes ne sont pas connect�s',                          // 6
   'Pas assez de VI sur le monde',                              // 7
   'Pas assez de VP sur le monde',                              // 8
   'L''auteur ne peut se d�clarer le Jihad',                    // 9
   'La flotte n''a pas assez de vaisseaux',                     // 10
   'La flotte n''est pas sur le monde',                         // 11
   'Pas assez d''industries disponibles sur le monde',          // 12
   'Pas assez de populations sur le monde',                      // 13
   'La flotte ne contient pas assez de MP',                     // 14
   'L''auteur ne poss�de pas le tr�sor',                        // 15
   'Le tr�sor n''est pas sur une flotte',                       // 16
   'L''auteur n''est pas chargeur',                             // 17
   'Pas assez de MP sur le monde',                              // 18
   'Les vaisseaux ont d�j� re�u un ordre exclusif',             // 19
   'La flotte a d�j� re�u un ordre exclusif',                   // 20
   'Erreur de syntaxe',                                         // 21
   'La flotte ne transporte pas la BOMBE',                      // 22
   'Le monde a d�j� �t� pill� � ce tour',                       // 23
   'Le monde n''a pas encore r�cup�r� d''un pillage',           // 24
   'Le destinataire n''a pas la bonne classe',                  // 25
   'La flotte ne contient pas assez de populations',            // 26
   'L''auteur ne poss�de pas les convertis',                    // 27

   // Doublon conserv� pour compatibilit� avec anciennes parties
   'Pas assez de populations sur le monde',                     // 28
   'Pas assez de convertis sur le monde',                       // 29
   'Pas assez de robots sur le monde',                          // 30
   'La flotte ne contient pas assez de convertis',              // 31
   'La flotte ne contient pas assez  de robots',                // 32
   'Vous avez d�j� la technologie maximale',                    // 33
   'Vous depassez le seuil maximum de la technologie',          // 34
   'Le nombre de robots doit �tre pair',                        // 35
   'Le nombre de robots doit �tre un multiple de 12',           // 36
   'La flotte n''a pas assez de VC',                            // 37
   'Vous n''avez pas un niveau en DEP suffisant',               // 38
   'L''auteur n''est pas Pirate',                               // 39
   'Cette cat�gorie d''espion n''existe pas',                   // 40
   'Le monde est surpeupl�',                                    // 41
   'Ce monde n''existe pas',                                    // 42
   'Cette flotte n''existe pas',                                // 43
   'Ce tr�sor n''existe pas',                                   // 44
   'Ce joueur n''existe pas',                                   // 45
   'Vous d�passez le maximum d''exploration posible',           // 46
   'L''auteur n''est pas chargeur de populations',              // 47
   'L''auteur n''est pas d�chargeur de populations',            // 48
   'Le nom d�passe le maximum de 70 caract�res',                // 49
   'Interdit de transf�rer vers la flotte d''origine',          // 50
   'Vous tentez d''emprunter une connexion inconnue de tous',   // 51
   'Cet ordre est interdit dans cette partie'                  // 52
  );

function GetErrorString(error : integer) : string;
begin
  //Result := LoadStr(error);
  Result := ErrorsTab[error];
end;

end.







