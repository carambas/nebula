unit imprime;

INTERFACE

uses
  AvatUnit,
  SysUtils,
  Classes,
  LectOrdres,
  NebData,
  erreurseval,
  ordreslist;

const
  ImpSuite = 1;
  ImpOem = 2;
  FicTempo = 4;

  LigneTailleMax = 76;

type
  TIntReal = (Int, Rea);

var
  //Lst: TextFile;
  Jou: byte;
  NomPartie: String;
  RepFic: String;
  CR : TStringList;

procedure ImprimeData(theData, oldData: TNebdata; NoJou: integer; CRList: TStringList); overload;
function ImprimeData(theData, oldData: TNebdata; NoJou: integer): String; overload;
procedure AfficheMonde(NebDat, oldData: TNebdata; NoJoueur, md: word);
function AfficheMondeStr(NebData: TNebdata; NoJoueur, md: word): string;
function AfficheMondeFirstLine(NebData: TNebdata; NoJoueur, md: word): string;

IMPLEMENTATION

uses Utils;

type
  TClassement = array [1 .. NB_JOU_MAX] of word;

var
  Data: TNebdata;
  NoJoueur, Position: word;
  Ch: String;
  Fini: boolean;

Procedure Affiche(st: string; Tab: word);

  function CoupeCh(var chaine: string): string;
  var
    i: integer;
    tempo: string;
  begin
    tempo := Ch + chaine;

    i := tempo.Substring(0, LigneTailleMax + 1).LastIndexOf(' ');
    if i <> -1 then
    begin
      Result := tempo.Substring(0, i);
      chaine := tempo.Substring(i + 1);
    end
    else
      Result := '';
    Position := 1;
  end;

var
  i: integer;
  OldCh: string;
begin
  Repeat
    if Position < Tab then
    begin
      for i := Position + 1 to Tab do
        Ch := Ch + ' ';
      Position := Tab;
    end;

    if Position + st.length <= LigneTailleMax then
    begin
      Position := Position + st.length;
      Ch := Ch + st;
      st := '';
    end
    else
    begin
      OldCh := Ch + st;
      Ch := CoupeCh(st);
      // test pour le cas o� on ne peut tronquer les espaces
      // (par manque d'espaces)
      if OldCh = Ch then
      begin
        st := Ch.Substring(LigneTailleMax);
        Ch := Ch.Substring(0, LigneTailleMax);
      end;
      CR.Add(Ch);
      Ch := '';
    end;
  Until st.length = 0;
end;

Procedure AfficheLn(Tab: word); overload;
begin
  CR.Add(Ch);
  Ch := '';
  Position := 1;
end;

Procedure AfficheLn(St: string; Tab: word); overload;
begin
  Affiche(St, Tab);
  AfficheLn(Tab);
end;

Procedure AfficheNb(z: integer; Tab: word);
begin
  Affiche(IntToStr(z), Tab);
end;

Procedure Afficheval(st: string; Tab: word);
begin
  Affiche(st, Tab);
end;

Function AffNom(jo, NbTour: word): String;
begin
  if jo > 0 then
  begin
    if NbTour > 0 then
      Result := Format('"%s:%d"', [Data.Jou[jo].Pseudo, NbTour])
    else
      Result := Format('"%s"', [Data.Jou[jo].Pseudo]);
  end
  else
    Result := '""';
end;


Function NbConnect(md: word): word;
var
  i, n: word;
begin
  n := 0;
  for i := 1 to 8 do
    if Data.M[md].Connect[i] > 0 then
      Inc(n);
  NbConnect := n;
end;

procedure AfficheStat(Jou: Integer; Niveau: UInt64; statid: UInt64);
var
  i, j, numJouCoequipier, rang: Integer;
  ValEquipe: integer;
  Nombres, clas: StatJoueurs;

  function GetValeur(rang: Integer): Integer;
  begin
    if (rang = 1) then
      Result := Data.jou[Jou].StatsPremier[statid]
    else if (rang = Data.NbJou) then
      Result := Data.jou[Jou].StatsDernier[statid]
    else
      Result := Nombres[clas[rang]];
  end;

begin
  if Niveau > 7 then
    Niveau := 7;

  FillChar(Nombres, sizeof(Nombres), 0);
  for j := 1 to Data.NbJou do
    Nombres[j] := Data.jou[Jou].Stats[j, statid];

  for rang := 1 to Data.NbJou do
    clas[rang] := Data.jou[Jou].StatsClassementRang[rang, statid];

  // On calcule la valeur de la Stat pour l'�quipe du joueur
  ValEquipe := 0;
  for i := 0 to Data.Jou[Jou].Equipe.Count - 1 do
  begin
    numJouCoequipier := StrToInt(Data.Jou[Jou].Equipe[i]);
    Inc(ValEquipe, Data.jou[numJouCoequipier].Stats[numJouCoequipier, statid]);
  end;

  if niveau = 0 then
  begin // Pas d'espionnage
    AfficheNb(Nombres[Jou], 5);
    Afficheval('  ', 5);
  end
  else
  begin // Espionnage
    // Affichage espionnage tri� par ordre d�croissant
    for rang := 1 to Data.NbJou do
      if Data.ConnuDeNom[Jou, clas[rang]] or (Jou = clas[rang]) or (rang = 1) or (rang = Data.NbJou) then
      begin
        AfficheNb(rang, 5);
        if Data.ConnuDeNom[Jou, clas[rang]] then
        begin
          Afficheval(':', 5);
          Afficheval(AffNom(clas[rang], 0), 5);
        end;
        Afficheval(':', 5);

        AfficheNb(GetValeur(rang), 5);

        Afficheval('  ', 5);
      end;

    Afficheval('M:', 5);
    AfficheNb(Data.jou[Jou].StatsMoyenne[statid], 5);
    Afficheval(' [Erreur = ', 5);
    AfficheNb(Round2(100 * COEF_ESPIONNAGE[Niveau]), 5);
    Afficheval('%]', 5);
  end;

  if Data.Jou[Jou].Equipe.Count > 1 then
    Afficheval(Format('(Equipe = %d)', [ValEquipe]), 5);
  Afficheln(1);
end;

Procedure AfficheEnTete;
const
  STATS_LIBELLE: array[1..NB_STATS] of String = ('Capacit� mini�re',
    'Populations', 'Cargaison transport�e', 'Mondes', 'Flottes', 'Industries',
    'Vaisseaux', 'Tr�sors', 'Industries actives', 'Convertis', 'Robots');

  STATS_ID: array[1..NB_STATS] of Integer = (
    STAT_CM, STAT_POPULATIONS, STAT_CONVERTIS, STAT_ROBOTS, STAT_CARGAISON,
    STAT_MONDES, STAT_FLOTTES, STAT_INDUSTRIES, STAT_IND_ACTIVES, STAT_VAISSEAUX,
    STAT_TRESORS);

var
  i: word;
  st: string;
  n: integer;
  AffAli: boolean;
  AffDetail: boolean;
  Nombres: StatJoueurs;

begin
  with Data do
  begin
{$IFDEF NEBUTIL}
    AfficheLn
      (Format('*** Compte rendu g�n�r� � partir de %s version %s ***',
      [ExtractFileName(ParamStr(0)), NoVersionNebutil]), 1);
{$ELSE}
    AfficheLn
      (Format('*** Compte rendu g�n�r� � partir de %s version %s ***',
      [ExtractFileName(ParamStr(0)), AvatUnit.Version]), 1);
{$ENDIF}
    AfficheLn(1);
    if not Fini then
      Affiche(Format('R�sultat du tour No. %d', [Tour]), 1)
    else
      Affiche('R�sultat FINAL', 1);
    AfficheLn(' de la partie ' + NomPartie, 1);
    st := IntToStr(NoJoueur);
    AfficheLn('Joueur ' + Data.Jou[NoJoueur].Nom + ', ' +
      NomClasse[Data.Jou[NoJoueur].Classe] + ' ' + AffNom(NoJoueur, 0) +
      ':' + st, 1);
    st := IntToStr(Data.Jou[NoJoueur].Score);
    AfficheLn('Total des points obtenu : ' + st, 1);
    Afficheln(1);

    if (Data.ConNom[NoJoueur] <> 0) and
      (Data.ConNom[NoJoueur] <> Puis2(NoJoueur - 1)) then
    begin
      Afficheval('Vous connaissez de nom :', 1);
      Afficheln(1);
      for i := 1 to Data.NbJou do
        if Data.ConnuDeNom[NoJoueur, i] and (i <> NoJoueur) then
        begin
          Afficheval(AffNom(i, 0) + ':', 1);
          AfficheNb(i, 1);
          Afficheval(' ', 1);
        end;
      Afficheln(1);
      Afficheln(1);
    end;

    if Data.Jou[NoJoueur].Equipe.Count > 1 then // On a une �quipe de form�e
    begin
      Afficheval('Votre �quipe contient les joueurs suivants (avec leur score) :', 1);
      Afficheln(1);
      for i := 0 to Data.Jou[NoJoueur].Equipe.Count - 1 do
      begin
        Afficheval(Format('"%s":%d : %d', [
          Data.Jou[StrToInt(Data.Jou[NoJoueur].Equipe[i])].Pseudo,
          StrToInt(Data.Jou[NoJoueur].Equipe[i]),
          Data.Jou[StrToInt(Data.Jou[NoJoueur].Equipe[i])].Score
        ]), 4);
        Afficheln(1);
      end;
      Afficheln(1);
    end;

    if Data.Allie[NoJoueur] <> 0 then
    begin
      Afficheval('Vos alli�s :', 1);
      Afficheln(1);
      for i := 1 to Data.NbJou do
        if EstAllie(NoJoueur, i) then
          Afficheval(AffNom(i, 0) + ' ', 1);
      Afficheln(1);
      Afficheln(1);
    end;
    if Data.Chargeur[NoJoueur] <> 0 then
    begin
      Afficheval('Vos chargeurs :', 1);
      Afficheln(1);
      for i := 1 to Data.NbJou do
        if EstChargeur(NoJoueur, i) then
          Afficheval(AffNom(i, 0) + ' ', 1);
      Afficheln(1);
      Afficheln(1);
    end;
    if Data.ChargeurPop[NoJoueur] <> 0 then
    begin
      Afficheval('Vos chargeurs de population :', 1);
      Afficheln(1);
      for i := 1 to Data.NbJou do
        if EstChargeurDePop(NoJoueur, i) then
          Afficheval(AffNom(i, 0) + ' ', 1);
      Afficheln(1);
      Afficheln(1);
    end;
    if Data.DechargeurPop[NoJoueur] <> 0 then
    begin
      Afficheval('Vos d�chargeurs de population :', 1);
      Afficheln(1);
      for i := 1 to Data.NbJou do
        if EstDechargeurDePop(NoJoueur, i) then
          Afficheval(AffNom(i, 0) + ' ', 1);
      Afficheln(1);
      Afficheln(1);
    end;
    if Data.Pilleur[NoJoueur] <> 0 then
    begin
      Afficheval('Vos pilleurs :', 1);
      Afficheln(1);
      for i := 1 to Data.NbJou do
        if EstPilleur(NoJoueur, i) then
          Afficheval(AffNom(i, 0) + ' ', 1);
      Afficheln(1);
      Afficheln(1);
    end;
    if Data.Jou[NoJoueur].Jihad <> 0 then
    begin
      //WriteAnsi('Vous avez proclam� le Jihad contre : ');
      Affiche('Vous avez proclam� le Jihad contre : ', 1);
//        WritelnAnsi(AffNom(Data.Jou[NoJoueur].Jihad, 0));
      AfficheLn(AffNom(Data.Jou[NoJoueur].Jihad, 0), 1);
      if Tour < Data.Jou[NoJoueur].TourChgtJihad + 5 then
//          WritelnAnsi
//            (Format('Vous ne pourrez pas le changer avant vos ordres du tour %d',
//            [Data.Jou[NoJoueur].TourChgtJihad + 5]))
        AfficheLn
          (Format('Vous ne pourrez pas le changer avant vos ordres du tour %d',
          [Data.Jou[NoJoueur].TourChgtJihad + 5]), 1)
      else
//          WritelnAnsi('Vous pouvez en changer � tout moment');
        AfficheLn('Vous pouvez en changer � tout moment', 1);
      CR.Add('');
    end;
    TriScores; // TODO Ne plus appeler ici
    if Data.Contact[NoJoueur] <> 0 then
    begin
      Afficheval('Vous �tes en contact avec :', 1);
      Afficheln(1);
      n := 0;
      for i := 1 to Data.NbJou do
        if Data.EstEnContact(NoJoueur, i) then
        begin
          Afficheval(AffNom(i, 0) + ' ', 1);
          Inc(n);
        end;
      Afficheln(1);
      Afficheln(1);

      if Data.AffichageScore then
      begin
        if not Fini and Data.AffichageScore then
        begin
          if n = 1 then
          begin
            Afficheval('Son score :', 1);
            Afficheln(1);
            if (Data.TypeFicDonnees = tfd_NBA) then
            begin
              for i := 1 to Data.NbJou do
                if EstEnContact(NoJoueur, i) then
                  AfficheNb(Data.Jou[i].Score, 1);
            end
            else if High(Data.ScoresDecroissants) > 0 then
              Affiche(Data.ScoresDecroissants[0], 1);
            Afficheln(1);
            Afficheln(1);
          end
          else
          begin
            Afficheval('Leur score (par ordre d�croissant) : ', 1);
            Afficheln(1);

            if Data.TypeFicDonnees = tfd_NBA then
            begin
              for i := 1 to Data.NbJou do
                if EstEnContact(NoJoueur, ScoresTries[i]) then
                begin
                  AfficheNb(Data.Jou[ScoresTries[i]].Score, 1);
                  Afficheval('  ', 1);
                end;
            end
            else if High(Data.ScoresDecroissants) > 0 then
            begin
              for i := Low(Data.ScoresDecroissants) to High(Data.ScoresDecroissants) do
              begin
                Affiche(Data.ScoresDecroissants[i], 1);
                Afficheval('  ', 1);
              end;
            end
            else
            begin
              Afficheval('  (Information non disponible)', 1);
            end;

            Afficheln(1);
            Afficheln(1);
          end;
        end;
      end;
    end;

    n := Tour;
    if n >= 1 then
    begin
      Afficheval('Statistiques :', 0);
      Afficheln(1);

      for i := 1 to NB_STATS do
      begin
        Afficheval(STATS_LIBELLE[STATS_ID[i]] + ' : ', 2);
        AfficheStat(NoJoueur, Jou[NoJoueur].Espions[STATS_ID[i]], STATS_ID[i]);
      end;

      Afficheln(1);
    end;

    Afficheval('Vos niveaux technologiques : ', 1);
    Afficheln(1);

    Afficheval('DEPlacement : ', 2);
    AfficheNb(Data.Jou[NoJoueur].DEP, 2);
    if (Data.Jou[NoJoueur].DEP < MaxTech[1, Data.Jou[NoJoueur].Classe]) or
      (MaxTech[1, Data.Jou[NoJoueur].Classe] = 0) then
    begin
      Afficheval('(', 2);
      if Data.Jou[NoJoueur].DEPreste > 0 then
      begin
        Afficheval('+', 2);
        AfficheNb(Data.Jou[NoJoueur].DEPreste, 2);
        Afficheval('/', 2);
      end;
      AfficheNb(TechClasse[1, Data.Jou[NoJoueur].Classe], 2);
      Afficheval(')', 2);
    end;
    Afficheln(1);

    Afficheval('ATTaque : ', 2);
    AfficheNb(Data.Jou[NoJoueur].ATT, 2);
    if (Data.Jou[NoJoueur].ATT < MaxTech[2, Data.Jou[NoJoueur].Classe]) or
      (MaxTech[2, Data.Jou[NoJoueur].Classe] = 0) then
    begin
      Afficheval('(', 2);
      if Data.Jou[NoJoueur].ATTreste > 0 then
      begin
        Afficheval('+', 2);
        AfficheNb(Data.Jou[NoJoueur].ATTreste, 2);
        Afficheval('/', 2);
      end;
      AfficheNb(TechClasse[2, Data.Jou[NoJoueur].Classe], 2);
      Afficheval(')', 2);
    end;
    Afficheval(', Votre CA est de ', 2);
    AfficheNb(Data.Jou[NoJoueur].CA, 2);
    if Data.Jou[NoJoueur].Jihad > 0 then
      Afficheval(Format(' (%d contre "%s")',
        [CalculeCA(Data.Jou[NoJoueur].ATT + 4),
        Data.Jou[Data.Jou[NoJoueur].Jihad].Pseudo]), 2);
    Afficheln(1);

    Afficheval('DEFense : ', 2);
    AfficheNb(Data.Jou[NoJoueur].DEF, 2);
    if (Data.Jou[NoJoueur].DEF < MaxTech[3, Data.Jou[NoJoueur].Classe]) or
      (MaxTech[3, Data.Jou[NoJoueur].Classe] = 0) then
    begin
      Afficheval('(', 2);
      if Data.Jou[NoJoueur].DEFreste > 0 then
      begin
        Afficheval('+', 2);
        AfficheNb(Data.Jou[NoJoueur].DEFreste, 2);
        Afficheval('/', 2);
      end;
      AfficheNb(TechClasse[3, Data.Jou[NoJoueur].Classe], 2);
      Afficheval(')', 2);
    end;
    Afficheval(', Votre CD est de ', 2);
    AfficheNb(Data.Jou[NoJoueur].CD, 2);
    Afficheln(1);

    Afficheval('RADar : ', 2);
    AfficheNb(Data.Jou[NoJoueur].RAD, 2);
    if (Data.Jou[NoJoueur].RAD < MaxTech[4, Data.Jou[NoJoueur].Classe]) or
      (MaxTech[4, Data.Jou[NoJoueur].Classe] = 0) then
    begin
      Afficheval('(', 2);
      if Data.Jou[NoJoueur].RADreste > 0 then
      begin
        Afficheval('+', 2);
        AfficheNb(Data.Jou[NoJoueur].RADreste, 2);
        Afficheval('/', 2);
      end;
      AfficheNb(TechClasse[4, Data.Jou[NoJoueur].Classe], 2);
      Afficheval(')', 2);
    end;
    Afficheln(1);

    Afficheval('Connaissance des ALIens : ', 2);
    AfficheNb(Data.Jou[NoJoueur].ALI, 2);
    if (Data.Jou[NoJoueur].ALI < MaxTech[5, Data.Jou[NoJoueur].Classe]) or
      (MaxTech[5, Data.Jou[NoJoueur].Classe] = 0) then
    begin
      Afficheval('(', 2);
      if Data.Jou[NoJoueur].ALIreste > 0 then
      begin
        Afficheval('+', 2);
        AfficheNb(Data.Jou[NoJoueur].ALIreste, 2);
        Afficheval('/', 2);
      end;
      AfficheNb(TechClasse[5, Data.Jou[NoJoueur].Classe], 2);
      Afficheval(')', 2);
    end;
    Afficheln(1);

    Afficheval('CARgaison : ', 2);
    AfficheNb(Data.Jou[NoJoueur].CAR, 2);
    if (Data.Jou[NoJoueur].CAR < MaxTech[6, Data.Jou[NoJoueur].Classe]) or
      (MaxTech[6, Data.Jou[NoJoueur].Classe] = 0) then
    begin
      Afficheval('(', 2);
      if Data.Jou[NoJoueur].CARreste > 0 then
      begin
        Afficheval('+', 2);
        AfficheNb(Data.Jou[NoJoueur].CARreste, 2);
        Afficheval('/', 2);
      end;
      AfficheNb(TechClasse[6, Data.Jou[NoJoueur].Classe], 2);
      Afficheval(')', 2);
    end;
    Afficheln(1);

    { Affichage des CD et CA des personnages connus }
    AffAli := False;
    FillChar(Nombres, sizeof(Nombres), 0);
    n := 0;
    if Data.Jou[NoJoueur].ALI >= 2 then
    begin
      for i := 1 to Data.NbJou do
      begin
        if (i <> NoJoueur) and Data.ConnuDeNom[NoJoueur, i] and
          ((Data.Jou[i].ALI < 3) or (Data.Jou[NoJoueur].ALI >= Data.Jou[i]
          .ALI)) then
        begin
          Nombres[i] := Data.Jou[i].CD;
          Inc(n);
        end;
      end;
      if n > 0 then
      begin
        Afficheln(1);
        AffAli := True;
        Afficheval
          ('Votre connaissance des aliens vous permet de conna�tre les CD suivants :',
          1);
        Afficheln(1);
        for i := 1 to Data.NbJou do
          if Nombres[i] > 0 then
          begin
            Afficheval(AffNom(i, 0) + ':', 2);
            AfficheNb(Nombres[i], 2);
            Afficheval(' ', 2);
          end;
      end;
    end;

    FillChar(Nombres, sizeof(Nombres), 0);
    n := 0;
    if Data.Jou[NoJoueur].ALI >= 3 then
    begin
      for i := 1 to Data.NbJou do
      begin
        if (i <> NoJoueur) and Data.ConnuDeNom[NoJoueur, i] and
          ((Data.Jou[i].ALI < 3) or (Data.Jou[NoJoueur].ALI >= Data.Jou[i]
          .ALI)) then
        begin
          Nombres[i] := Data.Jou[i].CA;
          Inc(n);
        end;
      end;
      if n > 0 then
      begin
        AffAli := True;
        Afficheln(1);
        Afficheln(1);
        Afficheval
          ('Votre connaissance des aliens vous permet de connaitres les CA suivants :',
          1);
        Afficheln(1);
        for i := 1 to Data.NbJou do
          if Nombres[i] > 0 then
          begin
            Afficheval(AffNom(i, 0) + ':', 2);
            AfficheNb(Nombres[i], 2);
            Afficheval(' ', 2);
          end;
      end;
    end;
    if AffAli then
      Afficheln(1);

    AffDetail := False;
    for i := 1 to NBSCORE do
      if DetailScores[NoJoueur, i] <> 0 then
      begin
        AffDetail := True;
        Break;
      end;
    if AffDetail then
    begin
      Afficheln(1);
      Afficheval('D�tail de votre score � ce tour', 1);
      Afficheln(1);
      for i := 1 to NBSCORE do
      begin
        if DetailScores[NoJoueur, i] <> 0 then
        begin
          Afficheval(ScoreNom[i] + ' : ', 2);
          AfficheNb(DetailScores[NoJoueur, i], 2);
          Afficheval(' points', 2);
          Afficheln(1);
        end;
      end;
    end;
    Afficheln(1);

    { Fin Affichage des niveaux technologiques 24/01/96 }

    // Affichage du classement tous les 5 tours dans les parties CRUEL 17/03/02
    if Data.AffichageScoreCRUEL then
    begin
      Afficheval('Affichage scores CRUEL : ', 1);
      Afficheln(1);
      Afficheval('Score du premier : ', 2);
      if TypeFicDonnees = tfd_NBA then
        AfficheNb(Data.Jou[ScoresTries[1]].Score, 2)
      else if Length(ScoresDecroissants) > 0 then
       Afficheval(ScoresDecroissants[0], 2);
      Afficheln(1);
      Afficheval('Votre place : ', 2);
      AfficheNb(ClassementJouScore[NoJoueur], 2);
      Afficheln(1);
      Afficheln(1);
    end;
  end; // With Data do
end;

Procedure AfficheOrdresData(theData: TNebdata);
Var
  debugstr: string;
  NbOrdres, NbErr: word;
  O: TOrdre;
  col: word;
  i: integer;
  co: Integer;
  ListeOrdres: TListeOrdres;
begin
  ListeOrdres := Data.Ordres;
  imprime.RepFic := RepFic;
  Afficheval('Ordres du joueur ' + AffNom(NoJoueur, 0), 1);
  Afficheln(1);
  Afficheln(1);
  NbErr := 0;
  NbOrdres := 0;
  col := 0;
  for i := 1 to 21 do
    for co := 0 to ListeOrdres.Count(i) - 1 do
    begin
      O := ListeOrdres[i, co];
      if O.Auteur = NoJoueur then
      begin
        Inc(NbOrdres);
        if O.Erreur > 0 then
          Inc(NbErr)
        else
        begin
          debugstr := TNebOrdres.OrdreCh(O, Data.Jou);
          Afficheval(debugstr, (col mod 2) * 40 + 1);
          if col mod 2 = 1 then
            Afficheln(1);
          Inc(col);
        end;
      end;
    end;
  Afficheln(1);
  if NbErr > 0 then
  begin
    if col mod 2 = 1 then
      Afficheln(1);
    if NbErr = 1 then
      Afficheval('L''ordre suivant n''a pu �tre ex�cut� :', 1);
    if NbErr > 1 then
      Afficheval('Les ordres suivants n''ont pu �tre ex�cut�s :', 1);
    Afficheln(1);
    Afficheln(1);
    for i := 0 to 21 do
      for co := 0 to ListeOrdres.Count(i) - 1 do
      begin
        O := ListeOrdres[i, co];
        if ((O.Auteur = NoJoueur) and (O.Erreur > 0)) then
        begin
          Afficheval(TNebOrdres.EnleveCommentsAndSpaces(O.OrdreCh), 1);
          Afficheval(' <- ', 1);
          Afficheval(GetErrorString(O.Erreur), 1);
          Afficheln(1);
        end;
      end;
  end;
  Afficheln(1);
  Afficheval('Nombre d''ordres : ', 1);
  AfficheNb(NbOrdres, 1);
  Afficheln(1);
  Afficheln(1);
  AfficheLn('Fin des ordres du joueur ' + AffNom(NoJoueur, 0), 1);
end;

// affiche un monde : md = num�ro du monde
procedure AfficheMonde(NebDat, oldData: TNebdata; NoJoueur, md: word);
var
  i, j, k: integer;
  avant: boolean;
  ALI: boolean;
  AffCoord: boolean;
  AfficheConnexionTN: boolean;
  fmt: TFormatSettings;
begin
  Data := NebDat;
  with Data do
  begin
    i := md;
    if NomMonde[i] <> '' then
    begin
      Afficheval(Format('"%s"', [NomMonde[i]]), 1);
      Afficheln(1);
    end;

    if Data.EstCorrompu(i, NoJoueur) then
    begin
      Afficheval
        ('*** Arrrgh ce monde est CORROMPU !!!! Il doit �tre d�truit ***', 1);
      Afficheln(1);
    end;
    Afficheval('M', 1);

    if (Data.EstMd(i) and ((NoJoueur > 0) and (NoJoueur <= NbJou) and
      (Data.Jou[NoJoueur].md = i) or (NoJoueur = 0))) then
      Afficheval('d', 1);
    if (M[i].Statut and Puis2(7)) > 0 then
      Afficheval('#', 1)
    else
      Afficheval('_', 1);
    AfficheNb(i, 1);

    if TN[i] then
      Afficheval('*** Trou noir ***', 8)

    else
    begin
      Afficheval('(', 8);
      for j := 1 to NbConnect(i) do
      begin
        AfficheConnexionTN := False;
        if ((NoJoueur > 0) and (NoJoueur <= NbJou)) then
          if Jou[NoJoueur].Classe = Explorateur then
            AfficheConnexionTN := True;
        if NoJoueur = 0 then
          AfficheConnexionTN := True;
        if AfficheConnexionTN and ((M[M[i].Connect[j]].Statut and 64) > 0)
        then
          Afficheval('@', 8);
        AfficheNb(M[i].Connect[j], 8);
        if j < NbConnect(i) then
          Afficheval(',', 8);
      end;
      if Assigned(oldData) and not oldData.AVuConnect[md, NoJou] then
        Afficheval(', ?', 8);
      Afficheval(') ', 8);

      if Assigned(oldData) and not oldData.Connu[md, Data.NoJou] then
        Afficheval('*** INCONNU ***', 8)

      else
      begin
        if (M[i].Proprio > 0) or (ME[i].AncienProprio > 0) then
        begin
          Afficheval('"', 8);
          if M[i].Proprio > 0 then
            Afficheval(Data.Jou[M[i].Proprio].Pseudo, 8);
          if ((ME[i].AncienProprio > 0) and
            (ME[i].AncienProprio <> M[i].Proprio)) then
            Afficheval('(' + Data.Jou[ME[i].AncienProprio].Pseudo + ')', 8);
          if ((M[i].Duree > 0) or (M[i].NbExploCM > 0)) and (M[i].Proprio > 0)
          then
          begin
            Afficheval(':', 8);
            AfficheNb(M[i].Duree, 8);
          end;
          if M[i].NbExploCM > 0 then
          begin
            Afficheval(':', 8);
            AfficheNb(M[i].Duree + M[i].NbExploCM * 3, 8);
          end;
          Afficheval('"', 8);
        end;
        if (ME[i].Statut and Puis2(0)) > 0 then
          Afficheval('!', 8);
        if (ME[i].Statut and Puis2(1)) > 0 then
          Afficheval('P', 8);
        if (ME[i].Statut and Puis2(2)) > 0 then
          Afficheval('C', 8);
        if (M[i].Proprio > 0) or (ME[i].AncienProprio > 0) or
          ((ME[i].Statut and Puis2(0)) > 0) then
          Afficheval(' ', 8);
        if ((M[i].VI > 0) or (ME[i].ActionVI > 0)) then
          Afficheval('[', 8);
        if M[i].Ind > 0 then
          Afficheval('I=', 8);
        if ME[i].IndLibre < M[i].Ind then
        begin
          AfficheNb(ME[i].IndLibre, 8);
          Afficheval('/', 8);
        end;
        if M[i].Ind > 0 then
          AfficheNb(M[i].Ind, 8);
        if ((M[i].VI > 0) or (ME[i].ActionVI > 0)) then
          Afficheval(']', 8);
        if M[i].VI > 0 then
        begin
          Afficheval('=', 8);
          AfficheNb(M[i].VI, 8);
        end;
        if ME[i].ActionVI in [3, 4] then
        begin
          Afficheval('*', 8);
          case ME[i].ActionVI of
            3:
              begin
                Afficheval('F_', 8);
                AfficheNb(ME[i].CibleVI, 8);
              end;
            4:
              Afficheval('*', 8);
          end;
        end;
        if ((M[i].Ind > 0) or (ME[i].ActionVI > 0) or (M[i].VI > 0)) then
          Afficheval(' ', 8);
        if ((M[i].VP > 0) or (ME[i].ActionVP > 0)) then
          Afficheval('[', 8);
        Afficheval('P=', 8);
        if M[i].Robot = 0 then
          AfficheNb(M[i].Pop, 8);
        if ((M[i].Pop = M[i].Conv) and (M[i].Pop > 0)) then
          Afficheval('C', 8);
        if M[i].Robot > 0 then
        begin
          AfficheNb(M[i].Robot, 8);
          Afficheval('R', 8);
        end;
        if ME[i].PopMoins - ME[i].ConvMoins > 0 then
        begin
          Afficheval('{-', 8);
          AfficheNb(ME[i].PopMoins - ME[i].ConvMoins, 8);
          Afficheval('P}', 8);
        end;
        if ME[i].ConvMoins > 0 then
        begin
          Afficheval('{-', 8);
          AfficheNb(ME[i].ConvMoins, 8);
          Afficheval('C}', 8);
        end;
        if ME[i].RobMoins > 0 then
        begin
          Afficheval('{-', 8);
          AfficheNb(ME[i].RobMoins, 8);
          Afficheval('R}', 8);
        end;
        Afficheval('(', 8);
        AfficheNb(M[i].MaxPop, 8);
        Afficheval(')', 8);
        if ((M[i].Conv > 0) and ((M[i].Conv <> M[i].Pop) or
          (M[i].Proprio <> M[i].ProprConv))) then
        begin
          Afficheval('/', 8);
          AfficheNb(M[i].Conv, 8);
          Afficheval('C', 8);
          if M[i].ProprConv <> M[i].Proprio then
            Afficheval(':' + AffNom(M[i].ProprConv, 0), 8);
        end;
        if ((M[i].VP > 0) or (ME[i].ActionVP > 0)) then
          Afficheval(']', 8);
        if M[i].VP > 0 then
        begin
          Afficheval('=', 8);
          AfficheNb(M[i].VP, 8);
        end;
        if ME[i].ActionVP in [1 .. 4] then
        begin
          Afficheval('*', 8);
          case ME[i].ActionVP of
            1:
              Afficheval('C', 8);
            2:
              Afficheval('N', 8);
            3:
              begin
                Afficheval('F_', 8);
                AfficheNb(ME[i].CibleVP, 8);
              end;
            4:
              Afficheval('*', 8);
          end;
        end;
        if ((M[i].Pop > 0) or (M[i].Robot > 0) or (M[i].MaxPop > 0) or
          (ME[i].ActionVP > 0)) then
          Afficheval(' ', 8);

        if ((M[i].MP <> 0) or (M[i].PlusMP <> 0)) then
          Afficheval('MP=', 8);
        if M[i].MP <> 0 then
          AfficheNb(M[i].MP, 8);
        if M[i].PlusMP <> 0 then
        begin
          Afficheval('(+', 8);
          AfficheNb(M[i].PlusMP, 8);
          Afficheval(')', 8);
        end;

        if (M[i].Statut mod 64) > 0 then
        begin
          Afficheval(' Pi=', 8);
          AfficheNb((M[i].Statut and 56) div 8, 8);
          if M[i].Statut mod 8 > 0 then
          begin
            Afficheval('/', 8);
            AfficheNb(M[i].Statut mod 8, 8);
          end;
        end;
        if M[i].PC > 0 then
        begin
          Afficheval(' PC=', 8);
          AfficheNb(M[i].PC, 8);
        end;
        if M[i].NbExplo > 0 then
        begin
          Afficheval(' E=', 8);
          AfficheNb(M[i].NbExplo, 8);
          Afficheval(Format('/%d', [MAXMEVA]), 8);
        end;
        if (M[i].PotExplo > 0) or (EstMd(i)) then
        begin
          Afficheval(Format(' PE=%d', [M[i].PotExplo]), 8);
        end;

        // MEVA Pop +Pop=x%
        if M[i].NbExploPop > 0 then
        begin
          Afficheval('', 8);
          fmt.DecimalSeparator := ',';
          fmt.ThousandSeparator := ' ';
          Afficheval(Format(' +Pop=%.1f%%',
            [10 + 2.5 * M[i].NbExploPop], fmt), 8)
        end;
      end;
    end;
    AffCoord := False;
    if (NoJoueur > 0) and (NoJoueur <= NbJou) then
      if ((M[i].Proprio = NoJoueur) and (Data.Jou[NoJoueur].RAD = 1)) or
        (Data.Jou[NoJoueur].RAD >= 2) then
        AffCoord := True;
    if NoJoueur = 0 then
      AffCoord := True;

    if Assigned(oldData) and not oldData.Connu[md, Data.NoJou] then
      AffCoord := False;

    if AffCoord then
    begin
      Afficheval(' [', 8);
      AfficheNb(M[i].X, 8);
      Afficheval(',', 8);
      AfficheNb(M[i].Y, 8);
      Afficheval(']', 8);
    end;
    Afficheln(8);
    for j := 1 to NbTres do
      if ((Data.T[j].Statut = 0) and (Data.T[j].Localisation = i)) then
      begin
        Afficheval('T_', 15);
        AfficheNb(j, 15);
        Afficheval('<' + TresNom(j) + '>', 21);
        Afficheln(1);
      end;
    for j := 1 to Data.NbFlotte do
      if Data.F[j].Localisation = i then
      begin
        Afficheval('F', 8);
        if (Data.F[j].Statut and Puis2(7)) > 0 then
          Afficheval('#', 8)
        else
          Afficheval('_', 8);
        AfficheNb(j, 8);
        Afficheval('', 15);
        if Data.F[j].Proprio > 0 then
        begin
          if Data.F[j].Statut mod 2 = 1 then
            Afficheval('(', 8);
          Afficheval('"', 14);
          Afficheval(Data.Jou[Data.F[j].Proprio].Pseudo, 8);
          if ((FE[j].AncienProprio > 0) and
            (FE[j].AncienProprio <> Data.F[j].Proprio)) then
            Afficheval('(' + Data.Jou[FE[j].AncienProprio].Pseudo + ')', 8);
          Afficheval('"', 8);
          if Data.F[j].Statut mod 2 = 1 then
            Afficheval(')', 8);
        end;
        if (FE[j].Statut and Puis2(0)) > 0 then
          Afficheval('!', 8);
        if (FE[j].Statut and Puis2(1)) > 0 then
          Afficheval('P', 8);
        if (FE[j].Statut and Puis2(2)) > 0 then
          Afficheval('C', 8);
        if (FE[j].Statut and Puis2(4)) > 0 then
          Afficheval('D', 8);
        if (FE[j].Statut and Puis2(5)) > 0 then
          Afficheval('E', 8);
        if Data.F[j].Proprio > 0 then
          Afficheval(' ', 8);
        if ((Data.F[j].NbVC + Data.F[j].NbVT > 0) or
          (FE[j].OrdrExcl in [1 .. 6]) or ((FE[j].Statut and Puis2(3)) > 0))
        then
        begin
          Afficheval('[', 8);
          if ((NoJoueur > 0) and (NoJoueur <= NbJou) and
            (((Data.F[j].Proprio <> NoJoueur) and
            (Data.Jou[NoJoueur].Equipe.IndexOf(IntToStr(Data.F[j].Proprio))
            = -1))) and (Data.F[j].NbVC + Data.F[j].NbVT > 0)) then
            Afficheval('?', 8)

          else
          begin
            avant := False;
            if Data.F[j].MP <> 0 then
            begin
              AfficheNb(Data.F[j].MP, 8);
              avant := True;
            end;

            if avant and (Data.F[j].n <> 0) then
              Afficheval(',', 8);
            if Data.F[j].n > 0 then
            begin
              AfficheNb(Data.F[j].n, 8);
              Afficheval('N', 8);
              avant := True;
            end;

            if avant and (Data.F[j].C <> 0) then
              Afficheval(',', 8);
            if Data.F[j].C > 0 then
            begin
              AfficheNb(Data.F[j].C, 8);
              Afficheval('C', 8);
              avant := True;
            end;

            if avant and (Data.F[j].R <> 0) then
              Afficheval(',', 8);
            if Data.F[j].R > 0 then
            begin
              AfficheNb(Data.F[j].R, 8);
              Afficheval('R', 8);
            end;
          end;
          Afficheval(']', 8);
          if Data.F[j].NbVC + Data.F[j].NbVT > 0 then
          begin
            Afficheval('=', 8);
            if (NoJoueur > 0) and (NoJoueur <= NbJou) then
            begin
              if (Data.Jou[NoJoueur].ALI = 0) and
                ((Data.F[j].Proprio <> NoJoueur) and
                (Data.Jou[NoJoueur].Equipe.IndexOf(IntToStr(Data.F[j].Proprio)
                ) = -1)) then
                ALI := False
              else
                ALI := True;
            end
            else
              ALI := True;

            if ALI then
            begin
              avant := False;
              if Data.F[j].NbVC > 0 then
              begin
                AfficheNb(Data.F[j].NbVC, 8);
                avant := True;
              end;
              if (Data.F[j].NbVT > 0) and avant then
                Afficheval('+', 8);
              if Data.F[j].NbVT > 0 then
              begin
                AfficheNb(Data.F[j].NbVT, 8);
                Afficheval('T', 8);
              end;
            end
            else
            begin
              AfficheNb(Data.F[j].NbVC + Data.F[j].NbVT, 8);
              Afficheval('?', 8);
            end;
          end;
          if FE[j].OrdrExcl in [1 .. 6] then
          begin
            Afficheval('*', 8);
            case FE[j].OrdrExcl of
              1:
                begin
                  Afficheval('F_', 8);
                  AfficheNb(FE[j].Cible, 8);
                end;
              2:
                Afficheval('M', 8);
              3:
                Afficheval('P', 8);
              4:
                Afficheval('I', 8);
              5:
                Afficheval('R', 8);
              6:
                Afficheval('B', 8);
            end;
          end;
          if (FE[j].Statut and Puis2(3)) > 0 then
            Afficheval('**', 8);
          Afficheval(' ', 8);
        end;
        if FE[j].Chemin[1] <> 0 then
          for k := 2 to MAX_CHEMIN do
            if ((FE[j].Chemin[k - 1] <> 0) and
              (FE[j].Chemin[k - 1] <> Integer(F[j].Localisation))
              and ((k = MAX_CHEMIN) or (FE[j].Chemin[k + 1] = 0))
              and ((FE[j].Chemin[k] = 0) or
              (FE[j].Chemin[k] = Integer(F[j].Localisation)))) then
            begin
              Afficheval('du M_', 8);
              if EstConnu(FE[j].Chemin[k - 1], NoJoueur) or (NoJoueur = 0) or
                (NoJoueur = 255) then
                AfficheNb(FE[j].Chemin[k - 1], 8)
              else
                Afficheval('?', 8);
            end;
        Afficheln(8);
        for k := 1 to NbTres do
          if ((Data.T[k].Statut = 1) and (Data.T[k].Localisation = j)) then
          begin
            Afficheval('T_', 15);
            AfficheNb(k, 15);
            Afficheval('<' + TresNom(k) + '>', 21);
            Afficheln(21);
          end;
      end;
    for j := 1 to Data.NbFlotte do
    begin
      for k := 1 to MAX_CHEMIN - 1 do
        if (FE[j].Chemin[k] = i) and
         ((FE[j].Chemin[k + 1] <> 0) // monde sur le chemin et pas en derni�re position
          or ((F[j].Localisation <> i) and (F[j].Localisation <> 0))) // monde sur le chemin en derni�re position et localisation diff�rente
        then
        begin
          Afficheval('{F_', 7);
          AfficheNb(j, 8);
          Afficheval(AffNom(FE[j].AncienProprio, 0) + ' ', 15);
          if k > 1 then
          begin
            Afficheval('du M_', 8);
            if (EstConnu(FE[j].Chemin[k - 1], NoJoueur) or (NoJoueur = 0)
              or (NoJoueur = 255))
            then
              AfficheNb(FE[j].Chemin[k - 1], 8)
            else
              Afficheval('?', 8);
          end;

          if k > 1 then
            Afficheval(' ', 8);
          Afficheval('vers M_', 8);
          if FE[j].Chemin[k + 1] > 0 then
            AfficheNb(FE[j].Chemin[k + 1], 8)
          else
            AfficheNb(F[j].Localisation, 8);

          Afficheval('}', 8);
          Afficheln(8);
        end;
    end;
  end;
end;

function AfficheMondeStr(NebData: TNebdata; NoJoueur, md: word): string;
begin
  CR.Clear;

  AfficheMonde(NebData, nil, NebData.NoJou, md);

  Result := CR.Text;
end;

function AfficheMondeFirstLine(NebData: TNebdata; NoJoueur, md: word): string;
var
  CRMonde: TSTringList;
  CRExpress: TSTringList;
  Ligne: string;
  finMonde: boolean;
  i: integer;
begin
  CRMonde := TSTringList.Create;
  CRExpress := TSTringList.Create;
  CRMonde.Text := AfficheMondeStr(NebData, NoJoueur, md);

  finMonde := False;
  i := 0;

  repeat
    Ligne := CRMonde[i];
    if Ligne.Contains('T_') or Ligne.Contains(' F_') or Ligne.Contains('{F_')
      or Ligne.Contains('F#') then
    begin
      finMonde := True;
    end
    else
    begin
      CRExpress.Add(Ligne);
      Inc(i);
    end;
  until finMonde or (i > CRMonde.Count - 1);

  Result := CRExpress.Text;

  CRMonde.Free;
  CRExpress.Free;
end;

Procedure AfficheResult(Data, oldData: TNebdata; NoJou: integer);
Var
  i: integer;
begin
  NoJoueur := NoJou;
  with Data do
  begin
    for i := 1 to Data.NbMonde do
    begin
      if (NoJoueur = 255) or (((NoJoueur = 0) and (NbConnu[i] <> 0)) or
        (EstConnu(i, NoJoueur) and (NoJoueur > 0))) then
      begin
        AfficheMonde(Data, oldData, NoJoueur, i);
        Afficheln(1);
      end;
    end;

    if (NoJoueur <> 255) then
    begin
      AfficheOrdresData(Data);

      // TODO : n'imprimer ce s�parateur que lorsqu'on imprime le CR pour plusieurs joueurs.
      AfficheLn(1);
      AfficheLn('--------------------------------------------------------------', 1);
    end;
  end;
end;

procedure ImprimeData(theData, oldData: TNebdata; NoJou: integer; CRList: TStringList); overload;
var
  Jou: integer;
begin
  CR.Clear;
  NoJoueur := NoJou;
  Position := 1;

  Data := theData;

  if NoJoueur <> 255 then
  begin
    for Jou := 1 to Data.NbJou do
      if (NoJou = 0) or (Jou = NoJou) then
      begin
        NoJoueur := Jou;
        AfficheEnTete;
        AfficheResult(Data, oldData, NoJoueur);
      end;
  end
  else
    AfficheResult(Data, nil, NoJoueur);

  CRList.Assign(CR);
end;

function ImprimeData(theData, oldData: TNebdata; NoJou: integer): String; overload;
var
  CRList: TStringList;
begin
  CRList := TStringList.Create;
  ImprimeData(theData, oldData, NoJou, CRList);
  Result := CRList.Text;
  CRList.Free;
end;

Initialization

begin
  Position := 1;
  Ch := '';
  CR := TSTringList.Create;
end;

Finalization

begin
  CR.Free;
end;

end.
