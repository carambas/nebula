// Mail G�n�rique
// Classe g�rant le contenu d'un mail minimaliste
// En vue de l'envoyer de diff�rentes fa�ons :
// SMTP, Eudora, Pegasus Mail, ...
// Un seul destinataire, un seul CC, Un body classique
// Et une liste de fichiers en attachement

unit genmail;

interface

uses Classes;

type TGenMail = class
private
public
  Recipient : string;
  CC : string;
  FromAddress : string;
  FromName : string;
  Subject : string;
  Body : TStringList;
  MailAgent : string;
  Attachements : TSTringList;

  constructor Create;
  destructor Destroy; override;
end;


implementation

constructor TGenMail.Create;
begin
  Body := TStringList.Create;
  Attachements := TStringList.Create;
end;

destructor TGenMail.Destroy;
begin
  Body.Free;
  Attachements.Free;
end;

end.
