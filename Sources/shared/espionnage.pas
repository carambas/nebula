unit espionnage;

// Permet de remplir les informations d'espionnage � sauver dans un fichier .nbx � partir
// - des donn�es d'une structure NebData
// - des informations d'espionnage d�j� calcul�es dans un fichier .nbx

interface

uses
  Classes, Nebdata, Utils, Eval.Etape;

type
TStatsEspionnage = class
  public
    ClassementsStatsPresents: Boolean;
    EspionsPresents: Boolean;

    constructor Create(Data: TNebData; Espions: PTabEspions = nil);
    procedure CalculeStatsEspionnage;
    procedure LitDansNBx(jou: Integer; List: TStringList);
    procedure SauveDansNBx(jou: Integer; List: TStringList);
    procedure InitStats;
    procedure FinaliseStats;

  private
    Data: TNebData;
    FEspions: PTabEspions;

    procedure AjouteStatsEspionnage;
    procedure CalculeStatPersonnelles;
    function CoefRandomEspionnage(Niveau: Integer): Real;
    function MoyenneStat(Nombres: StatJoueurs; NbJou: Integer): Integer;
    function StatsPresentesDansNbx(jou: Integer; List: TStringList): Boolean;
    function StatsEtClassementPresentsDansNbx(jou: Integer; List: TStringList): Boolean;
end;

implementation

uses
  SysUtils, Avatunit, Nbtstringlist;

constructor TStatsEspionnage.Create(Data: TNebData; Espions: PTabEspions = nil);
begin
  Self.Data := Data;
  FEspions := Espions;
  ClassementsStatsPresents := False;
  EspionsPresents := False;
end;

// Si on n'a pas trouv� de stats en lisant le fichier .nbx alors qu'il y
// a eu de l'esptionnage alors on les calcule.
procedure TStatsEspionnage.FinaliseStats;
begin
  if EspionsPresents and not ClassementsStatsPresents then
    CalculeStatsEspionnage;
end;

// Calcule les stats personnelles de chaque joueur
procedure TStatsEspionnage.CalculeStatPersonnelles;
var
  md, fl, tr: Integer;
begin
  with Data do
  begin
    for md := 1 to NbMonde do
    begin
      if M[md].Proprio > 0 then
      begin
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_CM], M[md].PlusMP);
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_POPULATIONS], M[md].Pop);
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_ROBOTS], M[md].Robot);
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_MONDES]);
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_INDUSTRIES], M[md].Ind);
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_IND_ACTIVES], ME[md].IndLibre);
        Inc(Jou[M[md].Proprio].Stats[M[md].Proprio, STAT_VAISSEAUX], M[md].VI + M[md].VP);
      end;

      if M[md].ProprConv > 0 then
        Inc(Jou[M[md].ProprConv].Stats[M[md].ProprConv, STAT_CONVERTIS], M[md].Conv);
    end;

    for fl := 1 to NbFlotte do
      if F[fl].Proprio > 0 then
        with Jou[F[fl].Proprio] do
        begin
          Inc(Stats[F[fl].Proprio, STAT_POPULATIONS], F[fl].N + F[fl].C);
          Inc(Stats[F[fl].Proprio, STAT_CONVERTIS], F[fl].C);
          Inc(Stats[F[fl].Proprio, STAT_ROBOTS], F[fl].R);
          Inc(Stats[F[fl].Proprio, STAT_CARGAISON], ContenuFlotte(fl));
          Inc(Stats[F[fl].Proprio, STAT_FLOTTES]);
          Inc(Stats[F[fl].Proprio, STAT_VAISSEAUX], F[fl].NbVC + F[fl].NbVT);
        end;

    for tr := 1 to NbTres do
      if T[tr].Proprio > 0 then
        Inc(Jou[T[tr].Proprio].Stats[T[tr].Proprio, STAT_TRESORS]);
  end;
end;

function TStatsEspionnage.CoefRandomEspionnage(Niveau: Integer): Real;
begin
  if Niveau > 7 then
    Niveau := 7;
  if Niveau < 0 then
    Niveau := 0;

  Result := 1 - COEF_ESPIONNAGE[Niveau] + 2 * COEF_ESPIONNAGE[Niveau] * Random;
end;

function TStatsEspionnage.MoyenneStat(Nombres: StatJoueurs; NbJou: Integer): Integer;
var
  j, tot: Integer;
begin
  tot := 0;
  for j := 1 to NbJou do
    Inc(tot, Nombres[j]);

  Result := Round2(tot / NbJou);
end;


function TStatsEspionnage.StatsPresentesDansNbx(jou: Integer; List: TStringList): Boolean;
begin
  Result := False;

  if List.IndexOfName(Format('Stats[%d]', [jou])) >=0 then
    Result := True;
end;


function TStatsEspionnage.StatsEtClassementPresentsDansNbx(jou: Integer; List: TStringList): Boolean;
begin
  Result := False;

  if (List.IndexOfName(Format('Stats[%d]', [jou])) >=0) and (List.IndexOfName('StatsPremier') >= 0) then
  begin
    ClassementsStatsPresents := True;
    Result := True;
  end;
end;

procedure TStatsEspionnage.InitStats;
var
  jou: Integer;
begin
  for jou := 1 to Data.NbJou do
  begin
    FillChar(Data.Jou[jou].Stats, sizeof(Data.Jou[jou].Stats), 0);
    FillChar(Data.Jou[jou].StatsClassementRang, sizeof(Data.Jou[jou].StatsClassementRang), 0);
    FillChar(Data.Jou[jou].StatsPremier, sizeof(Data.Jou[jou].StatsPremier), 0);
    FillChar(Data.Jou[jou].StatsDernier, sizeof(Data.Jou[jou].StatsDernier), 0);
    FillChar(Data.Jou[jou].StatsMoyenne, sizeof(Data.Jou[jou].StatsMoyenne), 0);
  end;
end;

procedure TStatsEspionnage.LitDansNBx(jou: Integer; List: TStringList);
var
  DataStr: string;
  DataList: TNBTStringList;
  i, rang, jou_vu, statid: Integer;

begin
  DataList := TNBTStringList.Create;

  DataStr := List.Values['Stat'];
  DataList.NbElt := 0;
  DataList.CommaText := DataStr;
  for i := 1 to DataList.Count do
  begin
    Data.Jou[jou].Espions[i] := StrToIntDef(DataList[i - 1], 0);
    if Data.Jou[jou].Espions[i] > 0 then
      EspionsPresents := True;
  end;

  StatsEtClassementPresentsDansNbx(jou, List);

  if StatsPresentesDansNbx(jou, List) then
  begin
    // Stats[j] : valeurs de stat du joueur j vu par le joueur courant
    for jou_vu := 1 to Data.NbJou do
    begin
      DataStr := List.Values['Stats[' + IntToStr(jou_vu) + ']'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for statid := 1 to DataList.Count do
        Data.Jou[jou].Stats[jou_vu, statid] := StrToIntDef(DataList[statid - 1], 0);
    end;

    // StatsClassementRang[r] : Pour chaque stat, num�ro du joueur de rang r
    for rang := 1 to Data.NbJou do
    begin
      DataStr := List.Values[Format('StatsClassementRang[%d]', [rang])];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for statid := 1 to DataList.Count do
        Data.Jou[jou].StatsClassementRang[rang, statid] := StrToIntDef(DataList[statid - 1], 0);
    end;

    // StatsPremier : valeur de chaque stat du premier (rang 1 pour la stat donn�e)
    DataStr := List.Values['StatsPremier'];
    DataList.NbElt := 0;
    DataList.CommaText := DataStr;
    for statid := 1 to DataList.Count do
      Data.Jou[jou].StatsPremier[statid] := StrToIntDef(DataList[statid - 1], 0);

    // StatsDernier : valeur de chaque stat du dernier
    DataStr := List.Values['StatsDernier'];
    DataList.NbElt := 0;
    DataList.CommaText := DataStr;
    for statid := 1 to DataList.Count do
      Data.Jou[jou].StatsDernier[statid] := StrToIntDef(DataList[statid - 1], 0);

    // StatsMoyenne : valeur moyenne de chaque stat
    DataStr := List.Values['StatsMoyenne'];
    DataList.NbElt := 0;
    DataList.CommaText := DataStr;
    for statid := 1 to DataList.Count do
      Data.Jou[jou].StatsMoyenne[statid] := StrToIntDef(DataList[statid - 1], 0);
  end;

  DataList.Free;
end;

procedure TStatsEspionnage.SauveDansNBx(jou: Integer; List: TStringList);
var
  jou_vu: Integer;
  strStats: String;
begin
  List.Add('Stat=' + CompresseChaine(TabToStr(Data.Jou[jou].Espions)));

  // Statistiques personnelles et issues de l'espionnage
  // Stats[i] : valeurs de stat du joueur i vu par le joueur courant
  // StatsClassementRang[r] : Pour chaque stat, num�ro du joueur de rang r
  // StatsPremier : valeur de chaque stat du premier (rang 1 pour la stat donn�e)
  // StatsDernier : valeur de chaque stat du dernier
  // StatsMoyenne : valeur moyenne de chaque stat
  for jou_vu := 1 to Data.NbJou do
    if Data.ConnuDeNom[jou, jou_vu] then
    begin
      strStats := CompresseChaine(TabToStr(Data.Jou[jou].Stats[jou_vu]));
      if strStats <> '' then
        List.Add(Format('Stats[%d]=', [jou_vu]) + strStats);
    end;

  for jou_vu := 1 to Data.NbJou do
  begin
    strStats := CompresseChaine(TabToStr(Data.Jou[jou].StatsClassementRang[jou_vu]));
    if strStats <> '' then
      List.Add(Format('StatsClassementRang[%d]=', [jou_vu]) + strStats);
  end;

  strStats := CompresseChaine(TabToStr(Data.Jou[jou].StatsPremier));
  if strStats <> '' then
    List.Add('StatsPremier=' + strStats);

  strStats := CompresseChaine(TabToStr(Data.Jou[jou].StatsDernier));
  if strStats <> '' then
    List.Add('StatsDernier=' + strStats);

  strStats := CompresseChaine(TabToStr(Data.Jou[jou].StatsMoyenne));
  if strStats <> '' then
    List.Add('StatsMoyenne=' + strStats);
end;

// Compl�te les stats personnelles avec l'espionnage d'autres joueurs
procedure TStatsEspionnage.AjouteStatsEspionnage;
var
  j, jou_vu, statid, rang: Integer;
  NiveauEspionnage: Integer;
  Nombres, clas: StatJoueurs;
begin
  with Data do
  begin
    for j := 1 to NbJou do
    begin
      if FEspions <> nil then
        FillChar(Jou[j].Espions, sizeof(Jou[j].Espions), 0);

      for statid := 1 to NB_STATS do
      begin
        // Si tableau des espions est fourni (suite � une �val), on s'en sert
        // Sinon on reste sur les espions d�j� pr�sents dans Data
        if FEspions <> nil then
          Jou[j].Espions[statid] := FEspions[j, statid];

        NiveauEspionnage := Jou[j].Espions[statid];

        if NiveauEspionnage > 0 then
        begin
          for jou_vu := 1 to NbJou do
            if (jou_vu <> j)  then
              // On recopie les stats du joueur
              Jou[j].Stats[jou_vu, statid] := Round2(Jou[jou_vu].Stats[jou_vu, statid] * CoefRandomEspionnage(NiveauEspionnage));

          // Calcul du classement
          FillChar(clas, sizeof(clas), 0);
          FillChar(Nombres, sizeof(Nombres), 0);
          for jou_vu := 1 to NbJou do
            Nombres[jou_vu] := Jou[j].Stats[jou_vu, statid];

          TriStats(Nombres, clas, NbJou);

          // Premier, Dernier et Moyenne
          for jou_vu := 1 to NbJou do
            Jou[j].StatsClassementRang[jou_vu, statid] := clas[jou_vu]; // Place => Numjou
          Jou[j].StatsPremier[statid] := Nombres[clas[1]];
          Jou[j].StatsDernier[statid] := Nombres[clas[NbJou]];
          Jou[j].StatsMoyenne[statid] := MoyenneStat(Nombres, NbJou);

          // Remettre stats � z�ro pour les joueurs non connus
          for jou_vu := 1 to NbJou do
          begin
            if (jou_vu <> j) and (not ConnuDeNom[j, jou_vu]) then
              Jou[j].Stats[jou_vu, statid] := 0;
          end;

          for rang := 1 to NbJou do
          begin
            if (clas[rang] <> j) and (not ConnuDeNom[j, clas[rang]]) then
              Jou[j].StatsClassementRang[rang, statid] := 0;
          end;
        end;
      end;
    end;
  end;
end;

procedure TStatsEspionnage.CalculeStatsEspionnage;
begin
  InitStats;

  CalculeStatPersonnelles;

  AjouteStatsEspionnage;
end;

end.

