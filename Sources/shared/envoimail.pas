unit envoimail;

interface
  uses genmail;

  function EnvoiMailEudora_new(Mail : TGenMail; LocEudora, ParamEudora : string) : boolean;
  function EnvoiMailSMTP(theMail : TGenMail; SMTPServer : string; VerifConnex : boolean) : boolean;

implementation

  uses windows, dialogs, sysutils, utils, IdMessageClient, IdSMTP, IdMessage,
       IdIPWatch, IdCoderQuotedPrintable, IDText, IdAttachment, IdAttachmentFile;

(*function EnvoiMailEudora(Mail : T_Mail; LocEudora, ParamEudora : string) : boolean;
var
  FicMail : TextFile;
  NomFic, tempStr : string;
  i : integer;
  erreur : integer;
begin
  // On attend qu'Eudora soit lanc�

  if ((FindWindow(nil, 'Eudora pro') = 0) and (FindWindow(nil, 'Eudora Light') = 0)) then
  begin
    //tempStr := EudoraPath;
    //erreur := WinExec('"' + PChar(LocEudora + '" "' + ParamEudora + '"'), SW_SHOWMINNOACTIVE);
    erreur := WinExec(PChar('"' + LocEudora + '" ' + ParamEudora), SW_SHOWMINNOACTIVE);
    if erreur < 32 then
      ShowMessage(Format('Impossible de lancer Eudora'#10#13'Erreur %d', [erreur]));
  end;

  NomFic := GetNomFicTempo;
  AssignFile(FicMail, NomFic);

  ReWrite(FicMail);
  Write(FicMail, 'To: ');
  for i := 0 to Mail.Recipients.Count - 1 do
    Write(FicMail, Mail.Recipients[i] + ';');
  Writeln(FicMail);

  Write(FicMail, 'Cc: ');
  for i := 0 to Mail.CC.Count - 1 do
    Write(FicMail, Mail.CC[i] + ';');
  Writeln(FicMail);

  Writeln(FicMail, 'From: ' + Mail.From);
  Writeln(FicMail, 'Subject: ' + Mail.Subject);
  Writeln(FicMail, '');

  {A l'ancienne : les retours � la ligens ont les m�mes que ceux du M�mo}
  {for i := 0 to Mail.Body.Count - 1 do
    Writeln(FicMail, Mail.Body[i]);}

  {Maintenant on r�cup�re le buffer d'un coup}
  Write(FicMail, Mail.Body.Text);


  {Envoi du mail � Eudora}
  NomFic := ChangeFileExt(NomFic, '.MSG');
  CloseFile(FicMail);
  Rename(FicMail, NomFic);

  tempStr := LowerCase('"' + LocEudora + '" ' + NomFic);
  erreur := WinExec(PChar(tempStr), SW_SHOWNOACTIVATE);
  if erreur < 32 then
    ShowMessage(Format('Impossible de lancer Eudora'#10#13'Erreur %d', [erreur]));
  AssignFile(FicMail, NomFic); {Par s�curit�}
  Erase(FicMail);
  if erreur >= 32 then
    result := true
  else
    result := false;
end;*)

function EnvoiMailEudora_new(Mail : TGenMail; LocEudora, ParamEudora : string) : boolean;
var
  FicMail : TextFile;
  NomFic, tempStr : string;
  erreur : integer;
begin
  // On attend qu'Eudora soit lanc�

  if ((FindWindow(nil, 'Eudora pro') = 0) and (FindWindow(nil, 'Eudora Light') = 0) and (FindWindow(nil, 'Eudora') = 0)) then
  begin
    erreur := WinExec(PAnsiChar(AnsiString('"' + LocEudora + '" ' + ParamEudora)), SW_SHOWMINNOACTIVE);
    if erreur < 32 then
      ShowMessage(Format('Impossible de lancer Eudora'#10#13'Erreur %d', [erreur]));
  end;

  NomFic := GetNomFicTempo;
  AssignFile(FicMail, NomFic);

  ReWrite(FicMail);
  Write(FicMail, 'To: ' + Mail.Recipient);
  //for i := 0 to Mail.Recipients.Count - 1 do
  //  Write(FicMail, Mail.Recipients[i] + ';');
  Writeln(FicMail);

  Write(FicMail, 'Cc: ' + Mail.CC);
  //for i := 0 to Mail.CC.Count - 1 do
  //  Write(FicMail, Mail.CC[i] + ';');
  Writeln(FicMail);

  Writeln(FicMail, 'From: ' + Mail.FromAddress);
  Writeln(FicMail, 'Subject: ' + Mail.Subject);
  Writeln(FicMail, '');

  {A l'ancienne : les retours � la ligens ont les m�mes que ceux du M�mo}
  {for i := 0 to Mail.Body.Count - 1 do
    Writeln(FicMail, Mail.Body[i]);}

  {Maintenant on r�cup�re le buffer d'un coup}
  Write(FicMail, Mail.Body.Text);


  {Envoi du mail � Eudora}
  NomFic := ChangeFileExt(NomFic, '.MSG');
  CloseFile(FicMail);
  Rename(FicMail, NomFic);

  tempStr := LowerCase('"' + LocEudora + '" ' + NomFic);
  erreur := WinExec(PAnsiChar(AnsiString(tempStr)), SW_SHOWNOACTIVATE);
  if erreur < 32 then
    ShowMessage(Format('Impossible de lancer Eudora'#10#13'Erreur %d', [erreur]));
  AssignFile(FicMail, NomFic); {Par s�curit�}
  Erase(FicMail);
  if erreur >= 32 then
    result := true
  else
    result := false;
end;

function EnvoiMailSMTP(theMail : TGenMail; SMTPServer : string; VerifConnex : boolean) : boolean;
var
  theSMTP : TIdSMTP;
  theMessage : TIdMessage;
  IdIPWatch : TIdIPWatch;
  IsOnline : boolean;
  Attachment : boolean;
begin
  IdIPWatch := TIdIPWatch.Create(nil);
  IdIPWatch.HistoryEnabled := False;
  IdIPWatch.ForceCheck;
  IsOnline := IdIPWatch.IsOnline;
  IdIPWatch.Free;

  If VerifConnex and not IsOnline then
  begin
    ShowMessage('Vous devez vous connecter � Internet au pr�alable');
    Result := False;
  end
  else
  begin
    theSMTP := TIdSMTP.Create(nil);
    theMessage := TIdMessage.Create(nil);
    try
      if theMail.Attachements.Count > 0 then
        Attachment := True
      else
        Attachment := False;

      theMessage.UseNowForDate := True;

      theMessage.From.Address := theMail.FromAddress;
      theMessage.From.Name := theMail.FromName;
      (theMessage.Recipients.Add).Address := theMail.Recipient;
      if theMail.CC <> '' then
        (theMessage.CCList.Add).Address := theMail.CC;
      theMessage.Subject := theMail.Subject;

      if Attachment then
      begin
        // Partie texte
        with TIdText.Create(theMessage.MessageParts, theMail.Body) do
        begin
          CharSet := 'ISO-8859-1';
        end;

        // Pi�ces jointes
        TIdAttachmentFile.Create(theMessage.MessageParts, theMail.Attachements[0]);
      end
      else
      begin
        theMessage.ContentTransferEncoding := 'quoted-printable';
        theMessage.ContentType := 'text/plain';
        theMessage.CharSet := 'ISO-8859-1';
        theMessage.ExtraHeaders.Add('MIME-Version: 1.0');
        theMessage.IsEncoded := True;
        theMessage.Body.Text := theMail.Body.Text;
      end;

      if theMail.MailAgent <> '' then
        theSMTP.MailAgent := theMail.MailAgent;
      theSMTP.Host := SMTPServer;
      theSMTP.Connect;
      try
        theSMTP.Send(theMessage);
      finally
        theSMTP.Disconnect;
      end;
    finally
      theSMTP.Free;
      theMessage.Free;
    end;
    Result := True;
  end;

end;


end.
