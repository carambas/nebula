unit listint;

interface

uses Classes;

type
  TIntList = class(TList)
  private
    function GetItem(index : integer) : integer;
    procedure SetItem(index, value : integer);
  public
    function Add(Item : integer): Integer;
    function IndexOf(Item : integer): Integer;
    procedure Insert(Index : Integer; Item : integer);
    function First : integer;
    function Last : integer;

    property Items[index : integer] : integer read GetItem write SetItem; default;
  end;

implementation

function TIntList.GetItem(index : integer) : integer;
begin
  Result := Integer(Inherited Items[index]);
end;

procedure TIntList.SetItem(index, value : integer);
begin
  Inherited Items[index] := Pointer(value);
end;

function TIntList.Add(Item : integer): Integer;
begin
  Result := Inherited Add(Pointer(Item));
end;

function TIntList.IndexOf(Item : integer): Integer;
begin
  Result := Inherited IndexOf(Pointer(Item))
end;

procedure TIntList.Insert(Index : Integer; Item : integer);
begin
  Inherited Insert(Index, Pointer(Item));
end;

function TIntList.First : integer;
begin
  Result := Integer(Inherited First);
end;

function TIntList.Last : integer;
begin
  Result := Integer(Inherited Last);
end;


end.
