unit utils;

INTERFACE

{$IFDEF NOGUI}
uses AvatUnit, ListInt, Classes
  {$IFDEF MSWINDOWS}
  ,windows
  {$ENDIF}
;
{$ELSE}
uses AvatUnit{,tcpip}, ListInt, Classes, IdGlobal;
{$ENDIF}

{$IFDEF NOGUI}
type
TMsgDlgBtn = (mbYes, mbNo, mbOK, mbCancel, mbAbort, mbRetry, mbIgnore, mbAll, mnNoToAll, mbYesToAll, mbHelp);
{$ENDIF}

type
  StatJoueurs = array [1 .. NB_JOU_MAX] of integer;



function chtoi(ch : PChar) : longint;

function MaxTabVal(a : array of integer) : integer;
function MaxTabIndex(a : array of integer) : integer;
function GetTourCh(Notour : integer) : string;
function Min(a,b : integer) : integer;
function GetNomFicTempo : string;
function ProgRep : string;


function EstNombre(s : string) : boolean;
function Round2(r : real) : longint;

Function CalculeCA(tech : integer) : longint;
Function CalculeCD(tech : integer) : longint;

function EnvoiCRCode(Destinataire : byte; NbRot : byte) : string;
function EnleveAccents(s : string) : string;
Function AugPop(x, max, NbExploPop : integer) : integer;

function BitfieldToListStr(tab : integer) : string;
function BitfieldToStr(tab : integer) : string;
function TabToStr(tab : array of integer) : string; overload;
function TabToStr(tab : array of UInt16) : string; overload;
function TabToStr(tab : array of UInt64) : string; overload;
function TabByteToStr(tab : array of byte) : string;
function TabStrToStr(tab : array of string) : string;

function CompresseChaine(Str : string) : string;
function GetNomFicOrdres(NomPartie : string; Tour : integer) : string;

{ressources demand�es -> ressources obtenues}
procedure RepartitionRessources(Demande, Obtenu : TIntList; Dispo : integer);
procedure RepartitionRessourcesTab(var Demande, Obtenu : array of integer; Dispo : integer);
function  BinomialCoeff (N, R: Cardinal): Extended;
{$IFDEF NOGUI}
procedure ShowMessage(Str : string);
procedure MessageDlg(Str : string; param1 : integer; param2 : array of TMsgDlgBtn; param3 : integer);
{$ENDIF}
function BoolToStr(b : boolean) : String;
function ConvertFilenameSlashes(fileName : string) : string;

procedure TriStats(Nombres: StatJoueurs; var clas: StatJoueurs; NbJou: Integer);

IMPLEMENTATION

{$IFDEF NOGUI}
uses SysUtils{$IFNDEF FPC}, System.IOUtils{$ENDIF};
{$ELSE}
uses Windows, SysUtils, Dialogs, eval.APIPoints, NebData, System.IOUtils;
{$ENDIF}


procedure Message_Tempo(st : string);
var
   PCharTempo : array[0..255] of char;
begin
   StrPCopy(@PCharTempo, st);
{$IFNDEF NOGUI}
   MessageBox(0, @PCharTempo, 'Divers', MB_ICONEXCLAMATION);
{$ENDIF}
end;

function chtoi(ch : PChar) : longint;
var
   i : longint;
   err : integer;
begin
   val(StrPas(ch), i, err);
   chtoi := i;
end;

function GetNomFicTempo : string;
var
  tempo, tempPath : array[0..144] of char;
begin
  {$IFDEF FPC}
  Result := GetTempFileName;
  {$ELSE}
  Result := TPath.GetTempFileName;
  {$ENDIF}
end;

function ProgRep : string;
var
  LastBackSlashPos, Index : Integer;
begin
  LastBackSlashPos := 0;
  Result := ParamStr(0);
  for Index := 1 to Result.Length do
    if Result[Index] = '\' then
       LastBackSlashPos := Index;
  { subtract 1 so that the last backslash is not included }
  SetLength(Result, LastBackSlashPos - 1);
end;

function EstNombre(s : string) : boolean;
var
  c : Char;
  b : Boolean;
  i : Integer;
begin
  b := True;

  if s.IsEmpty then b := false;

  for c in s do
    if not CharInSet(c, ['0'..'9']) then
    begin
      b := False;
      break;
    end;

  if b then
  begin
    try
      i := s.ToInteger;
      if (i <= 0) or (i > 32767) then b := False;
    except
      on E : EConvertError do b := False;
    end;
  end;

  Result := b;
end;

// Arrondi math�matique : lorsque la partie d�cimale est 0,5 on arrondi �l'entier sup�rieur
// Diff�rent de Round car le Pascal fait un arrondi au nombre pair le plus proche dans ce cas (arrondi bancaire)
function Round2(r : real) : longint;
begin
  if frac(r) = 0.5 then
    Result := trunc(r) + 1
  else
    Result := Round(r);
end;

Function CalculeCA(tech : integer) : longint;
var
  i : integer;
  CA : Real;
begin
  if tech > 100 then
    tech := 100;

  CA := 50;

  for i := 2 to tech do
    CA := CA * 1.1;

  Result := Round2(CA);
end;

Function CalculeCD(tech : integer) : longint;
var
  i : integer;
  CD : Real;
begin
  if tech = 0 then
  begin
    Result := 100;
    Exit;
  end;
  if tech > 100 then
    tech := 100;

  CD := 100;
  for i := 2 to tech do
    CD := CD * 0.9;
  CalculeCD := Round2(CD);
end;

function GetTourCh(NoTour : integer) : string;
begin
  Result := Format('%.2d', [NoTour]);
end;

function Min(a,b : integer) : integer;
begin
  if a < b then
    Result := a
  else
    Result := b;
end;

function EnvoiCRCode(Destinataire : byte; NbRot : byte) : string;
var
  Code : array[1..15] of byte; // en fait ce ne sont que des quartets
  NoQuartet, NoBit : byte;
  salt : byte;
  tempoXor : byte;
  i : integer;

  procedure DoSalt;
  var
    i : integer;
  begin
    salt := Random(15) + 1;
    for i := 1 to 14 do
      Code[i] := Code[i] xor salt;
    Code[15] := salt;
  end;

  procedure RDLR;
  var
    i : integer;
  begin
    for i := 1 to 14 do
    begin
      // tempoXor contient bit 4 xor bit 1 -> r�sulat sur le bit 4;
      tempoXor := ((Code[i] shl 3) xor Code[i]) and $08;
      Code[i] := Code[i] shr 1;
      Code[i] := Code[i] or tempoXor;
    end;
  end;

  function ChaineHexa : string;
  var
    i : integer;
  begin
    Result := '';
    for i := 1 to 15 do
      Result := Result + Format('%x', [Code[i]])
  end;

begin
  Result := '';
  FillChar(Code, sizeof(Code), 0);
  // Code = envoi de CR
  Code[14] := 8;

  // Destinataire
  Code[13] := Destinataire and $0F;
  Code[12] := Destinataire shr 4;

  // Liste des destinataires -> 1 seul
  NoQuartet := 3 + Destinataire div 4;
  NoBit := 3 - Destinataire mod 4;
  Code[NoQuartet] := 1 shl Nobit ;

  DoSalt;
  for i := 1 to NbRot do RDLR;

  Result := ChaineHexa;
end;

function EnleveAccents(s : string) : string;
var
  c : Char;
begin
  Result := '';
  for c in s do
    case c of
      '�', '�', '�', '�' : Result := Result + 'e';
      '�', '�' : Result := Result + 'i';
      '�', '�' : Result := Result + 'u';
      '�', '�' : Result := Result + 'o';
      '�', '�', '�' : Result := Result + 'a';
    else
      Result := Result + c;
    end;
end;

Function AugPop(x, max, NbExploPop : integer) : integer;
var
  Taux : double;
begin
   if NbExploPop < 0 then NbExploPop := 0;
   if NbExploPop > MAXMEVA then NbExploPop := 4;
   Taux := 1.1 + 0.025 * NbExploPop;
   Result := Trunc(x * Taux);
   if random < Frac(x * Taux) then
     Inc(Result);
   if Result > max then
     Result := max;
end;

function TabToStr(tab : array of integer) : string;
var
  i : integer;
begin
  Result := '';
  for i := low(tab) to high(tab) do
  begin
    Result := Result + IntToStr(tab[i]);
    if i < high(tab) then Result := result + ',';
  end;
end;

function TabToStr(tab : array of UInt16) : string;
var
  i : integer;
begin
  Result := '';
  for i := low(tab) to high(tab) do
  begin
    Result := Result + IntToStr(tab[i]);
    if i < high(tab) then Result := result + ',';
  end;
end;

function TabToStr(tab : array of UInt64) : string;
var
  i : integer;
begin
  Result := '';
  for i := low(tab) to high(tab) do
  begin
    Result := Result + IntToStr(tab[i]);
    if i < high(tab) then Result := result + ',';
  end;
end;

function TabByteToStr(tab : array of byte) : string;
var
  i : integer;
begin
  Result := '';
  for i := low(tab) to high(tab) do
  begin
    Result := Result + IntToStr(tab[i]);
    if i < high(tab) then Result := result + ',';
  end;
end;

function TabStrToStr(tab : array of string) : string;
var
  i : integer;
begin
  Result := '';
  for i := low(tab) to high(tab) do
  begin
    Result := Result + tab[i];
    if i < high(tab) then Result := result + ',';
  end;
end;

function BitfieldToStr(tab : integer) : string;
var
  i : integer;
begin
  Result := '';
  for i := 1 to 32 do
    Result := Result + IntToStr((tab and Puis2(i-1)) div Puis2(i-1)) + ',';
  SetLength(Result, length(Result)-1)
end;

function BitfieldToListStr(tab : integer) : string;
var
  i : integer;
begin
  Result := '';
  for i := 1 to 32 do if tab and Puis2(i-1) <> 0 then
    Result := Result + IntToStr(i) + ',';
  if length(Result) > 0 then
    SetLength(Result, length(Result)-1)
end;

function CompresseChaine(Str : string) : string;
var
  {i,} p : integer;
begin
  // On Enl�ve le premier z�ro
  Str := Str.Replace('=0,', '=,');
  if Str.StartsWith('0,') then
    Str := Str.Remove(0, 1);

  // On enl�ve les z�ros au milieu de la chaine
  // Double appel car bug de replace qui ne traite qu'un motif sur 2
  Str := Str.Replace(',0,', ',,', [rfReplaceAll]);
  Str := Str.Replace(',0,', ',,', [rfReplaceAll]);

  // On enl�ve le z�ro final
  if Str.EndsWith(',0') then
    Str := Str.Remove(Str.Length - 1);

  // On enl�ve les virgules finales
  while Str.EndsWith(',') do
    Str := Str.Remove(Str.Length - 1);

  Result := Str;
end;

// demande et Obtenu : listes d�j� allou�es forc�ment de m�me taille
procedure RepartitionRessources(Demande, Obtenu : TIntList; Dispo : integer);
var
  i : integer;
  x : double;
  TotalDemande : integer;
  Total : integer;
begin
  TotalDemande := 0;
  for i := 0 to Demande.Count - 1 do
    Inc(TotalDemande, Demande[i]);

  if TotalDemande <= Dispo then
  begin
    for i := 0 to Demande.Count - 1 do
      Obtenu[i] := Demande[i];
  end
  else
  begin
    // pas assez de ressources, on les r�parti une � une.
    while dispo > 0 do
    begin
      Total := 0;
      x := Random;
      for i := 0 to Demande.Count - 1 do
      begin
        inc(Total, Demande[i]);
        if Total >= TotalDemande * x then
        begin
          Obtenu[i] := Obtenu[i] + 1;
          Demande[i] := Demande[i] - 1;
          Dec(TotalDemande);
          Dec(dispo);
          break;
        end;
      end;
    end;
  end;
end;

procedure RepartitionRessourcesTab(var Demande, Obtenu : array of integer; Dispo : integer);
var
  ListeDemande, ListeObtenu : TIntList;
  i, j : integer;
begin
  ListeDemande := TIntList.Create;
  ListeObtenu := TIntList.Create;
  for i := Low(Demande) to High(Demande) do
  begin
    ListeDemande.Add(Demande[i]);
    ListeObtenu.Add(0);
  end;
  RepartitionRessources(ListeDemande, ListeObtenu, Dispo);
  j := 0;
  for i := Low(Obtenu) to High(Obtenu) do
  begin
    Obtenu[i] := ListeObtenu[j];
    Inc(j);
  end;
  ListeDemande.Free;
  ListeObtenu.Free;
end;

function BinomialCoeff (N, R: Cardinal): Extended;
var
   I: Integer;
   K: LongWord;
begin
   if (N = 0) or (R > N) or (N > 1547) then
   begin
      Result := 0.0;
      Exit;
   end;
   Result := 1.0;
   if (R = 0) or (R = N) then
      Exit;
   if R > N div 2 then
      R := N - R;
   K := 2;
   try
      for I := N - R + 1 to N do
      begin
         Result := Result * I;
         if K <= R then
         begin
            Result := Result / K;
            Inc (K);
         end;
      end;
      Result := Trunc(Result + 0.5);
   except
      Result := -1.0
   end;
End;

function GetNomFicOrdres(NomPartie : string; Tour : integer) : string;
begin
  Result := Format('%sOrdresTour%.2d.txt', [NomPartie, Tour]);
end;

{$IFDEF NOGUI}
procedure ShowMessage(Str : string);
begin
end;

procedure MessageDlg(Str : string; param1 : integer; param2 : array of TMsgDlgBtn; param3 : integer);
begin
end;
{$ENDIF}

function MaxTabVal(a : array of integer) : integer;
var
  max : integer;
  i : integer;
begin
  max := Low(Integer);
  for i := 1 to High(a) do
    if a[i] > max then
      max := a[i];
  Result := max;
end;

function MaxTabIndex(a : array of integer) : integer;
var
  max, indexmax : integer;
  i : integer;
begin
  indexmax := -1;
  max := Low(Integer);
  for i := 1 to High(a) do
    if a[i] > max then
    begin
      max := a[i];
      indexmax := i;
    end;
  Result := indexmax;
end;

function BoolToStr(b : boolean) : String;
begin
  if b then
    Result := 'Vrai'
  else
    result := 'Faux';
end;

// Evite les chemins de fichiers avec / et \ m�lang�s
function ConvertFilenameSlashes(fileName : string) : string;
var
  i : integer;
begin
  for i := 1 to length(fileName) do

  {$IFDEF LINUX}
    if fileName[i] = '\' then
      fileName[i] := '/';
  {$ENDIF}

  {$IFDEF MSWINDOWS}
    if fileName[i] = '/' then
      fileName[i] := '\';
  {$ENDIF}

  Result := fileName;
end;

procedure TriStats(Nombres: StatJoueurs; var clas: StatJoueurs; NbJou: Integer);
var
  i, j, temp: integer;
begin
  FillChar(clas, sizeof(clas), 0);
  for i := 1 to NbJou do
    clas[i] := i;

  for i := 1 to NbJou - 1 do
    for j := 1 to NbJou - i do
      if Nombres[clas[j]] < Nombres[clas[j + 1]] then
      begin
        temp := clas[j];
        clas[j] := clas[j + 1];
        clas[j + 1] := temp;
      end;

end;


end.
