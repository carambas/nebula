unit Prtpform;

interface

uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, Spin{, Datas}, AvatUnit, Dialogs, SysUtils, ComCtrls{,
  Scroll32}, NebData;

type        
  TPrintPlanDlg = class(TForm)
    GroupBox1: TGroupBox;
    TestPrint: TImage;                                        
    NbPageRadio: TRadioGroup;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    GroupBox2: TGroupBox;
    ZoomTrackBar: TTrackBar;
    GroupBox3: TGroupBox;
    HTrackBar: TTrackBar;
    VTrackBar: TTrackBar;
    FondCoulCheck: TCheckBox;
    GdMondeCheck: TCheckBox;
    Button1: TButton;
    PrinterSetupDialog: TPrinterSetupDialog;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure OptionsClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NbPageRadioClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure ZoomTrackBarChange(Sender: TObject);
    procedure HTrackBarChange(Sender: TObject);
    procedure VTrackBarChange(Sender: TObject);
    procedure FondCoulCheckClick(Sender: TObject);
    procedure GdMondeCheckClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    Zoom : double;
    nbPage : integer;
    CX, CY : double;
    Largeur, Hauteur : double;
    LargeurMultiple, HauteurMultiple : integer;

    procedure DessineAppercu;
    procedure DessinePlan;
    procedure SetTrackBarsSize;
    procedure ImprimePlan(X, Y, Taille : integer);
  public
    { Public declarations }
    Data : TNebData;
    ValZoom: integer;
    MondeSel: integer;
    TailleMonde: integer;
    XMin,YMin: integer;
    function Execute: Boolean;
  end;

var
  PrintPlanDlg: TPrintPlanDlg;

implementation

uses Printers, Plan, Utils;

{$R *.DFM}

procedure TPrintPlanDlg.FormCreate(Sender: TObject);
begin
   Data := nil;
   ValZoom := 1;
   MondeSel := 0;
   Self.TailleMonde := 0;
   XMin := 0;
   YMin := 0;
   CX := 0.5;
   CY := 0.5;
   HTrackBar.Width := TestPrint.Width;
   VTrackBar.Height := TestPrint.Height;
end;

function TPrintPlanDlg.Execute: Boolean;
var
   i: integer;
   S: String;
begin
   Result := False;
(*   //ListeMonde.Items.Clear;
   if Data=nil then begin
      { msg d'erreur }
      exit;
   end;
   TailleMonde := Printer.PageWidth div (2 * Data^.LongX + 1);
   for i := 1 to Data^.NbMonde do begin
      if (Data^.M[i].X <> 0) and (Data^.M[i].Y <> 0) then begin
         Str(i,S);
         ListeMonde.Items.AddObject(S,TObject(i));
      end;
   end;
      if ListeMonde.Items.Count=0 then begin
         { msg d'erreur }
         ShowMessage('Pas de monde � imprimer');
         exit;
      end;
      ListeMonde.ItemIndex:=0;*)
   Result := (ShowModal = mrOk);
end;

procedure TPrintPlanDlg.OptionsClick(Sender: TObject);
begin
   (*se Options.ItemIndex of
      0:begin
         ListeMonde.Visible := False;
         ZoomSpin.Visible := False;
        end;
      1..3:begin
         ListeMonde.Visible := True;
         ZoomSpin.Visible := False;
        end;
      4:begin
         ListeMonde.Visible := True;
         ZoomSpin.Visible := True;
        end;
   end;*)
end;

procedure TPrintPlanDlg.OKBtnClick(Sender: TObject);
var
   z: real;
   CentreX, CentreY : double;
   PageWidth, PageHeight, MinWL : integer;
   X, Y, Taille : integer;
begin
   if Data<>nil then
   begin
     Printer.BeginDoc;
     PageWidth := Printer.PageWidth;
     PageHeight := Printer.PageHeight;
     MinWL := Min(PageWidth, PageHeight);
     Taille := Round(MinWL * Zoom / (2 * Data.Map.LongX + 1));
     X := Round(MinWL * Zoom * (CX - Largeur / 2)) - Taille;
     Y := Round(MinWL * Zoom * (CY - Hauteur / 2)) - Taille;
     PlanForm.WinPlan.FondBlanc := not FondCoulCheck.Checked;
     ImprimePlan(X, Y, Taille);

     if nbPage > 1 then
     begin
       if PageWidth < PageHeight then
       begin
         X := Round(MinWL * Zoom * CX) - Taille;
         Y := Round(MinWL * Zoom * (CY - Hauteur / 2)) - Taille;
       end
       else
       begin
         X := Round(MinWL * Zoom * (CX - Largeur / 2)) - Taille;
         Y := Round(MinWL * Zoom * CY) - Taille;
       end;
       Printer.NewPage;
       ImprimePlan(X, Y, Taille);
     end;

     if nbPage > 2 then
     begin
       if PageWidth < PageHeight then
       begin
         X := Round(MinWL * Zoom * (CX - Largeur / 2)) - Taille;
         Y := Round(MinWL * Zoom * CY) - Taille;
       end
       else
       begin
         X := Round(MinWL * Zoom * CX) - Taille;
         Y := Round(MinWL * Zoom * (CY - Hauteur / 2)) - Taille;
       end;
       Printer.NewPage;
       ImprimePlan(X, Y, Taille);

       X := Round(MinWL * Zoom * CX) - Taille;
       Y := Round(MinWL * Zoom * CY) - Taille;
       Printer.NewPage;
       ImprimePlan(X, Y, Taille);
     end;
     Printer.EndDoc;
   end;
   ModalResult := mrOk;
end;

procedure TPrintPlanDlg.DessinePlan;
begin
  {Affichage de la pr�visualisation du plan}
  with TestPrint.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clWhite;
    Pen.Style := psClear;
    Rectangle(0, 0, ClientWidth, ClientHeight);
  end;
  Self.TailleMonde := TestPrint.ClientWidth div (2 * Data.Map.LongX + 1);
  XMin := -TailleMonde;
  YMin := -TailleMonde;
  PlanForm.WinPlan.Imprimer := True;
  PlanForm.WinPlan.AppercuImpression := True;
  PlanForm.WinPlan.FondBlanc := not FondCoulCheck.Checked;
  PlanForm.WinPlan.GdMondesImpr := GdMondeCheck.Checked;
  PlanForm.WinPlan.DessinePlan;
  PlanForm.WinPlan.Imprimer := False;
  PlanForm.WinPlan.AppercuImpression := False;
  DessineAppercu;
end;

procedure TPrintPlanDlg.DessineAppercu;
var
  X1, X2, Y1, Y2 : integer;
  PageHeight, PageWidth : integer;
begin
  {Affichage du conteur de la zone d'impression}
  LargeurMultiple := 0;
  Hauteurmultiple := 0;
  //ZoomEdit.Text := Format('%3.2f', [Zoom]);
  //CX := 0.5;
  //CY := 0.5;
  PageWidth := Printer.PageWidth;
  PageHeight := Printer.PageHeight;

  case nbPage of
    1 :
      begin
        LargeurMultiple := 1;
        HauteurMultiple := 1;
      end;
    2 :
      begin
        if PageWidth < PageHeight then
        begin
          LargeurMultiple := 2;
          HauteurMultiple := 1;
        end
        else
        begin
          LargeurMultiple := 1;
          HauteurMultiple := 2;
        end;
      end;
    4 :
      begin
        LargeurMultiple := 2;
        HauteurMultiple := 2;
      end;
  end;

  PageWidth := PageWidth * LargeurMultiple;
  PageHeight := PageHeight * HauteurMultiple;

  if Printer.PageWidth < Printer.PageHeight then
  begin
    Largeur := LargeurMultiple / Zoom;
    Hauteur := Largeur * PageHeight / PageWidth;
  end
  else
  begin
    Hauteur := HauteurMultiple / Zoom;
    Largeur := Hauteur * PageWidth / PageHeight;
  end;

  with TestPrint do
  begin
    X1 := Round(ClientWidth * (CX - Largeur / 2));
    X2 := Round(ClientWidth * (CX + Largeur / 2));
    Y1 := Round(ClientHeight * (CY - Hauteur / 2));
    Y2 := Round(ClientHeight * (CY + Hauteur / 2));

    {Mise � jour de la taille des barres de d�filement}
    Canvas.Brush.Style := bsClear;
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := clGray;
    Canvas.Pen.Mode := pmXor;
    case nbPage of
      1 : Canvas.Rectangle(X1, Y1, X2, Y2);
      2 :
        begin
          if PageWidth > PageHeight then
          begin
            Canvas.Rectangle(X1, Y1, (X1 + X2) div 2, Y2);
            Canvas.Rectangle((X1 + X2) div 2, Y1, X2, Y2);
          end
          else
          begin
            Canvas.Rectangle(X1, Y1, X2, (Y1 + Y2) div 2);
            Canvas.Rectangle(X1, (Y1 + Y2) div 2, X2, Y2);
          end;
        end;
      4 :
        begin
          Canvas.Rectangle(X1, Y1, (X1+X2) div 2, (Y1+Y2) div 2);
          Canvas.Rectangle((X1+X2) div 2, Y1, X2, (Y1+Y2) div 2);
          Canvas.Rectangle(X1, (Y1+Y2) div 2, (X1+X2) div 2, Y2);
          Canvas.Rectangle((X1+X2) div 2, (Y1+Y2) div 2, X2, Y2);
        end;
    end;
    Canvas.Pen.Mode := pmCopy;
  end;
end;

procedure TPrintPlanDlg.FormShow(Sender: TObject);
begin
  Zoom := ZoomTrackBar.Position / 10;
  case NbpageRadio.ItemIndex of
    0 : nbPage := 1;
    1 : nbpage := 2;
    2 : nbPage := 4;
  end;

  DessinePlan;
  //DessineAppercu;
  SetTrackBarsSize;
end;

procedure TPrintPlanDlg.NbPageRadioClick(Sender: TObject);
begin
  DessineAppercu;
  case NbpageRadio.ItemIndex of
    0 : nbPage := 1;
    1 : nbpage := 2;
    2 : nbPage := 4;
  end;
  DessineAppercu;
end;

procedure TPrintPlanDlg.FormPaint(Sender: TObject);
begin
{  DessinePlan;
  DessineAppercu;}
end;

procedure TPrintPlanDlg.ImprimePlan(X, Y, Taille : integer);
begin
  XMin := X;
  YMin := Y;
  Self.TailleMonde := Taille;

  PlanForm.WinPlan.Imprimer := True;
  PlanForm.WinPlan.DessinePlan;
  PlanForm.WinPlan.Imprimer := False;
end;

procedure TPrintPlanDlg.ZoomTrackBarChange(Sender: TObject);
begin
  DessineAppercu;
  Zoom := ZoomTrackBar.Position / 10;
  DessineAppercu;
end;

procedure TPrintPlanDlg.SetTrackBarsSize;
begin
  HTrackBar.Min := 0;
  HTrackBar.Max := Printer.PageWidth;
  HTrackBar.Position := Round(CX * HTrackBar.Max);

  VTrackBar.Min := 0;
  VTrackBar.Max := Printer.PageHeight;
  VTrackBar.Position := Round(CY * VTrackBar.Max);
end;

procedure TPrintPlanDlg.HTrackBarChange(Sender: TObject);
begin
  DessineAppercu;
  CX := HTrackBar.Position / HTrackBar.Max;
  DessineAppercu;
end;

procedure TPrintPlanDlg.VTrackBarChange(Sender: TObject);
begin
  DessineAppercu;
  CY := VTrackBar.Position / VTrackBar.Max;
  DessineAppercu;
end;

procedure TPrintPlanDlg.FondCoulCheckClick(Sender: TObject);
begin
  DessinePlan;
end;

procedure TPrintPlanDlg.GdMondeCheckClick(Sender: TObject);
begin
  DessinePlan;
end;

procedure TPrintPlanDlg.Button1Click(Sender: TObject);
begin
  PrinterSetupDialog.Execute;
  DessinePlan;
end;

end.
