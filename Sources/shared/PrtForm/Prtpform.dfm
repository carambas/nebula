�
 TPRINTPLANDLG 0�  TPF0TPrintPlanDlgPrintPlanDlgLeft� TopfHelpContext�BorderIconsbiSystemMenu BorderStylebsDialogCaptionImpression du planClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnPaint	FormPaintOnShowFormShowPixelsPerInchx
TextHeight 	TGroupBox	GroupBox1Left Top
Width�Height�Caption   Apperçu de l'impressionTabOrder  TImage	TestPrintLeftTopWidth@Height@  	TTrackBar	HTrackBarLeft
TopbWidthPHeight)OrientationtrHorizontalPageSize	FrequencyPosition SelEnd SelStart TabOrder TabStop	TickMarkstmBottomRight	TickStyletsNoneOnChangeHTrackBarChange  	TTrackBar	VTrackBarLeftYTopWidthHeightPOrientation
trVerticalPageSize	FrequencyPosition SelEnd SelStart TabOrderTabStop	TickMarkstmBottomRight	TickStyletsNoneOnChangeVTrackBarChange   TRadioGroupNbPageRadioLeft
Top
Width� HeightmCaptionNombre de pages	ItemIndex Items.Strings1 page2 pages4 pages TabOrderOnClickNbPageRadioClick  TBitBtnOKBtnLeftbTop�Width]HeightTabOrderOnClick
OKBtnClickKindbkOKMarginSpacing�  TBitBtn	CancelBtnLeft�Top�Width\HeightTabOrderKindbkCancelMarginSpacing�  	TGroupBox	GroupBox2Left
Top� Width� HeightFCaptionZoomTabOrder 	TTrackBarZoomTrackBarLeft
TopWidth� Height(MaxdMinOrientationtrHorizontalPageSize	Frequency
PositionSelEnd SelStart TabOrder 	TickMarkstmBottomRight	TickStyletsAutoOnChangeZoomTrackBarChange   	TGroupBox	GroupBox3Left
Top� Width� Height� CaptionOptionsTabOrder 	TCheckBoxFondCoulCheckLeft
TopWidth� HeightCaptionFond des mondes en couleurTabOrder OnClickFondCoulCheckClick  	TCheckBoxGdMondeCheckLeft
Top1Width� HeightCaptionGrands mondesTabOrderOnClickGdMondeCheckClick   TButtonButton1Left
Top�Width\HeightCaptionImprimante...TabOrderOnClickButton1Click  TBitBtnBitBtn1Left'Top�Width]HeightTabOrderKindbkHelpMarginSpacing�  TPrinterSetupDialogPrinterSetupDialogLeftXTop`   