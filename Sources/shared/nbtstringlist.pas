unit nbtstringlist;

interface

uses Classes;

type
  TNBTStringList = class(TStringList)
  private
    FNbElt : integer;
  protected
    function VireGuillemets(data : String) : String;
    function GetCommaText : String;
    procedure SetCommaText(Str : String);
  public
    constructor Create;
    procedure VireDoublons;
    property CommaText : String read GetCommaText write SetCommaText;
    property NbElt : integer read FNbElt write FNbElt;
  end;

implementation

uses SysUtils;

constructor TNBTStringList.Create;
begin
  inherited Create;
  Sorted := False;
end;

function TNBTStringList.VireGuillemets(data : String) : String;
begin
  Result := data.Replace('"', '', [rfReplaceAll]);
end;

function TNBTStringList.GetCommaText : String;
begin
  Result := VireGuillemets(inherited CommaText);
end;

procedure TNBTStringList.SetCommaText(Str : String);
var
  i : integer;
begin
  Str := VireGuillemets(Str);
  Clear;

  // D�coupage de la ligne et ajout des �l�ment dans la liste
  AddStrings(Str.Split([',']));

  // Remplacement des chaines vide par des 0
  if Count > NbElt then NbElt := Count;
  for i := 0 to Count - 1 do
    if Self.Strings[i] = '' then
      Self.Strings[i] := '0';

  // Completion de la liste avec des 0
  if Count < NbElt then
    for i := Count to NbElt - 1 do
      Add('0');
end;

procedure TNBTStringList.VireDoublons;
var
  i, j : integer;
begin
  i := 0;
  while (i < Count - 1) do
  begin
    j := i + 1;
    while (j < Count) do
    begin
      if Self[i] = Self[j] then
        Delete(j)
      else
        inc(j)
    end;
    inc(i);
  end;

  // On vire les 0 aussi
  i := 0;
  while (i < Count - 1) do
  begin
    if Self[i] = '0' then
      Delete(i)
    Else
      inc(i)
  end;
end;

end.



