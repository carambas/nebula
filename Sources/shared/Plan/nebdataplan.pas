unit NebDataPlan;

//**********************************************************************
//
// NebDataPlan : classe fille de TNebData
// M�thodes focalis�es sur des informations utiles � l'affichage du plan
//
// *********************************************************************

interface

uses NebData;

type TNebDataPlan = class(TNebData)
  public
    // Calcul sur coordonn�s et distances dans le tore
    function ModuloX(X: Integer): Integer;
    function ModuloY(Y: Integer): Integer;
    function DeltaX(X1, X2: Integer): integer;
    function DeltaY(Y1, Y2: Integer): integer;
    function isMondeSuperpose(NoMonde: Integer): boolean;
    function IsConnexionSurvoleAutreMonde(md1, md2: Integer): Boolean; overload;
    function IsConnexionSurvoleAutreMonde(x1, y1, x2, y2: Integer): Boolean; overload;
    function IsConnexionAngleMultiple45(md1, md2: Integer): Boolean; overload;
    function IsConnexionAngleMultiple45(x1, y1, x2, y2: Integer): Boolean; overload;
    function HasConnexionAngleNotMultiple45(md, x, y: Integer): Boolean;
    function CountConnexionAngleNotMultiple45(md: Integer): Integer; overload;
    function CountConnexionAngleNotMultiple45(md, x, y: Integer): Integer; overload;
    function CoordMondeToNumMonde(X, Y: integer): Integer;
    function CountConnexionSurvoleAutreMonde(md: Integer): Integer; overload;
    function CountConnexionSurvoleAutreMonde(md, x, y: Integer): Integer; overload;

  private
    // Calcul coordonn�es et longueur sur le tore
    function ModuloCoordSurPlan(position, LargeurPlan: Integer): Integer;
    function deltaDist(d1, d2, LargeurPlan: Integer): Integer;
end;

implementation

uses Math;

function TNebDataPlan.ModuloCoordSurPlan(position, LargeurPlan: Integer): Integer;
begin
  Result := position;

  Repeat
    if Result > LargeurPlan then
      dec(Result, LargeurPlan);
    if Result <= 0 then
      inc(Result, LargeurPlan);
  until (Result > 0) and (Result <= LargeurPlan);
end;

function TNebDataPlan.ModuloX(X: Integer): Integer;
begin
  Result := ModuloCoordSurPlan(X, Map.LongX);
end;

function TNebDataPlan.ModuloY(Y: Integer): Integer;
begin
  Result := ModuloCoordSurPlan(Y, Map.LongY);
end;


// retourne la distance entre les deux positions (entre -LargeurPlan/2 et +LargeurPlan/2)
function TNebDataPlan.deltaDist(d1, d2, LargeurPlan: Integer): Integer;
begin
  Result := d2 - d1;

  if Result > LargeurPlan div 2 then
    Result := Result - LargeurPlan;
  if Result < -LargeurPlan div 2 then
    Result := Result + LargeurPlan;
end;

function TNebDataPlan.DeltaX(X1, X2: Integer): integer;
begin
  Result := deltaDist(X1, X2, Map.LongX);
end;

function TNebDataPlan.DeltaY(Y1, Y2: Integer): integer;
begin
  Result := deltaDist(Y1, Y2, Map.LongY);
end;

function TNebDataPlan.HasConnexionAngleNotMultiple45(md, x, y: Integer): Boolean;
var
  c: Integer;
begin
  Result := CountConnexionAngleNotMultiple45(md, x, y) > 0;
end;

function TNebDataPlan.CountConnexionAngleNotMultiple45(md: Integer): Integer;
begin
  Result := 0;
  if (M[md].X > 0) and (M[md].Y > 0) then
    Result := CountConnexionAngleNotMultiple45(md, M[md].X, M[md].Y);

end;

function TNebDataPlan.CountConnexionAngleNotMultiple45(md, x, y: Integer): Integer;
var
  c: Integer;
begin
  Result := 0;
  c := 0;

  While (c < High(M[md].Connect)) and (M[md].Connect[c+1] <> 0) do
  begin
    Inc(c);
    if not IsConnexionAngleMultiple45(X, Y,
      M[M[md].Connect[c]].X, M[M[md].Connect[c]].Y) then
      Inc(Result);
  end;
end;

function TNebDataPlan.CountConnexionSurvoleAutreMonde(md: Integer): Integer;
begin
  Result := 0;
  if (M[md].X > 0) and (M[md].Y > 0) then
    Result := CountConnexionSurvoleAutreMonde(md, M[md].X, M[md].Y);

end;

function TNebDataPlan.CountConnexionSurvoleAutreMonde(md, x, y: Integer): Integer;
var
  c: Integer;
begin
  Result := 0;
  c := 0;

  While (c < High(M[md].Connect)) and (M[md].Connect[c+1] <> 0) do
  begin
    Inc(c);
    if IsConnexionSurvoleAutreMonde(X, Y,
      M[M[md].Connect[c]].X, M[M[md].Connect[c]].Y) then
      Inc(Result);
  end;
end;

function TNebDataPlan.isMondeSuperpose(NoMonde: Integer): boolean;
var
  md: Integer;
begin
  Result := False;
  for md := 1 to NbMonde do
  begin
    if (md <> NoMonde) and (M[md].X <> 0) and (M[md].Y <> 0)
      and (M[md].X = M[NoMonde].X) and (M[md].Y = M[NoMonde].Y) then
      begin
        Result := True;
        Exit;
      end;
  end;

end;

function TNebDataPlan.IsConnexionSurvoleAutreMonde(md1, md2: Integer): Boolean;
var
  dirX, dirY: Integer;
  X, Y: Integer;
  longueur: Integer;
  i: Integer;
begin
  Result := IsConnexionSurvoleAutreMonde(M[md1].X, M[md1].Y, M[md2].X, M[md2].Y);
end;

function TNebDataPlan.IsConnexionSurvoleAutreMonde(x1, y1, x2, y2: Integer): Boolean;
var
  dirX, dirY: Integer;
  X, Y: Integer;
  longueur: Integer;
  i: Integer;
begin
  Result := False;
  if IsConnexionAngleMultiple45(x1, y1, x2, y2) then
  begin
    // On d�termine la direction de la connexion
    dirX := DeltaX(x1, x2);
    dirY := DeltaY(y1, y2);

    longueur := Max(abs(dirX), abs(dirY));

    dirX := Sign(dirX);
    dirY := Sign(dirY);

    for i := 1 to longueur - 1 do
    begin
      X := ModuloX(x1 + i * dirX);
      Y := ModuloY(y1 + i * dirY);
      if CoordMondeToNumMonde(X, Y) > 0 then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
end;

function TNebDataPlan.IsConnexionAngleMultiple45(md1, md2: Integer): Boolean;
var
  dX, dY: Integer;
begin
  Result := IsConnexionAngleMultiple45(M[md1].X, M[md1].Y, M[md2].X, M[md2].Y);
end;

function TNebDataPlan.IsConnexionAngleMultiple45(x1, y1, x2, y2: Integer): Boolean;
var
  dX, dY: Integer;
begin
  Result := True;

  dX := DeltaX(x1, x2);
  dY := DeltaY(y1, y2);

  if (dX <> 0) and (dY <> 0) and (abs(dX) <> abs(dY)) then
      Result := False;
end;

function TNebDataPlan.CoordMondeToNumMonde(X, Y: integer): Integer;
var
  md, NoMonde: Integer;
  xm, ym: Integer;
begin
  Result := 0;
  if (X = 0) or (Y = 0) then
    Exit;

  xm := (x - 1) mod Map.LongX + 1;
  if xm <= 0 then
    xm := xm + Map.LongX;

  ym := (y - 1) mod Map.LongY + 1;
  if ym <= 0 then
    ym := ym + Map.LongX;

  NoMonde := 0;
  if (xm > 0) and (xm > 0) then
    for md := 1 to NbMonde do
      if (M[md].X = xm) and (M[md].Y = ym) then
        NoMonde := md;
  Result := NoMonde;
end;



end.

