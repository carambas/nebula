unit PlaceMondes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.WinXCtrls;

type
  TPlaceMondesForm = class(TForm)
    FermerButton: TButton;
    Message: TMemo;
    procedure FermerButtonClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  PlaceMondesForm: TPlaceMondesForm;

implementation

{$R *.dfm}

procedure TPlaceMondesForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_escape then
    Close
end;

procedure TPlaceMondesForm.FermerButtonClick(Sender: TObject);
begin
  Close;
end;

end.

