object PlaceMondesForm: TPlaceMondesForm
  Left = 0
  Top = 0
  Caption = 'Placement des mondes sur le plan'
  ClientHeight = 353
  ClientWidth = 783
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object FermerButton: TButton
    Left = 680
    Top = 22
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Fermer'
    Default = True
    ModalResult = 2
    TabOrder = 0
    OnClick = FermerButtonClick
  end
  object Message: TMemo
    Left = 32
    Top = 24
    Width = 617
    Height = 305
    TabStop = False
    Lines.Strings = (
      'Message')
    ReadOnly = True
    TabOrder = 1
  end
end
