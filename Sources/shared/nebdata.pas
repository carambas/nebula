{ -------------------------------------------------------------- }
{ }
{ NebData : classe de gestion des donn�es d'une partie de N�bula }
{ }
{ Date de cr�ation : 24/07/98 }
{ }
{ -------------------------------------------------------------- }

unit nebdata;

{$B-}

interface

uses Classes, AvatUnit, NBTStringList, Types, Generics.Collections, ordreslist;

type
  TTypeFicDonnees = (tfd_NONE, tfd_AVD, tfd_NBT, tfd_NEB, tfd_NBA);

  PNebData = ^TNebData;
  PInteger = ^Integer;

  TNebData = class(TPersistent)
  private
    FFileName: String;
    FTour: Integer;
    FNomPartie: string;
    Par: TPartie;
    Par2: TPartie2;
    Evt: TEvt;
    Entete: TEntete;
    FMap: TInfoMap;
    FNoJou: Integer;
    FTypeFicDonnees: TTypeFicDonnees;
    FConfianceVaisseaux: Integer;
    FNomMondes: array [1 .. 1024] of string;
    FOrdresClear: TStringList;
    fLigneOrdres: Integer;
    FLock: boolean;
    FDataHisto: array [0 .. 100] of PNebData;

    // gestion des ordres
    NbOrdres: longint;
    OrdresList: TStringList;
    fOrdres: TListeOrdres;


    procedure LitOrdresDansNBT;
    function GetFileName: String;
    function GetVersion: String;
    procedure SetVersion(Ver: string);
    function GetAdresseSM: String;
    procedure SetAdresseSM(Ch: string);
    function GetPasswordPartie: String;
    procedure SetPasswordPartie(Ch: string);
    function GetNomArbitre: String;
    procedure SetNomArbitre(Ch: string);
    function GetEmailArbitre: String;
    procedure SetEmailArbitre(Ch: string);
    function GetMondes: PTousMonde;
    function GetFlottes: PToutesFlotte;
    function GetTresors: PTousTresor;
    function GetMondesEvt: PTousMondeEvt;
    function GetFlottesEvt: PTousFlotteEvt;
    function GetContact: PRelationJou;
    function GetJou: PLesJoueurs;
    function GetAVuConnect(NoMonde, NoJou: Integer): boolean;
    procedure SetAVuConnect(NoMonde, NoJou: Integer; value: boolean);
    function GetAVuCoord(NoMonde, NoJou: Integer): boolean;
    procedure SetAVuCoord(NoMonde, NoJou: Integer; value: boolean);
    function GetStationMonde(NoMonde, NoJou: Integer): boolean;
    procedure SetStationMonde(NoMonde, NoJou: Integer; value: boolean);
    function GetConnu(NoMonde, NoJou: Integer): boolean;
    procedure SetConnu(NoMonde, NoJou: Integer; value: boolean);
    function GetDetailScores: PDetailScores;
    function GetNbAVuConnect(NoMonde: Integer): Integer;
    function GetNbStationMonde(NoMonde: Integer): Integer;
    function GetNbConnu(NoMonde: Integer): Integer;
    function GetCurJouRadar: Integer;
    procedure SetCurJouRadar(r: Integer);
    function GetPseudo(Joueur: Integer): String;
    function GetCurJouMd: Integer;
    function GetNbPi(NoMonde: Integer): Integer;
    procedure SetNbPi(NoMonde, NbPi: Integer);
    function GetRecupPi(NoMonde: Integer): Integer;
    procedure SetRecupPi(NoMonde, RecupPi: Integer);
    function IsTN(NoMonde: Integer): boolean;
    function IsCapture(NoMonde: Integer): boolean;
    function Iscadeau(NoMonde: Integer): boolean;
    function IsBombe(NoMonde: Integer): boolean;
    function IsPille(NoMonde: Integer): boolean;
    procedure SetTN(NoMonde: Integer; value: boolean);
    procedure SetCapture(NoMonde: Integer; value: boolean);
    procedure SetCadeau(NoMonde: Integer; value: boolean);
    procedure SetBombe(NoMonde: Integer; value: boolean);
    procedure SetPille(NoMonde: Integer; value: boolean);
    function isPaix(NoFlotte: Integer): boolean;
    function isTransporteBombe(NoFlotte: Integer): boolean;
    function isCapturee(NoFlotte: Integer): boolean;
    function isPiratee(NoFlotte: Integer): boolean;
    function isOfferte(NoFlotte: Integer): boolean;
    function isFlotteEmbuscade(NoFlotte: Integer): boolean;
    function isDPC(NoFlotte: Integer): boolean;
    function isFlotteExplo(NoFlotte: Integer): boolean;
    function isTresorSurMonde(NoTres: Integer): boolean;
    function isTresorSurFlotte(NoTres: Integer): boolean;
    procedure SetPaix(NoFlotte: Integer; value: boolean);
    procedure SetTransporteBombe(NoFlotte: Integer; value: boolean);
    procedure SetCapturee(NoFlotte: Integer; value: boolean);
    procedure SetPiratee(NoFlotte: Integer; value: boolean);
    procedure SetOfferte(NoFlotte: Integer; value: boolean);
    procedure SetFlotteEmbuscade(NoFlotte: Integer; value: boolean);
    procedure SetDPC(NoFlotte: Integer; value: boolean);
    procedure SetFlotteExplo(NoFlotte: Integer; value: boolean);
    function GetNomMondes(NoMonde: Integer): string;
    procedure SetNomMondes(NoMonde: Integer; value: string);
    function GetFilesDir: String;
    procedure SetConnuDeNom(j1, j2: Integer; value: boolean);
    function GetConnuDeNom(j1, j2: Integer): boolean;
    function GetHisto(NoTour: Integer): PNebData;
    function GetScoresDecroissants: TStringDynArray;
    procedure SetScoresDecroissants(ScoresDecroissants: TStringDynArray);


    // Gestion des Ordres
    function OpenWOrdres: boolean;
    procedure WriteOrdre(O: TOrdre);
    procedure CloseWOrdres(NoJou: Integer; NomFicNBT: string);
    procedure WriteOrdres(NoJou: Integer; NomFicNBT: string);
    function isAffichageScoreCRUEL: boolean;

  protected
  public
    // Info sur de Nebutil
    Tresors: array [1 .. 1024] of Integer;
    FlotteCarg: array [1 .. 1024] of string;
    VaisseauxPerso: array [1 .. 1024] of Integer;
    VaisseauxAutres: array [1 .. 1024] of Integer;
    FlottesPerso: array [1 .. 1024] of Integer;
    FlottesAutres: array [1 .. 1024] of Integer;
    TourSurListing: array [1 .. 1024] of Integer;
    Localise: array [1 .. 1024] of Integer; // TODO v�rifier l'utilit�, a priori inutilis�
    Confiance: array [0 .. NB_JOU_MAX] of Integer;
    MDepSup: TNBTStringList;
    EvalDEP: Integer;
    VersionSM: Integer; // 2 ou 3
    DateDebut: string;
    // D�but options r�gles des parties
    PiratageMultiClasse: boolean;
    // true = tout le monde peut pirater � 20 contre 1, def=0
    AffichageScore: boolean;
    // true = affiche le score des joueurs en contact, def=1
    BombesInterdites: boolean;
    // true = interdit de construire et de larguer des bombes, def=0
    MEVAPourTous: boolean;
    // true = Tous les joueurs peuvent faire des ordres de MEVA (mais les points uniquement aux explos), def=0
    DiploInterdite: boolean;
    // true = aucun ordre de cadeau ni de d�claration possible (sauf Jihad), def=0
    PillagesInterdits: boolean;
    // true = aucun ordre de Pillage n'est possible, def=0
    TirIndustries: Integer;
    // 0=non, 1=oui, 2=uniquement celles des autres �quipes, def=1
    EmigrationSansHasard: boolean;
    PartieCRUEL: boolean;
    // Fin Info Sup
    ScoresTries, ClassementJouScore: array [1 .. NB_JOU_MAX] of word;  // Utils� dans Nebula pas Nebutil
    Seed: Integer;
    LogEval: TStringList;


    constructor Create(const FileName: String; const NoTour: Integer);
    destructor Destroy; override;
    procedure FreeOrdres;
    procedure AssignOrdres(const lesOrdres: TListeOrdres);
    procedure RempliHisto;
    procedure VideContenu;
    procedure AssignTo(Dest: TPersistent); override;
    procedure AssignMapFromData(Source: TNebData);
    procedure SaveToFile;
    procedure SauveEnNBT(NoJou: Integer; Rep: string; FileName: string = '');
    function LectureFichierNBT(const FileName: string): boolean;
    procedure NouveauTour;
    procedure UpdateTresors;
    procedure UpdateMapCoord;
    function GetMapCoordFileName: string;


    Function Connect(md1, md2: Integer): boolean;
    Procedure DevientConnu(Md, Joueur: Integer);
    Function EstEnPaix(fl: Integer): boolean;
    Function EstConnu(Md, j: Integer): boolean;
    Function EstAllie(j1, j2: Integer): boolean;
    Function EstEnJihad(j1, j2: Integer): boolean;
    Function EstChargeur(j1, j2: Integer): boolean;
    Function EstChargeurDePop(j1, j2: Integer): boolean;
    Function EstDechargeurDePop(j1, j2: Integer): boolean;
    Function EstPilleur(j1, j2: Integer): boolean;
    Function CapaciteFlotte(fl: Integer): Integer;
    Function ContenuFlotte(fl: Integer): Integer;
    procedure UpdateContenuFlotte(fl: Integer);
    procedure DeclareAllie(j1, j2: word);
    procedure DeclareEnnemi(j1, j2: word);
    procedure DeclareChargeur(j1, j2: word);
    procedure DeclareNonChargeur(j1, j2: word);
    procedure DeclarePilleur(j1, j2: word);
    procedure DeclareNonPilleur(j1, j2: word);
    procedure DeclareJihad(j1, j2: word);
    procedure DeclareChargeurPop(j1, j2: word);
    procedure DeclareNonChargeurPop(j1, j2: word);
    procedure DeclareDechargeurPop(j1, j2: word);
    procedure DeclareNonDechargeurPop(j1, j2: word);
    procedure MetEnContact(j1, j2: word);
    Function QqunADejaStationne(Md: Integer): boolean;
    Function EstMd(monde: word): boolean;
    Function EstEnContact(j1, j2: word): boolean;
    function EstCorrompu(NoMonde, NoJou: Integer): boolean;

    function GetTresorMonde(NoTres: Integer): Integer;
    function GetTresorFlotte(NoTres: Integer): Integer;
    function GetNomListing : string;
    function GetNomListingTr(NoTour : integer) : string;
    function GetNomListingJouTr(NoJou, NoTour: integer): string;
    function GetNomFicOrdres: string;
    function GetNomFicNBA(NoTour : integer) : string;
    function GetNomFicNBTTr(NoTour : integer) : string;
    procedure RempliInfoSup;
    procedure RempliInfoSupNBT;
    procedure RempliInfoSupLight(TypePlan: TTypePlan);
    function EstConfianceVaisseaux(NoJou: Integer): boolean;
    procedure SetNomPartie(Nom: string);
    procedure SetMap(var theMap: TInfoMap);
    procedure RenameFile(Name: String);
    function GetNBTFileName(jou: Integer): string;

    procedure SetLock(value: boolean; ATour: Integer);
    function GetLock(ATour: Integer): boolean;
    function EstCoequipier(j1, j2: Integer): boolean;
    procedure ModuloCoordonnees;
    Procedure TriScores;
    procedure SupprimeConnexion(Cnx: TConnexion); overload;
    procedure SupprimeConnexion(md1, md2: Integer); overload;
    procedure SupprimeMonde(md: Integer);


    property FileName: String read GetFileName;
    property FilesDir: String read GetFilesDir;
    property Tour: Integer read FTour write FTour;
    property DernierTourStocke: smallint read Entete.TourMax;
    procedure AjouteConnexion(Cnx: TConnexion; DoubleSens: Boolean);

    // Gestion des ordres
    function OpenROrdres: longint;
    function ReadOrdre: TOrdre;
    procedure CloseROrdres;

    { ---- Debut des propri�t�s Data ----- }

    // Par
    property Version: String read GetVersion write SetVersion;
    property TourFinal: UInt16 read Par.TourFinal write Par.TourFinal;
    property NbJou: UInt16 read Par.NbJou write Par.NbJou;
    property NbMonde: UInt16 read Par.NbMonde write Par.NbMonde;
    property NbFlotte: UInt16 read Par.NbFlotte write Par.NbFlotte;
    property jou: PLesJoueurs read GetJou;
    property M: PTousMonde read GetMondes;
    property F: PToutesFlotte read GetFlottes;
    property T: PTousTresor read GetTresors;
    property ConNom: TRelationJou read Par.ConNom write Par.ConNom;
    property Allie: TRelationJou read Par.Allie write Par.Allie;
    property Chargeur: TRelationJou read Par.Chargeur write Par.Chargeur;
    property Pilleur: TRelationJou read Par.Pilleur write Par.Pilleur;
    property TechClasse: TTechClasse read Par.TechClasse write Par.TechClasse;
    property MaxTech: TMaxTech read Par.MaxTech write Par.MaxTech;
    property AdresseServeurMail: String read GetAdresseSM write SetAdresseSM;
    property PasswordPartie: String read GetPasswordPartie
      write SetPasswordPartie;
    property NomArbitre: string read GetNomArbitre write SetNomArbitre;
    property EmailArbitre: string read GetEmailArbitre write SetEmailArbitre;
    property ChargeurPop: TRelationJou read Par.ChargeurPop
      write Par.ChargeurPop;
    property DechargeurPop: TRelationJou read Par.DechargeurPop
      write Par.DechargeurPop;
    property NbRotCode: smallint read Par.NbRotCode write Par.NbRotCode;
    property TresorSurMonde[NoTres: Integer]: boolean read isTresorSurMonde;
    property TresorSurFlotte[NoTres: Integer]: boolean read isTresorSurFlotte;
    property AffichageScoreCRUEL: boolean read isAffichageScoreCRUEL;

    // Par2
    property AVuConnect[NoMonde, NoJou: Integer]: boolean read GetAVuConnect
      write SetAVuConnect;
    property AVuCoord[NoMonde, NoJou: Integer]: boolean read GetAVuCoord
      write SetAVuCoord;
    property StationMonde[NoMonde, NoJou: Integer]: boolean read GetStationMonde
      write SetStationMonde; // Historique des stations
    property DetailScores: PDetailScores read GetDetailScores;

    // --- Evt
    property Connu[NoMonde, NoJou: Integer]: boolean read GetConnu
      write SetConnu;
    property ME: PTousMondeEvt read GetMondesEvt;
    property FE: PTousFlotteEvt read GetFlottesEvt;
    property Contact: PRelationJou read GetContact;
    property ScoresDecroissants: TStringDynArray read GetScoresDecroissants write SetScoresDecroissants;

    // Map
    property Map: TInfoMap read FMap;

    // Ajouts
    property NbAVuConnect[NoMonde: Integer]: Integer read GetNbAVuConnect;
    property NbStationMonde[NoMonde: Integer]: Integer read GetNbStationMonde;
    property NbConnu[NoMonde: Integer]: Integer read GetNbConnu;
    property NomPartie: string read FNomPartie;
    property Histo[NoTour: Integer]: PNebData read GetHisto;


    // Nebutil
    property NoJou: Integer read FNoJou write FNoJou;
    property Radar: Integer read GetCurJouRadar write SetCurJouRadar;
    property Pseudos[Joueur: Integer]: String read GetPseudo;
    property MDep: Integer read GetCurJouMd;
    property TypeFicDonnees: TTypeFicDonnees read FTypeFicDonnees;
    property Pi[NoMonde: Integer]: Integer read GetNbPi write SetNbPi;
    property RecupPi[NoMonde: Integer]: Integer read GetRecupPi
      write SetRecupPi;
    property TN[NoMonde: Integer]: boolean read IsTN write SetTN;
    property Capture[NoMonde: Integer]: boolean read IsCapture write SetCapture;
    property Cadeau[NoMonde: Integer]: boolean read Iscadeau write SetCadeau;
    property Bombe[NoMonde: Integer]: boolean read IsBombe write SetBombe;
    property Pille[NoMonde: Integer]: boolean read IsPille write SetPille;

    property Paix[NoFlotte: Integer]: boolean read isPaix write SetPaix;
    property TransporteBombe[NoFlotte: Integer]: boolean read isTransporteBombe
      write SetTransporteBombe;
    property Capturee[NoFlotte: Integer]: boolean read isCapturee
      write SetCapturee;
    property Piratee[NoFlotte: Integer]: boolean read isPiratee
      write SetPiratee;
    property Offerte[NoFlotte: Integer]: boolean read isOfferte
      write SetOfferte;
    property FlotteEmbuscade[NoFlotte: Integer]: boolean read isFlotteEmbuscade
      write SetFlotteEmbuscade;
    property DPC[NoFlotte: Integer]: boolean read isDPC write SetDPC;
    property FlotteExplo[NoFlotte: Integer]: boolean read isFlotteExplo
      write SetFlotteExplo;
    property ConfianceVaisseaux: Integer read FConfianceVaisseaux
      write FConfianceVaisseaux;

    property NomMonde[NoMonde: Integer]: string read GetNomMondes
      write SetNomMondes;
    property ConnuDeNom[j1, j2: Integer]: boolean read GetConnuDeNom
      write SetConnuDeNom;

    property Ordres: TListeOrdres read fOrdres;
  end;

implementation

uses
{$IFNDEF NOGUI}
  Windows, Dialogs,
{$ENDIF}
 Character, SysUtils, Utils, IniFiles, Lectordres, Espionnage;


function IsDigit(c: char): Boolean;
begin
  {$IFDEF FPC}
  Result := TCharacter.IsDigit(c);
  {$ELSE}
  Result := c.IsDigit;
  {$ENDIF}
end;

constructor TNebData.Create(const FileName: String; const NoTour: Integer);
var
  i: Integer;
begin
  inherited Create;
  fOrdres := TListeOrdres.Create;
  MDepSup := TNBTStringList.Create;
  FOrdresClear := TStringList.Create;
  fLigneOrdres := 0;

  FillChar(Par, sizeof(Par), 0);
  for i := 0 to NB_JOU_MAX do
    Confiance[i] := 3;
  FillChar(Par2, sizeof(Par2), 0);
  FillChar(Evt, sizeof(Evt), 0);
  FillChar(FMap, sizeof(FMap), 0);
  FFileName := FileName;
  FTour := NoTour;
  FillChar(FlotteCarg, sizeof(FlotteCarg), 0);
  FillChar(Tresors, sizeof(Tresors), 0);
  VersionSM := 0;
  for i := 1 to Length(TourSurListing) do
    TourSurListing[i] := -1;

  for i := 0 to 100 do
    FDataHisto[i] := nil;

  Seed := -1;

  if (FFileName <> '') then
  begin
    if ExtractFileExt(FFileName) = '' then
      FFileName := FFileName + '.nba';
    if FileExists(FFileName) and
      ((UpperCase(ExtractFileExt(FFileName)) = '.NBT') or
      (UpperCase(ExtractFileExt(FFileName)) = '.NBA')) then
      LectureFichierNBT(FFileName)
  end;

  LogEval := TStringList.Create;
end;

procedure TNebData.VideContenu;
var
  i: Integer;
begin
  for i := 1 to NbJou do
  begin
    Par.jou[i].Email.Free;
    Par.jou[i].Equipe.Free;
  end;
  MDepSup.Free;
  FOrdresClear.Free;
  fOrdres.Free;
end;

procedure TNebData.LitOrdresDansNBT;
var
  O: TOrdre;
  i: Integer;
begin
  for i := 1 to OpenROrdres do
  begin
    O := ReadOrdre;
    if O.TypeO >= 0 then
      fOrdres.Add(O);
  end;
  CloseROrdres;
end;

procedure TNebData.FreeOrdres;
begin
  fOrdres.Clear;
end;

destructor TNebData.Destroy;
var
  i: Integer;
begin
  VideContenu;
  for i := Length(FDataHisto) - 1 downto 0 do
  begin
    if Assigned(FDataHisto[i]) then
    begin
      FDataHisto[i].Free;
      FreeMem(FDataHisto[i]);
    end;
  end;
  LogEval.Free;
end;

procedure TNebData.NouveauTour;
var
  TempoEvt: TEvt;
  i: Integer;
begin
  inc(FTour);
  FLock := False;
  TempoEvt := Evt;
  FillChar(Evt, sizeof(Evt), 0);
  for i := 1 to NbMonde do
  begin
    Evt.ME[i].IndLibre := TempoEvt.ME[i].IndLibre;
    Evt.ME[i].PopLibre := Par.M[i].Pop;
    Evt.ME[i].AncienProprio := Par.M[i].Proprio;;
    if Par.M[i].Robot > 0 then
      Evt.ME[i].PopLibre := Par.M[i].Robot;
    DevientConnu(i, Par.M[i].Proprio);
    if Par.M[i].Conv > 0 then
      DevientConnu(i, Par.M[i].ProprConv);
  end;

  for i := 1 to NbFlotte do
  begin
    DevientConnu(Par.F[i].Localisation, Par.F[i].Proprio);
    Evt.FE[i].AncienProprio := Par.F[i].Proprio;
  end;
end;

procedure TNebData.SaveToFile;
begin
  SauveEnNBT(0, '');
end;

function TNebData.GetVersion: String;
begin
  Result := Par.Version;
end;

procedure TNebData.SetVersion(Ver: string);
begin
  Par.Version := Ver;
end;

function TNebData.GetAdresseSM: String;
begin
  Result := Par.AdresseServeurMail;
end;

procedure TNebData.SetAdresseSM(Ch: string);
begin
  Par.AdresseServeurMail := Ch;
end;

function TNebData.GetPasswordPartie: String;
begin
  Result := Par.PasswordPartie;
end;

procedure TNebData.SetPasswordPartie(Ch: string);
begin
  Par.PasswordPartie := Ch;
end;

function TNebData.GetNomArbitre: String;
begin
  Result := Par.NomArbitre;
end;

procedure TNebData.SetNomArbitre(Ch: string);
begin
  Par.NomArbitre := Ch;
end;

function TNebData.GetEmailArbitre: String;
begin
  Result := Par.EmailArbitre;
end;

procedure TNebData.SetEmailArbitre(Ch: string);
begin
  Par.EmailArbitre := Ch;
end;

function TNebData.GetMondes;
begin
  Result := @Par.M;
end;

function TNebData.GetFlottes;
begin
  Result := @Par.F;
end;

function TNebData.GetTresors;
begin
  Result := @Par.T;
end;

function TNebData.GetMondesEvt: PTousMondeEvt;
begin
  Result := @Evt.ME;
end;

function TNebData.GetFlottesEvt: PTousFlotteEvt;
begin
  Result := @Evt.FE;
end;

function TNebData.GetHisto(NoTour: Integer): PNebData;
begin
  if NoTour > Tour then
    Result := nil
  else if NoTour = Tour then
    Result := @Self
  else
  begin
    if not Assigned(FDataHisto[NoTour]) then
      RempliHisto;

    Result := FDataHisto[NoTour];
  end;
end;

function TNebData.GetContact: PRelationJou;
begin
  Result := @Evt.Contact;
end;

function TNebData.GetScoresDecroissants: TStringDynArray;
begin
  Result := Evt.ScoresDecroissants;
end;

procedure  TNebData.SetScoresDecroissants(ScoresDecroissants: TStringDynArray);
begin
  Evt.ScoresDecroissants := ScoresDecroissants;
end;

function TNebData.GetJou: PLesJoueurs;
begin
  Result := @Par.jou;
end;

{ Utilitaires simples d'acc�s � ces donn�es }

Function TNebData.Connect(md1, md2: Integer): boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to 8 do
    if Par.M[md1].Connect[i] = md2 then
      Result := True;
end;

Procedure TNebData.AjouteConnexion(Cnx: TConnexion; DoubleSens: Boolean);

  procedure add(md1, md2: Integer);
  var
    i, j: Integer;
  begin
    // D�j� 8 connexions
    if Par.M[md1].Connect[High(Par.M[md1].Connect)] <> 0 then
      Exit;

    i := 0;
    repeat
      Inc(i);
    until (Par.M[md1].Connect[i] = 0) or ((Par.M[md1].Connect[i] = md2)) or (Par.M[md1].Connect[i] > md2);

    if Par.M[md1].Connect[i] = md2  then
      Exit;

    if Par.M[md1].Connect[i] = 0  then
      Par.M[md1].Connect[i] := md2
    else
    begin
      // D�calage des connexions de 1 vers la droite en commen�ant par la fin
      j := High(Par.M[md1].Connect);
      repeat
        Par.M[md1].Connect[j] := Par.M[md1].Connect[j - 1];
        Dec(j);
      until j <= i;
      Par.M[md1].Connect[i] := md2
    end;
  end;

begin
  add(Cnx.md1, Cnx.md2);

  if DoubleSens then
    add(Cnx.md2, Cnx.md1);
end;

procedure TNebData.SupprimeConnexion(md1, md2: Integer);
var
  Cnx: TConnexion;
begin
  Cnx.md1 := md1;
  Cnx.md2 := md2;
  SupprimeConnexion(Cnx);
end;

procedure TNebData.SupprimeConnexion(Cnx: TConnexion);
  procedure Supprime(md1, md2: Integer);
  var i, j:integer;
  begin
    for i := 1 to High(M[md1].Connect) do
      if M[md1].Connect[i] = md2 then
    begin
      if i < High(M[md1].Connect) then
        for j := i+1 to High(M[md1].Connect) do
          M[md1].Connect[j-1] := M[md1].Connect[j];

      M[md1].Connect[High(M[md1].Connect)] := 0;
    end;
  end;
begin
  Supprime(Cnx.md1, Cnx.md2);
  Supprime(Cnx.md2, Cnx.md1);
end;

// Ne touche pas aux tr�sors et flottes
// N�cessite une harmonisation ensuite.
procedure TNebData.SupprimeMonde(md: Integer);
var
  i, j, mde, fl, tr: Integer;

begin
  // Supprimer les connexions
  while M[md].Connect[1] > 0 do
    SupprimeConnexion(md, M[md].Connect[1]);

  // Renum�roter tous les mondes >= md
  for mde := md + 1 to NbMonde do
  begin
    M[mde - 1] := M[mde];
    ME[mde - 1] := ME[mde];
    FlottesPerso[mde - 1] := FlottesPerso[mde];
    FlottesAutres[mde - 1] := FlottesAutres[mde];
    VaisseauxPerso[mde - 1] := VaisseauxPerso[mde];
    VaisseauxAutres[mde - 1] := VaisseauxAutres[mde];
    RecupPi[mde - 1] := RecupPi[mde];
    TN[mde - 1] := TN[mde];
  end;

  // Connexions
  for mde := 1 to NbMonde do
    for i := 1 to High(M[mde].Connect) do
      if M[mde].Connect[i] > md then
        M[mde].Connect[i] := M[mde].Connect[i] - 1;

  // Md
  for j := 1 to NbJou do
  begin
    if Jou[j].Md = md then
      Jou[j].Md := 0;

    if Jou[j].Md > md then
      Jou[j].Md := Jou[j].Md - 1;
  end;

  // Supprimer flottes et tr�sors + renum localisation
  for fl := 1 to NbFlotte do
  begin
    if F[fl].Localisation = md then
      F[fl].Localisation := 0;

    if F[fl].Localisation > md then
      F[fl].Localisation := F[fl].Localisation - 1;
  end;

  // Supprimer les tr�sors + renum localisation
  for tr := 1 to NbTres do
    if (T[tr].Statut = 0) then
  begin
    if (T[tr].Localisation = md) then
      T[tr].Localisation := 0;

    if (T[tr].Localisation > md) then
      T[tr].Localisation := T[tr].Localisation - 1;
  end;


  FillChar(M[NbMonde], sizeof(M[NbMonde]), 0);
  FillChar(ME[NbMonde], sizeof(ME[NbMonde]), 0);

  NbMonde := NbMonde - 1;
end;

Procedure TNebData.DevientConnu(Md, Joueur: Integer);
begin
  if Md > 0 then
    Evt.Connu[Md] := (Evt.Connu[Md] or Puis2(Joueur - 1));
end;

Function TNebData.EstEnPaix(fl: Integer): boolean;
begin
  if ((Par.F[fl].Statut and Puis2(0))) <> 0 then
    Result := True
  else
    Result := False;
  if Par.F[fl].Proprio = 0 then
    Result := False;
end;

Function TNebData.EstAllie(j1, j2: Integer): boolean;
var
  b: boolean;
begin
  if (j1 = 0) or (j2 = 0) then
    b := False
  else
    b := (Allie[j1] and Puis2(j2 - 1)) <> 0;
  EstAllie := b;
end;

Function TNebData.EstEnJihad(j1, j2: Integer): boolean;
var
  b: boolean;
begin
  if ((j1 = 0) or (j2 = 0)) then
    b := False
  else
    b := (jou[j1].Jihad = j2);
  EstEnJihad := b;
end;

Function TNebData.EstChargeur(j1, j2: Integer): boolean;
var
  b: boolean;
begin
  if ((j1 = 0) or (j2 = 0)) then
    b := False
  else
    b := (Chargeur[j1] and Puis2(j2 - 1)) <> 0;
  EstChargeur := b;
end;

Function TNebData.EstPilleur(j1, j2: Integer): boolean;
var
  b: boolean;
begin
  if ((j1 = 0) or (j2 = 0)) then
    b := False
  else
    b := (Pilleur[j1] and Puis2(j2 - 1)) <> 0;
  Result := b;
end;

Function TNebData.EstChargeurDePop(j1, j2: Integer): boolean;
var
  b: boolean;
begin
  if ((j1 = 0) or (j2 = 0)) then
    b := False
  else
    b := (ChargeurPop[j1] and Puis2(j2 - 1)) <> 0;
  EstChargeurDePop := b;
end;

Function TNebData.EstDechargeurDePop(j1, j2: Integer): boolean;
var
  b: boolean;
begin
  if ((j1 = 0) or (j2 = 0)) then
    b := False
  else
    b := (DechargeurPop[j1] and Puis2(j2 - 1)) <> 0;
  EstDechargeurDePop := b;
end;

function TNebData.GetConnuDeNom(j1, j2: Integer): boolean;
begin
  Result := False;
  if (j1 > 0) and (j1 > 0) then
    Result := (Par.ConNom[j1] and Puis2(j2 - 1)) <> 0;
end;

procedure TNebData.SetConnuDeNom(j1, j2: Integer; value: boolean);
begin
  if (j1 > 0) and (j1 > 0) then
  begin
    if value then
      Par.ConNom[j1] := Par.ConNom[j1] or Puis2(j2 - 1)
    else
      Par.ConNom[j1] := Par.ConNom[j1] and (not Puis2(j2 - 1))
  end;
end;

Function TNebData.CapaciteFlotte(fl: Integer): Integer;
var
  Resultat: Integer;
begin
  Resultat := Par.F[fl].NBVC;
  if Par.F[fl].Proprio > 0 then
    inc(Resultat, Par.F[fl].NbVT * jou[Par.F[fl].Proprio].CAR)
  else
    inc(Resultat, Par.F[fl].NbVT);
  CapaciteFlotte := Resultat;
end;

Function TNebData.ContenuFlotte(fl: Integer): Integer;
begin
  ContenuFlotte := Par.F[fl].MP + Par.F[fl].N + Par.F[fl].C + Par.F[fl].r;
end;

procedure TNebData.UpdateContenuFlotte(fl: Integer);
var
  i, C, T: Integer;
  x: real;
begin
  if ContenuFlotte(fl) > CapaciteFlotte(fl) then
  begin
    for i := CapaciteFlotte(fl) + 1 to ContenuFlotte(fl) do
    begin
      C := ContenuFlotte(fl);
      x := Random;
      T := 0;
      T := T + Par.F[fl].MP;
      if T > (x * C) then
        dec(Par.F[fl].MP)
      else
      begin
        T := T + Par.F[fl].N;
        if T > (x * C) then
          dec(Par.F[fl].N)
        else
        begin
          T := T + Par.F[fl].C;
          if T > (x * C) then
            dec(Par.F[fl].C)
          else
          begin
            dec(Par.F[fl].r);
          end;
        end;
      end;
    end;
  end;
end;

procedure TNebData.UpdateMapCoord;
var
  x, y: Integer;
  md: Integer;
begin
  FillChar(FMap.Mondes, sizeof(FMap.Mondes), 0);
  for md := 1 to NbMonde do if (M[md].X > 0) and (M[md].Y > 0) then

  begin
    FMap.Mondes[M[md].X, M[md].Y].No := md;
    if estMd(md) then
      FMap.Mondes[M[md].X, M[md].Y].Md := True;
    if TN[md] then
      FMap.Mondes[M[md].X, M[md].Y].TN := True;
  end;
end;

procedure TNebData.DeclareAllie(j1, j2: word);
begin
  Par.Allie[j1] := (Par.Allie[j1] or Puis2(j2 - 1));
end;

procedure TNebData.DeclareEnnemi(j1, j2: word);
begin
  Par.Allie[j1] := (Par.Allie[j1] and (not Puis2(j2 - 1)));
end;

procedure TNebData.DeclareChargeur(j1, j2: word);
begin
  Par.Chargeur[j1] := (Par.Chargeur[j1] or Puis2(j2 - 1));
end;

procedure TNebData.DeclarePilleur(j1, j2: word);
begin
  Par.Pilleur[j1] := (Par.Pilleur[j1] or Puis2(j2 - 1));
end;

procedure TNebData.DeclareNonChargeur(j1, j2: word);
begin
  Par.Chargeur[j1] := (Par.Chargeur[j1] and (not Puis2(j2 - 1)));
end;

procedure TNebData.DeclareNonPilleur(j1, j2: word);
begin
  Par.Pilleur[j1] := (Par.Pilleur[j1] and (not Puis2(j2 - 1)));
end;

procedure TNebData.DeclareJihad(j1, j2: word);
begin
  Par.jou[j1].TourChgtJihad := Tour;
  Par.jou[j1].Jihad := j2;
end;

procedure TNebData.DeclareChargeurPop(j1, j2: word);
begin
  Par.ChargeurPop[j1] := (Par.ChargeurPop[j1] or Puis2(j2 - 1));
end;

procedure TNebData.DeclareNonChargeurPop(j1, j2: word);
begin
  Par.ChargeurPop[j1] := (Par.ChargeurPop[j1] and (not Puis2(j2 - 1)));
end;

procedure TNebData.DeclareDechargeurPop(j1, j2: word);
begin
  Par.DechargeurPop[j1] := (Par.DechargeurPop[j1] or Puis2(j2 - 1));
end;

procedure TNebData.DeclareNonDechargeurPop(j1, j2: word);
begin
  Par.DechargeurPop[j1] := (Par.DechargeurPop[j1] and (not Puis2(j2 - 1)));
end;

procedure TNebData.MetEnContact(j1, j2: word);
begin
  if ((j1<>0)and(j2<>0)and(j1<>j2)) then
  begin
    Evt.Contact[j1] := (Evt.Contact[j1] or Puis2(j2 - 1));
    Evt.Contact[j2] := (Evt.Contact[j2] or Puis2(j1 - 1));
  end;
end;

function TNebData.GetAVuConnect(NoMonde, NoJou: Integer): boolean;
var
  i: Integer;
begin
  Result := False;
  if (NoMonde > 0) and (NoJou > 0) then
    for i := 0 to jou[NoJou].Equipe.Count - 1 do
      Result := Result or
        ((Par2.AVuConnect[NoMonde] and Puis2(StrToInt(jou[NoJou].Equipe[i]) -
        1)) <> 0);
end;

procedure TNebData.SetAVuConnect(NoMonde, NoJou: Integer; value: boolean);
begin
  if value then
    Par2.AVuConnect[NoMonde] := Par2.AVuConnect[NoMonde] or Puis2(NoJou - 1)
  else
    Par2.AVuConnect[NoMonde] := Par2.AVuConnect[NoMonde] and
      (not Puis2(NoJou - 1));
end;

function TNebData.GetAVuCoord(NoMonde, NoJou: Integer): boolean;
begin
  Result := (Par2.AVuCoord[NoMonde] and Puis2(NoJou - 1)) <> 0;
end;

procedure TNebData.SetAVuCoord(NoMonde, NoJou: Integer; value: boolean);
begin
  if value then
    Par2.AVuCoord[NoMonde] := Par2.AVuCoord[NoMonde] or Puis2(NoJou - 1)
  else
    Par2.AVuCoord[NoMonde] := Par2.AVuCoord[NoMonde] and (not Puis2(NoJou - 1));
end;

function TNebData.GetStationMonde(NoMonde, NoJou: Integer): boolean;
begin
  Result := (Par2.StationMonde[NoMonde] and Puis2(NoJou - 1)) <> 0;
end;

procedure TNebData.SetStationMonde(NoMonde, NoJou: Integer; value: boolean);
begin
  if value then
    Par2.StationMonde[NoMonde] := Par2.StationMonde[NoMonde] or Puis2(NoJou - 1)
  else
    Par2.StationMonde[NoMonde] := Par2.StationMonde[NoMonde] and
      (not Puis2(NoJou - 1));
end;

function TNebData.GetConnu(NoMonde, NoJou: Integer): boolean;
begin
  if NoJou = 0 then
    Result := True
  else
    Result := EstConnu(NoMonde, NoJou);
end;

Function TNebData.EstConnu(Md, j: Integer): boolean;
{$IFNDEF NEBUTIL}
var
  i: Integer;
{$ENDIF}
begin
  Result := False;
  if (Md > 0) and (j > 0) and (j <> 255) then
{$IFNDEF NEBUTIL}
    for i := 0 to jou[j].Equipe.Count - 1 do
      Result := Result or
        ((Evt.Connu[Md] and Puis2(StrToInt(jou[j].Equipe[i]) - 1)) <> 0);
{$ELSE}
    Result := Result or ((Evt.Connu[Md] and Puis2(j - 1)) <> 0);
{$ENDIF}
end;

procedure TNebData.SetConnu(NoMonde, NoJou: Integer; value: boolean);
begin
  if value then
    Evt.Connu[NoMonde] := Evt.Connu[NoMonde] or Puis2(NoJou - 1)
  else
    Evt.Connu[NoMonde] := Evt.Connu[NoMonde] and (not Puis2(NoJou - 1));
  TourSurListing[NoMonde] := Tour;
end;

function TNebData.GetDetailScores: PDetailScores;
begin
  Result := @Par2.DetailScores;
end;

Function TNebData.QqunADejaStationne(Md: Integer): boolean;
begin
  Result := Par2.StationMonde[Md] <> 0;
end;

function TNebData.GetNbAVuConnect(NoMonde: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to NbJou do
  begin
    if AVuConnect[NoMonde, i] then
      inc(Result);
  end;
end;

function TNebData.GetNbStationMonde(NoMonde: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to NbJou do
  begin
    if StationMonde[NoMonde, i] then
      inc(Result);
  end;
end;

Function TNebData.EstMd(monde: word): boolean;
var
  i: word;
begin
  Result := False;
  for i := 1 to NbJou do
    if jou[i].Md = monde then
      Result := True;
  if MDepSup.IndexOf(IntToStr(monde)) >= 0 then
    Result := True;
end;

Function TNebData.EstEnContact(j1, j2: word): boolean;
var
  b: boolean;
begin
  b := False;
  if (Contact[j1] and Puis2(j2 - 1)) <> 0 then
    b := True;
  EstEnContact := b;
end;

function TNebData.GetNbConnu(NoMonde: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to NbJou do
  begin
    if Connu[NoMonde, i] then
      inc(Result);
  end;
end;

function TNebData.OpenWOrdres: boolean;
begin
  Result := True;
  NbOrdres := 0;
  FOrdresClear.Clear;
  fLigneOrdres := 0;
end;

procedure TNebData.WriteOrdre(O: TOrdre);
var
  TempCh: string;
begin
  TempCh := Format('%d,%d,%d,%d,%s,%s', [O.Auteur, O.Erreur, O.TypeO, O.StypeO,
    TabToStr(O.O), O.DataCh]);
  if O.Erreur > 0 then
    TempCh := TempCh + Format(',%s', [O.OrdreCh]);
  FOrdresClear.Add(CompresseChaine(TempCh));
  NbOrdres := FOrdresClear.Count;
end;

procedure TNebData.CloseWOrdres(NoJou: Integer; NomFicNBT: string);
var
  i: Integer;
  Ini: TMemIniFile;

begin
  Ini := TMemIniFile.Create(NomFicNBT);
  for i := 1 to FOrdresClear.Count do
    Ini.WriteString('ORDRES', IntToStr(i), FOrdresClear[i - 1]);
  Ini.UpdateFile;
  Ini.Free;
end;

procedure TNebData.WriteOrdres(NoJou: Integer; NomFicNBT: string);
var
  i, no: Integer;
begin
  OpenWOrdres;

  for i := 1 to NBTYPEORDRES do
      for no := 0 to fOrdres.Count(i) - 1 do
        if (NoJou = 0) or (fOrdres[i, no].Auteur = NoJou) then
          WriteOrdre(fOrdres[i, no]);

  CloseWOrdres(NoJou, NomFicNBT);

end;

function TNebData.OpenROrdres: longint;
var
  NomFic: string;
  Ini: TMemIniFile;
  i: Integer;
  ordresRaw: TStringList;
  ordreStr: string;
begin
  if Tour = 0 then
  begin
    Result := 0;
    Exit;
  end;

  FOrdresClear.Clear;

  // On cherche les ordres dans le fichier .orc
  NomFic := Format('%s%s%.2d.orc', [GetFilesDir, NomPartie, Tour]);
  if FileExists(NomFic) then
    FOrdresClear.LoadFromFile(NomFic)
  else
  begin
    // Si pas de fichier .orc on cherche les ordres dans le fichier .nba/nbt
    Ini := TMemIniFile.Create(FileName);
    ordresRaw := TStringList.Create;
    Ini.ReadSectionValues('ORDRES', ordresRaw);
    for i := 1 to ordresRaw.Count do
    begin
      ordreStr := ordresRaw[i - 1];
      ordreStr := ordreStr.SubString(ordreStr.IndexOf('=') + 1);

      FOrdresClear.Add(Ini.ReadString('ORDRES', IntToStr(i), ''));
    end;

    ordresRaw.Free;
    Ini.Free;
  end;

  NbOrdres := FOrdresClear.Count;
  Result := NbOrdres;
  fLigneOrdres := 0;
end;

function TNebData.ReadOrdre: TOrdre;
var
  Chaine: string;
  Elts: TNBTStringList;
  O: TOrdre;
  i: Integer;
begin
  InitOrdre(O);

  Chaine := FOrdresClear[fLigneOrdres];
  inc(fLigneOrdres);

  Elts := TNBTStringList.Create;
  Elts.NbElt := 14;
  Elts.CommaText := Chaine;

  O.Auteur := Elts[0].ToInteger;
  O.Erreur := Elts[1].ToInteger;
  O.TypeO := Elts[2].ToInteger;
  O.StypeO := Elts[3].ToInteger;
  for i := 1 to 8 do
    O.O[i] := Elts[3 + i].ToInteger;
  O.DataCh := Elts[12];
  O.OrdreCh := Elts[13];

  Elts.Free;

  Result := O;
end;

procedure TNebData.CloseROrdres;
begin
end;

function TNebData.GetFileName: String;
begin
  Result := FFileName;
end;


function TNebData.GetMapCoordFileName: string;
begin
  Result := format('%s%sMAP.TXT', [ExtractFilePath(FFileName), NomPartie]);
end;

function TNebData.GetCurJouRadar: Integer;
begin
  if FNoJou > 0 then
    Result := jou[FNoJou].RAD
  else
    Result := 2;
end;

procedure TNebData.SetCurJouRadar(r: Integer);
begin
  if FNoJou > 0 then
    jou[FNoJou].RAD := r;
end;

function TNebData.GetNomListing: string;
begin
  Result := Format('%s%.3sR%.2d%.2d.txt', [ExtractFilePath(FileName), NomPartie,
    NoJou, Tour])
end;

function TNebData.GetNomFicOrdres: string;
begin
  Result := Format('%s%sOrdresTour%.2d.txt', [ExtractFilePath(FileName), NomPartie, Tour + 1]);
end;

function TNebData.GetNomListingTr(NoTour: Integer): string;
begin
  Result := Format('%s%.3sR%.2d%.2d.txt', [ExtractFilePath(FileName), NomPartie,
    NoJou, NoTour])
end;

function TNebData.GetNomListingJouTr(NoJou, NoTour: Integer): string;
begin
  Result := Format('%s%.3sR%.2d%.2d.txt', [ExtractFilePath(FileName), NomPartie,
    NoJou, NoTour])
end;

function TNebData.GetNomFicNBA(NoTour: Integer): string;
begin
  // $$$ 07/05/02 : plus de limite de 6 chars
  // Result := Format('%s%.6s%.2d.nba', [ExtractFilePath(FFileName), NomPartie, NoTour]);
  Result := Format('%s%s%.2d.nba', [ExtractFilePath(FFileName),
    NomPartie, NoTour]);
end;

function TNebData.GetNomFicNBTTr(NoTour: Integer): string;
var
  NomFic, NomFicCustom: String;
begin
  NomFic := Format('%s%s%.2d%.2d.nbt', [ExtractFilePath(FFileName), NomPartie,
    NoJou, NoTour]);

  NomFicCustom := Format('%s%s%.2d%.2d_1.nbt', [ExtractFilePath(FFileName), NomPartie,
     NoJou, NoTour]);

  if FileExists(NomFicCustom) then
    Result := NomFicCustom
  else
    Result := NomFic;
end;

function TNebData.LectureFichierNBT(const FileName: string): boolean;
var
  Ini: TMemIniFile;
  SectionsList, List: TStringList;
  DataList: TNBTStringList;
  s: string;
  i, j, k, jou, monde, Flotte, Tresor: Integer;
  DataStr: string;
  sectionNum : integer;
  StatsEspionnage: TStatsEspionnage;

begin
  Result := True;
  if not FileExists(FileName) then
  begin
    ShowMEssage
      (Format('Le fichier %s n''existe pas'#10#13'Peut-�tre qu''une �val arrangerait les choses',
      [FileName]));
    Result := False;
    Exit;
  end;
  FFileName := FileName;
  if UpperCase(ExtractFileExt(FFileName)) = '.NBT' then
    FTypeFicDonnees := tfd_NBT
  else if UpperCase(ExtractFileExt(FFileName)) = '.NBA' then
    FTypeFicDonnees := tfd_NBA;
  FillChar(Tresors, sizeof(Tresors), 0);
  SectionsList := TStringList.Create;
  List := TStringList.Create;
  DataList := TNBTStringList.Create;
  Ini := TMemIniFile.Create(FileName);
  StatsEspionnage := TStatsEspionnage.Create(Self);
  StatsEspionnage.InitStats;

  // Liste des sections. Permet de r�cup�rer la liste
  // des mondes, flottes et tr�sors.
  Ini.ReadSections(SectionsList);

  // GENERAL
  Ini.ReadSectionValues('GENERAL', List);
  FNomPartie := List.Values['NomPartie'];
  FTour := StrToIntDef(List.Values['Tour'], 0);
  TourFinal := StrToIntDef(List.Values['TourFinal'], 0);
  NomArbitre := List.Values['NomArbitre'];
  EmailArbitre := List.Values['EmailArbitre'];
  FNoJou := StrToIntDef(List.Values['NoJoueur'], 0);
  Par.NbMonde := StrToIntDef(List.Values['NbMondes'], 0);
  Par.NbFlotte := StrToIntDef(List.Values['NbFlottes'], 0);
  if Par.NbFlotte = 0 then
    Par.NbFlotte := 1024;
  Par.NbJou := StrToIntDef(List.Values['NbJou'], 0);
  if Par.NbJou = 0 then
    Par.NbJou := 32;
  for i := 1 to NbJou do
  begin
    Par.jou[i].Email.Free;
    Par.jou[i].Equipe.Free;
    Par.jou[i].Email := TNBTStringList.Create;
    Par.jou[i].Equipe := TNBTStringList.Create;
  end;
  FMap.LongX := StrToIntDef(List.Values['TaillePlanX'], 0);
  FMap.LongY := StrToIntDef(List.Values['TaillePlanY'], 0);
  SetAdresseSM(List.Values['AdresseSM']);
  MDepSup.CommaText := List.Values['Mds'];
  ScoresDecroissants := List.Values['Scores'].Split([',']);
  if FNoJou = 0 then
  begin
    Par.PasswordPartie := List.Values['PasswordPartie'];
    Par.NbRotCode := StrToIntDef(List.Values['NbRotCode'], 0);
    DateDebut := List.Values['DateDebut']
  end;
  FLock := boolean(StrToIntDef(List.Values['Lock'], 0));
  VersionSM := StrToIntDef(List.Values['VersionSM'], 3);
  PiratageMultiClasse :=
    boolean(StrToIntDef(List.Values['PiratageMultiClasse'], 0));
  AffichageScore := boolean(StrToIntDef(List.Values['AffichageScore'], 1));
  BombesInterdites := boolean(StrToIntDef(List.Values['BombesInterdites'], 0));
  MEVAPourTous := boolean(StrToIntDef(List.Values['MEVAPourTous'], 0));
  DiploInterdite := boolean(StrToIntDef(List.Values['DiploInterdite'], 0));
  PillagesInterdits :=
    boolean(StrToIntDef(List.Values['PillagesInterdits'], 0));
  TirIndustries := StrToIntDef(List.Values['TirIndustries'], 1);
  EmigrationSansHasard :=
    boolean(StrToIntDef(List.Values['EmigrationSansHasard'], 0));
  PartieCRUEL := boolean(StrToIntDef(List.Values['PartieCRUEL'], 0));

  if NoJou = 0 then
  begin
    for i := 1 to Par.NbJou do
      Confiance[i] := 4;
  end
  else
    Confiance[NoJou] := 0;
  Seed := StrToIntDef(List.Values['Seed'], -1);

  // TECHNO
  Ini.ReadSectionValues('TECHNO', List);

  for i := 1 to 6 do
  begin
    DataList.NbElt := 7;
    DataList.CommaText := List.Values[Format('Max%s', [TechCh[i]])];
    for j := 1 to 7 do
      Par.MaxTech[i, j] := DataList[j - 1].ToInteger;

    DataList.NbElt := 7;
    DataList.CommaText := List.Values[Format('%sClasse', [TechCh[i]])];
    for j := 1 to 7 do
      Par.TechClasse[i, j] := DataList[j - 1].ToInteger;
  end;

  // Le reste
  for sectionNum := 0 to SectionsList.Count - 1 do
  begin
    s := SectionsList[sectionNum];
    if s.StartsWith('M_') then // Mondes
    begin
      Ini.ReadSectionValues(s, List);
      monde := s.SubString(2).ToInteger;

      // Monde M=xxxx
      DataStr := List.Values['M'];
      DataList.NbElt := 24;
      DataList.CommaText := DataStr;

      Par.M[monde].Duree := DataList[0].ToInteger;
      Par.M[monde].ProprConv := DataList[1].ToInteger;
      Par.M[monde].PlusMP := DataList[2].ToInteger;
      Par.M[monde].PC := DataList[3].ToInteger;
      Par.M[monde].Proprio := DataList[4].ToInteger;
      Par.M[monde].Ind := DataList[5].ToInteger;
      Par.M[monde].Pop := DataList[6].ToInteger;
      Par.M[monde].Conv := DataList[7].ToInteger;
      Par.M[monde].Robot := DataList[8].ToInteger;
      Par.M[monde].MaxPop := DataList[9].ToInteger;
      Par.M[monde].MP := DataList[10].ToInteger;
      Par.M[monde].VI := DataList[11].ToInteger;
      Par.M[monde].VP := DataList[12].ToInteger;
      Pi[monde] := DataList[13].ToInteger;
      RecupPi[monde] := DataList[14].ToInteger;
      Evt.ME[monde].AncienProprio := DataList[15].ToInteger;
      Evt.ME[monde].IndLibre := DataList[16].ToInteger;
      Evt.ME[monde].PopLibre := DataList[17].ToInteger;
      Capture[monde] := DataList[18].ToBoolean;
      Cadeau[monde] := DataList[19].ToBoolean;
      TN[monde] := DataList[20].ToBoolean;
      Bombe[monde] := DataList[21].ToBoolean;
      Pille[monde] := DataList[22].ToBoolean;
      if DataList[23].ToBoolean and (NoJou > 0) then
        Par.M[monde].PremierProprio := Par.jou[NoJou].Jihad;

      // TourSurListing[monde] := FTour;

      // ME=xxx
      DataStr := List.Values['ME'];
      DataList.NbElt := 8;
      DataList.CommaText := DataStr;

      Evt.ME[monde].ActionVI := DataList[0].ToInteger;
      Evt.ME[monde].ActionVP := DataList[1].ToInteger;
      Evt.ME[monde].CibleVI := DataList[2].ToInteger;
      Evt.ME[monde].CibleVP := DataList[3].ToInteger;
      Evt.ME[monde].PopMoins := DataList[4].ToInteger;
      Evt.ME[monde].ConvMoins := DataList[5].ToInteger;
      Evt.ME[monde].RobMoins := DataList[6].ToInteger;
      Evt.ME[monde].PC := DataList[7].ToInteger;

      // Coord
      DataStr := List.Values['Coord'];
      if DataStr <> '' then
      begin
        DataList.NbElt := 3;
        DataList.CommaText := DataStr;
        Par.M[monde].x := DataList[0].ToInteger;
        Par.M[monde].Y := DataList[1].ToInteger;
      end;

      // Connect
      DataStr := List.Values['C'];
      if DataStr <> '' then
      begin
        DataList.NbElt := 9;
        DataList.CommaText := DataStr;
        for j := 0 to DataList.Count - 1 do
          if (DataList[j].ToInteger > 0) and (j <= 8) then
            Par.M[monde].Connect[j + 1] := DataList[j].ToInteger;
      end;

      // Explo
      DataStr := List.Values['Explo'];
      if DataStr <> '' then
      begin
        DataList.NbElt := 8;
        DataList.CommaText := DataStr;
        Par.M[monde].NbExplo := DataList[0].ToInteger;
        Par.M[monde].NbExploCM := DataList[1].ToInteger;
        Par.M[monde].NbExploPop := DataList[2].ToInteger;
        Par.M[monde].NbExploLoc := DataList[3].ToInteger;
        Localise[monde] := DataList[4].ToInteger; // TODO v�rifier l'utilit�, a priori inutilis�
        AVuConnect[monde, NoJou] := DataList[4].ToBoolean;
        StationMonde[monde, NoJou] := DataList[5].ToBoolean;
        if NoJou > 0 then
          Connu[monde, NoJou] := DataList[6].ToBoolean;
        Par.M[monde].PotExplo := DataList[7].ToInteger;
      end;

      FNomMondes[monde] := List.Values['Nom'];

      if NoJou = 0 then
      begin
        // Connu
        DataStr := List.Values['Connu'];
        if DataStr <> '' then
        begin
          DataList.NbElt := 0;
          DataList.CommaText := DataStr;
          for j := 0 to DataList.Count - 1 do
            Connu[monde, DataList[j].ToInteger] := True;
        end;
        // AVuConnect
        DataStr := List.Values['AVuConnect'];
        if DataStr <> '' then
        begin
          DataList.NbElt := 0;
          DataList.CommaText := DataStr;
          for j := 0 to DataList.Count - 1 do
            AVuConnect[monde, DataList[j].ToInteger] := True;
        end;
        // AVuCoord
        DataStr := List.Values['AVuCoord'];
        if DataStr <> '' then
        begin
          DataList.NbElt := 0;
          DataList.CommaText := DataStr;
          for j := 0 to DataList.Count - 1 do
            AVuCoord[monde, DataList[j].ToInteger] := True;
        end;
        // StationMonde
        DataStr := List.Values['StationMonde'];
        if DataStr <> '' then
        begin
          DataList.NbElt := 0;
          DataList.CommaText := DataStr;
          for j := 0 to DataList.Count - 1 do
            StationMonde[monde, DataList[j].ToInteger] := True;
        end;
        // Possession
        DataStr := List.Values['Possession'];
        if DataStr <> '' then
        begin
          DataList.NbElt := 0;
          DataList.CommaText := DataStr;
          for j := 0 to DataList.Count - 1 do
            Par.M[monde].Possession[j + 1] := DataList[j].ToInteger;
        end;
        Par.M[monde].PremierProprio := StrToIntDef(List.Values['1erPr'], 0);

      end;
    end;

    // Flottes
    if s.StartsWith('F_') then
    begin
      Ini.ReadSectionValues(s, List);
      Flotte := s.SubString(2).ToInteger;

      // F= Liste de 13 nombres.
      DataStr := List.Values['F'];
      DataList.NbElt := 19;
      DataList.CommaText := DataStr;
      Par.F[Flotte].Localisation := DataList[0].ToInteger;
      Par.F[Flotte].NBVC := DataList[1].ToInteger;
      Par.F[Flotte].NbVT := DataList[2].ToInteger;
      Par.F[Flotte].MP := DataList[3].ToInteger;
      Par.F[Flotte].N := DataList[4].ToInteger;
      Par.F[Flotte].C := DataList[5].ToInteger;
      Par.F[Flotte].r := DataList[6].ToInteger;
      Par.F[Flotte].Proprio := DataList[7].ToInteger;
      Evt.FE[Flotte].AncienProprio := DataList[8].ToInteger;
      Evt.FE[Flotte].OrdrExcl := DataList[9].ToInteger;
      Evt.FE[Flotte].Cible := DataList[10].ToInteger;
      Paix[Flotte] := DataList[11].ToBoolean;
      TransporteBombe[Flotte] := DataList[12].ToBoolean;
      Capturee[Flotte] := DataList[13].ToBoolean;
      Piratee[Flotte] := DataList[14].ToBoolean;
      Offerte[Flotte] := DataList[15].ToBoolean;
      FlotteEmbuscade[Flotte] := DataList[16].ToBoolean;
      DPC[Flotte] := DataList[17].ToBoolean;
      FlotteExplo[Flotte] := DataList[18].ToBoolean;

      // Chemin=
      DataStr := List.Values['Chemin'];
      if DataStr <> '' then
      begin
        DataList.NbElt := 0;
        DataList.CommaText := DataStr;
        for j := 0 to Min(DataList.Count - 1, MAX_CHEMIN - 1) do
          Evt.FE[Flotte].Chemin[j + 1] := DataList[j].ToInteger;
      end;
    end;

    // Tr�sors
    if s.StartsWith('T_') then
    begin
      Ini.ReadSectionValues(s, List);
      Tresor := s.SubString(2).ToInteger;

      // T= Liste de 13 nombres.
      DataStr := List.Values['T'];
      DataList.NbElt := 3;
      DataList.CommaText := DataStr;
      Par.T[Tresor].Proprio := DataList[0].ToInteger;
      if DataList[1] = 'M' then
      begin
        Par.T[Tresor].Localisation := DataList[2].ToInteger;
        Par.T[Tresor].Statut := 0;
        inc(Tresors[Par.T[Tresor].Localisation]);
      end
      else
      begin
        Par.T[Tresor].Localisation := DataList[2].ToInteger;
        Par.T[Tresor].Statut := 1;
        inc(Tresors[Par.F[Par.T[Tresor].Localisation].Localisation]);
      end;
    end;

    // Joueurs
    if s.StartsWith('J_') then
    begin
      Ini.ReadSectionValues(s, List);
      jou := s.SubString(2).ToInteger;

      Par.jou[jou].Pseudo := List.Values['Pseudo'];
      Par.jou[jou].Nom := List.Values['Nom'];
      DataStr := List.Values['Email'];
      DataStr := DataStr.Replace('"', '', [rfReplaceAll]);
      Par.jou[jou].Email.CommaText := DataStr;
      for i := 0 to Par.jou[jou].Email.Count - 1 do
        Par.jou[jou].Email[i] := Trim(Par.jou[jou].Email[i]);
      Par.jou[jou].Equipe.CommaText := List.Values['Equipe'];
      if Par.jou[jou].Equipe.IndexOf(IntToStr(jou)) = -1 then
        Par.jou[jou].Equipe.Add(IntToStr(jou));

      Par.jou[jou].Md := StrToIntDef(List.Values['Md'], 0);
      Par.jou[jou].Score := StrToIntDef(List.Values['Score'], 0);

      // Classement du joueur pour CRUEL
      if TypeFicDonnees = tfd_NBT then
        ClassementJouScore[jou] := StrToIntDef(List.Values['Classement'], 0);

      Par.jou[jou].Coeq := StrToIntDef(List.Values['Coeq'], 0);

      DataStr := List.Values['ConNom'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        ConnuDeNom[jou, DataList[i].ToInteger] := True;

      DataStr := List.Values['Contact'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        MetEnContact(jou, DataList[i].ToInteger());

      DataStr := List.Values['Allies'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        DeclareAllie(jou, DataList[i].ToInteger());

      DataStr := List.Values['Chargeurs'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        DeclareChargeur(jou, DataList[i].ToInteger());

      DataStr := List.Values['ChargeursPop'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        DeclareChargeurPop(jou, DataList[i].ToInteger());

      DataStr := List.Values['DechargeursPop'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        DeclareDechargeurPop(jou, DataList[i].ToInteger());

      DataStr := List.Values['Pilleurs'];
      DataList.NbElt := 0;
      DataList.CommaText := DataStr;
      for i := 0 to DataList.Count - 1 do
        DeclarePilleur(jou, DataList[i].ToInteger());

      DataStr := List.Values['DetailScore'];
      DataList.NbElt := NBSCORE;
      DataList.CommaText := DataStr;
      for i := 1 to DataList.Count do
        DetailScores[jou, i] := StrToIntDef(DataList[i - 1], 0);

      // A appeler imp�rativement apr�s la lecture des mondes, flottes et tr�sors
      StatsEspionnage.LitDansNBx(jou, List);

      // J= Liste de 18 nombres.
      DataStr := List.Values['J'];
      DataList.NbElt := 18;
      DataList.CommaText := DataStr;
      Par.jou[jou].Classe := DataList[0].ToInteger;
      Par.jou[jou].TourChgtJihad := DataList[1].ToInteger;
      Par.jou[jou].Jihad := DataList[2].ToInteger;
      Par.jou[jou].DEP := DataList[3].ToInteger;
      Par.jou[jou].ATT := DataList[4].ToInteger;
      Par.jou[jou].DEF := DataList[5].ToInteger;
      Par.jou[jou].RAD := DataList[6].ToInteger;
      Par.jou[jou].ALI := DataList[7].ToInteger;
      Par.jou[jou].CAR := DataList[8].ToInteger;
      if Par.jou[jou].CAR < 1 then
        Par.jou[jou].CAR := 1;
      Par.jou[jou].DEPReste := DataList[9].ToInteger;
      Par.jou[jou].ATTReste := DataList[10].ToInteger;
      Par.jou[jou].DEFReste := DataList[11].ToInteger;
      Par.jou[jou].RADReste := DataList[12].ToInteger;
      Par.jou[jou].ALIReste := DataList[13].ToInteger;
      Par.jou[jou].CARReste := DataList[14].ToInteger;
      Par.jou[jou].CA := DataList[15].ToInteger;
      Par.jou[jou].CD := DataList[16].ToInteger;
      Par.jou[jou].Score := DataList[17].ToInteger;
    end;
  end;

  StatsEspionnage.FinaliseStats;

//  CloseFile(ficNBT);
  Ini.Free;
  List.Free;
  SectionsList.Free;
  DataList.Free;
  StatsEspionnage.Free;


  // Test sp�cial ouverture de fichier NBA de tour 0 :
  // on calcule la valeur correcte de AVuCoord pour chaque monde
  // Ca corrige une faiblesse de Debpar ainsi que le fait de ne pas savoir � l'avance le niveau
  // de RAD de chaque joueur dans les d�fis.

  if Tour = 0 Then
  begin
    for i := 1 to NbJou do
    begin
      if Par.jou[i].RAD = 1 then
      begin
        for j := 1 to NbMonde do
          if M^[j].Proprio = i then
            AVuCoord[j, i] := True;
      end;

      if Par.jou[i].RAD >= 2 then
      begin
        for j := 1 to NbMonde do
          if EstConnu(j, i) then
          begin
            AVuCoord[j, i] := True;

            { RAD = 3 -> on voit les coord des mondes voisins }
            if (Par.jou[i].RAD >= 3) and (M^[j].Statut and Puis2(6) = 0) then
            begin
              for k := 1 to 8 do
                if M^[j].Connect[k] > 0 then
                  AVuCoord[M^[j].Connect[k], i] := True;
            end;
          end;
      end;
    end;
  end;

  // Ordres
  LitOrdresDansNBT;
end;

function TNebData.GetPseudo(Joueur: Integer): String;
begin
  Result := '';
  if Joueur > 0 then
    Result := Par.jou[Joueur].Pseudo;
end;

function TNebData.GetCurJouMd: Integer;
begin
  Result := 0;
  if FNoJou > 0 then
    Result := Par.jou[FNoJou].Md;
end;

function TNebData.GetTresorMonde(NoTres: Integer): Integer;
begin
  Result := 0;
  if NoTres > 0 then
  begin
    if Par.T[NoTres].Statut = 0 then
      Result := Par.T[NoTres].Localisation
    else
      Result := Par.F[Par.T[NoTres].Localisation].Localisation;
  end;
end;

function TNebData.GetTresorFlotte(NoTres: Integer): Integer;
begin
  Result := 0;
  if NoTres > 0 then
  begin
    if Par.T[NoTres].Statut = 1 then
      Result := Par.T[NoTres].Localisation;
  end;
end;

function TNebData.GetNbPi(NoMonde: Integer): Integer;
begin
  Result := (Par.M[NoMonde].Statut and 56) div 8;
end;

procedure TNebData.SetNbPi(NoMonde, NbPi: Integer);
begin
  Par.M[NoMonde].Statut := (M^[NoMonde].Statut and (not 56)) + NbPi * 8;
end;

function TNebData.GetRecupPi(NoMonde: Integer): Integer;
begin
  Result := Par.M[NoMonde].Statut and 7;
end;

procedure TNebData.SetRecupPi(NoMonde, RecupPi: Integer);
begin
  Par.M[NoMonde].Statut := (M^[NoMonde].Statut and (not 7)) + RecupPi;
end;

procedure TNebData.RempliHisto;
var
  NoTour: Integer;
  NomFicData: string;
  CRData: PNebData;
begin
  for NoTour := Tour - 1 downto 0 do
    if not Assigned(FDataHisto[NoTour]) then
    begin
      if TypeFicDonnees = tfd_NBT then
        NomFicData := GetNomFicNBTTr(NoTour)
      else
        NomFicData := GetNomFicNBA(NoTour);

      if FileExists(NomFicData) then
      begin
        GetMem(CRData, sizeof(TNebData));
        CRData^ := TNebData.Create(NomFicData, NoTour);
        FDataHisto[NoTour] := CRData;
      end;
    end;
end;

function TNebData.isAffichageScoreCRUEL: boolean;
begin
  Result := PartieCRUEL and ((Tour = 10) or (Tour = 15));
end;

// Fonction sp�cifique � Nebutil ou Nebula, jamais utilis� par l'�val
// M�thode � supprimer
procedure TNebData.RempliInfoSup;
var
  fic: TextFile;
  ligne: string;
  Fin: boolean;
  num, oldnum: Integer;
  err: Integer;
  OK: boolean;
  i: Integer;
  i1, i2: Integer;
  tr: Integer;
  PiTrouve: boolean;
begin
  RempliInfoSupNBT;
  Exit;

  // TODO supprimer m�thode RempliInfoSup
  FillChar(Tresors, sizeof(Tresors), 0);
  FillChar(FlotteCarg, sizeof(FlotteCarg), 0);
  FillChar(VaisseauxPerso, sizeof(VaisseauxPerso), 0);
  FillChar(VaisseauxAutres, sizeof(VaisseauxAutres), 0);
  FillChar(FlottesPerso, sizeof(FlottesPerso), 0);
  FillChar(FlottesAutres, sizeof(FlottesAutres), 0);
  for i := 1 to 1024 do
    TourSurListing[i] := -1;
{$IFNDEF NEBULA}
  for i := 1 to NbMonde do
  begin
    if EstAllie(NoJou, M[i].Proprio) then
      inc(VaisseauxPerso[i], M[i].VI + M[i].VP)
    else
      inc(VaisseauxAutres[i], M[i].VI + M[i].VP);
    M[i].Ind := M[i].Ind;
  end;
  for i := 1 to 1024 do
    if F[i].Localisation > 0 then
    begin
      if EstAllie(NoJou, F[i].Proprio) then
      begin
        inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
        inc(FlottesPerso[F[i].Localisation]);
      end
      else
      begin
        inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
        inc(FlottesAutres[F[i].Localisation]);
      end;
    end;
  PiTrouve := False;
  for tr := 0 to FTour do
  begin
    if FileExists(GetNomListingTr(tr)) then
    begin
      Fin := False;
      ligne := '';
      AssignFile(fic, GetNomListingTr(tr));
      Reset(fic);
      num := 0;
      oldnum := 0;
      while (not EOF(fic) and (not Fin)) do
      begin
        Readln(fic, ligne);
        if ligne.Contains(#10) then
        begin
          ShowMEssage('Attention, le fichier ' + GetNomListingTr(tr) + #10#13 +
            'n''est pas utilisable par Nebutil.' + #10#13 + #10#13 +
            'Il est probablement au format unix.' + #10#13 + #10#13 +
            'Certaines informations fournies par Nebutil' + #10#13 +
            'seront erronn�es');
        end;
        if ligne.StartsWith('M_') or ligne.StartsWith('M#') then
        begin
          i := 2;
          while IsDigit(ligne.Chars[i]) do
            inc(i);
          dec(i);
          num := ligne.SubString(2, i - 1).ToInteger();
        end
        else if ligne.StartsWith('Md_') or ligne.StartsWith('Md#') then
        begin
          i := 3;
          while IsDigit(ligne.Chars[i]) do
            inc(i);
          dec(i);
          num := ligne.SubString(3, i - 2).ToInteger();
        end;
        if num <> oldnum then { analyse de la ligne de description du monde }
        begin
          if not PiTrouve and (oldnum > 0) then
            SetRecupPi(oldnum, 0);
          PiTrouve := False;
          TourSurListing[num] := tr;
          oldnum := num;
          M[num].Ind := 0;
          ME[num].IndLibre := 0;
        end;

        { Pillages du monde }
        i1 := ligne.IndexOf('Pi=');
        if (i1 <> -1) and (num <> 0) then
        begin
          PiTrouve := True;
          i2 := i1 + 3;
          while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
            inc(i2);
          if ligne.Chars[i2] = '/' then
          begin
            i1 := i2;
            i2 := i1 + 1;
            while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
              inc(i2);
            SetRecupPi(num, ligne.SubString(i1 + 1, i2 - i1 - 1).ToInteger);
          end
          else
            SetRecupPi(num, 0);
        end;

        if num <> 0 then
        begin
          { Capacit� mini�re du monde }
          i2 := ligne.IndexOf('(+');
          if i2 <> -1 then
          begin
            i1 := i2 + 2;
            while (i1 <= Length(ligne)) and IsDigit(ligne.Chars[i1]) do
              inc(i1);
            M[num].PlusMP := ligne.SubString(i2 + 2, i1 - i2 - 2).ToInteger;
          end
          else
          begin
            if ligne.StartsWith('M_') or ligne.StartsWith('M#') or
              ligne.StartsWith('Md') then
              M[num].PlusMP := 0;
          end;

          { Populations du monde }
          i1 := ligne.IndexOf('P=');
          if (i1 <> -1) and (i1 <> ligne.IndexOf('MP=') + 1) then
          begin
            i2 := i1 + 2;
            while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
              inc(i2);
            M[num].Pop := ligne.SubString(i1 + 2, i2 - i1 - 2).ToInteger;
            if ligne.Chars[i2] = 'C' then
              M[num].Conv := M[num].Pop;

            if ligne.Chars[i2] = 'R' then
            begin
              M[num].Robot := M[num].Pop;
              M[num].Pop := 0;
              M[num].Conv := 0;
            end;

            // On recherche (xxx)
            if ligne[i2] <> '(' then
              inc(i2);
            if ligne[i2] = '(' then
            begin
              i1 := i2;
              i2 := i1 + 1;
              while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
                inc(i2);
              M[num].MaxPop := ligne.SubString(i1 + 1, i2 - i1 - 1).ToInteger();
            end;

            // On recherche /xxC
            while (i2 <= ligne.Length) and
              ((ligne.Chars[i2] <> '/') and (ligne.Chars[i2] <> ' ') and
              (i2 < ligne.Length)) do
              inc(i2);

            if ligne.Chars[i2] = '/' then
            begin
              i1 := i2;
              i2 := i1 + 1;
              while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
                inc(i2);
              if ligne[i2] = 'C' then
                M[num].Conv := ligne.SubString(i1 + 1, i2 - i1 - 1).ToInteger;
            end;
          end;

          { Industries du monde }
          i1 := ligne.IndexOf('I=');
          if i1 > 0 then
          begin
            i2 := i1 + 2;
            while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
              inc(i2);
            if ligne.Chars[i2] = '/' then
            begin
              ME[num].IndLibre := ligne.SubString(i1 + 2, i2 - i1 - 2)
                .ToInteger();
              i1 := i2;
              i2 := i1 + 1;
              while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
                inc(i2);
              M[num].Ind := ligne.SubString(i1 + 1, i2 - i1 - 1).ToInteger();
            end
            else
            begin
              M[num].Ind := ligne.SubString(i1 + 2, i2 - i1 - 2).ToInteger();
              ME[num].IndLibre := M[num].Ind;
            end;
          end;

          if tr = FTour then
          begin
            { MP sur le monde }
            i1 := ligne.IndexOf('MP=');
            if i1 <> -1 then
            begin
              i2 := i1 + 3;
              while (i2 <= ligne.Length) and IsDigit(ligne.Chars[i2]) do
                inc(i2);
              M[num].MP := ligne.SubString(i1 + 3, i2 - i1 - 3).ToInteger();
            end;

            { Compte les tr�sors }
            if ligne.IndexOf('T_') <> -1 then
              inc(Tresors[num])
          end;
        end;
        if ligne.StartsWith('Ordres du joueur') then
        begin
          Fin := True;
        end;
      end;
      CloseFile(fic);
    end;
  end;
{$ELSE} { NEBULA }
  for i := 1 to NbFlotte do
  begin
    if (F[i].Localisation > 0) then
    // $$YeDo 2002/09/17 euh.. ya des barges qui collent des flottes sur le monde z�ro...
    begin
      if F[i].Proprio = M[F[i].Localisation].Proprio then
      begin
        inc(FlottesPerso[F[i].Localisation]);
        inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
      end
      else
      begin
        inc(FlottesAutres[F[i].Localisation]);
        inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
      end;
    end;
  end;

  for i := 1 to NbMonde do
  begin
    TourSurListing[i] := Tour;
    inc(VaisseauxPerso[i], M[i].VI);
    inc(VaisseauxPerso[i], M[i].VP);
  end;

  for i := 1 to NbTres do
    if (T[i].Statut = 0) and (T[i].Localisation <> 0) then
      inc(Tresors[T[i].Localisation])
    else if (T[i].Statut = 1) and (F[T[i].Localisation].Localisation <> 0) then
      inc(Tresors[F[T[i].Localisation].Localisation])

{$ENDIF}
end;

procedure TNebData.RempliInfoSupNBT;
var
  i: Integer;
  tr: Integer;
  tourData: PNebData;
begin
  FillChar(VaisseauxPerso, sizeof(VaisseauxPerso), 0);
  FillChar(VaisseauxAutres, sizeof(VaisseauxAutres), 0);
  FillChar(FlottesPerso, sizeof(FlottesPerso), 0);
  FillChar(FlottesAutres, sizeof(FlottesAutres), 0);

  RempliHisto;
  if NoJou = 0 then
  begin
    for i := 1 to 1024 do
      TourSurListing[i] := Tour;
  end
  else
  begin
    for tr := 0 to Tour do
    begin
      tourData := Histo[tr];
      if Assigned(tourData) then
      begin
        for i := 1 to 1024 do
          if tourData^.Connu[i, NoJou] then
            TourSurListing[i] := tr;
      end;
    end;
  end;

  // Alli�s/ennemis utilis� pour remplir VaisseauxPerso / Autres FlottesPerso / Autres
  // R�cup�ration des coordonn�es sur les tours pr�c�dents pour les extraits de CR
  if NoJou > 0 then
  begin
    // Mondes
    for i := 1 to NbMonde do
    begin
      // Vaisseaux monde
      if EstConfianceVaisseaux(M[i].Proprio) then
        inc(VaisseauxPerso[i], M[i].VI + M[i].VP)
      else
        inc(VaisseauxAutres[i], M[i].VI + M[i].VP);

      //  Coordonn�es monde
      if (not AVuConnect[i, NoJou]) and (TourSurListing[i] <> -1) and (TourSurListing[i] < Tour) then
      begin
        AVuConnect[i, NoJou] := True;
        M[i].Connect := Histo[TourSurListing[i]].M[i].Connect;
      end;
    end;

    // Flottes
    for i := 1 to NbFlotte do
    begin
      if F[i].Localisation > 0 then
      begin
        if EstConfianceVaisseaux(F[i].Proprio) then
        begin
          inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesPerso[F[i].Localisation]);
        end
        else
        begin
          inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesAutres[F[i].Localisation]);
        end;
      end;
    end;
  end
  else
  begin
    for i := 1 to NbMonde do
      inc(VaisseauxPerso[i], M[i].VI + M[i].VP);
    for i := 1 to 1024 do
      if (F[i].Localisation > 0) then
      begin
        if F[i].Proprio = M[F[i].Localisation].Proprio then
        begin
          inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesPerso[F[i].Localisation]);
        end
        else
        begin
          inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesAutres[F[i].Localisation]);
        end;
      end;
  end;
end;

// Sp�cifique � Nebutil et Nebula3, pas pour l'�val
// M�thode � supprimer
procedure TNebData.RempliInfoSupLight(TypePlan: TTypePlan);
var
  fic: TextFile;
  ligne: string;
  Fin: boolean;
  num, oldnum: Integer;
  err: Integer;
  OK: boolean;
  i: Integer;
  i1, i2: Integer;
  tr: Integer;
  PiTrouve: boolean;
begin
  RempliInfoSupNBT;
  Exit;

  // TODO supprimer m�thode RempliInfoSupLight
  FillChar(VaisseauxPerso, sizeof(VaisseauxPerso), 0);
  FillChar(VaisseauxAutres, sizeof(VaisseauxAutres), 0);
  FillChar(FlottesPerso, sizeof(FlottesPerso), 0);
  FillChar(FlottesAutres, sizeof(FlottesAutres), 0);

  if NoJou > 0 then
    for i := 1 to 1024 do
      TourSurListing[i] := -1
  else
    for i := 1 to 1024 do
      TourSurListing[i] := Tour;
  Exit;

{$IFDEF NEBUTIL}
  // Alli�s/ennemis
  if NoJou > 0 then
  begin
    for i := 1 to NbMonde do
    begin
      if EstConfianceVaisseaux(M[i].Proprio) then
        inc(VaisseauxPerso[i], M[i].VI + M[i].VP)
      else
        inc(VaisseauxAutres[i], M[i].VI + M[i].VP);
    end;
    for i := 1 to 1024 do
      if F[i].Localisation > 0 then
      begin
        if EstConfianceVaisseaux(F[i].Proprio) then
        begin
          inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesPerso[F[i].Localisation]);
        end
        else
        begin
          inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesAutres[F[i].Localisation]);
        end;
      end;
  end
  else
  begin
    for i := 1 to NbMonde do
      inc(VaisseauxPerso[i], M[i].VI + M[i].VP);
    for i := 1 to 1024 do
      if (F[i].Localisation > 0) then
      begin
        if F[i].Proprio = M[F[i].Localisation].Proprio then
        begin
          inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesPerso[F[i].Localisation]);
        end
        else
        begin
          inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
          inc(FlottesAutres[F[i].Localisation]);
        end;
      end;
  end;
  PiTrouve := False;
  if TypePlan = PlanAvecCR then
  begin
    for tr := 0 to FTour do
    begin
      if FileExists(GetNomListingTr(tr)) then
      begin
        Fin := False;
        ligne := '';
        AssignFile(fic, GetNomListingTr(tr));
        Reset(fic);
        num := 0;
        oldnum := 0;
        while (not EOF(fic) and (not Fin)) do
        begin
          Readln(fic, ligne);
          if ligne.Contains(#10) then
          begin
            ShowMEssage('Attention, le fichier ' + GetNomListingTr(tr) + #10#13
              + 'n''est pas utilisable par Nebutil.' + #10#13 + #10#13 +
              'Il est probablement au format unix.' + #10#13 + #10#13 +
              'Certaines informations fournies par Nebutil' + #10#13 +
              'seront erronn�es');
          end;

          if ligne.StartsWith('M_') or ligne.StartsWith('M#') then
          begin
            i := 2;
            while ligne.Chars[i].IsDigit do
              inc(i);
            dec(i);
            num := ligne.SubString(2, i - 1).ToInteger();
          end
          else if ligne.StartsWith('Md_') or ligne.StartsWith('Md#') then
          begin
            i := 3;
            while ligne.Chars[i].IsDigit do
              inc(i);
            dec(i);
            num := ligne.SubString(3, i - 2).ToInteger();
          end;

          if num <> oldnum then { analyse de la ligne de description du monde }
          begin
            TourSurListing[num] := tr;
            oldnum := num;
          end;

          // Compter flottes et tr�sors
          // inc(Sup[num].Tresors)
          if ligne.StartsWith('Ordres du joueur') then
          begin
            Fin := True;
          end;
        end;
        CloseFile(fic);
      end;
    end;
  end
  else { TypePlan <> PlanAvecCR }
  begin
    for i := 1 to NbMonde do
      if Connu[i, NoJou] then
        TourSurListing[i] := Tour;
  end;
{$ELSE} { NEBULA }
  for i := 1 to NbFlotte do
  begin
    if F[i].Proprio = M[F[i].Localisation].Proprio then
    begin
      inc(FlottesPerso[F[i].Localisation]);
      inc(VaisseauxPerso[F[i].Localisation], F[i].NBVC + F[i].NbVT);
    end
    else
    begin
      inc(FlottesAutres[F[i].Localisation]);
      inc(VaisseauxAutres[F[i].Localisation], F[i].NBVC + F[i].NbVT);
    end;
  end;

  for i := 1 to NbMonde do
  begin
    TourSurListing[i] := Tour;
    inc(VaisseauxPerso[i], M[i].VI);
    inc(VaisseauxPerso[i], M[i].VP);
  end;

  for i := 1 to NbTres do
    if (T[i].Statut = 0) and (T[i].Localisation <> 0) then
      inc(Tresors[T[i].Localisation])
    else if (T[i].Statut = 1) and (F[T[i].Localisation].Localisation <> 0) then
      inc(Tresors[F[T[i].Localisation].Localisation])

{$ENDIF}
end;

procedure TNebData.AssignMapFromData(Source: TNebData);
begin
  FMap := Source.FMap;
end;

procedure TNebData.AssignOrdres(const lesOrdres: TListeOrdres);
begin
  fOrdres.Clear;
  fOrdres.Assign(lesOrdres);
end;

procedure TNebData.AssignTo(Dest: TPersistent);
var
  NebdataDest: TNebData;
  i: Integer;
  O: TOrdre;
begin
  if Dest is TNebData then
  begin
    NebdataDest := TNebData(Dest);
    NebdataDest.VideContenu;
    // private
    NebdataDest.FLock := FLock;
    NebdataDest.FFileName := FFileName;
    NebdataDest.FTour := FTour;
    NebdataDest.FNomPartie := FNomPartie;
    NebdataDest.Par := Par;
    NebdataDest.Par2 := Par2;
    NebdataDest.Evt := Evt;
    NebdataDest.Entete := Entete;
    NebdataDest.FMap := FMap;
    NebdataDest.FNoJou := FNoJou;
    NebdataDest.FTypeFicDonnees := FTypeFicDonnees;
    NebdataDest.FConfianceVaisseaux := FConfianceVaisseaux;

    NebdataDest.NbOrdres := NbOrdres;
    NebdataDest.NbOrdres := NbOrdres;
    NebdataDest.FNomMondes := FNomMondes;

    NebdataDest.MDepSup := TNBTStringList.Create;
    //MDepSup.Sorted := True;
    NebdataDest.MDepSup.assign(MDepSup);

    NebdataDest.FOrdresClear := TStringList.Create;
    NebdataDest.FOrdresClear.assign(FOrdresClear);

    for i := 1 to NbJou do
    begin
      if Assigned(Par.jou[i].Email) then
      begin
        NebdataDest.Par.jou[i].Email := TNBTStringList.Create;
        NebdataDest.Par.jou[i].Email.assign(Par.jou[i].Email);
      end;
      if Assigned(Par.jou[i].Equipe) then
      begin
        NebdataDest.Par.jou[i].Equipe := TNBTStringList.Create;
        NebdataDest.Par.jou[i].Equipe.assign(Par.jou[i].Equipe);
      end;
    end;

    // public
    NebdataDest.Tresors := Tresors;
    NebdataDest.FlotteCarg := FlotteCarg;
    NebdataDest.VaisseauxPerso := VaisseauxPerso;
    NebdataDest.VaisseauxAutres := VaisseauxAutres;
    NebdataDest.FlottesPerso := FlottesPerso;
    NebdataDest.FlottesAutres := FlottesAutres;
    NebdataDest.TourSurListing := TourSurListing;
    NebdataDest.Localise := Localise; // TODO v�rifier l'utilit�, a priori inutilis�
    NebdataDest.Confiance := Confiance;
    NebdataDest.VersionSM := VersionSM;
    NebdataDest.DateDebut := DateDebut;
    NebdataDest.PiratageMultiClasse := PiratageMultiClasse;
    NebdataDest.AffichageScore := AffichageScore;
    NebdataDest.BombesInterdites := BombesInterdites;
    NebdataDest.MEVAPourTous := MEVAPourTous;
    NebdataDest.DiploInterdite := DiploInterdite;
    NebdataDest.PillagesInterdits := PillagesInterdits;
    NebdataDest.TirIndustries := TirIndustries;
    NebdataDest.EmigrationSansHasard := EmigrationSansHasard;
    NebdataDest.PartieCRUEL := PartieCRUEL;
    NebdataDest.Seed := Seed;
    NebdataDest.MaxTech := MaxTech;
    NebdataDest.TechClasse := TechClasse;
    NebdataDest.ScoresTries := ScoresTries;
    NebdataDest.ClassementJouScore := ClassementJouScore;
    NebdataDest.ScoresDecroissants := ScoresDecroissants;

    NebdataDest.fOrdres := TListeOrdres.Create;
    NebdataDest.fOrdres.Assign(fOrdres);
    NebDataDest.LogEval.Assign(LogEval);

  end
  else
    inherited AssignTo(Dest);
end;

function TNebData.IsTN(NoMonde: Integer): boolean;
begin
  Result := False;
  if NoMonde > 0 then
    Result := ((Par.M[NoMonde].Statut and Puis2(6)) <> 0);
end;

function TNebData.IsCapture(NoMonde: Integer): boolean;
begin
  Result := False;
  if NoMonde > 0 then
    Result := ((Evt.ME[NoMonde].Statut and Puis2(0)) <> 0);
end;

function TNebData.Iscadeau(NoMonde: Integer): boolean;
begin
  Result := False;
  if NoMonde > 0 then
    Result := ((Evt.ME[NoMonde].Statut and Puis2(2)) <> 0);
end;

function TNebData.IsBombe(NoMonde: Integer): boolean;
begin
  Result := False;
  if NoMonde > 0 then
    Result := ((Par.M[NoMonde].Statut and Puis2(7)) <> 0);
end;

function TNebData.IsPille(NoMonde: Integer): boolean;
begin
  Result := False;
  if NoMonde > 0 then
    Result := ((Evt.ME[NoMonde].Statut and Puis2(1)) <> 0);
end;

procedure TNebData.SetTN(NoMonde: Integer; value: boolean);
begin
  if value then
    Par.M[NoMonde].Statut := Par.M[NoMonde].Statut or Puis2(6)
  else
    Par.M[NoMonde].Statut := Par.M[NoMonde].Statut and (not Puis2(6));
end;

procedure TNebData.SetCapture(NoMonde: Integer; value: boolean);
begin
  if value then
    Evt.ME[NoMonde].Statut := Evt.ME[NoMonde].Statut or Puis2(0)
  else
    Evt.ME[NoMonde].Statut := Evt.ME[NoMonde].Statut and (not Puis2(0));
end;

procedure TNebData.SetCadeau(NoMonde: Integer; value: boolean);
begin
  if value then
    Evt.ME[NoMonde].Statut := Evt.ME[NoMonde].Statut or Puis2(2)
  else
    Evt.ME[NoMonde].Statut := Evt.ME[NoMonde].Statut and (not Puis2(2));
end;

procedure TNebData.SetBombe(NoMonde: Integer; value: boolean);
begin
  if value then
    Par.M[NoMonde].Statut := Par.M[NoMonde].Statut or Puis2(7)
  else
    Par.M[NoMonde].Statut := Par.M[NoMonde].Statut and (not Puis2(7));
end;

procedure TNebData.SetPille(NoMonde: Integer; value: boolean);
begin
  if value then
    Evt.ME[NoMonde].Statut := Evt.ME[NoMonde].Statut or Puis2(1)
  else
    Evt.ME[NoMonde].Statut := Evt.ME[NoMonde].Statut and (not Puis2(1));
end;

function TNebData.isPaix(NoFlotte: Integer): boolean;
begin
  Result := ((Par.F[NoFlotte].Statut and Puis2(0)) <> 0);
end;

function TNebData.isTransporteBombe(NoFlotte: Integer): boolean;
begin
  Result := ((Par.F[NoFlotte].Statut and Puis2(7)) <> 0);
end;

function TNebData.isTresorSurFlotte(NoTres: Integer): boolean;
begin
  Result := (Par.T[NoTres].Statut = 1);
end;

function TNebData.isTresorSurMonde(NoTres: Integer): boolean;
begin
  Result := (Par.T[NoTres].Statut = 0);
end;

function TNebData.isCapturee(NoFlotte: Integer): boolean;
begin
  Result := ((Evt.FE[NoFlotte].Statut and Puis2(0)) <> 0);
end;

function TNebData.isPiratee(NoFlotte: Integer): boolean;
begin
  Result := ((Evt.FE[NoFlotte].Statut and Puis2(1)) <> 0);
end;

function TNebData.isOfferte(NoFlotte: Integer): boolean;
begin
  Result := ((Evt.FE[NoFlotte].Statut and Puis2(2)) <> 0);
end;

function TNebData.isFlotteEmbuscade(NoFlotte: Integer): boolean;
begin
  Result := ((Evt.FE[NoFlotte].Statut and Puis2(3)) <> 0);
end;

function TNebData.isDPC(NoFlotte: Integer): boolean;
begin
  Result := ((Evt.FE[NoFlotte].Statut and Puis2(4)) <> 0);
end;

function TNebData.isFlotteExplo(NoFlotte: Integer): boolean;
begin
  Result := ((Evt.FE[NoFlotte].Statut and Puis2(5)) <> 0);
end;

procedure TNebData.SetPaix(NoFlotte: Integer; value: boolean);
begin
  if value then
    Par.F[NoFlotte].Statut := Par.F[NoFlotte].Statut or Puis2(0)
  else
    Par.F[NoFlotte].Statut := Par.F[NoFlotte].Statut and (not Puis2(0));
end;

procedure TNebData.SetTransporteBombe(NoFlotte: Integer; value: boolean);
begin
  if value then
    Par.F[NoFlotte].Statut := Par.F[NoFlotte].Statut or Puis2(7)
  else
    Par.F[NoFlotte].Statut := Par.F[NoFlotte].Statut and (not Puis2(7));
end;

procedure TNebData.SetCapturee(NoFlotte: Integer; value: boolean);
begin
  if value then
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut or Puis2(0)
  else
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut and (not Puis2(0));
end;

procedure TNebData.SetPiratee(NoFlotte: Integer; value: boolean);
begin
  if value then
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut or Puis2(1)
  else
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut and (not Puis2(1));
end;

procedure TNebData.SetOfferte(NoFlotte: Integer; value: boolean);
begin
  if value then
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut or Puis2(2)
  else
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut and (not Puis2(2));
end;

procedure TNebData.SetFlotteEmbuscade(NoFlotte: Integer; value: boolean);
begin
  if value then
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut or Puis2(3)
  else
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut and (not Puis2(3));
end;

procedure TNebData.SetDPC(NoFlotte: Integer; value: boolean);
begin
  if value then
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut or Puis2(4)
  else
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut and (not Puis2(4));
end;

procedure TNebData.SetFlotteExplo(NoFlotte: Integer; value: boolean);
begin
  if value then
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut or Puis2(5)
  else
    Evt.FE[NoFlotte].Statut := Evt.FE[NoFlotte].Statut and (not Puis2(5));
end;

function TNebData.GetFilesDir: String;
begin
  Result := ExtractFilePath(FFileName);
  if Result = '' then
    Result := '.' + DirectorySeparator;
end;

procedure TNebData.SauveEnNBT(NoJou: Integer; Rep: string;
  FileName: string = '');
var
  NomFicNBT: string;
  NomFicNEB: string;
  i, j, k, jou_vu: Integer;
  strStats: string;
  monde: TMonde;
  Flotte: TFlotte;
  Tresor: TTresor;
  MondeASauver: boolean;
  AfficheTout: boolean;
  StrScores: string;
  content: TStringList;

  procedure EcritMondeDansNBT(var monde: TMonde; var content: TStringList;
    NoMonde: Integer);
  var
    ChaineMonde, ChaineEvt: string;
    i: Integer;
    AStationne, EstLocalise: Integer;
    EstCapture, Cadeau, TrouNoir, MondeBombe, EstPille, EstVisible: Integer;
    Corrompu: Integer;
  begin
    ChaineMonde := '';
    ChaineEvt := '';
    with monde do
    begin
      content.Add(Format('[M_%d]', [NoMonde]));

      // Coordonn�es
      if (x > 0) and (Y > 0) then
        content.Add(CompresseChaine(Format('Coord=%d,%d,0', [x, Y])));
      // X,Y,Source

      // Connexions
      if Connect[1] <> 0 then
      begin
        for i := 1 to 8 do
          if Connect[i] <> 0 then
            ChaineMonde := ChaineMonde + IntToStr(Connect[i]) + ','
          else
            break;
        content.Add(CompresseChaine(Format('C=%s0', [ChaineMonde])));
        // source de l'info=0 (N�bula)
      end;

      AStationne := 0;
      if StationMonde[NoMonde, NoJou] then
        AStationne := 1;

      EstLocalise := 0;
      if AVuConnect[NoMonde, NoJou] then
        EstLocalise := 1;

      TrouNoir := 0;
      if Statut and Puis2(6) <> 0 then
        TrouNoir := 1;

      MondeBombe := 0;
      if Statut and Puis2(7) <> 0 then
        MondeBombe := 1;

      if EstCorrompu(NoMonde, NoJou) then
        Corrompu := 1
      else
        Corrompu := 0;

      EstVisible := 0;
      if (NoJou > 0) and Connu[NoMonde, NoJou] then
        EstVisible := 1;

      ChaineMonde := Format('%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d',
        [Duree, ProprConv, PlusMP, PC, Proprio, Ind, Pop, Conv, Robot, MaxPop,
        MP, VI, VP, (Statut and 56) div 8 { Pi } , Statut mod 8 { Recup Pi } ]);

      with ME[NoMonde] do
      begin
        EstCapture := 0;
        if Statut and Puis2(0) <> 0 then
          EstCapture := 1;
        EstPille := 0;
        if Statut and Puis2(1) <> 0 then
          EstPille := 1;
        Cadeau := 0;
        if Statut and Puis2(2) <> 0 then
          Cadeau := 1;

        if Connu[NoMonde, NoJou] or (NoJou = 0) or (TrouNoir = 1) then
        // TrouNoir dans le cas de l'explo qui les d�tecte
        begin
          ChaineMonde := ChaineMonde + Format(',%d,%d,%d,%d,%d,%d,%d,%d,%d',
            [AncienProprio, IndLibre, PopLibre, EstCapture, Cadeau, TrouNoir,
            MondeBombe, EstPille, Corrompu]);
          ChaineEvt := CompresseChaine(Format('%d,%d,%d,%d,%d,%d,%d,%d',
            [ActionVI, ActionVP, CibleVI, CibleVP, PopMoins, ConvMoins,
            RobMoins, PC]));
          if NomMonde[NoMonde] <> '' then
            content.Add(Format('Nom=%s', [NomMonde[NoMonde]]));
        end
        else
          ChaineMonde := ChaineMonde + ',0,0,0,0,0,0,0,0,0';
      end;
      ChaineMonde := CompresseChaine(ChaineMonde);
      if ChaineMonde <> '' then
        content.Add(Format('M=%s', [ChaineMonde]));

      if ChaineEvt <> '' then
        content.Add(Format('ME=%s', [ChaineEvt]));
      ChaineMonde := Format('%d,%d,%d,%d,%d,%d,%d',
        [NbExplo, NbExploCM, NbExploPop, NbExploLoc, EstLocalise, AStationne,
        EstVisible]);
      if Connu[NoMonde, NoJou] or (NoJou = 0) then
        ChaineMonde := ChaineMonde + Format(',%d', [M[NoMonde].PotExplo]);
      ChaineMonde := CompresseChaine(ChaineMonde);

      if ChaineMonde <> '' then
        content.Add(Format('Explo=%s', [ChaineMonde]));

      if NoJou = 0 then
      begin
        content.Add(Format('Connu=%s',
          [CompresseChaine(BitFieldToListStr(Evt.Connu[NoMonde]))]));
        content.Add(Format('AVuConnect=%s',
          [CompresseChaine(BitFieldToListStr(Par2.AVuConnect[NoMonde]))]));
        content.Add(Format('AVuCoord=%s',
          [CompresseChaine(BitFieldToListStr(Par2.AVuCoord[NoMonde]))]));
        content.Add(Format('StationMonde=%s',
          [CompresseChaine(BitFieldToListStr(Par2.StationMonde[NoMonde]))]));
        content.Add(Format('1erPr=%d', [Par.M[NoMonde].PremierProprio]));
        content.Add(Format('Possession=%s',
          [CompresseChaine(TabToStr(Par.M[NoMonde].Possession))]));
      end;
    end;
  end;

  procedure EcritFlotteDansNBT(var Flotte: TFlotte; var content: TStringList;
    NoFlotte: Integer);
  var
    Paix, Bombe, Capturee, Piratee, Offerte, Embuscade, DPC, Explo: Integer;
    i: Integer;
    ChaineChemin: string;
    MdePrecConnu: boolean;
  begin
    with Flotte do
    begin
      Paix := 0;
      if Statut and Puis2(0) <> 0 then
        Paix := 1;
      Bombe := 0;
      if Statut and Puis2(7) <> 0 then
        Bombe := 1;

      Capturee := 0;
      Piratee := 0;
      Offerte := 0;
      Embuscade := 0;
      DPC := 0;
      Explo := 0;

      if FE[NoFlotte].Statut and Puis2(0) <> 0 then
        Capturee := 1;
      if FE[NoFlotte].Statut and Puis2(1) <> 0 then
        Piratee := 1;
      if FE[NoFlotte].Statut and Puis2(2) <> 0 then
        Offerte := 1;
      if FE[NoFlotte].Statut and Puis2(3) <> 0 then
        Embuscade := 1;
      if FE[NoFlotte].Statut and Puis2(4) <> 0 then
        DPC := 1;
      if FE[NoFlotte].Statut and Puis2(5) <> 0 then
        Explo := 1;

      content.Add(Format('[F_%d]', [NoFlotte]));
      content.Add(
        CompresseChaine
        (Format('F=%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d',
        [Localisation, NBVC, NbVT, MP, N, C, r, Proprio,
        FE[NoFlotte].AncienProprio, FE[NoFlotte].OrdrExcl, FE[NoFlotte].Cible,
        Paix, Bombe, Capturee, Piratee, Offerte, Embuscade, DPC, Explo])));

      if { (Proprio = NoJou) and } (FE[NoFlotte].Chemin[1] <> 0) then
      begin
        ChaineChemin := '';
        MdePrecConnu := True;
        for i := 1 to MAX_CHEMIN do
          if (FE[NoFlotte].Chemin[i] <> 0) then
          begin
            if EstConnu(FE[NoFlotte].Chemin[i], NoJou) or
              ((i > 1) and EstConnu(FE[NoFlotte].Chemin[i - 1], NoJou)) or
              (NoJou = 0) then
            begin
              ChaineChemin := ChaineChemin +
                IntToStr(FE[NoFlotte].Chemin[i]) + ',';
              if EstConnu(FE[NoFlotte].Chemin[i], NoJou) or (NoJou = 0) then
                MdePrecConnu := True;
            end
            else
            begin
              if MdePrecConnu then
                ChaineChemin := ChaineChemin + '-1,';
              MdePrecConnu := False;
            end;
          end
          else
            break;
        if ChaineChemin.EndsWith('-1,') then
          ChaineChemin := ChaineChemin.SubString(0,
            ChaineChemin.Length - 3) + ',';
        content.Add(CompresseChaine(Format('Chemin=%s0', [ChaineChemin])));
        // 0 = source de l'info
      end;
    end;
  end;

  procedure EcritTresorDansNBT(var Tresor: TTresor; var content: TStringList;
    NoTresor: Integer);
  var
    FouM: Char;
    i: Integer;
  begin
    with Tresor do
    begin
      content.Add(Format('[T_%d]', [NoTresor]));
      if Statut = 1 then
        FouM := 'F'
      else
        FouM := 'M';
      content.Add(CompresseChaine(Format('T=%d,%s,%d', [Proprio, FouM,
        Localisation])));
      content.Add(Format('Nom=%s', [TresNom(NoTresor)]));
    end;
  end;

var
  buf: array [1 .. 100] of Char;
  tempStr: string;
  alienATT, alienDEF: Integer;
  alienCA, alienCD: Integer;
  StatsEspionnage: TStatsEspionnage;

begin
  if Rep = '' then
    Rep := GetFilesDir;
  if not Rep.EndsWith(DirectorySeparator) then
    Rep := Rep + DirectorySeparator;
  if NoJou = 0 then
    AfficheTout := True
  else
    AfficheTout := False;
{$IFDEF NEBUTIL} // Pas moyen de le virer
  AfficheTout := True;
{$ENDIF}
  NomFicNBT := Format('%s%s', [Rep, FileName]);

  if NoJou > 0 then
  begin
{$IFDEF NEBUTIL} // Pas moyen de le virer
    if FileName = '' then
      NomFicNBT := Format('%s%s%.2d%.2d_1.nbt', [Rep, NomPartie, NoJou, Tour]);
{$ELSE}
    if FileName = '' then
      NomFicNBT := Format('%s%s%.2d%.2d.nbt', [Rep, NomPartie, NoJou, Tour]);
{$ENDIF}
    if FTypeFicDonnees = tfd_NBT then
      FFileName := NomFicNBT;
  end
  else
  begin
    if FileName = '' then
      NomFicNBT := Format('%s%s%.2d.nba', [Rep, NomPartie, Tour]);

    if FTypeFicDonnees = tfd_NONE then
      FTypeFicDonnees := tfd_NBA;
    FFileName := NomFicNBT;
  end;

  StatsEspionnage := TStatsEspionnage.Create(Self);

  if not DirectoryExists(Rep) then
    CreateDir(Rep);

//  AssignFile(fic, NomFicNBT, 65001); // UTF-8
//  Rewrite(fic);
  content := TStringList.Create;

  content.Add('[GENERAL]');
  content.Add('NomPartie=' + NomPartie);
  content.Add('Tour=' + IntToStr(Tour));
  if NoJou = 0 then
    content.Add('TourFinal=' + IntToStr(TourFinal));
  content.Add('NomArbitre=' + NomArbitre);
  content.Add('EmailArbitre=' + EmailArbitre);
  if NoJou = 0 then
  begin
    content.Add('PasswordPartie=' + PasswordPartie);
    content.Add('NbRotCode=' + IntToStr(NbRotCode));
    content.Add('DateDebut=' + DateDebut);
  end;

  if NoJou > 0 then
    content.Add('NoJoueur=' + IntToStr(NoJou));

  content.Add('NbMondes=' + IntToStr(NbMonde));
  content.Add('NbFlottes=' + IntToStr(NbFlotte));
  content.Add('NbJou=' + IntToStr(NbJou));
  content.Add('TaillePlanX=' + IntToStr(Map.LongX));
  content.Add('TaillePlanY=' + IntToStr(Map.LongY));

  content.Add('AdresseSM=' + AdresseServeurMail);
  content.Add('Mds=' + MDepSup.CommaText);

  if AffichageScore or AffichageScoreCRUEL then
  begin
    StrScores := '';
    if (TypeFicDonnees = tfd_NBA) and (NoJou > 0) then
    begin
      TriScores;

      // Affichage score classique : on regarde les joueurs en contact
      if AffichageScore then
      begin
        for i := 1 to NbJou do
          if EstEnContact(NoJou, ScoresTries[i]) then
          begin
            if StrScores <> '' then
              StrScores := StrScores + ',';

            StrScores := StrScores + IntToStr(jou[ScoresTries[i]].Score);
          end;
      end

      // CRUEL : on ne garde que le score du premier
      else if AffichageScoreCRUEL then
      begin
        StrScores := IntToStr(jou[ScoresTries[1]].Score);
      end;

    end;

    if (TypeFicDonnees = tfd_NBT) then
      StrScores := StrScores.Join(',', ScoresDecroissants);

    if StrScores <> '' then
      content.Add('Scores=' + StrScores);
  end;

  if NoJou = 0 then
  begin
    content.Add('Lock=' + IntToStr(Integer(FLock)));
  end;
  content.Add('PiratageMultiClasse=' + IntToStr(Integer(PiratageMultiClasse)));
  content.Add('AffichageScore=' + IntToStr(Integer(AffichageScore)));
  content.Add('BombesInterdites=' + IntToStr(Integer(BombesInterdites)));
  content.Add('MEVAPourTous=' + IntToStr(Integer(MEVAPourTous)));
  content.Add('DiploInterdite=' + IntToStr(Integer(DiploInterdite)));
  content.Add('PillagesInterdits=' + IntToStr(Integer(PillagesInterdits)));
  content.Add('TirIndustries=' + IntToStr(Integer(TirIndustries)));
  content.Add('EmigrationSansHasard=' +
    IntToStr(Integer(EmigrationSansHasard)));
  content.Add('PartieCRUEL=' + IntToStr(Integer(PartieCRUEL)));

  if (Seed >= 0) then
    content.Add('Seed=' + IntToStr(Seed));

  content.Add('');
  content.Add('[TECHNO]');
  for i := 1 to 6 do
    content.Add(Format('Max%s=', [TechCh[i]]) + TabByteToStr(MaxTech[i]));
  for i := 1 to 6 do
    content.Add(Format('%sClasse=', [TechCh[i]]) +
      TabByteToStr(TechClasse[i]));

  content.Add('');
  for i := 1 to NbJou do
  begin
    if ConnuDeNom[NoJou, i] or (NoJou = 0) or (NoJou = i) then
    begin
      // *** Joueurs ***
      alienATT := 0;
      alienDEF := 0;
      alienCA := 0;
      alienCD := 0;

      content.Add(Format('[J_%d]', [i]));
      content.Add(Format('Pseudo=%s', [jou[i].Pseudo]));
      if (NoJou = 0) or (i = NoJou) or
        (jou[NoJou].Equipe.IndexOf(IntToStr(i)) > -1) then
      begin
        content.Add(Format('Nom=%s', [jou[i].Nom]));
        content.Add(Format('Email=%s', [jou[i].Email.CommaText]));
        if Assigned(jou[i].Equipe) then
          content.Add(Format('Equipe=%s', [jou[i].Equipe.CommaText]));
        content.Add(Format('Md=%d', [jou[i].Md]));
        content.Add(Format('Score=%d', [jou[i].Score]));
        content.Add(
          CompresseChaine
          (Format('J=%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d',
          [jou[i].Classe, jou[i].TourChgtJihad, jou[i].Jihad, jou[i].DEP,
          jou[i].ATT, jou[i].DEF, jou[i].RAD, jou[i].ALI, jou[i].CAR,
          jou[i].DEPReste, jou[i].ATTReste, jou[i].DEFReste, jou[i].RADReste,
          jou[i].ALIReste, jou[i].CARReste, jou[i].CA, jou[i].CD,
          jou[i].Score])));
        content.Add('ConNom=' + BitFieldToListStr(ConNom[i]));
        content.Add('Contact=' + BitFieldToListStr(Contact[i]));
        content.Add('Allies=' + BitFieldToListStr(Allie[i]));
        content.Add('Chargeurs=' + BitFieldToListStr(Chargeur[i]));
        content.Add('ChargeursPop=' + BitFieldToListStr(ChargeurPop[i]));
        content.Add('DechargeursPop=' + BitFieldToListStr(DechargeurPop[i]));
        content.Add('Pilleurs=' + BitFieldToListStr(Pilleur[i]));
        content.Add('DetailScore=' + CompresseChaine
          (TabToStr(DetailScores[i])));

        StatsEspionnage.SauveDansNBx(i, content);

        // parties CRUEL aux tours 10 et 15
        if (NoJou > 0) and AffichageScoreCRUEL then
          content.Add('Classement=' + IntToStr(ClassementJouScore[i]));
      end
      else
      begin
        // DEF et CD du joueur "alien"
        if (NoJou > 0) and (jou[NoJou].ALI >= 2) and
          ((jou[i].ALI < 3) or (jou[NoJou].ALI >= jou[i].ALI)) then
        begin
          alienDEF := jou[i].DEF;
          alienCD := jou[i].CD;
        end;

        // ATT et CA du joueur "alien"
        if (NoJou > 0) and (jou[NoJou].ALI >= 3) and
          ((jou[i].ALI < 3) or (jou[NoJou].ALI >= jou[i].ALI)) then
        begin
          alienATT := jou[i].ATT;
          alienCA := jou[i].CA;
        end;

        if (alienATT > 0) or (alienDEF > 0) or (alienCD > 0) or (alienCA > 0)
        then
          content.Add(
            CompresseChaine
            (Format('J=%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d',
            [0, 0, 0, 0, alienATT, alienDEF, 0, 0, 0, 0, 0, 0, 0, 0, 0, alienCA,
            alienCD, 0])));
      end;
    end;
  end;

  // Mondes
  content.Add('');
  for i := 1 to NbMonde do
  begin
    MondeASauver := False;

    if NoJou > 0 then
    begin
      FillChar(monde, sizeof(monde), 0);
      // Vu
      if Connu[i, NoJou] or (AfficheTout and (M[i].x > 0)) then
      begin
        monde := M[i];
        FillChar(monde.Connect, sizeof(monde.Connect), 0);
        monde.x := 0;
        monde.Y := 0;

        MondeASauver := True;
      end;

      // Connexions
      begin
        if AVuConnect[i, NoJou] or (AfficheTout and (M[i].x > 0)) then
        begin
          if not IsTN(i) then
            monde.Connect := M[i].Connect
          else
            monde.Statut := monde.Statut or Puis2(6);
          MondeASauver := True;
        end
        else // on rempli juste les connexions connues
        begin
          k := 0;
          for j := 1 to NbMonde do
            if (Connect(i, j) and AVuConnect[j, NoJou] and not IsTN(j)) then
            begin
              inc(k);
              monde.Connect[k] := j;
              MondeASauver := True;
            end;
        end;
      end;

      // Si TN et joueur explo a vu un des mondes connect�s -> on indique le TN
      if (jou[NoJou].Classe = Explorateur) and IsTN(i) then
        for j := 1 to 8 do
          if AVuConnect[M[i].Connect[j], NoJou] then
            monde.Statut := monde.Statut or Puis2(6);

      // Coordonn�es
      if (AVuCoord[i, NoJou] or (AfficheTout and (M[i].x > 0))
        or ((TypeFicDonnees = tfd_NBT) and (M[i].x > 0) and (M[i].y > 0))) then
      begin
        monde.x := M[i].x;
        monde.Y := M[i].Y;
        if jou[NoJou].Classe = Explorateur then
          monde.Statut := monde.Statut or (M[i].Statut and Puis2(6));
        MondeASauver := True;
      end;
    end;
    if NoJou = 0 then
    begin
      MondeASauver := True;
      monde := M[i];
    end;
    if MondeASauver then
      EcritMondeDansNBT(monde, content, i);
  end;

  // Flottes

  content.Add('');
  if NoJou > 0 then
  begin
    for i := 1 to NbFlotte do
    begin
      if (F[i].Proprio = NoJou) or Connu[F[i].Localisation, NoJou] then
      begin
        Flotte := F[i];
        if (Flotte.Proprio <> NoJou) and
          (jou[NoJou].Equipe.IndexOf(IntToStr(Flotte.Proprio)) = -1) then
        begin
          Flotte.MP := 0;
          Flotte.N := 0;
          Flotte.C := 0;
          Flotte.r := 0;
          if jou[NoJou].ALI < 1 then
          begin
            Flotte.NBVC := Flotte.NBVC + Flotte.NbVT;
            Flotte.NbVT := 0;
          end;
        end;
        EcritFlotteDansNBT(Flotte, content, i);
      end
      else
      begin
        for j := 1 to MAX_CHEMIN do
          if (FE[i].Chemin[j] > 0) and Connu[FE[i].Chemin[j], NoJou] then
          begin
            FillChar(Flotte, sizeof(Flotte), 0);
            EcritFlotteDansNBT(Flotte, content, i);
            break;
          end;
      end;

    end;
  end
  else
  begin
    for i := 1 to NbFlotte do
    begin
      Flotte := F[i];
      EcritFlotteDansNBT(Flotte, content, i);
    end;
  end;

  // Tr�sors;

  content.Add('');
  for i := 1 to NbTres do
    if T[i].Localisation > 0 then
    begin
      if NoJou > 0 then
      begin
        if T[i].Statut = 0 then // Sur un monde
        begin
          if Connu[T[i].Localisation, NoJou] then
          begin
            Tresor := T[i];
            EcritTresorDansNBT(Tresor, content, i);
          end;
        end
        else
        begin
          if Connu[F[T[i].Localisation].Localisation, NoJou] then
          begin
            Tresor := T[i];
            EcritTresorDansNBT(Tresor, content, i);
          end;
        end;
      end
      else
      begin
        Tresor := T[i];
        EcritTresorDansNBT(Tresor, content, i);
      end;
    end;

//  CloseFile(fic);
  content.SaveToFile(NomFicNBT);
  content.Free;
  StatsEspionnage.Free;

  WriteOrdres(NoJou, NomFicNBT);


end;

function TNebData.EstConfianceVaisseaux(NoJou: Integer): boolean;
begin
  Result := Confiance[NoJou] <= FConfianceVaisseaux;
end;

function TNebData.GetNomMondes(NoMonde: Integer): string;
begin
  Result := FNomMondes[NoMonde];
end;

procedure TNebData.SetNomMondes(NoMonde: Integer; value: string);
begin
  FNomMondes[NoMonde] := value;
end;

procedure TNebData.SetNomPartie(Nom: string);
begin
  FNomPartie := Nom;
end;

procedure TNebData.SetMap(var theMap: TInfoMap);
begin
  FMap := theMap;
end;

function TNebData.EstCorrompu(NoMonde, NoJou: Integer): boolean;
begin
  Result := False;
  if NoJou > NbJou then
    Exit;
  if (NoMonde > 0) and (NoJou > 0) and (jou[NoJou].Classe = Missionnaire) and
    (jou[NoJou].Jihad > 0) then
  begin
    if (M[NoMonde].PremierProprio = jou[NoJou].Jihad) or
      (M[NoMonde].Possession[jou[NoJou].Jihad] >= 5) or
      (M[NoMonde].Proprio = jou[NoJou].Jihad) then
      Result := True;
  end;
end;

procedure TNebData.RenameFile(Name: String);
begin
  FFileName := Name;
end;

function TNebData.GetNBTFileName(jou: Integer): string;
begin
  Result := Format('%s%s%.2d%.2d.NBT', [ExtractFilePath(FFileName), NomPartie,
    jou, Tour])
end;

procedure TNebData.SetLock(value: boolean; ATour: Integer);
var
  Ini: TMemIniFile;
begin
  if ATour = FTour then
  begin
    FLock := value;
    Ini := Nil; // pour supprimer un warning de compilation
    try
      Ini := TMemIniFile.Create(FFileName);
{$IFDEF FPC}
      Ini.CacheUpdates := False;
{$ELSE}
      Ini.AutoSave := True;
{$ENDIF}
      Ini.WriteBool('GENERAL', 'Lock', value);
    finally
      if Assigned(Ini) then
        Ini.Free;
    end;
  end;
end;

function TNebData.GetLock(ATour: Integer): boolean;
begin
  Result := FLock;
  if ATour <> FTour then
    Result := False;
end;

procedure TNebData.UpdateTresors;
var
  i: Integer;
begin
  FillChar(Tresors, sizeof(Tresors), 0);
  for i := 1 to NbTres do
    if GetTresorMonde(i) > 0 then
      inc(Tresors[GetTresorMonde(i)]);
end;

function TNebData.EstCoequipier(j1, j2: Integer): boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to jou[j1].Equipe.Count - 1 do
    if StrToInt(jou[j1].Equipe[i]) = j2 then
      Result := True;
end;

procedure TNebData.ModuloCoordonnees;
var
  i: Integer;
begin
  for i := 1 to NbMonde do
  begin
    if (M[i].x > 0) and (M[i].Y > 0) then
    begin
      M[i].x := M[i].x mod Map.LongX + 1;
      M[i].Y := M[i].Y mod Map.LongY + 1;
    end;
  end;
end;

// TODO : � faire fonctionner dans le cas d'un NBT d'une partie CRUEL o� on n'a que le score du premier et son classement dans le .nbt
// => � apperler dans eval.MiseAuPoint
// => ClassementJouScore sauv� dans .nba compl�tement et .nbt partiellement
// Imprime n'appelle plus TriScores
procedure TNebData.TriScores;
var
  i, j, s: word;
begin
  // Cette m�thode est con�ue pour n^'�tre appel�e que dans un contexte de fichier .nba
  if TypeFicDonnees <> tfd_NBA then
    Exit;

  for i := 1 to NbJou do
    ScoresTries[i] := i;
  for i := 1 to NbJou do
    ClassementJouScore[i] := i;

  // Fait le classement
  // ScoresTries[n] = Score du ni�me
  for i := 1 to NbJou - 1 do
    for j := 1 to NbJou - 1 do
      if jou[ScoresTries[j]].Score < jou[ScoresTries[j + 1]].Score then
      begin
        s := ScoresTries[j + 1];
        ScoresTries[j + 1] := ScoresTries[j];
        ScoresTries[j] := s;
      end;

  // D�termine la mlace de chaque joueur
  // ClassementJouScore[i] = place au score du joueur i
  // TODO ceci ne fonctionne que pour un fic NBA. Pas pour un fic NBT
  for i := 1 to NbJou do
    ClassementJouScore[ScoresTries[i]] := i;
end;

end.
