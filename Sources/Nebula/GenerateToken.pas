unit GenerateToken;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, NebJetons;

type
  TJetonForm = class(TForm)
    Label1: TLabel;
    DemandeEdit: TMemo;
    Label2: TLabel;
    JetonEdit: TEdit;
    Button2: TButton;
    DemandeDecodeeEdit: TMemo;
    Label3: TLabel;
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  JetonForm: TJetonForm;

implementation

{$R *.DFM}

procedure TJetonForm.Button2Click(Sender: TObject);
begin
  DemandeDecodeeEdit.Text := DecodeSignature(DemandeEdit.Text);
  JetonEdit.Text := MakeToken(DemandeDecodeeEdit.Text);
end;

end.
