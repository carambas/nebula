program nebula4;

uses
  FastMM5 in '..\ext-compo\FastMM5.pas',
  Forms,
  paraform in 'paraform.pas' {Param},
  ordreserreurs in 'ordreserreurs.pas' {ErreursOrdresForm},
  ORDRREND in 'ORDRREND.PAS' {RendusDialog},
  modif in 'modif.pas' {Dialog},
  about in 'about.pas' {APropos},
  listppap in 'listppap.pas' {ListingToClipboard},
  stat in 'stat.pas' {StatForm},
  selecjou in 'selecjou.pas' {SelectJouDlg},
  calcstat in 'calcstat.pas',
  ordrjou in 'ordrjou.pas' {SaisieOrdresJouForm},
  visu in 'visu.pas' {VisuTexte},
  saisordr in 'saisordr.pas' {SaisieOrdresForm},
  configreseau in 'configreseau.pas' {ConfigReseauForm},
  nebmain in 'nebmain.pas' {NebForm},
  avatunit in '..\shared\avatunit.pas',
  winplan in '..\shared\plan\winplan.pas',
  plan in '..\shared\plan\plan.pas' {PlanForm},
  prtpform in '..\shared\prtform\prtpform.pas' {PrintPlanDlg},
  lectordres in '..\shared\ordres\lectordres.pas',
  impmonde in '..\shared\plan\impmonde.pas' {ImpMondeForm},
  chemin in '..\shared\chemin.pas',
  nebdata in '..\shared\nebdata.pas',
  histo in '..\shared\plan\histo.pas' {HistoForm},
  listint in '..\shared\listint.pas',
  imprime in '..\shared\imprime.pas',
  nbtstringlist in '..\shared\nbtstringlist.pas',
  genmail in '..\shared\genmail.pas',
  envoimail in '..\shared\envoimail.pas',
  utils in '..\shared\utils.pas',
  ordreslist in '..\shared\ordres\ordreslist.pas',
  erreurseval in '..\shared\erreurseval.pas',
  avateval in '..\shared\eval\avateval.pas',
  eval.actionchose in '..\shared\eval\eval.actionchose.pas',
  eval.apipoints in '..\shared\eval\eval.apipoints.pas',
  eval.cadeau in '..\shared\eval\eval.cadeau.pas',
  eval.capture in '..\shared\eval\eval.capture.pas',
  eval.chargemp in '..\shared\eval\eval.chargemp.pas',
  eval.chargetresorexplo in '..\shared\eval\eval.chargetresorexplo.pas',
  eval.combat in '..\shared\eval\eval.combat.pas',
  eval.construction in '..\shared\eval\eval.construction.pas',
  eval.constructionbombe in '..\shared\eval\eval.constructionbombe.pas',
  eval.constructionind in '..\shared\eval\eval.constructionind.pas',
  eval.conversion in '..\shared\eval\eval.conversion.pas',
  eval.dechargemp in '..\shared\eval\eval.dechargemp.pas',
  eval.dechargepc in '..\shared\eval\eval.dechargepc.pas',
  eval.dechargetresor in '..\shared\eval\eval.dechargetresor.pas',
  eval.declaration in '..\shared\eval\eval.declaration.pas',
  eval.emigration in '..\shared\eval\eval.emigration.pas',
  eval.etape in '..\shared\eval\eval.etape.pas',
  eval.init in '..\shared\eval\eval.init.pas',
  eval.limitepop in '..\shared\eval\eval.limitepop.pas',
  eval.miseaupoint in '..\shared\eval\eval.miseaupoint.pas',
  eval.pillage in '..\shared\eval\eval.pillage.pas',
  eval.piratage in '..\shared\eval\eval.piratage.pas',
  eval.points in '..\shared\eval\eval.points.pas',
  eval.sondage in '..\shared\eval\eval.sondage.pas',
  eval.special in '..\shared\eval\eval.special.pas',
  eval.transfert in '..\shared\eval\eval.transfert.pas',
  eval.transfoviind in '..\shared\eval\eval.transfoviind.pas',
  espionnage in '..\shared\espionnage.pas';

{$R *.RES}

begin
  if DebugHook <> 0 then
  begin
    ReportMemoryLeaksOnShutdown := True;
//    FastMM_DeleteEventLogFile;
//    if FastMM_LoadDebugSupportLibrary then
//      FastMM_EnterDebugMode;
//    FastMM_LogToFileEvents := FastMM_LogToFileEvents + [mmetUnexpectedMemoryLeakDetail, mmetUnexpectedMemoryLeakSummary];
  end;

  Application.Initialize;
  Application.HelpFile := 'Nebula.hlp';
  Application.Title := 'N�bula';
  Application.CreateForm(TNebForm, NebForm);
  Application.CreateForm(TParam, Param);
  Application.CreateForm(TAPropos, APropos);
  Application.CreateForm(TConfigReseauForm, ConfigReseauForm);
  Application.CreateForm(TPrintPlanDlg, PrintPlanDlg);
  Application.CreateForm(TImpMondeForm, ImpMondeForm);
  Application.Run;

//  if DebugHook <> 0 then
//  begin
//    FastMM_ExitDebugMode;
//    FastMM_FreeDebugSupportLibrary;
//  end;
end.





