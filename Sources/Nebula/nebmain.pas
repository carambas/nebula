{$DEFINE PASDEBUG}

unit Nebmain;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, Shellapi, ActnList,
  ImgList, ComCtrls,  StdCtrls, ToolWin, ExtCtrls,
  AvatUnit, Utils, Visu, Imprime, SaisOrdr,
  AvatEval, About, Modif, Paraform, Stat, OrdrRend, Buttons, Spin,
  Plan, Listppap, OrdrJou, ColorGrd, WinPlan, OrdresErreurs,
  lectordres, NebData, System.Actions,
  System.ImageList, Winapi.ShlObj, System.zip, IdMessage, IdSMTP, IdAttachment,
  genmail, envoimail, System.IniFiles,
  REST.Types, REST.Client, REST.Authenticator.Basic, JSON, System.Generics.Collections,
  System.UITypes, Vcl.VirtualImageList, Vcl.BaseImageCollection,
  Vcl.ImageCollection;

type
  TNebForm = class(TForm)
    MainMenu1: TMainMenu;
    Fichier1: TMenuItem;
    ConsulterTour: TMenuItem;
    AfficherPlan: TMenuItem;
    Quitter: TMenuItem;
    Editeur1: TMenuItem;
    Fentre1: TMenuItem;
    MenuAction: TMenuItem;
    Info1: TMenuItem;
    Rechercher1: TMenuItem;
    Remplacer1: TMenuItem;
    Chercherencore1: TMenuItem;
    N2: TMenuItem;
    Insrerunfichier1: TMenuItem;
    MenuCascade: TMenuItem;
    MenuMosaiqueV: TMenuItem;
    MenuAlignerIcones: TMenuItem;
    N3: TMenuItem;
    MenuFermerTout: TMenuItem;
    MenuEvaluation: TMenuItem;
    GenerationListings: TMenuItem;
    Statistques: TMenuItem;
    OrdresRendusMenu: TMenuItem;
    CompteRenduEvalMenu: TMenuItem;
    MenuApropos: TMenuItem;
    MenuMosaiqueH: TMenuItem;
    SauverlesordresMenu: TMenuItem;
    Configurationdelimprimante1: TMenuItem;
    Policedimpression1: TMenuItem;
    PrinterSetupDialog1: TPrinterSetupDialog;
    FontDialog1: TFontDialog;
    OpenMailBoxDialog: TOpenDialog;
    Aide1: TMenuItem;
    AideMenu: TMenuItem;
    BarreOutilsPopup: TPopupMenu;
    Aidesurlabarredoutils1: TMenuItem;
    N4: TMenuItem;
    N6: TMenuItem;
    Imprimerleplan1: TMenuItem;
    SaisieOrdres: TMenuItem;
    Saisiedufichierdordres1: TMenuItem;
    Ordresnonexcuts1: TMenuItem;
    ImageList1: TImageList;
    Configurationrseau1: TMenuItem;
    N7: TMenuItem;
    Ouvrirpartie1: TMenuItem;
    OuvrePartieDialog: TOpenDialog;
    Statscompltes1: TMenuItem;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    ToolButton7: TToolButton;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton2: TToolButton;
    Panel2: TPanel;
    TourLabel: TLabel;
    PartieLabel2: TLabel;
    PartieLabel: TLabel;
    TourEdit: TEdit;
    LockCheck: TCheckBox;
    TourUpDown: TUpDown;
    N8: TMenuItem;
    N5: TMenuItem;
    Refaireunevalidentique1: TMenuItem;
    ActionList: TActionList;
    AfficherPlanAction: TAction;
    RecupererOrdresServeurMenu: TMenuItem;
    ImageCollection1: TImageCollection;
    VirtualImageList1: TVirtualImageList;
    EnvoiCRServeurMenu: TMenuItem;
    EnvoiCRServeur: TAction;
    RecupOrdresButton: TToolButton;
    RecupererOrdresServeur: TAction;
    EvalTour: TAction;
    OuvrirPartie: TAction;
    Configurationrseau: TAction;
    SaisieFichierOrdres: TAction;
    SauverOrdres: TAction;
    RefaireEvalIdentique: TAction;
    ModifierTour: TAction;
    Statistiques: TAction;
    StatsCompletes: TAction;
    OrdresRendus: TAction;
    CompteRenduEval: TAction;
    OrdresNonExecutes: TAction;
    Aide: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuAproposClick(Sender: TObject);
    procedure OrdresRendusExecute(Sender: TObject);
    procedure StatistiquesExecute(Sender: TObject);
    procedure QuitterClick(Sender: TObject);
    procedure MenuFermerToutClick(Sender: TObject);
    procedure MenuCascadeClick(Sender: TObject);
    procedure MenuMosaiqueVClick(Sender: TObject);
    procedure MenuAlignerIconesClick(Sender: TObject);
    procedure MenuMosaiqueHClick(Sender: TObject);
    procedure SaisieOrdres2Click(Sender: TObject);
    procedure SauverOrdresExecute(Sender: TObject);
    procedure GenerationListingsClick(Sender: TObject);
    procedure CompteRenduEvalExecute(Sender: TObject);
    procedure FicDonneesBoutonClick(Sender: TObject);
    procedure TourEditChange(Sender: TObject);
    procedure Configurationdelimprimante1Click(Sender: TObject);
    procedure Policedimpression1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GenerationFichiersDonneesClick(Sender: TObject);
    procedure AideExecute(Sender: TObject);
    procedure Aidesurlabarredoutils1Click(Sender: TObject);
    procedure Imprimerleplan1Click(Sender: TObject);
    procedure Fichierdonnesregroupanttouslesjoueurs1Click(Sender: TObject);
    procedure SaisieFichierOrdresExecute(Sender: TObject);
    procedure OrdresNonExecutesExecute(Sender: TObject);
    procedure StatsCompletesExecute(Sender: TObject);
    procedure SaisieOrdresClick(Sender: TObject);
    procedure ConsulterTourClick(Sender: TObject);
    procedure EnvoiVersSMTPClick(Sender: TObject);
    procedure LockCheckClick(Sender: TObject);
    procedure ToolButton6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AfficherleplanF71Click(Sender: TObject);
    procedure AfficherPlanActionExecute(Sender: TObject);
    procedure OuvrirPartieExecute(Sender: TObject);
    procedure Envoirelations1Click(Sender: TObject);
    procedure EnvoiCRServeurExecute(Sender: TObject);
    procedure RecupOrdresButtonClick(Sender: TObject);
    procedure RecupererOrdresServeurExecute(Sender: TObject);
    procedure EvalTourExecute(Sender: TObject);
    procedure ConfigurationrseauExecute(Sender: TObject);
    procedure RefaireEvalIdentiqueExecute(Sender: TObject);
    procedure ModifierTourExecute(Sender: TObject);
  private
    { D�clarations private }
    // DLV 19/04/2020 -- Agrandissement � 200 caract�res car sinon violation d'acc�s m�moire (�tait 40)
    buf : array[0..200] of Char;
    NomImpr, NomPort : PChar;
    Fini : Boolean;
    Consultation : boolean;
    NomPartie : String;
    Tour : word;
    RepFic : String;
    HCheckBitmap : HBITMAP;
    EnModif : boolean;
    ShiftState : TShiftState;
    FonteNom : string;
    FontSize : integer;
    LockActif : boolean;
    SMTPServer : string;
    Data : TNebData;
    Jeton : string;

    procedure EcritTitre;
    procedure RempliMenus;
    procedure EffaceMenus;
    function  GetNomFic : String;
    function  OuvreFic(NomFicFullPath : string) : boolean;
    procedure MenuPseudoDetruit(Menu : TMenuItem; Tous : Boolean);
    procedure MenuPseudo(Menu : TMenuItem; Tous : string);
    procedure GestionMenus(Sender: TObject);
    procedure ConsulteTour(NoJou : integer);
    procedure GenererFichDonnees(NoJou : integer; Prompt : boolean; Complet : boolean);
    procedure FichDonnees(NoJou : byte; var Data : TNebData; Complet : boolean);
    procedure GenListings(prompt : boolean);
    procedure SaisieOrdresJou(NoJou : integer);
    procedure SendRelations;
    procedure WMDropFiles(var MsgInfo : TWMDropFiles); message WM_DROPFILES;
    function  CheckNBAExt : boolean;
    procedure RegisterNBA;
    function GetOrdresFileName : string;
    procedure ChargeDonnees;
    function isDataAssigned : boolean;
    procedure AfficherPlanClic;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    Ini : TMemIniFile;

    procedure CheckOrdres;
    function GetFileName : string;
    function GetFileNameTour(Tour : integer) : string;
  end;

var
  NebForm: TNebForm;

implementation

uses
  Printers, FileCtrl, SelecJou, Impmonde, ConfigReseau, Registry, System.IOUtils;

{$R ..\Shared\NEBULA2.RES ..\Shared\NEBULA2.RC}

{$R *.DFM}


procedure TNebForm.FormCreate(Sender: TObject);
var
   tempo : PChar;
   nbtFileName : String;
   tmpNomPartie : String;
   tmpTour : Integer;
begin
   Jeton := '';
   ToolBar1.COlor := clBtnFace;
   if not CheckNBAExt then
     RegisterNBA;


   DragAcceptFiles(Handle, True);
   Screen.Cursor := crHourGlass;

   {$IFDEF LIGHT}
     MenuAction.Visible := False;
     SaisieOrdres.Visible := False;
     Sauverlesordres.Visible := False;
     CompteRenduEval.Visible := False;
     Configurationrseau1.Visible := False;
     Saisiedufichierdordres1.Visible := False;
     MenuAction.Enabled := False;
     SaisieOrdres.Enabled := False;
     Sauverlesordres.Enabled := False;
     CompteRenduEval.Enabled := False;
     Statistiques.Enabled := False;
     StatsCompletes.Enabled := False;
     OrdresNonExecutes.Enabled := False;
     Configurationrseau1.Enabled := False;
     Saisiedufichierdordres1.Enabled := False;
     ToolButton3.Enabled := False;
     ToolButton4.Enabled := False;
     ToolButton5.Enabled := False;
     LockCheck.Visible := False;
   {$ENDIF}


   Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBULA.INI');
   Ini.AutoSave := True;
   RepFic := Ini.ReadString('NEBULA', 'RepFic', '');
   Imprime.RepFic := RepFic;
   FonteNom := Ini.ReadString('NEBULA', 'FonteNom', 'Courier New');
   FontSize := Ini.ReadInteger('NEBULA', 'FontSize', 10);
   SMTPServer := Ini.ReadString('NEBULA', 'SMTPServer', '');
   tmpNomPartie := Ini.ReadString('NEBULA', 'NomPartie', '');
   tmpTour := Ini.ReadInteger('NEBULA', 'NoTour', 0);

   if not SysUtils.DirectoryExists(RepFic) then
   begin
     RepFic := ProgRep;
     Imprime.RepFic := ProgRep;
     NomPartie := '';
     Imprime.NomPartie := '';
     Tour := 0;
   end;

   SaisieOrdresForm := nil;
   Fini := False;
   Consultation := False;
   EnModif := False;
   LockActif := True;
   HCheckBitmap := LoadBitmap(HInstance, 'CHECK');
     if HCheckBitmap = 0 then ShowMessage('Erreur en chargement des ressources');

   GetProfileString('windows', 'device', nil, @buf, sizeof(buf));
   NomImpr := @buf;
   tempo := StrPos(@buf, ',');
   if tempo <> nil then
   begin
      tempo[0] := #0;
      NomPort := StrPos(tempo+1, ',')+1;
   end;
   RempliMenus;

   if ParamStr(1) = '' then
   begin
    nbtFileName := Format('%s\%s%.2d.nba', [RepFic, tmpNomPartie, tmpTour]);
    if FileExists(nbtFileName) then
      OuvreFic(nbtFileName);
   end
   else if ParamStr(1) <> '' then if FileExists(ParamStr(1)) then
   begin
     if (LowerCase(ExtractFileExt(ParamStr(1))) = '.nba') and
        FileExists(ParamStr(1)) then
          OuvreFic(ParamStr(1));
   end;

   Screen.Cursor := crDefault;
end;

procedure TNebForm.FormDestroy(Sender: TObject);
begin
   Ini.WriteString('NEBULA', 'NomPartie',NomPartie);
   Ini.WriteInteger('NEBULA', 'NoTour',Tour);
   Ini.WriteString('NEBULA', 'RepFic', RepFic);
   Ini.WriteString('NEBULA', 'FonteNom', FonteNom);
   Ini.WriteInteger('NEBULA', 'FontSize', FontSize);
   Ini.WriteString('NEBULA', 'SMTPServer', SMTPServer);

   EffaceMenus;

   DeleteObject(HCheckBitmap);
   Ini.Destroy;

   if Assigned(Data) then
    FreeAndNil(Data);
end;

procedure TNebForm.EcritTitre;
begin
  Caption := 'N�bula';
  {$IFDEF DEBUG}
  Caption := Caption + ' *** DEBUG ***';
  {$ENDIF}
  if NomPartie <> '' then
  begin

    LockCheck.Enabled := Data.Tour = Tour;
    TourEdit.Enabled := True;
    TourLabel.Enabled := True;

    LockActif := False;
    if Data.Tour = Tour then
    begin
      if Data.GetLock(Tour) then
        LockCheck.Checked := True
      else
        LockCheck.Checked := False;
    end
    else
      LockCheck.Checked := False;
    LockActif := True;
    PartieLabel.Caption := NomPartie;
    TourUpDown.Position := Tour;
  end
  else
  begin
    LockCheck.Enabled := False;
    TourEdit.Enabled := False;
    TourLabel.Enabled := False;
    PartieLabel.Caption := 'Aucune';
    TourUpDown.Position := 0;
  end;
end;

procedure TNebForm.RempliMenus;
begin
   EnModif := True;
   EcritTitre;
   if NomPartie <> '' then
   begin
     MenuPseudo(ConsulterTour, 'Tous');
     MenuPseudo(SaisieOrdres, '');
     Statistiques.Enabled := True;
     StatsCompletes.Enabled := True;
     OrdresRendus.Enabled := True;
     CompteRenduEval.Enabled := True;
     OrdresNonExecutes.Enabled := True;
     CheckOrdres;

     DrawMenuBar(Handle);
   end
   else
   begin
     Statistiques.Enabled := False;
     StatsCompletes.Enabled := False;
     OrdresRendus.Enabled := False;
     CompteRenduEval.Enabled := False;
     OrdresNonExecutes.Enabled := False;
     CheckOrdres;

     DrawMenuBar(Handle);
   end;

   EnModif := False;
end;

procedure TNebForm.EffaceMenus;
begin
   MenuPseudoDetruit(ConsulterTour, True);
   MenuPseudoDetruit(SaisieOrdres, True);
   Statistiques.Enabled := False;
   StatsCompletes.Enabled := False;
   OrdresRendus.Enabled := False;
   CompteRenduEval.Enabled := False;
   OrdresNonExecutes.Enabled := False;
   CheckOrdres;

   DrawMenuBar(Handle);
end;

procedure TNebForm.MenuAproposClick(Sender: TObject);
begin
  ShowAbout(version);
end;

procedure TNebForm.OrdresRendusExecute(Sender: TObject);
begin
  ShowOrdresRendus(Data);
end;

procedure TNebForm.StatistiquesExecute(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;
  ShowStat(RepFic + '\' + NomPartie, Tour);
end;

function TNebForm.GetNomFic : String;
var
   TourCh : String;
begin
   TourCh := IntToStr(Tour);
   if Tour < 10 then TourCh := '0' + TourCh;
   GetNomFic := NomPartie + TourCh;
end;


procedure TNebForm.CheckOrdres;
var
   NomFichier : string;
   Fichier : textFile;
   Chaine : string;
   OrdresRendus : array[1..NB_JOU_MAX] of integer;
   jou_courant, oldjou : integer;
   i : integer;
begin
  {Check pour les ordres rendus}
  FillChar(OrdresRendus, Sizeof(OrdresRendus), 0);
  NomFichier := GetOrdresFileName;
  if FileExists(Nomfichier) then
  begin
    jou_courant := 0;
    AssignFile(Fichier, NomFichier);
    Reset(Fichier);
    while not EOF(Fichier) do
    begin
      oldjou := jou_courant;
      Readln(Fichier, Chaine);
      if Copy(Chaine, 1, 2) = 'Jo' then
        jou_courant := StrToInt(Copy(Chaine, 3, 2));
      if Copy(Chaine, 1, Length(BeginCh)) = BeginCh then
        jou_courant := StrToInt(Copy(Chaine, Length(BeginCh)+1, 2));
      if Chaine = EndCh then
        jou_courant := 0;
      if (jou_courant > 0) and (jou_courant <= NB_JOU_MAX) and (oldjou = jou_courant)then
        inc(OrdresRendus[jou_courant]);
    end;
    CloseFile(Fichier);
  end;

  for i := 1 to SaisieOrdres.Count do
    SaisieOrdres.Items[i-1].Checked := (OrdresRendus[i] > 0);

end;

procedure TNebForm.MenuPseudoDetruit(Menu : TMenuItem; Tous : Boolean);
var
  i : word;
begin
   for i := Menu.Count downto 1 do
   begin
     Menu.Items[i-1].Free;
   end;
end;

procedure TNebForm.MenuPseudo(Menu : TMenuItem; Tous : string);
var
   i : word;
   M : TMenuItem;
begin
   if Tous <> '' then
   begin
      Menu.Add(NewItem(Tous, 0, False, True, GestionMenus, 0,
        Menu.Name + '00'));
      Menu.Add(NewItem('S�lection de joueurs', 0, False, True,
        GestionMenus, 0, Menu.Name + Format('%.2d', [NB_JOU_MAX + 1])));
      Menu.Add(NewLine);
   end;
   for i := 1 to Data.NbJou do
   begin
      M := NewItem(Data.Jou[i].Pseudo, 0, False, True,
        GestionMenus, 0, Menu.Name + Format('%.2d',  [i]));
      if (Data.NbJou > 17) and (((i = 16) and (Tous <> '')) or
        ((i = 17) and (Tous = ''))) then
        M.Break := mbBarBreak;
      Menu.Add(M);
   end;
end;

procedure TNebForm.GestionMenus(Sender: TObject);
var
  Nom : string;
  NoJou : integer;
begin
  Nom := Copy((Sender as TMenuItem).Name, 1,
    length((Sender as TMenuItem).Name) - 2);
  NoJou := StrToInt(Copy((Sender as TMenuItem).Name,
    length((Sender as TMenuItem).Name) - 1, 2));
  if Nom = ConsulterTour.Name then
  begin
    {Consulter le tour}
    ConsulteTour(NoJou);
  end
  else if Nom = SaisieOrdres.Name then
  begin
    { Saisie des ordres d'un joueur }
    SaisieOrdresJou(NoJou);
  end;
end;

procedure TNebForm.QuitterClick(Sender: TObject);
begin
  Close;
end;

procedure TNebForm.ConsulteTour(NoJou : integer);
var
  CRList:TStringList;
  fic : file;
  Pseudo : String;
  i : word;
  VisuTexte : TVisuTexte;
  SelectDlg : TSelectJouDlg;
  Selection : TSelectionJou;
begin
    CRList := TSTringList.Create;

    if (NoJou > 0) and (NoJou < NB_JOU_MAX+1) then
    begin
       Pseudo := Data.Jou[NoJou].Pseudo;
    end;

    if NoJou < NB_JOU_MAX+1 then
    begin
      Screen.Cursor := crHourGlass;
      ImprimeData(Data, nil, NoJou, CRList);
    end
    else
    begin
      {S�lection de joueurs}
      SelectDlg := TSelectJouDlg.Create2(Self, Data, Selection);
      SelectDlg.ShowModal;
      if SelectDlg.Modalresult = mrOK then
      begin
        Screen.Cursor := crHourGlass;
        for i := 1 to NB_JOU_MAX do if Selection[i] then
          ImprimeData(Data, nil, i, CRList);
      end;
      FreeAndNil(SelectDlg);
    end;

    Screen.Cursor := crDefault;

    VisuTexte := TVisuTexte.Create(Self);
    VisuTexte.Caption := 'Consultation -- Partie ' + NomPartie + ', Tour ' +
      Format('%.2d', [Tour]);
    if (NoJou > 0) and (NoJou < NB_JOU_MAX+1) then
      VisuTexte.Caption := VisuTexte.Caption + ', Joueur ' + Pseudo
    else if NoJou = 0 then
      VisuTexte.Caption := VisuTexte.Caption + ', tous les joueurs'
    else
      VisuTexte.Caption := VisuTexte.Caption + ', s�lection de joueurs';

    VisuTexte.BigText.Lines.BeginUpdate;
    VisuTexte.BigText.Lines.Assign(CRList);
    VisuTexte.BigText.Lines.EndUpdate;
    VisuTexte.BigText.SelStart := 0;
    VisuTexte.Show;

    CRList.Free;

end;

procedure TNebForm.MenuFermerToutClick(Sender: TObject);
var
  i : integer;
begin
  for i := 1 to MDIChildCount do
    MDIChildren[i-1].Close;
end;

procedure TNebForm.MenuCascadeClick(Sender: TObject);
begin
  Cascade;
end;

procedure TNebForm.MenuMosaiqueVClick(Sender: TObject);
begin
  TileMode := tbVertical;
  Tile;
end;

procedure TNebForm.MenuAlignerIconesClick(Sender: TObject);
begin
  ArrangeIcons;
end;

procedure TNebForm.MenuMosaiqueHClick(Sender: TObject);
begin
  TileMode := tbHorizontal;
  Tile;
end;

procedure TNebForm.SaisieOrdres2Click(Sender: TObject);
begin
  if SaisieOrdresForm = nil then
  begin
    SaisieOrdresForm := TSaisieOrdresForm.Create2(Self,
      GetOrdresFileName);
    SaisieOrdresForm.Caption := 'Ordres du tour ' + IntToStr(Tour);
  end;
  SaisieOrdresForm.Show;
end;

procedure TNebForm.SauverOrdresExecute(Sender: TObject);
var
  i : integer;
begin
  if SaisieOrdresForm <> nil then
    SaisieOrdresForm.Sauve;

  for i := 1 to MDIChildCount do
    if MDIChildren[i-1].Tag = 1 then
      (MDIChildren[i-1] as TSaisieOrdresJouForm).Sauve;
end;

procedure TNebForm.ModifierTourExecute(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;
  if not Data.GetLock(Tour) then
    ModTour(Data)
  else
    ShowMessage('La modification est impossible' + #13 +
                'car le tour est verrouill�');
end;

procedure TNebForm.FichDonnees(NoJou : byte; var Data : TNebData;
                               Complet : boolean);

  function ModuloX(x : integer) : integer;
  var
    resultat : integer;
  begin
    resultat := x;
    repeat
      if resultat > Data.Map.LongX then
        dec(resultat, Data.Map.LongX);
      if resultat <= 0 then
        inc(resultat, Data.Map.LongX);
    until (resultat > 0) and (resultat <= Data.Map.LongX);
    ModuloX := resultat;
  end;

  function ModuloY(y : integer) : integer;
  var
    resultat : integer;
  begin
    resultat := y;
    repeat
      if resultat > Data.Map.LongY then
        dec(resultat, Data.Map.LongY);
      if resultat <= 0 then
        inc(resultat, Data.Map.LongY);
    until (resultat > 0) and (resultat <= Data.Map.LongY);
    ModuloY := resultat;
  end;

begin
  Data.SauveEnNBT(NoJou, RepFic + '\data');
end;

procedure TNebForm.GenererFichDonnees(NoJou : integer; Prompt : boolean; Complet : boolean);
var
  i : integer;
  JouImp : TChoixJoueurs;
begin

  if (NoJou <> NB_JOU_MAX + 1) then
  begin
    Screen.Cursor := crHourGlass;

    if Complet then
    begin
      FichDonnees(0, Data, True);
    end
    else
    begin
      if NoJou = 0 then
      begin
        for i := 1 to Data.NbJou do
          FichDonnees(i, Data, False);
      end
      else if NoJou = NB_JOU_MAX+1 then
      begin
        for i := 1 to NB_JOU_MAX do
          if JouImp[i] then
            FichDonnees(i, Data, False);
      end
      else if (NoJou > 0) and (NoJou < NB_JOU_MAX+1) then
      begin
        FichDonnees(NoJou, Data, False);
      end;
    end;

    Screen.Cursor := crDefault;
    if Prompt then
      ShowMessage('G�n�ration des fichiers de donn�es effectu�e');
  end
  else
    if Prompt then
      ShowMessage('La g�n�ration des fichiers de donn�es a �chou�');
end;

procedure TNebForm.GenListings(prompt : boolean);
var
  i : integer;
  JouCh : string;
  TourCh : string;
  NomFic : string;
  CRList: TStringList;
begin
  Screen.Cursor := crHourGlass;
  CRList := TStringList.Create;
  TourCh := IntToStr(Tour);
  if Tour < 10 then TourCh := '0' + TourCh; // TODO utiliser format
  for i := 1 to Data.NbJou do
  begin
    JouCh := IntToStr(i);
    if i < 10 then JouCh := '0' + JouCh;
    NomFic := RepFic + '\' + Copy(NomPartie, 1, 3) + 'R' + JouCh + TourCh + '.TXT'; // TODO utiliser m�thode pour g�n�rer le nom du fichier (TNebData ?)
    ImprimeData(Data, nil, i, CRList);
    CRList.SaveToFile(NomFic);
  end;
  CRList.Free;
  Screen.Cursor := crDefault;
  if prompt then
    ShowMessage('G�n�ration des listings effectu�e');

end;

procedure TNebForm.GenerationListingsClick(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;
  GenListings(True);
end;

procedure TNebForm.CompteRenduEvalExecute(Sender: TObject);
var
  VisuLog : TVisuTexte;
begin
  VisuLog := TVisutexte.Create(Self);
  VisuLog.Caption := 'Ev�nements notables';
  if FileExists(Format('%s\%s%.2d.log', [ExtractFilePath(Data.FileName), Data.NomPartie, Data.Tour])) then
    VisuLog.Bigtext.Lines.LoadFromFile(Format('%s\%s%.2d.log', [ExtractFilePath(Data.FileName), Data.NomPartie, Data.Tour]));
  VisuLog.Show;
end;

procedure TNebForm.FicDonneesBoutonClick(Sender: TObject);
begin
  GenererFichDonnees(0, True, False);
end;

procedure TNebForm.TourEditChange(Sender: TObject);
begin
  if NomPartie <> '' then
    if not EnModif then
    begin
      Tour := StrToIntdef(TourEdit.Text, 0);

      if Data.Tour = Tour then
        Exit;

      ChargeDonnees;
      EffaceMenus;
      RempliMenus;
      if SaisieOrdresForm <> nil then
      begin
        SaisieOrdresForm.Close;
        SaisieOrdresForm := nil;
      end;
    end;
end;

procedure TNebForm.AfficherPlanClic;
begin
  if not isDataAssigned then
    Exit;

  PlanForm := TPlanForm.Create2(Self, Data, PlanSansCR);

  if ssShift in ShiftState then
  begin
    PlanForm.FormStyle := fsNormal;
    PlanForm.WindowState := wsMaximized;
  end
  else
    PlanForm.FormStyle := fsMDIChild;

  PlanForm.Show;
{  end;}
end;

procedure TNebForm.Configurationdelimprimante1Click(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
end;

procedure TNebForm.Policedimpression1Click(Sender: TObject);
begin
  FontDialog1.Font.Name := FonteNom;
  FontDialog1.Font.Size := FontSize;
  if FontDialog1.Execute then
  begin
    FonteNom := FontDialog1.Font.Name;
    FontSize := FontDialog1.Font.Size;
  end;
end;

procedure TNebForm.FormActivate(Sender: TObject);
begin
  //Panel1.Visible := True;
end;

procedure TNebForm.GenerationFichiersDonneesClick(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;
  GenererFichDonnees(0, true, False);
end;

procedure TNebForm.AideExecute(Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TNebForm.Aidesurlabarredoutils1Click(Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTEXT, 1001);
end;

procedure TNebForm.Imprimerleplan1Click(Sender: TObject);
var
  PlanForm : TPlanForm;
begin
  if not isDataAssigned then
    Exit;
  PlanForm := TPlanForm.CreatePar(Self, RepFic + '\' + NomPartie, Tour, True);
  PlanForm.WinPlan.ImprimerPlan;
  PlanForm.Destroy;
end;

procedure TNebForm.SaisieOrdresJou(NoJou : integer);
var
  s : TSaisieOrdresJouForm;
begin
  s := TSaisieOrdresJouForm.Create2(Self, RepFic + '\' + NomPartie, Tour, NoJou);
  s.Caption := 'Saisie des ordres de ' + Data.Jou[NoJou].Pseudo
               + ' pour le tour ' + IntToStr(Tour);
  s.Show;
end;

procedure TNebForm.Fichierdonnesregroupanttouslesjoueurs1Click(
  Sender: TObject);
begin
  GenererFichDonnees(0, true, true);
end;

procedure TNebForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);

//  Params.ExStyle := Params.ExStyle or WS_EX_CLIENTEDGE;
end;


procedure TNebForm.SaisieFichierOrdresExecute(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;

  if Tour = 0 then
  begin
    ShowMessage('Passez d''abord au tour 1');
    Exit;
  end;

  if SaisieOrdresForm <> nil then
    SaisieOrdresForm.Show
  else
    SaisieOrdresForm := TSaisieOrdresForm.Create2(Self, GetOrdresFileName);
end;

procedure TNebForm.OrdresNonExecutesExecute(Sender: TObject);
begin
  if ErreursOrdresForm = nil then
  begin
    ErreursOrdresForm :=TErreursOrdresForm.Create(Self);
  end;
  ErreursOrdresForm.AfficheErreurs(RepFic,NomPartie,Tour);

  ErreursOrdresForm.Show;
end;

procedure TNebForm.ConfigurationrseauExecute(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;

  ConfigReseauForm.NomEdit.Text := Data.NomArbitre;
  ConfigReseauForm.EmailEdit.Text := Data.EmailArbitre;
  ConfigReseauForm.SMTPEdit.Text := SMTPServer;
  ConfigReseauForm.ShowModal;

  if ConfigReseauForm.Modalresult = mrOK then
  begin
    Data.NomArbitre := ConfigReseauForm.NomEdit.Text;
    Data.EmailArbitre := ConfigReseauForm.EmailEdit.Text;
    SMTPServer := ConfigReseauForm.SMTPEdit.Text;
    Data.SauveEnNBT(0, '');
  end;
end;

procedure TNebForm.SendRelations;
var
  i, j : integer;
  RESTClient: TRESTClient;
  RESTRequest: TRESTRequest;
  JSONObject : TJSONObject;
  JSONRelation : TJSONArray;
  Rep: TCustomRESTResponse;
  Auth: THTTPBasicAuthenticator;
  ReqStr : string;
begin
    // On pr�pare la requ�te JSON
    JSONObject := TJSONObject.Create;

    for i := 1 to Data.NbJou do
    begin

      // On pr�pare l'objet JSONRelation qui correspond aux joueurs connus par le joueur i
      JSONRelation := TJSonArray.Create;
      for j := 1 to Data.NbJou do
        if Data.ConnuDeNom[i, j] then
        begin
          JSONRelation.Add(TJSONObject.Create(TJSONPair.Create('id', Format('%s-%u', [Data.NomPartie, j]))));
        end;

      JSONObject.AddPair(IntToStr(i),
        TJSONObject.Create(TJSONPair(TJSONPair.Create('id', Format('%s-%u', [Data.NomPartie, i])))) //Joueur dont on va afficher les relations
          .AddPair(TJSONPair.Create('relations', JSONRelation) // Ses relations
          )
      );
    end;
    ReqStr := JSONObject.ToString;
    RESTRequest := TRESTRequest.Create(nil);
    RESTClient := TRESTClient.Create(nil);
    RESTRequest.Client := RESTClient;
    RESTClient.BaseURL := URL_API_NEBMAIL;

    Auth := THTTPBasicAuthenticator.Create
      (Format('%s-%d', [UpperCase(Data.NomPartie), Data.NoJou]),
      Data.PasswordPartie);
    RESTClient.Authenticator := Auth;

    RESTRequest.Params.Clear;
    RESTRequest.AddBody(JSONObject);
    RESTRequest.Method := TRESTRequestMethod.rmPUT;
    RESTRequest.Resource := 'relation';

    Rep := TCustomRESTResponse.Create(Self);
    try
      RESTRequest.Execute;
      Rep := RESTRequest.Response;
      if Rep.StatusCode <> 200 then
        ShowMessage('Erreur dans l''envoi des relations : ' + Rep.Content);
    except
        ShowMessage('Erreur dans l''envoi des relations : ' + Rep.Content);
    end;
    Auth.Free;
    RESTClient.Free;
    RESTRequest.Free;


    //ShowMessage(JSONObject.ToString);

end;

{Gestion du dropdown : l'utilisateur fait glisser un nom de fichier sur
 la fen�tre principale}
procedure TNebForm.WMDropFiles(var MsgInfo : TWMDropFiles);
var
  NomFic : array[0..256] of char;
begin
  DragQueryFile(MsgInfo.Drop, 0, NomFic, sizeof(NomFic));
  if LowerCase(ExtractFileExt(StrPas(NomFic))) <> '.nba' then
    ShowMessage('Mauvais type de fichier')
  else
    OuvreFic(StrPas(NomFic));
  DragFinish(MsgInfo.Drop);
end;

function TNebForm.OuvreFic(NomFicFullPath : string) : boolean;
var
  PCharTemp : array[0..80] of char;
begin
  Result := True;
  if not FileExists(NomFicFullPath) then
    result := False
  else
  begin
    RepFic := ExtractFilePath(NomFicFullPath);
    if Copy(RepFic, length(RepFic), 1) = '\' then
      RepFic := Copy(RepFic, 1, length(RepFic) - 1);
    Imprime.RepFic := RepFic;
    Data := TNebData.Create(NomFicFullPath, Tour);
    Tour := Data.Tour;
    NomPartie := Data.NomPartie;
    Imprime.NomPartie := NomPartie;
    if SaisieOrdresForm <> nil then
    begin
      SaisieOrdresForm.Close;
      SaisieOrdresForm.Free;
      SaisieOrdresForm := nil;
    end;
    EffaceMenus;
    RempliMenus;

    GetPrivateProfileString(PChar(Data.NomPartie), 'Jeton', '',
      PCharTemp, sizeof(PCharTemp), 'NEBULA.INI');
    Jeton := PCharTemp;
  end;
end;

{regarde si l'extention de fichier .NBA est bien associ�e � N�bula}
function TNebForm.CheckNBAExt : boolean;
var
  Reg : TRegistry;
  st : string;
  p : integer;
begin
  Result := False;
  Reg := TRegistry.Create(KEY_READ);
  Reg.RootKey := HKEY_CLASSES_ROOT;
  if Reg.OpenKey('.nba', false) then
  begin
    st := Reg.ReadString('');
    if st <> '' then
    begin
      Reg.CloseKey;
      if Reg.OpenKey(st+'\Shell\Open\Command', false) then
      begin
        p := Pos(' "%1"', Reg.ReadString(''));
        if p = 0 then
          Result := False {La commande doit �tre NomProg "%1"}
        else
        begin
          if StrLComp(PChar(Reg.ReadString('')), PChar(ParamStr(0)), p-1) <> 0 then
            Result := False {Le prog associ� au .NBA n'est pas au m�me endroit que celui qui est ex�cut�}
          else
            Result := True; {Tout va bien, on ne change rien}
        end;
      end;
    end;
  end;
  Reg.Free;
end;

{Associe l'extention de fichier .NBA � N�bula}
procedure TNebForm.RegisterNBA;
var
  Reg : TRegistry;
begin
  if not IsUserAnAdmin then
    Exit;

  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  if Reg.OpenKey('.nba', true) then
  begin
    Reg.WriteString('', 'Nebula_NBA');
    Reg.WriteString('Content Type', 'application/x-nebula');
    Reg.CloseKey;
    if Reg.OpenKey('Nebula_NBA', true) then
    begin
      Reg.WriteString('', 'N�bula - Partie');
      Reg.OpenKey('DefaultIcon', true);
      Reg.WriteString('', ParamStr(0)+',0');
      Reg.CloseKey;
      Reg.OpenKey('Nebula_NBA\Shell\Open\Command', true);
      Reg.WriteString('', ParamStr(0) + ' "%1"');
      Reg.CloseKey;
    end
    else
      ShowMessage('Erreur register NBA 2');
  end
  else
    ShowMessage('Impossible de faire l''association avec le type de fichier .nba. Pour sa premi�re ex�cution lancer le programme avec les droits administrateur.');
  Reg.Free;
end;

procedure TNebForm.StatsCompletesExecute(Sender: TObject);
begin
  if not isdataAssigned then
    Exit;
  if CalculeStatsCompletes(Repfic + '\' + NomPartie, Tour) then
    ShowMessage(Format('Stats g�n�r�es dans %s%s-test.txt', [ExtractFilePath(ParamStr(0)), NomPartie]))
  else
    ShowMessage('Erreur dans la g�n�ration des stats'); 
end;

procedure TNebForm.EnvoiCRServeurExecute(Sender: TObject);
var
  RESTClient: TRESTClient;
  RESTRequest: TRESTRequest;
  JSONCRData : TJSONObject;
  Rep: TCustomRESTResponse;
  Auth: THTTPBasicAuthenticator;

begin
  if not isDataAssigned then
  begin
    ShowMessage('Pas de partie ouverte');
    Exit;
  end;


  if MessageDlg('Vous allez stocker le fichier .nba sur le serveur et envoyer les CR aux joueurs' + #10#13 +
    'Voulez-vous continuer ?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrNo then
    Exit;

  JSONCRData := TJSONObject.Create;
  JSONCRData.AddPair('data',
  TJSONObject.Create(
    TJSONPair.Create('tour', TJSONNumber.Create(Data.Tour))) // tour
      .AddPair(
        TJSONPair.Create('envoi_cr', TJSONNumber.Create(1)) // envoi_cr
      )
  );

  Auth := THTTPBasicAuthenticator.Create
    (Format('%s-%d', [UpperCase(Data.NomPartie), Data.NoJou]),
    Data.PasswordPartie);

  RESTClient := TRESTClient.Create(nil);
  RESTClient.BaseURL := URL_API_NEBMAIL;
  RESTClient.ContentType := 'multipart/form-data';
  RESTClient.Authenticator := Auth;

  RESTRequest := TRESTRequest.Create(nil);
  RESTRequest.Method := TRESTRequestMethod.rmPOST;
  RESTRequest.Resource := 'cr';
  RESTRequest.Client := RESTClient;
  RESTRequest.Params.Clear;


  // Cette ligne est une bidouille car se fait supprimer par AddParameter plus bas
  // Et en plus le content-length est mal calcul�
  RESTRequest.AddFile('toto', Data.GetNomFicNBA(Data.Tour));

  // La vraie ligne d'ajour de fichier
  RESTRequest.AddFile('nba', Data.GetNomFicNBA(Data.Tour));

  RESTRequest.AddParameter('json', JSONCRData.ToString, pkREQUESTBODY);


  Rep := TCustomRESTResponse.Create(Self);
  try
    RESTRequest.Execute;
    Rep := RESTRequest.Response;
    if Rep.StatusCode <> 200 then
      ShowMessage('Erreur dans l''envoi du fichier .nba : ' + Rep.Content + Rep.ErrorMessage)
    else
      SendRelations; // On envoie les relations au serveur
  except
      ShowMessage('Erreur dans l''envoi du fichier .nba : ' + Rep.Content + Rep.ErrorMessage);
  end;
  Auth.Free;
  RESTClient.Free;
  RESTRequest.Free;
  if Rep.StatusCode = 200 then
    ShowMessage('Fichier ' + ExtractFileName(Data.GetNomFicNBA(Data.Tour)) + ' envoy� avec succ�s' +#10#13#10#13
    + 'Le tour ' + IntToStr(Data.Tour) + ' est stock� sur le serveur');

end;

procedure TNebForm.Envoirelations1Click(Sender: TObject);
begin
  SendRelations;
end;

function TNebForm.GetFileName : string;
begin
  Result := Format('%s\%s%.2d.nba', [RepFic, NomPartie, Tour]);
end;

function TNebForm.GetFileNameTour(Tour : integer) : string;
begin
  Result := Format('%s\%s%.2d.nba', [RepFic, NomPartie, Tour]);
end;

function TNebForm.GetOrdresFileName : string;
begin
  Result :=   Format('%s\%s', [RepFic, GetNomFicOrdres(NomPartie, Tour)]);
end;

procedure TNebForm.ChargeDonnees;
begin
  if FileExists(GetFileName) then
  begin
    Data.Free;
    Data := TNebData.Create(GetFileName, Tour);
  end;
  CheckOrdres;
end;

function TNebForm.isDataAssigned : boolean;
begin
  Result := Assigned(Data);
  if not Result then
    ShowMessage('Ouvrez d''abord une partie');
end;

procedure TNebForm.SaisieOrdresClick(Sender: TObject);
begin
  if not isDataAssigned then
    exit;
  if Tour = 0 then
  begin
    ShowMessage('Pas d''ordres au tour 0');
    Exit;
  end;
end;

procedure TNebForm.ConsulterTourClick(Sender: TObject);
begin
  isDataAssigned;
end;

procedure TNebForm.EnvoiVersSMTPClick(Sender: TObject);
begin
  if not isDataAssigned then
    Exit;
end;

procedure TNebForm.EvalTourExecute(Sender: TObject);
var
  i : integer;
  Ordres : TStringList;
  newData: TNebData;
begin
  if not isDataAssigned then
    Exit;

  if Tour = 0 then
  begin
    ShowMessage('Pas d''eval au tour 0, allez au tour 1');
    Exit;
  end;
  {$IFNDEF LIGHT}
  //OK := True;
  begin
    if (NomPartie <> '') and (Tour>0) and (not Data.GetLock(Tour)) then
    begin
      Screen.Cursor := crHourGlass;
      if SaisieOrdresForm <> nil then
        SaisieOrdresForm.Sauve;

      for i := 1 to MDIChildCount do
        if MDIChildren[i-1].Tag = 2 then
          (MDIChildren[i-1] as TSaisieOrdresForm).Sauve;

      for i := 1 to MDIChildCount do
        if MDIChildren[i-1].Tag = 1 then
          (MDIChildren[i-1] as TSaisieOrdresJouForm).Sauve;

      (*{$DEFINE DEBUG}*)

      Data.Free;
      Dec(Tour);
      Data := TNebdata.Create(GetFileName, Tour);
      Inc(Tour);
      Ordres := TStringList.Create;
      if FileExists(GetOrdresFileName) then
        Ordres.LoadFromFile(GetOrdresFileName);
      newData := EvalTourData(Data, Ordres, 0, False);
      //ShowMessage(newData.LogEval.Text);
      Data.Free;
      Data := newData;
      Data.LogEval.SaveToFile(Format('%s%s%.2d.log', [ExtractFilepath(Data.FileName),
        Data.NomPartie, Data.Tour]));
      FreeAndNil(Ordres);

      {$IFNDEF DEBUG}
      LockCheck.Enabled := True;
      {$ENDIF}
      Screen.Cursor := crDefault;
      CompteRenduEvalExecute(Self);
    end
    else if Data.GetLock(Tour) then
      ShowMessage('Impossible de faire l''�valuation' + #13 +
                  'car le tour est verrouill�')
    else
      ShowMessage('Pas d''�valuation au tour 0' + #13 +
                  'Il faut �tre au moins au tour 1');
  end;
  //Data.Free;
  {$ENDIF}
end;

procedure TNebForm.RecupererOrdresServeurExecute(Sender: TObject);
var
  RESTClient : TRESTClient;
  RESTRequest : TRESTRequest;
  Rep : TCustomRESTResponse;
  Auth : THTTPBasicAuthenticator;
  JSONValue : TJSONValue;
  JSONObject : TJSonObject;
  i, j : Integer;
  OrdresContent : string;
  Fichier : textFile;

begin

  if not isDataAssigned Then
    Exit;

  if Data.GetLock(Tour) then
  begin
    ShowMessage('Le tour est verrouill�, cette op�ration �craserait les ordres');
    Exit;
  end;

  if (Application.MessageBox(
    'Cette op�ration remplacera l''ensemble des ordres actuellement stock�s. Souhaitez-vous continuer ?',
    'Attention', MB_OKCANCEL)
     = IDCANCEL) then
    Exit;


  RESTRequest := TRESTRequest.Create(nil);
  RESTClient := TRESTClient.Create(nil);
  RESTRequest.Client := RESTClient;
  // TODO : rendre param�trable l'adreses de l'API
  RESTClient.BaseURL := 'https://nebmail.ludimail.net/api';

  Auth := THTTPBasicAuthenticator.Create(nil);
  with Auth do
  begin
    Username := Format('%s-%d', [NomPartie, 0]);
    Password := Data.PasswordPartie;
  end;

  RESTClient.Authenticator := Auth;

  RESTRequest.Params.Clear;
  RESTRequest.Method := TRESTRequestMethod.rmGET;
  RESTRequest.Resource := 'ordres';
  try
    RESTRequest.Execute;
    Rep := RESTRequest.Response;

    if Rep.StatusCode = 200 then
    begin
      JSONValue := Rep.JSONValue;
      JSONValue := (JSonValue as TJSONObject).Get('ordres').JsonValue;

      try
        if (JSONValue is TJSONObject) then
        begin
          OrdresContent := '';
          JSONObject := (JSONValue as TJSONObject);
          for i := 0 to JSONObject.Count - 1 do
          begin
            j := StrToInt(JSONObject.Pairs[i].JsonString.Value);
            OrdresContent := OrdresContent +
              Format(
                '%s%d' + sLineBreak + '%s' + sLineBreak + '%s' + sLineBreak,
                [BeginCh, j, JSONObject.Pairs[i].JsonValue.Value, EndCh]);
          end;

          AssignFile(Fichier, GetOrdresFileName);
          Rewrite(Fichier);
          Write(Fichier, OrdresContent);
          CloseFile(Fichier);
          ShowMessage('Ordres r�cup�r�s et stock�s avec succ�s');
        end
        else
        begin
          ShowMessage('Erreur analyse des ordres : TJSONObject attendu');
        end;
      finally
        CheckOrdres;
      end;
  end;
  finally
  Auth.Free;
  RESTClient.Free;
  RESTRequest.Free;

  end;

end;

procedure TNebForm.RecupOrdresButtonClick(Sender: TObject);
begin
  ShowMessage('Je vais r�cup�rer les ordres sur le serveur');
end;

procedure TNebForm.RefaireEvalIdentiqueExecute(Sender: TObject);
var
  theData, DataNBT : TNebData;
  jou, j2 : integer;
  md, fl{, tr} : integer;
  log : TextFile;
  OldX, OldY : integer;
begin
  if not isDataAssigned then
    Exit;
  for jou := 1 to Data.NbJou do if not FileExists(Format('%s%s%.2d%.2d.nbt', [ExtractFilePath(Data.FileName), NomPartie, jou, Tour])) then
  begin
    ShowMessage(format('Impossible de trouver le fichier %s', [Format('%s%s%.2d%.2d.nbt', [ExtractFilePath(Data.FileName), NomPartie, jou, Tour])]));
    Exit;
  end;
  if not FileExists(GetFileNameTour(Tour)) then
  begin
    ShowMessage(format('Impossible de trouver le fichier %s', [GetFileNameTour(Tour)]));
    Exit;
  end;
  MessageDlg('Pour refaire une �val identique N�bula va'+#13+#10+
             'utiliser l'+#39+'ensemble des fichiers NBT ainsi que'+#13+#10+
             'le fichier NBA issu d'+#39+'une r�eval (non identique).'+#13+#10+#13+#10+
             'N�bula va alors utiliser les informations de ces'+#13+#10+
             'fichiers pour g�n�rer un fichier NBA correspondant'+#13+#10+
             '� la nouvelle Eval.'+#13+#10+#13+#10+
             'Afin de ne pas d�truire le fichier NBA courant'+#13+#10+
             'ce fichier sera plac� dans le sous-r�pertoire'+#13+#10+
             '"R��val".',mtInformation,[mbOK],0);

  // On l'ouvre l'�val � modifier en fonction des NBT

  SysUtils.ForceDirectories(Format('%sR��val\', [ExtractFilePath(Data.FileName)]));
  AssignFile(log, Format('%sR��val\log.txt', [ExtractFilePath(Data.FileName)]));
  Rewrite(log);

  theData := TNebData.Create(GetFileNameTour(Tour), Tour);
  theData.RenameFile(Format('%sR��val\%s%.2d.nbt', [ExtractFilePath(Data.FileName), NomPartie, Tour]));

  // Maintenant on boucle sur les NBT
  for jou := 1 to theData.NbJou do
  begin
    DataNBT := TNebData.Create(Format('%s%s%.2d%.2d.nbt', [ExtractFilePath(Data.FileName), NomPartie, jou, Tour]), Tour);

    theData.DetailScores[jou] := DataNBT.DetailScores[jou];
    theData.Jou[jou].Score := DataNBT.Jou[jou].Score;
    theData.Contact[jou] := DataNBT.Contact[jou];
    for j2 := 1 to theData.NbJou do
      theData.ConnuDeNom[jou, j2] := DataNBT.ConnuDeNom[jou, j2];

    for md := 1 to theData.NbMonde do
    begin
      theData.Connu[md, jou] := DataNBT.Connu[md, jou];
      if DataNBT.EstConnu(md, jou) then
      begin
        OldX := theData.M[md].X;
        OldY := theData.M[md].Y;
        theData.M[md] := DataNBT.M[md];
        theData.M[md].X := OldX;
        theData.M[md].Y := OldY;
        theData.ME[md] := DataNBT.ME[md];
      end;
    end;

    for fl := 1 to theData.NbFlotte do
      if (DataNBT.F[fl].Proprio = jou) or
         ((DataNBT.F[fl].Proprio = 0) and (DataNBT.FE[fl].AncienProprio = jou)) then
    begin
      if (DataNBT.F[fl].Localisation <> theData.F[fl].Localisation) or
         (DataNBT.F[fl].NbVC <> theData.F[fl].NbVC) or
         (DataNBT.F[fl].NbVT <> theData.F[fl].NbVT) or
         (DataNBT.F[fl].Proprio <> theData.F[fl].Proprio) then
      begin
        Writeln(log, Format('Intervention pour la flotte %d (M_%d, M_%d)', [fl, theData.F[fl].Localisation, DataNBT.F[fl].Localisation]));
        theData.F[fl] := DataNBT.F[fl];
        theData.FE[fl] := DataNBT.FE[fl];
      end;
    end;

    // On remet La Chose � sa place
    theData.T[200] := DataNBT.T[200];

    DataNBT.Free;
  end;

  CloseFile(log);
  theData.SauveenNBT(0, '');

  PlanForm := TPlanForm.Create2(Self, theData, PlanSansCR);
  PlanForm.Show;

  theData.Free;
  ShowMessage('Tour modifi� et sauv� dans le r�pertoire R��val');
end;


procedure TNebForm.LockCheckClick(Sender: TObject);
begin
  if Data.Tour = Tour then
    Data.SetLock(LockCheck.Checked, Tour);
end;

procedure TNebForm.ToolButton6MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ShiftState := Shift;
end;

procedure TNebForm.AfficherleplanF71Click(Sender: TObject);
begin
  ShiftState := [];
  AfficherPlanClic;
end;

procedure TNebForm.AfficherPlanActionExecute(Sender: TObject);
begin
  AfficherPlanClic;
end;

procedure TNebForm.OuvrirPartieExecute(Sender: TObject);
begin
  OuvrePartieDialog.InitialDir := RepFic;
  if OuvrePartieDialog.execute then
  begin
    if FileExists(OuvrePartieDialog.FileName) then
      OuvreFic(OuvrePartieDialog.FileName);
  end;
  ToolBar1.Invalidate;
end;

end.
