unit CalcStat;

interface

uses Avatunit, NebData;

type
  TStatParFunc = function(var Data : TNebData; Jou : byte) : integer;
  //TStatParEvtFunc = function(var DataPar : PPartie; Evt : PEvt; Jou : byte) : integer;

function CalcPoints(var Data : TNebData; Jou : byte) : integer;
function CalcVaiss(var Data : TNebData; Jou : byte) : integer;
function CalcVC(var Data : TNebData; Jou : byte) : integer;
function CalcVT(var Data : TNebData; Jou : byte) : integer;
function CalcMondes(var Data : TNebData; Jou : byte) : integer;
function CalcFlottes(var Data : TNebData; Jou : byte) : integer;
function CalcTresors(var Data : TNebData; Jou : byte) : integer;
function CalcInd(var Data : TNebData; Jou : byte) : integer;
function CalcIndActives(var Data : TNebData; Jou : byte) : integer;

//function CalcIndAct(var Data : TNebData; Evt : PEvt; Jou : byte) : integer;
function CalcMP(var Data : TNebData; Jou : byte) : integer;
function CalcCapMin(var Data : TNebData; Jou : byte) : integer;
function CalcPop(var Data : TNebData; Jou : byte) : integer;
function CalcConv(var Data : TNebData; Jou : byte) : integer;
function CalcRob(var Data : TNebData; Jou : byte) : integer;
function CalcTech(var Data : TNebData; Jou : byte) : integer;
function CalcVI(var Data : TNebData; Jou : byte) : integer;
function CalcVP(var Data : TNebData; Jou : byte) : integer;
function CalcVProtec(var Data : TNebData; Jou : byte) : integer;
function CalcNConv(var Data : TNebData; Jou : byte) : integer;
function CalcMPTran(var Data : TNebData; Jou : byte) : integer;
function CalcNConvTran(var Data : TNebData; Jou : byte) : integer;
function CalcConvTran(var Data : TNebData; Jou : byte) : integer;
function CalcRobTran(var Data : TNebData; Jou : byte) : integer;
function CalcPopTran(var Data : TNebData; Jou : byte) : integer;
function CalcCargaison(var Data : TNebData; Jou : byte) : integer;
function CalcDEP(var Data : TNebData; Jou : byte) : integer;
function CalcATT(var Data : TNebData; Jou : byte) : integer;
function CalcDEF(var Data : TNebData; Jou : byte) : integer;
function CalcRAD(var Data : TNebData; Jou : byte) : integer;
function CalcCAR(var Data : TNebData; Jou : byte) : integer;
function CalcALI(var Data : TNebData; Jou : byte) : integer;

implementation

function CalcPoints(var Data : TNebData; Jou : byte) : integer;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].Score;
end;

function CalcVaiss(var Data : TNebData; Jou : byte) : integer;
begin
  Result := CalcVC(Data, Jou) + CalcVT(Data, Jou);
end;

function CalcVC(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].VI + Data.M[i].VP);

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].NbVC);
end;

function CalcVT(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].NbVT);
end;

function CalcMondes(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
   { Calcule le nombre de mondes du joueur }

   Result := 0;

   for i := 1 to Data.NbMonde do
     if (Data.M[i].Proprio = Jou) or (Jou = 0) then
       inc(Result);
end;

function CalcFlottes(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := 0;

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result);
end;

function CalcInd(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].Ind);
end;

function CalcIndActives(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.ME[i].IndLibre);
end;

function CalcIndAct(var Data : TNebData; Evt : PEvt; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Evt^.ME[i].IndLibre);
end;

function CalcMP(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].MP);
end;

function CalcCapMin(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].PlusMP);
end;

function CalcPop(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].Pop);

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].N + Data.F[i].C);
end;

function CalcConv(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].ProprConv = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].Conv);

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].C);
end;

function CalcRob(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].Robot);

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0)then
      Inc(Result, Data.F[i].R);
end;

function CalcTech(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].DEP + Data.Jou[Jou].ATT + Data.Jou[Jou].DEF +
              Data.Jou[Jou].RAD + Data.Jou[Jou].CAR + Data.Jou[Jou].ALI;
end;

function CalcVI(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].VI);
end;

function CalcVP(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].VP);
end;

function CalcVProtec(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;

  for i := 1 to Data.NbMonde do
    if (Data.M[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.M[i].VI + Data.M[i].VP);
end;

function CalcNConv(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := CalcPop(Data, Jou) - CalcConv(Data, Jou);
end;

function CalcMPTran(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := 0;

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].MP);
end;

function CalcConvTran(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := 0;

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].C);
end;

function CalcNConvTran(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := 0;

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].N);
end;

function CalcRobTran(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := 0;

  for i := 1 to Data.NbFlotte do
    if (Data.F[i].Proprio = Jou) or (Jou = 0) then
      Inc(Result, Data.F[i].R);
end;

function CalcPopTran(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := CalcConvTran(Data, Jou) + CalcNConvTran(Data, Jou);
end;

function CalcCargaison(var Data : TNebData; Jou : byte) : integer;
var
  i : word;
begin
  Result := CalcConvTran(Data, Jou) + CalcNConvTran(Data, Jou) +
            CalcRobTran(Data, Jou) + calcMPTran(Data, Jou);
end;

function CalcDEP(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].DEP;
end;

function CalcATT(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].ATT;
end;

function CalcDEF(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].DEF;
end;

function CalcRAD(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].RAD;
end;

function CalcCAR(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].CAR;
end;

function CalcALI(var Data : TNebData; Jou : byte) : integer;
var
   i : word;
begin
  Result := 0;
  if Jou = 0 then
  else
    Result := Data.Jou[Jou].ALI;
end;

function CalcTresors(var Data : TNebData; Jou : byte) : integer;
var
  tres : word;
begin
  Result := 0;
  for tres := 1 to NBTRES do if (Data.T[tres].Proprio = Jou) or (Jou = 0) then
    inc(Result);
end;

end.
