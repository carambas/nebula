�
 TRENDUSDIALOG 0  TPF0TRendusDialogRendusDialogLeft�Top� ActiveControlOKBtnBorderStylebsDialogCaptionOrdres rendusClientHeight=ClientWidthgColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthiHeightCaptionOrdres non rendus  TLabelLabel2Left� TopWidthPHeightCaptionOrdres rendus  TLabelLabel3LeftTop� WidthIHeight9AutoSizeCaption�   Cette boite de dialgue vous permet de connaitre les joueurs qui ont rendu ou non leurs ordres. Il faut d'abord faire une évaluation pour que ce qui s'affiche ici soit représentatif.WordWrap	  TBitBtnOKBtnLeft� TopWidthMHeightKindbkOKMargin	NumGlyphsSpacing�TabOrder 	IsControl	  TListBoxPasRenduListBoxLeftTop Width� Height� 
ItemHeightTabOrder  TListBoxRenduListBoxLeft� Top Width� Height� 
ItemHeightTabOrder   