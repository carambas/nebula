unit ConfigReseau;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms;

type
  TConfigReseauForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    NomEdit: TEdit;
    EmailEdit: TEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    SMTPEdit: TEdit;
  end;

var
  ConfigReseauForm: TConfigReseauForm;

implementation

{$R *.DFM}

end.
