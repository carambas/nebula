program nebula4;

uses
  Forms,
  paraform in 'paraform.pas' {Param},
  ordreserreurs in 'ordreserreurs.pas' {ErreursOrdresForm},
  ORDRREND in 'ORDRREND.PAS' {RendusDialog},
  modif in 'modif.pas' {Dialog},
  about in 'about.pas' {APropos},
  listppap in 'listppap.pas' {ListingToClipboard},
  stat in 'stat.pas' {StatForm},
  selecjou in 'selecjou.pas' {SelectJouDlg},
  calcstat in 'calcstat.pas',
  ordrjou in 'ordrjou.pas' {SaisieOrdresJouForm},
  visu in 'visu.pas' {VisuTexte},
  saisordr in 'saisordr.pas' {SaisieOrdresForm},
  configreseau in 'configreseau.pas' {ConfigReseauForm},
  nebmain in 'nebmain.pas' {NebForm},
  avatunit in '..\shared\avatunit.pas',
  winplan in '..\shared\plan\winplan.pas',
  plan in '..\shared\plan\plan.pas' {PlanForm},
  prtpform in '..\shared\prtform\prtpform.pas' {PrintPlanDlg},
  avateval in '..\shared-not-distr\eval\avateval.pas',
  emigrati in '..\shared-not-distr\eval\emigrati.pas',
  combats in '..\shared-not-distr\eval\combats.pas',
  apipoints in '..\shared-not-distr\eval\apipoints.pas',
  anaordre in '..\shared\ordres\anaordre.pas',
  lectordres in '..\shared\ordres\lectordres.pas',
  impmonde in '..\shared\plan\impmonde.pas' {ImpMondeForm},
  chemin in '..\shared\chemin.pas',
  nebdata in '..\shared\nebdata.pas',
  histo in '..\shared\plan\histo.pas' {HistoForm},
  listint in '..\shared\listint.pas',
  imprime in '..\shared\imprime.pas',
  nbtstringlist in '..\shared\nbtstringlist.pas',
  evaldlg in '..\shared\eval\evaldlg.pas' {EvalForm},
  special in '..\shared-not-distr\eval\special.pas',
  genmail in '..\shared\genmail.pas',
  envoimail in '..\shared\envoimail.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.HelpFile := 'Nebula.hlp';
  Application.Title := 'N�bula';
  Application.CreateForm(TNebForm, NebForm);
  Application.CreateForm(TParam, Param);
  Application.CreateForm(TAPropos, APropos);
  Application.CreateForm(TConfigReseauForm, ConfigReseauForm);
  Application.CreateForm(TPrintPlanDlg, PrintPlanDlg);
  Application.CreateForm(TImpMondeForm, ImpMondeForm);
  Application.CreateForm(TEvalForm, EvalForm);
  Application.Run;
end.
