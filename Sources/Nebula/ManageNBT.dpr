library ManageNBT;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  NebData in '..\Shared\NebData.pas',
  Imprime in '..\Shared\IMPRIME.PAS',
  Evaldlg in '..\Shared\Eval\EVALDLG.PAS' {EvalForm};

function OuvreNBT(NomFic : PChar) : Pointer; stdcall;
var
  Data : TNebData;
begin
  result := nil;
  if not FileExists(NomFic) then
    Exit;
  Data := TNebData.Create(NomFic, 0);
  Result := Data;
end;

procedure AfficheMonde(theData : Pointer; NoMonde : integer; NomFicTempo : PChar); stdcall;
var
  Data : TNebData;
begin
  Data := TNebData(theData);

  AssignFile(Lst, NomFicTempo);
  ReWrite(Lst);
  Imprime.AfficheMonde(Data, nil, Data.NoJou, NoMonde);
  CloseFile(Lst);
end;

procedure FermeNBT(theData : pointer); stdcall;
var
  Data : TNebData;
begin
  Data := TNebData(theData);
  Data.Free;
end;

exports OuvreNBT, AfficheMonde, FermeNBT;

begin
end.
