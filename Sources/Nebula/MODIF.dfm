object Dialog: TDialog
  Left = 231
  Top = 146
  HelpContext = 1008
  BorderStyle = bsDialog
  Caption = 'Modifier le tour courant'
  ClientHeight = 474
  ClientWidth = 651
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 10
    Top = 8
    Width = 631
    Height = 414
    ActivePage = TabSheetMonde
    DockSite = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object TabSheetMonde: TTabSheet
      Caption = 'Monde'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      object Bevel1: TBevel
        Left = -4
        Top = 3
        Width = 380
        Height = 155
      end
      object Bevel4: TBevel
        Left = 404
        Top = 8
        Width = 92
        Height = 49
      end
      object Label1: TLabel
        Left = 7
        Top = 20
        Width = 61
        Height = 13
        Caption = 'Connexions :'
      end
      object Label3: TLabel
        Left = 7
        Top = 48
        Width = 59
        Height = 13
        Caption = 'Propri'#233'taire :'
      end
      object Label4: TLabel
        Left = 201
        Top = 48
        Width = 125
        Height = 13
        Caption = 'Propri'#233'taire des convertis :'
      end
      object Label5: TLabel
        Left = 7
        Top = 75
        Width = 35
        Height = 13
        Caption = 'Dur'#233'e :'
      end
      object Label6: TLabel
        Left = 136
        Top = 75
        Width = 22
        Height = 13
        Caption = 'MP :'
      end
      object Label7: TLabel
        Left = 226
        Top = 75
        Width = 34
        Height = 13
        Caption = '(+MP) :'
      end
      object Label8: TLabel
        Left = 305
        Top = 77
        Width = 20
        Height = 13
        Caption = 'PC='
      end
      object Label9: TLabel
        Left = 8
        Top = 132
        Width = 13
        Height = 13
        Caption = 'P='
      end
      object Label10: TLabel
        Left = 77
        Top = 132
        Width = 13
        Height = 13
        Caption = 'C='
      end
      object Label11: TLabel
        Left = 142
        Top = 132
        Width = 14
        Height = 13
        Caption = 'R='
      end
      object Label12: TLabel
        Left = 209
        Top = 132
        Width = 51
        Height = 13
        Caption = '(MaxPop) :'
      end
      object Label13: TLabel
        Left = 305
        Top = 132
        Width = 20
        Height = 13
        Caption = 'VP :'
      end
      object Label14: TLabel
        Left = 7
        Top = 103
        Width = 51
        Height = 13
        Caption = 'Industries :'
      end
      object Label15: TLabel
        Left = 142
        Top = 103
        Width = 16
        Height = 13
        Caption = 'VI :'
      end
      object Label16: TLabel
        Left = 238
        Top = 103
        Width = 15
        Height = 13
        Caption = 'Pi='
      end
      object Label17: TLabel
        Left = 311
        Top = 103
        Width = 4
        Height = 16
        Caption = '/'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'System'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NomIndex: TLabel
        Left = 404
        Top = 87
        Width = 48
        Height = 13
        Caption = 'NomIndex'
      end
      object Label2: TLabel
        Left = 396
        Top = 115
        Width = 58
        Height = 13
        Caption = 'NbExploLoc'
      end
      object LabelNoMonde: TLabel
        Left = 459
        Top = 148
        Width = 59
        Height = 13
        Caption = 'No. Monde :'
      end
      object Connexion1: TEdit
        Left = 71
        Top = 16
        Width = 34
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        OnChange = Connexion1Change
      end
      object Connexion3: TEdit
        Left = 146
        Top = 16
        Width = 34
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
        OnChange = Connexion3Change
      end
      object Connexion4: TEdit
        Left = 183
        Top = 16
        Width = 35
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 2
        OnChange = Connexion4Change
      end
      object Connexion5: TEdit
        Left = 221
        Top = 16
        Width = 34
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 3
        OnChange = Connexion5Change
      end
      object Connexion6: TEdit
        Left = 259
        Top = 16
        Width = 34
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 4
        OnChange = Connexion6Change
      end
      object Connexion8: TEdit
        Left = 334
        Top = 16
        Width = 34
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 5
        OnChange = Connexion8Change
      end
      object Connexion7: TEdit
        Left = 296
        Top = 16
        Width = 35
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 6
        OnChange = Connexion7Change
      end
      object Connexion2: TEdit
        Left = 108
        Top = 16
        Width = 35
        Height = 19
        Hint = 'Connexion '#224' un autre monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 7
        OnChange = Connexion2Change
      end
      object Duree: TEdit
        Left = 71
        Top = 71
        Width = 34
        Height = 19
        Hint = 'Nombre de tours depuis la capture du monde par son propi'#233'taire'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 8
        OnChange = DureeChange
      end
      object MP: TEdit
        Left = 162
        Top = 71
        Width = 35
        Height = 19
        Hint = 'Nombre de MP en stock'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 9
        OnChange = MPChange
      end
      object PlusMP: TEdit
        Left = 265
        Top = 71
        Width = 34
        Height = 19
        Hint = 'Capacit'#233's mini'#232'res'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 10
        OnChange = PlusMPChange
      end
      object PC: TEdit
        Left = 334
        Top = 71
        Width = 34
        Height = 19
        Hint = 'Nombre de fois que des PC ont '#233't'#233' d'#233'charg'#233's'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 11
        OnChange = PCChange
      end
      object Proprio: TEdit
        Left = 71
        Top = 44
        Width = 34
        Height = 19
        Hint = 'No. du propi'#233'taire du monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 12
        OnChange = ProprioChange
      end
      object ProprioConv: TEdit
        Left = 334
        Top = 44
        Width = 34
        Height = 19
        Hint = 'No. du propi'#233'taire des convertis'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 13
        OnChange = ProprioConvChange
      end
      object Pop: TEdit
        Left = 29
        Top = 128
        Width = 34
        Height = 19
        Hint = 'Nombre de populations'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 14
        OnChange = PopChange
      end
      object Conv: TEdit
        Left = 96
        Top = 128
        Width = 35
        Height = 19
        Hint = 'Nombre de convertis'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 15
        OnChange = ConvChange
      end
      object Rob: TEdit
        Left = 162
        Top = 128
        Width = 35
        Height = 19
        Hint = 'Nombre de robots'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 16
        OnChange = RobChange
      end
      object MaxPop: TEdit
        Left = 265
        Top = 128
        Width = 34
        Height = 19
        Hint = 'Population maximale du monde'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 17
        OnChange = MaxPopChange
      end
      object VP: TEdit
        Left = 334
        Top = 128
        Width = 34
        Height = 19
        Hint = 'Nombre de VP'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 18
        OnChange = VPChange
      end
      object Industries: TEdit
        Left = 71
        Top = 99
        Width = 34
        Height = 19
        Hint = 'Nombre d'#39'industries'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 19
        OnChange = IndustriesChange
      end
      object VI: TEdit
        Left = 162
        Top = 99
        Width = 35
        Height = 19
        Hint = 'Nombre de VI'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 20
        OnChange = VIChange
      end
      object NbPille: TEdit
        Left = 265
        Top = 99
        Width = 34
        Height = 19
        Hint = 'Nombre de fois que le monde a '#233't'#233' pill'#233
        ParentShowHint = False
        ShowHint = False
        TabOrder = 21
        OnChange = NbPilleChange
      end
      object TourPille: TEdit
        Left = 334
        Top = 99
        Width = 34
        Height = 19
        Hint = 'Nombre de tour avant la r'#233'cup'#233'ration du dernier pillage'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 22
        OnChange = TourPilleChange
      end
      object TNoir: TCheckBox
        Left = 414
        Top = 12
        Width = 78
        Height = 17
        Caption = 'T. Noir'
        TabOrder = 23
        OnClick = TNoirClick
      end
      object BOMBE: TCheckBox
        Left = 414
        Top = 32
        Width = 80
        Height = 17
        Caption = 'Bomb'#233
        TabOrder = 24
        OnClick = BOMBEClick
      end
      object NomIndexEdit: TEdit
        Left = 459
        Top = 83
        Width = 120
        Height = 19
        TabOrder = 25
        Text = 'NomIndexEdit'
        OnChange = NomIndexEditChange
      end
      object NbExploLocEdit: TEdit
        Left = 459
        Top = 111
        Width = 120
        Height = 19
        TabOrder = 26
        Text = 'NbExploLocEdit'
        OnChange = NbExploLocEditChange
      end
    end
    object TabSheetEvenements: TTabSheet
      Caption = #201'v'#233'n'#233'ments'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object Label20: TLabel
        Left = 459
        Top = 148
        Width = 59
        Height = 13
        Caption = 'No. Monde :'
      end
      object GroupBox1: TGroupBox
        Left = 218
        Top = 4
        Width = 106
        Height = 88
        Caption = 'Statut'
        TabOrder = 0
        object MondeCapture: TCheckBox
          Left = 8
          Top = 18
          Width = 96
          Height = 17
          Caption = 'Captur'#233
          TabOrder = 0
          OnClick = MondeCaptureClick
        end
        object MondePille: TCheckBox
          Left = 8
          Top = 40
          Width = 96
          Height = 17
          TabStop = False
          Caption = 'Pill'#233
          TabOrder = 1
          OnClick = MondePilleClick
        end
        object MondeOffert: TCheckBox
          Left = 8
          Top = 61
          Width = 96
          Height = 18
          TabStop = False
          Caption = 'Offert'
          TabOrder = 2
          OnClick = MondeOffertClick
        end
      end
      object GroupBox2: TGroupBox
        Left = 328
        Top = 4
        Width = 156
        Height = 128
        TabOrder = 1
        object Label21: TLabel
          Left = 11
          Top = 17
          Width = 94
          Height = 13
          Caption = 'Ancien propri'#233'taire :'
        end
        object Label22: TLabel
          Left = 27
          Top = 45
          Width = 78
          Height = 13
          Caption = 'Industries libres :'
        end
        object Label23: TLabel
          Left = 17
          Top = 73
          Width = 88
          Height = 13
          Caption = 'Populations libres :'
        end
        object Label24: TLabel
          Left = 32
          Top = 102
          Width = 73
          Height = 13
          Caption = 'PC d'#233'charg'#233's :'
        end
        object MondeAncienProprio: TEdit
          Left = 109
          Top = 13
          Width = 35
          Height = 19
          Hint = 'Propri'#233'taire du monde au tour pr'#233'c'#233'dent'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          OnChange = MondeAncienProprioChange
        end
        object IndLibres: TEdit
          Left = 109
          Top = 41
          Width = 35
          Height = 19
          Hint = 'Industries pouvant produire'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 1
          OnChange = IndLibresChange
        end
        object PopLibres: TEdit
          Left = 109
          Top = 69
          Width = 35
          Height = 19
          Hint = 'Populations pouvant aider les industries '#224' produire'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
          OnChange = PopLibresChange
        end
        object PCDech: TEdit
          Left = 109
          Top = 98
          Width = 35
          Height = 19
          Hint = 'Nombre de PC d'#233'charg'#233's '#224' ce tour'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 3
          OnChange = PCDechChange
        end
      end
      object GroupBox3: TGroupBox
        Left = 218
        Top = 132
        Width = 233
        Height = 44
        Caption = 'Populations mortes'
        TabOrder = 2
        object Label25: TLabel
          Left = 13
          Top = 19
          Width = 13
          Height = 13
          Caption = 'P :'
        end
        object Label26: TLabel
          Left = 81
          Top = 19
          Width = 13
          Height = 13
          Caption = 'C :'
        end
        object Label27: TLabel
          Left = 160
          Top = 19
          Width = 14
          Height = 13
          Caption = 'R :'
        end
        object PopMortes: TEdit
          Left = 32
          Top = 16
          Width = 41
          Height = 19
          Hint = 'Populations mortes '#224' ce tour'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          OnChange = PopMortesChange
        end
        object ConvMorts: TEdit
          Left = 105
          Top = 15
          Width = 47
          Height = 19
          Hint = 'Convertis morts '#224' ce tour'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 1
          OnChange = ConvMortsChange
        end
        object RobMorts: TEdit
          Left = 182
          Top = 15
          Width = 41
          Height = 19
          Hint = 'Robots morts '#224' ce tour'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
          OnChange = RobMortsChange
        end
      end
      object GroupBox6: TGroupBox
        Left = 108
        Top = 4
        Width = 107
        Height = 172
        Caption = 'Action VP'
        TabOrder = 3
        object Label19: TLabel
          Left = 18
          Top = 145
          Width = 40
          Height = 13
          Caption = 'Cible VP'
        end
        object VPRien: TRadioButton
          Left = 6
          Top = 18
          Width = 69
          Height = 17
          Caption = 'Rien'
          TabOrder = 0
          TabStop = True
          OnClick = VPRienClick
        end
        object VPTireC: TRadioButton
          Left = 6
          Top = 38
          Width = 49
          Height = 17
          Caption = '*C'
          TabOrder = 2
          OnClick = VPTireCClick
        end
        object VPTireN: TRadioButton
          Left = 6
          Top = 57
          Width = 67
          Height = 18
          Caption = '*N'
          TabOrder = 3
          OnClick = VPTireNClick
        end
        object VPTireF: TRadioButton
          Left = 6
          Top = 79
          Width = 55
          Height = 16
          Caption = '*F_f'
          TabOrder = 4
          OnClick = VPTireFClick
        end
        object VPEmbuscade: TRadioButton
          Left = 6
          Top = 99
          Width = 65
          Height = 17
          Caption = '**'
          TabOrder = 5
          OnClick = VPEmbuscadeClick
        end
        object CibleVP: TEdit
          Left = 63
          Top = 142
          Width = 38
          Height = 19
          Hint = 'No. de la flotte victime du tir'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 1
          OnChange = CibleVPChange
        end
      end
      object GroupBox4: TGroupBox
        Left = 2
        Top = 4
        Width = 103
        Height = 172
        Caption = 'Action VI'
        TabOrder = 4
        object Label28: TLabel
          Left = 14
          Top = 145
          Width = 36
          Height = 13
          Caption = 'Cible VI'
        end
        object VIRien: TRadioButton
          Left = 4
          Top = 20
          Width = 79
          Height = 17
          Caption = 'Rien'
          TabOrder = 0
          TabStop = True
          OnClick = VIRienClick
        end
        object VITireF: TRadioButton
          Left = 4
          Top = 40
          Width = 75
          Height = 17
          Caption = '*F_f'
          TabOrder = 2
          OnClick = VITireFClick
        end
        object VIEmbuscade: TRadioButton
          Left = 4
          Top = 59
          Width = 80
          Height = 18
          Caption = '**'
          TabOrder = 3
          OnClick = VIEmbuscadeClick
        end
        object CibleVI: TEdit
          Left = 55
          Top = 143
          Width = 42
          Height = 19
          Hint = 'No. de la flotte victime du tir'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 1
          OnChange = CibleVIChange
        end
      end
    end
    object TabSheetVisibilite: TTabSheet
      Caption = 'Visibilit'#233
      ImageIndex = 2
      object LabelListBox1: TLabel
        Left = 135
        Top = 16
        Width = 66
        Height = 13
        Caption = 'LabelListBox1'
      end
      object LabelListBox2: TLabel
        Left = 317
        Top = 16
        Width = 66
        Height = 13
        Caption = 'LabelListBox2'
      end
      object Label70: TLabel
        Left = 459
        Top = 148
        Width = 59
        Height = 13
        Caption = 'No. Monde :'
      end
      object Label72: TLabel
        Left = 8
        Top = 16
        Width = 84
        Height = 13
        Caption = 'Type de visibilit'#233' :'
      end
      object Label74: TLabel
        Left = 8
        Top = 63
        Width = 32
        Height = 13
        Caption = 'Joueur'
      end
      object ListBox1: TListBox
        Left = 135
        Top = 32
        Width = 136
        Height = 151
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 0
      end
      object ListBox2: TListBox
        Left = 317
        Top = 32
        Width = 136
        Height = 151
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 1
      end
      object VisibiliteCombo: TComboBox
        Left = 8
        Top = 32
        Width = 120
        Height = 21
        Style = csDropDownList
        TabOrder = 2
        OnChange = VisibiliteComboChange
        Items.Strings = (
          '[M] Vu par'
          '[M] Connexions vue par'
          '[M] Coordonn'#233'es vues par'
          '[J] Alli'#233's'
          '[J] Chargeurs'
          '[J] Joueurs connus'
          '[J] Joueurs en contact')
      end
      object DroiteBouton: TButton
        Left = 281
        Top = 71
        Width = 25
        Height = 25
        Caption = '->'
        TabOrder = 3
        OnClick = DroiteBoutonClick
      end
      object GaucheBouton: TButton
        Left = 281
        Top = 119
        Width = 25
        Height = 25
        Caption = '<-'
        TabOrder = 4
        OnClick = GaucheBoutonClick
      end
      object JoueurCombo: TComboBox
        Left = 8
        Top = 79
        Width = 120
        Height = 21
        Style = csDropDownList
        TabOrder = 5
        OnChange = JoueurComboChange
      end
    end
    object TabSheetFlottes: TTabSheet
      Caption = 'Flottes'
      ImageIndex = 3
      object Label29: TLabel
        Left = 459
        Top = 148
        Width = 53
        Height = 13
        Caption = 'No. Monde'
      end
      object Label31: TLabel
        Left = 459
        Top = 92
        Width = 46
        Height = 13
        Caption = 'No. Flotte'
      end
      object Bevel9: TBevel
        Left = 121
        Top = 154
        Width = 332
        Height = 32
      end
      object Label38: TLabel
        Left = 126
        Top = 162
        Width = 35
        Height = 13
        Caption = 'Chemin'
      end
      object GroupBox8: TGroupBox
        Left = 244
        Top = 0
        Width = 209
        Height = 152
        Caption = 'Divers'
        TabOrder = 0
        object Label33: TLabel
          Left = 49
          Top = 15
          Width = 50
          Height = 13
          Caption = 'Propi'#233'taire'
        end
        object Label34: TLabel
          Left = 11
          Top = 38
          Width = 88
          Height = 13
          Caption = 'Ancien propri'#233'taire'
        end
        object Label35: TLabel
          Left = 43
          Top = 61
          Width = 56
          Height = 13
          Caption = 'Localisation'
        end
        object Label36: TLabel
          Left = 84
          Top = 84
          Width = 14
          Height = 13
          Caption = 'VC'
        end
        object Label37: TLabel
          Left = 5
          Top = 131
          Width = 16
          Height = 13
          Caption = 'MP'
        end
        object AllerPosFlotte: TSpeedButton
          Left = 154
          Top = 55
          Width = 24
          Height = 26
          Hint = 'Aller o'#249' se trouve la flotte'
          Glyph.Data = {
            7E010000424D7E01000000000000760000002800000016000000160000000100
            0400000000000801000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
            FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFF
            FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF0FFFFFFFFF00FFFFFFFFFFFF00FFFFFF
            FF00FFFFFFFFFFFF000FFFFFFF00FFFFFFFFFFFF0000FFFFFF00FF0000000000
            00000FFFFF00FF0000000000000000FFFF00FF00000000000000000FFF00FF00
            0000000000000000FF00FF00000000000000000FFF00FF0000000000000000FF
            FF00FF000000000000000FFFFF00FFFFFFFFFFFF0000FFFFFF00FFFFFFFFFFFF
            000FFFFFFF00FFFFFFFFFFFF00FFFFFFFF00FFFFFFFFFFFF0FFFFFFFFF00FFFF
            FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
            FF00}
          Layout = blGlyphRight
          ParentShowHint = False
          ShowHint = True
          OnClick = AllerPosFlotteClick
        end
        object Label53: TLabel
          Left = 84
          Top = 107
          Width = 14
          Height = 13
          Caption = 'VT'
        end
        object Label54: TLabel
          Left = 67
          Top = 131
          Width = 7
          Height = 13
          Caption = 'P'
        end
        object Label55: TLabel
          Left = 115
          Top = 131
          Width = 7
          Height = 13
          Caption = 'C'
        end
        object Label56: TLabel
          Left = 162
          Top = 131
          Width = 8
          Height = 13
          Caption = 'R'
        end
        object FlotteProprio: TEdit
          Left = 111
          Top = 11
          Width = 33
          Height = 19
          Hint = 'No. du propri'#233'taire'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          OnChange = FlotteProprioChange
        end
        object FlotteAncienProprio: TEdit
          Left = 111
          Top = 34
          Width = 33
          Height = 19
          Hint = 'No. du propri'#233'taire au tour pr'#233'c'#233'dent'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 1
          OnChange = FlotteAncienProprioChange
        end
        object FlotteLocalisation: TEdit
          Left = 111
          Top = 57
          Width = 33
          Height = 19
          Hint = 'No. du monde o'#249' se trouve la flotte'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
          OnChange = FlotteLocalisationChange
        end
        object FlotteVC: TEdit
          Left = 111
          Top = 81
          Width = 33
          Height = 19
          Hint = 'Vaisseaux transport'#233's par la flotte'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 3
          OnChange = FlotteVCChange
        end
        object FlotteMP: TEdit
          Left = 26
          Top = 127
          Width = 33
          Height = 19
          Hint = 'MP transport'#233'es par la flotte'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 5
          OnChange = FlotteMPChange
        end
        object FlotteVT: TEdit
          Left = 111
          Top = 103
          Width = 33
          Height = 19
          TabOrder = 4
          OnChange = FlotteVTChange
        end
        object FlotteP: TEdit
          Left = 79
          Top = 127
          Width = 25
          Height = 19
          TabOrder = 6
          OnChange = FlottePChange
        end
        object FlotteC: TEdit
          Left = 127
          Top = 127
          Width = 25
          Height = 19
          TabOrder = 7
          OnChange = FlotteCChange
        end
        object FlotteR: TEdit
          Left = 174
          Top = 127
          Width = 25
          Height = 19
          TabOrder = 8
          OnChange = FlotteRChange
        end
      end
      object GroupBox5: TGroupBox
        Left = 2
        Top = 0
        Width = 110
        Height = 187
        Caption = 'Statut'
        TabOrder = 1
        object FlotteEnPaix: TCheckBox
          Left = 6
          Top = 29
          Width = 96
          Height = 17
          Caption = 'En paix'
          TabOrder = 0
          OnClick = FlotteEnPaixClick
        end
        object FlotteBombe: TCheckBox
          Left = 6
          Top = 49
          Width = 96
          Height = 16
          TabStop = False
          Caption = 'BOMBE'
          TabOrder = 1
          OnClick = FlotteBombeClick
        end
        object FlotteCapturee: TCheckBox
          Left = 6
          Top = 69
          Width = 96
          Height = 17
          TabStop = False
          Caption = 'Captur'#233'e'
          TabOrder = 2
          OnClick = FlotteCaptureeClick
        end
        object FlottePiratee: TCheckBox
          Left = 6
          Top = 89
          Width = 96
          Height = 17
          TabStop = False
          Caption = 'Pirat'#233'e'
          TabOrder = 3
          OnClick = FlottePirateeClick
        end
        object FlotteOfferte: TCheckBox
          Left = 6
          Top = 109
          Width = 96
          Height = 17
          TabStop = False
          Caption = 'Offerte'
          TabOrder = 4
          OnClick = FlotteOfferteClick
        end
        object FlotteEmbuscade: TCheckBox
          Left = 6
          Top = 130
          Width = 96
          Height = 17
          TabStop = False
          Caption = 'Embuscade'
          TabOrder = 5
          OnClick = FlotteEmbuscadeClick
        end
        object FlottePC: TCheckBox
          Left = 6
          Top = 150
          Width = 96
          Height = 16
          TabStop = False
          Caption = 'D'#233'charge PC'
          TabOrder = 6
          OnClick = FlottePCClick
        end
      end
      object GroupBox7: TGroupBox
        Left = 119
        Top = 0
        Width = 118
        Height = 150
        Caption = 'Ordre exclusif'
        TabOrder = 2
        object Label32: TLabel
          Left = 6
          Top = 119
          Width = 29
          Height = 13
          Caption = 'Cible :'
        end
        object FlotteRien: TRadioButton
          Left = 4
          Top = 27
          Width = 57
          Height = 17
          Caption = 'Rien'
          TabOrder = 0
          TabStop = True
          OnClick = FlotteRienClick
        end
        object FlotteTireF: TRadioButton
          Left = 4
          Top = 49
          Width = 53
          Height = 16
          Caption = '*F_f'
          TabOrder = 1
          OnClick = FlotteTireFClick
        end
        object FlotteTireM: TRadioButton
          Left = 4
          Top = 69
          Width = 45
          Height = 17
          Caption = '*M'
          TabOrder = 2
          OnClick = FlotteTireMClick
        end
        object FlotteTireP: TRadioButton
          Left = 6
          Top = 91
          Width = 41
          Height = 17
          Caption = '*P'
          TabOrder = 3
          OnClick = FlotteTirePClick
        end
        object FlotteTireI: TRadioButton
          Left = 57
          Top = 27
          Width = 53
          Height = 17
          Caption = '*I'
          TabOrder = 4
          OnClick = FlotteTireIClick
        end
        object FlotteTireR: TRadioButton
          Left = 57
          Top = 49
          Width = 57
          Height = 16
          Caption = '*R'
          TabOrder = 5
          OnClick = FlotteTireRClick
        end
        object FlotteTireB: TRadioButton
          Left = 57
          Top = 69
          Width = 45
          Height = 17
          Caption = '*B'
          TabOrder = 6
          OnClick = FlotteTireBClick
        end
        object FlotteDepl: TRadioButton
          Left = 57
          Top = 91
          Width = 55
          Height = 17
          Caption = 'D'#233'pl.'
          TabOrder = 7
          OnClick = FlotteDeplClick
        end
        object CibleFlotte: TEdit
          Left = 53
          Top = 115
          Width = 43
          Height = 19
          Hint = 'No. de la flotte victime du tir'
          ParentShowHint = False
          ShowHint = False
          TabOrder = 8
          OnChange = CibleFlotteChange
        end
      end
      object FlotteChemin1: TEdit
        Left = 170
        Top = 158
        Width = 29
        Height = 19
        Hint = 'Mondes par o'#249' est pass'#233'e la flotte'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 3
        OnChange = FlotteChemin1Change
      end
      object FlotteChemin2: TEdit
        Left = 203
        Top = 158
        Width = 28
        Height = 19
        Hint = 'Mondes par o'#249' est pass'#233'e la flotte'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 4
        OnChange = FlotteChemin2Change
      end
      object FlotteChemin3: TEdit
        Left = 235
        Top = 158
        Width = 28
        Height = 19
        Hint = 'Mondes par o'#249' est pass'#233'e la flotte'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 5
        OnChange = FlotteChemin3Change
      end
      object FlotteChemin4: TEdit
        Left = 267
        Top = 158
        Width = 27
        Height = 19
        Hint = 'Mondes par o'#249' est pass'#233'e la flotte'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 6
        OnChange = FlotteChemin4Change
      end
      object FlotteChemin5: TEdit
        Left = 299
        Top = 158
        Width = 27
        Height = 19
        Hint = 'Mondes par o'#249' est pass'#233'e la flotte'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 7
        OnChange = FlotteChemin5Change
      end
      object FlotteChemin6: TEdit
        Left = 331
        Top = 158
        Width = 27
        Height = 19
        Hint = 'Mondes par o'#249' est pass'#233'e la flotte'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 8
        OnChange = FlotteChemin6Change
      end
      object NoFlotte: TComboBox
        Left = 511
        Top = 89
        Width = 56
        Height = 21
        Hint = 'No. de la flotte '#224' visualiser/modifier'
        ParentShowHint = False
        ShowHint = False
        TabOrder = 9
        OnChange = NoFlotteChange
      end
      object FlotteChemin7: TEdit
        Left = 362
        Top = 158
        Width = 24
        Height = 19
        TabOrder = 10
        OnChange = FlotteChemin7Change
      end
      object FlotteChemin8: TEdit
        Left = 390
        Top = 158
        Width = 25
        Height = 19
        TabOrder = 11
        OnChange = FlotteChemin8Change
      end
      object FlotteChemin9: TEdit
        Left = 419
        Top = 158
        Width = 25
        Height = 19
        TabOrder = 12
        OnChange = FlotteChemin9Change
      end
    end
    object TabSheetTresors: TTabSheet
      Caption = 'Tr'#233'sors'
      ImageIndex = 4
      object Label30: TLabel
        Left = 459
        Top = 148
        Width = 53
        Height = 13
        Caption = 'No. Monde'
      end
      object Label39: TLabel
        Left = 459
        Top = 86
        Width = 50
        Height = 13
        Caption = 'No. Tr'#233'sor'
      end
      object Bevel11: TBevel
        Left = 14
        Top = 143
        Width = 367
        Height = 33
      end
      object Label41: TLabel
        Left = 20
        Top = 150
        Width = 106
        Height = 13
        Caption = 'Rechercher un tr'#233'sor :'
      end
      object GroupBox9: TGroupBox
        Left = 14
        Top = 16
        Width = 209
        Height = 104
        Caption = 'Emplacement'
        TabOrder = 0
        object Label40: TLabel
          Left = 39
          Top = 22
          Width = 56
          Height = 13
          Caption = 'Localisation'
        end
        object AllerTresorPos: TSpeedButton
          Left = 145
          Top = 18
          Width = 25
          Height = 25
          Hint = 'Aller o'#249' se trouve le tr'#233'sor'
          Glyph.Data = {
            7E010000424D7E01000000000000760000002800000016000000160000000100
            0400000000000801000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
            FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFF
            FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF0FFFFFFFFF00FFFFFFFFFFFF00FFFFFF
            FF00FFFFFFFFFFFF000FFFFFFF00FFFFFFFFFFFF0000FFFFFF00FF0000000000
            00000FFFFF00FF0000000000000000FFFF00FF00000000000000000FFF00FF00
            0000000000000000FF00FF00000000000000000FFF00FF0000000000000000FF
            FF00FF000000000000000FFFFF00FFFFFFFFFFFF0000FFFFFF00FFFFFFFFFFFF
            000FFFFFFF00FFFFFFFFFFFF00FFFFFFFF00FFFFFFFFFFFF0FFFFFFFFF00FFFF
            FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
            FF00}
          ParentShowHint = False
          ShowHint = True
          OnClick = AllerTresorPosClick
        end
        object TresorLocalisation: TEdit
          Left = 101
          Top = 18
          Width = 41
          Height = 19
          TabOrder = 0
          OnChange = TresorLocalisationChange
        end
        object TresorMonde: TRadioButton
          Left = 39
          Top = 50
          Width = 111
          Height = 17
          Caption = 'Sur un monde'
          TabOrder = 1
          OnClick = TresorMondeClick
        end
        object TresorFlotte: TRadioButton
          Left = 39
          Top = 69
          Width = 111
          Height = 17
          Caption = 'Sur une flotte'
          TabOrder = 2
          OnClick = TresorFlotteClick
        end
      end
      object NomTresor: TComboBox
        Left = 129
        Top = 147
        Width = 245
        Height = 21
        Style = csDropDownList
        TabOrder = 1
        OnChange = NomTresorChange
      end
      object NoTresor: TComboBox
        Left = 526
        Top = 83
        Width = 54
        Height = 21
        TabOrder = 2
        OnChange = NoTresorChange
      end
    end
    object TabSheetJoueurs: TTabSheet
      Caption = 'Joueurs'
      ImageIndex = 5
      object Bevel13: TBevel
        Left = 459
        Top = 85
        Width = 144
        Height = 37
      end
      object Label42: TLabel
        Left = 459
        Top = 148
        Width = 53
        Height = 13
        Caption = 'No. Monde'
      end
      object Label43: TLabel
        Left = 469
        Top = 98
        Width = 52
        Height = 13
        Caption = 'No. Joueur'
      end
      object NoJoueur: TSpinEdit
        Left = 541
        Top = 93
        Width = 56
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
        OnChange = NoJoueurChange
      end
      object GroupBox10: TGroupBox
        Left = 10
        Top = 8
        Width = 443
        Height = 168
        Caption = 'Joueur'
        TabOrder = 1
        object Label44: TLabel
          Left = 10
          Top = 20
          Width = 28
          Height = 13
          Caption = 'Nom :'
        end
        object Label45: TLabel
          Left = 10
          Top = 45
          Width = 42
          Height = 13
          Caption = 'Pseudo :'
        end
        object Label46: TLabel
          Left = 10
          Top = 69
          Width = 37
          Height = 13
          Caption = 'Classe :'
        end
        object Label47: TLabel
          Left = 10
          Top = 95
          Width = 34
          Height = 13
          Caption = 'Score :'
        end
        object Label50: TLabel
          Left = 10
          Top = 120
          Width = 81
          Height = 13
          Caption = 'Monde de d'#233'part'
        end
        object Label57: TLabel
          Left = 198
          Top = 16
          Width = 120
          Height = 13
          Caption = 'Niveaux technologiques :'
        end
        object Label58: TLabel
          Left = 196
          Top = 38
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'DEP'
        end
        object Label59: TLabel
          Left = 197
          Top = 67
          Width = 21
          Height = 13
          Alignment = taRightJustify
          Caption = 'ATT'
        end
        object Label60: TLabel
          Left = 197
          Top = 94
          Width = 21
          Height = 13
          Alignment = taRightJustify
          Caption = 'DEF'
        end
        object Label61: TLabel
          Left = 258
          Top = 38
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = 'RAD'
        end
        object Label62: TLabel
          Left = 265
          Top = 67
          Width = 16
          Height = 13
          Alignment = taRightJustify
          Caption = 'ALI'
        end
        object Label63: TLabel
          Left = 259
          Top = 94
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'CAR'
        end
        object Label64: TLabel
          Left = 204
          Top = 122
          Width = 14
          Height = 13
          Alignment = taRightJustify
          Caption = 'CA'
        end
        object Label65: TLabel
          Left = 266
          Top = 122
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = 'CD'
        end
        object Label66: TLabel
          Left = 348
          Top = 38
          Width = 48
          Height = 13
          Caption = 'Pop tu'#233'es'
        end
        object Label67: TLabel
          Left = 345
          Top = 67
          Width = 52
          Height = 13
          Caption = 'Mdes pill'#233's'
        end
        object Label68: TLabel
          Left = 328
          Top = 94
          Width = 69
          Height = 13
          Caption = 'Mdes dif. pill'#233's'
        end
        object Label69: TLabel
          Left = 8
          Top = 145
          Width = 34
          Height = 13
          Caption = 'E-mail :'
        end
        object Label76: TLabel
          Left = 190
          Top = 145
          Width = 60
          Height = 13
          Caption = '2'#232'me Email :'
        end
        object Nom: TEdit
          Left = 40
          Top = 16
          Width = 136
          Height = 19
          TabOrder = 0
          Text = 'Nom'
          OnChange = NomChange
        end
        object Pseudo: TEdit
          Left = 55
          Top = 41
          Width = 121
          Height = 19
          TabOrder = 1
          Text = 'Pseudo'
          OnChange = PseudoChange
        end
        object Classe: TComboBox
          Left = 55
          Top = 65
          Width = 121
          Height = 21
          Style = csDropDownList
          TabOrder = 2
          OnChange = ClasseChange
        end
        object Score: TEdit
          Left = 55
          Top = 91
          Width = 121
          Height = 19
          TabOrder = 3
          Text = 'Score'
          OnChange = ScoreChange
        end
        object MdEdit: TEdit
          Left = 103
          Top = 116
          Width = 73
          Height = 19
          TabOrder = 4
          Text = 'MdEdit'
          OnChange = MdEditChange
        end
        object DEP: TEdit
          Left = 222
          Top = 34
          Width = 33
          Height = 19
          TabOrder = 5
          Text = 'DEP'
          OnChange = DEPChange
        end
        object ATT: TEdit
          Left = 222
          Top = 63
          Width = 33
          Height = 19
          TabOrder = 6
          Text = 'ATT'
          OnChange = ATTChange
        end
        object DEF: TEdit
          Left = 222
          Top = 90
          Width = 33
          Height = 19
          TabOrder = 7
          Text = 'DEF'
          OnChange = DEFChange
        end
        object RAD: TEdit
          Left = 285
          Top = 34
          Width = 33
          Height = 19
          TabOrder = 8
          Text = 'RAD'
          OnChange = RADChange
        end
        object ALI: TEdit
          Left = 285
          Top = 63
          Width = 33
          Height = 19
          TabOrder = 9
          Text = 'ALI'
          OnChange = ALIChange
        end
        object CAR: TEdit
          Left = 285
          Top = 90
          Width = 33
          Height = 19
          TabOrder = 10
          Text = 'CAR'
          OnChange = CARChange
        end
        object CA: TEdit
          Left = 222
          Top = 119
          Width = 33
          Height = 19
          Enabled = False
          TabOrder = 11
          Text = 'CA'
        end
        object CD: TEdit
          Left = 285
          Top = 119
          Width = 33
          Height = 19
          Enabled = False
          TabOrder = 12
          Text = 'CD'
        end
        object PopTuees: TEdit
          Left = 401
          Top = 34
          Width = 33
          Height = 19
          TabOrder = 13
          Text = 'PopTuees'
          OnChange = PopTueesChange
        end
        object MdesPilles: TEdit
          Left = 401
          Top = 63
          Width = 33
          Height = 19
          TabOrder = 14
          Text = 'MdesPilles'
          OnChange = MdesPillesChange
        end
        object DifMdesPilles: TEdit
          Left = 401
          Top = 90
          Width = 33
          Height = 19
          TabOrder = 15
          Text = 'DifMdesPilles'
          OnChange = DifMdesPillesChange
        end
        object EmailEdit: TEdit
          Left = 48
          Top = 143
          Width = 128
          Height = 19
          TabOrder = 16
          Text = 'EmailEdit'
          OnChange = EmailEditChange
        end
        object Email2Edit: TEdit
          Left = 253
          Top = 143
          Width = 121
          Height = 19
          TabOrder = 17
          Text = 'Email2Edit'
          OnChange = Email2EditChange
        end
      end
    end
    object TabSheetGeneral: TTabSheet
      Caption = 'G'#233'n'#233'ral'
      ImageIndex = 6
      object Label75: TLabel
        Left = 459
        Top = 148
        Width = 53
        Height = 13
        Caption = 'No. Monde'
      end
      object GroupBox11: TGroupBox
        Left = 8
        Top = 8
        Width = 358
        Height = 160
        Caption = 'Divers'
        TabOrder = 0
        object Label48: TLabel
          Left = 17
          Top = 15
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Num'#233'ro du tour final'
        end
        object Label49: TLabel
          Left = 23
          Top = 36
          Width = 89
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre de joueurs'
        end
        object Label51: TLabel
          Left = 29
          Top = 61
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre de flottes'
        end
        object Label52: TLabel
          Left = 20
          Top = 85
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre de mondes'
        end
        object Label71: TLabel
          Left = 14
          Top = 109
          Width = 99
          Height = 13
          Caption = 'Email serveur de mail'
        end
        object Label73: TLabel
          Left = 20
          Top = 132
          Width = 93
          Height = 13
          Caption = 'Mot de passe partie'
        end
        object Code: TLabel
          Left = 206
          Top = 15
          Width = 69
          Height = 13
          Caption = 'Code cryptage'
        end
        object TourFinal: TEdit
          Left = 121
          Top = 11
          Width = 49
          Height = 19
          TabOrder = 0
          Text = 'TourFinal'
          OnChange = TourFinalChange
        end
        object NbJouEdit: TEdit
          Left = 121
          Top = 35
          Width = 49
          Height = 19
          TabOrder = 1
          Text = 'NbJouEdit'
          OnChange = NbJouEditChange
        end
        object NbFlottesEdit: TEdit
          Left = 121
          Top = 57
          Width = 49
          Height = 19
          TabOrder = 2
          Text = 'Nb Flottes'
          OnChange = NbFlottesEditChange
        end
        object NbMondesEdit: TEdit
          Left = 121
          Top = 81
          Width = 49
          Height = 19
          TabOrder = 3
          Text = 'Nb Mondes'
          OnChange = NbMondesEditChange
        end
        object ServeurMailEdit: TEdit
          Left = 121
          Top = 105
          Width = 120
          Height = 19
          TabOrder = 4
          Text = 'ServeurMailEdit'
          OnChange = ServeurMailEditChange
        end
        object PasswordEdit: TEdit
          Left = 121
          Top = 128
          Width = 120
          Height = 19
          TabOrder = 5
          Text = 'PasswordEdit'
          OnChange = PasswordEditChange
        end
        object CodeEdit: TEdit
          Left = 285
          Top = 11
          Width = 57
          Height = 19
          TabOrder = 6
          Text = 'CodeEdit'
          OnChange = CodeEditChange
        end
      end
    end
  end
  object OKBtn: TBitBtn
    Left = 191
    Top = 435
    Width = 76
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Kind = bkOK
    Margin = 2
    NumGlyphs = 2
    ParentFont = False
    Spacing = -1
    TabOrder = 3
    OnClick = OKBtnClick
  end
  object CancelBtn: TBitBtn
    Left = 383
    Top = 435
    Width = 77
    Height = 26
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Kind = bkCancel
    Margin = 2
    NumGlyphs = 2
    ParentFont = False
    Spacing = -1
    TabOrder = 4
  end
  object NoMondeEdit: TSpinEdit
    Left = 519
    Top = 165
    Width = 55
    Height = 22
    Hint = 'No. du monde '#224' visualiser/modifier'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxValue = 0
    MinValue = 0
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    Value = 0
    OnChange = NoMondeEditChange
  end
  object FenMonde: TMemo
    Left = 15
    Top = 212
    Width = 618
    Height = 207
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    WordWrap = False
  end
end
