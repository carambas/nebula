unit OrdresErreurs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TErreursOrdresForm = class(TForm)
    ListBox: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
    Procedure AfficheErreurs(RepFic : string; NomPartie : string; NumTour : integer);
  end;

var
  ErreursOrdresForm: TErreursOrdresForm;

implementation

uses AvatUnit, utils, Lectordres, NebData, erreurseval;

{$R *.DFM}


procedure TErreursOrdresForm.FormCreate(Sender: TObject);
begin
  ListBox.Align := alClient;
end;

procedure TErreursOrdresForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ErreursOrdresForm := nil;
  Action := caFree;
end;

Procedure TErreursOrdresForm.AfficheErreurs(RepFic : string; NomPartie : string;
                                            NumTour : integer);
Var
    {OrdreFichier : file of TOrdre;}
    NbErr : array[0..NB_JOU_MAX] of integer;
    O : TOrdre;
    ordre : integer;
    Ligne : string;
    jou : integer;
    Data : TNebData;
begin
  ListBox.Clear;
  FillChar(NBErr, sizeof(NbErr), 0);
  Data := TNebData.Create(Format('%s\%s%.2d.NBA', [RepFic, NomPartie, NumTour]), NumTour);
  begin
    for ordre := 1 to Data.OpenROrdres do
    begin
      O := Data.ReadOrdre;
      if O.Erreur > 0 then if (O.Auteur <= NB_JOU_MAX) then
        inc(NbErr[O.Auteur]);
    end;
    Data.CloseROrdres;

    for jou := 0 to Data.NbJou do
    begin
      if NbErr[jou] > 0 then
      begin
        Ligne := '';
        if jou = 0 then
          Ligne := Ligne + '0 : ' {ne devrait pas arriver...}
        else
          Ligne := Ligne + Format('"%s:%d" : ', [Data.Jou[jou].Pseudo, jou]);
        Ligne := Ligne + Format('%d erreur(s)', [NbErr[jou]]);
        ListBox.Items.Add(Ligne);

        for ordre := 1 to Data.OpenROrdres do
        begin
          O := Data.ReadOrdre;
          if ((O.Auteur=jou)and(O.Erreur>0)) then
          begin
            Ligne := Format('  %s <- %s',
              [TNebOrdres.EnleveCommentsAndSpaces(O.OrdreCh), GetErrorString(O.Erreur)]
            );
            ListBox.Items.Add(Ligne);
          end;
        end;
        Data.CloseROrdres;
        if NbErr[jou] > 0 then
        ListBox.Items.Add('');
      end;
    end;
  end;
  Data.Free;
//  Dispose(Par);
end;

Initialization
begin
  ErreursOrdresForm := nil;
end;

end.
