unit Listppap;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ClipBrd,
  Avatunit, Utils, Imprime, NebData;

type
  TListingToClipboard = class(TForm)
    Label1: TLabel;
    JouPrecBouton: TBitBtn;
    JouSuivBouton: TBitBtn;
    EnvoyerBouton: TBitBtn;
    Label2: TLabel;
    NoJouEdit: TEdit;
    Label3: TLabel;
    PseudoEdit: TEdit;
    OKBouton: TBitBtn;
    Memo1: TMemo;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKBoutonClick(Sender: TObject);
    procedure JouSuivBoutonClick(Sender: TObject);
    procedure JouPrecBoutonClick(Sender: TObject);
  private
    { Déclarations private }
//    HPar : THandle;
//    Par : PPartie;
    Data : TNebdata;
    Joueur : integer;
    Partie : string;
    Tour : integer;
    RepFic : string;
  public
    { Déclarations public }
    constructor Create2(AOwner : TComponent; RFic : string;
      NomPartie : string; NoTour : integer);
  end;

var
  ListingToClipboard: TListingToClipboard;

implementation

{$R *.DFM}

constructor TListingToClipboard.Create2(AOwner : TComponent; RFic : string;
  NomPartie : string; NoTour : integer);
begin
  Partie := NomPartie;
  Tour := NoTour;
  RepFic := RFic;

//  HPar := GlobalAlloc(GPTR, sizeof(TPartie));
//  Par := GlobalLock(HPar);
  Data := TNebData.Create(RepFic + '\' + NomPartie, NoTour);

//  LectureFichiers(Par, nil, nil, nil, nil, RepFic + '\' + NomPartie, NoTour);

  inherited Create(AOwner);
end;

procedure TListingToClipboard.FormDestroy(Sender: TObject);
begin
//  GlobalUnlock(HPar);
//  GlobalFree(HPar);
  Data.Free;
end;

procedure TListingToClipboard.FormCreate(Sender: TObject);
begin
  Joueur := 1;
  NoJouEdit.Text := '1';
  PseudoEdit.Text := Data.Jou[1].Pseudo;
end;

procedure TListingToClipboard.OKBoutonClick(Sender: TObject);
begin
  Close;
end;

procedure TListingToClipboard.JouSuivBoutonClick(Sender: TObject);
begin
  if Joueur = Data.NbJou then
    Joueur := 1
  else
    inc(Joueur);

  NoJouEdit.Text := IntToStr(Joueur);
  PseudoEdit.Text := Data.Jou[Joueur].Pseudo;
end;

procedure TListingToClipboard.JouPrecBoutonClick(Sender: TObject);
begin
  if Joueur = 1 then
    Joueur := Data.NbJou
  else
    dec(Joueur);

  NoJouEdit.Text := IntToStr(Joueur);
  PseudoEdit.Text := Data.Jou[Joueur].Pseudo;
end;

end.
