unit VisuEvalCR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, ToolWin, NebData, Plan, AvatUnit, ImgList,
  SynEdit, System.ImageList, Vcl.Buttons;

type
  TCRForm = class(TForm)
    ToolBar1: TToolBar;
    Memo: TSynEdit;
    SpeedButton1: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { D�clarations priv�es }
    Data : TNebData;
    //procedure AdjustToHighDPI;
  public
    { D�clarations publiques }
    procedure SetData(theData : TNebData);
    procedure UpdateFont;
  end;

var
  CRForm: TCRForm;

implementation

uses NebutilWin;

{$R *.DFM}

procedure TCRForm.FormCreate(Sender: TObject);
begin
  Data := nil;
  Memo.Align := alClient;
  UpdateFont;
//  AdjustToHighDPI;
end;

procedure TCRForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TCRForm.SetData(theData : TNebData);
begin
  if assigned(theData) then
  begin
    if not Assigned(Data) then
      Data := TNebData.Create('', 0);
    Data.Assign(theData);
    Data.RempliInfoSupLight(PlanSansCR);
  end;
end;

procedure TCRForm.ToolButton1Click(Sender: TObject);
var
  winState : TWindowState;
  SimulPlanForm : TPlanForm;
begin
  if assigned(Data) then
  begin
    // sauvegarde du mode d'affichage avant cr�ation de la fen�tre de plan
    winState := self.WindowState;

    SimulPlanForm := TPlanForm.Create2(Parent, Data, PlanSansCR);

    if assigned(PlanForm) then
    begin
      SimulPlanForm.WinPlan.XOff := PlanForm.WinPlan.XOff;
      SimulPlanForm.WinPlan.YOff := PlanForm.WinPlan.YOff;
      SimulPlanForm.WinPlan.Gdmondes := PlanForm.WinPlan.Gdmondes;
      SimulPlanForm.WinPlan.LargeChange := PlanForm.WinPlan.LargeChange;
      SimulPlanForm.WinPlan.SmallChange := PlanForm.WinPlan.SmallChange;
      SimulPlanForm.WinPlan.Affichage := PlanForm.WinPlan.Affichage;
      SimulPlanForm.WinPlan.fTailleMonde := PlanForm.WinPlan.fTailleMonde;
      SimulPlanForm.WinPlan.ZoomPlan;
    end;

    // Fix : si la simul plan est en plein �cran, afficher le plan en plein �cran
    if winState = wsMaximized then
    begin
      SimulPlanForm.WindowState := wsNormal; // contournement sinon maximized ne fonctionne pas
      SimulPlanForm.WindowState := wsMaximized;
    end;

    SimulPlanForm.Show;
  end;
end;

procedure TCRForm.UpdateFont;
begin
  LockWindowUpdate(Memo.Handle);
  Memo.Font.Assign(NebWin.MemoFont);
  LockWindowUpdate(0);
end;

procedure TCRForm.FormDestroy(Sender: TObject);
begin
  Data.Free;
end;

procedure TCRForm.FormActivate(Sender: TObject);
begin
  Memo.SetFocus;
end;

//procedure TCRForm.AdjustToHighDPI;
//var
//  bmp : TBitmap;
//  i : integer;
//begin
//  // 13-04-2020 : DLV prise en compte des �crans haute def
//    for i := 0 to ToolBar1.ControlCount - 1 do
//    begin
//      if ToolBar1.Controls[i] is TSpeedButton then
//        with (ToolBar1.Controls[i] as TSpeedButton) do
//        begin
//          if (Assigned(Glyph)) then
//          begin
//            bmp := Nil;   // pour supprimer un warning de compilation
//            try
//              bmp := TBitmap.Create;
//              bmp.Assign(Glyph);
//              Glyph.Height := MulDiv(Glyph.Height, Screen.PixelsPerInch, 96);
//              Glyph.Width := MulDiv(Glyph.Width, Screen.PixelsPerInch, 96);
//              Glyph.Canvas.StretchDraw(Rect(0,0,Glyph.Width ,Glyph.Height), bmp);
//            finally
//              if Assigned(bmp) then bmp.Free;
//            end;
//          end;
//        end;
//    end;
//end;


end.
