unit EnvoiMessage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Avatunit, ChoixJou, Menus, GenMail;

type
  TEnvoiMessageForm = class(TForm)
    CorpsMessage: TMemo;
    Panel1: TPanel;
    Label1: TLabel;
    SujetEdit: TEdit;
    DestinataireButton: TButton;
    EnvoyerButton: TButton;
    PopupMenu1: TPopupMenu;
    SelectAll1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EnvoyerButtonClick(Sender: TObject);
    procedure DestinataireButtonClick(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
  private
    { D�clarations priv�es }
    ChoixJouForm : TChoixJouForm;
  public
    { D�clarations publiques }
    procedure UpdateFont;
  end;

implementation

uses NebutilWin, EnvoiSplash, Utils, {tcpip,} envoimail;

{$R *.DFM}

procedure TEnvoiMessageForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  ChoixJouForm.Free;
end;

procedure TEnvoiMessageForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if CorpsMessage.Modified then
    CanClose := Application.MessageBox('Vous allez perdre votre message.'#10#13'Voulez-vous vraiment fermer cette fen�tre ?',               'Fermeture du message', MB_YESNO) = IDYES;

end;

procedure TEnvoiMessageForm.UpdateFont;
begin
  CorpsMessage.Font.Name := NebWin.MemoFont.Name;
  CorpsMessage.Font.Size := NebWin.MemoFont.Size;
  SujetEdit.Font.Name := NebWin.MemoFont.Name;
  SujetEdit.Font.Size := NebWin.MemoFont.Size;
end;

procedure TEnvoiMessageForm.FormCreate(Sender: TObject);
begin
  Width := 613;
  UpdateFont;
  ChoixJouForm := TChoixJouForm.Create(Self);
  with NebWin do if (Assigned(MWData) and (MWData.Map.LongX <> 0)) then
    SujetEdit.Text := Format('MESSAGE %s %d %s <>', [UpperCase(MWData.NomPartie), MWData.NoJou, NebWin.Password]);
end;

procedure TEnvoiMessageForm.EnvoyerButtonClick(Sender: TObject);
var
  OK : boolean;
  Sujet : string;
  theMail : TGenMail;
begin
  Sujet := SujetEdit.Text;
  if Sujet.Contains('<>') then
  begin
    ShowMessage('Vous n''avez pas s�lectionn� de destinataire');
    Exit;
  end;

  EnvoiSplashForm.SetText('Envoi du message...');
  EnvoiSplashForm.Show;
  EnvoiSplashForm.Refresh;
  theMail := TGenMail.Create;
  try
    with NebWin do
    begin
      OK := False;

      theMail.FromAddress := Email;
      theMail.Recipient := GetAdresseSM;
      if CC then
        theMail.CC := Email;
      theMail.Subject := SujetEdit.Text;
      theMail.MailAgent := 'Nebutil ' + NoVersionNebutil;
      theMail.Body.Clear;
      theMail.Body.Text := CorpsMessage.Text;
      theMail.Body.Insert(0, 'DEBUT');
      theMail.Body.ADD('FIN');
      if EnvoiMail = te_SMTP then
      begin
        OK := EnvoiMailSMTP(theMail, SMTPServer, VerifConnex);
      end
      else
      begin
        OK := False;
        ShowMessage('Erreur de configuration de votre Email');
      end;
      if OK then
      begin
        CorpsMessage.Modified := False;
        Self.Close;
      end;
    end;
  finally
    EnvoiSplashForm.Hide;
    //SMTP.Free;
    theMail.Free;
    //MailMime.Free;
  end;
end;

procedure TEnvoiMessageForm.DestinataireButtonClick(Sender: TObject);
begin
  with NebWin do
  begin
    if Assigned(MWData) then if MWData.Map.LongX <> 0 then
    ChoixJouForm.ShowModal;
    if ChoixJouForm.Modalresult = ID_OK then
    begin
      SujetEdit.Text := Format('MESSAGE %s %d %s %s', [Uppercase(MWData.NomPartie), MWData.NoJou, NebWin.Password, ChoixJouForm.GetNums]);
      Self.Caption := 'Message � ' + ChoixJouForm.GetPseudosList;
    end;
    CorpsMessage.SetFocus;
  end;
end;

procedure TEnvoiMessageForm.SelectAll1Click(Sender: TObject);
begin
  CorpsMessage.SelectAll;
end;

end.
