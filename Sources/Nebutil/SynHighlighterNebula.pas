{+-----------------------------------------------------------------------------+
 | Class:       TSynNebula
 | Created:     2003-04-22
 | Last change: 2003-04-24
 | Author:      Olivier Gangloff
 | Description: Syntaxe des ordres de N�bula
 | Version:     1.1
 |
 | Copyright (c) 2003 Olivier Gangloff. All rights reserved.
 |
 | Generated with SynGen pour la 1.0.
 | Pas mal bidouill� � la main ensuite ;-)
 +----------------------------------------------------------------------------+}

unit SynHighlighterNebula;

{$I SynEdit.inc}

interface

uses
  SysUtils,
  Classes,
{$IFDEF SYN_CLX}
  QControls,
  QGraphics,
{$ELSE}
  Windows,
  Controls,
  Graphics,
{$ENDIF}
  SynEditTypes,
  SynEditHighlighter,
  lectordres;

type
  TtkTokenKind = (
    tkArobaseMonde,
    tkBidonComment,
    tkComment,
    tkIdentifier,
    tkKey,
    tkNull,
    tkString,
    tkSyntaxError,
    tkNumber,
    tkUnknown);

  TRangeState = (rsUnKnown, rsBraceComment, rsArobaseMonde, rsComment, rsPtVirguleComment, rsString, rsSyntaxError);

  TProcTableProc = procedure of object;

  PIdentFuncTableFunc = ^TIdentFuncTableFunc;
  TIdentFuncTableFunc = function: TtkTokenKind of object;

  TSynIdentChars = set of char;

const
  MaxKey = 79;
  SYN_ATTR_NUMBER            =   8001;
  SYN_ATTR_SYNTAXERROR       =   8002;


type
  TSynNebula = class(TSynCustomHighlighter)
  private
    fLine: PChar;
    fLineNumber: Integer;
    fProcTable: array[#0..#255] of TProcTableProc;
    fRange: TRangeState;
    Run: LongInt;
    fStringLen: Integer;
    fToIdent: PChar;
    fTokenPos: Integer;
    fTokenID: TtkTokenKind;
    fIdentFuncTable: array[0 .. MaxKey] of TIdentFuncTableFunc;
    fArobaseMondeAttri: TSynHighlighterAttributes;
    fBidonCommentAttri: TSynHighlighterAttributes;
    fCommentAttri: TSynHighlighterAttributes;
    fIdentifierAttri: TSynHighlighterAttributes;
    fKeyAttri: TSynHighlighterAttributes;
    fPtVirguleCommentAttri: TSynHighlighterAttributes;
    fStringAttri: TSynHighlighterAttributes;
    fNumberAttri: TSynHighlighterAttributes;
    fSyntaxErrorAttri: TSynHighlighterAttributes;
    fCommentInterrompu : boolean;
    fCommentAReprendre : boolean;
    fSyntaxeOk : boolean;
    fNebOrdres: TNebOrdres;
    function KeyHash(ToHash: PChar): Integer;
    function KeyComp(const aKey: string): Boolean;
    function Func26: TtkTokenKind;
    function Func46: TtkTokenKind;
    function Func79: TtkTokenKind;
    procedure IdentProc;
    procedure UnknownProc;
    function AltFunc: TtkTokenKind;
    procedure InitIdent;
    function IdentKind(MayBe: PChar): TtkTokenKind;
    procedure MakeMethodTables;
    procedure NullProc;
    procedure CRProc;
    procedure LFProc;
    procedure ArobaseMondeOpenProc;
    procedure ArobaseMondeProc;
    procedure CommentOpenProc;
    procedure CommentProc;
    procedure StringOpenProc;
    procedure StringProc;
    procedure SyntaxErrorOpenProc;
    procedure SyntaxErrorProc;
    procedure NumberProc;
  protected
    function GetIdentChars: TSynIdentChars;
    function GetSampleSource: string; override;
    function IsFilterStored: Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    {$IFNDEF SYN_CPPB_1} class {$ENDIF}
    function GetLanguageName: string; override;
    function GetRange: Pointer; override;
    procedure ResetRange; override;
    procedure SetRange(Value: Pointer); override;
    function GetDefaultAttribute(Index: integer): TSynHighlighterAttributes; override;
    function GetEol: Boolean; override;
    function GetKeyWords(TokenKind: Integer): string; override;
    function GetTokenID: TtkTokenKind;
    procedure SetLine(const NewValue: String; LineNumber: Integer); override;
    function GetToken: String; override;
    function GetTokenAttribute: TSynHighlighterAttributes; override;
    function GetTokenKind: integer; override;
    function GetTokenPos: Integer; override;
    procedure Next; override;
  published
    property ArobaseMondeAttri: TSynHighlighterAttributes read fArobaseMondeAttri write fArobaseMondeAttri;
    property BidonCommentAttri: TSynHighlighterAttributes read fBidonCommentAttri write fBidonCommentAttri;
    property CommentAttri: TSynHighlighterAttributes read fCommentAttri write fCommentAttri;
    property IdentifierAttri: TSynHighlighterAttributes read fIdentifierAttri write fIdentifierAttri;
    property KeyAttri: TSynHighlighterAttributes read fKeyAttri write fKeyAttri;
    property PtVirguleCommentAttri: TSynHighlighterAttributes read fPtVirguleCommentAttri write fPtVirguleCommentAttri;
    property StringAttri: TSynHighlighterAttributes read fStringAttri write fStringAttri;
    property SyntaxErrorAttri: TSynHighlighterAttributes read fSyntaxErrorAttri write fSyntaxErrorAttri;
    property NumberAttri: TSynHighlighterAttributes read fNumberAttri write fNumberAttri;
  end;

implementation

uses
  SynEditStrConst;

{$IFDEF SYN_COMPILER_3_UP}
resourcestring
{$ELSE}
const
{$ENDIF}
  SYNS_FilterNbula = 'All files (*.*)|*.*';
  SYNS_LangNbula = 'N�bula';
  SYNS_AttrArobaseMonde = 'ArobaseMonde';
  SYNS_AttrBidonComment = 'BidonComment';
  SYNS_AttrComment = 'Comment';
  SYNS_AttrPtVirguleComment = 'PtVirguleComment';

  SYNS_FriendlyAttrArobaseMonde = 'ArobaseMonde';
  SYNS_FriendlyAttrBidonComment = 'BidonComment';
  SYNS_FriendlyAttrComment = 'Comment';
  SYNS_FriendlyAttrPtVirguleComment = 'PtVirguleComment';

var
  Identifiers: array[#0..#255] of ByteBool;
  mHashTable : array[#0..#255] of Integer;

procedure MakeIdentTable;
var
  I, J: Char;
begin
  for I := #0 to #255 do
  begin
    case I of
      'a'..'z', 'A'..'Z', '@': Identifiers[I] := True;
    else
      Identifiers[I] := False;
    end;
    J := UpCase(I);
    case CharInSet(I, ['_', 'A'..'Z', 'a'..'z']) of
      True: mHashTable[I] := Ord(J) - 64
    else
      mHashTable[I] := 0;
    end;
  end;
end;

procedure TSynNebula.InitIdent;
var
  I: Integer;
  pF: PIdentFuncTableFunc;
begin
  pF := PIdentFuncTableFunc(@fIdentFuncTable);
  for I := Low(fIdentFuncTable) to High(fIdentFuncTable) do
  begin
    pF^ := AltFunc;
    Inc(pF);
  end;
  fIdentFuncTable[26] := Func26;
  fIdentFuncTable[46] := Func46;
  fIdentFuncTable[79] := Func79;
end;

function TSynNebula.KeyHash(ToHash: PChar): Integer;
begin
  Result := 0;
  while CharInSet(ToHash^, ['a'..'z', 'A'..'Z', '@']) do
  begin
    inc(Result, mHashTable[ToHash^]);
    inc(ToHash);
  end;
  fStringLen := ToHash - fToIdent;
end;

function TSynNebula.KeyComp(const aKey: String): Boolean;
var
  I: Integer;
  Temp: PChar;
begin
  Temp := fToIdent;
  if Length(aKey) = fStringLen then
  begin
    Result := True;
    for i := 1 to fStringLen do
    begin
      if mHashTable[Temp^] <> mHashTable[aKey[i]] then
      begin
        Result := False;
        break;
      end;
      inc(Temp);
    end;
  end
  else
    Result := False;
end;

function TSynNebula.Func26: TtkTokenKind;
begin
  if KeyComp('@OK@') then Result := tkKey else Result := tkIdentifier;
end;

function TSynNebula.Func46: TtkTokenKind;
begin
  if KeyComp('@RIEN@') then Result := tkKey else Result := tkIdentifier;
end;

function TSynNebula.Func79: TtkTokenKind;
begin
  if KeyComp('@PRODOK@') then Result := tkKey else Result := tkIdentifier;
end;

function TSynNebula.AltFunc: TtkTokenKind;
begin
  Result := tkIdentifier;
end;

function TSynNebula.IdentKind(MayBe: PChar): TtkTokenKind;
var
  HashKey: Integer;
begin
  fToIdent := MayBe;
  HashKey := KeyHash(MayBe);
  if HashKey <= MaxKey then
    Result := fIdentFuncTable[HashKey]
  else
    Result := tkIdentifier;
end;

procedure TSynNebula.MakeMethodTables;
var
  I: Char;
begin
  for I := #0 to #255 do
    case I of
      #0: fProcTable[I] := NullProc;
      #10: fProcTable[I] := LFProc;
      #13: fProcTable[I] := CRProc;
      '(': fProcTable[I] := ArobaseMondeOpenProc;
      '#', ';' : fProcTable[I] := CommentOpenProc;
      '"': fProcTable[I] := StringOpenProc;
      'A'..'Z', 'a'..'z', '@': fProcTable[I] := IdentProc;
      '0'..'9': fProcTable[I] := NumberProc;
    else
      fProcTable[I] := UnknownProc;
    end;
end;

procedure TSynNebula.NullProc;
begin
  fTokenID := tkNull;
end;

procedure TSynNebula.CRProc;
begin
  fTokenID := tkUnknown;
  inc(Run);
  if fLine[Run] = #10 then
    inc(Run);
end;

destructor TSynNebula.Destroy;
begin
  FreeAndNil(fNebOrdres);

  inherited;
end;

procedure TSynNebula.LFProc;
begin
  fTokenID := tkUnknown;
  inc(Run);
end;

procedure TSynNebula.ArobaseMondeOpenProc;
begin
  Inc(Run);
  if (fLine[Run] = '@') then
  begin
    fRange := rsArobaseMonde;
    ArobaseMondeProc;
    fTokenID := tkArobaseMonde;
  end
  else
    fTokenID := tkIdentifier;
end;

procedure TSynNebula.ArobaseMondeProc;
begin
  fTokenID := tkArobaseMonde;
  repeat
    if (fLine[Run] = '@') and
       (fLine[Run + 1] = ')') then
    begin
      Inc(Run, 2);
      fRange := rsUnKnown;
      if not CharInSet(fLine[Run], [#0, #10, #13]) and fCommentInterrompu then
        fCommentAReprendre := True
      else
        fCommentInterrompu := False;
      Break;
    end;
    if not CharInSet(fLine[Run], [#0, #10, #13]) then
      Inc(Run);
  until CharInSet(fLine[Run], [#0, #10, #13]);
end;

procedure TSynNebula.CommentOpenProc;
begin
  Inc(Run);
  fRange := rsComment;
  fCommentInterrompu := False;
  fCommentAReprendre := False;
  CommentProc;
  fTokenID := tkComment;
end;

procedure TSynNebula.CommentProc;
begin
  fTokenID := tkComment;
  repeat
    if (fLine[Run] = '(') and
       (fLine[Run + 1] = '@') then
    begin
      fCommentInterrompu := True;
      Break;
    end;
    if not CharInSet(fLine[Run], [#0, #10, #13]) then
      Inc(Run);
  until CharInSet(fLine[Run], [#0, #10, #13]);
end;

procedure TSynNebula.StringOpenProc;
begin
  Inc(Run);
  fRange := rsString;
  StringProc;
  fTokenID := tkString;
end;

procedure TSynNebula.StringProc;
begin
  fTokenID := tkString;
  repeat
    if (fLine[Run] = '"') then
    begin
      Inc(Run, 1);
      fRange := rsUnKnown;
      Break;
    end;
    if not CharInSet(fLine[Run], [#0, #10, #13]) then
      Inc(Run);
  until CharInSet(fLine[Run], [#0, #10, #13]);
end;

procedure TSynNebula.SyntaxErrorOpenProc;
begin
  Inc(Run);
  fRange := rsSyntaxError;
  SyntaxErrorProc;
  fTokenID := tkSyntaxError;
end;

procedure TSynNebula.SyntaxErrorProc;
begin
  fTokenID := tkSyntaxError;
  repeat
    if (fLine[Run] = '#') or (fLine[Run] = ';') then
    begin
      fRange := rsUnKnown;
      Break;
    end;
    if not CharInSet(fLine[Run], [#0, #10, #13]) then
      Inc(Run);
  until CharInSet(fLine[Run], [#0, #10, #13]);
end;

constructor TSynNebula.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  fArobaseMondeAttri := TSynHighLighterAttributes.Create(SYNS_AttrArobaseMonde, SYNS_FriendlyAttrArobaseMonde);
  fArobaseMondeAttri.Foreground := clBlue;
  AddAttribute(fArobaseMondeAttri);

  fBidonCommentAttri := TSynHighLighterAttributes.Create(SYNS_AttrBidonComment, SYNS_FriendlyAttrBidonComment);
  AddAttribute(fBidonCommentAttri);

  fCommentAttri := TSynHighLighterAttributes.Create(SYNS_AttrComment, SYNS_FriendlyAttrComment);
  fCommentAttri.Foreground := clGreen;
  AddAttribute(fCommentAttri);

  fIdentifierAttri := TSynHighLighterAttributes.Create(SYNS_AttrIdentifier, SYNS_FriendlyAttrIdentifier);
  AddAttribute(fIdentifierAttri);

  fKeyAttri := TSynHighLighterAttributes.Create(SYNS_AttrReservedWord, SYNS_FriendlyAttrReservedWord);
  //fKeyAttri.Style := [fsbold];
  fKeyAttri.Style := [];
  fKeyAttri.Foreground := clGreen;
  AddAttribute(fKeyAttri);

  fPtVirguleCommentAttri := TSynHighLighterAttributes.Create(SYNS_AttrPtVirguleComment, SYNS_FriendlyAttrPtVirguleComment);
  fPtVirguleCommentAttri.Foreground := clGreen;
  AddAttribute(fPtVirguleCommentAttri);

  fStringAttri := TSynHighLighterAttributes.Create(SYNS_AttrString, SYNS_FriendlyAttrString);
  fStringAttri.Foreground := clBlue;
  AddAttribute(fStringAttri);

  fSyntaxErrorAttri := TSynHighLighterAttributes.Create(SYNS_AttrSyntaxError, SYNS_FriendlyAttrSyntaxError);
  fSyntaxErrorAttri.Foreground := clRed;
  AddAttribute(fSyntaxErrorAttri);

  fNumberAttri := TSynHighLighterAttributes.Create(SYNS_AttrNumber, SYNS_FriendlyAttrNumber);
  fNumberAttri.Foreground := clMaroon;
  AddAttribute(fNumberAttri);

  SetAttributesOnChange(DefHighlightChange);
  InitIdent;
  MakeMethodTables;
  fDefaultFilter := SYNS_FilterNbula;
  fRange := rsUnknown;
  fNebOrdres := TNebOrdres.Create;
end;

procedure TSynNebula.SetLine(const NewValue: String; LineNumber: Integer);
begin
  fLine := PChar(NewValue);
  Run := 0;
  fCommentInterrompu := False;
  fCommentAReprendre := False;
  fSyntaxeOk := fNebOrdres.CheckSyntax(fLine);
  fLineNumber := LineNumber;
  Next;
end;

procedure TSynNebula.IdentProc;
begin
  fTokenID := IdentKind((fLine + Run));
  inc(Run, fStringLen);
  while Identifiers[fLine[Run]] do
    Inc(Run);
end;

procedure TSynNebula.NumberProc;
begin
  inc(Run);
  fTokenID := tkNumber;
  while CharInSet(FLine[Run], ['0'..'9']) do
    inc(Run);
end;


procedure TSynNebula.UnknownProc;
begin
{$IFDEF SYN_MBCSSUPPORT}
  if FLine[Run] in LeadBytes then
    Inc(Run,2)
  else
{$ENDIF}
  inc(Run);
  fTokenID := tkUnknown;
end;

procedure TSynNebula.Next;
begin
  fTokenPos := Run;

  // Analyse syntaxique seulement si on est en d�but de ligne
  if (not fSyntaxeOK) and (Run = 0) then
  begin
    SyntaxErrorOpenProc;
  end

  // Analyse "classique" si syntaxe OK
  // On peut se trouver ici avec syntaxOK = false et Run > 0
  // soit en fin de ligne, soit au d�but d'un commentaire.
  else if fCommentAReprendre then
    CommentOpenProc
  else
  begin
    fRange := rsUnknown;
    fProcTable[fLine[Run]];
  end;
end;

function TSynNebula.GetDefaultAttribute(Index: integer): TSynHighLighterAttributes;
begin
  case Index of
    SYN_ATTR_IDENTIFIER   : Result := fIdentifierAttri;
    SYN_ATTR_KEYWORD      : Result := fKeyAttri;
    SYN_ATTR_STRING       : Result := fStringAttri;
    SYN_ATTR_SYNTAXERROR  : Result := fSyntaxErrorAttri;
    SYN_ATTR_NUMBER       : Result := fNumberAttri;
  else
    Result := nil;
  end;
end;

function TSynNebula.GetEol: Boolean;
begin
  Result := fTokenID = tkNull;
end;

function TSynNebula.GetKeyWords(TokenKind: Integer): string;
begin
  Result := 
    '@OK@,@RIEN@';
end;

function TSynNebula.GetToken: String;
var
  Len: LongInt;
begin
  Len := Run - fTokenPos;
  SetString(Result, (FLine + fTokenPos), Len);
end;

function TSynNebula.GetTokenID: TtkTokenKind;
begin
  Result := fTokenId;
end;

function TSynNebula.GetTokenAttribute: TSynHighLighterAttributes;
begin
  case GetTokenID of
    tkArobaseMonde: Result := fArobaseMondeAttri;
    tkBidonComment: Result := fBidonCommentAttri;
    tkComment: Result := fCommentAttri;
    tkIdentifier: Result := fIdentifierAttri;
    tkKey: Result := fKeyAttri;
    tkString: Result := fStringAttri;
    tkSyntaxError: Result := fSyntaxErrorAttri;
    tkNumber: Result := fNumberAttri;
    tkUnknown: Result := fIdentifierAttri;
  else
    Result := nil;
  end;
end;

function TSynNebula.GetTokenKind: integer;
begin
  Result := Ord(fTokenId);
end;

function TSynNebula.GetTokenPos: Integer;
begin
  Result := fTokenPos;
end;

function TSynNebula.GetIdentChars: TSynIdentChars;
begin
  Result := ['a'..'z', 'A'..'Z', '@', '0'..'9'];
end;

function TSynNebula.GetSampleSource: string;
begin
  Result := '############################################'#13#10 +
            '# Ordres du joueur "Toto":9 pour le tour 8 #'#13#10 +
            '# Ordres saisis avec Nebutil version 3.11b1#'#13#10 +
            '############################################'#13#10 +
            ''#13#10 +
            ''#13#10 +
            '# Monde 10 (@M_109@) ddd'#13#10 +
            '; M_10   (146,177) "Toto:1" P=56(56) MP=8(+5) [14,8]'#13#10 +
            ';        F_120  "Toto" []=2 du M_146'#13#10 +
            ';        F_247  "Toto" []=2 du M_177'#13#10 +
            ''#13#10 +
            '#?F_100 C MP ;@OK@'#13#10 +
            ';F 100 -> @OK@'#13#10 +
            '?F_227 C MP ;@OK@'#13#10 +
            '? F 12 M 12 ;'#13#10 +
            'ddd @PRODOK@ fdff'#13#10 +
            'F_227 ? F 100'#13#10 +
            'F_227 ->'#13#10 +
            '# Toto                         : 4 VC (2 flottes)'#13#10 +
            ''#13#10 +
            '; Autres ordres (@M_0@)'#13#10 +
            ''#13#10 +
            ';F_372 @RIEN@      33'#13#10 +
            ';F_454 @RIEN@'#13#10;
end;

function TSynNebula.IsFilterStored: Boolean;
begin
  Result := fDefaultFilter <> SYNS_FilterNbula;
end;

{$IFNDEF SYN_CPPB_1} class {$ENDIF}
function TSynNebula.GetLanguageName: string;
begin
  Result := SYNS_LangNbula;
end;

procedure TSynNebula.ResetRange;
begin
  fRange := rsUnknown;
end;

procedure TSynNebula.SetRange(Value: Pointer);
begin
  fRange := TRangeState(Value);
end;

function TSynNebula.GetRange: Pointer;
begin
  Result := Pointer(fRange);
end;

initialization
  MakeIdentTable;
{$IFNDEF SYN_CPPB_1}
  RegisterPlaceableHighlighter(TSynNebula);
{$ENDIF}
end.
