unit ModifSM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, GenMail, Avatunit;

type
  TModifSMForm = class(TForm)
    CancelButton: TBitBtn;
    OKButton: TBitBtn;
    PasswordLabel1: TLabel;
    PasswordEdit: TEdit;
    procedure OKButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  ModifSMForm: TModifSMForm;

implementation

uses EnvoiSplash, NebutilWin, Utils, System.IniFiles;

{$R *.DFM}

procedure TModifSMForm.OKButtonClick(Sender: TObject);
var
  Ini: TMemIniFile;
begin
    NebWin.Password := PasswordEdit.Text;
    Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
    Ini.AutoSave := True;
    Ini.WriteString('Password', Format('%s:%d', [Nebwin.MWData.NomPartie,
      Nebwin.MWData.NoJou]), Nebwin.Password);
    Ini.Free;
end;

procedure TModifSMForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := not ((PasswordEdit.Text = '') and (Modalresult = idOK));
  if not CanClose then
  begin
    ShowMessage('Veuillez entrer votre mot de passe');
    Exit;
  end;
  PasswordEdit.Text := '';
end;

procedure TModifSMForm.FormShow(Sender: TObject);
begin
  PasswordEdit.Text := NebWin.Password;
  PasswordEdit.SetFocus;
end;

end.
