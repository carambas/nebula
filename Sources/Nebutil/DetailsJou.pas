unit DetailsJou;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  NebData,
  StdCtrls, ExtCtrls, ComCtrls, Buttons;

type
  TDetailsJouForm = class(TForm)
    Label1: TLabel;
    ClasseCombo: TComboBox;
    Label2: TLabel;
    ConfianceEdit: TEdit;
    GroupBox1: TGroupBox;
    AllieCheck: TCheckBox;
    ChargeurCheck: TCheckBox;
    ChargeurPopCheck: TCheckBox;
    DechargeurPopCheck: TCheckBox;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    ATTEdit: TEdit;
    DEFEdit: TEdit;
    ConfianceUpDown: TUpDown;
    Panel1: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    ATTUpDown: TUpDown;
    DEFUpDown: TUpDown;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure ClasseComboDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
  private
    { D�clarations priv�es }
    FData: TNebData;
    FNoJou: Integer;
    FClassesImages: TImageList;

    procedure SetClassesImages(images: TImageList);
  public
    { D�clarations publiques }
    procedure Init(Data: TNebData; NoJou: Integer);

    property ClassesImages: TImageList read FClassesImages
      write SetClassesImages;
  end;

var
  DetailsJouForm: TDetailsJouForm;

implementation

// uses WinProcs;
uses CommCtrl, Utils;

{$R *.DFM}

procedure TDetailsJouForm.Init(Data: TNebData; NoJou: Integer);
begin
  FData := Data;
  FNoJou := NoJou;
  Caption := Format('D�tails sur %s', [FData.Jou[FNoJou].Pseudo]);
  ClasseCombo.ItemIndex := FData.Jou[FNoJou].Classe;
  ConfianceUpDown.Position := FData.Confiance[FNoJou];
  ATTUpDown.Position := FData.Jou[FNoJou].ATT;
  DEFUpDown.Position := FData.Jou[FNoJou].DEF;
  if Data.EstAllie(FNoJou, FData.NoJou) then
    AllieCheck.Checked := True;
  if Data.EstChargeur(FNoJou, FData.NoJou) then
    ChargeurCheck.Checked := True;
  if Data.EstChargeurDePop(FNoJou, FData.NoJou) then
    ChargeurPopCheck.Checked := True;
  if Data.EstDechargeurDePop(FNoJou, FData.NoJou) then
    DechargeurPopCheck.Checked := True;
end;

procedure TDetailsJouForm.BitBtn1Click(Sender: TObject);
begin
  FData.Jou[FNoJou].Classe := ClasseCombo.ItemIndex;

  FData.Confiance[FNoJou] := ConfianceUpDown.Position;
  FData.Jou[FNoJou].ATT := ATTUpDown.Position;
  FData.Jou[FNoJou].CA := CalculeCA(ATTUpDown.Position);
  FData.Jou[FNoJou].DEF := DEFUpDown.Position;
  FData.Jou[FNoJou].CD := CalculeCD(DEFUpDown.Position);

  if AllieCheck.Checked then
    FData.DeclareAllie(FNoJou, FData.NoJou)
  else
    FData.DeclareEnnemi(FNoJou, FData.NoJou);

  if ChargeurCheck.Checked then
    FData.DeclareChargeur(FNoJou, FData.NoJou)
  else
    FData.DeclareNonChargeur(FNoJou, FData.NoJou);

  if ChargeurPopCheck.Checked then
    FData.DeclareChargeurPop(FNoJou, FData.NoJou)
  else
    FData.DeclareNonChargeurPop(FNoJou, FData.NoJou);

  if DechargeurPopCheck.Checked then
    FData.DeclareDechargeurPop(FNoJou, FData.NoJou)
  else
    FData.DeclareNonDechargeurPop(FNoJou, FData.NoJou);
end;

procedure TDetailsJouForm.ClasseComboDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  bitmap: TBitmap;
  x, y, coul: Integer;
  P: PByteArray;
  CoulFond: array [0 .. 2] of byte;
  cCoulFond: TColor;
begin
  with ClasseCombo.Canvas do
  begin
    FillRect(Rect);

    // On dessine le bitmap qui sera affich�
    bitmap := TBitmap.Create;
    bitmap.PixelFormat := pf24bit;
    bitmap.Height := ClassesImages.Height;
    bitmap.Width := ClassesImages.Width;
    bitmap.HandleType := bmDIB;

    // Permet de r�cup�rer le triplet RVG de la couleur de s�lection
    // J'ai pas trouv� mieux
    bitmap.Canvas.Pixels[0, 0] := clHighlight;
    cCoulFond := bitmap.Canvas.Pixels[0, 0];
    Move(cCoulFond, CoulFond, 3);

    ClassesImages.Draw(bitmap.Canvas, 0, 0, Index);

    if odSelected in State then
    begin
      for y := 0 to bitmap.Height - 1 do
      begin
        P := bitmap.ScanLine[y];
        for x := 0 to bitmap.Width - 1 do
          for coul := 0 to 2 do
          begin
            P[3 * x + coul] := (CoulFond[2 - coul] + P[3 * x + coul]) div 2;
          end;
      end;
    end;

    ClasseCombo.Canvas.Draw(Rect.Left + 1, Rect.Top + 1, bitmap);
    TextOut(Rect.Left + ClassesImages.Width + 2, Rect.Top + 1,
      ClasseCombo.Items[Index]);
    bitmap.Free;
  end;
end;

procedure TDetailsJouForm.SetClassesImages(images: TImageList);
begin
  FClassesImages := images;
  //ClasseCombo.ItemHeight := FClassesImages.Height; // Pquoi �a ne marche pas ?

end;

end.
