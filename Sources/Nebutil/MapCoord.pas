unit MapCoord;

interface

uses
  SysUtils, NebData, NebDataPlan, Generics.Collections, Types;

var
  uMap : array of TPoint;
  uFakeMap : boolean;

procedure FakeRad3(RepFic : string; MWData : TNebData);
procedure LoadCoordMap(var MWData : TNebData);
procedure SaveCoordMap(MWData : TNebData);

type
  TCoord = record
    X, Y: Integer;
  end;

  TLesMondesPlanTailleConnect = array [1 .. 32, 1 .. 32] of record
    No: smallint;
    Md, TN: boolean;
    LongueurConnexions: Integer;
  end;

  TPlaceMondesPlan = class(TObject)
  public
    constructor Create(MWData: TNebData);
    function PlaceMondesPlan: TNebDataPlan;

  private
    Data: TNebDataPlan;
    MondesTaillesConnect: TLesMondesPlanTailleConnect;
    function LongueurConnexions(md: Integer): Integer;
    function LongueurConnexion(md, n: Integer): Integer;
    procedure UpdateMapCoord;
    function LongueurConnexionCoord(md, x, y: Integer): Integer;
    function TrouveMeilleureEmplacement(md: Integer; doitEtreVide: Boolean = true): TCoord;
    function LongueurConnexionsCoord(md, x, y: Integer): Integer;
    procedure DeplaceMonde(md, x, y: Integer);
    function getTotalLongueurConnexions: Integer;
    function getTotalConnexionsKO: Integer;
    procedure PlaceNouveauxMondesPlan(nvMonde: TList<Integer>);
    function DeplaceAuMeilleurEmplacementLibre(md: Integer): Boolean;
    procedure ConnexionSurMapCoord(x1, y1, x2, y2: Integer; Supprime: Boolean = false);
  end;

implementation

uses Vcl.Dialogs, System.Math, Winapi.Windows, PlaceMondes;

procedure WriteLog(Msg: String);
begin
  {$IFDEF DEBUG}
  OutputDebugString(PChar(Msg));
  {$ENDIF}
end;

procedure FakeRad3(RepFic : string; MWData : TNebData);
var
  md, i : integer;
  mapPosDispo : array of array of Boolean;
  nvMonde : TList<Integer>;
  x, y : Integer;
  Placement: TPlaceMondesPlan;
  MondesStr: String;
  NbMondesSurPlan: Integer;


begin
  // Ne rien faire si RAD = 3
  NbMondesSurPlan := 0;
  uFakeMap := false;
  if MWData.Radar = 3 then exit;

  uFakeMap := true;

  nvMonde := TList<Integer>.Create;
  // NOTE : les tableaux dynamique son index� � partir de 0
  SetLength(uMap, MWData.NbMonde);
  for i := 0 to MWData.NbMonde - 1 do
  begin
     uMap[i].X := 0;
     uMap[i].Y := 0;
  end;

  SetLength(mapPosDispo, MWData.Map.LongX, MWData.Map.LongY);
  for x := 1 to MWData.Map.LongX  do
  begin
    for y := 1 to MWData.Map.LongY do
      mapPosDispo[x-1][y-1] := True;
  end;

  // On fixe le Radar � 3
  MWData.Radar := 3;
  // Relecture et affectation des positions inconnues d�j� enregistr�e
  LoadCoordMap(MWData);

  // M�moriser les mondes sans position avec au moins une connection
  // M�moriser les coordonn�es connues
  for md := 1 to MWData.NbMonde do
  begin
    if (MWData.M[md].X = 0) or (MWData.M[md].Y = 0) then
    begin
      if (uMap[md-1].X > 0) and (uMap[md-1].Y > 0)
        and mapPosDispo[uMap[md-1].X-1][uMap[md-1].Y-1] then
      begin
        MWData.M[md].X := uMap[md-1].X;
        MWData.M[md].Y := uMap[md-1].Y;
        mapPosDispo[MWData.M[md].X-1][MWData.M[md].Y-1] := False;
        Inc(NbMondesSurPlan);
      end
      else
      for i := 1 to 8 do
        if (MWData.AVuCoord[md, MWData.NoJou]) or (MWData.M[md].Connect[i] > 0) then
        begin
          Inc(NbMondesSurPlan);
          nvMonde.Add(md);
          break;
        end;
    end
    else
    begin
      uMap[md-1].X := MWData.M[md].X;
      uMap[md-1].Y := MWData.M[md].Y;
      mapPosDispo[MWData.M[md].X-1][MWData.M[md].Y-1] := False;
      Inc(NbMondesSurPlan);
    end;
  end;

  // S'il y a des nouveaux mondes, on fait un placement automatique des mondes
  if nvMonde.Count > 0 then
  begin
    PlaceMondesForm := TPlaceMondesForm.Create(nil);
    Placement := TPlaceMondesPlan.Create(MWData);
    try
      PlaceMondesForm.FermerButton.Enabled := False;
      PlaceMondesForm.Show;

      WriteLog(IntToStr(nvMonde.Count) + ' monde(s) � placer');
      PlaceMondesForm.Message.Text := IntToStr(nvMonde.Count) + ' monde(s) � placer';
      PlaceMondesForm.Refresh;

      Placement.PlaceNouveauxMondesPlan(nvMonde);

      if NbMondesSurPlan = nvMonde.Count then
        Placement.PlaceMondesPlan;


      PlaceMondesForm.Message.Clear;
      if nvMonde.Count = 1 then
        PlaceMondesForm.Message.Lines.Add('1 nouveau monde a �t� plac� sur le plan')
      else
        PlaceMondesForm.Message.Lines.Add(IntToStr(nvMonde.Count) + ' nouveaux mondes ont plac�s sur le plan :');

      MondesStr := '';
      for md in nvMonde do
        MondesStr := MondesStr + Format(' %d,', [md]);
      MondesStr := MondesStr.Remove(MondesStr.Length - 1).TrimLeft;

      PlaceMondesForm.Message.Lines.Add(MondesStr);
      PlaceMondesForm.Message.Lines.Add('');
      PlaceMondesForm.Message.Lines.Add('Si le positionnement vous convient cliquez sur le bouton de sauvegarde pour le conserver par la suite');

      PlaceMondesForm.FermerButton.Enabled := True;
      PlaceMondesForm.Close;
      PlaceMondesForm.ShowModal;
    finally
      FreeAndNil(PlaceMondesForm);
      Placement.Free;
    end;
  end;

  nvMonde.Free;
end;

procedure LoadCoordMap(var MWData : TNebData);
var
  MapFile : TextFile;
  ligne : string;
  idx, idx2 : integer;
  md, x, y : integer;
begin
  if not uFakeMap then exit;

  if FileExists(MWData.GetMapCoordFileName) then
  begin
    AssignFile(MapFile, MWData.GetMapCoordFileName);
    Reset(MapFile);
    while not EOF(MapFile) do
    begin
      Readln(MapFile, ligne);
      ligne := ligne.Trim;
      if ligne = '' then continue;

      idx := ligne.IndexOf('=');
      if idx = -1 then continue;

      md := 0;
      try
        md := ligne.Substring(0, idx).ToInteger();
      except
        on E: EConvertError do md := 0;
      end;
      if md = 0 then continue;

      inc(idx);
      idx2 := ligne.IndexOf(',', idx);
      if idx2 = -1 then continue;

      x := 0;
      y := 0;
      try
        x := ligne.Substring(idx, idx2-idx).ToInteger();
        y := ligne.Substring(idx2+1).ToInteger();
      except
        on E: EConvertError do
        begin
          x := 0;
          y := 0;
        end;
      end;
      if (x = 0) or (y = 0) then continue;

      uMap[md-1].X := x;
      uMap[md-1].Y := y;
    end;
    CloseFile(MapFile);
  end;
end;

procedure SaveCoordMap(MWData : TNebData);
var
  MapFile : TextFile;
  ligne : string;
  md : integer;
  fileName: string;
begin
  if not Assigned(uMap) then
    Exit;

  fileName := MWData.GetMapCoordFileName;
  AssignFile(MapFile, MWData.GetMapCoordFileName);
  Rewrite(MapFile);

  for md := 1 to MWData.NbMonde do
  begin
    // Mise � jour de la map locale uMap
    if (MWData.M[md].X > 0) and (MWData.M[md].Y > 0) then
    begin
      uMap[md-1].X := MWData.M[md].X;
      uMap[md-1].Y := MWData.M[md].Y;
    end;

    // Enregistrement des coordonn�es
    if (uMap[md-1].X > 0) and (uMap[md-1].Y > 0) then
    begin
      ligne := Format('%d=%d,%d', [md, uMap[md-1].X, uMap[md-1].Y]);
      Writeln(MapFile, ligne);
    end;
  end;

  CloseFile(MapFile);

end;

{ TPlaceMondesPlan }

constructor TPlaceMondesPlan.Create(MWData: TNebData);
begin
  Data := TNebDataPlan(MWData);
  FillChar(MondesTaillesConnect, sizeof(MondesTaillesConnect), 0);
end;

procedure TPlaceMondesPlan.ConnexionSurMapCoord(x1, y1, x2, y2: Integer; Supprime: Boolean = false);
var
  dirX, dirY: Integer;
  longueur: Integer;
  i: Integer;
  X, Y: Integer;
  OldValeur, NewValeur: Integer;

begin
  // Connexion de taille 2 et multiple de 45�
  if Data.IsConnexionAngleMultiple45(x1, y1, x2, y2) then
  begin
    // On d�termine la direction de la connexion
    dirX := Data.DeltaX(x1, x2);
    dirY := Data.DeltaY(y1, y2);

    longueur := Max(abs(dirX), abs(dirY));

    dirX := Sign(dirX);
    dirY := Sign(dirY);
    if Supprime then
    begin
       OldValeur := -1;
       Newvaleur := 0;
    end
    else
    begin
       OldValeur := 0;
       Newvaleur := -1;
    end;

    for i := 1 to longueur - 1 do
    begin
      X := Data.ModuloX(x1 + i * dirX);
      Y := Data.ModuloY(y1 + i * dirY);
      if MondesTaillesConnect[X, Y].No = OldValeur then
        // On note sur MapCoord le passage de la connexion (ou on le supprime)
        MondesTaillesConnect[X, Y].No := NewValeur;
    end;
  end;
end;

procedure TPlaceMondesPlan.UpdateMapCoord;
var
  md: Integer;
  c: Integer;
begin
  FillChar(MondesTaillesConnect, sizeof(MondesTaillesConnect), 0);
  for md := 1 to Data.NbMonde do if (Data.M[md].X > 0) and (Data.M[md].Y > 0) then
  begin
    MondesTaillesConnect[Data.M[md].X, Data.M[md].Y].No := md;

    if Data.estMd(md) then
      MondesTaillesConnect[Data.M[md].X, Data.M[md].Y].Md := True;

    if Data.TN[md] then
      MondesTaillesConnect[Data.M[md].X, Data.M[md].Y].TN := True;

    MondesTaillesConnect[Data.M[md].X, Data.M[md].Y].LongueurConnexions :=
      LongueurConnexions(md);

    // On indique les mondes travers�s par la connexion
    c := 1;
    if Data.M[md].Connect[1] > 0 then
    Repeat
      ConnexionSurMapCoord(Data.M[md].X, Data.M[md].Y, Data.M[Data.M[md].Connect[c]].X, Data.M[Data.M[md].Connect[c]].Y);
      Inc(c)
    Until (c >= High(Data.M[md].Connect)) or (Data.M[md].Connect[c] = 0);
  end;
end;

function TPlaceMondesPlan.getTotalLongueurConnexions: Integer;
var
  md: Integer;
begin
  Result := 0;
  for md := 1 to Data.NbMonde do
    Inc(Result, LongueurConnexions(md));
end;

function TPlaceMondesPlan.getTotalConnexionsKO: Integer;
var
  md: Integer;
begin
  Result := 0;
  for md := 1 to Data.NbMonde do
  begin
    Inc(Result, Data.CountConnexionAngleNotMultiple45(md) +
                Data.CountConnexionSurvoleAutreMonde(md));
  end;
end;


// Londueur de la connexion entre le monde md et un monde situ� en x, y.
function TPlaceMondesPlan.LongueurConnexionCoord(md, x, y: Integer): Integer;
begin
  Result := Max(
    Abs(Data.DeltaX(Data.M[md].X, x)),
    Abs(Data.DeltaY(Data.M[md].Y, y)));

  if Data.IsConnexionSurvoleAutreMonde(Data.M[md].X, Data.M[md].Y, x, y) then
    Inc(Result, 1);

  if not Data.IsConnexionAngleMultiple45(Data.M[md].X, Data.M[md].Y, x, y) then
    Inc(Result, 1);
end;

function TPlaceMondesPlan.LongueurConnexion(md, n: Integer): Integer;
var
  md_connecte: Integer;
begin
  Result := -1;

  if n > Length(Data.M[md].Connect) then
    Exit;

  md_connecte := Data.M[md].Connect[n];

  if md_connecte = 0 then
    Exit;

  Result := LongueurConnexionCoord(md_connecte, Data.M[md].X, Data.M[md].Y);
end;

// Longueur des connexions du monde md s'il �tait situ� aux coordonn�es x et y
function TPlaceMondesPlan.LongueurConnexionsCoord(md, x, y: Integer): Integer;
var
  c: Integer;
  longueur: Integer;
  md_c: Integer;
begin
  Result := 0;
  c := 0;
  Repeat
    Inc(c);
    if c > High(Data.M[md].Connect) then
      Break;

    md_c := Data.M[md].Connect[c];

    if md_c = 0 then
      Break;

    longueur := LongueurConnexionCoord(md_c, x, y);

    if (longueur > 0) then
      Inc(Result, longueur);
  Until false;
end;


function TPlaceMondesPlan.LongueurConnexions(md: Integer): Integer;
var
  c: Integer;
  longueur: Integer;
begin
  Result := 0;
  c := 0;
  Repeat
    Inc(c);
    longueur := LongueurConnexion(md, c);
    if (longueur > 0) then
      Inc(Result, longueur);
  Until longueur <= 0;
end;

function TPlaceMondesPlan.TrouveMeilleureEmplacement(md: Integer; doitEtreVide: Boolean = true): TCoord;
var
  x, y: Integer;
  l, longueurMin, cnx_ko, cnx_ko_min: Integer;
begin
  Result.X := Data.M[md].X;
  Result.Y := Data.M[md].Y;

  if (Data.M[md].X = 0) or (Data.M[md].Y = 0) then
  begin
    longueurMin := 32;
    cnx_ko_min := High(Data.M[md].Connect)
  end
  else
  begin
    longueurMin := LongueurConnexionsCoord(md, Data.M[md].X, Data.M[md].Y);
    cnx_ko_min := Data.CountConnexionAngleNotMultiple45(md, Data.M[md].X, Data.M[md].Y) +
                  Data.CountConnexionSurvoleAutreMonde(md, Data.M[md].X, Data.M[md].Y);
  end;

  for x := 1 to Data.Map.LongX do
    for y := 1 to Data.Map.LongY do
  begin
    l := LongueurConnexionsCoord(md, x, y);
    cnx_ko := Data.CountConnexionAngleNotMultiple45(md, x, y) +
              Data.CountConnexionSurvoleAutreMonde(md, x, y);
    // TODO si on met un test l < longueurMin l'algorithme bloque beaucoup trop vite
    if (not doitEtreVide or (MondesTaillesConnect[x, y].No = 0))
      and (l <= longueurMin * 1.1)
      and (cnx_ko <= cnx_ko_min)
      then
    begin
      longueurMin := l;
      cnx_ko_min := cnx_ko;
      Result.X := x;
      Result.Y := y;
    end;
  end;
end;

procedure TPlaceMondesPlan.DeplaceMonde(md, x, y: Integer);
var
  c: Integer;
begin
  if (Data.M[md].X > 0) and (Data.M[md].Y > 0) then
  begin

    // On efface l'espace travers� par les connexions
    c := 1;
    if Data.M[md].Connect[1] > 0 then
    Repeat
      ConnexionSurMapCoord(Data.M[md].X, Data.M[md].Y,
        Data.M[Data.M[md].Connect[c]].X, Data.M[Data.M[md].Connect[c]].Y, True);
      Inc(c)
    Until (c >= High(Data.M[md].Connect)) or (Data.M[md].Connect[c] = 0);


    MondesTaillesConnect[x, y] := MondesTaillesConnect[Data.M[md].X, Data.M[md].Y];
    FillChar(MondesTaillesConnect[Data.M[md].X, Data.M[md].Y],
      sizeof(MondesTaillesConnect[Data.M[md].X, Data.M[md].Y]), 0);
  end
  else
  begin
    MondesTaillesConnect[x, y].No := md;
  end;

  Data.M[md].X := x;
  Data.M[md].Y := y;

  // On note l'espace travers� par les connexions
  c := 1;
  if Data.M[md].Connect[1] > 0 then
  Repeat
    ConnexionSurMapCoord(Data.M[md].X, Data.M[md].Y,
      Data.M[Data.M[md].Connect[c]].X, Data.M[Data.M[md].Connect[c]].Y, False);
    Inc(c)
  Until (c >= High(Data.M[md].Connect)) or (Data.M[md].Connect[c] = 0);


end;

function TPlaceMondesPlan.DeplaceAuMeilleurEmplacementLibre(md: Integer): Boolean;
var
  md_occupe: Integer;
  coord: TCoord;
begin
  Result := False;

  if md = 0 then
    Exit;


  coord := TrouveMeilleureEmplacement(md, true);
  if (coord.X = 0) or (coord.Y = 0) then
  begin
    coord.X := 1;
    coord.Y := 1;
  end;

  md_occupe := MondesTaillesConnect[coord.X, coord.Y].No;

  // Si l'emplacement est libre...
  if (coord.X > 0) and (coord.Y > 0) and (md_occupe = 0) then
  begin
    DeplaceMonde(md, coord.X, coord.Y);
    Result := True;
  end;

end;

// Ne place que les nouveaux mondes sans toucher � ceux d�j� plac�s
procedure TPlaceMondesPlan.PlaceNouveauxMondesPlan(nvMonde: TList<Integer>);
var
  md: Integer;
begin
  UpdateMapCoord;
  for md in nvMonde do
    DeplaceAuMeilleurEmplacementLibre(md);
end;

// Travail global sur tout le plan, destructeur
function TPlaceMondesPlan.PlaceMondesPlan: TNebDataPlan;

  procedure IterePlacement(numIteration: Integer);
  var
    md: Integer;
    sens: Integer;
    c: Integer;
  begin
    if Odd(numIteration) then
    begin
      md := 1;
      sens := 1;
    end
    else
    begin
      md := Data.NbMonde;
      sens := -1;
    end;

    repeat
      // On d�place le monde au meilleur emplcement disponible...
      if (Data.M[md].X > 0) and (Data.M[md].Y > 0)
        and DeplaceAuMeilleurEmplacementLibre(md) then
      begin
        c := 1;
        // On fait la m�me chose pour les mondes connect�s
        repeat
          DeplaceAuMeilleurEmplacementLibre(Data.M[md].Connect[c]);
          Inc(c)
        until (c > High(Data.M[md].Connect[c])) or (Data.M[md].Connect[c] = 0)
      end;

      Inc(md, sens);
    until (md > Data.NbMonde) or (md <= 0);
  end;

  procedure IterePlacementParCoord(numIteration: Integer);
  var
    md: Integer;
    c: Integer;
    x, y: Integer;
  begin

    for y := 1 to Data.Map.LongY do
      for x := 1 to Data.Map.LongX do
      begin
        md := Data.CoordMondeToNumMonde(x, y);
        if md > 0 then
        begin
          // On d�place le monde au meilleur emplcement disponible...
          if (Data.M[md].X > 0) and (Data.M[md].Y > 0)
            and DeplaceAuMeilleurEmplacementLibre(md) then
          begin
            c := 1;
            // On fait la m�me chose pour les mondes connect�s
            repeat
              DeplaceAuMeilleurEmplacementLibre(Data.M[md].Connect[c]);
              Inc(c)
            until (c > High(Data.M[md].Connect[c])) or (Data.M[md].Connect[c] = 0)
          end;
        end;
      end;
  end;

var
  longueur, old_longueur, longueur_min, best_data_longueur: Integer;
  cnx_ko, old_cnx_ko, cnx_ko_min, best_data_ko: Integer;
  i: Integer;
  best_data: TNebData;
  nb_it_sans_chgt: Integer;
begin
  WriteLog('Placer les mondes sur le plan');
  UpdateMapCoord;

  longueur := getTotalLongueurConnexions;
  longueur_min := longueur;
  cnx_ko := getTotalConnexionsKO;
  cnx_ko_min := cnx_ko;
  best_data_ko := cnx_ko;
  best_data_longueur := longueur;

  best_data := TNebDataPlan.Create('', 0);

  i := 0;
  nb_it_sans_chgt := 0;
  WriteLog(Format('It�ration %d : longueur Connexions=%d, cnx KO=%d', [i, longueur, cnx_ko]));
  Repeat
    inc(i);
    old_longueur := longueur;
    old_cnx_ko := cnx_ko;
    if Assigned(PlaceMondesForm) then
    begin
      PlaceMondesForm.Message.Lines.Add(Format('It�ration %d/30 (max)', [i]));
      PlaceMondesForm.Refresh;
    end;

    //IterePlacement(i);
    IterePlacementParCoord(i);

    longueur := getTotalLongueurConnexions;
    if old_longueur < longueur_min then
      longueur_min := old_longueur;

    cnx_ko := getTotalConnexionsKO;
    if old_cnx_ko < cnx_ko_min then
      cnx_ko_min := old_cnx_ko;

    if cnx_ko <= cnx_ko_min then
    begin
      // Ici on garde une copie du plan
      best_data.Assign(Data);
      best_data_ko := cnx_ko;
      best_data_longueur := longueur;
    end;

    if (longueur >= longueur_min) and ((cnx_ko >= cnx_ko_min)) then
      Inc(nb_it_sans_chgt)
    else
      nb_it_sans_chgt := 0;

    WriteLog(Format('It�ration %d : longueur Connexions=%d(%d), cnx KO=%d(%d), sschgt=%d', [i, longueur, longueur_min, cnx_ko, old_cnx_ko, nb_it_sans_chgt]));
  Until (nb_it_sans_chgt > 2) or (i > 30);

  Data.Assign(best_data);
  best_data.Free;
  longueur := getTotalLongueurConnexions;
  cnx_ko := getTotalConnexionsKO;

  WriteLog(Format('Fin : longueur Connexions=%d, cnx KO=%d', [best_data_longueur, cnx_ko]));
  if Assigned(PlaceMondesForm) then
    PlaceMondesForm.Message.Lines.Add(Format('%d connexion(s) en rouge', [cnx_ko]));

  // Optimisations post-placement :
  // trouver les mondes avec bcp de connexions KO et les bouger � c�t� d'un monde connect� o� il y a de la place


  Result := Data;
end;

end.
