object ExtraitCRDialog: TExtraitCRDialog
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Photos de mondes / Extraits de CR'
  ClientHeight = 665
  ClientWidth = 910
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object CardPanel: TCardPanel
    Left = 0
    Top = 0
    Width = 785
    Height = 665
    Align = alLeft
    ActiveCard = CardCR
    Caption = 'CardPanel'
    TabOrder = 0
    object CardCR: TCard
      Left = 1
      Top = 1
      Width = 783
      Height = 663
      Caption = 'CardCR'
      CardIndex = 0
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 449
        Height = 13
        Caption = 
          'Collez ci-dessous vos extraits de CR texte et ils seront import'#233 +
          's dans le fichier .nbt de ce tour'
      end
      object Memo: TMemo
        Left = 8
        Top = 44
        Width = 761
        Height = 604
        ScrollBars = ssVertical
        TabOrder = 0
        OnChange = MemoChange
      end
    end
    object CardNumJou: TCard
      Left = 1
      Top = 1
      Width = 783
      Height = 663
      Caption = 'CardNumJou'
      CardIndex = 1
      TabOrder = 1
      object JoueurInconnuLabel: TLabel
        Left = 74
        Top = 61
        Width = 634
        Height = 16
        Caption = 
          'Ce joueur vous est inconnu. Merci de pr'#233'ciser son num'#233'ro afin de' +
          ' pouvoir il'#39'afficher correctement sur votre CR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object NomJoueurInconnuEdit: TEdit
        Left = 127
        Top = 112
        Width = 370
        Height = 27
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = 'NomJoueurInconnuEdit'
      end
      object NumJouEdit: TEdit
        Left = 503
        Top = 112
        Width = 137
        Height = 27
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '1'
        OnChange = NumJouEditChange
      end
      object NumJouUpDown: TUpDown
        Left = 640
        Top = 112
        Width = 16
        Height = 27
        Associate = NumJouEdit
        Position = 1
        TabOrder = 2
        OnChangingEx = NumJouUpDownChangingEx
      end
      object PoursuivreButton: TButton
        Left = 319
        Top = 176
        Width = 145
        Height = 49
        Caption = 'Poursuivre >>>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = PoursuivreButtonClick
      end
    end
  end
  object CancelBtn: TButton
    Left = 812
    Top = 59
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Annuler'
    ModalResult = 2
    TabOrder = 1
    OnClick = CancelBtnClick
  end
  object OKBtn: TButton
    Left = 812
    Top = 12
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OKBtnClick
  end
end
