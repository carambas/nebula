unit ProbaConv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Utils, Math;

type
  TStatConv = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    NbVCEdit: TEdit;
    NbPopEdit: TEdit;
    ProbaEdit: TEdit;
    CalculButton: TButton;
    Button1: TButton;
    procedure CalculButtonClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StatConv: TStatConv;

implementation

{$R *.DFM}

function ProbaPop(VC, Pop : integer) : Extended;
begin
  Result := BinomialCoeff(VC, Pop) * Power(0.75, VC - Pop) * Power(0.25, Pop);
end;

procedure TStatConv.CalculButtonClick(Sender: TObject);
var
  VC : integer;
  Pop : integer;
  Proba : Extended;
  i : integer;
begin
  VC := StrToIntDef(NbVCEdit.Text, 0);
  Pop := StrToIntDef(NbPopEdit.Text, 0);

  if VC > 1547 then
  begin
    ShowMessage('Le calcul est trop compliqu� au-del� de 1547 VC');
    Exit;
  end;

  Proba := 0;
  for i := 0 to Pop - 1 do
  begin
    Proba := Proba + ProbaPop(VC, i); 
  end;
  
  ProbaEdit.Text := Format('%.2f%%', [(1 - Proba) * 100]);
end;

procedure TStatConv.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
