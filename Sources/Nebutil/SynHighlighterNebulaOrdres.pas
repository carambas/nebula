

unit SynHighlighterNebulaOrdres;

{$I SynEdit.inc}

interface

uses
  SysUtils,
  Graphics,
  SynEditTypes,
  SynEditHighlighter,
  SynUnicode,
  Classes,
  lectordres;

type
  TtkTokenKind = (tkComment, tkArobaseMonde, tkText, tkNull, tkNumber,
    tkSpace, tkString, tkSyntaxError, tkUnknown);

const
  SYN_ATTR_SYNTAXERROR       =   8002;

type
  TSynNebOrdres = class(TSynCustomHighlighter)
  private
    FTokenID: TtkTokenKind;
    fCommentAttri: TSynHighlighterAttributes;
    fArobaseMondeAttri: TSynHighlighterAttributes;
    fTextAttri: TSynHighlighterAttributes;
    fNumberAttri: TSynHighlighterAttributes;
    fSpaceAttri: TSynHighlighterAttributes;
    fStringAttri: TSynHighlighterAttributes;
    fSyntaxErrorAttri: TSynHighlighterAttributes;
    fSyntaxeOk : boolean;
    fCommentInterrompu : boolean;
    fCommentAReprendre : boolean;
    fNebOrdres: TNebOrdres;
    procedure CRProc;
    procedure TextProc;
    procedure LFProc;
    procedure NullProc;
    procedure NumberProc;
    procedure CommentProc;
    procedure ArobaseMondeProc;
    procedure SpaceProc;
    procedure StringProc;  // ""
    procedure SyntaxErrorProc;
  protected
    function GetSampleSource: string; override;
    function IsFilterStored: Boolean; override;
  public
    class function GetLanguageName: string; override;
    class function GetFriendlyLanguageName: string; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetDefaultAttribute(Index: integer): TSynHighlighterAttributes;
      override;
    function GetEol: Boolean; override;
    function GetTokenID: TtkTokenKind;
    function GetTokenAttribute: TSynHighlighterAttributes; override;
    function GetTokenKind: integer; override;
    procedure Next; override;
  published
    property CommentAttri: TSynHighlighterAttributes read fCommentAttri
      write fCommentAttri;
    property ArobaseMondeAttri: TSynHighlighterAttributes read fArobaseMondeAttri
      write fArobaseMondeAttri;
    property TextAttri: TSynHighlighterAttributes read fTextAttri
      write fTextAttri;
    property NumberAttri: TSynHighlighterAttributes read fNumberAttri
      write fNumberAttri;
    property SpaceAttri: TSynHighlighterAttributes read fSpaceAttri
      write fSpaceAttri;
    property StringAttri: TSynHighlighterAttributes read fStringAttri
      write fStringAttri;
    property SyntaxErrorAttri: TSynHighlighterAttributes read fSyntaxErrorAttri
      write fSyntaxErrorAttri;
  end;

implementation

uses
  SynEditStrConst;

const
  SYNS_FilterNbula = 'All files (*.*)|*.*';
  SYNS_NebulaOrdes = 'OrdresNebula';
  SYNS_FriendlyNebulaOrdes = 'Ordres Nébula';

  SYNS_AttrArobaseMonde = 'ArobaseMonde';
  SYNS_FriendlyArobaseMonde = 'Arobase Monde';

constructor TSynNebOrdres.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  fCommentAttri := TSynHighlighterAttributes.Create(SYNS_AttrComment, SYNS_FriendlyAttrComment);
  fCommentAttri.Foreground := clGreen;
  AddAttribute(fCommentAttri);

  fArobaseMondeAttri := TSynHighLighterAttributes.Create(SYNS_AttrArobaseMonde, SYNS_FriendlyArobaseMonde);
  fArobaseMondeAttri.Foreground := clBlue;
  AddAttribute(fArobaseMondeAttri);

  fTextAttri := TSynHighlighterAttributes.Create(SYNS_AttrText, SYNS_FriendlyAttrText);
  AddAttribute(fTextAttri);

  fNumberAttri := TSynHighlighterAttributes.Create(SYNS_AttrNumber, SYNS_FriendlyAttrNumber);
  fNumberAttri.Foreground := clMaroon;
  AddAttribute(fNumberAttri);

  fSpaceAttri := TSynHighlighterAttributes.Create(SYNS_AttrSpace, SYNS_FriendlyAttrSpace);
  AddAttribute(fSpaceAttri);

  fStringAttri := TSynHighlighterAttributes.Create(SYNS_AttrString, SYNS_FriendlyAttrString);
  fStringAttri.Foreground := clBlue;
  AddAttribute(fStringAttri);

  fSyntaxErrorAttri := TSynHighLighterAttributes.Create(SYNS_AttrSyntaxError, SYNS_FriendlyAttrSyntaxError);
  fSyntaxErrorAttri.Foreground := clRed;
  AddAttribute(fSyntaxErrorAttri);

  SetAttributesOnChange(DefHighlightChange);

  fDefaultFilter := SYNS_FilterNbula;

  fCommentInterrompu := False;
  fCommentAReprendre := False;

  fNebOrdres := TNebOrdres.Create;
end; { Create }

procedure TSynNebOrdres.CRProc;
begin
  fTokenID := tkSpace;
  case FLine[Run + 1] of
    #10: inc(Run, 2);
    else inc(Run);
  end;
end;

destructor TSynNebOrdres.Destroy;
begin
  FreeAndNil(fNebOrdres);

  inherited;
end;

procedure TSynNebOrdres.TextProc;

  function IsTextChar: Boolean;
  begin
    case fLine[Run] of
      'a'..'z', 'A'..'Z', '_':
        Result := True;
      else
        Result := False;
    end;
  end;

begin
    fTokenID := tkText;
    inc(Run);
    while FLine[Run] <> #0 do
      if IsTextChar then
        inc(Run)
      else
        break;
end;

procedure TSynNebOrdres.LFProc;
begin
  fTokenID := tkSpace;
  inc(Run);
end;

procedure TSynNebOrdres.NullProc;
begin
  fTokenID := tkNull;
  inc(Run);
end;

procedure TSynNebOrdres.NumberProc;
begin
  inc(Run);
  fTokenID := tkNumber;
  while CharInSet(fLine[Run], ['0'..'9']) do inc(Run);
end;

// # ;
procedure TSynNebOrdres.CommentProc;
begin
  fTokenID := tkComment;

  fCommentAReprendre := False;

  inc(Run);
  while FLine[Run] <> #0 do
    if (Fline[Run] = '(') and (Fline[Run+1] = '@') then
    begin
      fCommentInterrompu := True;
      Break;
    end
    else
      case FLine[Run] of
        #10: break;
        #13: break;
        else inc(Run);
      end;
end;

procedure TSynNebOrdres.ArobaseMondeProc;
begin
  fTokenID := tkArobaseMonde;
  repeat
    if (fLine[Run] = '@') and
       (fLine[Run + 1] = ')') then
    begin
      Inc(Run, 2);
      if not CharInSet(fLine[Run], [#0, #10, #13]) then
        fCommentAReprendre := True;
      fCommentInterrompu := False;
      Break;
    end;
    if not CharInSet(fLine[Run], [#0, #10, #13]) then
      Inc(Run);
  until CharInSet(fLine[Run], [#0, #10, #13]);
  if CharInSet(fLine[Run], [#0, #10, #13]) then
  begin
    fCommentAReprendre := False;
    fCommentInterrompu := False;
  end;
end;

procedure TSynNebOrdres.SpaceProc;
begin
  inc(Run);
  fTokenID := tkSpace;
  while (FLine[Run] <= #32) and not IsLineEnd(Run) do inc(Run);
end;

// ""
procedure TSynNebOrdres.StringProc;
begin
  fTokenID := tkString;
  repeat
    case FLine[Run] of
      #0, #10, #13: break;
    end;
    inc(Run);
  until FLine[Run] = '"';
  if FLine[Run] <> #0 then inc(Run);
end;

procedure TSynNebOrdres.SyntaxErrorProc;
begin
  fTokenID := tkSyntaxError;
  repeat
    case FLine[Run] of
      #0, #10, #13: break;
    end;
    inc(Run);
  until (FLine[Run] = '#') or (FLine[Run] = ';');
  fSyntaxeOk := True;
end;

procedure TSynNebOrdres.Next;
begin
  fTokenPos := Run;

  // Vérification de la syntaxe pour coloration d'erreur
  if Run = 0 then
  begin
    fSyntaxeOk := fNebOrdres.CheckSyntax(fLine);
  end;

  // Colorisation particulière si Erreur de syntaxe
  if not fSyntaxeOK then
  begin
    SyntaxErrorProc;
  end
  else if fCommentInterrompu then
  begin
    ArobaseMondeProc;
  end
  else if fCommentAReprendre then
  begin
    CommentProc;
  end
  else
  begin
    case fLine[Run] of
      #0: NullProc;
      #10: LFProc;
      #13: CRProc;
      #34: StringProc;  // "
      '0'..'9': NumberProc;
      ';': CommentProc;
      '#': CommentProc;
      #1..#9, #11, #12, #14..#32: SpaceProc;
      else TextProc;
    end;
  end;
  inherited;
end;

function TSynNebOrdres.GetDefaultAttribute(Index: integer): TSynHighlighterAttributes;
begin
  case Index of
    SYN_ATTR_COMMENT: Result := fCommentAttri;
    SYN_ATTR_STRING: Result := fStringAttri;
    SYN_ATTR_WHITESPACE: Result := fSpaceAttri;
    SYN_ATTR_SYNTAXERROR: Result := fSyntaxErrorAttri;
  else
    Result := nil;
  end;
end;

function TSynNebOrdres.GetEol: Boolean;
begin
  Result := Run = fLineLen + 1;
end;

function TSynNebOrdres.GetTokenID: TtkTokenKind;
begin
  Result := fTokenId;
end;

function TSynNebOrdres.GetTokenAttribute: TSynHighlighterAttributes;
begin
  case fTokenID of
    tkComment: Result := fCommentAttri;
    tkArobaseMonde: Result := fArobaseMondeAttri;
    tkText: Result := fTextAttri;
    tkNumber: Result := fNumberAttri;
    tkSpace: Result := fSpaceAttri;
    tkString: Result := fStringAttri;
    tkSyntaxError: Result := fSyntaxErrorAttri;
    tkUnknown: Result := fTextAttri;
    else Result := nil;
  end;
end;

function TSynNebOrdres.GetTokenKind: integer;
begin
  Result := Ord(fTokenId);
end;

function TSynNebOrdres.IsFilterStored: Boolean;
begin
  Result := fDefaultFilter <> SYNS_FilterINI;
end;

class function TSynNebOrdres.GetLanguageName: string;
begin
  Result := SYNS_NebulaOrdes;
end;

function TSynNebOrdres.GetSampleSource: string;
begin
  Result := '; Syntax highlighting'#13#10+
            '[Section]'#13#10+
            'Key=value'#13#10+
            'String="Arial"'#13#10+
            'Number=123456';
end;

class function TSynNebOrdres.GetFriendlyLanguageName: string;
begin
  Result := SYNS_FriendlyNebulaOrdes;
end;

initialization
  RegisterPlaceableHighlighter(TSynNebOrdres);
end.
