unit Preferences;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, ComCtrls, IniFiles, Dialogs, ImgList,
  System.ImageList;

type
  TPrefForm = class(TForm)
    OKButton: TButton;
    Button2: TButton;
    Button3: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ConvertCaract: TCheckBox;
    IconesPlan256: TCheckBox;
    Label1: TLabel;
    SauvegardeAuto: TCheckBox;
    CursorTrack: TCheckBox;
    DessineChemin: TCheckBox;
    UtiliseTransparence: TCheckBox;
    Ordres: TTabSheet;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    TailleLigneEdit: TEdit;
    EnleveCommentCheck: TCheckBox;
    RetourLigneCheck: TCheckBox;
    OrdresTree: TTreeView;
    OrdresImages: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RetourLigneCheckClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure OrdresTreeGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure OrdresTreeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    IniFile : TMemIniFile;
    OrdresSquelette : array[0..10] of boolean;

    procedure LitPrefs;
    function GetStateIconIndex(enabled : boolean): integer;
    function GetValueFromStateIcon(index : integer) : boolean;
  public
    function OrdresMondes : boolean;
    function OrdresFlottes : boolean;
    function OrdresTraces : boolean;
    function OrdresRapForce : boolean;
    function OrdresDeplFlottes : boolean;
    function OrdresDechCar : boolean;
    function OrdresChargeMP : boolean;
    function OrdresTransFV : boolean;
    function OrdresConstr : boolean;
  end;

var
  PrefForm: TPrefForm;

const
  Ordres_Mondes = 1;
  Ordres_Flottes = 2;
  Ordres_Traces = 3;
  Ordres_RapForce = 4;
  Ordres_DeplFlottes = 6;
  Ordres_Constr = 7;
  Ordres_DechCar = 8;
  Ordres_ChargeMP = 9;
  Ordres_TransFV = 10;

implementation

uses Ordres;

{$R *.DFM}

procedure TPrefForm.FormCreate(Sender: TObject);
begin
  IniFile := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
  IniFile.AutoSave := True;
  PageControl1.ActivePage := TabSheet1;
  OrdresTree.FullExpand;
  OrdresTree.TopItem := OrdresTree.Items[0];
  LitPrefs;
end;

procedure TPrefForm.FormDestroy(Sender: TObject);
begin
  IniFile.Free;
end;

procedure TPrefForm.LitPrefs;
begin
  // On lit toutes les préférences dans NEBUTIL.INI
  //CROrdres.Checked := IniFile.ReadBool('Preferences', 'CRDansOrdres', True);
  ConvertCaract.Checked := IniFile.ReadBool('Preferences', 'Conversion_Caract', True);
  IconesPlan256.Checked := IniFile.ReadBool('Preferences', 'IconesPlan256', True);
  SauvegardeAuto.Checked := IniFile.ReadBool('Preferences', 'SauvegardeAuto', True);
  CursorTrack.Checked := IniFile.ReadBool('Preferences', 'CursorTrack', False);
  DessineChemin.Checked := IniFile.ReadBool('Preferences', 'DessineChemin', True);
  RetourLigneCheck.Checked := IniFile.ReadBool('Preferences', 'RetourLigne', True);
  EnleveCommentCheck.Checked := IniFile.ReadBool('Preferences', 'EnleveComment', False);
  TailleLigneEdit.Text := IntToStr(IniFile.ReadInteger('Preferences', 'TailleLigne', 70));
  UtiliseTransparence.Checked := IniFile.ReadBool('Preferences', 'UtiliseTransparence', True);

  // Squelettes des ordres
  OrdresSquelette[Ordres_Mondes] := IniFile.ReadBool('Preferences', 'CRDansOrdres', True);
  OrdresTree.Items[Ordres_Mondes].Data := Pointer(Ordres_Mondes);
  OrdresTree.Items[Ordres_Mondes].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_Mondes]);

  OrdresSquelette[Ordres_Flottes] := IniFile.ReadBool('Preferences', 'Ordres_Flottes', True);
  OrdresTree.Items[Ordres_Flottes].Data := Pointer(Ordres_Flottes);
  OrdresTree.Items[Ordres_Flottes].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_Flottes]);

  OrdresSquelette[Ordres_Traces] := IniFile.ReadBool('Preferences', 'Ordres_Traces', False);
  OrdresTree.Items[Ordres_Traces].Data := Pointer(Ordres_Traces);
  OrdresTree.Items[Ordres_Traces].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_Traces]);

  OrdresSquelette[Ordres_RapForce] := IniFile.ReadBool('Preferences', 'Ordres_RapForce', True);
  OrdresTree.Items[Ordres_RapForce].Data := Pointer(Ordres_RapForce);
  OrdresTree.Items[Ordres_RapForce].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_RapForce]);

  OrdresSquelette[Ordres_DeplFlottes] := IniFile.ReadBool('Preferences', 'Ordres_DeplFlottes', True);
  OrdresTree.Items[Ordres_DeplFlottes].Data := Pointer(Ordres_DeplFlottes);
  OrdresTree.Items[Ordres_DeplFlottes].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_DeplFlottes]);

  OrdresSquelette[Ordres_DechCar] := IniFile.ReadBool('Preferences', 'Ordres_DechCar', True);
  OrdresTree.Items[Ordres_DechCar].Data := Pointer(Ordres_DechCar);
  OrdresTree.Items[Ordres_DechCar].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_DechCar]);

  OrdresSquelette[Ordres_ChargeMP] := IniFile.ReadBool('Preferences', 'Ordres_ChargeMP', True);
  OrdresTree.Items[Ordres_ChargeMP].Data := Pointer(Ordres_ChargeMP);
  OrdresTree.Items[Ordres_ChargeMP].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_ChargeMP]);

  OrdresSquelette[Ordres_TransFV] := IniFile.ReadBool('Preferences', 'Ordres_TransFV', True);
  OrdresTree.Items[Ordres_TransFV].Data := Pointer(Ordres_TransFV);
  OrdresTree.Items[Ordres_TransFV].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_TransFV]);

  OrdresSquelette[Ordres_Constr] := IniFile.ReadBool('Preferences', 'Ordres_Constr', True);
  OrdresTree.Items[Ordres_Constr].Data := Pointer(Ordres_Constr);
  OrdresTree.Items[Ordres_Constr].StateIndex := GetStateIconIndex(OrdresSquelette[Ordres_Constr]);


end;

procedure TPrefForm.OKButtonClick(Sender: TObject);
begin
  // On stocke toutes les préférences dans NEBULA.INI
  //IniFile.WriteBool('Preferences', 'CRDansOrdres', CROrdres.Checked);
  IniFile.WriteBool('Preferences', 'Conversion_Caract', ConvertCaract.Checked);
  IniFile.WriteBool('Preferences', 'IconesPlan256', IconesPlan256.Checked);
  IniFile.WriteBool('Preferences', 'SauvegardeAuto', SauvegardeAuto.Checked);
  IniFile.WriteBool('Preferences', 'CursorTrack', CursorTrack.Checked);
  IniFile.WriteBool('Preferences', 'DessineChemin', DessineChemin.Checked);
  IniFile.WriteBool('Preferences', 'UtiliseTransparence', UtiliseTransparence.Checked);
  IniFile.WriteBool('Preferences', 'RetourLigne', RetourLigneCheck.Checked);
  IniFile.WriteBool('Preferences', 'EnleveComment', EnleveCommentCheck.Checked);
  //IniFile.WriteBool('Preferences', 'ColorMemo', ColorMemo.Checked);
  IniFile.WriteInteger('Preferences', 'TailleLigne', StrToIntDef(TailleLigneEdit.Text, 0));
  if OrdresForm <> nil then
  begin
    OrdresForm.AutoSaveTimer.Enabled := SauvegardeAuto.Checked;
    OrdresForm.CursorTrack := CursorTrack.Checked;
  end;

  // Squellette des ordres
  IniFile.WriteBool('Preferences', 'CRDansOrdres', OrdresSquelette[Ordres_Mondes]);
  IniFile.WriteBool('Preferences', 'Ordres_Flottes', OrdresSquelette[Ordres_Flottes]);
  IniFile.WriteBool('Preferences', 'Ordres_Traces', OrdresSquelette[Ordres_Traces]);
  IniFile.WriteBool('Preferences', 'Ordres_RapForce', OrdresSquelette[Ordres_RapForce]);
  IniFile.WriteBool('Preferences', 'Ordres_DeplFlottes', OrdresSquelette[Ordres_DeplFlottes]);
  IniFile.WriteBool('Preferences', 'Ordres_DechCar', OrdresSquelette[Ordres_DechCar]);
  IniFile.WriteBool('Preferences', 'Ordres_ChargeMP', OrdresSquelette[Ordres_ChargeMP]);
  IniFile.WriteBool('Preferences', 'Ordres_TransFV', OrdresSquelette[Ordres_TransFV]);
  IniFile.WriteBool('Preferences', 'Ordres_Constr', OrdresSquelette[Ordres_Constr]);

  IniFile.UpdateFile;
end;

procedure TPrefForm.FormShow(Sender: TObject);
begin
  LitPrefs;
end;

procedure TPrefForm.RetourLigneCheckClick(Sender: TObject);
begin
  TailleLigneEdit.Enabled := RetourLigneCheck.Checked;
end;

procedure TPrefForm.Button3Click(Sender: TObject);
var
  HelpFileName : string;
begin
  // OG 01/04/2007 : utilisation de fichiers d'aide .chm au lieu de .hlp
  // WinHelp(Handle, 'nebutil.hlp', HELP_CONTEXT, 1008);

  HelpFileName := ExpandFileName(ExtractFilePath(ParamStr(0)) + 'nebutil.chm');
  HtmlHelp(0, PChar(HelpFileName), HH_HELP_CONTEXT, 1008);
end;

procedure TPrefForm.OrdresTreeGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  //Node.StateIndex := 1;
end;

procedure TPrefForm.OrdresTreeMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Node : TTreeNode;
begin
  Node := OrdresTree.GetNodeAt(X, Y);
  if htOnStateIcon in OrdresTree.GetHitTestInfoAt(X, Y) then
  begin
    if Integer(Node.Data) > 0 then
    begin
      OrdresSquelette[Integer(Node.Data)] := not GetValueFromStateIcon(Node.StateIndex);
      Node.StateIndex := GetStateIconIndex(OrdresSquelette[Integer(Node.Data)])
    end;
  end;

end;

function TPrefForm.GetStateIconIndex(enabled : boolean): integer;
begin
  if enabled then
    Result := 3
  else
    Result := 2;
end;

function TPrefForm.GetValueFromStateIcon(index: integer): boolean;
begin
  Result := False;
  if index = 3 then
    Result := True;
end;

function TPrefForm.OrdresChargeMP: boolean;
begin
  Result := OrdresSquelette[Ordres_ChargeMP];
end;

function TPrefForm.OrdresDechCar: boolean;
begin
  Result := OrdresSquelette[Ordres_DechCar];
end;

function TPrefForm.OrdresDeplFlottes: boolean;
begin
  Result := OrdresSquelette[Ordres_DeplFlottes];
end;

function TPrefForm.OrdresFlottes: boolean;
begin
  Result := OrdresSquelette[Ordres_Flottes];
end;

function TPrefForm.OrdresMondes: boolean;
begin
  Result := OrdresSquelette[Ordres_Mondes];
end;

function TPrefForm.OrdresTraces: boolean;
begin
  Result := OrdresSquelette[Ordres_Traces];
end;

function TPrefForm.OrdresTransFV: boolean;
begin
  Result := OrdresSquelette[Ordres_TransFV];
end;

function TPrefForm.OrdresConstr: boolean;
begin
  Result := OrdresSquelette[Ordres_Constr];
end;

function TPrefForm.OrdresRapForce: boolean;
begin
  Result := OrdresSquelette[Ordres_RapForce];
end;

end.
