unit Visulist;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ComCtrls, SynEdit;

type
  TVisuListingForm = class(TForm)
  published
    Memo: TSynEdit;

    constructor Create(Parent : TForm; CR: TStringList); reintroduce;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure UpdateFont;
  public
    procedure UpdateCR(CR: TStringList); overload;
    procedure UpdateCR(CR: String); overload;

  private
    CR: TStringList;

  end;

var
  VisuListingForm: TVisuListingForm;

implementation

{$R *.DFM}

uses NebutilWin, Utils;

constructor TVisuListingForm.Create(Parent : TForm; CR: TStringList);
begin
  Self.CR := CR;
  inherited Create(Parent);
end;

procedure TVisuListingForm.FormCreate(Sender: TObject);
begin
  Memo.Align := alClient;

  Memo.Lines.BeginUpdate;
  if Assigned(CR) then
    Memo.Lines.Assign(CR);
  Memo.Lines.EndUpdate;
end;

procedure TVisuListingForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Memo.Lines.Clear;
  Action := caFree;
end;

procedure TVisuListingForm.FormDestroy(Sender: TObject);
begin
  VisuListingForm := nil;
end;

procedure TVisuListingForm.FormDeactivate(Sender: TObject);
begin
  NebWin.ChercherMenu.Enabled := False;
  NebWin.OccSuivMenu.Enabled := False;
end;

procedure TVisuListingForm.FormActivate(Sender: TObject);
begin
  NebWin.FindDialog.CloseDialog;
  NebWin.ChercherMenu.Enabled := True;
  NebWin.ChercherMondeMenu.Enabled := True;
  NebWin.OccSuivMenu.Enabled := True;
end;

procedure TVisuListingForm.UpdateFont;
begin
  LockWindowUpdate(Memo.Handle);
  Memo.Font.Assign(NebWin.MemoFont);
  LockWindowUpdate(0);
end;

procedure TVisuListingForm.UpdateCR(CR: TStringList);
begin
  Self.CR := CR;
  Memo.Lines.BeginUpdate;
  Memo.Lines.Assign(CR);
  Memo.Lines.EndUpdate;
end;

procedure TVisuListingForm.UpdateCR(CR: String);
begin
  Memo.Lines.BeginUpdate;
  Memo.Lines.Text := CR;
  Memo.Lines.EndUpdate;
end;

Initialization
begin
  VisuListingForm := nil;
end;

end.
