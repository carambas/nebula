unit ConfigReseau;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls;

type
  TReseauConfigForm = class(TForm)
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    Label4: TLabel;
    OpenDialog: TOpenDialog;
    BitBtn1: TBitBtn;
    ProxyNameLabel: TLabel;
    ProxyPortLabel: TLabel;
    Label8: TLabel;
    UseProxyCheck: TCheckBox;
    ProxyNameEdit: TEdit;
    ProxyPortEdit: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure UseProxyCheckClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  ReseauConfigForm: TReseauConfigForm;

implementation

{$R *.DFM}




procedure TReseauConfigForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := True;
end;


procedure TReseauConfigForm.FormCreate(Sender: TObject);
begin
  UseProxyCheckClick(Self);
end;

procedure TReseauConfigForm.UseProxyCheckClick(Sender: TObject);
begin
  if UseProxyCheck.Checked then
  begin
    ProxyNameEdit.Enabled := True;
    ProxyNameLabel.Enabled := True;
    ProxyPortEdit.Enabled := True;
    ProxyPortLabel.Enabled := True;
  end
  else
  begin
    ProxyNameEdit.Enabled := False;
    ProxyNameLabel.Enabled := False;
    ProxyPortEdit.Enabled := False;
    ProxyPortLabel.Enabled := False;
  end;
end;


end.
