unit about;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, jpeg, avatunit{, osversion},
  Utils, ShellAPI;

type
  TAPropos = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Version: TLabel;
    BetaLabel: TLabel;
    Image1: TImage;
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    Button1: TButton;
    URL_Nebula: TLabel;
    Email_Olivier: TLabel;
    Email_Cyril: TLabel;
    Email_Sylvie: TLabel;
    Email_Cedric: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure URL_Label_click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  APropos: TAPropos;

implementation

uses NebutilWin;

{$R *.dfm}

procedure TAPropos.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TAPropos.Button1Click(Sender: TObject);
begin
  NebWin.Testversionsplusrcentes1Click(Self);
end;

procedure TAPropos.FormCreate(Sender: TObject);
var
  VersionCh : string;
begin
  VersionCh := 'Version ' + NoVersionNebutil;
  Version.Caption := VersionCh;

  if VersionNebutilbeta > 0 then
    BetaLabel.Caption := Format('Cette version beta est encore valable %d jours',
      [60-NebWin.DureeDepuisCompile])
  else
    BetaLabel.Visible := False;
end;

procedure TAPropos.URL_Label_click(Sender: TObject);
var
  URL, caption : String;
begin
  caption := TLabel(Sender).Caption;
  if caption.StartsWith('http://') then
  begin
    URL := caption;
  end
  else if caption.StartsWith('@') then
  begin
    URL := 'mailto:' + caption;
  end;
  // Spécifique Windows
  ShellExecute(GetDesktopWindow(), 'open', PChar(URL), nil, nil, SW_SHOWNORMAL);
end;

end.
