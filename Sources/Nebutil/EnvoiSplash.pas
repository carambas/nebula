unit EnvoiSplash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TEnvoiSplashForm = class(TForm)
    TextLabel: TLabel;
  private
    { Déclarations privées }
    //procedure CreateParams(var Params: TCreateParams); override;
  public
    { Déclarations publiques }
    procedure SetText(texte : string);
  end;

var
  EnvoiSplashForm: TEnvoiSplashForm;

implementation

{$R *.DFM}

{procedure TEnvoiSplashForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);

  Params.Style := WS_POPUP;
  Params.ExStyle := WS_EX_CLIENTEDGE;
end;}

procedure TEnvoiSplashForm.SetText(texte : string);
begin
  TextLabel.Caption := texte;
end;

end.
