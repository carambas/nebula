program Nebutil;

uses
  Forms,
  NebutilWin in 'NebutilWin.pas' {NebWin},
  SEARCH in 'SEARCH.PAS',
  SimuPop in 'SimuPop.pas' {SimuPopForm},
  Preferences in 'Preferences.pas' {PrefForm},
  ModifSM in 'ModifSM.pas' {ModifSMForm},
  ConfigReseau in 'ConfigReseau.pas' {ReseauConfigForm},
  DetailsJou in 'DetailsJou.pas' {DetailsJouForm},
  RelationJous in 'RelationJous.pas' {RelationsForm},
  ChoixJou in 'ChoixJou.pas' {ChoixJouForm},
  AL_ENEMI in 'AL_ENEMI.PAS' {AlEnnemiForm},
  RESULTAT in 'RESULTAT.PAS' {ResultForm},
  Visulist in 'Visulist.pas' {VisuListingForm},
  FLOTTES in 'FLOTTES.PAS' {FlottesForm},
  SIMU in 'SIMU.PAS' {SimuForm},
  EnvoiSplash in 'EnvoiSplash.pas' {EnvoiSplashForm},
  VisuEvalCR in 'VisuEvalCR.pas' {CRForm},
  Chemin in '..\Shared\Chemin.pas',
  nebdata in '..\Shared\nebdata.pas',
  AVATUNIT in '..\Shared\AVATUNIT.PAS',
  WINPLAN in '..\Shared\Plan\WINPLAN.PAS',
  PLAN in '..\Shared\Plan\PLAN.PAS' {PlanForm},
  IMPMONDE in '..\Shared\Plan\IMPMONDE.PAS' {ImpMondeForm},
  HISTO in '..\Shared\Plan\HISTO.PAS' {HistoForm},
  LEctordres in '..\Shared\Ordres\LEctordres.pas',
  Prtpform in '..\Shared\PrtForm\Prtpform.pas' {PrintPlanDlg},
  NBTStringList in '..\Shared\NBTStringList.pas',
  sortie in '..\Shared\sortie.pas' {SortieForm},
  ListInt in '..\Shared\ListInt.pas',
  IMPRIME in '..\Shared\IMPRIME.PAS',
  erreurseval in '..\shared\erreurseval.pas',
  envoimail in '..\shared\envoimail.pas',
  about in 'about.pas' {APropos},
  genmail in '..\shared\genmail.pas',
  SynHighlighterNebula in 'SynHighlighterNebula.pas',
  utils in '..\shared\utils.pas',
  SynHighlighterNebulaOrdres in 'SynHighlighterNebulaOrdres.pas',
  MapCoord in 'MapCoord.pas',
  ExtraitCR in 'ExtraitCR.pas' {ExtraitCRDialog},
  ordreslist in '..\shared\ordres\ordreslist.pas',
  ordres in 'ordres.pas' {OrdresForm},
  avateval in '..\shared\eval\avateval.pas',
  espionnage in '..\shared\espionnage.pas',
  nebdataplan in '..\shared\Plan\nebdataplan.pas',
  PlaceMondes in '..\shared\Plan\PlaceMondes.pas' {PlaceMondesForm};

{$E .exe}
{$R *.RES}

begin
{$IFDEF DEBUG}
ReportMemoryLeaksOnShutdown := true;
{$ENDIF}
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Nebutil';
  Application.HelpFile := 'nebutil.chm';
  Application.CreateForm(TNebWin, NebWin);
  Application.CreateForm(TSimuPopForm, SimuPopForm);
  Application.CreateForm(TPrefForm, PrefForm);
  Application.CreateForm(TModifSMForm, ModifSMForm);
  Application.CreateForm(TAlEnnemiForm, AlEnnemiForm);
  Application.CreateForm(TEnvoiSplashForm, EnvoiSplashForm);
  Application.CreateForm(TImpMondeForm, ImpMondeForm);
  Application.CreateForm(TPrintPlanDlg, PrintPlanDlg);
  Application.CreateForm(TSortieForm, SortieForm);
  Application.CreateForm(TCRForm, CRForm);
  Application.CreateForm(TExtraitCRDialog, ExtraitCRDialog);
  Application.Run;

end.
