unit NebutilWin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  WinTypes, WinProcs, Menus, Shellapi, clipbrd,
  Avatunit, About, ExtCtrls, Plan,
  Utils, Printers, Simu, Flottes, Al_enemi, System.IniFiles, ComCtrls,
  LectOrdres, NebData,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  genmail, Winapi.ShlObj, IdIPWatch,
  REST.Types, REST.Client, REST.Authenticator.Basic, JSON, Vcl.WinXPanels;

type
  TEnvoiMail = (te_None, te_SMTP, te_HTTP);

  TNebWin = class(TForm)
    OpenDialog: TOpenDialog;
    MainMenu1: TMainMenu;
    FichierMenu: TMenuItem;
    propos1: TMenuItem;
    Ouvrir1: TMenuItem;
    N1: TMenuItem;
    Quitter1: TMenuItem;
    Vrificationdesordres1: TMenuItem;
    Sauverlesordres1: TMenuItem;
    N3: TMenuItem;
    Listedesordres1: TMenuItem;
    Aide2: TMenuItem;
    Configurer1: TMenuItem;
    NiveaudeDEPlacement1: TMenuItem;
    MenuFenetre: TMenuItem;
    ImprimerPlanMenu: TMenuItem;
    ImprimerListingMenu: TMenuItem;
    Squencdexcutiondesordres1: TMenuItem;
    Simulationdecombat1: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    MenuFlottes: TMenuItem;
    Options1: TMenuItem;
    ChercherMenuBar: TMenuItem;
    ChercherMenu: TMenuItem;
    OccSuivMenu: TMenuItem;
    FindDialog: TFindDialog;
    ReplaceDialog: TReplaceDialog;
    Remplacer1: TMenuItem;
    Personalisation1: TMenuItem;
    FontDialog: TFontDialog;
    FileHistoryMenuSeparator: TMenuItem;
    FileHistoryFic1: TMenuItem;
    FileHistoryFic2: TMenuItem;
    FileHistoryFic3: TMenuItem;
    FileHistoryFic8: TMenuItem;
    ChercherMondeMenu: TMenuItem;
    N7: TMenuItem;
    StatusBar: TStatusBar;
    Paramtresmail1: TMenuItem;
    Testversionsplusrcentes1: TMenuItem;
    N8: TMenuItem;
    Configurationimprimante1: TMenuItem;
    N10: TMenuItem;
    PrinterSetupDialog: TPrinterSetupDialog;
    Simulationdvolutiondelapopulation1: TMenuItem;
    MenuPref: TMenuItem;
    AllerMondeCourMenu: TMenuItem;
    ChercherFlotteMenu: TMenuItem;
    ChercherTresorMenu: TMenuItem;
    Simulationdeval1: TMenuItem;
    Relationsaveclesautresjoueurs1: TMenuItem;
    AfficheCR1: TMenuItem;
    N9: TMenuItem;
    ConfigSMMenu: TMenuItem;
    N11: TMenuItem;
    FusiondeNBT1: TMenuItem;
    Simulationdevalintermdiaire1: TMenuItem;
    FileHistoryFic7: TMenuItem;
    FileHistoryFic6: TMenuItem;
    FileHistoryFic5: TMenuItem;
    FileHistoryFic4: TMenuItem;
    ChercherMondeOrdres: TMenuItem;
    Chercheruneflotteetsesordres1: TMenuItem;
    Chercheruntrsoretsesordres1: TMenuItem;
    Pointsparclasse1: TMenuItem;
    Pointsdestrsors1: TMenuItem;
    EnvoyerOrdresPP: TMenuItem;
    IndyHttp: TIdHTTP;
    EnvoiOrdresAPIMenu: TMenuItem;
    Splitter1: TSplitter;
    PanelPlan: TPanel;
    PanelDroite: TPanel;
    Planpleincran1: TMenuItem;
    PageControlCR: TPageControl;
    PanelOrdres: TPanel;
    Splitter2: TSplitter;
    PhotoMondesMenu: TMenuItem;

    procedure CreateParams(var Params: TCreateParams); override;
    procedure FormCreate(Sender: TObject);
    procedure propos1Click(Sender: TObject);
    procedure Ouvrir1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Quitter1Click(Sender: TObject);
    procedure Affichageduplan1Click(Sender: TObject);
    procedure Sauverlesordres1Click(Sender: TObject);
    procedure Vrificationdesordres1Click(Sender: TObject);
    procedure Listedesordres1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Aide2Click(Sender: TObject);
    procedure NiveaudeDEPlacement1Click(Sender: TObject);
    procedure MenuPlanClick(Sender: TObject);
    procedure MenuOrdresClick(Sender: TObject);
    procedure MenuListingClick(Sender: TObject);
    procedure MenuMosaiqueHClick(Sender: TObject);
    procedure MenuMosaiqueVClick(Sender: TObject);
    procedure MenuCascadeClick(Sender: TObject);
    procedure Rorganiserlesicnes1Click(Sender: TObject);
    procedure ImprimerPlanMenuClick(Sender: TObject);
    procedure ImprimerListingMenuClick(Sender: TObject);
    procedure Squencdexcutiondesordres1Click(Sender: TObject);
    procedure Simulationdecombat1Click(Sender: TObject);
    procedure MenuFlottesClick(Sender: TObject);
    procedure ChercherMenuClick(Sender: TObject);
    procedure FindDialogFind(Sender: TObject);
    procedure OccSuivMenuClick(Sender: TObject);
    procedure Remplacer1Click(Sender: TObject);
    procedure Personalisation1Click(Sender: TObject);
    procedure FileHistoryOpen(Sender: TObject);
    procedure ChercherMondeMenuClick(Sender: TObject);
    procedure SendMailDone(Sender: TObject);
    procedure Paramtresmail1Click(Sender: TObject);
    procedure Testversionsplusrcentes1Click(Sender: TObject);
    procedure Configurationimprimante1Click(Sender: TObject);
    procedure Simulationdvolutiondelapopulation1Click(Sender: TObject);
    procedure Slectionnertout1Click(Sender: TObject);
    procedure Copier1Click(Sender: TObject);
    procedure Couper1Click(Sender: TObject);
    procedure Coller1Click(Sender: TObject);
    procedure MenuPrefClick(Sender: TObject);
    procedure AllerMondeCourMenuClick(Sender: TObject);
    procedure ChercherFlotteMenuClick(Sender: TObject);
    procedure ChercherTresorMenuClick(Sender: TObject);
    procedure Simulationdeval1Click(Sender: TObject);
    procedure Relationsaveclesautresjoueurs1Click(Sender: TObject);
    procedure AfficheCR1Click(Sender: TObject);
    procedure ConfigSMMenuClick(Sender: TObject);
    procedure FusiondeNBT1Click(Sender: TObject);
    procedure Simulationdevalintermdiaire1Click(Sender: TObject);
    procedure ChercherMondeOrdresClick(Sender: TObject);
    procedure Chercheruneflotteetsesordres1Click(Sender: TObject);
    procedure Chercheruntrsoretsesordres1Click(Sender: TObject);
    procedure Pointsparclasse1Click(Sender: TObject);
    procedure Pointsdestrsors1Click(Sender: TObject);
    procedure EnvoyerOrdresPPClick(Sender: TObject);
    procedure EnvoiOrdresAPIMenuClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure Splitter2Moved(Sender: TObject);
    procedure Planpleincran1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormAfterMonitorDpiChanged(Sender: TObject; OldDPI,
      NewDPI: Integer);
    procedure PhotoMondesMenuClick(Sender: TObject);
  private
    { Private-d�clarations }
    FOrdreExcl: array [1 .. 1024] of boolean;
    MProdOK: array [1 .. 1024] of boolean;
    NomFichierAVD: string;
    VTFl, VCFl, VI, VP: array [1 .. 1024] of integer;
    FenRechercheCourante: TForm;
    FileHistory: TStringList;
    UPProduites: array [1 .. 1024] of integer;
    TechInvestie: array [1 .. 6] of integer;
    Espionnage: array [1 .. 9] of integer;
    TempoFicList: TStringList;
    PageSimulEval: integer;
    PageFlottes: integer;
    PageCR: integer;
    PageVerifOrdres: integer;

    procedure OuvreFic(FileName: string);
    procedure WMDropFiles(var MsgInfo: TWMDropFiles); message WM_DROPFILES;
    function AllieToString: string;
    procedure StringToAllie(st: string);
    function CheckNBTExt: boolean;
    procedure RegisterNBT;
    procedure MenuAPI(Enable: boolean);
    procedure EnableMenuMail;
    procedure EnleveComment(List: TStringList);
    procedure RetourLigne(List: TStringList; tailleMax: integer);
    procedure SimulEval(EtapeStop: integer);
    procedure ReadHistory;
    procedure WriteHistory;
    procedure AddHistory(FileName: string);
    procedure ShowHelp(contextId: integer);
    procedure ShowPanels(show: boolean);
    procedure SetPanelsLayout;
    procedure CloseChildrenWindows;
    procedure ClosePageControlCRChildren;
  public
    { Public-d�clarations }
    MWData: TNebData;
    RepFic: string;
    LigneNb, LigneNbInv: array [0 .. 1024] of integer;
    Poids: array [0 .. 1024] of integer;
    Cle: integer;
    Allie: array [0 .. 32] of integer;
    Ini: TMemIniFile;
    MemoFont: TFont;
    Email, SMTPServer, AdresseServeurNebula, Password: string;
    AdresseSMDansNBT, VerifConnex: boolean;
    UseProxy: boolean;
    ProxyName: string;
    ProxyPort: integer;
    CC: boolean;
    EnvoiMail: TEnvoiMail;
    DureeDepuisCompile: integer;
    Xoff, Yoff: integer;
    mHelpFile: string; // Variable servant � stocker le chemin du fichier CHM
    DefaultEncoding: TEncoding;
    PanelPlanWidthRatio, PanelCRHeightRatio: integer;

    function StrToEtatFen(st: string): TWindowState;
    Procedure Traite2(var st: string; var O: TOrdre; jn: integer);
    procedure RempliFlottes;
    function EtatFenToStr(ws: TWindowState): string;
    procedure UpdateFont;
    function GetOrdresFileName: string;
    function GetAdresseSM: string;
  end;

var
  NebWin: TNebWin;

implementation

uses
  Ordres, Resultat, Search, {BigText,} VisuList, WinPlan, ImpMonde,
  ConfigReseau, EnvoiSplash, Registry, Prtpform,
  SimuPop, Histo, Preferences, AvatEval,
  Imprime, VisuEvalCR, RelationJous, Chemin, ModifSM, FileCtrl,
  EnvoiMail, System.IOUtils, MapCoord, System.UITypes, ExtraitCR;

{$R ..\Shared\NEBULA2.RES ..\Shared\NEBULA2.RC}
{$R *.DFM}

const
  EspionCh: array [1 .. 9] of string = ('Capacit�s mini�res', 'Populations',
    'Cargaison transport�e', 'Mondes', 'Flottes', 'Industries', 'Vaisseaux',
    'Tr�sors', 'Industries actives');
  EspionPrec: array [1 .. 7] of integer = (50, 30, 20, 10, 5, 2, 0);

procedure TNebWin.FormClose(Sender: TObject; var Action: TCloseAction);
var
  AffichageCh: string;
  SectionName: string;

begin
  Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
  Ini.AutoSave := True;
  FindDialog.CloseDialog;
  if MWData <> nil then
    if (MWData.NomPartie <> '') and (MWData.NoJou > 0) then
    begin
      Ini.WriteString(MWData.NomPartie, 'Allie', AllieToString);
      Ini.WriteInteger(MWData.NomPartie, 'DEP', MWData.EvalDEP);
    end;

  // Disposition des fene�tres

  // Fen�tre principale
  if WindowState <> wsmaximized then
  begin
    Ini.WriteInteger('Fenetres', 'Nebutil_Gauche', Left);
    Ini.WriteInteger('Fenetres', 'Nebutil_Haut', Top);
    Ini.WriteInteger('Fenetres', 'Nebutil_Hauteur', Height);
    Ini.WriteInteger('Fenetres', 'Nebutil_Largeur', Width);
  end;
  Ini.WriteString('Fenetres', 'Nebutil_Etat', EtatFenToStr(WindowState));

  // Largeur fen�tre plan
  Ini.WriteInteger('Fenetres', 'Plan_Largeur_Ratio', PanelPlanWidthRatio);

  // Hauteur CR
  Ini.WriteInteger('Fenetres', 'CR_Hauteur_Ratio', PanelCRHeightRatio);

  if PlanForm <> nil then
  begin
    Ini.WriteInteger('Fenetres', 'Plan_Taille_Monde', PlanForm.WinPlan.fTailleMonde);

    case PlanForm.WinPlan.Affichage of
      ta_NoMonde:
        AffichageCh := 'No_Monde';
      ta_Economique:
        AffichageCh := 'Economique';
      ta_Militaire:
        AffichageCh := 'Militaire';
      ta_Explo:
        AffichageCh := 'Explo';
    end;
    Ini.WriteString('Fenetres', 'Plan_Affichage', AffichageCh);

    // Plan
    SectionName := Format('%s-Plan%.2d', [MWData.NomPartie, MWData.NoJou]);
    Ini.WriteString(SectionName, 'Mds', PlanForm.GetMDepSup);

    Ini.WriteInteger(SectionName, 'XOff', Xoff);
    Ini.WriteInteger(SectionName, 'YOff', Yoff);
  end;

  { Fontes }
  Ini.WriteString('Fonte', 'Nom', MemoFont.Name);
  Ini.WriteInteger('Fonte', 'Taille', MemoFont.Size);

  { Email }
  Ini.WriteString('Email', 'Email', Email);
  Ini.WriteString('Email', 'SMTP_Server', SMTPServer);
  Ini.WriteString('Email', 'Adresse_Serveur_Mail', AdresseServeurNebula);
  Ini.WriteBool('Email', 'Adresse_SM_NBT', AdresseSMDansNBT);
  Ini.WriteBool('Email', 'VerifConnex', VerifConnex);
  Ini.WriteBool('Email', 'CC', CC);
  if EnvoiMail = te_SMTP then
    Ini.WriteString('Email', 'Methode_Envoi_Mail', 'SMTP')
  else
    Ini.WriteString('Email', 'Methode_Envoi_Mail', 'None');
  Ini.WriteBool('Email', 'Use_Proxy', UseProxy);
  Ini.WriteString('Email', 'Proxy_Name', ProxyName);
  Ini.WriteInteger('Email', 'Proxy_Port', ProxyPort);

end;

procedure TNebWin.FormCreate(Sender: TObject);
var
  st: string;
begin
{$IFDEF DEBUG}
  Caption := Caption + ' --- DEBUG VERSION !!!!!!!!!!!!!!!!!';
{$ENDIF}
  DefaultEncoding := TEncoding.ANSI;
  PageSimulEval := -1;
  PageFlottes := -1;
  PageCR := -1;
  PageVerifOrdres := -1;

  mHelpFile := ExtractFilePath(ParamStr(0)) + 'nebutil.chm';
  mHelpFile := ExpandFileName(mHelpFile);
  if (not FileExists(mHelpFile)) then
    // D�clench� si le fichier d'aide est introuvable
    ShowMessage('Le fichier d''aide est introuvable !');

  Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
  Ini.AutoSave := True;

  FileHistory := TStringList.Create;

  if not CheckNBTExt then
    RegisterNBT;

  WindowMenu := MenuFenetre;

  PanelPlanWidthRatio := 50;
  PanelCRHeightRatio := 50;

  // Fen�tre principale
  Left := Ini.ReadInteger('Fenetres', 'Nebutil_Gauche', Left);
  Top := Ini.ReadInteger('Fenetres', 'Nebutil_Haut', Top);
  Height := Ini.ReadInteger('Fenetres', 'Nebutil_Hauteur', Height);
  Width := Ini.ReadInteger('Fenetres', 'Nebutil_Largeur', Width);
  WindowState := StrToEtatFen(Ini.ReadString('Fenetres', 'Nebutil_Etat',
    'Normale'));

  // Ratios pour les splitters plan / CR
  PanelPlanWidthRatio := Ini.ReadInteger('Fenetres', 'Plan_Largeur_Ratio',
    PanelPlanWidthRatio);
  PanelCRHeightRatio := Ini.ReadInteger('Fenetres', 'CR_Hauteur_Ratio',
    PanelCRHeightRatio);

  MemoFont := TFont.Create;
  MemoFont.Name := Ini.ReadString('Fonte', 'Nom', 'Lucida Console');
  MemoFont.Size := Ini.ReadInteger('Fonte', 'Taille', 11);
  Font.Assign(MemoFont);

  Email := Ini.ReadString('Email', 'Email', '');
  SMTPServer := Ini.ReadString('Email', 'SMTP_Server', '');
  AdresseServeurNebula := Ini.ReadString('Email', 'Adresse_Serveur_Mail',
    adresseSMdefaut);
  AdresseSMDansNBT := Ini.ReadBool('Email', 'Adresse_SM_NBT', True);
  VerifConnex := Ini.ReadBool('Email', 'VerifConnex', True);
  CC := Ini.ReadBool('Email', 'CC', False);
  st := Ini.ReadString('Email', 'Methode_Envoi_Mail', 'None');
  if st = 'SMTP' then
    EnvoiMail := te_SMTP
  else
    EnvoiMail := te_None;
  UseProxy := Ini.ReadBool('Email', 'Use_Proxy', False);
  ProxyName := Ini.ReadString('Email', 'Proxy_Name', '');
  ProxyPort := Ini.ReadInteger('Email', 'Proxy_Port', 0);
  ReadHistory;

  DragAcceptFiles(Handle, True);

  RepFic := '';
  FillChar(Allie, sizeof(Allie), 0);
  FlottesForm := nil;
  Cle := 1;

  TempoFicList := TStringList.Create;
  StatusBar.Panels.Items[4].Text := '';
  Ini.Free;

  // Masque les panel de positionnement des fen�tres filles
  ShowPanels(False);
  SetPanelsLayout;

end;

procedure TNebWin.propos1Click(Sender: TObject);
begin
  if not Assigned(Apropos) then
    Apropos := TApropos.Create(Self);

  Apropos.ShowModal;
end;

{ ********* Ouverture du fichier ********* }
{ Il y a plein d'initialisations � faire ici }
procedure TNebWin.OuvreFic(FileName: string);
var
  Ch: string;
  jou: integer;
  tempo: integer;
  SectionName: string;
  CR: TSTringList;

  //CRdyn: boolean;

begin
  //CRdyn := False;
  if (ExtractFileExt(FileName).ToUpper <> '.NBT') and
    (ExtractFileExt(FileName).ToUpper <> '.NBA') then
  begin
    ShowMessage('Type de fichier inconnu');
    Exit;
  end;

  if not FileExists(FileName) then
  begin
    ShowMessage(Format('Le fichier %s n''existe pas', [FileName]));
    Exit;
  end;

  CloseChildrenWindows;

  RepFic := ExtractFilePath(FileName);

  CR := TStringList.Create;

  Self.MWData.Free;
  Self.MWData := TNebData.Create(FileName, 0);

  // Simuler un RAD=3 pour construire le plan avec Nebutil
  if MWData.Radar < 3 then
    MapCoord.FakeRad3(RepFic, MWData);

  { Recherche des infos de personalisation dans le fichier .INI }

  Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
  Ini.AutoSave := True;
  if MWData.NoJou > 0 then
  begin
    StringToAllie(Ini.ReadString(MWData.NomPartie, 'Allie',
      '0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'));
    Allie[MWData.NoJou] := 1;
    MWData.EvalDEP := MWData.jou[MWData.NoJou].DEP;
    tempo := Ini.ReadInteger(MWData.NomPartie, 'DEP',
      MWData.jou[MWData.NoJou].DEP);
    if MWData.EvalDEP < tempo then
      MWData.EvalDEP := tempo;

   // Menu toujours activ� car utilis� pour mail + utilisation de l'API
    ConfigSMMenu.Enabled := True;

    Password := Ini.ReadString('Password',
      Format('%s:%d', [MWData.NomPartie, MWData.NoJou]), '');

    // Relations avec les autres joueurs
    SectionName := Format('%s-Relations%.2d', [MWData.NomPartie, MWData.NoJou]);
    MWData.ConfianceVaisseaux := Ini.ReadInteger(SectionName,
      'ConfianceMax', 0);
    for jou := 1 to MWData.NbJou do
      if (jou <> MWData.NoJou) and MWData.ConnuDeNom[MWData.NoJou, jou] then
      begin
        MWData.jou[jou].Classe := Ini.ReadInteger(SectionName,
          Format('Classe%.2d', [jou]), 0);
        MWData.Confiance[jou] := Ini.ReadInteger(SectionName,
          Format('Confiance%.2d', [jou]), 3);
        if jou = MWData.NoJou then
          MWData.Confiance[jou] := 0;
        if MWData.jou[jou].ATT = 0 then
        begin
          MWData.jou[jou].ATT := Ini.ReadInteger(SectionName,
            Format('ATT%.2d', [jou]), 0);
          MWData.jou[jou].CA := CalculeCA(MWData.jou[jou].ATT);
        end;
        if MWData.jou[jou].DEF = 0 then
        begin
          MWData.jou[jou].DEF := Ini.ReadInteger(SectionName,
            Format('DEF%.2d', [jou]), 0);
          MWData.jou[jou].CD := CalculeCD(MWData.jou[jou].DEF);
        end;
        if Ini.ReadBool(SectionName, Format('Allie%.2d', [jou]), False) then
          MWData.DeclareAllie(jou, MWData.NoJou)
        else
          MWData.DeclareEnnemi(jou, MWData.NoJou);
        if Ini.ReadBool(SectionName, Format('Chargeur%.2d', [jou]), False) then
          MWData.DeclareChargeur(jou, MWData.NoJou)
        else
          MWData.DeclareNonChargeur(jou, MWData.NoJou);
        if Ini.ReadBool(SectionName, Format('ChargeurPop%.2d', [jou]), False)
        then
          MWData.DeclareChargeurPop(jou, MWData.NoJou)
        else
          MWData.DeclareNonChargeurPop(jou, MWData.NoJou);
        if Ini.ReadBool(SectionName, Format('DechargeurPop%.2d', [jou]), False)
        then
          MWData.DeclareDechargeurPop(jou, MWData.NoJou)
        else
          MWData.DeclareNonDechargeurPop(jou, MWData.NoJou);
      end;

    // Informations concernant le plan
    SectionName := Format('%s-Plan%.2d', [MWData.NomPartie, MWData.NoJou]);
    MWData.MDepSup.CommaText := MWData.MDepSup.CommaText + ',' +
      Ini.ReadString(SectionName, 'Mds', '');
    // OG 30/10/01 Enlever les doublons
    MWData.MDepSup.VireDoublons;
    Xoff := Ini.ReadInteger(SectionName, 'XOff', (MWData.Map.LongX div 2) -
      MWData.M[MWData.MDep].x);
    Yoff := Ini.ReadInteger(SectionName, 'YOff', (MWData.Map.LongY div 2) -
      MWData.M[MWData.MDep].y);
  end;

  NomFichierAVD := FileName;
  StatusBar.Panels.Items[0].Text := Format('Partie %s', [MWData.NomPartie]);
  if MWData.NoJou > 0 then
    StatusBar.Panels.Items[1].Text :=
      Format('"%s":%d', [MWData.Pseudos[MWData.NoJou], MWData.NoJou])
  else
    StatusBar.Panels.Items[1].Text := '';
  StatusBar.Panels.Items[2].Text := Format('Tour %d', [MWData.Tour]);
  if MWData.NoJou > 0 then
    StatusBar.Panels.Items[3].Text := Format('DEP=%d', [MWData.EvalDEP])
  else
    StatusBar.Panels.Items[3].Text := '';
  StatusBar.Panels.Items[4].Text := '';

  // G�n�ration du CR texte
  if FileExists(MWData.GetNomListing) then
    CR.LoadFromFile(MWData.GetNomListing)
  else
    ImprimeData(MWData, nil, MWData.NoJou, CR);

    if MWData.TypeFicDonnees = tfd_NBA then
  begin
    PlanForm := TPlanForm.Create2(Application, MWData, PlanSansCR);
  end
  else
  begin
    PlanForm := TPlanForm.Create2(Application, MWData, PlanAvecCR);
    PlanForm.WinPlan.Xoff := Xoff;
    PlanForm.WinPlan.Yoff := Yoff;
  end;
  PlanForm.Align := alCLient;
  PlanForm.ManualDock(PanelPlan);
  PlanForm.show;

  if MWData.TypeFicDonnees <> tfd_NBA then
  begin
    OrdresForm := TOrdresForm.CreateData(Application, MWData, CR);
    OrdresForm.Align := alCLient;
    OrdresForm.ManualDock(PanelOrdres);
    OrdresForm.show;
  end;

  // Affichage fen�tre CR
  VisuListingForm := TVisuListingForm.Create(Self, CR);
  VisuListingForm.Align := alCLient;
  VisuListingForm.dragkind := dkDock;

  // Accrochage au PageControl
  if PageCR = -1 then
  begin
    VisuListingForm.ManualDock(PageControlCR);
    PageCR := PageControlCR.PageCount - 1;
  end;

  PageControlCR.ActivePageIndex := PageCR;

  VisuListingForm.show;

  Ch := IntToStr(MWData.NoJou);
  if MWData.NoJou < 10 then
    Ch := '0' + Ch;

  ImprimerPlanMenu.Enabled := True;
  ImprimerListingMenu.Enabled := True;
  MenuFlottes.Enabled := True;

  UpdateFont;

  AddHistory(FileName);

  Caption := 'Nebutil - ' + FileName;
  Ini.Free;

  EnableMenuMail;

  CR.Free;

  // Affiche les panels de positionnement des fen�tres filles
  ShowPanels(True);
  PlanForm.SetFocus;
end;

procedure TNebWin.Ouvrir1Click(Sender: TObject);
begin
  if OpenDialog.Execute then
    OuvreFic(OpenDialog.FileName);
end;

function TNebWin.EtatFenToStr(ws: TWindowState): string;
begin
  case ws of
    wsNormal:
      Result := 'Normale';
    wsMinimized:
      Result := 'Normale'; // fix pour windows10
    wsmaximized:
      Result := 'Max';
  end;
end;

function TNebWin.StrToEtatFen(st: string): TWindowState;
begin
  Result := wsNormal;
  if st = 'Normale' then
    Result := wsNormal;
  if st = 'Icone' then
    Result := wsNormal; // fix pour windows10
  if st = 'Max' then
    Result := wsmaximized;
end;

procedure TNebWin.FormDestroy(Sender: TObject);
begin
  if TempoFicList <> nil then
    while TempoFicList.Count > 0 do
    begin
      DeleteFile(PChar(TempoFicList[0]));
      TempoFicList.Delete(0);
    end;

  if Assigned(VisuListingForm) then
    VisuListingForm.Free;
  if Assigned(Apropos) then
    Apropos.Free;

  MemoFont.Free;
  FileHistory.Free;
  MWData.Free;
  TempoFicList.Free;
  Ini.Free;
end;

procedure TNebWin.FormResize(Sender: TObject);
begin
  PanelPlan.Width := Round(ClientWidth * (PanelPlanWidthRatio / 100));
  PageControlCR.Height := Round(ClientHeight * (PanelCRHeightRatio / 100));
end;

function TNebWin.AllieToString: string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to 32 do
  begin
    if MWData.EstAllie(MWData.NoJou, i) then
      Result := Result + '1'
    else
      Result := Result + '0';
    if i < 32 then
      Result := Result + ',';
  end;
end;

procedure TNebWin.ShowPanels(show: boolean);
begin
  if show then
  begin
    Splitter1.show;
    PanelPlan.show;
    PanelDroite.show;
    FormResize(nil);
  end
  else
  begin
    Splitter1.Hide;
    PanelPlan.Hide;
    PanelDroite.Hide;
  end;
end;

procedure TNebWin.ShowHelp(contextId: integer);
begin
  if (contextId = 0) then
    HtmlHelp(0, PChar(mHelpFile), HH_DISPLAY_TOC, 0)
  else
    HtmlHelp(0, PChar(mHelpFile), HH_HELP_CONTEXT, contextId);
end;

procedure TNebWin.StringToAllie(st: string);
var
  i: integer;
  splitted: TArray<String>;
begin
  splitted := st.Split([','], 31);
  for i := 0 to length(splitted) - 1 do
  begin
    if splitted[i].trim = '1' then
      Allie[i] := 1;
  end;
end;

procedure TNebWin.Quitter1Click(Sender: TObject);
begin
  Close;
end;

procedure TNebWin.Affichageduplan1Click(Sender: TObject);
begin
  PlanForm.show;
end;

procedure TNebWin.Sauverlesordres1Click(Sender: TObject);
begin
  if not Assigned(MWData) then
    Exit;
  if MWData.NoJou > 0 then
    OrdresForm.SaveOrdres;
end;

procedure TNebWin.Vrificationdesordres1Click(Sender: TObject);
var
  st: string;
  i, tech, espion, TotalEspion: integer;
  nb: integer;
  Bid: integer;
  TotalProd, TotalAct: integer;
  List, VerifOrdresList: TStringList;
  DebutComment: integer;
  EspPrec: integer;
  O, O2: TOrdre;
  NebOrdres: TNebOrdres;
begin
  if not Assigned(MWData) then
    Exit;
  NebOrdres := TNebOrdres.Create;
  VerifOrdresList := TStringList.Create;
  VerifOrdresList.Clear;
  if MWData.NoJou > 0 then
  begin
    { Initialisations }
    FillChar(FOrdreExcl, sizeof(FOrdreExcl), 0);
    FillChar(MProdOK, sizeof(MProdOK), 0);
    FillChar(UPProduites, sizeof(UPProduites), 0);
    FillChar(TechInvestie, sizeof(TechInvestie), 0);
    FillChar(Espionnage, sizeof(Espionnage), 0);
    FillChar(VTFl, sizeof(VTFl), 0);
    FillChar(VCFl, sizeof(VCFl), 0);
    FillChar(VI, sizeof(VI), 0);
    FillChar(VP, sizeof(VP), 0);

    for i := 1 to MWData.NbMonde do
    begin
      VI[i] := MWData.M[i].VI;
      VP[i] := MWData.M[i].VP;
    end;

    for i := 1 to 1024 do
    begin
      VCFl[i] := MWData.f[i].NbVC;
      VTFl[i] := MWData.f[i].NbVT;
    end;

    nb := 0;
    for i := 0 to OrdresForm.mwEdit.Lines.Count - 1 do
    begin
      st := OrdresForm.mwEdit.Lines.Strings[i];
      if (st <> '') and (st <> 'Jo') then
      // if (st <> '') and ((st[1] <> 'J') and (st[2] <> 'o')) then
      begin
        O.erreur := 0;
        if not st.StartsWith(';') and not st.StartsWith('#') then
        begin
          Bid := MWData.NoJou;
          NebOrdres.Traite(st, O, Bid);
          NebOrdres.ParseOrdre(st, O2);
          O.Ch := O2.Ch;
          if O.Ch[1] <> '' then
            inc(nb);

          if O.erreur = 0 then
            Traite2(st, O, MWData.NoJou);
          if O.erreur = 21 then
            st := '!!! ' + st + ' -> Erreur de syntaxe'
          else if (O.erreur >= 42) and (O.erreur <= 45) then
            st := '!!! ' + st + ' -> Erreur dans un num�ro';
          if ((st[1] = '?') or (st[1] = '!')) and
            (not st.Contains('@OK@') or (O.erreur = 21)) then
          begin
            VerifOrdresList.Add(st);
            VerifOrdresList.Objects[VerifOrdresList.Count - 1] := TObject(i);
          end;
        end
        else if st.ToUpper.Contains('@RIEN@') then
        begin
          Bid := MWData.NoJou;
          DebutComment := 0;
          if st.Contains(';') then
            DebutComment := st.IndexOf(';');
          if st.Contains(',') and (st.IndexOf(',') < DebutComment) then
            DebutComment := st.IndexOf(',');
          NebOrdres.ParseOrdre(st.Substring(DebutComment + 1), O);
          if (O.Ch[1] = 'F') and (O.Ch[3] = '@RIEN@') then
          begin
            FOrdreExcl[O.O[1]] := True;
          end;
        end
        else if st.ToUpper.Contains('@PRODOK@') then
        begin
          Bid := MWData.NoJou;
          DebutComment := 0;
          if st.Contains(';') then
            DebutComment := st.IndexOf(';');
          if st.Contains(',') and (st.IndexOf(',') < DebutComment) then
            DebutComment := st.IndexOf(',');
          NebOrdres.ParseOrdre(st.Substring(DebutComment + 1), O);
          if (O.Ch[1] = 'M') and (O.Ch[3] = '@PRODOK@') then
          begin
            MProdOK[O.O[1]] := True;
          end;
        end
      end;
    end;

    { On v�rifie que toutes les flottes ont bien re�u un ordre exclusif }
    List := TStringList.Create;
    for i := 1 to 1024 do
    begin
      if (MWData.f[i].Proprio = MWData.NoJou) and not FOrdreExcl[i] then
      begin
        List.Add(Format
          ('%.4d %.4d F_%1:d (M_%0:d) n''a pas re�u d''ordre exclusif',
          [MWData.f[i].Localisation, i]));
      end;
    end;
    List.Sort;
    for i := 0 to List.Count - 1 do
    begin
      VerifOrdresList.Add('??? ' + List[i].Substring(10));
      VerifOrdresList.Objects[VerifOrdresList.Count - 1] :=
        TObject(LE_FLOTTE_EXCLUSIF);
    end;
    List.Free;

    { On v�rifie que la production tient debout }
    for i := 1 to 1024 do
      if (MWData.M[i].Proprio = MWData.NoJou) and (not MProdOK[i]) then
      begin
        if UPProduites[i] < MWData.ME[i].IndLibre then
        begin
          VerifOrdresList.Add
            (Format('??? M_%-3d : Vous n''avez pas utilis� toute votre production (%d/%d)',
            [i, UPProduites[i], MWData.ME[i].IndLibre]));
          VerifOrdresList.Objects[VerifOrdresList.Count - 1] :=
            TObject(LE_MONDE_PRODUCTION);
        end
        else if UPProduites[i] > MWData.ME[i].IndLibre then
        begin
          VerifOrdresList.Add
            (Format('!!! M_%-3d : Vous avez d�pens� %d UP alors que le monde ne peut en produire que %d',
            [i, UPProduites[i], MWData.ME[i].IndLibre]));
          VerifOrdresList.Objects[VerifOrdresList.Count - 1] :=
            TObject(LE_MONDE_PRODUCTION);
        end;
      end;

    { bilan de la production }
    TotalProd := 0;
    TotalAct := 0;
    for i := 1 to 1024 do
    begin
      if MWData.M[i].Proprio = MWData.NoJou then
      begin
        inc(TotalProd, UPProduites[i]);
        inc(TotalAct, MWData.ME[i].IndLibre);
      end;
    end;

    if nb > 1 then
      VerifOrdresList.Add(Format('%d ordres �crits.', [nb]))
    else if nb = 1 then
      VerifOrdresList.Add('1 ordre �crit.')
    else
      VerifOrdresList.Add('pas d''ordre �crit.');
    VerifOrdresList.Objects[VerifOrdresList.Count - 1] := TObject(LE_RIEN);

    { Bilan de la production }
    VerifOrdresList.Add(Format('Vous avez produit %d UP sur %d',
      [TotalProd, TotalAct]));
    VerifOrdresList.Objects[VerifOrdresList.Count - 1] := TObject(LE_RIEN);

    { Bilan des investissements en technologies }
    for tech := 1 to 6 do
    begin
      if TechInvestie[tech] > 0 then
      begin
        VerifOrdresList.Add(Format('Vous avez investi %d UP en %s',
          [TechInvestie[tech], TechCh[tech]]));
        VerifOrdresList.Objects[VerifOrdresList.Count - 1] := TObject(LE_RIEN);
      end;
    end;

    // Bilan de l'espionnage
    TotalEspion := 0;
    for espion := 1 to 9 do
      inc(TotalEspion, Espionnage[espion]);

    if TotalEspion > 0 then
    begin
      VerifOrdresList.Add('Espionnage :');
      VerifOrdresList.Objects[VerifOrdresList.Count - 1] := TObject(LE_RIEN);
      for espion := 1 to 9 do
        if Espionnage[espion] > 0 then
        begin
          EspPrec := Espionnage[espion];
          if EspPrec > 7 then
            EspPrec := 7;
          VerifOrdresList.Add(Format('  %d UP en %s -> Erreur = %d%%',
            [Espionnage[espion], EspionCh[espion], EspionPrec[EspPrec]]));
          VerifOrdresList.Objects[VerifOrdresList.Count - 1] :=
            TObject(LE_RIEN);
        end;
    end;

    if ResultForm = nil then
      ResultForm := TResultForm.Create(Self);

    if PageVerifOrdres = -1 then
    begin
      ResultForm.ManualDock(PageControlCR);
      PageVerifOrdres := PageControlCR.PageCount - 1;
    end;

    ResultForm.FillListView(VerifOrdresList);
    PageControlCR.ActivePageIndex := PageVerifOrdres;
    ResultForm.show;
  end;
  VerifOrdresList.Free;
  NebOrdres.Free;
end;

{ Cette proc�dure fait des tests sur le proprietaire des mondes et flottes
  et sur les connexions lors des d�placements }

Procedure TNebWin.Traite2(var st: string; var O: TOrdre; jn: integer);
var
  i, j: integer;
  OK: boolean;
  MDep, Marr: Word;
  stop: boolean;
begin
  stop := False;
  MDep := 0;
  Marr := 0;
  if O.Ch[1] = 'F' then
    if (MWData.f[O.O[1]].Proprio <> MWData.NoJou) then
    begin
      { L'auteur ne poss�de pas la flotte... }
      if O.TypeO = 20 then
        { Cadeau -> Warning : on peut capturer ou pirater avant }
        st := '??? ' + st
      else
      begin
        O.erreur := 1;
        st := '!!! ' + st;
      end;
      st := st + ' -> Vous n''�tes pas propri�taire de la flotte';
    end;
  if O.Ch[1] = 'M' then
    if MWData.M[O.O[1]].Proprio <> MWData.NoJou then
    begin
      { Warning : le monde n'appartient pas au joueur. Il est cependant possible
        qu'il le capture pendant l'�migration et puisse donner des ordres
        ensuite. ceci n'est possible que si le monde est neutre }
      if (MWData.M[O.O[1]].Proprio > 0) and (O.TypeO <> 20) and (O.TypeO <> 18)
      then
      begin
        O.erreur := 1;
        st := '!!! ' + st;
      end
      else
        st := '??? ' + st;
      st := st + ' -> Vous n''�tes pas propri�taire du monde';
    end;

  { On compte le nombre d'ordre exclusif que re�oit chaque flotte }
  if (O.Ch[1] = 'F') and ((O.TypeO = 14) or (O.TypeO = 20)) then
  begin
    if FOrdreExcl[O.O[1]] then
    begin
      O.erreur := 1;
      st := '!!!' + st + ' -> La flotte ' + IntToStr(O.O[1]) +
        ' a d�j� re�u un ordre exclusif';
    end
    else
      FOrdreExcl[O.O[1]] := True;
  end;

  { V�rification de la propri�t� si l'ordre commence
    par VI ou VP }
  if (O.erreur = 0) and ((O.Ch[1] = 'VI') or (O.Ch[1] = 'VP')) then
  begin
    if MWData.M[O.O[1]].Proprio <> MWData.NoJou then
    begin
      if MWData.M[O.O[1]].Proprio = 0 then
        st := '??? ' + st
      else { on ne peut pas �migrer }
      begin
        O.erreur := 1;
        st := '!!! ' + st;
      end;
      st := st + ' -> Vous n''�tes pas propri�taire du monde';
    end;
  end;

  { d�placement d'une flotte }
  if (O.erreur = 0) and (O.TypeO = 14) and (O.STypeO = 11) then
  begin
    i := 2;
    MDep := MWData.f[O.O[1]].Localisation;
    if MDEP > 0 then
    begin
      Marr := 0;
      while O.O[i] <> 0 do
      begin
        Marr := O.O[i];

        { on teste si la connexion Mdep -> Marr existe }
        if (MWData.M[MDep].Connect[1] > 0) then
        begin
          OK := False;
          for j := 1 to 8 do
            if MWData.M[MDep].Connect[j] = Marr then
              OK := True;
          if not OK then
          begin
            O.erreur := 1;
            st := '!!! ' + st + ' -> Les mondes ' + IntToStr(MDep) + ' et ' +
              IntToStr(Marr) + ' ne sont pas connect�s';
          end;
        end;

        MDep := Marr;
        inc(i);
        if i > 8 then
          break;
      end;
      if i - 2 > MWData.EvalDEP then
      begin
        O.erreur := 1;
        st := '!!! ' + st + ' -> Vous n''avez pas assez en DEP';
      end;
    end
  end;

  { Sondage ou �migration }
  if (O.erreur = 0) and ((O.TypeO = 2) or (O.TypeO = 5)) then
  begin
    { Sondage }
    if O.TypeO = 2 then
    begin
      Marr := O.O[2];
      { VI ou VP qui sondent }
      if (O.STypeO = 1) or (O.STypeO = 2) then
        MDep := O.O[1]

        { Flotte qui sonde }
      else
        MDep := MWData.f[O.O[1]].Localisation;
    end

    { Emigration }
    else if (O.TypeO = 5) and (O.Ch[1] = 'M') then
    begin
      MDep := O.O[1];
      Marr := O.O[3];
    end
    else
      stop := True;

    { V�rification de la connexion }
    if not stop then
      if (MWData.M[MDep].Connect[1] > 0) or (MWData.M[Marr].Connect[1] > 0) then
      begin
        if MWData.M[MDep].Connect[1] = 0 then
        begin
          O.erreur := 1;
          st := '!!! ' + st + ' -> Les mondes ' + IntToStr(MDep) + ' et ' +
            IntToStr(Marr) + ' ne sont pas connect�s';
        end
        else
        begin
          OK := False;
          for i := 1 to 8 do
            if MWData.M[MDep].Connect[i] = Marr then
              OK := True;
          if not OK then
          begin
            O.erreur := 1;
            st := '!!! ' + st + ' -> Les mondes ' + IntToStr(MDep) + ' et ' +
              IntToStr(Marr) + ' ne sont pas connect�s';
          end;
        end;
      end;
  end;

  // stop := False;
  { Transferts sur une flotte }
  if (O.erreur = 0) and (O.Ch[3] = 'T') and ((O.Ch[6] = 'F') or (O.Ch[7] = 'F'))
  then
  begin
    if O.Ch[1] = 'M' then
      MDep := O.O[1]
    else
      MDep := MWData.f[O.O[1]].Localisation;
    Marr := MWData.f[O.O[3]].Localisation;
    if Marr <> MDep then
    begin
      if O.Ch[1] = 'M' then
      begin
        O.erreur := 1;
        st := '!!! ' + st + ' -> La flotte ' + IntToStr(O.O[3]) +
          ' ne se trouve pas sur le monde ' + IntToStr(MDep);
      end
      else
      begin
        O.erreur := 1;
        st := '!!! ' + st + ' -> Les flottes ' + IntToStr(O.O[1]) + ' et ' +
          IntToStr(O.O[3]) + ' ne se trouvent pas sur le m�me monde';
      end;
    end
    else if MWData.f[O.O[3]].Proprio = 0 then
      st := '??? ' + st + Format(' -> La flotte %d est neutre !', [O.O[3]])
    else if MWData.f[O.O[3]].Proprio <> MWData.NoJou then
      st := '??? ' + st +
        Format(' -> La flotte %d appartient � un autre joueur !', [O.O[3]])

  end;

  { Transferts sur un monde }
  if (O.erreur = 0) and (O.TypeO = 11) and (O.STypeO in [5, 6]) then
  begin
    MDep := MWData.f[O.O[1]].Localisation;
    if MWData.M[MDep].Proprio = 0 then
      st := '??? ' + st + Format(' -> Le monde %d est neutre !', [MDep])
    else if MWData.M[MDep].Proprio <> MWData.NoJou then
      st := '??? ' + st +
        Format(' -> Le Monde %d appartient � un autre joueur !', [MDep])
  end;

  { Transfert de tr�sor }
  if (O.erreur = 0) and (O.Ch[1] = 'T') then
  begin
    { On v�rifie la propri�t� du tr�sor }
    if MWData.T[O.O[1]].Proprio <> MWData.NoJou then
    begin
      O.erreur := 1;
      st := '!!!' + st + ' -> Vous ne poss�dez pas le tr�sor';
    end

    { Transfert d'un tr�sor sur une flotte }
    else if O.Ch[3] = 'F' then
    begin
      if MWData.GetTresorMonde(O.O[1]) <> MWData.f[O.O[2]].Localisation then
      begin
        O.erreur := 1;
        st := '!!! ' + st + ' -> Le tr�sor ' + IntToStr(O.O[1]) +
          ' et la flotte ' + IntToStr(O.O[2]) +
          ' ne se trouvent pas sur le m�me monde';
      end
      else if (MWData.f[O.O[2]].Proprio <> MWData.NoJou) then
      begin
        { Warning : la flotte de destination n'appartient pas au joueur.
          L'ordre passe seulement si le destinataire est antiquaire,
          ce dont le joueur ne peut �tre s�r � 100% }
        st := '??? ' + st +
          ' -> La flotte de destination ne vous appartient pas'
      end
      else if (MWData.GetTresorFlotte(O.O[1]) > 0) and
        (MWData.jou[MWData.NoJou].Classe <> Antiquaire) then
      begin
        O.erreur := 1;
        st := '??? ' + st +
          ' -> Vous n''�tes pas antiquaire. Il faut que le destinataire le soit ou bien avoir largu� le tr�sor au pr�alable sur le monde';
      end;
    end;
    { Le largage d'un tr�sor sur un monde r�ussit toujours, il n'y a
      donc pas de test � effectuer }
  end;

  { Construction sur une flotte }
  if (O.erreur = 0) and (O.Ch[1] = 'M') and (O.Ch[3] = 'C') and (O.Ch[6] = 'F')
  then
  begin
    if MWData.f[O.O[3]].Localisation <> O.O[1] then
    begin
      O.erreur := 1;
      st := '!!! ' + st + ' -> La flotte ' + IntToStr(O.O[3]) +
        ' ne se trouve pas sur le monde ' + IntToStr(O.O[1]);
    end
    else if MWData.f[O.O[3]].Proprio = 0 then
      st := '??? ' + st + Format(' -> La flotte %d est neutre !', [O.O[3]])
    else if MWData.f[O.O[3]].Proprio <> MWData.NoJou then
      st := '??? ' + st +
        Format(' -> La flotte %d appartient � un autre joueur !', [O.O[3]])
  end;

  { Tirs sur une flotte }
  if (O.erreur = 0) and
    ((((O.Ch[4] = '*') or (O.Ch[4] = '?')) and (O.Ch[5] = 'F')) or
    (((O.Ch[3] = '*') or (O.Ch[3] = '?')) and (O.Ch[4] = 'F'))) then
  begin
    if O.Ch[5] = 'F' then { Ce sont les VI ou les VP qui tirent }
    begin
      MDep := O.O[1];
      Marr := MWData.f[O.O[2]].Localisation;
      if Marr <> MDep then
      begin
        O.erreur := 1;
        st := '!!! ' + st + ' -> La flotte ' + IntToStr(O.O[2]) +
          ' ne se trouve pas sur le monde ' + IntToStr(O.O[1]);
      end;
    end
    else { C'est une flotte qui tire }
    begin
      MDep := MWData.f[O.O[1]].Localisation;
      Marr := MWData.f[O.O[2]].Localisation;
      if Marr <> MDep then
      begin
        O.erreur := 1;
        st := '!!! ' + st + ' -> Les flottes ' + IntToStr(O.O[1]) + ' et ' +
          IntToStr(O.O[2]) + ' ne se trouvent pas sur le m�me monde';
      end;
    end;
    if (O.erreur = 0) and (MWData.f[O.O[2]].Proprio = MWData.NoJou) then
      st := '??? ' + st + ' -> La flotte vous appartient (Mazo ?)';
  end;

  { Construction d'UP }
  if (O.erreur = 0) and ((O.TypeO = 3) and not(O.STypeO in [6, 7, 15])) or
    ((O.TypeO = 5) and (O.STypeO in [1 .. 4])) or (O.TypeO in [4, 6]) then
  begin
    { ici O.O[2] contient toujours le nombre a construire = nb d'UP
      sauf pour la construction d'industries }

    if O.TypeO <> 6 then
    begin
      inc(UPProduites[O.O[1]], O.O[2]);

      { R�capitulatif des tech }
      if (O.TypeO = 3) and (O.STypeO in [8 .. 13]) then
      begin
        inc(TechInvestie[O.STypeO - 7], O.O[2]);
      end;

      // Espionnage
      if (O.TypeO = 3) and (O.STypeO = 5) then
      begin
        if O.O[3] <= 9 then
          inc(Espionnage[O.O[3]], O.O[2]);
      end;
    end
    else
      inc(UPProduites[O.O[1]],
        O.O[2] * Constrind[MWData.jou[MWData.NoJou].Classe])
  end;

  { Prise en compte des constructions }
  if (O.erreur = 0) and (O.TypeO = 3) then
  begin
    if O.STypeO in [1, 2, 3, 14] then
    begin
      if O.Ch[5] = 'VI' then
        inc(VI[O.O[1]], O.O[2]);
      if O.Ch[5] = 'VP' then
        inc(VP[O.O[1]], O.O[2]);
      if O.Ch[5] = 'VC' then
        inc(VCFl[O.O[3]], O.O[2]);
      if O.Ch[5] = 'VT' then
        inc(VTFl[O.O[3]], O.O[2]);
    end
    else if O.STypeO in [6, 7, 15] then
    begin
      if O.Ch[6] = 'VI' then
        inc(VI[O.O[1]], O.O[2] div 2);
      if O.Ch[6] = 'VP' then
        inc(VP[O.O[1]], O.O[2] div 2);
      if O.Ch[6] = 'F' then
        inc(VCFl[O.O[3]], O.O[2] div 2);
    end;
  end;
  { V�rifications de pr�sence des vaisseaux pour les transferts }
  if (O.erreur = 0) and (O.TypeO in [2, 11]) then
  begin
    if O.TypeO = 2 then
    begin
      if O.STypeO = 1 then
      begin
        if VI[O.O[1]] > 0 then
          Dec(VI[O.O[1]])
        else
        begin
          O.erreur := 1;
          st := '!!! ' + st + ' -> Pas assez de VI sur le monde';
        end;
      end;
      if O.STypeO = 2 then
      begin
        if VP[O.O[1]] > 0 then
          Dec(VP[O.O[1]])
        else
        begin
          O.erreur := 1;
          st := '!!! ' + st + ' -> Pas assez de VP sur le monde';
        end;
      end;
      if O.STypeO = 3 then
      begin
        if VCFl[O.O[1]] > 0 then
          Dec(VCFl[O.O[1]])
        else
        begin
          O.erreur := 1;
          st := '??? ' + st + ' -> Pas assez de VC sur la flotte';
        end;
      end;
    end
    else if O.TypeO = 11 then
    begin
      if O.Ch[5] = 'VI' then
      begin
        { source = VI }
        if VI[O.O[1]] >= O.O[2] then
          Dec(VI[O.O[1]], O.O[2])
        else
        begin
          O.erreur := 1;
          st := '!!! ' + st + ' -> Pas assez de VI sur le monde';
        end;
      end;
      if O.Ch[5] = 'VP' then
      begin
        { source = VP }
        if VP[O.O[1]] >= O.O[2] then
          Dec(VP[O.O[1]], O.O[2])
        else
        begin
          O.erreur := 1;
          st := '!!! ' + st + ' -> Pas assez de VP sur le monde';
        end;
      end;
      if O.Ch[5] = 'VC' then
      begin
        { source = VC }
        if VCFl[O.O[1]] >= O.O[2] then
          Dec(VCFl[O.O[1]], O.O[2])
        else
        begin
          O.erreur := 1;
          st := '??? ' + st + ' -> Pas assez de VC sur la flotte';
        end;
      end;
      if O.Ch[5] = 'VT' then
      begin
        { source = VT }
        if VTFl[O.O[1]] >= O.O[2] then
          Dec(VTFl[O.O[1]], O.O[2])
        else
        begin
          O.erreur := 1;
          st := '??? ' + st + ' -> Pas assez de VT sur la flotte';
        end;
      end;

      { On ajoute les vaisseaux sur la flotte de destination }
      if (O.erreur = 0) and (O.STypeO in [3, 4, 7, 8, 9, 10, 11, 12]) then
      begin
        if O.STypeO in [8, 10] then { transfert de VT }
          inc(VTFl[O.O[3]], O.O[2])
        else if O.STypeO = 9 then // VC -> VT sur la m�me flotte
          inc(VCFl[O.O[1]], O.O[2])
        else
          inc(VCFl[O.O[3]], O.O[2]);
      end;
    end;
  end;

  { D�placement des flottes vides }
  if (O.TypeO = 14) and (O.STypeO = 11) then
  begin
    if VCFl[O.O[1]] + VTFl[O.O[1]] = 0 then
    begin
      st := '??? ' + st + ' -> La flotte est vide';
    end;
  end;

  { Cadeau du monde de d�part }
  if O.erreur = 0 then
    if (O.TypeO = 20) and (O.STypeO = 2) then
      if O.O[1] = MWData.MDep then
      begin
        // O.Erreur = 1;
        st := Format('??? %s -> Vous offrez votre monde de d�part', [st]);
      end;

end;

procedure TNebWin.Listedesordres1Click(Sender: TObject);
begin
  ShowHelp(1000);
end;

procedure TNebWin.FormActivate(Sender: TObject);
var
  NomFic: string;
begin
  if (not Assigned(MWData)) and (ParamCount > 0) then
  begin
    FormResize(Self);
    NomFic := ParamStr(1);
    if NomFic.StartsWith('"') then
      NomFic := NomFic.Substring(0, NomFic.length - 2);
    OuvreFic(NomFic);
  end;
end;

procedure TNebWin.FormAfterMonitorDpiChanged(Sender: TObject; OldDPI,
  NewDPI: Integer);
begin
  // TODO : rafraichir toutes les fen�tres

end;

procedure TNebWin.Aide2Click(Sender: TObject);
begin
  ShowHelp(0);
end;

procedure TNebWin.NiveaudeDEPlacement1Click(Sender: TObject);
var
  OldDep: integer;
  Ini: TMemIniFile;
begin
  if (RepFic <> '') and (MWData.Map.LongX <> 0) then
  begin
    Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
    Ini.AutoSave := True;
    OldDep := MWData.EvalDEP;
    MWData.EvalDEP :=
      StrToIntDef(InputBox('Saisie de votre futur niveau de DEP',
      'Saisissez ici le niveau en DEP que vous '#10#13'aurez � la fin du tour',
      IntToStr(MWData.EvalDEP)), MWData.EvalDEP);
    if MWData.EvalDEP <> OldDep then
    begin
      Ini.WriteInteger(MWData.NomPartie, 'DEP', MWData.EvalDEP);
      StatusBar.Panels.Items[3].Text := Format('DEP = %d', [MWData.EvalDEP])
    end;
    Ini.Free;
  end;
end;

procedure TNebWin.MenuPlanClick(Sender: TObject);
begin
  if PlanForm <> nil then
  begin
    PlanForm.show;
  end;
end;

procedure TNebWin.MenuOrdresClick(Sender: TObject);
begin
  if OrdresForm <> nil then
    OrdresForm.show;
end;

procedure TNebWin.MenuListingClick(Sender: TObject);
begin
  if VisuListingForm <> nil then
    VisuListingForm.show;
end;

procedure TNebWin.MenuMosaiqueHClick(Sender: TObject);
begin
  TileMode := tbHorizontal;
  Tile;
end;

procedure TNebWin.MenuMosaiqueVClick(Sender: TObject);
begin
  TileMode := tbVertical;
  Tile;
end;

procedure TNebWin.MenuCascadeClick(Sender: TObject);
begin
  Cascade;
end;

procedure TNebWin.Rorganiserlesicnes1Click(Sender: TObject);
begin
  ArrangeIcons;
end;

procedure TNebWin.ImprimerPlanMenuClick(Sender: TObject);
begin
  if PlanForm <> nil then
  begin
    PlanForm.WinPlan.ImprimerPlan;
  end;
end;

procedure TNebWin.ImprimerListingMenuClick(Sender: TObject);
var
  NomFic: string;
  Fic, FicPrn: TextFile;
  ligne: string;
begin
  NomFic := MWData.GetNomListing;

  if FileExists(NomFic) then
  begin
    with Printer.Canvas do
    begin
      Font.PixelsPerInch := GetDeviceCaps(Printer.Handle, logPixelsY);
      Font.Name := 'Courier New';
      Font.Size := 12;
      Font.color := clBlack;
      Brush.Style := bsClear;
    end;
    AssignFile(Fic, NomFic);
    AssignPrn(FicPrn);
    Rewrite(FicPrn);
    Reset(Fic);
    while not eof(Fic) do
    begin
      Readln(Fic, ligne);
      Writeln(FicPrn, ligne);
    end;
    CloseFile(Fic);
    CloseFile(FicPrn);
  end
  else
    ShowMessage('Impossible d''ouvrir le fichier ' + NomFic);
end;

procedure TNebWin.Squencdexcutiondesordres1Click(Sender: TObject);
begin
  ShowHelp(1001);
end;

procedure TNebWin.Simulationdecombat1Click(Sender: TObject);
begin
  if not Assigned(SimuForm) then
    SimuForm := TSimuForm.Create(Self);

  SimuForm.ShowModal;
end;

procedure TNebWin.MenuFlottesClick(Sender: TObject);
begin
  if not Assigned(MWData) then
    Exit;
  if FlottesForm = nil then
  begin
    FlottesForm := TFlottesForm.Create(Self);
  end;

  // Chercher la page correspondant.
  if PageFlottes = -1 then
  begin
    FlottesForm.dragkind := dkDock;
    FlottesForm.ManualDock(PageControlCR);
    PageFlottes := PageControlCR.PageCount - 1;
  end;

  PageControlCR.ActivePageIndex := PageFlottes;

  FlottesForm.AssignData(MWData);
  RempliFlottes;
  FlottesForm.show;
end;

procedure TNebWin.RempliFlottes;
var
  i, tr, ligne, li, j: integer;
  tempo: string;
  temp: integer;
  FlotteVisible: array [1 .. 1024] of boolean;
  NbLigne: integer;
begin
  FillChar(LigneNb, sizeof(LigneNb), 0);
  FillChar(LigneNbInv, sizeof(LigneNbInv), 0);
  FillChar(Poids, sizeof(Poids), 0);
  FillChar(FlotteVisible, sizeof(FlotteVisible), 0);
  if MWData.Map.LongX <> 0 then
    with FlottesForm.Grid do
    begin
      for j := 0 to 4 do
        Cells[j, 1] := '';

      ligne := 0;
      for i := 1 to 1024 do
        if (MWData.f[i].Localisation > 0) then
          if ((MWData.f[i].Proprio = MWData.NoJou) and
            (FlottesForm.PersoCheck.Checked)) or
            ((MWData.f[i].Proprio = 0) and (FlottesForm.NeutreCheck.Checked)) or
            (((MWData.f[i].Proprio > 0) and (MWData.f[i].Proprio <>
            MWData.NoJou)) and (FlottesForm.AlienCheck.Checked)) or
            (MWData.TypeFicDonnees = tfd_NBA) then
          begin
            inc(ligne);
            LigneNb[ligne] := ligne;

            if Cle = 0 then
              Poids[ligne] := ligne
            else if Cle = 1 then
              Poids[ligne] := MWData.f[i].Localisation
            else if Cle = 2 then
              Poids[ligne] := MWData.f[i].NbVC + MWData.f[i].NbVT;
          end;

      NbLigne := ligne + 1;

      { Tri a bulle }
      for i := 1 to NbLigne - 2 do
        for j := 1 to NbLigne - i - 1 do
        begin
          if Poids[LigneNb[j]] > Poids[LigneNb[j + 1]] then
          begin
            temp := LigneNb[j];
            LigneNb[j] := LigneNb[j + 1];
            LigneNb[j + 1] := temp;
          end;
        end;

      { Calcul de l'inverse de LigneNb }
      for i := 1 to NbLigne - 1 do
        LigneNbInv[LigneNb[i]] := i;

      ligne := 0;
      for i := 1 to 1024 do
        if (MWData.f[i].Localisation > 0) then
          if ((MWData.f[i].Proprio = MWData.NoJou) and
            (FlottesForm.PersoCheck.Checked)) or
            ((MWData.f[i].Proprio = 0) and (FlottesForm.NeutreCheck.Checked)) or
            (((MWData.f[i].Proprio > 0) and (MWData.f[i].Proprio <>
            MWData.NoJou)) and (FlottesForm.AlienCheck.Checked)) or
            (MWData.TypeFicDonnees = tfd_NBA) then
          begin
            inc(ligne);
            FlotteVisible[i] := True;
            Cells[0, LigneNbInv[ligne]] := IntToStr(i);
            Cells[1, LigneNbInv[ligne]] := IntToStr(MWData.f[i].Localisation);

            { Vaisseaux }
            tempo := '';
            if MWData.f[i].NbVC > 0 then
              tempo := IntToStr(MWData.f[i].NbVC);
            if MWData.f[i].NbVT > 0 then
            begin
              if MWData.f[i].NbVC > 0 then
                tempo := tempo + '+';
              tempo := tempo + IntToStr(MWData.f[i].NbVT) + 'T';
            end;
            if (MWData.f[i].NbVC = 0) and (MWData.f[i].NbVT = 0) then
              tempo := '-';
            Cells[2, LigneNbInv[ligne]] := tempo;

            { Cargaison -> fichier texte }
            Cells[3, LigneNbInv[ligne]] := MWData.FlotteCarg[i];

            { RAZ Tresor }
            Cells[4, LigneNbInv[ligne]] := '';
            for tr := 1 to NbTres do
              if MWData.GetTresorFlotte(tr) = i then
              begin
                li := LigneNbInv[ligne];
                if length(Cells[4, li]) > 0 then
                  Cells[4, li] := Cells[4, li] + ', ';
                Cells[4, li] := Cells[4, li] + IntToStr(tr);
              end;
          end;
      if ligne = 0 then
        ligne := 1;
      RowCount := ligne + 1;

      { Tresors }

    end;
end;

{ Gestion du dropdown : l'utilisateur fait glisser un nom de fichier sur
  la fen�tre principale }
procedure TNebWin.WMDropFiles(var MsgInfo: TWMDropFiles);
var
  NomFic: array [0 .. 256] of char;
begin
  DragQueryFile(MsgInfo.Drop, 0, NomFic, sizeof(NomFic));
  if (ExtractFileExt(StrPas(NomFic)).ToUpper <> '.AVD') and
    (ExtractFileExt(StrPas(NomFic)).ToUpper <> '.NBT') and
    (ExtractFileExt(StrPas(NomFic)).ToUpper <> '.NBA') then
    ShowMessage('Mauvais type de fichier')
  else
    OuvreFic(StrPas(NomFic));
  DragFinish(MsgInfo.Drop);
end;

procedure TNebWin.ChercherMenuClick(Sender: TObject);
begin
  FenRechercheCourante := ActiveMDIChild;
  FindDialog.Execute;
end;

procedure TNebWin.FindDialogFind(Sender: TObject);
begin
  if FenRechercheCourante = OrdresForm then
  begin
    if not SearchMwEdit(OrdresForm.mwEdit, FindDialog.FindText,
      FindDialog.Options) then
      MessageBeep(0)
    else if OrdresForm.CursorTrack then
      OrdresForm.ChercheMondeCourant(True);
  end
  else if FenRechercheCourante = VisuListingForm then
  begin
    if not SearchMwEdit(VisuListingForm.Memo, FindDialog.FindText,
      FindDialog.Options) then
      MessageBeep(0);
  end;
  FindDialog.CloseDialog;
end;

procedure TNebWin.OccSuivMenuClick(Sender: TObject);
begin
  FindDialogFind(Self);
end;

procedure TNebWin.Remplacer1Click(Sender: TObject);
begin
  ReplaceDialog.Execute;
end;

procedure TNebWin.Personalisation1Click(Sender: TObject);
begin
  FontDialog.Font.Name := MemoFont.Name;
  FontDialog.Font.Size := MemoFont.Size;
  FontDialog.Options := [fdFixedPitchOnly, fdForceFontExist];
  if FontDialog.Execute then
  begin
    MemoFont.Name := FontDialog.Font.Name;
    MemoFont.Size := FontDialog.Font.Size;
    UpdateFont;
  end;
end;

procedure TNebWin.PhotoMondesMenuClick(Sender: TObject);
var
  ErrMessage: String;
  CR: String;

begin
  // Ouvrir la boite de dialogue d'import d'extrait de CR
  if (not Assigned(MWData)) then
    Exit;

  ExtraitCRDialog.Run(MWData);


  if (ExtraitCRDialog.ModalResult = mrOK) then
  begin
    // Mise � jour de la fen�tre du plan
    MWData.Radar := 1;
    MapCoord.FakeRad3(MWData.FilesDir, MWData);
    MWData.RempliInfoSupNBT;
    PlanForm.UpdateData(MWData);

    if not FileExists(MWData.GetNomListing) and Assigned(VisuListingForm) then
    begin
      // Mise � jour du CR
      CR := ImprimeData(MWData, nil, MWData.NoJou);
      VisuListingForm.UpdateCR(CR);
    end;

    MWData.SauveEnNBT(MWData.NoJou, '');

    ErrMessage := 'Import de CR effectu�';
    if ExtraitCRDialog.Erreurs.Count > 0 then
      ErrMessage := ErrMessage + #13#10#13#10'Erreurs :'#13#10 + ExtraitCRDialog.Erreurs.Text;
    ShowMessage(ErrMessage);

  end;
end;

procedure TNebWin.Planpleincran1Click(Sender: TObject);
begin
  if Assigned(PlanForm) then
    PlanForm.FullScreen;
end;

procedure TNebWin.FileHistoryOpen(Sender: TObject);
begin
  OuvreFic(FileHistory.Strings[TMenuItem(Sender).Tag]);
end;

procedure TNebWin.ChercherMondeMenuClick(Sender: TObject);
begin
  if PlanForm <> nil then
    PlanForm.ChercherMonde(True);
end;

procedure TNebWin.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
end;

procedure TNebWin.SendMailDone(Sender: TObject);
begin
  ShowMessage('Ordres envoy�s');
end;

procedure TNebWin.SetPanelsLayout;
begin
end;

procedure TNebWin.EnvoiOrdresAPIMenuClick(Sender: TObject);
var
  Ordres: TStringList;
  RESTClient: TRESTClient;
  RESTRequest: TRESTRequest;
  JSONObject: TJSONObject;
  Rep: TCustomRESTResponse;
  Auth: THTTPBasicAuthenticator;
  TailleLigne: integer;
  ReqStr : string;
begin
  if Password = '' then
  begin
    ShowMessage('Vous devez renseigner votre mot de passe avant de pouvoir envoyer vos ordres');
    ConfigSMMenuClick(Self);
  end;


  if Application.MessageBox('Vous �tes sur le point d''envoyer vos ' +
    'ordres au SM. Etes vous s�r de vouloir continuer ?', 'Avertissement',
    MB_YESNO) = IDNO then
    Exit;

  if OrdresForm = nil then
    ShowMessage
      ('Le fichier des ordres n''est pas ouvert'#10#13'Ouvrez le fichier avant')
  else
  begin
    EnvoiSplashForm.SetText('Envoi des ordres...');
    EnvoiSplashForm.show;
    EnvoiSplashForm.Refresh;

    Ordres := TStringList.Create;
    try
      // On formatte les ordres avant envoi
      Ordres.Text := OrdresForm.mwEdit.Text;
      if PrefForm.EnleveCommentCheck.Checked then
      begin
        EnleveComment(Ordres);
        OrdresForm.AddHeader(Ordres);
      end;
      TailleLigne := StrToIntDef(PrefForm.TailleLigneEdit.Text, 0);
      if PrefForm.RetourLigneCheck.Checked and (TailleLigne > 0) then
      begin
        if TailleLigne < 85 then
          TailleLigne := 85;

        RetourLigne(Ordres, TailleLigne);
      end;
      Ordres.Insert(0, '#@# Ordres envoy�s par API par Nebutil version ' +
        NoVersionNebutil);

      // On pr�pare la requ�te JSON
      JSONObject := TJSONObject.Create(TJSonPair.Create('ordres', Ordres.Text));
      JSONObject.AddPair(TJSonPair.Create('tour', TJSONNumber.Create(MWData.Tour + 1)));
      ReqStr := JSONObject.ToString;
      RESTRequest := TRESTRequest.Create(nil);
      RESTClient := TRESTClient.Create(nil);
      RESTRequest.Client := RESTClient;
      // TODO : rendre param�trable l'adreses de l'API
      RESTClient.BaseURL := URL_API_NEBMAIL;

      Auth := THTTPBasicAuthenticator.Create
        (Format('%s-%d', [UpperCase(MWData.NomPartie), MWData.NoJou]),
        Password);
      RESTClient.Authenticator := Auth;

      RESTRequest.Params.Clear;
      RESTRequest.AddBody(JSONObject);
      RESTRequest.Method := TRESTRequestMethod.rmPUT;
      RESTRequest.Resource := Format('ordres/%d', [MWData.NoJou]);

      try
        RESTRequest.Execute;
        Rep := RESTRequest.Response;
        if Rep.StatusCode <> 200 then
          ShowMessage('Erreur dans l''envoi des ordres : ' + Rep.Content);
      finally
        Auth.Free;
        RESTClient.Free;
        RESTRequest.Free;
      end;

    finally
      Ordres.Free;
      EnvoiSplashForm.Hide;
    end;
  end;
end;

procedure TNebWin.Paramtresmail1Click(Sender: TObject);
begin
  { Initisalisation des champs de la boite de dialogue }
  ReseauConfigForm := TReseauConfigForm.Create(Self);
  ReseauConfigForm.UseProxyCheck.Checked := UseProxy;
  ReseauConfigForm.ProxyNameEdit.Text := ProxyName;
  if ProxyPort > 0 then
    ReseauConfigForm.ProxyPortEdit.Text := IntToStr(ProxyPort)
  else
    ReseauConfigForm.ProxyPortEdit.Text := '';

  { Affichage de la boite de dialogue }
  ReseauConfigForm.ShowModal;
  if ReseauConfigForm.ModalResult = mrOK then
  begin
    { On r�cup�re le contenu des champs de la boite }
    UseProxy := ReseauConfigForm.UseProxyCheck.Checked;
    ProxyName := ReseauConfigForm.ProxyNameEdit.Text;
    ProxyPort := StrToIntDef(ReseauConfigForm.ProxyPortEdit.Text, 0);
  end;
  ReseauConfigForm.Free;

  { On met � jour les menus en fonction des nouvelles donn�es }
  EnableMenuMail;
end;

procedure TNebWin.UpdateFont;
begin
  Font.Assign(MemoFont);

  if OrdresForm <> nil then
    OrdresForm.UpdateFont;
  if VisuListingForm <> nil then
    VisuListingForm.UpdateFont;
  if ResultForm <> nil then
    ResultForm.UpdateFont;
  if ImpMondeForm <> nil then
    ImpMondeForm.UpdateFont;
end;

function TNebWin.CheckNBTExt: boolean;
var
  Reg: TRegistry;
  st: string;
  p: integer;
begin
  Result := False;
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  if Reg.OpenKey('.nbt', False) then
  begin
    st := Reg.ReadString('');
    if st <> '' then
    begin
      Reg.CloseKey;
      if Reg.OpenKey(st + '\Shell\Open\Command', False) then
      begin
        p := Reg.ReadString('').IndexOf(' "%1"');
        if p = -1 then
          Result := False { La commande doit �tre NomProg "%1" }
        else
        begin
          if StrLComp(PChar(Reg.ReadString('')), PChar(ParamStr(0)), p) <> 0
          then
            Result := False { Le prog associ� au .NBT n'est pas au m�me endroit que celui qui est ex�cut� }
          else
            Result := True; { Tout va bien, on ne change rien }
        end;
      end;
    end;
  end;
  Reg.Free;
end;

procedure TNebWin.RegisterNBT;
var
  Reg: TRegistry;
begin
  if not IsUserAnAdmin then
    Exit;

  Reg := TRegistry.Create();
  Reg.Access := KEY_WRITE;
  Reg.RootKey := HKEY_CLASSES_ROOT;
  if Reg.OpenKey('.nbt', True) then
  begin
    Reg.WriteString('', 'Nebutil_NBT');
    Reg.WriteString('Content Type', 'application/x-nebutil-nbt');
    Reg.CloseKey;
    if Reg.OpenKey('Nebutil_NBT', True) then
    begin
      Reg.WriteString('', 'Nebutil - Fichier de donn�es NBT');
      Reg.OpenKey('DefaultIcon', True);
      Reg.WriteString('', ParamStr(0) + ',0');
      Reg.CloseKey;
      Reg.OpenKey('Nebutil_NBT\Shell\Open\Command', True);
      Reg.WriteString('', ParamStr(0) + ' "%1"');
      Reg.CloseKey;
    end
  end
  else
    ShowMessage
      ('Impossible de faire l''assotiation avec le type de fichier .nbt. Pour sa premi�re ex�cution lancer le programme avec les droits administrateur.');
end;

procedure TNebWin.MenuAPI(Enable: boolean);
begin
  EnvoiOrdresAPIMenu.Enabled := Enable;
end;

procedure TNebWin.EnableMenuMail;
begin
  if not Assigned(MWData) then
    Exit;

  // Menu toujours activ� car utilis� pour mail + utilisation de l'API
  ConfigSMMenu.Enabled := True;

  // Toujours activ� car demande le mot de passe si absent
  MenuAPI(True);

end;

procedure TNebWin.Testversionsplusrcentes1Click(Sender: TObject);
var
  Body: TStringList;
  finalversion, finalpl, betaversion, betabeta, betapl: integer;
  FinalCh, BetaCh: string;
  FinalOK, BetaOK: boolean;
  HttpResponse: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if UseProxy then
    begin
      IndyHttp.ProxyParams.ProxyServer := ProxyName;
      IndyHttp.ProxyParams.ProxyPort := ProxyPort;
    end
    else
    begin
      IndyHttp.ProxyParams.ProxyServer := '';
      IndyHttp.ProxyParams.ProxyPort := 0;
    end;
    Body := TStringList.Create;
    HttpResponse := IndyHttp.Get('http://nebula.ludimail.net/version.txt');
    Body.Text := HttpResponse;
    if (Body.Count = 2) and Body[0].StartsWith('Nebutil') and
      Body[1].StartsWith('Beta') then
    begin
      { On regarde s'il y a une version plus r�cente }
      finalversion := Body[0].Substring(10, 3).ToInteger;
      finalpl := Body[0].Substring(14, 3).ToInteger;
      betaversion := Body[1].Substring(7, 3).ToInteger;
      betabeta := Body[1].Substring(11, 3).ToInteger;
      betapl := Body[1].Substring(15, 3).ToInteger;

      { Version Finale }
      if finalversion > VersionNebutil then
        FinalOK := False
      else if (finalversion = VersionNebutil) and (VersionNebutilBeta > 0) then
        FinalOK := False
      else if finalpl > VersionNebutilPl then
        FinalOK := False
      else
        FinalOK := True;

      { Version beta }
      if betaversion < VersionNebutil then
        BetaOK := True
      else if betaversion > VersionNebutil then
        BetaOK := False
      else if (betabeta > VersionNebutilBeta) then
        BetaOK := False
      else if (betabeta = 0) and (VersionNebutilBeta > 0) then
        BetaOK := False
      else if betapl > VersionNebutilPl then
        BetaOK := False
      else
        BetaOK := True;

      if FinalOK then
        FinalCh := 'Version finale : � jour'
      else
        FinalCh := Format('Version Finale : la version %s est disponible',
          [CalcNoVersionNebutil(finalversion, 0, finalpl)]);

      if BetaOK then
        BetaCh := 'Version Beta : � jour'
      else
        BetaCh := Format('Version beta : la version %s est disponible',
          [CalcNoVersionNebutil(betaversion, betabeta, betapl)]);
    end
    else
    begin
      ShowMessage('Impossible de d�terminer la version la plus r�cente');
    end;
    Body.Free;

    ShowMessage
      (Format('Vous utilisez actuellement Nebutil %s'#10#13#10#13'%s'#10#13'%s',
      [NoVersionNebutil, FinalCh, BetaCh]));
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TNebWin.Configurationimprimante1Click(Sender: TObject);
begin
  PrinterSetupDialog.Execute;
end;

procedure TNebWin.Simulationdvolutiondelapopulation1Click(Sender: TObject);
begin
  SimuPopForm.ShowModal;
end;

procedure TNebWin.Slectionnertout1Click(Sender: TObject);
begin
  if ActiveMDIChild <> nil then
  begin
    if ActiveMDIChild = OrdresForm then
      OrdresForm.mwEdit.SelectAll
    else if ActiveMDIChild = VisuListingForm then
      VisuListingForm.Memo.SelectAll;
  end;
end;

procedure TNebWin.Splitter1Moved(Sender: TObject);
begin
  PanelPlanWidthRatio := Round(100 * PanelPlan.Width / ClientWidth);
end;

procedure TNebWin.Splitter2Moved(Sender: TObject);
begin
  PanelCRHeightRatio := Round(100 * PageControlCR.Height / ClientHeight);
end;

procedure TNebWin.Copier1Click(Sender: TObject);
begin
  if ActiveMDIChild <> nil then
  begin
    if ActiveMDIChild = OrdresForm then
      OrdresForm.mwEdit.CopyToClipboard
    else if ActiveMDIChild = VisuListingForm then
      VisuListingForm.Memo.CopyToClipboard
    else if ActiveMDIChild.Name = 'HistoForm' then
      (ActiveMDIChild as THistoForm).Memo.CopyToClipboard;
  end;
end;

procedure TNebWin.Couper1Click(Sender: TObject);
begin
  if ActiveMDIChild <> nil then
  begin
    if ActiveMDIChild = OrdresForm then
      OrdresForm.mwEdit.CutToClipboard
    else if ActiveMDIChild = VisuListingForm then
      VisuListingForm.Memo.CutToClipboard;
  end;
end;

procedure TNebWin.Coller1Click(Sender: TObject);
begin
  if ActiveMDIChild <> nil then
  begin
    if ActiveMDIChild = OrdresForm then
      OrdresForm.mwEdit.PasteFromClipboard
    else if ActiveMDIChild = VisuListingForm then
      VisuListingForm.Memo.PasteFromClipboard;
  end;
end;

procedure TNebWin.MenuPrefClick(Sender: TObject);
begin
  PrefForm.ShowModal;
  if (PrefForm.ModalResult = idOK) and Assigned(PlanForm) then
    PlanForm.WinPlan.RedessineTout;
end;

function TNebWin.GetOrdresFileName: string;
begin
  Result := '';
  if MWData <> nil then
    Result := Format('%s%.3sO%.2d%.2d.TXT', [RepFic, MWData.NomPartie,
      MWData.NoJou, MWData.Tour + 1]);
end;

procedure TNebWin.AllerMondeCourMenuClick(Sender: TObject);
begin
  if Assigned(OrdresForm) and Assigned(PlanForm) then
    OrdresForm.ChercheMondeCourant(False);
end;

procedure TNebWin.ChercherFlotteMenuClick(Sender: TObject);
begin
  if PlanForm <> nil then
    PlanForm.ChercherFlotte(True);
end;

procedure TNebWin.ChercherTresorMenuClick(Sender: TObject);
begin
  if PlanForm <> nil then
    PlanForm.ChercherTresor(True);
end;

procedure TNebWin.SimulEval(EtapeStop: integer);
var
  Data2: TNebData;
  AvaExt: STring;
  CR: TStringList;
begin
  AvaExt := 'AVA';
  Data2 := EvalTourData(MWData, TStringList(OrdresForm.mwEdit.Lines), EtapeStop, True);
  CR := TSTringList.Create;

  ImprimeData(Data2, MWData, MWData.NoJou, CR);

  CRForm.SetData(Data2);
  CRForm.Memo.Lines.Assign(CR);
  CR.Free;

  // Chercher la page correspondant.
  if PageSimulEval = -1 then
  begin
    CRForm.dragkind := dkDock;
    CRForm.ManualDock(PageControlCR);
    PageSimulEval := PageControlCR.PageCount - 1;
  end;
  PageControlCR.ActivePageIndex := PageSimulEval;
  Data2.Free;

  CRForm.show;
  CRForm.Memo.SetFocus;
end;

procedure TNebWin.Simulationdeval1Click(Sender: TObject);
begin
  if Assigned(MWData) then
  begin
    if (MWData.TypeFicDonnees <> tfd_NBT) or (MWData.NoJou = 0) then
    begin
      MessageDlg('D�sol� mais vous devez ouvrir le fichier .NBT' + #13 + #10 +
        'pour pouvoir faire une simulation d' + #39 + '�val', mtError,
        [mbCancel], 0);
      Exit;
    end;
    SimulEval(0);
  end;
end;

procedure TNebWin.Relationsaveclesautresjoueurs1Click(Sender: TObject);
var
  RelationsForm: TRelationsForm;
  SectionName: String;
  jou: integer;
  Ini: TMemIniFile;
  //CRdyn: boolean;
begin
  if Assigned(MWData) then
  begin
    //CRdyn := False;
    RelationsForm := TRelationsForm.Create(Self);
    RelationsForm.Data := MWData;
    if RelationsForm.ShowModal = idOK then
    begin
      // On sauve les modifs dans le fichier .ini
      Ini := Nil; // pour supprimer un warning de compilation
      try
        Screen.Cursor := crHourGlass;
        Ini := TMemIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NEBUTIL.INI');
        Ini.AutoSave := True;
        SectionName := Format('%s-Relations%.2d',
          [MWData.NomPartie, MWData.NoJou]);
        Ini.WriteInteger(SectionName, 'ConfianceMax',
          MWData.ConfianceVaisseaux);
        for jou := 1 to MWData.NbJou do
          if (jou <> MWData.NoJou) and MWData.ConnuDeNom[MWData.NoJou, jou] then
          begin
            Ini.WriteInteger(SectionName, Format('Classe%.2d', [jou]),
              MWData.jou[jou].Classe);
            Ini.WriteInteger(SectionName, Format('Confiance%.2d', [jou]),
              MWData.Confiance[jou]);
            Ini.WriteInteger(SectionName, Format('ATT%.2d', [jou]),
              MWData.jou[jou].ATT);
            Ini.WriteInteger(SectionName, Format('DEF%.2d', [jou]),
              MWData.jou[jou].DEF);
            Ini.WriteBool(SectionName, Format('Allie%.2d', [jou]),
              MWData.EstAllie(jou, MWData.NoJou));
            Ini.WriteBool(SectionName, Format('Chargeur%.2d', [jou]),
              MWData.EstChargeur(jou, MWData.NoJou));
            Ini.WriteBool(SectionName, Format('ChargeurPop%.2d', [jou]),
              MWData.EstChargeurDePop(jou, MWData.NoJou));
            Ini.WriteBool(SectionName, Format('DechargeurPop%.2d', [jou]),
              MWData.EstDechargeurDePop(jou, MWData.NoJou));
          end;

        // On remet � jour l'affichage des vaisseaux;
//        If not FileExists(MWData.GetNomListing) then
//        begin
//          CRdyn := True;
//          ImprimeData(MWData, nil, nil, MWData.NoJou, MWData.GetNomListing);
//        end;

        MWData.RempliInfoSupNBT;
        //MWData.RempliInfoSupLight(PlanAvecCR);

//        if CRdyn then
//          DeleteFile(PChar(MWData.GetNomListing));

        if Assigned(PlanForm) then
        begin
          PlanForm.UpdateData(MWData);
          PlanForm.WinPlan.RedessineTout;
        end;
      finally
        if Assigned(Ini) then
          Ini.Free;
        Screen.Cursor := crDefault;
      end;
    end;
    RelationsForm.Free;
  end;
end;

procedure TNebWin.AfficheCR1Click(Sender: TObject);
var
//  NomFichier: String;
  AfficheEval: TCRForm;
  CR: TStringList;
begin
  if not Assigned(MWData) then
    Exit;
  if MWData.TypeFicDonnees = tfd_AVD then
  begin
    MessageDlg('Vous devez utiliser un fichier .NBT pour' + #13 + #10 +
      'acc�der � cette fonctionalit�', mtError, [mbCancel], 0);
    Exit;
  end;
//  NomFichier := TPath.GetTempFileName;

  CR := TStringList.Create;
  ImprimeData(MWData, nil, MWData.NoJou, CR);
  AfficheEval := TCRForm.Create(Self);
  AfficheEval.Visible := True;
  AfficheEval.Memo.Lines.Assign(CR);
//  TFile.Delete(NomFichier);
  CR.Free;
end;

procedure TNebWin.ConfigSMMenuClick(Sender: TObject);
begin
  ModifSMForm.ShowModal;
  EnableMenuMail;
end;

procedure TNebWin.EnleveComment(List: TStringList);

// Enl�ve commentaires et espaces finaux de la cha�ne
  function NettoieLigne(ligne: string): string;
  var
    posComment: integer;
  begin
    posComment := ligne.IndexOfAny([';', '#']);
    if posComment <> -1 then
      Result := ligne.Substring(0, posComment).TrimRight
    else
      Result := ligne.TrimRight;
  end;

var
  i: integer;
  tmp: string;
begin
  for i := 0 to List.Count - 1 do
  begin
    tmp := NettoieLigne(List[i]);
    List[i] := tmp;
  end;
  for i := List.Count - 1 downto 0 do
    if List[i] = '' then
      List.Delete(i);
end;

procedure TNebWin.RetourLigne(List: TStringList; tailleMax: integer);
var
  i: integer;

begin
  i := 0;
  while i < List.Count do
  begin
    if List[i].length > tailleMax then
    begin
      List.Insert(i + 1, '   ;' + List[i].Substring(tailleMax));
      List[i] := List[i].Substring(0, tailleMax);
    end;
    inc(i);
  end;
end;

// Cette fonction va lire les fichiers NBT se trouvant dans le sous-r�pertoire
// fusion (par rapport au fichier NBT ouvert) et y ajoute les informations
// qu'ils contiennent afin de g�n�rer un nouveau fichier NBT contenant les
// inforlmations issues de la fusion des NBT.

procedure TNebWin.FusiondeNBT1Click(Sender: TObject);
var
  FusionRep: string;
  jou: integer;
  newData, DataNBT: TNebData;
  JoueursList: TList;
  md, fl, tr: integer;
  MondeDepCh: string;
  Ini: TMemIniFile;
  FlotteAPrendre: boolean;
begin
  if not Assigned(MWData) then
  begin
    ShowMessage('Ouvrez votre fichier NBT avant');
    Exit;
  end;
  if MWData.TypeFicDonnees <> tfd_NBT then
  begin
    ShowMessage
      ('Vous devez ouvrir un fichier de type NBT pour cette op�ration');
    Exit;
  end;

  FusionRep := ExtractFilePath(MWData.FileName) + 'Fusion\';
  if not System.SysUtils.DirectoryExists(FusionRep) then
  begin
    ShowMessage(Format('Le r�pertoire %s n''existe pas', [FusionRep]));
    Exit;
  end;

  newData := TNebData.Create(MWData.FileName, MWData.NoJou);
  newData.RenameFile(FusionRep + ExtractFileName(MWData.FileName));

  JoueursList := TList.Create;
  MondeDepCh := '';

  // On d�termine la liste de la fusion
  for jou := 1 to newData.NbJou do
    if (jou <> newData.NoJou) and FileExists(newData.GetNBTFileName(jou)) then
      JoueursList.Add(Pointer(jou));

  for jou := 1 to newData.NbJou do
    if (jou <> newData.NoJou) and FileExists(newData.GetNBTFileName(jou)) then
    begin
      // On fusionne newData avec les donn�es du fichiers .NBT
      DataNBT := TNebData.Create(newData.GetNBTFileName(jou), newData.Tour);

      for md := 1 to newData.NbMonde do
        if DataNBT.Connu[md, jou] and (not newData.Connu[md, newData.NoJou]) and
          ((JoueursList.IndexOf(Pointer(DataNBT.M[md].Proprio)) = -1) or
          (DataNBT.M[md].Proprio = jou)) then
        begin
          newData.Connu[md, newData.NoJou] := True;
          newData.AVuCoord[md, newData.NoJou] := DataNBT.AVuCoord[md, jou];
          newData.AVuConnect[md, newData.NoJou] := DataNBT.AVuConnect[md, jou];
          newData.M[md] := DataNBT.M[md];
          newData.ME[md] := DataNBT.ME[md];
        end;

      for fl := 1 to newData.NbFlotte do
      begin
        FlotteAPrendre := False;
        if DataNBT.f[fl].Proprio = DataNBT.NoJou then
          FlotteAPrendre := True
        else if (DataNBT.f[fl].Localisation > 0) and
          (newData.f[fl].Localisation = 0) then
          FlotteAPrendre := True;

        if FlotteAPrendre then
        begin
          newData.f[fl] := DataNBT.f[fl];
          newData.FE[fl] := DataNBT.FE[fl];
        end;
      end;

      for tr := 1 to NbTres do
        if (DataNBT.T[tr].Localisation > 0) and (newData.T[tr].Localisation = 0)
        then
          newData.T[tr] := DataNBT.T[tr];

      MondeDepCh := MondeDepCh + IntToStr(DataNBT.jou[jou].md) + ',';

      DataNBT.Free;
    end;
  JoueursList.Free;

  for md := 1 to newData.NbMonde do
  begin
    if newData.M[md].x > 0 then
      newData.AVuCoord[md, newData.NoJou] := True;
    if newData.M[md].Connect[1] > 0 then
      newData.AVuConnect[md, newData.NoJou] := True;
  end;

  newData.SauveEnNBT(newData.NoJou, '');
  Ini := TMemIniFile.Create(newData.FileName);
  Ini.AutoSave := True;
  Ini.WriteString('GENERAL', 'Mds', CompresseChaine(MondeDepCh));
  Ini.Free;

  ShowMessage('Fusion termin�e. Le fichier r�sultant se trouve dans'#13 +
    newData.FileName);

  newData.Free;
end;

procedure TNebWin.Simulationdevalintermdiaire1Click(Sender: TObject);
begin
  if Assigned(MWData) then
  begin
    if (MWData.TypeFicDonnees <> tfd_NBT) or (MWData.NoJou = 0) then
    begin
      MessageDlg('D�sol� mais vous devez ouvrir le fichier .NBT' + #13 + #10 +
        'pour pouvoir faire une simulation d' + #39 + '�val', mtError,
        [mbCancel], 0);
      Exit;
    end;
    SimulEval(13);
  end;
end;

{ HistoryChanged : met � jour ce qui d�pend de l'historique des fichiers }
procedure TNebWin.ReadHistory;
var
  i: integer;
  MenuItem: TMenuItem;
begin
  FileHistory.Clear;
  if FileExists(Ini.ReadString('Fichiers', 'Fic1', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic1', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic2', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic2', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic3', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic3', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic4', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic4', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic5', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic5', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic6', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic6', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic7', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic7', ''));
  if FileExists(Ini.ReadString('Fichiers', 'Fic8', '')) then
    FileHistory.Add(Ini.ReadString('Fichiers', 'Fic8', ''));

  MenuItem := nil;
  if FileHistory.Count > 0 then
    FileHistoryMenuSeparator.Visible := True;
  for i := 1 to FileHistory.Count do
  begin
    case i of
      1:
        MenuItem := FileHistoryFic1;
      2:
        MenuItem := FileHistoryFic2;
      3:
        MenuItem := FileHistoryFic3;
      4:
        MenuItem := FileHistoryFic4;
      5:
        MenuItem := FileHistoryFic5;
      6:
        MenuItem := FileHistoryFic6;
      7:
        MenuItem := FileHistoryFic7;
      8:
        MenuItem := FileHistoryFic8;
    end;
    MenuItem.Caption := Format('&%d %s',
      [i, MinimizeName(FileHistory[i - 1], Canvas, 200)]);
    MenuItem.Visible := True;
    OpenDialog.HistoryList.Assign(FileHistory);
  end;
end;

procedure TNebWin.WriteHistory;
var
  i: integer;
begin
  { Historique des fichiers }
  for i := 1 to 8 do
    if FileHistory.Count >= i then
      Ini.WriteString('Fichiers', Format('Fic%d', [i]),
        FileHistory.Strings[i - 1])
    else
      Ini.WriteString('Fichiers', Format('Fic%d', [i]), '');
end;

procedure TNebWin.AddHistory(FileName: string);
var
  i: integer;
begin
  ReadHistory;
  FileHistory.Insert(0, FileName);
  for i := FileHistory.Count - 1 downto 1 do
    if lowercase(FileHistory[i]) = lowercase(FileName) then
      FileHistory.Delete(i);
  WriteHistory;
  ReadHistory;
end;

procedure TNebWin.ChercherMondeOrdresClick(Sender: TObject);
begin
  if PlanForm <> nil then
    PlanForm.ChercherMonde(True);
end;

procedure TNebWin.Chercheruneflotteetsesordres1Click(Sender: TObject);
begin
  if PlanForm <> nil then
    PlanForm.ChercherFlotte(True);
end;

procedure TNebWin.Chercheruntrsoretsesordres1Click(Sender: TObject);
begin
  if PlanForm <> nil then
    PlanForm.ChercherTresor(True);
end;

procedure TNebWin.CloseChildrenWindows;
begin
  ClosePageControlCRChildren;

  if Assigned(OrdresForm) then
  begin
    OrdresForm.Free;
  end;

  if Assigned(PlanForm) then
  begin
    PlanForm.Free;
  end;
end;

procedure TNebWin.ClosePageControlCRChildren;
begin
  if Assigned(VisuListingForm) then
  begin
    FreeAndNil(VisuListingForm);
    PageCR := -1;
  end;

  if Assigned(CRForm) then
  begin
    CRForm.Hide; // Non supprim� car auto-cr�� et auto-d�truit
    PageSimulEval := -1;
  end;

  if Assigned(FlottesForm) then
  begin
    FreeAndNil(FlottesForm);
    PageFlottes := -1;
  end;

  if Assigned(ResultForm) then
  begin
    FreeAndNil(ResultForm);
    PageVerifOrdres := -1;
  end;

end;

procedure TNebWin.Pointsparclasse1Click(Sender: TObject);
begin
  ShowHelp(1012);
end;

procedure TNebWin.Pointsdestrsors1Click(Sender: TObject);
begin
  ShowHelp(1011);
end;

procedure TNebWin.EnvoyerOrdresPPClick(Sender: TObject);
var
  Ordres: TStringList;
begin
  if OrdresForm = nil then
    ShowMessage
      ('Le fichier des ordres n''est pas ouvert'#10#13'Ouvrez le fichier avant')
  else
  begin
    Ordres := TStringList.Create;
    Ordres.Text := OrdresForm.mwEdit.Text;
    if PrefForm.EnleveCommentCheck.Checked then
    begin
      EnleveComment(Ordres);
      OrdresForm.AddHeader(Ordres);
    end;
    if PrefForm.RetourLigneCheck.Checked and
      (StrToIntDef(PrefForm.TailleLigneEdit.Text, 0) > 0) then
      RetourLigne(Ordres, StrToIntDef(PrefForm.TailleLigneEdit.Text, 0));
    //Ordres.Insert(0, 'DEBUT');
    Ordres.Insert(0, '# Ordres envoy�s via copier-coller (Nebutil version ' +
      NoVersionNebutil + ')');
    //Ordres.Add('FIN');
    Clipboard.SettextBuf(PChar(Ordres.Text));
    ShowMessage('Les ordres ont �t� copi�s dans le presse-papier');
  end;

end;

function TNebWin.GetAdresseSM: string;
begin
  Result := '';
  if AdresseSMDansNBT and (MWData <> nil) then
    Result := MWData.AdresseServeurMail
  else
    Result := AdresseServeurNebula;

end;

end.
