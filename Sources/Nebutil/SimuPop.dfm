�
 TSIMUPOPFORM 0  TPF0TSimuPopFormSimuPopFormLeftDTopHelpContext�BorderStylebsDialogCaption(   Simulation d'évolution de la populationClientHeight2ClientWidthTColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1Left6TopWidth2HeightCaption
Population  TLabelLabel2Left<Top,Width.HeightCaption	Convertis  TLabelLabel3LeftTopDWidthaHeightCaptionPopulation maximale  TLabelLabel4LeftTop`Width7HeightCaptionSimulation :  TButtonButton1Left� TopWidthKHeightCaptionOKDefault	ModalResultTabOrder OnClickButton1Click  TButtonButton3Left� Top1WidthKHeightHelpContext�Caption&AideTabOrderOnClickButton3Click  TEditPopEditLeftpTopWidthyHeightTabOrderTextPopEdit  TEditConvEditLeftpTop(WidthyHeightTabOrderTextConvEdit  TEdit
MaxPopEditLeftpTop@WidthyHeightTabOrderText
MaxPopEdit  TMemoSimuMemoLeftToppWidth� Height� Lines.StringsSimuMemo TabOrder  TButton
SimuButtonLeft� Top`WidthKHeightCaption
SimulationTabOrderOnClickSimuButtonClick   