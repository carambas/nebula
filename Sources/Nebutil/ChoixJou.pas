unit ChoixJou;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, checklst, NebData;

type
  TChoixJouForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    AllCheckBox: TCheckBox;
    AnonymeCheckBox: TCheckBox;
    ListBox: TListBox;
    MultiCheckBox: TCheckBox;
    BitBtn3: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure AnonymeCheckBoxClick(Sender: TObject);
    procedure MultiCheckBoxClick(Sender: TObject);
  private
    { Déclarations privées }
    Data : TNebData;
    PlayerList : array[0..32] of integer;
  public
    { Déclarations publiques }
    function GetNums : string;
    function GetPseudosList : string;
  end;

implementation

uses AvatUnit, NebutilWin;

{$R *.DFM}

procedure TChoixJouForm.FormCreate(Sender: TObject);
var
  i : integer;
  NoJoueur : integer;
  //ListItem : TListItem;
begin
  FillChar(PlayerList, sizeof(PlayerList), 0);
  NoJoueur := 0;
  Data := NebWin.MWData;
  if Data <> nil then if Data.Map.LongX > 0 then
  begin
    {Arbitre}
    ListBox.Items.Add('*** ARBITRE ***');
    PlayerList[NoJoueur] := 0;
    inc(NoJoueur);
    {ListItem := ListView.Items.Add;
    ListItem.Caption := ;
    ListItem.Data := Pointer(0);}

    {Autres joueurs}
    for i := 1 to NB_JOU_MAX do if Data.Pseudos[i] <> '' then
    begin
      ListBox.Items.add(Format('%s:%.2d', [Data.Pseudos[i], i]));
      PlayerList[NoJoueur] := i;
      inc(NoJoueur);
      {ListItem := ListView.Items.Add;
      ListItem.Caption := Format('%s:%.2d', [Data^.Pseudos[i], i]);
      ListItem.Data := Pointer(i);}
    end;
  end;
end;

function TChoixJouForm.GetNums : string;
var
  i : integer;
  NeedComma : boolean;
begin
  Result := '';
  NeedComma := False;
  if AnonymeCheckBox.Checked then
    Result := Result + 'A ';
  if MultiCheckBox.Checked then
    Result := Result + 'M ';
  Result := Result + '<';
  if AllCheckBox.Checked then
  begin
    Result := Result + 'All';
    NeedComma := True;
  end;
  for i := 0 to ListBox.items.Count - 1 do if ListBox.Selected[i] then
  begin
    if NeedComma then
      Result := Result + ',';
    Result := Result + IntToStr(PlayerList[i]);
    NeedComma := True;
  end;
  Result := Result + '>';
end;

function TChoixJouForm.GetPseudosList : string;
var
  i : integer;
  NeedComma : boolean;
  Pseudo : string;
begin
  Result := '';
  NeedComma := False;
  if AllCheckBox.Checked then
  begin
    Result := Result + 'tout le monde';
    Exit;
  end;
  for i := 0 to ListBox.items.Count - 1 do if ListBox.Selected[i] then
  begin
    if NeedComma then
      Result := Result + ' et ';
    if PlayerList[i] = 0 then
      Pseudo := 'l''arbitre'
    else
      Pseudo := Data.Pseudos[PlayerList[i]];
    Result := Result + Pseudo;
    NeedComma := True;
  end;
end;

procedure TChoixJouForm.AnonymeCheckBoxClick(Sender: TObject);
begin
  if AnonymeCheckBox.Checked then
    MultiCheckBox.Checked := False;
end;

procedure TChoixJouForm.MultiCheckBoxClick(Sender: TObject);
begin
  if MultiCheckBox.Checked then
    AnonymeCheckBox.Checked := False;
end;

end.
