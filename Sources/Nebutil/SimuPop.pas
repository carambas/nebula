unit SimuPop;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms;

type
  TSimuPopForm = class(TForm)
    Button1: TButton;
    Button3: TButton;
    PopEdit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ConvEdit: TEdit;
    MaxPopEdit: TEdit;
    SimuMemo: TMemo;
    Label4: TLabel;
    SimuButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SimuButtonClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);

  private
    FPop, FConv, FMaxPop : integer;
    FTour : integer;

    procedure Simulation;
    procedure AffichePop(Pop, Conv, MaxPop : integer);
  public
    procedure SetParams(Pop, Conv, MaxPop : integer);
  end;

var
  SimuPopForm: TSimuPopForm;

implementation

{$R *.DFM}

uses Utils, NebutilWin;

procedure TSimuPopForm.FormCreate(Sender: TObject);
begin
  SimuMemo.Text := '';
  SetParams(0, 0, 0);
end;

procedure TSimuPopForm.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TSimuPopForm.SetParams(Pop, Conv, MaxPop : integer);
begin
  FPop := Pop;
  FConv := Conv;
  FMaxPop := MaxPop;

  // Affichage des paramètres
  PopEdit.Text := IntToStr(FPop);
  ConvEdit.Text := IntToStr(FConv);
  MaxPopEdit.Text := IntToStr(FMaxPop);

  // Simulation
  Simulation;
end;

procedure TSimuPopForm.Simulation;
var
  Pop, Conv : real;
  i : integer;
begin
  SimuMemo.Clear;
  if (FPop <= 0) or (FMaxPop <= 0) then
    Exit;

  FTour := 0;
  if NebWin.MWData <> nil then
    FTour := NebWin.MWData.Tour;

  Pop := FPop;
  Conv := FConv;
  AffichePop(Round(Pop), Round(Conv), FMaxPop);

  for i := 1 to 10 do
  begin
    inc(FTour);
    Pop := Pop * 1.1;
    if Pop > FMaxPop then
      Pop := FMaxPop;
    Conv := Conv*1.25;
    if Conv > Pop then
      Conv := Pop;
    AffichePop(Round(Pop), Round(Conv), FMaxPop);
  end;
end;

procedure TSimuPopForm.AffichePop(Pop, Conv, MaxPop : integer);
var
  Ligne : string;
begin
  if Pop <> Conv then
  begin
    Ligne := Format('Tour %d : P=%d(%d)', [FTour,Pop, MaxPop]);
    if Conv > 0 then
      Ligne := Ligne + Format('/%dC', [Conv]);
  end
  else
    Ligne := Format('Tour %d : P=%dC(%d)', [FTour,Pop, MaxPop]);
  SimuMemo.Lines.Add(Ligne);
end;

procedure TSimuPopForm.SimuButtonClick(Sender: TObject);
begin
  FPop := StrToIntDef(PopEdit.Text, 0);
  FConv := StrToIntDef(ConvEdit.Text, 0);
  FMaxPop := StrToIntDef(MaxPopEdit.Text, 0);
  Simulation;
end;

procedure TSimuPopForm.Button3Click(Sender: TObject);
var
  HelpFileName : string;
begin
  // WinHelp(Handle, 'nebutil.hlp', HELP_CONTEXT, HelpContext);

  HelpFileName := ExpandFileName(ExtractFilePath(ParamStr(0)) + 'nebutil.chm');
  HtmlHelp(0, PChar(HelpFileName), HH_HELP_CONTEXT, HelpContext);
end;

end.
