unit Ordres;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Math,
  Avatunit, Sortie, Menus, Utils, ComCtrls{, ColMemo}, ExtCtrls, NebData,
 SynEdit, SynHighlighterNebula, SynEditHighlighter, SynHighlighterPas,
 SynHighlighterNebulaOrdres;
type
  TOrdresForm = class(TForm)
    PopupMenu1: TPopupMenu;
    SelectAll1: TMenuItem;
    AutoSaveTimer: TTimer;
    mwEdit: TSynEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ColorMemoKeyPress(Sender: TObject; var Key: Char);
    procedure PopupMenuClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MenuClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ColorMemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
    procedure ColorMemoHotSpotClick(Sender: TObject; const SRC: String);
    procedure AutoSaveTimerTimer(Sender: TObject);
    procedure ColorMemoClick(Sender: TObject);
    procedure ColorMemoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private-d�clarations }
    JoueursMenu : TPopupMenu;
    PlanDist, PlanType : array[0..1024] of byte;
    PlanFrom : array[0..1024] of integer;
    NbMemoLock : integer;
    NbLignes : integer;
    TailleSelection, OldTailleSelection : integer;
    DebutSelection, OldDebutSelection : integer;
    SynNebulaSyn : TSynCustomHighlighter;

    function TrouveChemin(Dep, Arr, Dist, Lim : integer) : boolean;
    procedure InitPlan;
    function cursorOnHotSpot : integer;
    procedure LoadFromFile(FileName : String);
  public
    { Public-d�clarations }
    Data : TNebData;
    EstActive : boolean;
    CursorTrack : boolean;
    OldMondeTrack : integer;

    constructor CreateData(AOwner : TComponent; var Data2 : TNebData; CR: TStringList);
    procedure UpdateFont;
    procedure TrouveMonde(NoMonde : integer; b_Show : boolean);
    procedure SaveOrdres;
    procedure ChercheMondeCourant(track : boolean);
    procedure AddHeader(List : TStringList);
  end;

var
  OrdresForm: TOrdresForm;

implementation

{$R *.DFM}

uses NebutilWin, Plan, Resultat, Preferences, Lectordres, Winplan, Chemin;

constructor TOrdresForm.CreateData(AOwner : TComponent; var Data2 : TNebData; CR: TStringList);
var
  Ch: string;
  i : integer;
  fileName : string;
  filePath : string;
  ligne : string;
  ligneIndex: Integer;
  NoMonde: integer;
  OrdresList : TStringList;
  MondeOrdre : integer;
  LigneDebOrdres : integer;
  Flottes : array[0..32] of integer;
  VC : array[0..32] of integer;
  VT : array[0..32] of integer;
  RapForceStr : string;
begin
  TailleSelection := 0;
  OldTailleSelection := 0;
  DebutSelection := 0;
  OldDebutSelection := 0;
  Data := Data2;
  inherited Create(AOwner);
  mwEdit.Show;

  Left := NebWin.Ini.ReadInteger('Fenetres', 'Ordres_Gauche', Left);
  Top := NebWin.Ini.ReadInteger('Fenetres', 'Ordres_Haut', Top);
  Height := NebWin.Ini.ReadInteger('Fenetres', 'Ordres_Hauteur', Height);
  Width := NebWin.Ini.ReadInteger('Fenetres', 'Ordres_Largeur', Width);
  WindowState := NebWin.StrToEtatFen(nebWin.Ini.ReadString('Fenetres', 'Ordres_Etat', 'Normale'));

  Ch := IntToStr(Data.NoJou);
  if Data.NoJou < 10 then
    Ch := '0' + Ch;

  fileName := Format('%sO%s%s.TXT', [Data.NomPartie.Substring(0, 3),
    Ch, format('%.2d', [Data.Tour+1])]);
  Caption := Format('Saisie des ordres - [%s] (Alt+F3)', [fileName]);

  filePath := NebWin.RepFic + fileName;
  If FileExists(filePath) then
  begin
    UpdateFont;
    LoadFromFile(filePath);
  end
  else
  begin
    mwEdit.ClearAll;
    UpdateFont;
    OrdresList := TStringList.Create;
    OrdresList.BeginUpdate;

    AddHeader(OrdresList);

    // ajout des indications de mondes @M_m@
    if Assigned(CR) then
    begin
      ligne := '';
      NoMonde := 0;
      MondeOrdre := 0;
      for ligneIndex := 0 to CR.Count - 1 do
      begin
        ligne := CR[ligneIndex];

        try
          // Mondes
          if ligne.StartsWith('M_') or ligne.StartsWith('M#') then
            NoMonde := ligne.Substring(2, ligne.IndexOf(' ') - 2).ToInteger()
          else if ligne.StartsWith('Md_') or ligne.StartsWith('Md#') then
            NoMonde := ligne.Substring(3, ligne.IndexOf(' ') - 3).ToInteger()
          // Flottes et tr�sors
          else if ligne.Contains('T_') or
            ligne.Contains('F_') or ligne.Contains('F#') then
          begin
            if not PrefForm.OrdresFlottes then
              NoMonde := 0;
            if  ligne.Contains('{F') and (not PrefForm.OrdresTraces) then
              NoMonde := 0;
          end
          else if (ligne = '') or (ligneIndex = CR.Count - 1) or not CharInSet(ligne.Chars[0], ['M', ' ']) then
            NoMonde := 0;
        except
          on E : EConvertError do NoMonde := 0;
        end;

        if NoMonde > 0 then // On ins�re en commentaire la ligne
        begin
          MondeOrdre := NoMonde;
          if ligne.StartsWith('M') then
          begin
            OrdresList.Add('');
            OrdresList.Add('');
            OrdresList.Add('');
            OrdresList.Add(Format('; Monde %0:d'#30'(@M_%0:d@)'#31, [NoMonde]));
          end;

          // Ligne de description du monde du CR en commentaire
          if PrefForm.OrdresMondes {PrefForm.CROrdres.Checked} then
          begin
            OrdresList.Add(Format('; %s', [ligne]));
          end
          else
            NoMonde := 0;
        end;

        LigneDebOrdres := OrdresList.Count;
        if (NoMonde = 0) and (MondeOrdre > 0) then
        begin
          // Calcul des rapports de forces
          FillChar(Flottes, sizeof(Flottes), 0);
          FillChar(VC, sizeof(VC), 0);
          FillChar(VT, sizeof(VT), 0);

          for i := 1 to Data.NbFlotte do
            if Data.F[i].Localisation = MondeOrdre then
            begin
              inc(Flottes[Data.F[i].Proprio]);
              inc(VC[Data.F[i].Proprio], Data.F[i].NbVC);
              inc(VT[Data.F[i].Proprio], Data.F[i].NbVT);
            end;
          inc(VC[Data.M[MondeOrdre].Proprio], Data.M[MondeOrdre].VI);
          inc(VC[Data.M[MondeOrdre].Proprio], Data.M[MondeOrdre].VP);

          // Constructions
          if PrefForm.OrdresConstr then
          begin
            if (Data.M[MondeOrdre].Proprio = Data.NoJou) and
               (Data.ME[MondeOrdre].IndLibre > 0) then
            begin
              OrdresList.Add(Format('M_%d C %d ',
                [MondeOrdre, Data.ME[MondeOrdre].IndLibre]));
            end;
          end;

          for i := 1 to Data.NbFlotte do
          begin
            // D�chargements automatiques
            if PrefForm.OrdresDechCar then
            begin
              if (Data.F[i].Localisation = MondeOrdre) and
                 (Data.F[i].Proprio = Data.NoJou) then
              begin
                if (Data.F[i].MP > 0) and
                   (Data.M[MondeOrdre].Ind > 0) then
                  OrdresList.Add(Format('?F_%d D MP', [i]));
                if Data.F[i].N  > 0 then
                  OrdresList.Add(Format('?F_%d D N', [i]));
                if Data.F[i].C  > 0 then
                  OrdresList.Add(Format('?F_%d D C', [i]));
                if Data.F[i].R > 0 then
                  OrdresList.Add(Format('?F_%d D R', [i]));
              end;
            end;

            //Transferts flottes vides
            if PrefForm.OrdresTransFV then
            begin
              // calcul du nb de flottes vides du proprio sur le monde
              if (Data.F[i].Localisation = MondeOrdre) and
                 (Data.F[i].Proprio = Data.NoJou) and
                 (Data.F[i].NbVC + Data.F[i].NbVT = 0) //and
                 //(VC[MondeOrdre] + VT[MondeOrdre] - Data.M[MondeOrdre].VI - Data.M[MondeOrdre].VP - Flottes[Data.NoJou] + FlottesVides > 0)
                 then
                 begin
                   OrdresList.Add(Format('F_ T x VC F_%d', [i]));
                 end;
            end;

            // Chargements automatiques
            if PrefForm.OrdresChargeMP then
            begin
              if (Data.F[i].Localisation = MondeOrdre) and
                 (Data.F[i].Proprio = Data.NoJou) and
                 (Data.M[MondeOrdre].Proprio = Data.NoJou) and
                 (Data.CapaciteFlotte(i) > Data.ContenuFlotte(i)) and
                 (Data.M[MondeOrdre].PlusMP >= Data.M[MondeOrdre].Ind) and
                 (Data.M[MondeOrdre].MP > Data.ME[MondeOrdre].IndLibre) then
              begin
                OrdresList.Add(Format('?F_%d C MP', [i]));
              end;
            end;

            // D�placements
            if PrefForm.OrdresDeplFlottes then
            begin
                if (Data.F[i].Localisation = MondeOrdre) and
                   (Data.F[i].Proprio = Data.NoJou) then
                begin
                  OrdresList.Add(Format('F_%d -> ', [i]));
                end;
            end;
          end;

          // Affichage des rapports de forces
          if PrefForm.OrdresRapForce then
          begin
            for i := 0 to Data.NbJou do
              if (Flottes[i] + VC[i] + VT[i] > 0) then
              begin
                if i > 0 then
                  RapForceStr := Format('# %-30s : ', [Data.Jou[i].Pseudo])
                else
                  RapForceStr := Format('# %-30s : ', ['neutre']);
                if VT[i] > 0 then
                begin
                  RapForceStr := RapForceStr + Format('%dT', [VT[i]]);
                  if VC[i] > 0 then
                    RapForceStr := RapForceStr + '+';
                end;

                if VC[i] > 0 then
                begin
                  RapForceStr := RapForceStr + Format('%d', [VC[i]]);
                  if VT[i] = 0 then
                    RapForceStr := RapForceStr + ' VC';
                end
                else
                  if VT[i] + VC[i] = 0 then
                    RapForceStr := RapForceStr + '0';

                if Flottes[i] > 0 then
                begin
                  if Flottes[i] > 1 then
                    RapForceStr := RapForceStr + Format(' (%d flottes)', [Flottes[i]])
                  else
                    RapForceStr := RapForceStr + Format(' (%d flotte)', [Flottes[i]])
                end;
                OrdresList.Add(RapForceStr);
              end;
          end;

          if LigneDebOrdres <> OrdresList.Count then
            OrdresList.Insert(LigneDebOrdres, '');
          MondeOrdre := 0;
        end;

        if ligne.StartsWith('Ordres du joueur') then
          break;
      end;
    end;

    OrdresList.Add('');
    OrdresList.Add('');
    OrdresList.Add('');
    OrdresList.Add('; Autres ordres (@M_0@)');
    OrdresList.Add('');

    OrdresList.EndUpdate;

    mwEdit.Lines.BeginUpdate;
    mwEdit.Lines.Text := OrdresList.text;
    mwEdit.Lines.EndUpdate;

    NebWin.StatusBar.Panels.Items[4].Text := '';
    OrdresList.Free;
  end;

  NbLignes := mwEdit.Lines.Count;
  mwEdit.Modified := False;
end;

procedure TOrdresForm.FormCreate(Sender: TObject);
begin
  mwEdit.Align := alClient;
  NbMemoLock := 0;
  EstActive := true;
  AutoSaveTimer.Enabled := PrefForm.SauvegardeAuto.Checked;
  CursorTrack := PrefForm.CursorTrack.Checked;
  OldMondeTrack := 0;

  // Mise en place de la syntaxedes ordres
//  SynNebulaSyn := TSynNebula.Create(Self);
  SynNebulaSyn := TSynNebOrdres.Create(Self);
  mwEdit.Highlighter := SynNebulaSyn;
end;

procedure TOrdresForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  sor : TSortieFOrm;
  NomFic : string;
begin
  if mwEdit.Modified then
  begin
    NomFic := NebWin.GetOrdresFileName;

    sor := TSortieForm.Create(Self);

    sor.Caption := Format('Sauvegarde dans [%s]', [NomFic]);
    sor.Ligne1.Caption := 'Les ordres ont �t� modifi�s.';
    sor.Ligne2.Caption := 'Voulez-vous les sauver ?';
    sor.ShowModal;
    CanClose := False;
    case sor.Modalresult of
      mrYes :
      begin
        CanClose := True;
        SaveOrdres;
      end;
      mrNo : CanClose := True;
    end;
  end;
end;

procedure TOrdresForm.ColorMemoKeyPress(Sender: TObject; var Key: Char);
begin
  if PrefForm.ConvertCaract.Checked then
  case Key of
    '�' : Key := '*';
    '/' : Key := ':';
    '.' : Key := ';';
    '<' : Key := '>';
  end;
  NebWin.StatusBar.Panels.Items[4].Text := 'Ordres modifi�s';
end;

procedure TOrdresForm.PopupMenuClick(Sender: TObject);
begin
  mwEdit.SelText := '12';
end;

procedure TOrdresForm.FormShow(Sender: TObject);
var
  i : integer;
  TabMenu : array[0..NB_JOU_MAX-1] of TMenuItem;
begin
  if Data <> nil then
  begin
    FillChar(TabMenu, sizeof(TabMenu), 0);
    for i := 1 to NB_JOU_MAX do
    begin
      TabMenu[i-1] := NewItem(Data.Pseudos[i] + ':' + IntToStr(i), 0, False, True, MenuClick, 0, Format('Jou%d', [i]));
      if (Data.Pseudos[i] = '') or (i = Data.NoJou) then
        TabMenu[i-1].Visible := False;
    end;
    JoueursMenu := NewPopupMenu(mwEdit, 'JoueursMenu', paLeft, True, TabMenu);
  end;
  mwEdit.PopupMenu := JoueursMenu;
end;

procedure TOrdresForm.MenuClick(Sender: TObject);

  function FormatDeclarat(Ordre, Comment : string; NumJou : integer) : string;
  const
    CommentPlace = 25;
  var
    i : integer;
  begin
    Result := Ordre;
    for i := Ordre.Length to CommentPlace do Result := Result + ' ';
    Result := Format('%s ; %s "%s"', [Result, Comment, Data.Pseudos[NumJou]]);
  end;

var
  i,n : integer;
  ch : string;
  Ligne : integer;
  LigneStr, name : string;
  bid : integer;
  O : TOrdre;
  XPos : integer;
  NebOrdres: TNebOrdres;
begin
  NebOrdres := TNebOrdres.Create;
  name := (Sender as TMenuItem).Name;
  n := name.Substring(3, 2).ToInteger();
  LigneStr := mwEdit.LineText;
  Xpos := LigneStr.Length;
  ch := '';
  if mwEdit.CaretX > 0 then
  begin
    if LigneStr.Length > 0 then
    begin
      XPos := mwEdit.CaretX-1;
      if XPos = 0 then XPos := 1;

      if XPos > LigneStr.Length then
        XPos := LigneStr.Length;
      if (LigneStr.Chars[XPos] = ':') or LigneStr.EndsWith(':') then
        ch := ' ';
    end;
  end;
  ch := ch + IntToStr(n);
  LigneStr := LigneStr.Substring(0, Xpos) + Ch;

  bid := Data.NoJou;
  NebOrdres.ParseOrdre(LigneStr, O);
  if O.Ch[1] = '@C' then
  begin
    Ligne := mwEdit.CaretY-1;
    mwEdit.Lines.Delete(Ligne);
    mwEdit.Lines.Insert(Ligne,
      FormatDeclarat(Format('C : %d', [n]),
                     Format('C :', [n]),
                     n));
    mwEdit.Lines.Insert(Ligne + 1,
      FormatDeclarat(Format('CP : %d', [n]),
                     Format('CP :', [n]),
                     n));
    mwEdit.Lines.Insert(Ligne + 2,
      FormatDeclarat(Format('DP : %d', [n]),
                     Format('DP :', [n]),
                     n));
  end
  else
  begin
    //Traite(LigneStr, O, bid, '', '');
    NebOrdres.Traite(LigneStr, O, bid);
    if O.Erreur = 0 then
    begin
      for i := LigneStr.Length to 25 do LigneStr := LigneStr + ' ';
      LigneStr := LigneStr + ' ';
      LigneStr := LigneStr + '; ';
      i := 1;
      while O.Ch[i+1] <> '' do
      begin
        LigneStr := LigneStr + O.Ch[i] + ' ';
        inc(i);
      end;
      LigneStr := LigneStr + '"' + Data.Pseudos[n] + '"';
    end;
    mwEdit.LineText := LigneStr;
  end;
  NebOrdres.Free;
end;

procedure TOrdresForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TOrdresForm.ColorMemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  LigneStr : string;
  i : integer;
  chaine : string;
  Flotte, dep, arr : integer;
  O : TOrdre;
  CheminList : TCheminList;
  arrStr : string;
begin
  if Key = VK_RETURN then
  begin
    LigneStr := mwEdit.LineText;
    TNebOrdres.ParseOrdre(LigneStr, O);

    if (O.Ch[1] = 'F') and EstNombre(O.Ch[2]) and (O.Ch[3] = '->') and
      (EstNombre(O.Ch[4]) or (UpperCase(O.Ch[4])='MD')) and (O.Ch[5] = '') then
    begin
      Key := 0;
      Flotte := StrToInt(O.Ch[2]);
      if O.Ch[4].ToUpper = 'MD' then
      begin
        arr := Data.Jou[Data.NoJou].Md;
        arrStr := 'Md';
      end
      else
      begin
        arr := StrToInt(O.Ch[4]);
        arrStr := O.Ch[4];
      end;
      dep := Data.F[Flotte].localisation;

      if dep > 0 then
      begin
        CheminList := TCheminList.Create(Data);
        CheminList.RecenceChemins(dep, arr);
        CheminList.Sort;

        if CheminList.Count > 0 then
        begin
           // Affichage de l'ordre de d�placement
           Chaine := 'F ' + IntToStr(Flotte);
           if CheminList[0].Count > 1 then
             Chaine := Chaine + ' MM'
           else
             Chaine := Chaine + ' M';
           for i := 1 to CheminList[0].Count - 1 do
             Chaine := Chaine + ' ' + IntToStr(CheminList[0][i]);
           for i := Length(Chaine) to 25 do
             Chaine := Chaine + ' ';
           Chaine := Chaine + ' ';
           Chaine := Chaine + '; F ' + IntToStr(Flotte) + ' -> ' + arrStr;
           Chaine := Chaine + ' [dist = ' + IntToStr(CheminList[0].Count - 1) + ']';
           mwEdit.LineText := Chaine;
           PlanForm.DessineChemin(CheminList[0]);
           Key := VK_END;
           Shift := [];
        end;
      end
      else
        ShowMessage('Vous ne poss�dez pas la flotte ' +
          IntToStr(Flotte));
    end;
  end;
end;

procedure TOrdresForm.ChercheMondeCourant(track : boolean);
var
  ligne, p, monde : integer;
  go : boolean;
begin
  monde := 0;
  go := false;
  // On retrouve la ligne courante et on remonte
  for ligne := mwEdit.CaretY - 1 downto 0 do
  begin
    p := mwEdit.Lines[ligne].IndexOf('@M_');
    if p <> -1 then
    begin
      try
        inc(p, 3);  // on avance du pr�fixe @M_
        monde := mwEdit.Lines[ligne].Substring(p, mwEdit.Lines[ligne].IndexOf('@', p) - p).ToInteger();
      except
        on E : EConvertError do monde := 0;
      end;
      if monde > 0 then
      begin
        if track then
        begin
          if monde <> OldMondeTrack then go := true;
          OldMondeTRack := monde;
        end
        else go := true;
        if go then
        begin
          if assigned(PlanForm) then
            PlanForm.WinPlan.ChercheMonde(monde);
          if not track then
            ResultForm.TrouveMondeSurCR(monde);
        end;
      end;
      break;
    end;
  end;
  if not track then
  begin
    if monde = 0 then
      ShowMessage('Impossible de trouver @M_m@ haut-dessus de la ligne courante');
  end
end;


function TOrdresForm.TrouveChemin(Dep, Arr, Dist, Lim : integer) : boolean;
var
  i : integer;
  ResTempo : boolean;
begin
  if dist = 0 then
    InitPlan;

  PlanDist[Dep] := Dist;

  if Dep = Arr then
    Result := True
  else
    Result := False;

  if (Dep <> Arr) and (Dist < Data.Jou[Data.NoJou].DEP) then
  begin
    for i := 1 to 8 do
      if (Data.M[Dep].Connect[i] <> 0) then
        if ((PlanType[Data.M[Dep].Connect[i]] <= Lim) or
           (Data.M[Dep].Connect[i] = Arr)) and
           (PlanDist[Data.M[Dep].Connect[i]] > Dist + 1) then
        begin
          PlanFrom[Data.M[Dep].Connect[i]] := Dep;
          ResTempo := TrouveChemin(Data.M[Dep].Connect[i], Arr, Dist + 1, Lim);
          Result := Result or ResTempo;
        end;
  end;

end;

procedure TOrdresForm.InitPlan;
var
  i : integer;
begin
  FillChar(PlanDist, sizeof(PlanDist), 255);
  FillChar(PlanType, sizeof(PlanType), 255);
  FillChar(PlanFrom, sizeof(PlanType), 0);

  {1 -> tout est aujoueur}
  {2 -> Monde ou une flotte n'appartient pas au joueur}
  {A peaufiner !}

  for i := 1 to Data.NbMonde do
  begin
    if Data.M[i].Proprio = Data.NoJou then
      PlanType[i] := 1
    else if Data.M[i].Proprio = 0 then
    begin
      if (Data.M[i].VI + Data.M[i].VP = 0) and
        (Data.TourSurListing[i] = Data.Tour)
      then
        PlanType[i] := 1
      else
        PlanType[i] := 3;
    end
    else
      PlanType[i] := 3;

  end;

  for i := 1 to 1024 do
  begin
    if (Data.F[i].Localisation <> 0) and (Data.F[i].proprio <> Data.NoJou)
    then
    begin
      if ((Data.F[i].Proprio <> 0) or ((Data.F[i].Proprio = 0) and
        (Data.F[i].NbVC + Data.F[i].NbVT > 0))) and
        (PlanType[Data.F[i].Proprio] < 3) then
        inc(PlanType[Data.F[i].Proprio]);
    end;
  end;
end;

procedure TOrdresForm.FormDestroy(Sender: TObject);
begin
  OrdresForm := nil;
  SynNebulaSyn.Free;
end;

procedure TOrdresForm.FormActivate(Sender: TObject);
begin
  NebWin.FindDialog.CloseDialog;
  NebWin.ChercherMenu.Enabled := True;
  NebWin.ChercherMondeMenu.Enabled := True;
  NebWin.OccSuivMenu.Enabled := True;
  EstActive := True;
  NebWin.AllerMondeCourMenu.Enabled := True;
end;

procedure TOrdresForm.FormDeactivate(Sender: TObject);
begin
  NebWin.ChercherMenu.Enabled := False;
  NebWin.OccSuivMenu.Enabled := False;
  EstActive := False;
end;

procedure TOrdresForm.UpdateFont;
begin
    mwEdit.Font.Assign(NebWin.MemoFont);
end;

procedure TOrdresForm.TrouveMonde(NoMonde : integer; b_Show : boolean);
var
  i : integer;
  monde : string;
begin
  monde := Format('@M_%d@', [NoMonde]);
  for i := 0 to mwEdit.Lines.Count - 1 do
    if mwEdit.Lines[i].Contains(monde) then
    begin
      mwEdit.CaretY := i + 1;
      mwEdit.CaretX := 0;
      if i > 0 then
        mwEdit.TopLine := i
      else
        mwEdit.TopLine := 1;
      if b_Show then Show;
      break;
    end;
end;

procedure TOrdresForm.SelectAll1Click(Sender: TObject);
begin
  mwEdit.SelectAll;
end;

procedure TOrdresForm.ColorMemoHotSpotClick(Sender: TObject; const SRC: String);
var
  Md : integer;
  MdCh : string;
  Err : integer;
begin
  MdCh := SRC.Substring(4);
  MdCh := MdCh.Substring(0, MdCh.IndexOf('@'));
  Val(MdCh, Md, Err);
  if Err <> 0 then
    ShowMessage('Erreur le format doit �tre "(@M_m@)"')
  else
    ResultForm.TrouveMondeSurCR(Md);
end;

procedure TOrdresForm.AutoSaveTimerTimer(Sender: TObject);
begin
  // Sauvegarde des ordres
  SaveOrdres;
end;

procedure TOrdresForm.SaveOrdres;
begin
  // On sauve le fichier d'ordres;
  //mwEdit.Lines.SaveToFile(NebWin.GetOrdresFileName);
  mwEdit.Lines.SaveToFile(NebWin.GetOrdresFileName, NebWin.DefaultEncoding);
  mwEdit.Modified := False;
  NebWin.StatusBar.Panels.Items[4].Text := '';
end;

procedure TOrdresForm.ColorMemoClick(Sender: TObject);
var
  noMonde : integer;
begin
  if CursorTrack then
    ChercheMondeCourant(true);
  noMonde := cursorOnHotSpot;
  if noMonde > 0 then
    ResultForm.TrouveMondeSurCR(noMonde);
end;

procedure TOrdresForm.ColorMemoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if CursorTrack and
    (Key in [VK_RETURN, VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN, VK_PRIOR, VK_NEXT]) then
  begin
    ChercheMondeCourant(true);
  end;
end;

// Ins�re un en-t�te au d�but de la liste pass�e en param�tres
procedure TOrdresForm.AddHeader(List : TStringList);
var
  ChaineTempo : string;
  ChaineVersion : string;
  ChaineVide, ChaineDiese : string;
  TailleMax : integer;
  i : integer;
begin
  ChaineTempo := Format('# Ordres du joueur "%s":%d pour le tour %d',
    [Data.Pseudos[Data.NoJou], Data.NoJou, Data.Tour + 1]);
  ChaineVersion := '# Ordres saisis avec Nebutil version ' + NoVersionNebutil;
  TailleMax := Max(ChaineTempo.Length, ChaineVersion.Length);
  for i := Length(ChaineVersion) + 1 to TailleMax + 1 do
      ChaineVersion := ChaineVersion + ' ';
  for i := Length(ChaineTempo) + 1 to TailleMax + 1 do
      ChaineTempo := ChaineTempo + ' ';
  ChaineVersion := ChaineVersion + '#';
  ChaineTempo := ChaineTempo + '#';

  ChaineVide := '#';
  For i := 2 to TailleMax + 2 do
    ChaineVide := ChaineVide + ' ';
  ChaineVide := ChaineVide + '#';

  ChaineDiese := '';
  for i := 1 to TailleMax + 2 do
    ChaineDiese := ChaineDiese + '#';
  List.Insert(0, ChaineDiese);
  List.Insert(1, ChaineTempo);
  List.Insert(2, ChaineVersion);
  List.Insert(3, ChaineDiese);
  List.Insert(4, '');
end;

function TOrdresForm.cursorOnHotSpot: integer;
var
  linePos : integer;
  line : string;
  debut, fin : integer;
begin
  Result := 0;
  line := mwEdit.LineText;
  linePos := mwEdit.CaretX;
  if (linePos = 0) or (linePos = line.Length) then
    Exit;
  debut := line.IndexOf('@M_');
  if debut = 0 then
    Exit;
  fin := line.IndexOf('@', debut + 1);
  if (debut > linePos) or (fin < linePos) then
    Exit;

  // On a pass� le gros des tests. On doit avoir une cha�ene de la forme @M_m@
  try
    Result := line.Substring(debut + 3, fin - debut - 3).ToInteger();
  except
    on E : EConvertError do Result := 0;
  end;
end;

procedure TOrdresForm.LoadFromFile(FileName: String);
var
  OrdresList : TStringList;
  c : integer;
  buf : string;
begin
  // On pr�pare les ordres avant de les charger dans
  // la fen�tre d'ordre
  // On remplace #30 et #31 par des espaces.
  OrdresList := TStringList.Create;
  OrdresList.LoadFromFile(FileName);
  buf := OrdresList.text;
  for c := 1 to buf.Length do
    if (buf[c] = #30) or (buf[c] = #31) then
      buf[c] := ' ';
  OrdresList.Text := buf;

  mwEdit.Lines.Assign(OrdresList);
  OrdresList.Free;
end;

end.
