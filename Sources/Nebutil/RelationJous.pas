unit RelationJous;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, NebData, DetailsJou, ImgList, System.ImageList;

type
  TRelationsForm = class(TForm)
    ListView: TListView;
    ClassesImages: TImageList;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ModifierButton: TButton;
    Label1: TLabel;
    ConfMaxEdit: TEdit;
    ConfianceMax: TUpDown;
    BitBtn3: TBitBtn;
    procedure ListViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ListViewKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListViewDblClick(Sender: TObject);
  private
    { D�clarations priv�es }
    FData : TNebData;
    OriginalData : TNebdata;

    procedure SetData(theData : TNebData);
    procedure UpdateItem(Item : TListItem);
    function GetIconIndex(jou : integer) : integer;
  public
    { D�clarations publiques }
    property Data : TNebData read FData write SetData;
  end;

implementation

uses AvatUnit;

{$R *.DFM}

procedure TRelationsForm.ListViewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ListView.SelCount > 0 then
    ModifierButton.Enabled := True;
end;

procedure TRelationsForm.ListViewKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ListView.SelCount > 0 then
    ModifierButton.Enabled := True;
end;

procedure TRelationsForm.SetData(theData : TNebData);
var
  jou : integer;
  Item : TListItem;
begin
  FData.Free;
  FData := TNebData.Create('', 0);
  FData.Assign(theData);
  OriginalData := theData;

  // On rempli la ListView avec les infos de Data;

  ListView.Items.BeginUpdate;
  ListView.Items.Clear;
  for jou := 1 to Data.NbJou do if (jou <> Data.NoJou) and (Data.ConnuDeNom[Data.NoJou, jou]) then
  begin
    Item := ListView.Items.Add;
    Item.ImageIndex := GetIconIndex(jou);
    Item.Data := Pointer(jou);
    Item.Caption := Format('%s:%d', [Data.Jou[jou].Pseudo, jou]);
    Item.SubItems.Add(IntToStr(Data.Confiance[jou]));
    Item.SubItems.Add(IntToStr(Data.Jou[jou].ATT));
    Item.SubItems.Add(IntToStr(Data.Jou[jou].DEF));
    if Data.EstAllie(jou, Data.NoJou) then
      Item.SubItems.Add('Alli�')
    else
      Item.SubItems.Add('Ennemi');
    if Data.EstChargeur(jou, Data.NoJou) then
      Item.SubItems.Add('Oui')
    else
      Item.SubItems.Add('Non');
    if Data.EstChargeurDePop(jou, Data.NoJou) then
      Item.SubItems.Add('Oui')
    else
      Item.SubItems.Add('Non');
    if Data.EstDechargeurDePop(jou, Data.NoJou) then
      Item.SubItems.Add('Oui')
    else
      Item.SubItems.Add('Non');
  end;
  ListView.Items.EndUpdate;
  ConfianceMax.Position := Data.ConfianceVaisseaux;
end;

procedure TRelationsForm.BitBtn1Click(Sender: TObject);
begin
  FData.ConfianceVaisseaux := ConfianceMax.Position;
  OriginalData.Assign(FData);
end;

procedure TRelationsForm.FormDestroy(Sender: TObject);
begin
  FData.Free;
end;

procedure TRelationsForm.FormCreate(Sender: TObject);
begin
  ListView.Items.Clear;
  // Test Overlay
//  if not ClassesImages.Overlay(22, 0) then
//    ShowMessage('non');
end;

procedure TRelationsForm.ListViewDblClick(Sender: TObject);
begin
  if Assigned(ListView.Selected) then
  begin
    DetailsJouForm := TDetailsJouForm.Create(Self);
    DetailsJouForm.ClassesImages := ClassesImages;
    DetailsJouForm.Init(FData, Integer(ListView.Selected.Data));
    if DetailsJouForm.ShowModal = idOK then
    begin
      UpdateItem(ListView.Selected);
    end;
    DetailsJouForm.Free;
  end;
end;

procedure TRelationsForm.UpdateItem(Item : TListItem);
var
  jou : integer;
begin
  jou := Integer(Item.Data);
  Item.ImageIndex := GetIconIndex(jou);
  Item.SubItems[0] := IntToStr(Data.Confiance[jou]);
  Item.SubItems[1] := IntToStr(Data.Jou[jou].ATT);
  Item.SubItems[2] := IntToStr(Data.Jou[jou].DEF);

  if Data.EstAllie(jou, Data.NoJou) then
    Item.SubItems[3] := 'Alli�'
  else
    Item.SubItems[3] := 'Ennemi';

  if Data.EstChargeur(jou, Data.NoJou) then
    Item.SubItems[4] := 'Oui'
  else
    Item.SubItems[4] := 'Non';

  if Data.EstChargeurDePop(jou, Data.NoJou) then
    Item.SubItems[5] := 'Oui'
  else
    Item.SubItems[5] := 'Non';

  if Data.EstDechargeurDePop(jou, Data.NoJou) then
    Item.SubItems[6] := 'Oui'
  else
    Item.SubItems[6] := 'Non';
end;

function TRelationsForm.GetIconIndex(jou : integer) : integer;
begin
  Result := Data.Jou[jou].Classe;
  if Result > 0 then
  begin
    if FData.Confiance[jou] < 3 then
      Inc(Result, 2 * NBCLASSE)
    else if FData.Confiance[jou] > 3 then
      Inc(Result, NBCLASSE)
  end;
end;

end.
