﻿unit ExtraitCR;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, RegularExpressions,
  nebdata, avatunit, Vcl.WinXPanels, Vcl.ComCtrls, Generics.Collections, Generics.Defaults;

type
  TFragmentChemin = record
    Monde, Du, Vers: Integer;
    function Compare(AutreFragment: TFragmentChemin): Integer;
  end;

  TChemin = class(TList<TFragmentChemin>)
    function Add(Fragment: TFragmentChemin): Integer;
    function IsEqual(Chemin: TChemin): Boolean;
  end;

  TChemins = TArray<TChemin>;

  TCRParser = class
  type
    TypeLigne = (TL_MONDE, TL_MONDE_TRESOR, TL_FLOTTE, TL_FLOTTE_TRESOR,
      TL_TRACE, TL_NOM_MONDE, TL_JOUEUR, TL_VIDE, TL_NON_VIDE, TL_FINCR);

  private
    regexNomMonde, regexMonde, regexFlotte, regexTresor, regexTrace, regexJoueur : TRegEx;
    regexEspacesDebutLigne : TRegEx;

    var Data : TNebData;
    idx1 : uint16;

    ligne : String;
    numLigne : UInt16;
    numMonde : UInt16;
    numFlotte : UInt16;
    numTresor : UInt16;
    nomMonde: String;
    Chemins : TChemins;
  public
    Erreurs : TStringList;
    JoueursInconnus: TStringList;

    constructor Create(d: TNebData);
    destructor Destroy; override;

    procedure ImportExtrait(Data: TNebData; extrait : TStrings);
    function UpdateJoueurInconnu(Pseudo: String; NumJou: Integer): Boolean;

  private
    { Déclarations privées }
    ComparateurChemins: IComparer<TFragmentChemin>;

    procedure blocSuivant;
    function ImportMonde(NumLigne: Integer) : boolean;
    procedure ImportNomMonde;
    procedure ImportMondeRaz;
    function ImportMondeConnexion : boolean;
    function ImportMondeProprietaire(NumLigne: Integer) : boolean;
    function ImportMondeIndustrie : boolean;
    function ImportMondePopulation : boolean;
    function ImportMondeProduction : boolean;
    function ImportMondePillage : boolean;
    function ImportMondePC : boolean;
    function ImportMondeExplo : boolean;
    function ImportMondeCoordonnees : boolean;
    function ImportFlotte : boolean;
    procedure ImportFlotteRaz;
    function ImportFlotteProprietaire : boolean;
    function ImportFlotteCargaison : boolean;
    function ImportFlotteTir : boolean;
    function ImportFlotteVaisseau : boolean;
    function ImportFlotteDeplacement : boolean;
    procedure ImportFlotteTrace(ligne: String);
    function ImportTresor : boolean;
    function ImportJoueur(ligne: String) : boolean;

    procedure Clear;
    procedure MAJMondesConnectes;

    function ChercheNoJoueur(Pseudo : String) : uint16;
    procedure AddJoueurInconnu(Pseudo: String);
    procedure FinaliseFlottesTraces;
    procedure FreeChemins;
    procedure AjouteChemin(fl, Du, Mde, Vers: Integer);


  end;

  TExtraitCRDialog = class(TForm)
    CardPanel: TCardPanel;
    CardCR: TCard;
    Label1: TLabel;
    Memo: TMemo;
    CancelBtn: TButton;
    OKBtn: TButton;
    CardNumJou: TCard;
    JoueurInconnuLabel: TLabel;
    NomJoueurInconnuEdit: TEdit;
    NumJouEdit: TEdit;
    NumJouUpDown: TUpDown;
    PoursuivreButton: TButton;

    procedure MemoChange(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelBtnClick(Sender: TObject);
    procedure PoursuivreButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure NumJouUpDownChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Integer; Direction: TUpDownDirection);
    procedure NumJouEditChange(Sender: TObject);

  private
    Data: TNebData;
    Parser: TCRParser;

    function GetErreurs: TStringList;
    function GetJoueursInconnus: TStringList;
    procedure SaisieNumeroJoueurInconnu;
    procedure CheckJoueursInconnus;
    procedure ImportExtract(Extrait : TStrings);
    function GetNextJouInconnus(NumJou: Integer): Integer;
    function GetPremierJouInconnus: Integer;
    function GetPrevJouInconnus(NumJou: Integer): Integer;
    function GetDernierJouInconnus: Integer;

  public
    procedure Run(Data: TNebData);
    property Erreurs: TStringList read GetErreurs;
    property JoueursInconnus: TStringList read GetJoueursInconnus;
  end;

var
  ExtraitCRDialog: TExtraitCRDialog;
const
  TabAlign = 8;

implementation

{$R *.dfm}

uses Dialogs, MapCoord, System.Character;

constructor TCRParser.Create(d : TNebData);
begin
  regexNomMonde.Create('^"([^"]+)"$');
  regexMonde.Create('^Md{0,1}[_#](\d+)');
  regexFlotte.Create('^\s+F[_#](\d+)');
  regexTresor.Create('^\s+T_(\d+)');
  regexTrace.Create('^\s+\{F_(\d+)');
  regexTrace.Create('^\s+\{F_(?P<fl>\d+)\s+"(?P<pseudo>[^"]+)"(\s+du M_(?P<du>\d+|\?))?\s+vers M_(?P<vers>\d+)');
  regexJoueur.Create('\s*"(?P<pseudo>[^"]+)":(?P<numjou>\d+)\s*');
  regexEspacesDebutLigne := TRegEx.Create('^\s+');

  Data := d;
  Erreurs := TStringList.Create;
  JoueursInconnus := TStringList.Create;

  ComparateurChemins := TComparer<TFragmentChemin>.Construct(
    function(const A, B: TFragmentChemin): Integer
    begin
      Result := A.Compare(B);
    end
  );
end;

destructor TCRParser.Destroy;
begin
  FreeAndNil(Erreurs);
  FreeAndNil(JoueursInconnus);
  FreeChemins;
  inherited;
end;

// Comme les espaces jusqu'a prochaine groupe de données à importer
procedure TCRParser.blocSuivant;
begin
  while ligne.Chars[idx1] = ' ' do inc(idx1);
end;

// RaZ du monde
procedure TCRParser.ImportMondeRaz;
begin

end;

procedure TCRParser.ImportNomMonde;
var
  Match: TMatch;
begin
  Match := regexNomMonde.Match(ligne);
  if Match.Groups.Count = 2 then
    nomMonde := Match.Groups[1].Value;
end;

// Importe les connexions d'un monde
function TCRParser.ImportMondeConnexion : boolean;
var
  idx2 : UInt16;
  splitted : TArray<String>;
  tmp : String;
  n : integer;
  connexion: TConnexion;
begin
    Result := True;
  with Data do
  begin
    // liste des connexion
    if ligne.Chars[idx1] <> '(' then
    begin
      erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + IntToStr(numMonde) + ' echec d''analyse des connexions');
      Result := False;
      exit;
    end;
    inc(idx1);

    if idx1 >= ligne.Length then
      Exit;

    idx2 := idx1 + 1;
    while ligne.Chars[idx2] <> ')' do inc(idx2);
    splitted := ligne.Substring(idx1, idx2-idx1).Split([',']);
    connexion.md1 := numMonde;
    for tmp in splitted do
    begin
      if tmp.StartsWith('@') then
      begin
        connexion.md2 := tmp.Substring(1).ToInteger;
        TN[connexion.md2] := True;
      end
      else
      begin
        connexion.md2 := tmp.ToInteger;
      end;
      AjouteConnexion(connexion, False);
    end;

    // element suivant
    idx1 := idx2 + 1;
    blocSuivant;

    if idx1 >= ligne.Length then
      Exit;

  end;
end;

procedure TCRParser.AddJoueurInconnu(Pseudo: String);
begin
  if JoueursInconnus.IndexOf(Pseudo) = -1 then
    JoueursInconnus.Add(Pseudo);
end;


// Importe les infos liées au propriétaire d'un monde
function TCRParser.ImportMondeProprietaire(NumLigne: Integer) : boolean;
var
  idx2 : UInt16;
  JoueurPseudo: String;
begin
  Result := True;
  with Data do
  begin
    // propriétaire / ancien proprio
    if ligne.Chars[idx1] = '"' then
    begin
      if idx1 >= ligne.Length then
        Exit;

      // proprio
      inc(idx1);

      idx2 := idx1;
      while (ligne.Chars[idx2] <> '"') and (ligne.Chars[idx2] <> '(')
          and (ligne.Chars[idx2] <> ':') do inc(idx2);
      if idx2 > idx1 then
      begin
        JoueurPseudo := ligne.Substring(idx1, idx2-idx1);
        M[numMonde].Proprio := ChercheNoJoueur(JoueurPseudo);
      end
      else // monde neutre
        M[numMonde].Proprio := 0;
      idx1 := idx2;

      // ancien proprio
      if ligne.Chars[idx1] = '(' then
      begin
        inc(idx1);
        idx2 := idx1 + 1;
        while ligne.Chars[idx2] <> ')' do inc(idx2);
        JoueurPseudo := ligne.Substring(idx1, idx2-idx1);
        ME[numMonde].AncienProprio := ChercheNoJoueur(JoueurPseudo);
        idx1 := idx2 + 1;
      end
      else
      begin
        // Par defaut on suppose que l'ancien proprio est le proprio actuel
        // On actualisera si besoins si le plag capture est présent
        ME[numMonde].AncienProprio := M[numMonde].Proprio;
      end;

      // compteurs de tour de possetion
      if ligne.Chars[idx1] = ':' then
      begin
        inc(idx1);
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);
        M[numMonde].Duree := ligne.Substring(idx1, idx2-idx1).ToInteger;

        if ligne.Chars[idx2] = ':' then
        begin
          idx1 := idx2 + 1;
          idx2 := idx1 + 1;
          while ligne.Chars[idx2].IsDigit do inc(idx2);
          M[numMonde].NbExploCM := Round((ligne.Substring(idx1, idx2-idx1).ToInteger - M[numMonde].Duree) / 3);
        end;
        idx1 := idx2;
      end;
      inc(idx1);

      // flag !PC
      if ligne.Chars[idx1] = '!' then
      begin
        Capture[numMonde] := true;
        // Capturé ce tour ci, donc le proprio actuel ne peut pas etre 'ancien propri
        // Actualisation de la valeur si besoins
        if ME[numMonde].AncienProprio = M[numMonde].Proprio then
          ME[numMonde].AncienProprio := 0;

        inc(idx1);
      end
      else
        Capture[numMonde] := false;

      if ligne.Chars[idx1] = 'P' then
      begin
        Pille[numMonde] := true;
        inc(idx1);
      end
      else
        Pille[numMonde] := false;

      if ligne.Chars[idx1] = 'C' then
      begin
        Cadeau[numMonde] := true;
        inc(idx1);
      end
      else
        Cadeau[numMonde] := false;
    end
    else
    begin
      // monde neutre
      Capture[numMonde] := false;
      Pille[numMonde] := false;
      Cadeau[numMonde] := false;
    end;

    // element suivant
    blocSuivant;

    if idx1 >= ligne.Length then
      Exit;

  end;
end;

// Importe les infos liées aux industries d'un monde
function TCRParser.ImportMondeIndustrie : boolean;
var
  idx2 : UInt16;
  aVI : boolean;
  n : integer;
begin
  Result := True;
  with Data do
  begin
    if idx1 >= ligne.Length then
      Exit;

    aVi := False;

    // Industrie et VI (optionnels) I= ou [I=]= ou []=
    if (ligne.Chars[idx1] = 'I') or ((ligne.Chars[idx1] = '[') and (ligne.Chars[idx1+1]='I'))
      or ((ligne.Chars[idx1] = '[') and (ligne.Chars[idx1+1]=']')) then
    begin
      if ligne.Chars[idx1] = '[' then
      begin
        aVI := true;
        inc(idx1);
      end;

      // traier les industries I=xx ou I=xx/yy
      if ligne.Chars[idx1] = 'I' then
      begin
        inc(idx1, 2);
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);
        n := ligne.Substring(idx1, idx2-idx1).ToInteger;

        if ligne.Chars[idx2] = '/' then
        begin
          idx1 := idx2 + 1;
          idx2 := idx1 + 1;
          while ligne.Chars[idx2].IsDigit do inc(idx2);
          M[numMonde].Ind := ligne.Substring(idx1, idx2-idx1).ToInteger;
          ME[numMonde].IndLibre := n;
        end
        else
        begin
          M[numMonde].Ind := n;
          ME[numMonde].IndLibre := M[numMonde].Ind;
        end;
        idx1 := idx2;
      end
      else
      begin
        M[numMonde].Ind := 0;
        ME[numMonde].IndLibre := 0;
      end;

      // traiter les VI + tir ou embuscade
      if aVI then
      begin
        if ligne.Chars[idx1] <> ']' then
        begin
          erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse des VIs');
          Result := False;
          exit;
        end;
        inc(idx1);
        // reste-t-il des VI ?
        if ligne.Chars[idx1] = '=' then
        begin
          inc(idx1);
          idx2 := idx1 + 1;
          while ligne.Chars[idx2].IsDigit do inc(idx2);
          M[numMonde].VI := ligne.Substring(idx1, idx2-idx1).ToInteger;
          idx1 := idx2;
        end;

        if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = '*') then
        begin
          // embuscade
          ME[numMonde].ActionVI := 4;
          ME[numMonde].CibleVI := 0;
          while ligne.Chars[idx1] <> ' ' do inc(idx1);
        end
        else if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = 'F') then
        begin
          // tir *F_xxx
          inc(idx1, 3);
          idx2 := idx1 + 1;
          while ligne.Chars[idx2].IsDigit do inc(idx2);

          ME[numMonde].ActionVI := 3;
          ME[numMonde].CibleVI := ligne.Substring(idx1, idx2-idx1).ToInteger;
          idx1 := idx2;
        end
        else
        begin
          ME[numMonde].ActionVI := 0;
          ME[numMonde].CibleVI := 0;
        end;
      end
      else
      begin
        M[numMonde].VI := 0;
        ME[numMonde].ActionVI := 0;
        ME[numMonde].CibleVI := 0;
      end;

      // element suivant
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

    end;
  end;
end;

// Importe les infos liées à la population d'un monde
function TCRParser.ImportMondePopulation : boolean;
var
  idx2 : UInt16;
  aVP : boolean;
  JoueurPseudo: String;
begin
  Result := True;
  with Data do
  begin
    if idx1 >= ligne.Length then
      Exit;


    aVp := False;

    // Pop et VP
    if ligne.Chars[idx1] = '[' then
    begin
      aVP := true;
      inc(idx1);
    end;

    if ligne.Chars[idx1] <> 'P' then
    begin
      erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse des populations');
      Result := False;
      exit;
    end;

    // P=xxx{-aaaP}{-bbbC}{-cccR}(yyy)
    // P=xxxC
    // P=xxxR

    inc(idx1, 2);
    idx2 := idx1 + 1;
    while ligne.Chars[idx2].IsDigit do inc(idx2);
    M[numMonde].Pop := ligne.Substring(idx1, idx2 - idx1).ToInteger; // Pop

    // gestion des convertis ou des robots
    if ligne.Chars[idx2] = 'C' then
    begin
      M[numMonde].Conv := M[numMonde].Pop;
      M[numMonde].Robot := 0;
      M[numMonde].ProprConv := M[numMonde].Proprio;
      inc(idx2);
    end
    else if ligne.Chars[idx2] = 'R' then
    begin
      M[numMonde].Robot := M[numMonde].Pop;
      M[numMonde].Pop := 0;
      M[numMonde].Conv := 0;
      inc(idx2);
    end
    else
    begin
      M[numMonde].Conv := 0;
      M[numMonde].Robot := 0;
      M[numMonde].ProprConv := 0;
    end;
    idx1 := idx2;

    // gestion pop mortes
    ME[numMonde].PopMoins := 0;
    ME[numMonde].ConvMoins := 0;
    ME[numMonde].RobMoins := 0;
    while ligne.Chars[idx1] = '{' do
    begin
      if ligne.Chars[idx1 + 1] <> '-' then
      begin
        erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse des populations mortes');
        continue
      end;
      inc(idx1, 2);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      case ligne.Chars[idx2] of
        'P':
          begin
            ME[numMonde].PopMoins := ligne.Substring(idx1, idx2 - idx1).ToInteger;
          end;
        'C':
          begin
            ME[numMonde].ConvMoins := ligne.Substring(idx1, idx2 - idx1).ToInteger;
          end;
        'R':
          begin
            ME[numMonde].RobMoins := ligne.Substring(idx1, idx2 - idx1).ToInteger;
          end
      else
        begin
          // erreur
        end;
      end;

      idx1 := idx2 + 2;
    end;

    if ligne.Chars[idx1] <> '(' then
    begin
      erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse de pop max');
      Result := False;
      exit;
    end;
    inc(idx1);
    idx2 := idx1 + 1;
    while ligne.Chars[idx2].IsDigit do inc(idx2);
    M[numMonde].MaxPop := ligne.Substring(idx1, idx2 - idx1).ToInteger; // Max Pop
    if ligne.Chars[idx2] <> ')' then
    begin
      erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse de pop max');
      Result := False;
      exit;
    end;
    idx1 := idx2 + 1;

    // gestion des convertions partielles P=xx(yy)/zzC ou P=xx(yy)/zzC:"mmmmm"
    if ligne.Chars[idx1] = '/' then
    begin
      inc(idx1);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      M[numMonde].Conv := ligne.Substring(idx1, idx2 - idx1).ToInteger;

      // proprietaire différent ?
      idx1 := idx2 + 1;
      if ligne.Chars[idx1] = ':'  then
      begin
        inc(idx1, 2); // :"
        idx2 := idx1 + 1;
        while ligne.Chars[idx2] <> '"' do inc(idx2);
        JoueurPseudo := ligne.Substring(idx1, idx2-idx1);
        M[numMonde].ProprConv := ChercheNoJoueur(JoueurPseudo);
        idx1 := idx2 + 1;
      end
      else
      begin // meme proprietaire
        M[numMonde].ProprConv := M[numMonde].Proprio;
      end;
    end;

    // traiter les VP + tir ou embuscade
    if aVP then
    begin
      if ligne.Chars[idx1] <> ']' then
      begin
        erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse des VPs');
        Result := false;
        exit;
      end;
      inc(idx1);

      // reste-t-il des VP ?
      if ligne.Chars[idx1] = '=' then
      begin
        inc(idx1);
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);
        M[numMonde].VP := ligne.Substring(idx1, idx2-idx1).ToInteger;
        idx1 := idx2;
      end;

      if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = '*') then
      begin
        // embuscade
        ME[numMonde].ActionVP := 4;
        ME[numMonde].CibleVP := 0;
      end
      else if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = 'F') then
      begin
        // tir *F_xxx
        inc(idx1, 3);
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);

        ME[numMonde].ActionVP := 3;
        ME[numMonde].CibleVP := ligne.Substring(idx1, idx2-idx1).ToInteger;
        idx1 := idx2;
      end
      else if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = 'N') then
      begin
        // tir sur non convertis
        ME[numMonde].ActionVP := 2;
        ME[numMonde].CibleVP := 0;
        inc(idx1, 2);
      end
      else if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = 'C') then
      begin
        // tir sur convertis
        ME[numMonde].ActionVP := 1;
        ME[numMonde].CibleVP := 0;
        inc(idx1, 2);
      end
      else
      begin
        ME[numMonde].ActionVP := 0;
        ME[numMonde].CibleVP := 0;
      end;
    end
    else
    begin
      M[numMonde].VP := 0;
      ME[numMonde].ActionVP := 0;
      ME[numMonde].CibleVP := 0;
    end;

    // element suivant
    blocSuivant;

    if idx1 >= ligne.Length then
      Exit;

  end;
end;

// Importe les infos liées à la production de MP d'un monde
function TCRParser.ImportMondeProduction : boolean;
var
  idx2 : UInt16;
begin
  Result := True;
  with Data do
  begin
    if idx1 >= ligne.Length then
      Exit;

    // MP=xx ou MP=xx(+yy) ou MP=(+yy)
    M[numMonde].MP := 0;
    M[numMonde].PlusMP := 0;
    if (ligne.Chars[idx1] = 'M') and (ligne.Chars[idx1+1] = 'P') then
    begin
      inc(idx1, 3);
      if not ligne.Chars[idx1].IsDigit then  // MP= equivalent a MP=0
      begin
        idx2 := idx1;
        M[numMonde].MP := 0;
      end
      else
      begin
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);
        M[numMonde].MP := ligne.Substring(idx1, idx2-idx1).ToInteger;
      end;

      if ligne.Chars[idx2] = '(' then // (+xxx)
      begin
        idx1 := idx2 + 2;
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);
        M[numMonde].PlusMP := ligne.Substring(idx1, idx2-idx1).ToInteger;

        inc(idx2);
      end;

      // element suivant
      idx1 := idx2 + 1;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

    end;

  end;
end;

// Importe les infos liées au pillage d'un monde
function TCRParser.ImportMondePillage : boolean;
var
  idx2 : UInt16;
begin
  Result := True;
  with Data do
  begin
    if idx1 >= ligne.Length then
      Exit;

    // Pillage Pi=xx ou Pi=xx/yy
    Pi[numMonde] := 0;
    RecupPi[numMonde] := 0;
    if (ligne.Chars[idx1] = 'P') and (ligne.Chars[idx1+1] = 'i') then
    begin
      inc(idx1, 3);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      Pi[numMonde] := ligne.Substring(idx1, idx2-idx1).ToInteger;

      if ligne.Chars[idx2] = '/' then
      begin
        idx1 := idx2 + 1;
        idx2 := idx1 + 1;
        while ligne.Chars[idx2].IsDigit do inc(idx2);
        RecupPi[numMonde] := ligne.Substring(idx1, idx2-idx1).ToInteger;
      end
      else
      begin
        RecupPi[numMonde] := 0;
      end;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

    end;

  end;
end;

// Importe les infos liées aux produits de consommation pour un monde
function TCRParser.ImportMondePC : boolean;
var
  idx2 : UInt16;
begin
  if idx1 >= ligne.Length then
    Exit;

  Result := True;
  with Data do
  begin
    // PC=xxx
    ME[numMonde].PC := 0;
    if (ligne.Chars[idx1] = 'P') and (ligne.Chars[idx1 + 1] = 'C') then
    begin
      inc(idx1, 3);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      ME[numMonde].PC := ligne.Substring(idx1, idx2-idx1).ToInteger;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

    end;
  end;
end;

// Importe les infos liées à l'exploration d'un monde
function TCRParser.ImportMondeExplo : boolean;
var
  idx2 : UInt16;
begin
  Result := True;
  with Data do
  begin
    if idx1 >= ligne.Length then
      Exit;

    // Exploration E=xx/yy
    M[numMonde].NbExplo := 0;
    if ligne.Chars[idx1] = 'E' then
    begin
      inc(idx1, 2);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      M[numMonde].NbExplo := ligne.Substring(idx1, idx2-idx1).ToInteger;

      // /yyy (nb max explo, a ne pas stocker)
      idx1 := idx2 + 1;
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      // ligne.Substring(idx1, idx2-idx1).ToInteger;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

    end;

    // Point d'explo pour Md
    M[numMonde].PotExplo := 0;
    if (ligne.Chars[idx1] = 'P') and (ligne.Chars[idx1+1] = 'E') then
    begin
      inc(idx1, 3);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      M[numMonde].PotExplo := ligne.Substring(idx1, idx2-idx1).ToInteger;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;
    end;

    // gestion de +Pop=xx,y%
    M[numMonde].NbExploPop := 0;
    if (ligne.Chars[idx1] = '+') and (ligne.Chars[idx1 + 1] = 'P') then
    begin
      inc(idx1, 5);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2] <> '%' do inc(idx2);
      M[numMonde].NbExploPop := Round((ligne.Substring(idx1, idx2-idx1).ToDouble - 10.0)/2.5);

      idx1 := idx2 + 1;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;
    end;
  end;
end;

// Importe les coordonnées d'un monde
function TCRParser.ImportMondeCoordonnees : boolean;
var
  idx2 : Integer;
  splitted : TArray<String>;
begin
  if idx1 >= ligne.Length then
    Exit;

  Result := True;
  with Data do
  begin
    // Coordonnées
    if ligne.Chars[idx1] = '[' then
    begin
      inc(idx1);
      idx2 := ligne.IndexOf(']', idx1);
      if idx2 = -1 then
      begin
        erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + inttostr(numMonde) + ' echec d''analyse coordonnées');
        Result := False;
        exit;
      end;
      splitted := ligne.Substring(idx1, idx2-idx1).Split([',']);
      M[numMonde].X := splitted[0].ToInteger;
      M[numMonde].Y := splitted[1].ToInteger;
      AVuCoord[numMonde, NoJou] := True;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;
    end;

  end;
end;

// Mise a jour des connexions et mondes connecté
procedure TCRParser.MAJMondesConnectes;
var
  i, j : integer;
begin
  with Data do
  begin
    // Gestion des connexions et coordonnées des mondes connectés
    if not (AVuConnect[numMonde, NoJou]) then
    begin
      for i := 1 to 8 do
        if M[numMonde].Connect[i] > 0 then
        begin
          AVuCoord[numMonde, NoJou] := True;
          AVuCoord[M[numMonde].Connect[i], NoJou] := True;
        end;

      // On indique que le joueur voit les connexions
      AVuConnect[numMonde, NoJou] := True;
    end;
  end;
end;

// Importe un monde a partir d'un extrait
function TCRParser.ImportMonde(NumLigne: Integer): boolean;
var
  estBombe : boolean;
  idx2 : UInt16;
begin
  numMonde := 0;
  numFlotte := 0;

  Result := True;
  try
    with Data do
    begin
      idx1 :=1;
      if ligne.StartsWith('Md') then inc(idx1);

      // monde bombé ?
      estBombe := (ligne.Chars[idx1] = '#');

      // obtention du numéro du monde
      inc(idx1);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      numMonde := ligne.Substring(idx1, idx2-idx1).ToInteger;
      ImportMondeRaz();

      if Self.nomMonde <> '' then
      begin
        NomMonde[numMonde] := Self.nomMonde;
        Self.nomMonde := '';
      end;

      // Bombé -> Data
      Bombe[numMonde] := estBombe;

      // On indique que le monde est connu du joueur (même si ce n'est pas le cas)
      Connu[numMonde, NoJou] := True;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

      // Trou noir ?
      if (ligne.Chars[idx1] = '*') and ligne.Contains('Trou noir') then
      begin
        TN[nummonde] := True;
        Result := False;
        exit;
      end;
      TN[nummonde] := False;

      if not ImportMondeConnexion then exit;
      if not ImportMondeProprietaire(NumLigne) then exit;
      if not ImportMondeIndustrie then exit;
      if not ImportMondePopulation then exit;
      if not ImportMondeProduction then exit;
      if not ImportMondePillage then exit;
      if not ImportMondePC then exit;
      if not ImportMondeExplo then exit;
      if not ImportMondeCoordonnees then exit;
      MAJMondesConnectes;
    end;
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + IntToStr(nummonde) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : M ' + IntToStr(nummonde) + ' erreur d''analyse.');
  end;
end;

// RaZ de la flotte
procedure TCRParser.ImportFlotteRaz;
begin
  with Data do
  begin
    TransporteBombe[numFlotte] := false;
    F[numFlotte].Localisation := numMonde;
    F[numFlotte].Proprio := 0;
    FE[numFlotte].AncienProprio := 0;
    Paix[numFlotte] := false;
    Piratee[numFlotte] := false;
    Capturee[numFlotte] := false;
    DPC[numFlotte] := false;
    FlotteExplo[numFlotte] := false;
    Offerte[numFlotte] := false;
    F[numFlotte].MP := 0;
    F[numFlotte].N := 0;
    F[numFlotte].C := 0;
    F[numFlotte].R := 0;
    F[numFlotte].NbVC := 0;
    F[numFlotte].NbVT := 0;
    FlotteEmbuscade[numFlotte] := false;
    FE[numFlotte].OrdrExcl := 0;
    FE[numFlotte].Cible := 0;

  end;
end;

function TCRParser.ImportFlotteProprietaire : boolean;
var
  idx2 : UInt16;
  enPaix : boolean;
  JoueurPseudo: String;
begin
  Result := true;
  enPaix := false;

  with Data do
  begin
    // TODO : Ajouter un test de fin de ligne.
    // Si c'est le cas, flotte neutre.

    // Flotte en paix ?
    if ligne.Chars[idx1] = '(' then
    begin
      Paix[numFlotte] := True;
      enPaix := True;
      inc(idx1);
    end;

    // propriétaire / ancien proprio
    if ligne.Chars[idx1] = '"' then
    begin
      // proprio
      inc(idx1);

      idx2 := idx1;
      while (ligne.Chars[idx2] <> '"') and (ligne.Chars[idx2] <> '(')
          do inc(idx2);
      if idx2 > idx1 then
      begin
        JoueurPseudo := ligne.Substring(idx1, idx2-idx1);
        F[numFlotte].Proprio := ChercheNoJoueur(JoueurPseudo);
      end;
      idx1 := idx2;

      if ligne.Chars[idx1] = '(' then
      begin
        // ancien proprio
        inc(idx1);
        idx2 := idx1 + 1;
        while ligne.Chars[idx2] <> ')' do inc(idx2);
        JoueurPseudo := ligne.Substring(idx1, idx2-idx1);
        FE[numFlotte].AncienProprio := ChercheNoJoueur(JoueurPseudo);
        idx1 := idx2 + 1;
      end
      else
        // Par defaut on suppose que l'ancien proprio est le proprio actuel
        // On actualisera si besoins si le plag capture est présent
        FE[numFlotte].AncienProprio := F[numFlotte].Proprio;
      inc(idx1);

      // verif fermeture enPaix
      if enPaix then
      begin
        if ligne.Chars[idx1] <> ')' then
        begin
          erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + inttostr(numFlotte) + ' echec d''analyse flotte en paix');
          Result := False;
          exit;
        end;
        inc(idx1);
      end;

      // Flag !PCDE (ordre obtenu dans la fonction d'impression du monde)
      if ligne.Chars[idx1] = '!' then
      begin
        Capturee[numFlotte] := true;
        // Capturé ce tour ci, donc le proprio actuel ne peut pas etre l'ancien propri
        // Actualisation de la valeur si besoins
        if FE[numFlotte].AncienProprio = F[numFlotte].Proprio then
          FE[numFlotte].AncienProprio := 0;
          
        inc(idx1);
      end;

      if ligne.Chars[idx1] = 'P' then
      begin
        Piratee[numFlotte] := true;
        inc(idx1);
      end;

      if ligne.Chars[idx1] = 'C' then
      begin
        Offerte[numFlotte] := true;
        inc(idx1);
      end;

      if ligne.Chars[idx1] = 'D' then
      begin
        DPC[numFlotte] := true;
        inc(idx1);
      end;

      if ligne.Chars[idx1] = 'E' then
      begin
        FlotteExplo[numFlotte] := true;
        inc(idx1);
      end;
    end;

    // element suivant
    blocSuivant;

    if idx1 >= ligne.Length then
      Exit;
  end;
end;

function TCRParser.ImportFlotteCargaison : boolean;
var
  idx2 : UInt16;
  splitted : TArray<String>;
  tmp : String;
begin
  Result := True;

  try
    with Data do
    begin
      inc(idx1);

      idx2 := idx1;
      while ligne.Chars[idx2] <> ']' do inc(idx2);

      splitted := ligne.Substring(idx1, idx2-idx1).Split([',']);
      for tmp in splitted do
      begin
        if tmp = '?' then
        begin
          // cargaison inconnue, rien a faire
        end
        else if tmp.EndsWith('N') then
        begin
          F[numFlotte].N := tmp.Substring(0, tmp.Length-1).ToInteger;
        end
        else if tmp.EndsWith('C') then
        begin
          F[numFlotte].C := tmp.Substring(0, tmp.Length-1).ToInteger;
        end
        else if tmp.EndsWith('R') then
        begin
          F[numFlotte].R := tmp.Substring(0, tmp.Length-1).ToInteger;
        end
        else
        begin
          F[numFlotte].MP := tmp.ToInteger;      
        end;
      end;
      idx1 := idx2;
    end
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur d''analyse.');
  end;
end;

function TCRParser.ImportFlotteTir : boolean;
var
  idx2 : UInt16;
begin
  Result := True;

  try
    with Data do
    begin
      // Analyse des actions de tir
      if (ligne.Chars[idx1] = '*') and (ligne.Chars[idx1+1] = '*') then
      begin
        FlotteEmbuscade[numFlotte] := true;
        inc(idx1, 2);
      end
      else if ligne.Chars[idx1] = '*' then
      begin
        inc(idx1);
        case ligne.Chars[idx1] of
          'F': 
            begin
              FE[numFlotte].OrdrExcl := 1;
              inc(idx1, 2);
              idx2 := idx1 + 1;
              while ligne.Chars[idx2].IsDigit do inc(idx2);
              FE[numFlotte].Cible := ligne.Substring(idx1, idx2-idx1).ToInteger;
              idx1 := idx2;
            end;
          'M':
            begin 
              FE[numFlotte].OrdrExcl := 2;
              inc(idx1);
            end;
          'P': 
            begin
              FE[numFlotte].OrdrExcl := 3;
              inc(idx1);
            end;
          'I': 
            begin
              FE[numFlotte].OrdrExcl := 4;
              inc(idx1);
            end;
          'R': 
            begin
              FE[numFlotte].OrdrExcl := 5;
              inc(idx1);
            end;
          'B': 
            begin
              FE[numFlotte].OrdrExcl := 6;
              inc(idx1);
            end;
        end;
      
      end;
    end
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur d''analyse.');
  end;

  // element suivant
  blocSuivant;

  if idx1 >= ligne.Length then
    Exit;

end;

procedure TCRParser.FinaliseFlottesTraces;

  procedure CheminVersFragments(fl: Integer);
  var
    Du, Monde, Vers, ch: Integer;
  begin
    for ch := 1 to High(Data.FE[fl].Chemin) do
    begin
      Du := 0;
      Monde := Data.FE[fl].Chemin[ch];
      Vers := 0;

      if ch > 1 then
        Du := Data.FE[fl].Chemin[ch - 1];

      if ch < High(Data.FE[fl].Chemin) then
        Vers := Data.FE[fl].Chemin[ch + 1];

      if (Monde = 0) then
        Break;

      if (Monde <> -1) and (Vers <> -1) then
      begin
        AjouteChemin(fl, Du, Monde, Vers)
      end;
    end;
  end;

  procedure FragmentsVersChemin(fl: Integer);
  var
    ch: Integer;
    fr: TFragmentChemin;
  begin
    Fillchar(Data.FE[fl].Chemin, 0, sizeof(Data.FE[fl].Chemin));
    if not Assigned(Chemins[fl - 1]) then
      Exit;

    ch := 1;

    for fr in Chemins[fl - 1] do
    begin
      // Il y a un trou dans la trace, il faut faire quelque chose
      if (ch <= High(Data.FE[fl].Chemin)) and (Data.FE[fl].Chemin[ch] <> fr.Monde) and (fr.Du <> 0) then
      begin
        if ((fr.Du = -1) or (fr.Du = Data.FE[fl].Chemin[ch])) then
        begin
          // Trou dist = 1 => on reconstitue
          if Data.FE[fl].Chemin[ch] >= 0 then
            Inc(ch);

          Data.FE[fl].Chemin[ch] := fr.Du;
          Inc(ch);
        end
        else if Data.FE[fl].Chemin[ch] <> 0 then // Trou dist > 1
        begin
          Inc(ch);
          Data.FE[fl].Chemin[ch] := fr.Du;
          Inc(ch);
        end;

      end;

      Data.FE[fl].Chemin[ch] := fr.Monde;
      Inc(ch);

      if ch <= High(Data.FE[fl].Chemin) then
        Data.FE[fl].Chemin[ch] := fr.Vers;
    end;
  end;

var
  fl: Integer;
begin
  for fl := 1 to Data.NbFlotte do
  begin
    CheminVersFragments(fl);

    // On utilise la fonction de tri pour classer les fragments dans l'ordre du chemin
    if Assigned(Chemins[fl - 1]) then
    begin
      // On appelle plusieurs fois la fonction de tri car ce n'est pas un vrai
      // tri et il faut itérer au moins 3 fois pour éviter certaines erreurs
      Chemins[fl - 1].Sort(ComparateurChemins);
      Chemins[fl - 1].Sort(ComparateurChemins);
      Chemins[fl - 1].Sort(ComparateurChemins);
      Chemins[fl - 1].Sort(ComparateurChemins);
      Chemins[fl - 1].Sort(ComparateurChemins);
    end;

    FragmentsVersChemin(fl);
  end;
end;

procedure TCRParser.AjouteChemin(fl, Du, Mde, Vers: Integer);
var
  FragmentChemin: TFragmentChemin;
begin
  if not Assigned(Chemins[fl - 1]) then
    Chemins[fl - 1] := TChemin.Create;

  FragmentChemin.Du := Du;
  FragmentChemin.Monde := Mde;
  FragmentChemin.Vers := Vers;
  Chemins[fl - 1].Add(FragmentChemin);

  Exit;
end;


procedure TCRParser.ImportFlotteTrace(ligne: String);
var
  match: TMatch;
  pseudo: String;
  MondeDu, MondeVers: Integer;
  MondeDuStr: String;

begin
  match := regexTrace.Match(ligne);

  if (match.Success) then
  begin
    numFlotte := StrToInt(match.Groups['fl'].Value);
    pseudo := match.Groups['pseudo'].Value;
    MondeDuStr := match.Groups['du'].Value;
    if MondeDuStr = '?' then
      MondeDuStr := '-1';

    MondeDu := StrToIntDef(MondeDuStr, 0);

    MondeVers := StrToInt(match.Groups['vers'].Value);

    Data.FE[numFlotte].AncienProprio := ChercheNoJoueur(pseudo);

    AjouteChemin(numFlotte, MondeDu, numMonde, MondeVers);
  end;
end;

function TCRParser.ImportFlotteVaisseau : boolean;
var
  idx2 : UInt16;
begin
  if idx1 >= ligne.Length then
    Exit;

  Result := True;

  try
    with data do
    begin
      if ligne.Chars[idx1] = '[' then
      begin
        ImportFlotteCargaison();

        if ligne.Chars[idx1] <> ']' then
        begin
          erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + inttostr(numFlotte) + ' echec d''analyse des vaisseaux');
          Result := False;
          exit;
        end;
        inc(idx1);

        // Analyse des vaisseaux
        if ligne.Chars[idx1] = '=' then
        begin
          inc(idx1);
          idx2 := idx1 + 1;
          while ligne.Chars[idx2].IsDigit do inc(idx2);
          if ligne.Chars[idx2] = '?' then
          begin
            // D'après analyse d'un NBT, si le type des vaisseaux est inconnu
            // la valeur est stockée dans le champ VCs
            F[numFlotte].NbVC := ligne.Substring(idx1, idx2-idx1).ToInteger;
            inc(idx2);
          end
          else if ligne.Chars[idx2] = 'T' then
          begin
            F[numFlotte].NbVT := ligne.Substring(idx1, idx2-idx1).ToInteger;
            inc(idx2);
          end
          else
          begin
            F[numFlotte].NbVC := ligne.Substring(idx1, idx2-idx1).ToInteger;
          end;
          idx1 := idx2;

          if ligne.Chars[idx1] = '+' then
          begin
            inc(idx1);
            idx2 := idx1 + 1;
            while ligne.Chars[idx2].IsDigit do inc(idx2);
            F[numFlotte].NbVC := ligne.Substring(idx1, idx2-idx1).ToInteger;
            idx1 := idx2;
          end;
        end;
      end;

    ImportFlotteTir();
  end
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur d''analyse.');
  end;

  // element suivant
  blocSuivant;

  if idx1 >= ligne.Length then
    Exit;

end;

function TCRParser.ImportFlotteDeplacement : boolean;
var
  idx2, j : UInt16;
begin
  if idx1 >= ligne.Length then
    Exit;

  Result := True;

  // Note, on est dans le monde d'arret de la flotte
  // donc logiquement pas de ' du M_'
  try
    with Data do
    begin
      if ligne.Substring(idx1).StartsWith('du M_') then
      begin
        FE[numFlotte].OrdrExcl := 7;
        // recherche de l'index de dernière position du chemin
        j := 1;
        while (j < MAX_CHEMIN) AND (FE[numFlotte].Chemin[j] <> 0) do inc(j);

        // Déterminer le numéro du monde
        inc(idx1, 5);
        if ligne.Chars[idx1] = '?' then
          AjouteChemin(numFlotte, -1, numMonde, 0)
        else
        begin
          idx2 := idx1 + 1;
          while ligne.Chars[idx2].IsDigit do inc(idx2);
          AjouteChemin(numFlotte, ligne.Substring(idx1, idx2-idx1).ToInteger, numMonde, 0);
        end;
      end;
    end;
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur d''analyse.');
  end;

end;

// Importe les informations d'une flotte
function TCRParser.ImportFlotte : boolean;
var
  idx2 : UInt16;
  aBombe : boolean;

begin
  Result := True;
  numFlotte := 0;

  try
    with Data do
    begin
      idx1 :=1;

      // transport de la bombe ?
      aBombe := ligne.Chars[idx1] = '#';

      // obtention du numéro du monde
      inc(idx1);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      numFlotte := ligne.Substring(idx1, idx2-idx1).ToInteger;
      ImportFlotteRaz();

      TransporteBombe[numFlotte] := aBombe;
      F[numFlotte].Localisation := numMonde;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

      if not ImportFlotteProprietaire() then exit;
      if not ImportFlotteVaisseau() then exit;
      if not ImportFlotteDeplacement() then exit;
    end;
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : F ' + IntToStr(numFlotte) + ' erreur d''analyse.');
  end;

end;

// Importe les informations d'un trésor
function TCRParser.ImportTresor : boolean;
var
  idx2 : UInt16;
begin
  if idx1 >= ligne.Length then
    Exit;

  Result := True;
  numTresor := 0;

  if numMonde = 0 then
  begin
    Result := False;
    exit;
  end;

  try
    with Data do
    begin
      idx1 := 2;
      idx2 := idx1 + 1;
      while ligne.Chars[idx2].IsDigit do inc(idx2);
      numTresor := ligne.Substring(idx1, idx2-idx1).ToInteger;

      // Tresor sur un monde ou une flotte ?
      if numFlotte > 0 then
      begin
        T[numTresor].Statut := 1;
        T[numTresor].Localisation := numFlotte;
        T[numTresor].Proprio := F[numFlotte].Proprio;
      end
      else
      begin
        T[numTresor].Statut := 0;
        T[numTresor].Localisation := numMonde;
        T[numTresor].Proprio := M[numMonde].Proprio;
      end;

      // element suivant
      idx1 := idx2;
      blocSuivant;

      if idx1 >= ligne.Length then
        Exit;

      // Lecture du nom du trésor
      if ligne.Chars[idx1] <> '<' then
      begin
        erreurs.Add('Ligne ' + IntToStr(numligne) + ' : T ' + IntToStr(numTresor) + ' erreur d''analyse debut du nom.');
        Result := False;
        exit;
      end;
      inc(idx1);
      idx2 := idx1 + 1;
      while ligne.Chars[idx2] <> '>' do inc(idx2);

      // vérification du nom du trésor
      if TresNom(numTresor).ToLower <> ligne.Substring(idx1, idx2-idx1).ToLower then
      begin
        erreurs.Add('Ligne ' + IntToStr(numligne) + ' : T ' + IntToStr(numTresor) + ' incohérence de nom : '
        + 'trouvé : "' + ligne.Substring(idx1, idx2-idx1) +'" - attendu : "' + TresNom(numTresor) + '"');
        Result := False;
        exit;
      end;
      idx1 := idx2;

      //
      if ligne.Chars[idx1] <> '>' then
      begin
        erreurs.Add('Ligne ' + IntToStr(numligne) + ' : T ' + IntToStr(numTresor) + ' erreur d''analyse fin du nom.');
        Result := False;
        exit;
      end;

    end;
  except
    on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : T ' + IntToStr(numTresor) + ' erreur de conversion numérique.' + Econv.Message);
    on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : T ' + IntToStr(numTresor) + ' erreur d''analyse.');
  end;

end;

// Importe les informations d'un joueur
function TCRParser.ImportJoueur(ligne: String) : boolean;
var
  numJoueur : UInt16;
  Match: TMatch;
begin
  Result := True;

  for Match in regexJoueur.Matches(ligne) do
  begin
    try
      numJoueur := StrToInt(Match.Groups['numjou'].Value);
      Data.Jou[numJoueur].Pseudo := Match.Groups['pseudo'].Value;
      Data.ConnuDeNom[Data.NoJou, numJoueur] := True;
    except
      on Econv : EConvertError do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : J ' + Match.Value + ' erreur de conversion numérique.' + Econv.Message);
      on Excpt : Exception do erreurs.Add('Ligne ' + IntToStr(numligne) + ' : J ' + Match.Value + ' erreur d''analyse.');
    end;
  end;
end;

// Retourne le numéro d'un joueur à partir de son nom
// Le résultat est 0 si le joueur n'est pas trouvé
function TCRParser.ChercheNoJoueur(Pseudo : String) : uint16;
var
  idx : integer;
  numero : uint16;
begin
  numero := 0;
  with Data do
  begin
    for idx := 1 to NbJou do
    begin
      if Jou[idx].Pseudo = Pseudo then
      begin
        numero := idx;
        break;
      end;
    end;
  end;

  if numero = 0 then
    AddJoueurInconnu(Pseudo);

  Result := numero;
end;

procedure TCRParser.FreeChemins;
var
  fl: Integer;
begin
  for fl := Low(Chemins) to High(Chemins) do
    if Assigned(Chemins[fl]) then
      FreeAndNil(Chemins[fl]);
end;

procedure TCRParser.Clear;
begin
  Erreurs.Clear;
  JoueursInconnus.Clear;
  FreeChemins;
end;

function TCRParser.UpdateJoueurInconnu(Pseudo: String; NumJou: Integer): Boolean;
var
  Index: Integer;
begin
  Result := False;
  if Data.Jou[NumJou].Pseudo <> '' then
  begin
    ShowMessage('Erreur : le numéro de joueur ' + IntToStr(NumJou)
      + ' est déjà affecté à ' + Data.Jou[NumJou].Pseudo);
    Exit;
  end;

  Data.jou[NumJou].Pseudo := Pseudo;
  Data.ConnuDeNom[Data.NoJou, Numjou] := True;

  Index := JoueursInconnus.IndexOf(Pseudo);
  if Index >= 0 then
    JoueursInconnus.Delete(Index);

  Result := True;
end;

// Fonction d'import d'extrait de CR
procedure TCRParser.ImportExtrait(Data: TNebData; extrait : TStrings);
var
  tl, tl_old : TypeLigne;
  finCR: Boolean;
  fl: integer;

begin
  Self.Data := Data;

  SetLength(Chemins, Data.NbFlotte);
  for fl := 0 to Data.NbFlotte - 1 do
    Chemins[fl] := nil;

  numLigne := 0;
  nomMonde := '';

  tl := TL_VIDE;
  finCR := False;
  Repeat
    tl_old := tl;
    tl := TL_VIDE;

    if regexJoueur.IsMatch(extrait[numLigne]) then
      ImportJoueur(extrait[numLigne])

    else if extrait[numLigne].StartsWith('Ordres du joueur') then
      finCR := True

    else if regexNomMonde.IsMatch(extrait[numLigne]) then tl := TL_NOM_MONDE

    else if regexMonde.IsMatch(extrait[numLigne]) then tl := TL_MONDE

    else if regexFlotte.IsMatch(extrait[numLigne]) then tl := TL_FLOTTE

    else if regexTresor.IsMatch(extrait[numLigne]) then
    begin
      if (tl_old = TL_MONDE) or (tl_old = TL_MONDE_TRESOR) then
        tl := TL_MONDE_TRESOR
      else if (tl_old = TL_FLOTTE) or (tl_old = TL_FLOTTE_TRESOR) then
        tl := TL_FLOTTE_TRESOR;

      if (tl <> TL_MONDE_TRESOR) and (tl <> TL_FLOTTE_TRESOR) then
        ShowMessage('Erreur ligne ' + IntToStr(numLigne) + ' impossible de savoir à qui appartient le trésor' + #10#13 + extrait[numLigne]);
    end

    else if regexTrace.IsMatch(extrait[numLigne]) then tl := TL_TRACE

    else if regexJoueur.IsMatch(extrait[numLigne]) then tl := TL_JOUEUR

    else if (extrait[numLigne] <> '') then tl := TL_NON_VIDE

    else tl := TL_VIDE;

    // Actions liées à l'analyse de la ligne en cours

    // Monde sur plusieurs lignes : on fusionne avec la ligne précédente
    if (tl_old = TL_MONDE) and (tl = TL_NON_VIDE) then
    begin
      tl := TL_MONDE;
      extrait[numLigne] := regexEspacesDebutLigne.Replace(extrait[numLigne], ' ');
      extrait[numLigne-1] := extrait[numLigne-1] + extrait[numLigne];
      extrait.Delete(numLigne);
      Dec(numLigne);
    end;

    if (tl_old = TL_MONDE) and ((tl <> TL_MONDE) or (numLigne >= extrait.Count)) then
    begin
      ligne := extrait[numLigne-1];
      ImportMonde(numLigne-1);
    end;

    ligne := extrait[numLigne].Trim;

    if tl = TL_NOM_MONDE then
      ImportNomMonde;

    if tl = TL_FLOTTE then
      ImportFlotte;

    if (tl = TL_MONDE_TRESOR) or (tl = TL_FLOTTE_TRESOR) then
      ImportTresor;

    if tl = TL_TRACE then
      ImportFlotteTrace(extrait[numLigne]);

    Inc(numLigne);
  Until (numLigne >= extrait.Count) or finCR;

  FinaliseFlottesTraces;

end;

//
// {TExtraitCRDialog}
//

procedure TExtraitCRDialog.PoursuivreButtonClick(Sender: TObject);
begin
  if Parser.UpdateJoueurInconnu(
    NomJoueurInconnuEdit.Text, StrToInt(NumJouEdit.Text)
    ) then
    begin
      ImportExtract(Memo.Lines);
      CheckJoueursInconnus;
    end;


end;

procedure TExtraitCRDialog.CancelBtnClick(Sender: TObject);
begin
  Erreurs.Clear;
  JoueursInconnus.Clear;
  Close;
end;

procedure TExtraitCRDialog.FormActivate(Sender: TObject);
begin
  Parser.Clear;
  Memo.Lines.Clear;
  OKBtn.Enabled := True;
  CardPanel.ActiveCard := CardCR;
  Memo.SetFocus;
end;

procedure TExtraitCRDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := (JoueursInconnus.Count = 0);
end;

procedure TExtraitCRDialog.FormCreate(Sender: TObject);
begin
  Parser := TCRParser.Create(Data);
end;

procedure TExtraitCRDialog.FormDestroy(Sender: TObject);
begin
  Parser.Free;
end;

function TExtraitCRDialog.GetErreurs: TStringList;
begin
  Result := Parser.Erreurs;
end;

function TExtraitCRDialog.GetJoueursInconnus: TStringList;
begin
  Result := Parser.JoueursInconnus;
end;

function TExtraitCRDialog.GetNextJouInconnus(NumJou: Integer): Integer;
var j: Integer;
begin
  Result := -1;
  for j := NumJou to Data.NbJou do
    if (j <> Data.NoJou) and not Data.ConnuDeNom[Data.NoJou, j] then
    begin
      Result := j;
      Break;
    end;

end;

function TExtraitCRDialog.GetPrevJouInconnus(NumJou: Integer): Integer;
var j: Integer;
begin
  Result := -1;
  for j := NumJou downto 1 do
    if (j <> Data.NoJou) and not Data.ConnuDeNom[Data.NoJou, j] then
    begin
      Result := j;
      Break;
    end;

end;

function TExtraitCRDialog.GetPremierJouInconnus: Integer;
begin
  Result := GetNextJouInconnus(1);
end;

function TExtraitCRDialog.GetDernierJouInconnus: Integer;
begin
  Result := GetPrevJouInconnus(Data.NbJou);
end;

procedure TExtraitCRDialog.SaisieNumeroJoueurInconnu;

var
  NumJou: Integer;
begin
  OKBtn.Enabled := False;
  CardPanel.ActiveCard := CardNumJou;
  NomJoueurInconnuEdit.Text := JoueursInconnus[0];

  NumJou := GetPremierJouInconnus;

  NumJouEdit.Text := IntToStr(NumJou);
  NumJouUpDown.Min := 1;
  NumJouUpDown.Max := Data.NbJou;
end;

procedure TExtraitCRDialog.CheckJoueursInconnus;
begin
  if Parser.JoueursInconnus.Count > 0 then
    SaisieNumeroJoueurInconnu
  else
    ModalResult := mrOk;
end;

// Fonction d'import d'extrait de CR
procedure TExtraitCRDialog.ImportExtract(Extrait : TStrings);
begin
  Parser.ImportExtrait(Data, Extrait);
  CheckJoueursInconnus;
end;

procedure TExtraitCRDialog.MemoChange(Sender: TObject);
begin
  Memo.Lines.Text := AdjustLineBreaks(Memo.Lines.Text);
end;

procedure TExtraitCRDialog.NumJouEditChange(Sender: TObject);
var
  tmpNoJou: Integer;
begin
  try
    tmpNoJou := StrToInt(NumJouEdit.Text);
  except
    on Econv : EConvertError do tmpNoJou := 0;
  end;

  if (tmpNoJou < 1) or (tmpNoJou > Data.NbJou) then
  begin
      ShowMessage('Erreur : numéro de joueur incorrect');
      if (tmpNoJou > Data.NbJou) then
        tmpNoJou := GetDernierJouInconnus
      else
        tmpNoJou := GetPremierJouInconnus;
      NumJouEdit.Text := IntToStr(tmpNoJou);
  end;
end;

procedure TExtraitCRDialog.NumJouUpDownChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Integer; Direction: TUpDownDirection);
var
  tmpNoJou: Integer;
begin
  AllowChange := True;

  if Data.ConnuDeNom[Data.NoJou, NewValue] then
  begin
    AllowChange := False;
    case Direction of
      updUp: tmpNoJou := GetNextJouInconnus(NewValue);
      updDown: tmpNoJou := GetPrevJouInconnus(NewValue);
    end;
    if tmpNoJou <> -1 then
      NumJouUpDown.Position := tmpNoJou;
  end;
end;

procedure TExtraitCRDialog.OKBtnClick(Sender: TObject);
begin
  ImportExtract(Memo.Lines);
end;

procedure TExtraitCRDialog.Run(Data: TNebData);
begin
  Parser.Clear;
  Self.Data := Data;
  ExtraitCRDialog.Memo.Text := '';
  ShowModal;
end;

{ TFragmentChemin }

// Pour permettre le tri de la liste des fragments de chemin
function TFragmentChemin.Compare(AutreFragment: TFragmentChemin): Integer;
begin
  Result := 0;

  if (AutreFragment.Monde = Monde) and (AutreFragment.Du = Du) and (AutreFragment.Vers = vers) then
    Exit;

  // Départ de la trace
  if Du = 0 then
  begin
    Result := -1000;
    Exit;
  end;

  if AutreFragment.Du = 0 then
  begin
    Result := 1000;
    Exit;
  end;

  // Arrivée de la trace
  if Vers = 0 then
  begin
    Result := 1000;
    Exit;
  end;

  if AutreFragment.Vers = 0 then
  begin
    Result := -1000;
    Exit;
  end;

  // 1 monde d'écart
  if Monde = AutreFragment.Vers then
  begin
    Result := 1;
    Exit;
  end;

  if Vers = AutreFragment.Monde then
  begin
    Result := -1;
    Exit;
  end;

  // 2 mondes d'écart
  if Vers = AutreFragment.Du then
  begin
    Result := -2;
    Exit;
  end;

  if AutreFragment.Vers = Du then
  begin
    Result := 2;
    Exit;
  end;

end;

{ TChemin }

// Ajout évitant les doublons
// TODO: Cette fonction ne sait pas gérer le cas d'une flotte passant plusieurs fois au-dessus d'un monde
function TChemin.Add(Fragment: TFragmentChemin): Integer;
var
  i: Integer;
begin
  // On regarde si on n'a pas déjà un fragment pour le monde
  for i := 0 to Self.Count - 1 do
    if (Items[i].Monde = Fragment.Monde) and (Items[i].Vers = Fragment.Vers) then
    begin
      // On garde le fragment le plus complet (par rapport à l'info du monde d'origine)
      if (Items[i].Du = -1) and (Fragment.Du > 0) then
        Items[i] := Fragment;
      Exit;
    end;

  Result := inherited Add(Fragment);
end;

function TChemin.IsEqual(Chemin: TChemin): Boolean;
var
  i: Integer;
begin
  Result := False;

  if Count <> Chemin.Count then
  begin
    Exit;
  end;

  for i := 0 to Count - 1 do
  begin
    if Items[i].Compare(Chemin[i]) <> 0 then
      Exit;
  end;

  Result := True;
end;

end.

