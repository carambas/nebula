# Sources Nébula

Il s'agit des sources Delphi (Rad studio) pour construire les applications Nebula4.exe, Nebutil.exe, eval_cmd.exe et Debpar.exe. Ce sont des applications Windows 64 bit. A noter que le programme eval_cmd peut également être compilé avec Lazarus pour Windows et pour Linux.

## Organisation du répertoire
Un fichier projet .proj se trouve dans chaque répertoire 
* Sources\Nebutil\
* Sources\Nebula\
* Sources\eval_cmd\
* Sources\DebPar\

## Installation
Vous aurez besoin de Delphi 12.1 pour compiler les sources de Nébula. Il est disponible gratuitement pour un usage personnel. Il faut demander une licence gratuite valable un an sur le site d'Embarcadero.

## Synedit
Le package Synedit for VCL est nécessaire pour compiler le projet, il est accessible par l'outil Getit de Delphi, accessible par "Outils / gestionnaire de packages Getit"
En recherchant Synedit, un package "Synedit for VCL 2024.02" de TurboPack est proposé. Il suffit de l'installer. L'expérience montre qu'il faut l'installer avant d'ouvrir le projet Nebutil.

## Compilation des sources
Si le pré-requis de paramétrage de l'IDE a bien été réalisé, la compilation se fait très facilement.
1. Ouvrir le fichier .proj souhaité
2. Fichier > Construire (ou Shift + F9). Le programme compilé se trouve dans le répertoire exe situé au mêmae niveau que Sources

## Nebutil
Si Nebutil est exécuté une fois en mode administrateur il va le détecter et automatiquement s'associer avec le type de fichier .nbt.
Si lors de l'ouverture de l'aide on obtient une fenêtre blanche, chercher le fichier nebutil.chm situé dans le même répertoire que nebutil.exe et faire la manipulation suivante :
1. Clic-droit sur le fichier dans l'explorateur de fichier > Propriétés. Cocher la case débloquer

